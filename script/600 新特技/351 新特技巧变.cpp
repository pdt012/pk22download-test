﻿// ## 2023/05/05 # 江东新风 # 加入新的入参，目标地格，只会传入本来战法无法释放的地格 #
// ## 2020/10/01 # 江东新风 ##

namespace 战法_无视地形
{
	class Main
	{
		Main()
		{
			pk::set_func(51, pk::func51_t(callback));
		}

		bool callback(pk::unit@ unit, const pk::point& in dst_pos)
		{
			//如果目标地格是森林，兵种是弩兵，就相当于射手的判断了，不过射手判断已经执行，不需要在此填写
			//int unit_id = unit.get_id();
			//pk::trace("dst_pos" + dst_pos.x + dst_pos.y);
			//pk::trace(pk::format("外部测试-巧变判断,部队id{}",unit_id));
			//if (ch::has_skill(unit,125)) pk::trace("拥有特技巧变-外部");
			//else ch::u8debug("未拥有特技巧变-外部");
			return ch::has_skill(unit, 特技_巧变);
		}

	}

	Main main;
}