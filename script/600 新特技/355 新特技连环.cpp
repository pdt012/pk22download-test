﻿// ## 2023/05/06 # 江东新风 # 拆分连环特技 #

namespace 计策连环
{
	class Main
	{
		Main()
		{
			pk::set_func(69, pk::func69_t(callback));
		}

		int callback(int strategy_id, int unit_id, const pk::point& in dst_pos)
		{
			pk::unit@ unit = pk::get_unit(unit_id);
			int person_id = unit.who_has_skill(特技_连环);
			//pk::trace("strategy_id:" + strategy_id + ",unit_id："+unit_id + ",dst_pos：" + dst_pos.x + dst_pos.y);
			if (pk::is_valid_person_id(person_id)) return person_id;
			//没有满足连环的情况下， 如果有满足对应策略可以释放连环的，也返回正常的person_id
			//if ()
			return -1;//返回-1时则不触发连环

		}

	}

	Main main;
}