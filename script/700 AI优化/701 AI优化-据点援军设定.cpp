﻿// ## 2023/05/20 # 铃 # 翻译注释,增加注释,整理代码 ##
// ## 2022/02/14 # 江东新风 # 部分常量中文化 ##
// ## 2021/10/04 # 江东新风 # 部队任务_收复改为修复 ##
// ## 2020/10/30 # 江东新风 # 同步马术书大神的更新 ##
// ## 2020/07/26 ##
/*


// 작성자: 기마책사
// Update: '18.11.10   / 변경내용 : 거점원군 기능 신규 추가
// Update: '19.3.4     / 변경내용 : 거점원군 세팅값 일부 조정, 수송부대/전투부대 선택 알고리즘 적용, 원군부대수 조절기능 추가, 다수거점에서 원군출병 가능
// Update: '19.3.7     / 변경내용 : 교전거점 무장파병 기능 추가, 관문/항구로 수송시 병력/물자 최대치 조정 (증발방지), 출진거점 검색함수 개선
// Update: '19.3.14    / 변경내용 : 据点搜索模式 추가, 원군거점 검색오류 조치, 도시거점 원군인 경우 다방면 분산출진 기능 추가, 원군출병 거점의 전방/후방여부에 따라 기준병력 가변
// Update: '19.3.16    / 변경내용 : 교전병력 열세인 거점부터 원군파병
// Update: '19.3.17    / 변경내용 : 동맹세력원군 파병설정 추가
// Update: '19.3.30    / 변경내용 : 동맹세력원군 파병설정 유저세력 제외
// Update: '19.5.26    / 변경내용 : 难易度_超级 설정 시에만 동작하도록 변경
// Update: '20.2.8     / 변경내용 : 동맹세력원군은 무조건 전투부대로 출병하도록 수정
// Update: '20.9.15    / 변경내용 : 전투병과 노병특기 우대함수 오류 수정, 병력있는 빈거점으로 무장소환 추가, 거점원군 실행함수 분리, 거점공략 지원군 기능 추가
// Update: '20.9.20    / 변경내용 : 거점주변 검색 시 부대좌표의 영역도시 확인 추가 (교전 중인 타거점과 거리가 가까운 경우 오인식 안되도록 함)
//作者:骑马找书。
/ Update: 18.11.10  /变更内容:新增据点援军功能
/ Update: 19.03.04  /变更内容:据点援军配置值部分调整，应用运输部队/战斗部队选择算法，扩充援军部队数量调节功能，多据点可出兵援军
/ Update: 19.03.07  /变更内容:增加交战据点武装派兵功能，向关隘/港口运输时兵力/物资最大值(防止蒸发)，据点检索函数改善
/ Update: 19.03.14  /变更内容:倾巢倾巢式倾巢模式倾巢倾巢，查找援军据点错误措施，城市据点援军情况增加多方面分散出阵功能，援军出兵据点根据前方/后方与否而变更基准兵力
/ Update: 19.03.16  /变更内容:从交战兵力处于劣势的据点开始派遣援军
/ Update: 19.03.17  /变更内容:增加同盟势力的援军派兵设定
/ Update: 19.03.30  /变更内容:同盟势力援军派兵设定除玩家势力之外
/ Update: 19.05.26  /变更内容:只在设置易度_超级时启动
/ Update: 20.02.08  /变更内容:同盟势力援军无条件出兵战斗部队
/ Update: 20.09.15  /变更内容:战斗兵和弩兵特长优待舰首修正错误，添加武装兵力向空置据点召唤，据点援军实行舰首分离，据点攻略援军功能
/ Update: 20.09.20  /变更内容:搜索据点周边时，添加部队坐标上的区域城市确认(与交战的其他据点距离近时，不要误认)
*/

namespace AI优化_据点援军
{

    //=======================================================================================
    // 基地防御援军 (by 기마책사)
    const bool 据点援军_设定 = true; // 相邻城市援军运输队出征设置

    const int 据点援军_敌军搜索范围 = 5; // 从交战基地开始搜索敌军的距离（关口/港口最大为6）

    const int 据点援军_据点最小兵力 = 5000; // 非相邻敌方基地的后方基地，超过基准兵力的兵力将进行援军运输
    const int 据点援军_警戒兵力单位 = 3000; // 邻近敌方基地时，以警戒兵力单位递增基地最小兵力
    const int 据点援军_留存兵力单位 = 3000; // 邻近敌方基地时，以警戒兵力单位递增基地最小兵力

    const int 据点援军_最大战斗兵力 = 20000; // 出征战斗部队最大兵力标准
    const int 据点援军_最小出征兵力 = 5000;  // 出征部队最小兵力标准，不足最小兵力标准时不出征
    const int 据点援军_城市出征距离 = 2;     // 对于城市，在满足距离的基地进行援军出征（1 ~ 3之间），关口/港口只在距离1内搜索
    const int 据点援军_出征据点武将数 = 0;   // 援军出征基地的剩余武将数，达到标准数且未行动时可出征（在敌方基地附近时，剩余武将数按比例增加）

    const int 据点援军_据点搜索模式 = 1; // 0: 只搜索城市并进行援军出征，  1: 搜索城市+关口+港口并进行援军出征

    // 部队种类模式
    // ㄴ 使用模式2时，根据基地间距离，基地武将数量决定兵种选择（远距离基地，交战基地无武将或交战基地武将充足，出征基地武将不足时运输部队出征）
    const int 据点援军_部队种类模式 = 0;    // 2: 允许所有战斗和运输部队，  1: 只允许运输部队，  0: 只允许战斗部队
    const int 据点援军_交战据点武将数 = 10; // 应用部队种类模式2时: 交战基地武将数量达到标准时，认为武将充足并派遣运输部队出征

    const int 据点援军_战斗部队数 = 4; // 每回合可出征的最大部队数（但根据基地间距离，每个基地出征部队数每距离递减1，最少1个部队）
    const int 据点援军_运输部队数 = 2; // 每回合可出征的最大部队数

    const bool 据点援军_武将支援设定 = true; // 在空基地被敌军侵略时，从相邻基地派遣武将支援

    const bool 据点援军_同盟势力援军 = true; // 同盟势力的据点也可以派遣援军

    //------------------------------------------------------------
    // 据点攻略援军 (by 骑马书商)
    const bool 据点攻略_设定 = true; // 对邻近的攻略城市派遣支援部队设置（运输队，攻城武器，战斗部队）

    const int 据点攻略_战斗部队数 = 2; // 攻略支援军战斗部队出征部队数（最少1 ~ 最多3）

    const bool 据点攻略_允许运输部队 = false; // 攻略支援军运输队允许
    const int 据点攻略_运输部队数 = 1;        // 攻略支援军运输部队出征部队数（最少1 ~ 最多3）

    const bool 据点攻略_允许攻城兵器 = false; // 攻略支援军攻城武器允许
    const int 据点攻略_攻城部队数 = 3;        // 攻略支援军攻城部队出征部队数（最少1 ~ 最多5）

    //=======================================================================================

    class Main
    {
        pk::func261_t @prev_callback_261;
        Main()
        {
            @prev_callback_261 = cast<pk::func261_t @>(pk::get_func(261));
            pk::reset_func(261);
            pk::set_func(261, pk::func261_t(callback_261));
        }

        bool callback_261(pk::force @force)
        {
            int force_id = force.get_id();

            if (!pk::is_campaign())
            {
                // 玩家和贼势力除外
                // 不明白为什么只有超级难度才有AI逻辑,删除了难度限制.
                // if (!force.is_player() and pk::is_normal_force(force_id) and pk::get_scenario().difficulty == 难易度_超级)
                if (!force.is_player() and pk::is_normal_force(force_id))

                {

                    if (据点援军_设定)
                        func_reinforcement(force);
                    if (据点攻略_设定)
                        func_attack_support(force);
                }
            }

            if (prev_callback_261 !is null)
                return prev_callback_261(force);

            return false;
        }

        //---------------------------------------------------------------------------------------
        //          据点援军执行函数
        //---------------------------------------------------------------------------------------
        // 禁止修改
        pk::list<pk::building @> list_engage_base;      // 需要援军的据点列表
        pk::list<pk::building @> list_reinforce_base;   // 出征据点列表
        pk::array<int> arr_reinforce_count(据点_末, 0); // 援军候选据点出征目标数量
        pk::array<int> arr_engage_troops(据点_末, 0);   // 交战中据点的部队数量
        pk::array<int> unit_count(2, 0);                // [0] 战斗部队出征数量，[1] 运输部队出征数量

        //----- 据点援军执行函数
        void func_reinforcement(pk::force @force)
        {
            int force_id = force.get_id();

            list_engage_base.clear();
            list_reinforce_base.clear();
            clear_reinforce_info();

            for (int i = 0; i < 据点_末; i++)
            {
                pk::building @base = pk::get_building(i);
                pk::district @district = pk::get_district(base.get_district_id());

                if (base.get_force_id() == force_id and needReinforce(base)) // 寻找需要援军的据点
                {
                    // 空据点提供武将支援
                    if (据点援军_武将支援设定 and needPersonSupport(base))
                        PushPersonSupport(base); // 每回合在空据点提供一次武将支援（召唤命令）

                    // 寻找有部队但无武将支援的据点以便派遣武将支援
                    int send_person = getNearestReinforce(base, force, /*has_person*/ false);
                    if (据点援军_武将支援设定 and send_person != -1)
                        PushPersonSupport(pk::get_building(send_person)); // 向缺乏武将支援的据点派遣武将支援

                    // 寻找可以出征的候选据点
                    int src_target = getNearestReinforce(base, force, /*has_person*/ true);
                    if (src_target != -1)
                    {
                        list_engage_base.add(base); // 将需要援军的据点添加到列表中

                        // 如果需要援军的据点是城市，则增加援军候选据点的数量
                        if (base.get_id() < 城市_末)
                            arr_reinforce_count[src_target] += 1;
                    }
                }
            }

            // 按兵力差排序
            list_engage_base.sort(function(a, b) {
                if (a.get_id() < 城市_末 and b.get_id() < 城市_末)
                    return (main.arr_engage_troops[a.get_id()] > main.arr_engage_troops[b.get_id()]);

                return (a.get_id() > b.get_id());
            });

            // 向需要援军的据点派遣援军
            pk::array<pk::building @> arr_engage_base = pk::list_to_array(list_engage_base);
            for (int i = 0; i < int(arr_engage_base.length); i++)
            {
                list_reinforce_base.clear(); // 初始化已出征据点列表
                pk::building @base = arr_engage_base[i];

                unit_count[0] = 0;  // 初始化已出征战斗部队数
                unit_count[1] = 0;  // 初始化已出征运输部队数
                int push_count = 0; // 检查推送次数的变量

                int engage_troops = arr_engage_troops[base.get_id()];
                pk::array<int> max_count(2, 0); // 最大可出征部队数
                max_count[0] = pk::max(1, pk::min(据点援军_战斗部队数, 1 + engage_troops / 4000));
                max_count[1] = pk::max(1, 据点援军_运输部队数);

                // 包括出征据点搜索，执行出征命令的重复次数
                bool cmd = false;
                while (unit_count[0] < max_count[0] and unit_count[1] < max_count[1] and push_count < (max_count[0] + max_count[1]))
                {
                    push_count += 1;
                    cmd = (PushReinforce(base) or cmd); // 援军出征
                }
            }

            list_engage_base.clear();
            list_reinforce_base.clear();
            clear_reinforce_info();
        }

        /** 判断是否需要援军 */
        bool needReinforce(pk::building @base)
        {
            int enemy_units1 = 0;
            int enemy_units2 = 0;
            int enemy_units3 = 0;
            int enemy_troops1 = 0;
            int enemy_troops2 = 0;
            int enemy_troops3 = 0;
            int force_units3 = 0;

            // 搜索敌军的范围
            int ref_city_id = pk::get_city_id(base.pos);
            pk::building @ref_city = pk::get_building(ref_city_id);
            pk::array<pk::point> range = pk::range(base.pos, 1, ((base.facility == 设施_都市) ? (据点援军_敌军搜索范围 + 1) : pk::min(6, 据点援军_敌军搜索范围)));
            for (int i = 0; i < int(range.length); i++)
            {
                pk::point pos = range[i];
                pk::unit @unit = pk::get_unit(pos);
                if (pk::is_alive(unit))
                {
                    int pos_city_id = pk::get_city_id(unit.pos);
                    pk::building @pos_city = pk::get_building(pos_city_id);
                    if (pk::is_alive(pos_city) and pk::is_alive(ref_city) and pos_city_id == ref_city_id)
                    {
                        int distance = pk::get_distance(base.get_pos(), pos);
                        if (pk::is_enemy(base, unit))
                        {
                            if (distance <= 1)
                            {
                                enemy_units1++;
                            }
                            if (distance <= pk::max(3, (据点援军_敌军搜索范围 / 2)))
                            {
                                enemy_units2++;
                                enemy_troops2 += unit.troops;
                            }
                            if (distance <= 据点援军_敌军搜索范围)
                            {
                                enemy_units3++;
                                enemy_troops3 += unit.troops;
                            }
                        }
                        else if (base.get_force_id() == unit.get_force_id() and unit.type == 部队类型_战斗)
                        {
                            if (distance <= 据点援军_敌军搜索范围)
                            {
                                force_units3++;
                            }
                        }
                    }
                }
            }

            int base_troops = pk::get_troops(base);

            // 更新交战中的部队数量
            arr_engage_troops[base.get_id()] = (enemy_troops3 - (base_troops + force_units3));

            // 防守部队数量少于进攻部队数量
            if ((enemy_units1 >= 1 or enemy_units2 >= 2 or enemy_units3 >= 3) and force_units3 <= enemy_units3 and force_units3 > 0)
                return true;

            // 剩余士气必须触发援军出征的条件
            if (base_troops < int(enemy_troops3 * 1.0f) and int(base.hp) > pk::max(500, int(pk::get_max_hp(base) * 0.3f)) and int(base.hp) < int(pk::get_max_hp(base) * 0.9f))
                return true;

            // 剩余部队必须触发援军出征的条件
            if (base_troops < int(enemy_troops2 * 1.0f) and base_troops < int(enemy_troops3 * 0.5f) and int(base.hp) < pk::max(1000, int(pk::get_max_hp(base) * 0.5f)))
                return true;

            // 如果是关隘/港口，则没有部队，而且当敌军入侵时立即出动援军
            if (base.get_id() >= 城市_末 and base_troops == 0 and enemy_units3 > 0)
                return true;

            return false;
        }

        /** 获取距离指定敌方据点最近的友方据点。*/
        pk::building @dst_t;
        int getNearestReinforce(pk::building @dst, pk::force @force, bool has_person = true)
        {
            int best_src = -1;
            int best_distance = 1;

            int dst_id = dst.get_id();

            pk::city @city_of_dst = pk::get_city(pk::get_city_id(dst.pos));
            bool is_enemy_line = pk::is_enemy(city_of_dst, force);
            int max_distance = (dst_id >= 城市_末) ? (is_enemy_line ? 2 : 1) : pk::max(1, pk::min(3, 据点援军_城市出征距离));
            bool reinforce_mode = pk::is_enemy(force, dst); // true: 敌方势力，false: 己方势力或盟友势力
            if (reinforce_mode)
                max_distance = pk::min(2, max_distance); // 如果是攻打敌方势力，则将搜索距离限制在2以内

            @dst_t = @dst;
            pk::list<pk::building @> src_list;

            int search_base = 城市_末;
            if (has_person and 据点援军_据点搜索模式 == 0)
                search_base = 城市_末;
            else if (has_person and 据点援军_据点搜索模式 == 1)
                search_base = 据点_末;
            else if (!has_person)
                search_base = 据点_末;

            // 搜索友方据点
            for (int i = 0; i < search_base; i++)
            {
                pk::building @src = pk::get_building(i);
                int src_id = src.get_id();

                bool check_reinforce_force = false;
                if (!reinforce_mode) // 搜索己方或盟友据点
                    check_reinforce_force = (据点援军_同盟势力援军) ? (!pk::is_enemy(dst, src) and !src.is_player()) : (dst.get_force_id() == src.get_force_id() and !src.is_player());
                else // 搜索其他势力据点（在函数中指定的势力）
                    check_reinforce_force = (pk::is_enemy(dst, src) and !src.is_player()) and (force.get_id() == src.get_force_id());

                int enemy_weight = countNeighborEnemyBase(src); // 敌方兵力加权

                // 根据敌方兵力数,计算留存量,可用兵力小于留存量则不支援.
                int ref_troops = enemy_weight * 据点援军_留存兵力单位 + 据点援军_最小出征兵力;
                if (pk::get_troops(src) < ref_troops)
                    continue; // 兵力不足

                // 没有武将则跳过
                auto person_list = pk::get_idle_person_list(src);

                if (person_list.count == 0)
                    continue; // 武将不足

                if (dst_id == src_id)
                    continue;

                // 计算据点距离
                // 只能从建筑距离为x的城市支援,不能用city_distance,因为2的city_distance有可能已经很远了
                int base_dist = pk::get_building_distance(dst_id, src_id, dst.get_force_id());
                if ((base_dist > max_distance))
                    continue;

                // 如果该据点不是目标据点，不在战斗中，不是需要援军的据点，不是已经派兵的据点，也不是已经攻击的据点，则符合条件
                if (check_reinforce_force and !pk::enemies_around(src) and !needReinforce(src) and !list_reinforce_base.contains(src) and !list_attack_base.contains(src))
                {

                    // 计算出兵数量
                    int src_troops = pk::get_troops(src);
                    int src_food = pk::get_food(src);

                    int ref_p_count = pk::max(1, (enemy_weight / 4) + 据点援军_出征据点武将数);
                    int person_count = pk::get_person_list(src, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般)).count;
                    int idle_p_count = pk::get_idle_person_list(src).count;

                    int weapon_amount = 0;
                    int siege_amount = 0;

                    for (int j = 兵器_枪; j <= 兵器_战马; j++) // 统计长枪、弓箭等兵器数量
                    {
                        weapon_amount += pk::get_weapon_amount(src, j);
                    }
                    for (int j = 兵器_冲车; j <= 兵器_木兽; j++) // 统计攻城器材数量
                    {
                        siege_amount += pk::get_weapon_amount(src, j);
                    }

                    // 符合派兵条件（拥有武将、出兵数量符合要求、拥有足够的兵器、食物）
                    if (((has_person and person_count >= ref_p_count and idle_p_count > 0) or (!has_person and person_count == 0)) and src_troops > ref_troops and weapon_amount >= ref_troops and src_food >= int(1.5f * ref_troops))
                    {
                        if (!reinforce_mode) // 己方或盟友势力
                        {
                            best_src = src_id;
                            src_list.add(src); // 可派兵的据点列表
                        }
                        else if (reinforce_mode and siege_amount > 2) // 攻打敌方势力时，还需要有攻城器材
                        {
                            best_src = src_id;
                            src_list.add(src); // 可派兵的据点列表
                        }
                    }
                }
            }

            // 对可派兵的据点进行排序，按照据点距离和坐标距离升序排列
            //对于同样距离的城市来说,只要是1格以内的建筑,都应该以兵力优先而不是pos距离优先,因为兵力优先可以在最多兵力的据点出证后继续触发其他据点共同支援,pos优先的话始终只有一个据点可以支援.
            if (src_list.count == 0)
                best_src = -1;
            else
            {
                src_list.sort(function(a, b) {
                    int build_dist_a = pk::get_building_distance(a.get_id(), main.dst_t.get_id(), a.get_force_id());
                    int build_dist_b = pk::get_building_distance(b.get_id(), main.dst_t.get_id(), b.get_force_id());

                    int troops_a = pk::get_troops(a);
                    int troops_b = pk::get_troops(b);

                    return (build_dist_a != build_dist_b) ? (troops_a > troops_b) : (build_dist_a < build_dist_b);
                });
                best_src = src_list[0].get_id();
            }

            return best_src;
        }

        //----------------------------------------------------------------------------------

        // ***** 检查周围敌方兵力总量 ***** //
        int countNeighborEnemyBase(pk::building @src)
        {
            int weight = 0;
            int troops = 0;
            for (int i = 0; i < 据点_末; i++)
            {
                pk::building @dst = pk::get_building(i);
                int src_id = src.get_id();
                int dst_id = dst.get_id();

                if (src_id != dst_id and pk::is_neighbor_base(src_id, dst_id) and pk::is_enemy(src, dst))
                    troops += pk::get_troops(dst);
            }

            weight = int(troops / 据点援军_警戒兵力单位);
            return weight;
        }

        //----------------------------------------------------------------------------------

        // ***** 出征命令 ***** //
        bool PushReinforce(pk::building @dst_base)
        {
            // 检查是否有附近的出征驻军基地
            pk::force @force = pk::get_force(dst_base.get_force_id());
            int target = getNearestReinforce(dst_base, force, /* has_person */ true);
            if (target == -1)
                return false;

            // 统帅出征基地
            pk::building @src_base = pk::get_building(target);
            int src_id = src_base.get_id();
            int dst_id = dst_base.get_id();

            // 判断出征部队类型
            int reinforce_type = -1;
            if (据点援军_同盟势力援军 and !pk::is_enemy(src_base, dst_base) and src_base.get_force_id() != dst_base.get_force_id())
                reinforce_type = 部队类型_战斗; // 同盟势力统帅一律为战斗部队
            else
            {
                if (据点援军_部队种类模式 == 0)
                    reinforce_type = 部队类型_战斗; // 一律为战斗部队
                if (据点援军_部队种类模式 == 1)
                    reinforce_type = 部队类型_运输; // 一律为运输部队
                if (据点援军_部队种类模式 == 2)
                    reinforce_type = get_reinforce_unit_type(src_base, dst_base);
            }
            if (reinforce_type < 0 or reinforce_type > 部队类型_末)
                return false;

            // 出征基地与目标基地之间的距离
            int distance = pk::get_building_distance(src_id, dst_id, src_base.get_force_id());

            // 当目标基地较多时进行分散
            int divide = pk::max(1, arr_reinforce_count[src_id]);

            // 根据基地距离和类型限制可出征部队数
            int engage_troops = arr_engage_troops[dst_base.get_id()];
            pk::array<int> max_unit(2, 0); // 最大可出征部队数
            max_unit[0] = pk::max(1, pk::min(1 + engage_troops / 4000, int((据点援军_战斗部队数 - (distance - 1)) / divide)));
            max_unit[1] = pk::max(1, int(据点援军_运输部队数 / divide));

            int push_count = 0; // 检查 push 次数的变量

            // 在出征据点反复执行出征命令
            while (unit_count[0] < max_unit[0] and unit_count[1] < max_unit[1] and push_count < (max_unit[0] + max_unit[1]))
            {
                push_count += 1;

                if (reinforce_type == 部队类型_运输)
                {
                    int unit_id = PushTransportUnit(src_base, dst_base, true); // 运输部队出征
                    if (unit_id != -1)
                    {
                        unit_count[1] += 1; // 运输部队计数
                        if (!list_reinforce_base.contains(src_base))
                            list_reinforce_base.add(src_base);
                    }
                }
                else if (reinforce_type == 部队类型_战斗)
                {
                    int unit_id = PushCombatUnit(src_base, dst_base, true); // 战斗部队出征
                    if (unit_id != -1)
                    {
                        unit_count[0] += 1; // 战斗部队计数
                        if (!list_reinforce_base.contains(src_base))
                            list_reinforce_base.add(src_base);
                    }
                }
            }
            if ((unit_count[0] + unit_count[1]) > 0)
                return true;

            return false;
        }
        //----------------------------------------------------------------------------------

        // 根据出征据点和目标据点的武将数量决定出征部队类型
        int get_reinforce_unit_type(pk::building @src_base, pk::building @dst_base)
        {
            if (!pk::is_alive(src_base) or !pk::is_alive(dst_base))
                return -1;

            // 各据点的隶属武将信息
            pk::list<pk::person @> list_src_men = pk::get_person_list(src_base, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般));
            pk::list<pk::person @> list_dst_men = pk::get_person_list(dst_base, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般));

            // 武将数量
            int src_count = list_src_men.count; // 出征据点未行动武将数量
            int dst_count = list_dst_men.count; // 目标据点全部武将数量

            // 据点距离
            int distance = pk::get_building_distance(src_base.get_id(), dst_base.get_id(), src_base.get_force_id());

            // 决定出征部队类型
            int reinforce_type = -1;
            if (pk::get_troops(dst_base) < 3000 and unit_count[1] < 1)
                reinforce_type = 部队类型_运输; // 如果目标据点兵力不足且未派遣运输部队，派遣运输部队出征

            else if (distance <= 1)
                reinforce_type = 部队类型_战斗; // 如果距离较近：战斗部队

            else if (dst_count == 0)
                reinforce_type = 部队类型_战斗; // 如果目标据点没有武将：战斗部队

            else if (dst_count >= pk::max(1, 据点援军_交战据点武将数))
                reinforce_type = 部队类型_运输; // 如果目标据点有 n 人以上：运输部队

            else if (src_count <= pk::max(1, 据点援军_出征据点武将数))
                reinforce_type = 部队类型_运输; // 如果出征武将较少：运输部队

            else
                reinforce_type = 部队类型_战斗; // 战斗部队

            return reinforce_type;
        }

        // 根据建筑ID返回据点类型值
        int get_base_type(int building_id)
        {
            int type = -1;
            if (据点_城市开始 <= building_id and building_id < 城市_末)
                type = 0;
            if (建筑_关卡开始 <= building_id and building_id < 据点_关卡末)
                type = 1;
            if (据点_港口开始 <= building_id and building_id < 建筑_港口末)
                type = 2;
            return type;
        }

        //----------------------------------------------------------------------------------
        //  运输部队出征处理
        int PushTransportUnit(pk::building @src_base, pk::building @dst_base, bool do_cmd = true)
        {
            if (!pk::is_alive(src_base) or !pk::is_alive(dst_base))
                return -1;

            int enemy_weight = countNeighborEnemyBase(src_base); //  敌方兵力权重
            int ref_troops = (enemy_weight * 据点援军_警戒兵力单位) + 据点援军_据点最小兵力;
            if (pk::get_troops(src_base) <= (ref_troops + 据点援军_最小出征兵力))
                return -1; // 兵力不足

            int person_count = pk::get_person_list(src_base, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般)).count;
            if (person_count <= pk::max(1, (enemy_weight / 4) + 据点援军_出征据点武将数))
                return -1; // 装备不足

            auto person_list = pk::get_idle_person_list(src_base);
            int base_type = get_base_type(dst_base.get_id());
            if (base_type == 0 and person_list.count <= 1)
                return -1; // 装备不足（城市）
            if (base_type > 0 and person_list.count < 1)
                return -1; // 装备不足（关口，港口）

            // 按政治能力从高到低排序。
            person_list.sort(function(a, b) {
                return (float(a.stat[武将能力_政治]) / float(a.stat[武将能力_武力])) > (float(b.stat[武将能力_政治]) / float(b.stat[武将能力_武力]));
            });
            pk::person @leader = pk::get_person(person_list[0].get_id());

            // 援军兵力计算：基准兵力超过部分
            int reinforce_troops = pk::min(pk::max(1, (45000 - ref_troops)), pk::max(1, pk::get_troops(src_base) - ref_troops));

            // 如果目标据点是关口/港口，则限制援军兵力
            if (base_type > 0)
            {
                if (pk::has_tech(dst_base, 技巧_港关扩张))
                    reinforce_troops = pk::min(20000, reinforce_troops);
                else
                    reinforce_troops = pk::min(10000, reinforce_troops);
            }

            // 计算兵力
            int unit_food = int(pk::min(2.0f * reinforce_troops, pk::max(0.5f * pk::get_food(src_base), 1.2f * reinforce_troops)));
            if (unit_food < int(0.5f * reinforce_troops))
                return -1; //  兵力不足

            // 出征命令
            if (do_cmd)
            {
                // 创建出征命令信息。
                pk::com_deploy_cmd_info cmd;
                @cmd.base = @src_base;
                cmd.type = 部队类型_运输;
                cmd.member[0] = leader.get_id();
                cmd.gold = (pk::get_gold(src_base) >= 1000) ? int(pk::min(2000.f, pk::get_gold(src_base) * 0.1f)) : 0;
                cmd.food = pk::min(unit_food, 500000);
                cmd.troops = pk::min(60000, pk::max(1, reinforce_troops));

                // 合计枪戟马兵器数量
                int weapon_sum = 0;
                for (int j = 兵器_枪; j <= 兵器_战马; j++)
                {
                    weapon_sum += pk::get_weapon_amount(src_base, j);
                }

                // 兵器总数量与兵力相同，按各兵器比例分配
                int i = 0;
                for (int weapon_id = 兵器_枪; weapon_id <= 兵器_战马; weapon_id++)
                {
                    int weapon_amount = int(pk::get_weapon_amount(src_base, weapon_id) * 0.9f);
                    if (weapon_id <= 兵器_战马 and weapon_amount > 0)
                    {
                        cmd.weapon_id[i] = weapon_id;
                        cmd.weapon_amount[i] = pk::min(100000, weapon_amount, reinforce_troops * weapon_amount / weapon_sum);
                        i++;
                    }
                    else if (is_siege_weapon(weapon_id) and weapon_amount > 0)
                    {
                        cmd.weapon_id[i] = weapon_id;
                        cmd.weapon_amount[i] = pk::min(100, (1 + weapon_amount / 4));
                        i++;
                    }
                }

                cmd.order = 部队任务_移动;
                cmd.target_pos = dst_base.get_pos(); // 目标是战斗中的据点

                // 출진.
                int unit_id = pk::command(cmd);

                pk::unit @unit_cmd = pk::get_unit(unit_id);
                if (pk::is_alive(unit_cmd))
                {
                    unit_cmd.action_done = true;

                    if (调试模式)
                    {
                        string src_name = pk::decode(pk::get_name(src_base));
                        string dst_name = pk::decode(pk::get_name(dst_base));
                        string unit_name = pk::decode(pk::get_name(leader));
                        string order_str = get_order_info(unit_cmd.order);
                        string cmd_info = (pk::is_enemy(src_base, dst_base)) ? "据点攻略" : "据点援军";
                        pk::info(pk::format("{}: {} {}: {}→{}", cmd_info, unit_name, order_str, src_name, dst_name));
                    }
                    return unit_id;
                }
            }

            return -1;
        }

        //----------------------------------------------------------------------------------

        // 战斗单位出征处理
        int PushCombatUnit(pk::building @src_base, pk::building @dst_base, bool do_cmd = true)
        {
            if (!pk::is_alive(src_base) or !pk::is_alive(dst_base))
                return -1;

            // 修改了此处出征兵力不应该考虑敌军兵力,难道敌军太多就不支援了嘛.实际上不支援的情况大多数都和兵力判决相关
            int enemy_weight = countNeighborEnemyBase(src_base); // 敌方兵力加权

            // 根据敌方兵力数,计算留存量,.可用兵力小于留存量则不支援.
            int ref_troops = enemy_weight * 据点援军_留存兵力单位 + 据点援军_最小出征兵力;
            if (pk::get_troops(src_base) < ref_troops)
                return -1; // 兵力不足

            // 如果有能力,需要支援的时候1支部队也应该出征.
            auto person_list = pk::get_idle_person_list(src_base);

            if (person_list.count == 0)
                return -1; // 武将不足

            // 如果目标建筑为关口/港口，则优先出动弩兵。
            int base_type = get_base_type(dst_base.get_id());
            cmd_archer = (base_type > 0);

            // 按统帅+武力排序。
            person_list.sort(function(a, b) {
                if (main.cmd_archer) // 优先出动弩兵
                {
                    bool a_archer = (pk::has_skill(a, 特技_弓神) or pk::has_skill(a, 特技_弓将) or pk::has_skill(a, 特技_射手));
                    bool b_archer = (pk::has_skill(b, 特技_弓神) or pk::has_skill(b, 特技_弓将) or pk::has_skill(b, 特技_射手));
                    if (a_archer and !b_archer)
                        return true;
                    if (!a_archer and b_archer)
                        return false;
                    // 弩兵适性
                    if (a.tekisei[兵种_弩兵] != b.tekisei[兵种_弩兵])
                        return (a.tekisei[兵种_弩兵] > b.tekisei[兵种_弩兵]);
                    // 武将能力
                    return (a.stat[武将能力_武力] + a.stat[武将能力_统率]) > (b.stat[武将能力_武力] + b.stat[武将能力_统率]);
                }

                return (a.stat[武将能力_武力] + a.stat[武将能力_统率]) > (b.stat[武将能力_武力] + b.stat[武将能力_统率]);
            });
            pk::person @leader = pk::get_person(person_list[0].get_id());

            // 计算出发部队兵力：超过基准兵力，确认指挥能力
            int reinforce_troops = pk::min(据点援军_最大战斗兵力, pk::get_command(leader), pk::max(1, pk::get_troops(src_base) - ref_troops));

            // 选择最佳的武器
            int ground_weapon_id = 兵器_剑;
            int water_weapon_id = 兵器_走舸;
            int unit_troops = reinforce_troops;

            // 选择陆上武器
            get_ground_weapon(src_base, leader, reinforce_troops, ground_weapon_id, unit_troops);
            if (ground_weapon_id == 0)
                return -1; // 兵器不足

            // 选择水上武器
            if (check_building_has_port(src_base) and check_building_has_port(dst_base))
            {
                if (leader.tekisei[兵种_水军] == 适性_C)
                    water_weapon_id = 兵器_走舸;
                else
                {
                    if (pk::get_weapon_amount(src_base, 兵器_楼船) > 0)
                        water_weapon_id = 兵器_楼船;
                    else if (pk::get_weapon_amount(src_base, 兵器_斗舰) > 0)
                        water_weapon_id = 兵器_斗舰;
                    else
                        water_weapon_id = 兵器_走舸;
                }
            }
            else
                water_weapon_id = 兵器_走舸;

            // 计算部队消耗
            // int unit_food = int(pk::min(2.0f * unit_troops, pk::max(0.5f * pk::get_food(src_base), 1.2f * unit_troops)));
            int unit_food = int(pk::min(unit_troops * 2, int(0.5f * pk::get_food(src_base))));
            if (unit_food < int(0.5f * unit_troops))
                return -1;

            if (do_cmd)
            {
                // 出征命令信息生成
                pk::com_deploy_cmd_info cmd;
                @cmd.base = @src_base;
                cmd.type = 部队类型_战斗;
                cmd.member[0] = leader.get_id();
                cmd.gold = (pk::get_gold(src_base) >= 1000) ? int(pk::min(1000.f, pk::get_gold(src_base) * 0.1f)) : 0;
                cmd.troops = pk::max(1, unit_troops);
                cmd.weapon_id[0] = ground_weapon_id;
                cmd.weapon_id[1] = water_weapon_id;
                cmd.weapon_amount[0] = (is_siege_weapon(ground_weapon_id)) ? 1 : pk::max(1, unit_troops);
                cmd.weapon_amount[1] = (water_weapon_id == 兵器_走舸) ? 0 : 1;
                cmd.food = pk::min(50000, unit_food);

                cmd.order = (src_base.get_force_id() == dst_base.get_force_id()) ? 部队任务_攻击 : ((pk::is_enemy(src_base, dst_base)) ? 部队任务_攻击 : 部队任务_移动); // 자세력, 적거점, 동맹군

                cmd.target_pos = dst_base.get_pos(); // 目标是战斗中的据点

                int unit_id = pk::command(cmd);

                pk::unit @unit_cmd = pk::get_unit(unit_id);
                if (pk::is_alive(unit_cmd))
                {
                    unit_cmd.action_done = true;

                    if (调试模式)
                    {
                        string src_name = pk::decode(pk::get_name(src_base));
                        string dst_name = pk::decode(pk::get_name(dst_base));
                        string unit_name = pk::decode(pk::get_name(leader));
                        string order_str = get_order_info(unit_cmd.order);
                        string cmd_info = (pk::is_enemy(src_base, dst_base)) ? "据点攻略" : "据点援军";
                        pk::info(pk::format("{}: {} {}: {}→{}", cmd_info, unit_name, order_str, src_name, dst_name));
                    }
                    return unit_id;
                }
            }

            return -1;
        }

        // 判断是否优先派遣弩兵出征
        bool cmd_archer = false;

        // 攻城部队出征处理
        int PushSiegeUnit(pk::building @src_base, pk::building @dst_base, bool do_cmd = true)
        {
            if (!pk::is_alive(src_base) or !pk::is_alive(dst_base))
                return -1;

            int enemy_weight = countNeighborEnemyBase(src_base); // 敌军兵力加权值
            int ref_troops = (enemy_weight * 据点援军_警戒兵力单位) + 据点援军_据点最小兵力;
            if (pk::get_troops(src_base) <= (ref_troops + 据点援军_最小出征兵力))
                return -1; // 兵力不足

            int person_count = pk::get_person_list(src_base, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般)).count;
            if (person_count <= pk::max(1, (enemy_weight / 4) + 据点援军_出征据点武将数))
                return -1; // 武将不足

            auto person_list = pk::get_idle_person_list(src_base);
            if (person_list.count == 0)
                return -1; // 武将不足

            // 检查攻城兵器库存
            int siege_dir_id = (pk::has_tech(src_base, 기교_목수개발)) ? 兵器_木兽 : 兵器_冲车;
            int siege_rng_id = (pk::has_tech(src_base, 기교_투석개발)) ? 兵器_投石 : 兵器_井阑;
            int amt_siege_dir = pk::get_weapon_amount(src_base, siege_dir_id); // 直接攻城兵器数量
            int amt_siege_rng = pk::get_weapon_amount(src_base, siege_rng_id); // 间接攻城兵器数量
            if ((amt_siege_rng + amt_siege_dir) == 0)
                return -1; // 没有攻城兵器

            // 根据攻城特技、体力恢复特技和统率力与武力的高低排序，优先选择攻城特技，其次是体力恢复特技，最后根据兵种适性和武将能力排序
            person_list.sort(function(a, b) {
                // 优先选择攻城特技
                bool a_skill = (pk::has_skill(a, 特技_工神) or pk::has_skill(a, 特技_攻城) or pk::has_skill(a, 特技_射程));
                bool b_skill = (pk::has_skill(b, 特技_工神) or pk::has_skill(b, 特技_攻城) or pk::has_skill(b, 特技_射程));
                if (a_skill and !b_skill)
                    return true;
                if (!a_skill and b_skill)
                    return false;
                // 其次选择体力恢复特技
                bool a_energy = (pk::has_skill(a, 特技_奏乐) or pk::has_skill(a, 特技_诗想) or pk::has_skill(a, 特技_怒发));
                bool b_energy = (pk::has_skill(b, 特技_奏乐) or pk::has_skill(b, 特技_诗想) or pk::has_skill(b, 特技_怒发));
                if (a_energy and !b_energy)
                    return true;
                if (!a_energy and b_energy)
                    return false;
                // 再根据兵种适性排序
                if (a.tekisei[兵种_兵器] != b.tekisei[兵种_兵器])
                    return (a.tekisei[兵种_兵器] > b.tekisei[兵种_兵器]);
                // 最后根据武将能力排序
                return (a.stat[武将能力_武力] + a.stat[武将能力_统率]) > (b.stat[武将能力_武力] + b.stat[武将能力_统率]);
            });
            pk::person @leader = pk::get_person(person_list[0].get_id());

            // 计算部队兵力：超出基准兵力，检查指挥兵力
            int reinforce_troops = pk::min(据点援军_最大战斗兵力, pk::get_command(leader), pk::max(1, pk::get_troops(src_base) - ref_troops));

            // 选择最佳武器
            int ground_weapon_id = 兵器_剑;
            int water_weapon_id = 兵器_走舸;
            int unit_troops = reinforce_troops;

            // 从间接攻城兵器开始优先选择(射精特技为间接，攻城特技为直接兵器优先安排)
            if (amt_siege_rng > 0 and pk::has_skill(leader, 特技_射程))
                ground_weapon_id = siege_rng_id;
            else if (amt_siege_dir > 0 and pk::has_skill(leader, 特技_攻城))
                ground_weapon_id = siege_dir_id;
            else if (amt_siege_rng > 0)
                ground_weapon_id = siege_rng_id;
            else if (amt_siege_dir > 0)
                ground_weapon_id = siege_dir_id;

            // 粮食数量计算
            int unit_food = int(pk::min(2.0f * unit_troops, pk::max(0.5f * pk::get_food(src_base), 1.2f * unit_troops)));
            if (unit_food < int(0.5f * unit_troops))
                return -1; // 粮食不足

            // 出征命令
            if (do_cmd)
            {
                // 生成出征命令信息，创建出征命令信息。
                pk::com_deploy_cmd_info cmd;
                @cmd.base = @src_base;
                cmd.type = 部队类型_战斗;
                cmd.member[0] = leader.get_id();
                cmd.gold = 0; // 攻城器械部队无法建造，因此不携带金币
                cmd.troops = pk::max(1, unit_troops);
                cmd.weapon_id[0] = ground_weapon_id;
                cmd.weapon_id[1] = water_weapon_id;
                cmd.weapon_amount[0] = (is_siege_weapon(ground_weapon_id)) ? 1 : pk::max(1, unit_troops);
                cmd.weapon_amount[1] = (water_weapon_id == 兵器_走舸) ? 0 : 1;
                cmd.food = pk::min(50000, unit_food);

                cmd.order = (src_base.get_force_id() == dst_base.get_force_id()) ? 部队任务_攻击 : ((pk::is_enemy(src_base, dst_base)) ? 部队任务_攻击 : 部队任务_移动); // 同势力、敌据点、盟军

                cmd.target_pos = dst_base.get_pos(); // 目标是正在战斗的据点

                // 出征。
                int unit_id = pk::command(cmd);
                pk::unit @unit_cmd = pk::get_unit(unit_id);
                if (pk::is_alive(unit_cmd))
                {
                    unit_cmd.action_done = true;

                    if (调试模式)
                    {
                        string src_name = pk::decode(pk::get_name(src_base));
                        string dst_name = pk::decode(pk::get_name(dst_base));
                        string unit_name = pk::decode(pk::get_name(leader));
                        string order_str = get_order_info(unit_cmd.order);
                        string cmd_info = (pk::is_enemy(src_base, dst_base)) ? "据点攻略" : "据点援军";
                        pk::info(pk::format("{}: {} {}: {}→{}", cmd_info, unit_name, order_str, src_name, dst_name));
                    }
                    return unit_id;
                }
            }

            return -1;
        }

        //----------------------------------------------------------------------------------

        // 武器选择函数
        void get_ground_weapon(pk::building @base, pk::person @leader, int troops_max, int &out weapon_sel, int &out troops_sel)
        {
            int troops_min = 据点援军_最小出征兵力;
            int weapon_max = 0;
            int best_tekisei = 适性_C;

            weapon_sel = 兵器_剑;
            troops_sel = 0;

            // 弩兵优先出征
            if (cmd_archer)
            {
                int tekisei = leader.tekisei[pk::equipment_id_to_heishu(兵种_弩兵)];
                int weapon = pk::get_weapon_amount(base, 兵器_弩);
                if (troops_min <= weapon and 适性_B <= tekisei)
                {
                    weapon_sel = 兵器_弩;
                    troops_sel = pk::min(weapon, troops_max);
                }
            }
            // 普通出征
            else
            {
                // 检查拥有出征兵力以上的武器的最高适性
                for (int id = 兵器_枪; id <= 兵器_战马; id++)
                {
                    int tekisei = leader.tekisei[pk::equipment_id_to_heishu(id)];
                    int weapon_t = pk::get_weapon_amount(base, id);
                    if (troops_min <= weapon_t and best_tekisei <= tekisei)
                        best_tekisei = tekisei;
                }

                // 选择最高适性以上并拥有最多武器的兵种
                for (int id = 兵器_枪; id <= 兵器_战马; id++)
                {
                    int tekisei = leader.tekisei[pk::equipment_id_to_heishu(id)];
                    int weapon_t = pk::get_weapon_amount(base, id);
                    if (troops_min <= weapon_t and weapon_max <= weapon_t and best_tekisei <= tekisei)
                    {
                        best_tekisei = tekisei;
                        weapon_max = weapon_t;
                        weapon_sel = id;
                        troops_sel = pk::min(weapon_max, troops_max);
                    }
                }
            }

            if (weapon_sel == 0)
            {
                troops_sel = troops_min;
            }
        }

        bool is_siege_weapon(int weapon_id)
        {
            if (兵器_冲车 <= weapon_id and weapon_id <= 兵器_木兽)
                return true;
            return false;
        }

        bool check_building_has_port(pk::building @base)
        {
            bool has_port = false;
            int base_id = base.get_id();
            if (据点_关卡末 <= base_id and base_id < 建筑_港口末)
                return true;

            if (据点_城市开始 <= base_id and base_id < 城市_末)
            {
                pk::city @city = pk::building_to_city(base);
                for (int i = 0; i < 5; i++)
                {
                    int sub_id = city.gate[i];
                    if (sub_id != -1)
                    {
                        pk::building @sub_t = pk::get_building(sub_id);
                        if (pk::is_alive(sub_t) and (据点_关卡末 <= sub_id and sub_id < 建筑_港口末))
                            return true;
                    }
                }
            }
            return false;
        }

        //----------------------------------------------------------------------------------

        // 出征目标据点计数初始化
        void clear_reinforce_info()
        {
            for (int i = 0; i < 据点_末; i++)
            {
                arr_reinforce_count[i] = 0; // 将各据点所需援军数量初始化为0
            }
        }

        //----------------------------------------------------------------------------------
        // 空城支援
        //----------------------------------------------------------------------------------

        // 判断是否需要派遣武将提供支援
        bool needPersonSupport(pk::building @base)
        {
            if (!pk::is_alive(base))
                return false;

            int enemy_troops = 0;                                                                        // 周围敌军数量
            pk::array<pk::point> range = pk::range(base.pos, 1, ((base.facility == 设施_都市) ? 6 : 5)); // 获取目标建筑周围的点
            for (int i = 0; i < int(range.length); i++)
            {
                pk::unit @unit = pk::get_unit(range[i]);
                if (pk::is_alive(unit) and pk::is_enemy(base, unit))
                    enemy_troops += unit.troops; // 统计周围敌军兵力
            }

            int count = pk::get_person_list(base, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般)).count; // 获取目标建筑中符合条件的武将数量
            if (count == 0 and pk::get_troops(base) >= 1000 and (pk::enemies_around(base) or enemy_troops >= 4000))   // 如果目标建筑没有符合条件的武将、兵力大于等于1000、周围有敌军或周围敌军总兵力大于等于4000，则需要派遣武将提供支援
                return true;

            return false;
        }

        // ***** 派遣武将提供支援 ***** //
        bool PushPersonSupport(pk::building @dst_base)
        {
            // 检查是否有附近据点有武将
            int target = getNearestPerson(dst_base);
            if (target == -1)
                return false;

            // 可以提供支援的武将所在据点
            pk::building @src_base = pk::get_building(target);

            int person_count = pk::get_person_list(src_base, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般)).count;
            if (person_count <= pk::max(1, 据点援军_出征据点武将数))
                return false; // 武将不足

            // 未行动武将列表
            auto person_list = pk::get_idle_person_list(src_base);
            if (person_list.count == 0)
                return false; // 武将不足

            // 按统率+武力从高到低排序
            person_list.sort(function(a, b) {
                return (a.stat[武将能力_武力] + a.stat[武将能力_统率]) > (b.stat[武将能力_武力] + b.stat[武将能力_统率]);
            });

            // 需要召唤的武将数量
            int max = pk::max(1, pk::min(5, (person_list.count / 4)));

            // 召唤目标武将
            pk::list<pk::person @> actors;
            for (int i = 0; i < max; i++)
            {
                actors.add(person_list[i]);
            }

            // 召唤命令
            pk::summon_cmd_info cmd_summon;
            @cmd_summon.base = dst_base;
            cmd_summon.actors = actors;
            pk::command(cmd_summon);

            return true;
        }

        /** 获取最近的带有士兵的友方据点。 */
        int getNearestPerson(pk::building @dst)
        {
            int best_src = -1;
            int best_distance = 0;

            // 遍历所有据点。
            for (int i = 0; i < 据点_末; i++)
            {
                pk::building @src = pk::get_building(i);

                // 如果源据点与目标据点不同，且在同一势力中，且周围没有敌人，也没有需要增援的情况，则进行以下判断。
                if (dst.get_id() != i and dst.get_force_id() == src.get_force_id() and (!pk::enemies_around(src) and !needReinforce(src)))
                {
                    // 计算距离，并获取源据点中的闲置士兵列表。
                    int distance = pk::get_building_distance(dst.get_id(), i, dst.get_force_id());
                    auto person_list = pk::get_idle_person_list(src);

                    // 根据据点援军_城市出征距离，确定是否应该前往该据点增援。如果距离小于等于据点援军_城市出征距离，并且该据点的闲置士兵数大于据点援军_出征据点武将数，则更新最佳源据点和最佳距离。

                    int max_distance = pk::max(1, pk::min(3, 据点援军_城市出征距离));
                    if ((0 <= distance and distance < best_distance and distance <= max_distance) or best_src == -1)
                    {
                        if (person_list.count > 据点援军_出征据点武将数)
                        {
                            best_src = i;
                            best_distance = distance;
                        }
                    }
                }
            }
            return best_src;
        }

        //---------------------------------------------------------------------------------------
        // 支援执行函数
        //---------------------------------------------------------------------------------------
        pk::list<pk::building @> list_target_base; // 需要攻击的交战据点列表
        pk::list<pk::building @> list_attack_base; // 攻击部队出征据点列表

        void func_attack_support(pk::force @force)
        {
            int force_id = force.get_id();

            // 每回合更新势力城市区域：交战中的部队数量和编队数量
            clear_engage_info();
            update_engage_info();
            list_target_base.clear();
            list_attack_base.clear();

            for (int i = (城市_末 - 1); i >= 0; i--)
            {
                pk::building @dst_base = pk::get_building(i);
                int atk_force_id = get_force_invaded(dst_base);
                if (pk::is_enemy(force, dst_base) and is_invaded_base(dst_base) and atk_force_id == force_id) // 与其他势力交战中的据点
                {
                    int src_target = getNearestReinforce(dst_base, force, /*has_person*/ true); // 返回可提供支援的据点
                    pk::building @src_base = pk::get_building(src_target);
                    if (pk::is_alive(src_base) and !list_target_base.contains(dst_base))
                        list_target_base.add(dst_base);
                }
            }

            // 按交战部队差距排序（优先派遣支援到防守方优势区域）
            list_target_base.sort(function(a, b) {
                if (a.get_id() < 城市_末 and b.get_id() < 城市_末)
                    return (main.get_engage_status(a.get_id()) < main.get_engage_status(b.get_id()));

                return (a.get_id() > b.get_id());
            });

            // 依次向需要攻击的据点派遣支援部队
            pk::array<pk::building @> arr_target_base = pk::list_to_array(list_target_base);
            for (int i = 0; i < int(arr_target_base.length); i++)
            {
                list_attack_base.clear();
                pk::building @dst_base = arr_target_base[i];

                bool cmd = PushAttackSupport(dst_base, force);
            }

            // 每回合更新势力城市区域：交战中的部队数量和编队数量
            clear_engage_info();
            update_engage_info();
            list_target_base.clear();
            list_attack_base.clear();
        }
        //---------------------------------------------------------------------------------------

        // 指定发送到攻击目标的部队类型
        bool PushAttackSupport(pk::building @dst_base, pk::force @force)
        {
            // 检查是否有附近的基地可以派遣援军
            int src_target = getNearestReinforce(dst_base, force, /*has_person*/ true);
            if (src_target == -1)
                return false;

            // 攻击部队出发地点
            pk::building @src_base = pk::get_building(src_target);

            int city_id = pk::get_city_id(dst_base.pos); // 据点所在的区域城市
            int status = get_engage_status(city_id);     // 攻-守兵力差：攻击方多则为正，防守方多则为负
            string status_info = "";

            bool cmd = false;
            if ((2 * ref_status) <= status and status < (4 * ref_status))
            { // 攻击方优势
                status_info = "攻击方兵力优势";
                int num_atk_support_unit = count_atk_unit_type(src_base, dst_base, 部队类型_运输, /*siege_weapon*/ false);
                int unit_type = (!据点攻略_允许运输部队 or pk::enemies_around(src_base) or num_atk_support_unit > max_unit_supply) ? 部队类型_战斗 : 部队类型_运输;
                cmd = (PushAttackUnit(dst_base, src_base, unit_type, /*siege_weapon*/ false, /*max_unit*/ 1) or cmd); // 战斗 or 运输部队
            }
            else if (0 <= status and status < (2 * ref_status))
            { // 攻守激烈
                status_info = "攻守激烈";
                int num_atk_support_unit = count_atk_unit_type(src_base, dst_base, 部队类型_战斗, /*siege_weapon*/ true);
                bool siege = (!据点攻略_允许攻城兵器 or pk::enemies_around(src_base) or num_atk_support_unit > max_unit_siege) ? false : true;
                cmd = (PushAttackUnit(dst_base, src_base, 部队类型_战斗, /*siege_weapon*/ siege, /*max_unit*/ 1) or cmd); // 战斗 or 攻城部队
            }
            else if ((-1 * ref_status) <= status and status < 0)
            { // 防守方优势
                status_info = "防守方兵力优势";
                int max_unit = (pk::enemies_around(src_base)) ? 1 : max_unit_combat;
                cmd = (PushAttackUnit(dst_base, src_base, 部队类型_战斗, /*siege_weapon*/ false, /*max_unit*/ max_unit) or cmd); // 战斗部队
            }

            if (调试模式)
            {
                pk::force @def_force = pk::get_force(dst_base.get_force_id());
                pk::force @atk_force = force;
                string dst_name = pk::decode(pk::get_name(dst_base));
                string src_name = pk::decode(pk::get_name(src_base));
                string def_force_name = pk::decode(pk::get_name(pk::get_person(def_force.kunshu)));
                string atk_force_name = pk::decode(pk::get_name(pk::get_person(atk_force.kunshu)));

                if (cmd)
                    pk::info(pk::format("{}军, {}军 {} 攻略中, {} ({}派兵)", atk_force_name, def_force_name, dst_name, status_info, src_name));
            }
            return true;
        }

        int ref_status = 10000; // 攻防优势_基准兵力
        int max_unit_combat = pk::max(1, pk::min(3, 据点攻略_战斗部队数));
        int max_unit_supply = pk::max(1, pk::min(3, 据点攻略_运输部队数));
        int max_unit_siege = pk::max(1, pk::min(5, 据点攻略_攻城部队数));

        bool PushAttackUnit(pk::building @dst_base, pk::building @src_base, int attack_type, bool siege_weapon = false, int max_unit = 1)
        {
            // 进攻出发据点
            int src_id = src_base.get_id();
            int dst_id = dst_base.get_id();

            // 出击命令条件
            int unit_count = 0;
            int push_count = 0; // push次数检查变量

            if (dst_id >= 城市_末) // 关隘/据点只从一个据点派遣1个部队
                max_unit = 1;

            // 在出发据点上循环执行出击命令
            while (unit_count < max_unit and push_count < max_unit)
            {
                push_count += 1;

                if (attack_type == 部队类型_运输)
                {
                    int unit_id = PushTransportUnit(src_base, dst_base, true); // 运输部队出击
                    if (unit_id != -1)
                    {
                        unit_count += 1; // 运输部队计数
                        if (!list_attack_base.contains(src_base))
                            list_attack_base.add(src_base);
                    }
                }
                else if (attack_type == 部队类型_战斗 and !siege_weapon)
                {
                    int unit_id = PushCombatUnit(src_base, dst_base, true); // 战斗部队出击
                    if (unit_id != -1)
                    {
                        unit_count += 1; // 战斗部队计数
                        if (!list_attack_base.contains(src_base))
                            list_attack_base.add(src_base);
                    }
                }
                else if (attack_type == 部队类型_战斗 and siege_weapon)
                {
                    int unit_id = PushSiegeUnit(src_base, dst_base, true); // 攻城部队出击
                    if (unit_id != -1)
                    {
                        unit_count += 1; // 战斗部队计数
                        if (!list_attack_base.contains(src_base))
                            list_attack_base.add(src_base);
                    }
                }
            }
            if (unit_count > 0)
                return true;

            return false;
        }

        // 正在向目标敌方据点行进的部队数量
        int count_atk_unit_type(pk::building @src_base, pk::building @dst_base, int check_type, bool siege_weapon = false)
        {
            int count = 0;
            if (!pk::is_alive(src_base) or !pk::is_alive(dst_base))
                return -1;
            int src_id = src_base.get_id();
            int dst_id = dst_base.get_id();
            if (src_id == dst_id)
                return -1;

            pk::district @district = pk::get_district(src_base.get_district_id());
            pk::array<pk::unit @> arr_unit = pk::list_to_array(pk::get_unit_list(district));
            for (int i = 0; i < int(arr_unit.length); i++)
            {
                pk::unit @unit = arr_unit[i];
                int service_id = pk::get_service(unit);

                bool is_valid_type = false;
                if (check_type == 部队类型_运输)
                    is_valid_type = (unit.type == check_type);
                else if (check_type == 部队类型_战斗)
                {
                    if (siege_weapon and is_siege_weapon(pk::get_ground_weapon_id(unit)))
                        is_valid_type = (unit.type == check_type);
                    else if (!siege_weapon and !is_siege_weapon(pk::get_ground_weapon_id(unit)))
                        is_valid_type = (unit.type == check_type);
                }

                if (is_valid_type and service_id == src_id and unit.target_type == 부대임무대상_거점)
                {
                    pk::building @building_t = pk::get_building(unit.target);
                    if (pk::is_alive(building_t) and building_t.get_id() == dst_id and pk::is_enemy(unit, building_t))
                        count++;
                }
            }
            return count;
        }

        //---------------------------------------------------------------------------------------
        // 各据点军力情况更新
        //---------------------------------------------------------------------------------------

        // 不可修改
        array<int> def_troopsbase(城市_末, 0); // 区域内各据点基础兵力
        array<int> def_troops_sum(城市_末, 0); // 区域内防御军总兵力
        array<int> def_unit_count(城市_末, 0); // 区域内防御军部队数量
        array<int> atk_troops_sum(城市_末, 0); // 区域内进攻军总兵力
        array<int> atk_unit_count(城市_末, 0); // 区域内进攻军部队数量

        // 回合内各势力据点区域：军力信息初始化
        void clear_engage_info()
        {
            for (int city_id = 0; city_id < 城市_末; city_id++)
            {
                def_troopsbase[city_id] = 0;
                def_troops_sum[city_id] = 0;
                def_unit_count[city_id] = 0;
                atk_troops_sum[city_id] = 0;
                atk_unit_count[city_id] = 0;
            }
        }

        // 回合内各势力据点区域：交战军力总和、部队数量更新
        void update_engage_info()
        {
            pk::array<pk::unit @> arr_unit = pk::list_to_array(pk::get_unit_list());
            for (int i = 0; i < int(arr_unit.length); i++)
            {
                pk::unit @unit = arr_unit[i];
                int unit_id = unit.get_id();
                int city_id = pk::get_city_id(unit.pos); // 部队所在坐标区域的据点
                if (city_id >= 0 and city_id <= 城市_末)
                {
                    pk::city @city = pk::get_city(city_id);
                    if (unit.get_force_id() == city.get_force_id()) // 部队和据点属于同一势力
                    {
                        def_troops_sum[city_id] += unit.troops; // 区域内防御军总兵力
                        def_unit_count[city_id] += 1;           // 区域内防御军部队数量
                    }
                    else if (pk::is_enemy(unit, city)) // 部队和据点属于敌对势力
                    {
                        atk_troops_sum[city_id] += unit.troops; // 区域内进攻军总兵力
                        atk_unit_count[city_id] += 1;           // 区域内进攻军部队数量
                    }
                }
            }
            for (int city_id = 0; city_id < 城市_末; city_id++)
            {
                pk::city @city = pk::get_city(city_id);
                def_troopsbase[city_id] = pk::get_troops(city);
            }
        }

        // 袭击的敌军据点兵力水平确认
        bool is_invaded_base(pk::building @base)
        {
            if (!pk::is_alive(base))
                return false;

            int city_id = pk::get_city_id(base.pos);
            if (atk_unit_count[city_id] == 0)
                return false;

            if (atk_unit_count[city_id] <= 2 and get_engage_status(city_id) < 0)
                return false;

            return true;
        }

        // 袭击的势力确认
        int get_force_invaded(pk::building @base)
        {
            // 范围内敌军部队所属势力数量计算
            pk::array<int> arr_force_unit_count(势力_末, 0);
            pk::array<pk::point> range = pk::range(base.pos, 1, 6);
            for (int i = 0; i < int(range.length); i++)
            {
                pk::unit @unit = pk::get_unit(range[i]);
                if (pk::is_alive(unit) and pk::is_enemy(unit, base))
                    arr_force_unit_count[unit.get_force_id()]++;
            }
            // 返回敌军部队最多的所属势力
            int max_force_id = -1;
            int max_force_count = -1;
            for (int j = 0; j < 势力_末; j++)
            {
                int count = arr_force_unit_count[j];
                if (max_force_count < count)
                {
                    max_force_count = count;
                    max_force_id = j;
                }
            }

            if (max_force_count <= 0)
                return -1;
            if (!pk::is_normal_force(max_force_id))
                return -1;

            return max_force_id;
        }

        // 交战军力比较结果
        int get_engage_status(int city_id)
        {
            if (city_id < 0 and city_id > 城市_末)
                return 0;

            int def_base_troops = def_troopsbase[city_id];
            int def_unit_troops = def_troops_sum[city_id];
            int atk_unit_troops = atk_troops_sum[city_id];

            int status = atk_unit_troops - (def_base_troops + def_unit_troops);
            return status; // 进攻方兵力多，返回正数；防御方兵力多，返回负数
        }

        //---------------------------------------------------------------------------------------

        string get_order_info(int order)
        {
            string name;
            switch (order)
            {
            case 0:
                name = "部队任务_待命";
                break;
            case 1:
                name = "部队任务_移动";
                break;
            case 2:
                name = "部队任务_设置";
                break;
            case 3:
                name = "部队任务_攻击";
                break;
            case 4:
                name = "部队任务_撤退";
                break;
            case 5:
                name = "部队任务_拦截";
                break;
            case 6:
                name = "部队任务_护卫";
                break;
            case 7:
                name = "部队任务_攻城";
                break;
            case 8:
                name = "部队任务_修复";
                break;
            case 9:
                name = "部队任务_征服";
                break;
            case 10:
                name = "部队任务_补给";
                break;
            case 11:
                name = "部队任务_歼灭";
                break;
            case 12:
                name = "部队任务_追随";
                break;
            case 13:
                name = "部队任务_末";
                break;
            default:
                name = "部队任务_没有";
                break;
            }

            return name;
        }

        //---------------------------------------------------------------------------------------
        bool 调试模式 = false;
    };

    Main main;
}