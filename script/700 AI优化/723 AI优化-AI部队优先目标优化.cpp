﻿// ## 2023/05/09 # 铃 # 缩减探测距离到2 ##
// ## 2023/03/19 # 铃 # 创建,目的是为了让AI在野战中优先攻击府##

namespace AI部队优先目标优化
{
    const int 部队目标变更_势力条件 = 0;          // 0:电脑AI和player _委任军团，1:player _委任军团，2:电脑AI, 3:均未使用  0:
    const bool 部队目标变更_适用玩家军团 = false; // 与势力条件无关，设定是否适用，忽略玩家的任务指定，建议false
    const int 探测距离 = 2;
    bool 调试模式 = false;

    //=======================================================================================

    class Main
    {

        Main()
        {
            pk::bind(111, pk::trigger111_t(onTurnStart));
            pk::bind(164, pk::trigger164_t(onkillbuilding));
        }
        //---------------------------------------------------------------------------

        pk::list<pk::building @> target_list;

        //---------------------------------------------------------------------------

        void onTurnStart(pk::force @force)
        {
            if (pk::is_campaign())
                return;

            if (!pk::is_normal_force(force))
                return;
            int force_id = pk::get_current_turn_force_id();
            if (!pk::is_normal_force(force_id))
                return;

            // 本势力全体未行动部队
            pk::array<pk::unit @> arr_unit_list = pk::list_to_array(pk::get_unit_list(force));
            if (int(arr_unit_list.length) == 0)
                return;
            for (int i = 0; i < int(arr_unit_list.length); i++)
            {
                pk::unit @unit = arr_unit_list[i];
                if (pk::is_alive(unit) and check_force_option(unit))
                {
                    if (!unit.action_done and unit.get_force_id() == force_id)
                        optimize_target(unit); // 部队目标重新设置函数
                }
            }
        }

        void onkillbuilding(int force_id, int facility_id, const pk::point &in pos,int a)
        {
            pk::force @force = pk::get_force(force_id);
            if (!pk::is_alive(force) or !pk::is_normal_force(force_id))
                return;
            if (force_id == -1)
                return;
            pk::list<pk::unit @> unit_list = pk::get_unit_list(force);
            if (unit_list.count > 0)
            {
                for (int i = 0; i < unit_list.count; i += 1)
                {
                    pk::unit @unit = unit_list[i];
                    ss_unit_info @ss_unit = @special_unit[unit.get_id()];
                    /*
                        封地打下后，2种情况要还原
                        １ 部队的任务也是这个封地的。需测试建筑打下后，原系统设定是否会更新部队任务情况
                        2 部队待机的。打下封地的部队任务完成，观察是会变为待机。需持续观察
                    */
                    if (((unit.order == 部队任务_攻击 and unit.target_pos == pos) or unit.order == 部队任务_待命) and ss_unit.setting)
                    {
                        // 还原设置攻击任务
                        pk::set_order(unit, ss_unit.order, pk::point(ss_unit.pos_x, ss_unit.pos_y));
                    }
                }
            }
        }

        //---------------------------------------------------------------------------

        // 重新设置部队目标的函数
        void optimize_target(pk::unit @unit)
        {

            if (!pk::is_alive(unit))
                return;

            // 运异常状态跳过
            if (unit.status != 部队状态_通常)
                return;

            if (!pk::is_alive(unit))
                return;

            // 运输部队跳过
            if (unit.type != 部队类型_战斗)
                return;

                
            /// 检查异常势力
            int force_id = unit.get_force_id();
            pk::force @force = pk::get_force(force_id);
            if (!pk::is_alive(force) or !pk::is_normal_force(force_id))
                return;

            // 撤退部队无效
            if (unit.order == 部队任务_撤退  || unit.order == 部队任务_补给  || unit.order == 部队任务_修复)
                return;
 
            // 排除无法确认所属据点的部队
            if (!pk::is_alive(pk::get_building(pk::get_service(unit))))
                return; // 排除无法确认所属基地的部队

            // pk::say(pk::encode(pk::format("确认", pk::get_person(unit.leader))));

            // 获取列表
            get_target_list(unit);

            // 如果列表为空就跳过
            if (target_list.count == 0)
                return;

            ss_unit_info @ss_unit = @special_unit[unit.get_id()];
            // pk::trace(pk::format("{}部队的命令{},x1{},x2{},y1{},y2{},", pk::decode(pk::get_name(pk::get_person(unit.leader))),unit.order,unit.target_pos.x,target_list[0].pos.x,unit.target_pos.y,target_list[0].pos.y));

            // 防止重复设置，去除改设定
            if (unit.order == 部队任务_攻击 and unit.target_pos == target_list[0].pos and !ss_unit.setting)
                return;

            // 存储部队原始任务
            ss_unit.set_unit_data(unit);

            // 重新设置攻击任务
            pk::set_order(unit, 部队任务_攻击, target_list[0].pos);

            // 显示部队任务变更对话框
            if (pk::is_in_screen(target_list[0].pos))
                say_change_target_unit(unit, target_list[0]);
        }

        //---------------------------------------------------------------------------
        // 获取列表
        void get_target_list(pk::unit @unit)
        {
            target_list.clear();

            pk::array<pk::point> range = pk::range(unit.pos, 1, 探测距离 + get_max_atk_range(unit));

            for (int i = 0; i < int(range.length); i++)
            {
                if (!pk::is_valid_pos(range[i]))
                    continue;

                if (!pk::is_alive(pk::get_building(range[i])))
                    continue;

                pk::building @target_building = pk::get_building(range[i]);

                int building_id = target_building.get_id();

                // 如果不是府就跳过
                if (ch::to_spec_id(building_id) == -1)
                    continue;

                // 如果是空建筑就跳过
                if (pk::get_building(range[i]).get_force_id() == -1)
                    continue;

                // 不是敌对跳过
                if (!pk::is_enemy(unit, target_building))
                    continue;

                // 列表已经有就跳过
                if (target_list.contains(target_building))
                    continue;

                // 不在移动范围就跳过
                // if (!check_base_in_movable_range(unit, target_building))
                //  continue;

                target_list.add(target_building);
            }
        }

        //---------------------------------------------------------------------------

        void say_change_target_unit(pk::unit @unit, pk::building @building)
        {
            pk::person @leader = pk::get_person(unit.leader);

            string target_name = ch::get_spec_name(ch::to_spec_id(building.get_id()));

            pk::say(pk::encode(pk::format("向\x1b[2x{}\x1b[0x发起进攻吧", target_name)), leader);
        }

        // 检查目标据点是否在部队的移动范围内
        bool check_base_in_movable_range(pk::unit @unit, pk::building @building)
        {
            if (!pk::is_alive(unit) or !pk::is_alive(building))
                return false;

            pk::array<pk::point> arr_range = pk::get_movable_pos(unit);

            for (int i = 0; i < int(arr_range.length); i++)
            {
                if (building.pos == arr_range[i])
                    return true;
            }
            return false;
        }

        // 检查势力选项函数
        bool check_force_option(pk::unit @unit)
        {
            // 不适用玩家军团（因为会忽略用户的任务指定并重新设定目标，所以建议不适用）
            if (!pk::is_alive(unit))
                return false;
            if (pk::is_player_controlled(unit))
            {
                if (部队目标变更_适用玩家军团)
                    return true;
                else
                    return false;
            }

            // 0：电脑AI和玩家_授权军团均不适用，1：只有玩家_授权军团，2：只有电脑AI，3：全部不适用
            if (部队目标变更_势力条件 == 0 and !pk::is_player_controlled(unit))
                return true;
            else if (部队目标变更_势力条件 == 1 and unit.is_player() and !pk::is_player_controlled(unit))
                return true;
            else if (部队目标变更_势力条件 == 2 and !unit.is_player())
                return true;
            else if (部队目标变更_势力条件 == 3)
                return false;

            return false;
        }

        // 部队攻击最大射程函数
        int get_max_atk_range(pk::unit @unit)
        {
            if (!pk::is_alive(unit))
                return 1;

            int weapon_id = unit.weapon;
            int max_range = 0;

            if (weapon_id == 兵器_弩)
                max_range = (unit.has_tech(技巧_强弩)) ? 3 : 2;
            else if (weapon_id == 兵器_战马)
                max_range = (unit.has_tech(技巧_骑射) or unit.has_skill(特技_白马)) ? 2 : 1;
            else if (weapon_id == 兵器_井阑)
                max_range = ((unit.has_skill(特技_射程)) ? 1 : 0) + (pk::get_tactics(战法_攻城火矢).max_range);
            else if (weapon_id == 兵器_投石)
                max_range = ((unit.has_skill(特技_射程)) ? 1 : 0) + (pk::get_tactics(战法_攻城投石).max_range);
            else if (weapon_id >= 兵器_走舸)
                max_range = 2;
            else
                max_range = 1;

            return max_range;
        }
    }

    Main main;
}
