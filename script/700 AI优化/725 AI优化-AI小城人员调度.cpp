﻿
// ## 2023/03/11 # 铃 # 完成,AI可以把把武将送到没有武将但是有资源的小城,并把资源送到大城.
// ## 2022/11/11 # 铃 # 初稿,初衷是当小城市有钱有粮草,但是没有武将的时候,调度过来运输

namespace AI小城市调度
{
	const int 召唤人数 = 1;
	const int 召唤城市距离 = 2;

	/// =========================================================================================

	const bool 调试模式 = false;

	const int KEY = pk::hash("AI小城市调度");

	class Main
	{

		Main()
		{
			pk::bind(202, pk::trigger202_t(onAIRunningOrder));
		}

		void onAIRunningOrder(pk::ai_context @context, pk::building @building, int cmd)
		{
			if (cmd == 据点AI_小城调度)
			{
				pk::force @force = pk::get_force(building.get_force_id());
				pk::ai_context_base @base = context.base[building.get_id()];
				int src_id = -1;

			

				// 如果是大城市则不执行
				if (building.get_id() < 城市_末)
					return;

				// 如果是战斗中则不执行
				if (base.status == 据点状态_战斗)
					return;

				// 如果是玩家城市,就退出
				if (building.is_player())
					return;

				// 如果是小城市没有资源,就退出
				if (pk::get_gold(building) * 5 + pk::get_food(building) < 100000)
					return;

				// 如果港口本就有人,就退出
				auto des_person_list = pk::get_person_list(building, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般));



				if (des_person_list.count > 0)
					return;



				// 获取距离1以内的,人数最多的城市.
				src_id = getClosestCity(building);

				// 如果获取不到则退出.
				if (src_id == -1)
					return;


				// 获取源城市武将列表,并且按照能力最低来排序

				auto person_list = pk::get_idle_person_list(pk::get_building(src_id));

				person_list.sort(function(a, b) {
					return (a.stat[武将能力_武力] + a.stat[武将能力_统率]) < (b.stat[武将能力_武力] + b.stat[武将能力_统率]);
				});

				// 召唤对象武将

				pk::list<pk::person @> actors;
				for (int i = 0; i <= 召唤人数 - 1; i++)
				{
					actors.add(person_list[i]);

				if (调试模式)
				{
					string person_name = pk::format("\x1b[1x{}\x1b[0x", pk::decode(pk::get_name(person_list[i])));
					string building_name = pk::format("\x1b[1x{}\x1b[0x", pk::decode(pk::get_name(building)));
					string dst_name = pk::format("\x1b[1x{}\x1b[0x", pk::decode(pk::get_name(pk::get_building(src_id))));
					string action_message = pk::format("{}从{}召唤了{}", building_name,dst_name,person_name);
					pk::message_box(pk::encode(action_message));
				}

				}

				// 召唤命令
				pk::summon_cmd_info cmd_summon;
				@cmd_summon.base = building;
				cmd_summon.actors = actors;
				pk::command(cmd_summon);



			}
			return;
		}

		int getClosestCity(pk::building @dst)
		{
			int src_id = -1;
			int best_src = -1;
			int src_headcount = 0;

			for (int i = 0; i < 87; i++)
			{
				pk::building @src = pk::get_building(i);

				// 如果选到自己就跳过
				if (dst.get_id() == i)
					continue;

				// 如果距离太远就跳过
				if (pk::get_building_distance(dst.get_id(), i, dst.get_force_id()) > 召唤城市距离)
					continue;

				// 如果不同势力就跳过
				if (dst.get_force_id() != src.get_force_id())
					continue;

				// 如果除了君主之外的人数小于3个就跳过
				auto src_person_list = pk::get_person_list(src, pk::mibun_flags(身份_都督, 身份_太守, 身份_一般));
				auto src_idle_person_list = pk::get_idle_person_list(src);

				//如果召唤人数不够,则跳过
				if (src_idle_person_list.count < 召唤人数 )
					continue;

				//如果城市本来人就不多,就跳过
				if (src_person_list.count < 召唤人数 * 3)
					continue;

				if (src_person_list.count > src_headcount and src_idle_person_list.count > 1 and src_person_list.count > 3)
				{
					src_headcount = src_person_list.count;
					best_src = i;
				}
			}
			return best_src;
		}

	} Main main;
}