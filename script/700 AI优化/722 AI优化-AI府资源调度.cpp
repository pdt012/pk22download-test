// ## 2023/5/18 #铃# 修复AI会操控玩家府的Bug ##
// ## 2023/5/09 #铃# AI府濒临陷落时会携带几乎所有的资源出逃,并且播放运输动画 ##
// ## 2023/3/19 #铃# 結合新的內政系統设计了AI府资源调度 ##

namespace AI府资源调度_AI
{

	// ================ CUSTOMIZE ================

	// ===========================================

	class Main
	{

		Main()
		{
			pk::bind(109, pk::trigger109_t(onNew_Season));
			pk::bind(173, pk::trigger173_t(unit_action_done));
		}

		void onNew_Season()
		{
			for (int i = 0; i < ch::get_spec_end(); ++i)
			{
				pk::point pos = ch::get_spec_pos(i);
				pk::building @building = pk::get_building(pos);

				// pk::trace(pk::format("i :{},x :{},y :{},building.get_id :{}", i, pos.x, pos.y, building.get_id()));

				if (!pk::is_alive(building))
					continue;

				//玩家控制的军团除外
				if (pk::is_player_controlled(building))
					continue;

				pk::force @force = pk::get_force(building.get_force_id());

				if (!pk::is_alive(force))
					continue;

				int spec_id = ch::to_spec_id(building.get_id());

				// pk::trace(pk::format("spec_id :{}",spec_id));

				if (special_ex[i].person == -1)
					continue;

				if (building.get_force_id() != force.get_id())
					continue;

				int city_id = pk::get_building_id(building.pos);
				if (pk::get_building(city_id).get_force_id() != force.get_id())
					continue;

				if (city_id >= 城市_末)
					continue;

				BaseInfo @base_t = @base_ex[city_id];
				BuildingInfo @building_p = @building_ex[city_id];
				pk::building @city = pk::get_building(city_id);
				int person_id = special_ex[i].person;

				specialinfo @spec_t = @special_ex[i];
				spec_id = ch::to_spec_id(building.get_id());

				if (!ch::is_valid_spec_id(spec_id))
					continue;

				string src_name = ch::get_spec_name(spec_id);

				// pk::trace(pk::format("城市id：{},城市名:{},府:{},spec_t.troops:{}", city_id, pk::decode(pk::get_name(pk::get_building(city_id))), src_name,spec_t.troops));

				// 府兵资源转移

				if (((spec_t.troops > 12000 and spec_t.troops > int(pk::get_troops(city) * 2)) or spec_t.troops > 20000) and pk::get_troops(city) < pk::get_max_troops(city) - 10000 and !pk::enemies_around(city))
				{
					// pk::trace(pk::format("城市id：{},城市名:{},spec_t.gold:{},城市收支:{},魅力:{}", city_id ,pk::decode(pk::get_name(pk::get_building(city_id))),spec_t.gold,building_p.building_revenue,sqrt(pk::get_person(spec_t.person).stat[武将能力_魅力] * 0.1)));

					int reduce = int(spec_t.troops * 0.3) + pk::rand(200);

					spec_t.troops -= reduce;

					if (pk::is_in_screen(pos))
						府兵运输队(building, city, person_id, reduce, 0, 0);

					ch::add_troops(city, reduce, true);

					pk::combat_text(-reduce, 1, pos);

					// string t = pk::format("\x1b[2x{}向{}\x1b[0x转移了士兵\x1b[1x{}人\x1b[0x,现有府兵{}人", src_name, pk::decode(pk::get_name(city)), 5000, spec_t.troops); //"\x1b[2x" +  + pk::encode("\x1b[0x获得了封地\x1b[1x" + spec_name + "\x1b[0x");
					string t = pk::format("\x1b[2x{}向{}\x1b[0x转移了士兵\x1b[1x{}人\x1b[0x", src_name, pk::decode(pk::get_name(city)), reduce); //"\x1b[2x" +  + pk::encode("\x1b[0x获得了封地\x1b[1x" + spec_name + "\x1b[0x");

					pk::history_log(building.pos, pk::get_force(building.get_force_id()).color, pk::encode(t));
					// pk::trace(pk::format("常规转移城市id：{},城市名:{},府名:{},兵力:{},reduce:{}", city_id ,pk::decode(pk::get_name(pk::get_building(city_id))),src_name,pk::get_troops(city),reduce));
				}

				// 府兵多余资金转移
				if ((spec_t.gold > 8000 and (spec_t.gold > int(pk::get_gold(city) * 3) or spec_t.gold > 30000)) and pk::get_gold(building) < pk::get_max_gold(city) - 10000 and !pk::enemies_around(city) and spec_t.troops > 5000)
				{
					// pk::trace(pk::format("城市id：{},城市名:{},spec_t.gold:{},城市收支:{},魅力:{}", city_id ,pk::decode(pk::get_name(pk::get_building(city_id))),spec_t.gold,building_p.building_revenue,sqrt(pk::get_person(spec_t.person).stat[武将能力_魅力] * 0.1)));

					int reduce = int(spec_t.gold * 0.8) + pk::rand(800);
					spec_t.gold -= reduce;

					if (pk::is_in_screen(pos))
						府兵运输队(building, city, person_id, 1000, reduce, 0);

					pk::add_gold(building, reduce, true);
					pk::combat_text(-reduce, 3, pos);

					string t = pk::format("\x1b[2x{}向{}\x1b[0x运输了资金\x1b[1x{}\x1b[0x", src_name, pk::decode(pk::get_name(city)), reduce); //"\x1b[2x" +  + pk::encode("\x1b[0x获得了封地\x1b[1x" + spec_name + "\x1b[0x");
					pk::history_log(building.pos, pk::get_force(building.get_force_id()).color, pk::encode(t));
				}

				// 府兵多余兵粮转移
				if ((spec_t.food > 80000 and (spec_t.food > int(pk::get_food(city) * 3) or spec_t.food > 80000)) and pk::get_food(building) < pk::get_max_food(city) - 30000 and !pk::enemies_around(city) and spec_t.troops > 5000)
				{
					// pk::trace(pk::format("城市id：{},城市名:{},spec_t.food:{},城市收支:{},魅力:{}", city_id ,pk::decode(pk::get_name(pk::get_building(city_id))),spec_t.food,building_p.building_revenue,sqrt(pk::get_person(spec_t.person).stat[武将能力_魅力] * 0.1)));
					int reduce = int(spec_t.food * 0.8) + pk::rand(3000);

					if (pk::is_in_screen(pos))
						府兵运输队(building, city, person_id, 1000, 0, reduce);
					spec_t.food -= reduce;
					pk::add_food(building, reduce, true);

					pk::combat_text(-reduce, 4, pos);

					string t = pk::format("\x1b[2x{}向{}\x1b[0x运输了兵粮\x1b[1x{}\x1b[0x", src_name, pk::decode(pk::get_name(city)), reduce); //"\x1b[2x" +  + pk::encode("\x1b[0x获得了封地\x1b[1x" + spec_name + "\x1b[0x");
					pk::history_log(building.pos, pk::get_force(building.get_force_id()).color, pk::encode(t));
				}

				int 损耗 = 0;
				损耗 = 府一格内敌方部队(building) * 10 + int(城市3格内敌军兵力(city) / 1000) + pk::rand(10);
				损耗 = pk::clamp(损耗, 0, 99);

				bool 完全包围 = (府一格内敌方部队(building) == 府一格内有效地形(building));

				// 城市紧急状态下府兵资源转移,如果府被包围则无法完成.
				// 兵力差距足够大才会调度,防止玩家恶意利用.
				// 每次调度数百到1000多人左右,缓慢调度以免让玩家觉得困惑.
				// 调度量和兵力差距直接相关.
				// 损耗和府被围困程度.城市被围困程度有关.
				if (spec_t.troops > 5000 and pk::enemies_around(city) and 计算己方兵力(city) < int(城市3格内敌军兵力(city) * 0.8))
				{
					if (完全包围)
					{
						string t = pk::format("\x1b[2x{}\x1b[0x危在旦夕,\x1b[2x{}\x1b[0x的部队救援\x1b[2x{},\x1b[0x但由于\x1b[2x{}\x1b[0x自身被完全包围而无法救援", pk::decode(pk::get_name(city)), src_name, pk::decode(pk::get_name(city)), src_name);
						pk::history_log(building.pos, pk::get_force(building.get_force_id()).color, pk::encode(t));
						return;
					}

					// pk::trace(pk::format("城市id：{},城市名:{},spec_t.gold:{},城市收支:{},魅力:{}", city_id ,pk::decode(pk::get_name(pk::get_building(city_id))),spec_t.gold,building_p.building_revenue,sqrt(pk::get_person(spec_t.person).stat[武将能力_魅力] * 0.1)));
					int reduce = pk::rand(300) + pk::clamp(int((城市3格内敌军兵力(city) - 计算己方兵力(city)) * 0.1), 0, 1500);
					pk::clamp(reduce, 0, spec_t.troops);
					spec_t.troops -= reduce;

					if (pk::is_in_screen(pos))
						府兵运输队(building, city, person_id, reduce, 0, 0);

					ch::add_troops(city, int(reduce * (1 - 损耗 / 100.0)), true);

					pk::combat_text(-reduce, 1, pos);

					string t = pk::format("\x1b[2x{}\x1b[0x危在旦夕,\x1b[2x{}\x1b[0x向\x1b[2x{}\x1b[0x支援,但受到攻城部队的攻击,最后仅有\x1b[1x{}\x1b[0x人到达\x1b[2x{}", pk::decode(pk::get_name(city)), src_name, pk::decode(pk::get_name(city)), int(reduce * (1 - 损耗 / 100.0)), pk::decode(pk::get_name(city))); //"\x1b[2x" +  + pk::encode("\x1b[0x获得了封地\x1b[1x" + spec_name + "\x1b[0x");
					pk::history_log(building.pos, pk::get_force(building.get_force_id()).color, pk::encode(t));
					// pk::trace(pk::format("紧急转移城市id：{},城市名:{},敌方兵力:{},己方兵力:{},reduce:{}", city_id, pk::decode(pk::get_name(pk::get_building(city_id))), 城市3格内敌军兵力(city), 计算己方兵力(city), reduce));
				}
			}
		}

		void unit_action_done(pk::unit @unit, int type)
		{
			if (unit.type == 部队类型_运输)
				return;

			// 节约计算
			if (unit.target_type != 部队任务对象_据点)
				return;

			for (int i = 0; i < ch::get_spec_end(); ++i)
			{
				pk::point pos = ch::get_spec_pos(i);
				pk::building @building = pk::get_building(pos);

				if (!pk::is_alive(building))
					continue;

				specialinfo @spec_t = @special_ex[i];

				// pk::trace(pk::format("府的坐标x：{},府的坐标y：:{}", pos.x, pos.y));

				if (building.get_id() == -1)
					continue;

				if (ch::to_spec_id(building.get_id()) == -1)
					continue;

				if (spec_t.person != -1)
					continue;

				int spec_id = ch::to_spec_id(building.get_id());

				string src_name = ch::get_spec_name(spec_id);

				pk::force @force = pk::get_force(building.get_force_id());
				if (!pk::is_alive(force))
					continue;

				if (!pk::is_alive(building))
					continue;

				int city_id = pk::get_building_id(building.pos);
				if (pk::get_building(city_id).get_force_id() != force.get_id())
					continue;

				BaseInfo @base_t = @base_ex[city_id];
				BuildingInfo @building_p = @building_ex[city_id];
				pk::building @city = pk::get_building(city_id);

				int 损耗 = 0;
				损耗 = 府一格内敌方部队(building) * 10 + int(城市3格内敌军兵力(city) / 1000) + pk::rand(10);
				损耗 = pk::clamp(损耗, 0, 99);

				bool 完全包围 = (府一格内敌方部队(building) == 府一格内有效地形(building));

				// 府濒临破坏的时候向城市转移资源,如果府被包围则无法完成.
				// 损耗和府被围困程度.城市被围困程度有关.

				if (spec_t.troops > 2000 and building.hp - int(0.2 * (pk::get_max_hp(building))) < 0)
				{
					if (完全包围)
					{
						string t = pk::format("\x1b[2x{}的部队试图向{}\x1b[0x撤退,但由于被完全包围而无法突围", src_name, pk::decode(pk::get_name(city)));
						pk::history_log(building.pos, pk::get_force(building.get_force_id()).color, pk::encode(t));
						return;
					}
					// pk::trace(pk::format("城市id：{},城市名:{},spec_t.gold:{},城市收支:{},魅力:{}", city_id ,pk::decode(pk::get_name(pk::get_building(city_id))),spec_t.gold,building_p.building_revenue,sqrt(pk::get_person(spec_t.person).stat[武将能力_魅力] * 0.1)));
					int reduce_troops = spec_t.troops;
					pk::clamp(reduce_troops, 0, spec_t.troops);
					spec_t.troops -= reduce_troops;
					ch::add_troops(city, reduce_troops, true);

					int reduce_gold = spec_t.gold - 100;
					pk::clamp(reduce_gold, 0, spec_t.gold);
					spec_t.gold -= reduce_gold;
					pk::add_gold(city, reduce_gold, true);

					int reduce_food = spec_t.food - 1000;
					pk::clamp(reduce_food, 0, spec_t.food);
					spec_t.food -= reduce_food;
					pk::add_food(city, reduce_food, true);

					int person_id = spec_t.person;
					if (pk::is_in_screen(pos))
						府兵运输队(building, city, person_id, reduce_troops, reduce_gold, reduce_food);

					string t = pk::format("\x1b[2x{}即将失陷,剩余部队紧急向{}\x1b[2x\x1b[0x撤退,但最终仅有\x1b[1x{}人\x1b[0x到达{}", src_name, pk::decode(pk::get_name(city)), int(reduce_troops * (1 - 损耗 / 100)), pk::decode(pk::get_name(city))); //"\x1b[2x" +  + pk::encode("\x1b[0x获得了封地\x1b[1x" + spec_name + "\x1b[0x");
					pk::history_log(building.pos, pk::get_force(building.get_force_id()).color, pk::encode(t));
					// pk::trace(pk::format("紧急转移城市id：{},城市名:{},敌方兵力:{},己方兵力:{},reduce:{}", city_id, pk::decode(pk::get_name(pk::get_building(city_id))), 城市3格内敌军兵力(city), 计算己方兵力(city), reduce));
				}
			}
		}

		int 城市3格内敌军兵力(pk::building @building)
		{

			int enemy_troops = 0;
			int enemy_units = 0;

			auto range = pk::range(building.get_pos(), 1, 5);
			for (int i = 0; i < int(range.length); i++)
			{
				auto unit = pk::get_unit(range[i]);
				if (pk::is_alive(unit) and pk::is_enemy(building, unit))
				{

					enemy_troops += unit.troops;
					// enemy_units++;
					// pk::trace(pk::format("range.length：{},unit.troops:{},enemy_troops:{},enemy_unit:{}", range.length, unit.troops, enemy_troops,enemy_units));
				}
			}
			return enemy_troops;
		}

		int 计算己方兵力(pk::building @building)
		{

			int self_troops = 0;

			auto range = pk::range(building.get_pos(), 1, 2);
			for (int i = 0; i < int(range.length); i++)
			{
				auto unit = pk::get_unit(range[i]);
				if (pk::is_alive(unit) and (building).get_force_id() == unit.get_force_id())
				{

					self_troops += unit.troops;
				}
			}

			self_troops += pk::get_troops(building);
			self_troops += int(building.hp * 0.5);

			return self_troops;
		}
		int 府一格内敌方部队(pk::building @building)
		{

			int enemy_units = 0;

			auto range = pk::range(building.get_pos(), 1, 1);
			for (int i = 0; i < int(range.length); i++)
			{
				auto unit = pk::get_unit(range[i]);
				if (pk::is_alive(unit) and pk::is_enemy(building, unit))
				{

					enemy_units++;
					// pk::trace(pk::format("range.length：{},unit.troops:{},enemy_troops:{},enemy_unit:{}", range.length, unit.troops, enemy_troops,enemy_units));
				}
			}
			return enemy_units;
		}

		int 府一格内有效地形(pk::building @building)
		{
			int valid_pos_count = 0;

			auto range = pk::range(building.get_pos(), 1, 1);
			for (int i = 0; i < int(range.length); i++)
			{
				pk::hex @hex = pk::get_hex(range[i]);
				int terrain_id = hex.terrain;
				if (pk::is_enabled_terrain(terrain_id))
					valid_pos_count++;
				// pk::trace(pk::format("range.length：{},unit.troops:{},enemy_troops:{},enemy_unit:{}", range.length, unit.troops, enemy_troops,enemy_units));
			}

			return valid_pos_count;
		}

		void 府兵运输队(pk::building @building, pk::building @city, int person_id, int troops, int gold, int food)
		{

			if (!func_person_slot_available())
				return; // 沒有空余武将位则不生成

			pk::point src_pos = get_empty_pos(building.pos, 1, 2);
			pk::point dst_pos = get_empty_pos(city.pos, 1, 2);
			//	break;
			pk::person @virtual_person = func_create_person(pk::get_person(person_id));
			if (!pk::is_alive(virtual_person))
			{
				pk::reset(virtual_person);
				return;
			}
			if (!pk::is_valid_pos(src_pos))
				src_pos = get_empty_pos(building.pos, 1, 2); // 生成部队前再次核实坐标有效
															 // pk::trace(pk::format("x1 :{},y1:{},x2 :{},y2 :{}", src_pos.x, src_pos.y, dst_pos.x, dst_pos.y));

			if (src_pos.x == -1)
				return;
			if (dst_pos.x == -1)
				return;

			pk::point next_pos = get_empty_pos(city.pos, 1, 2);
			int distance = pk::get_distance(src_pos, next_pos);
			if (distance > 10)
				return;

			pk::com_march_cmd_info cmd_draft;
			@cmd_draft.base = building;
			cmd_draft.type = 部队类型_运输;
			cmd_draft.member[0] = virtual_person.get_id();
			cmd_draft.gold = gold;
			cmd_draft.troops = troops;
			cmd_draft.food = food;
			cmd_draft.order = 部队任务_移动;
			cmd_draft.target_pos = dst_pos;
			cmd_draft.weapon_id[0] = 1;
			cmd_draft.weapon_amount[0] = 5000;
			pk::unit @unit = pk::create_unit2(cmd_draft, src_pos, false);
			unit.energy = 100;

			if (next_pos != unit.pos)
			{
				auto paths = pk::get_path(unit, unit.pos, next_pos);
				if (paths.length != 0) pk::move(unit, paths);
			}
			// pk::kill(unit, true);
		}

		pk::point get_empty_pos(pk::point pos, int distance_min, int distance_max)
		{
			array<pk::point> range_pos_arr = pk::range(pos, distance_min, distance_max);
			for (int arr_index = 0; arr_index < int(range_pos_arr.length); arr_index++)
			{
				pk::point range_pos = range_pos_arr[arr_index];
				if (!pk::is_valid_pos(range_pos))
					continue;

				pk::hex @hex = pk::get_hex(range_pos);
				if (hex.has_building)
					continue;
				if (hex.has_unit)
					continue;

				int terrain_id = hex.terrain;
				if (!pk::is_valid_terrain_id(terrain_id))
					continue;
				if (!pk::is_enabled_terrain(terrain_id))
					continue;

				return range_pos;
			}

			return pk::point(-1, -1);
		}

		void kill(pk::unit @unit, int spec_id)
		{
			specialinfo @spec_t = @special_ex[spec_id];
			spec_t.troops += unit.troops;
			spec_t.gold += unit.gold;
			spec_t.food += unit.food;
			pk::kill(unit, true);
		}

		// 遍历所有武将，身??死亡或无的都可用来生成
		bool func_person_slot_available()
		{
			for (int person_id = 敌将_开始; person_id < 敌将_末; person_id++)
			{
				pk::person @person = pk::get_person(person_id);
				if (person.mibun == 身份_无)
					return true;
				if (person.mibun == 身份_死亡)
					return true;
			}
			return false;
		}

		// 统领武将生成
		pk::person @func_create_person(pk::person @person)
		{
			pk::person @virtual_person = pk::create_bandit(pk::get_person(武将_卫士));
			virtual_person.name_read = pk::encode("府兵");
			// if (virtual_person.mibun == 身份_在野) pk::set_mibun(virtual_person, 身份_一般);
			pk::set_district(virtual_person, person.get_district_id());

			virtual_person.base_stat[武将能力_统率] = pk::max(50, pk::min(80, int(person.base_stat[武将能力_统率] * 0.5f + ch::randint(5, 10))));
			virtual_person.base_stat[武将能力_武力] = pk::max(50, pk::min(80, int(person.base_stat[武将能力_武力] * 0.5f + ch::randint(5, 10))));
			virtual_person.base_stat[武将能力_智力] = pk::max(50, pk::min(80, int(person.base_stat[武将能力_智力] * 0.6f + ch::randint(5, 10))));
			for (int heishu_id = 兵种_枪兵; heishu_id < 兵种_末; heishu_id++)
				virtual_person.tekisei[heishu_id] = person.tekisei[heishu_id] + ch::randint(-2, 1);
			virtual_person.rank = pk::min(81, person.rank + 8);
			virtual_person.skill = 特技_突袭;
			virtual_person.update();

			return virtual_person;
		}

	} // class
	Main main;
}