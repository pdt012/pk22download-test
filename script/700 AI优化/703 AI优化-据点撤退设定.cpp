﻿// ## 2023/05/20 # 铃 # 翻译注释,增加注释,整理代码 ##
// ## 2022/02/14 # 江东新风 # 部分常量中文化 ##
// ## 2020/10/29 # 江东新风 # 同步马术书大神的更新##
// ## 2020/07/26 ##
/*
pk2.1 기본제공되는 거점철수 기능에서 수정

@수정자: 기마책사
@Update: '19.3.5     / 변경내용 : 거점철수 세팅값 조정, 기존 거점원군 기능은 별도 스크립트로 분리, 원거리이동/임시도주설정 기능 추가
@Update: '19.3.9     / 변경내용 : 거점철수 조건 세팅값 조정, 거점검색모드 추가
@Update: '19.4.5     / 변경내용 : 마지막 도시인 경우 철수불가

*/

namespace 撤离据点
{

	//---------------------------------------------------------------------------------------
	// 用户设置

	const bool 是否增加一次部队移动 = true;		// 增加一次部队移动（推荐功能：用于逃脱）
	const bool 是否出击瞬间应用逃跑效果 = true; // 出击瞬间应用逃跑效果 (推荐功能：可突破包围)

	const float 撤离时黄金食物回收率 = 0.50f; // 撤离时黄金/食物回收率 (0.0f ~ 1.0f 之间)
	const float 撤离时武器回收率 = 0.95f;	  // 撤离时武器回收率   (0.0f ~ 1.0f 之间)

	const int 撤退时搜索的目标类型 = 1; // 0：仅搜索城市以进行撤退，1：搜索城市、关口和港口以进行撤退

	//---------------------------------------------------------------------------------------

	class Main
	{
		pk::func261_t @prev_callback_;

		Main()
		{
			@prev_callback_ = cast<pk::func261_t @>(pk::get_func(261));
			pk::reset_func(261);
			pk::set_func(261, pk::func261_t(callback));
		}

		int retreat_skill = -1; // 禁止修改

		bool callback(pk::force @force)
		{
			int force_id = force.get_id();

			if (!pk::is_campaign())
			{
				// 排除玩家和其他势力。
				if (!force.is_player() and pk::is_normal_force(force_id))
				{
					for (int i = 0; i < 据点_末; i++)
					{
						auto base = pk::get_building(i);

						if (base.get_force_id() == force_id and needRetreat(base))
							PushRetreat(base);
					}
				}
			}
			// 如果存在先前的处理程序，则链接。
			if (prev_callback_ !is null)
				return prev_callback_(force);
			return false;
		}

		//----------------------------------------------------------------------------------
		//           撤离据点
		//----------------------------------------------------------------------------------

		// 判断是否需要撤
		bool needRetreat(pk::building @base)
		{
			// 1格内敌部队数
			int enemy_units1 = 0;
			// 3格内敌部队数
			int enemy_units3 = 0;
			// 3格内敌兵力数
			int enemy_troops3 = 0;

			auto range = pk::range(base.get_pos(), 1, 4 + (base.facility == 시설_도시 ? 1 : 0));
			for (int i = 0; i < int(range.length); i++)
			{
				auto unit = pk::get_unit(range[i]);
				if (pk::is_alive(unit))
				{
					int distance = pk::get_distance(base.get_pos(), range[i]);
					if (pk::is_enemy(base, unit))
					{
						if (distance <= 1)
						{
							enemy_units1++;
						}
						if (distance <= 3)
						{
							enemy_units3++;
							enemy_troops3 += unit.troops;
						}
					}
					else
					{
					}
				}
			}

			if (enemy_units1 >= 10) // 无法在据点被完全包围下撤离，不能撤退
				return false;
			int base_troops = pk::get_troops(base);
			int base_hp = base.hp;
			// 当剩余耐久度非常危险时
			if (enemy_units3 > 0 and base_troops < enemy_troops3 * 0.8f and base_hp < pk::max(500, int(pk::get_max_hp(base) * 0.3f)))
				return true;
			// 当剩余部队数量非常危险时
			if (enemy_units3 > 0 and base_troops < pk::max(1, int(enemy_troops3 * 0.1f)))
				return true;

			return false;
		}

		/** 发送撤退指令。*/
		bool PushRetreat(pk::building @base)
		{
			// 检查是否有可用的武将。
			auto person_list = pk::get_idle_person_list(base);
			if (person_list.count == 0)
				return false;

			// 检查是否有相邻的撤退目的地。
			int target = getNearestBase(base);
			if (target == -1)
				return false;

			// 根据政治值/武力值比例排序。
			person_list.sort(function(a, b) {
				return (a.stat[武将能力_政治] / a.stat[武将能力_武力]) > (b.stat[武将能力_政治] / b.stat[武将能力_武力]);
			});
			pk::person @leader = pk::get_person(person_list[0].get_id());

			// 暂时加上逃跑效果
			if (是否出击瞬间应用逃跑效果)
			{
				retreat_skill = leader.skill; // 备份技能
				leader.skill = 特技_遁走;	  // 加上逃跑技能
			}

			float supply_rate = pk::min(1.0f, pk::max(0.0f, 撤离时黄金食物回收率));
			float weapon_rate = pk::min(1.0f, pk::max(0.0f, 撤离时武器回收率));

			// 创建出征指令信息。
			pk::com_deploy_cmd_info cmd;
			@cmd.base = @base;
			cmd.type = 部队类型_运输;
			cmd.member[0] = leader.get_id();
			cmd.gold = pk::min(int(pk::get_gold(base) * supply_rate), 100000);
			cmd.food = pk::min(int(pk::get_food(base) * supply_rate), 500000);
			cmd.troops = pk::min(int(pk::max(1.f, pk::get_troops(base) * 0.60f)), 60000);
			int i = 0;
			for (int weapon_id = 0; weapon_id < 兵器_末; weapon_id++)
			{
				int weapon_amount = 0;
				if (weapon_id < 兵器_冲车)
					weapon_amount = pk::min(int(pk::get_weapon_amount(base, weapon_id) * weapon_rate), 100000);
				else
					weapon_amount = pk::min(int(pk::get_weapon_amount(base, weapon_id)), 100);

				if (weapon_amount > 0)
				{
					cmd.weapon_id[i] = weapon_id;
					cmd.weapon_amount[i] = weapon_amount;
					i++;
				}
			}
			cmd.order = 部队任务_移动;
			cmd.target_pos = pk::get_building(target).get_pos();

			// 出征。
			int unit_id = pk::command(cmd);
			pk::unit @unit_cmd = pk::get_unit(unit_id);
			if (pk::is_alive(unit_cmd))
				unit_cmd.action_done = (!是否增加一次部队移动); // 将默认值从 true 更改为 false，以便进行两次移动（‘19.3.5）

			pk::say(pk::encode("快突破敌阵，撤退"), leader);

			// 暂时加上逃跑效果
			if (是否出击瞬间应用逃跑效果)
			{
				leader.skill = retreat_skill; // 恢复技能
			}

			return true;
		}

		//获取最近我方据点
		pk::building @src_t;
		int getNearestBase(pk::building @src)
		{
			int best_dst = -1;
			int best_distance = 0;
			int src_id = src.get_id();
			pk::list<pk::building @> dst_list;
			@src_t = @src;

			int search_base = 城市_末;
			if (撤退时搜索的目标类型 == 0)
				search_base = 城市_末;
			else if (撤退时搜索的目标类型 == 1)
				search_base = 据点_末;

			pk::force @force = pk::get_force(src.get_force_id());

			// 撤离目标据点搜索
			if (pk::get_city_list(force).count <= 1 and src_id < 城市_末)
				return -1; // 如果这是我方最后一个城市，则无法撤退
			else
			{
				for (int i = 0; i < search_base; i++)
				{
					pk::building @dst = pk::get_building(i);
					int dst_id = dst.get_id();

					if (src_id != dst_id and src.get_force_id() == dst.get_force_id())
					{
						best_dst = dst_id;
						dst_list.add(dst); // 添加可撤离的据点列表
					}
				}
			}

			// 列出可用据点，按坐标距离和据点距离排序
			// (据点距离按升序排列，坐标距离按升序排列)
			if (dst_list.count == 0)
				best_dst = -1;
			else
			{
				dst_list.sort(function(a, b) {
					int build_dist_a = pk::get_building_distance(a.get_id(), main.src_t.get_id(), a.get_force_id());
					int build_dist_b = pk::get_building_distance(b.get_id(), main.src_t.get_id(), b.get_force_id());

					int pos_dist_a = pk::get_distance(a.pos, main.src_t.pos);
					int pos_dist_b = pk::get_distance(b.pos, main.src_t.pos);

					if (build_dist_a != build_dist_b)
						return (build_dist_a < build_dist_b);

					return (pos_dist_a < pos_dist_b);
				});
				best_dst = dst_list[0].get_id();
			}

			return best_dst;
		}
	};

	Main main;
}