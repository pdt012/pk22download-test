﻿// ## 2023/05/21 # 铃 # 使用262函数重写
// ## 2023/03/29 # 铃 # 创建,目的是为了让AI在粮草不足或者兵力不足时提前撤退

namespace AI部队提前撤退
{
    const int 部队目标变更_势力条件 = 0;          // 0:电脑AI和player _委任军团，1:player _委任军团，2:电脑AI, 3:均未使用  0:
    const bool 部队目标变更_适用玩家军团 = false; // 与势力条件无关，设定是否适用，忽略玩家的任务指定，建议false

    //=======================================================================================

    class Main
    {
		// 用于存储262
		pk::func262_t@ prev_callback_262;
        bool action = false;

        Main()
        {
			// 262函数前处理器存储、函数初始化、函数重新定义   
			@prev_callback_262 = cast<pk::func262_t@>(pk::get_func(262));
			pk::reset_func(262);
			pk::set_func(262, pk::func262_t(func262));
        }
        //---------------------------------------------------------------------------

 
		bool func262(pk::unit@ unit)
		{
			//部队所属主城还在的时候执行    
			pk::building@ building = pk::get_building(pk::get_service(unit));
			if (pk::is_alive(building))
			{
				pk::force@ force = pk::get_force(unit.get_force_id());
				AI_withdraw(unit, force, /*is_deployed*/true);

				if(action) return true;
			}

			if (prev_callback_262 is null) return false;
			return prev_callback_262(unit);
		}

        //---------------------------------------------------------------------------

        // 重新设置部队目标的函数
		void AI_withdraw(pk::unit@ unit, pk::force@ force, bool is_deployed)
        {
            if (!pk::is_alive(unit))
                return;

            if (unit.action_done)
                return;
            // 异常状态的部队本回合不设定跳过
            if (unit.status != 部队状态_通常)
                return;

            // 运输部队跳过? 要不要给运输部队生效呢?
            if (unit.type == 部队类型_运输)
                return;

            // 撤退部队无效,避免多次设置
            if (unit.order == 部队任务_撤退)
                return;

            // 排除无法确认所属据点的部队
            if (!pk::is_alive(pk::get_building(pk::get_service(unit))))
                return;

            // 排除本城被攻陷的部队
            int force_id = pk::get_current_turn_force_id();
            if (pk::get_building(pk::get_service(unit)).get_force_id() != force_id)
                return;

            // 排除玩家部队
            if (pk::is_player_controlled(unit))
                return;

            pk::building @building = pk::get_building(pk::get_service(unit));
            pk::person @leader = pk::get_person(unit.leader);
            string target_name = pk::decode(pk::get_name(building));
            int days = 10;

            // 担心函数的算法太过于拖慢计算,暂时这么粗略算

            int move = unit.attr.stat[部队能力_移动];
            int distance = pk::get_distance(building.pos, unit.pos);
            int food_days = int(unit.food * 10 / unit.troops);
            // int days = int(ceil(distance / (move / 5)));
            //  // 使用了新的计算距离的函数,但是不知道会不会严重的拖慢回合

            //担心get_path会闪退,先暂时改成pk::get_distance
            if (distance < 50)
                //days = cal_base_unit_time(unit, building.pos, unit.pos, unit.weapon);
                days = int(pk::get_distance(unit.pos, building.pos) / 6);

            // 兵力小于1000,撤退距离在大约6回合以内撤退,太远就放弃.
            if (unit.troops < 1000 and days < 6)
            {

                pk::set_order(unit, 部队任务_撤退, building.pos);
                action == true;
                if (pk::is_in_screen(unit.pos))
                    pk::say(pk::encode(pk::format("已无力再战,剩下的人快向\x1b[2x{}\x1b[0x撤退!", target_name)), leader);
            }

            // 兵粮不足5日,且兵粮勉强支撑到回据点.
            if (food_days < 6 and food_days >= days - 3)
            {
                // pk::trace("兵力" + unit.troops + "移动能力" + move + "兵粮日" + food_days + "天数" + days + "主将" + pk::decode(pk::get_name(leader)));
                pk::set_order(unit, 部队任务_撤退, building.pos);
                //  pk::trace( "命令" + unit.order + "坐标" + building.pos.x+ "主将" + pk::decode(pk::get_name(leader)));
                if (pk::is_in_screen(unit.pos))
                    pk::say(pk::encode(pk::format("弹尽粮绝,快向\x1b[2x{}\x1b[0x撤退!", target_name)), leader);
            }
        }

        // 检查势力选项函数
        bool check_force_option(pk::unit @unit)
        {
            // 不适用玩家军团（因为会忽略用户的任务指定并重新设定目标，所以建议不适用）
            if (!pk::is_alive(unit))
                return false;
            if (pk::is_player_controlled(unit))
            {
                if (部队目标变更_适用玩家军团)
                    return true;
                else
                    return false;
            }
            // 0：电脑AI和玩家_授权军团均不适用，1：只有玩家_授权军团，2：只有电脑AI，3：全部不适用
            if (部队目标变更_势力条件 == 0 and !pk::is_player_controlled(unit))
                return true;
            else if (部队目标变更_势力条件 == 1 and unit.is_player() and !pk::is_player_controlled(unit))
                return true;
            else if (部队目标变更_势力条件 == 2 and !unit.is_player())
                return true;
            else if (部队目标变更_势力条件 == 3)
                return false;

            return false;
        }

        int cal_base_unit_time(pk::unit @unit, pk::point src, pk::point dst, int weapon_id = 1)
        {
            if (src == dst)
                return 0;

            int src_id = pk::get_building_id(src);

            pk::change_weapon(unit, weapon_id);
            auto paths = pk::get_path(unit, src, dst);
            int 基础移动 = unit.attr.stat[部队能力_移动];
            int movement = 基础移动; // unit.attr.stat[部队能力_移动];
            auto equipment = pk::get_equipment(unit.weapon);
            auto equipment2 = pk::get_equipment(pk::get_sea_weapon_id(unit));
            int day = 0;
            for (int i = 0; i < int(paths.length); i++)
            {
                auto hex = pk::get_hex(paths[i]);
                int terrain_id = hex.terrain;
                if (terrain_id == 6 or terrain_id == 7 or terrain_id == 8)
                {
                    if (movement >= int(equipment2.movement_cost[terrain_id]))
                        movement -= equipment2.movement_cost[terrain_id];
                    else
                    {
                        // pk::trace(pk::format("day:{},x:{},y:{}", day, paths[i].x, paths[i].y));
                        day += 1;
                        movement = 基础移动; // unit.attr.stat[部队能力_移动];
                    }
                }
                else
                {
                    if (movement >= int(equipment.movement_cost[terrain_id]))
                        movement -= equipment.movement_cost[terrain_id];
                    else
                    {
                        // pk::trace(pk::format("day:{},x:{},y:{}", day, paths[i].x, paths[i].y));
                        day += 1;
                        movement = 基础移动; // unit.attr.stat[部队能力_移动];
                    }
                }
            }
            return day;
        }

    } Main main;
}
