﻿// ## 2023/05/20 # 铃 # 重写小城市运输的逻辑.
// ## 2023/03/11 # 铃 # 完成,AI可以把把武将送到没有武将但是有资源的小城,并把资源送到大城.
// ## 2022/11/11 # 铃 # 初稿,初衷是当小城市有钱有粮草,但是没有武将的时候,调度过来运输

namespace AI小城市运输
{
	const int 运输兵力下限 = 5000;
	const int 运输资金下限 = 2000;
	const int 运输兵粮下限 = 10000;
	const int 小城兵和兵粮比例 = 5;
	const int 兵装留存基础值 = 3000;
	const int 兵力留存基础值 = 3000;
	/// =========================================================================================

	const bool 调试模式 = false;

	const int KEY = pk::hash("AI小城市运输");

	class Main
	{

		Main()
		{
			pk::bind(202, pk::trigger202_t(onAIRunningOrder));
		}

		void onAIRunningOrder(pk::ai_context @context, pk::building @building, int cmd)
		{
			int building_force_id = building.get_force_id();
			pk::force @force = pk::get_force(building.get_force_id());
			pk::ai_context_base @base = context.base[building.get_id()];

			// 如果是大城市则不执行
			if (building.get_id() < 城市_末)
				return;

			if (building.is_player())
				return;

			if (base.status == 据点状态_战斗)
				return;

			if (pk::get_idle_person_list(building).count == 0)
				return;

			if (building.energy < 80)
				return;

			if ((pk::get_gold(building) < 运输资金下限 and pk::get_food(building) < 运输兵粮下限) or pk::get_troops(building) < 运输兵力下限)
				return;

			if (cmd == 据点AI_小城运输)
			{
				func_AI小城运输(force, building);
			}
		}

		void func_AI小城运输(pk::force @force, pk::building @building)
		{
			array<pk::building @> neighbor_city_arr = 获取目标城市列表(building);

			if (neighbor_city_arr.length == 0) return ;

			// 运输候选城市的优先顺序
			neighbor_city_arr.sort(function(a, b) { return main.比较运输城市(a, b); });

			pk::building @neighbor_city = neighbor_city_arr[0];

			if (!pk::is_alive(neighbor_city))
				return;

			func_运输部队出征(building, neighbor_city);

			return;
		}

		void func_运输部队出征(pk::building @building, pk::building @neighbor_building)
		{
			int 金运输量;
			int 粮运输量;
			int 兵运输量;
			float 资源留存比例 = 0.1;
			int 兵力留存量 = 0;

			if (附近敌方据点数量(building, 2) == 0)
				资源留存比例 = 0.1;
			else if (附近敌方据点数量(building, 2) > 0 and 附近敌方据点数量(building, 1) == 0)
				资源留存比例 = 0.2;
			else if (附近敌方据点数量(building, 1) > 0 and 附近敌方据点数量(building, 1) <= 2)
				资源留存比例 = 0.3;
			else if (附近敌方据点数量(building, 1) >= 2)
				资源留存比例 = 0.5;

			if (pk::get_gold(building) < int(pk::get_gold(neighbor_building) * 资源留存比例))
			{
				金运输量 = 0;
			}
			else
			{
				金运输量 = pk::get_gold(building) - int(pk::get_gold(neighbor_building) * 资源留存比例);
			}

			if (pk::get_food(building) < int(pk::get_food(neighbor_building) * 资源留存比例))
			{
				粮运输量 = 0;
			}
			else
			{
				粮运输量 = pk::get_food(building) - int(pk::get_food(neighbor_building) * 资源留存比例);
			}

			兵力留存量 = int(附近敌方据点兵力(building, 1) / 1000) * 兵力留存基础值;

			if (pk::get_troops(building) < 兵力留存量)
			{
				兵运输量 = 0;
			}
			else
			{
				兵运输量 = pk::get_troops(building) - int(pk::get_troops(neighbor_building) - 兵力留存量);
			}

			pk::com_deploy_cmd_info cmd;
			@cmd.base = @building;
			cmd.type = 部队类型_运输;

			// 兵装运输
			int 兵装运输量 = 0;
			int 兵装留存量 = 0;
			int 兵装运输量_最大值 = 0;

			兵装留存量 = int(附近敌方据点兵力(building, 1) / 1000) * 兵装留存基础值;

			for (int weapon_id = 兵器_枪; weapon_id <= 兵器_战马; weapon_id++)
			{

				兵装运输量_最大值 = pk::get_max_weapon_amount(neighbor_building, weapon_id) - pk::get_weapon_amount(neighbor_building, weapon_id);

				if (int(pk::get_weapon_amount(building, weapon_id)) > 兵装留存量)
					兵装运输量 = pk::get_weapon_amount(building, weapon_id) - 兵装留存量;
				else
					兵装运输量 = 0;

				if (兵装运输量 > 兵装运输量_最大值)
					兵装运输量 = 兵装运输量_最大值;

				if (weapon_id <= 兵器_战马 and 兵装运输量 > 0)
				{
					cmd.weapon_id[weapon_id] = 兵装运输量;
				}
			}

			pk::list<pk::person @> person_list = pk::get_idle_person_list(building);
			if (person_list.count == 0)
				return;

			person_list.sort(function(a, b) { return a.stat[武将能力_统率] > b.stat[武将能力_统率]; });

			cmd.member[0] = person_list[0].get_id();

			int 金运输量_最大值 = pk::min(pk::get_gold(building), pk::get_max_gold(neighbor_building) - pk::get_gold(neighbor_building));
			cmd.gold = pk::min(金运输量_最大值, 金运输量);

			// 屯兵太多的时候,还要考虑自己的兵粮够不够吃的问题
			int 粮运输量_最大值 = pk::min(pk::get_food(building), pk::get_max_food(neighbor_building) - pk::get_food(neighbor_building), pk::get_food(building) - pk::get_troops(building) * 小城兵和兵粮比例);
			cmd.food = pk::min(粮运输量_最大值, 粮运输量);

			int 兵运输量_最大值 = pk::min(pk::get_troops(building), pk::get_max_troops(neighbor_building) - pk::get_troops(neighbor_building));
			cmd.troops = pk::min(兵运输量_最大值, 兵运输量);

			// 运输队自己要够吃
			cmd.food = pk::max(cmd.food, 3000);

			cmd.troops = pk::max(cmd.troops, 200);

			cmd.order = 部队任务_移动;

			cmd.target_pos = neighbor_building.pos;

			int unit_id = pk::command(cmd);

			if (unit_id != -1)
			{
				pk::unit @unit = pk::get_unit(unit_id);
				unit.action_done = true;

				if (调试模式)
				{
					string unit_name = pk::format("\x1b[1x{}\x1b[0x", pk::decode(pk::get_name(unit)));
					string building_name = pk::format("\x1b[1x{}\x1b[0x", pk::decode(pk::get_name(building)));
					string neighbor_building_name = pk::format("\x1b[1x{}\x1b[0x", pk::decode(pk::get_name(neighbor_building)));
					string action_message = pk::format("{}从{}运输到{}", unit_name, building_name, neighbor_building_name);
					pk::force @force = pk::get_force(building.get_force_id());
					pk::message_box(pk::encode(action_message));
				}
			}
		}

		// 在运输部队出发的城市接壤的自势力城市中搜寻可运输的候选城市
		array<pk::building @> 获取目标城市列表(pk::building @building)
		{
			array<pk::building @> neighbor_city_arr;
			for (int i = 0; i < 城市_末; i++)
			{
				pk::building @neighbor_city = pk::get_building(i);

				int distanceToBuilding = pk::get_building_distance(building.get_id(), i, building.get_force_id());

				if (distanceToBuilding > 1)
					continue;

				if (!pk::is_alive(neighbor_city))
					continue;
				if (neighbor_city.get_force_id() != building.get_force_id())
					continue;

				neighbor_city_arr.insertLast(neighbor_city);
			}

			return neighbor_city_arr;
		}

		int 附近敌方据点数量(pk::building @building, int distance)
		{
			int enemy_building_count = 0;
			for (int i = 0; i < 据点_末; i++)
			{
				pk::building @neighbor_building = pk::get_building(i);
				int distanceToBuilding = pk::get_building_distance(building.get_id(), i, building.get_force_id());
				if (distanceToBuilding > distance)
					continue;
				if (!pk::is_enemy(building, neighbor_building))
					continue;
				enemy_building_count++;
			}
			return enemy_building_count;
		}
		int 附近敌方据点兵力(pk::building @building, int distance)
		{
			int enemy_troops_count = 0;
			for (int i = 0; i < 据点_末; i++)
			{
				pk::building @neighbor_building = pk::get_building(i);
				int distanceToBuilding = pk::get_building_distance(building.get_id(), i, building.get_force_id());
				if (distanceToBuilding > distance)
					continue;
				if (!pk::is_enemy(building, neighbor_building))
					continue;
				enemy_troops_count += pk::get_max_marchable_troops(neighbor_building);
			}
			return enemy_troops_count;
		}

		int 据点周围敌军(pk::building @building, int distance)
		{
			int enemy_troops = 0;
			pk::list<pk::unit @> unit_list = pk::get_unit_list();
			for (int i = 0; i < unit_list.count; i++)
			{
				pk::unit @unit0 = unit_list[i];
				if (!pk::is_alive(unit0))
					continue;
				if (!pk::is_enemy(unit0, building))
					continue;

				if (pk::get_distance(building.get_pos(), unit0.get_pos()) < distance)
					enemy_troops += unit0.troops;
			}
			return enemy_troops;
		}
		// 使用多因子模型比较需要输送的城市之间的优先级
		bool 比较运输城市(pk::building @building_a, pk::building @building_b)
		{
			float score_a = 0.f;
			float score_b = 0.f;

			// 因子1:城市前线状态：敌人据点越多的优先级越高
			score_a += 附近敌方据点数量(building_a, 1);
			score_b += 附近敌方据点数量(building_b, 1);

			// 因子2:城市前线状态：敌军兵力越多的优先级越高
			score_a += 附近敌方据点兵力(building_a, 1) / 30000.f;
			score_b += 附近敌方据点兵力(building_b, 1) / 30000.f;

			// 因子3:城市战斗状态
			score_a += 据点周围敌军(building_a, 30) / 30000.f;
			score_b += 据点周围敌军(building_b, 30) / 30000.f;

			// 因子4:城市自身兵力
			score_a -= pk::get_troops(building_a) / 10000.f;
			score_b -= pk::get_troops(building_b) / 10000.f;

			return score_a > score_b;
		}
	} Main main;

}