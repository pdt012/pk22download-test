
// ## 2023/03/14 ## 黑店小小二 # 为AI野战逻辑的优化而设置的独立结构体

namespace special_setting {
  const int KEY = pk::hash("特殊设定");
  const int KEY_索引_追加_部队起始 = 0;

  class Main
  {

    Main()
    {
      pk::bind(102, 0, pk::trigger102_t(onLoad));
      pk::bind(105, pk::trigger105_t(onSave));

      pk::bind(171, 999, pk::trigger171_t(onUnitRemove));
    }

    void onLoad()
    {
      if (!pk::get_scenario().loaded)
      {
      }
      if (pk::get_scenario().loaded)
      {
        for (int i = 0; i < 部队_末; i++)
        {
          for (int j = 0; j <= (设定部队结构体_uint32数 - 1); j++)
            ss_unit_info_temp[j][i] = uint32(pk::load(KEY, (KEY_索引_追加_部队起始 + (i * 设定部队结构体_uint32数 + j)), 0));

          ss_unit_info unit_t(i);
          special_unit[i] = unit_t;
        }
      }
    }

    void onSave(int file_id)
    {
      for (int i = 0; i < 部队_末; i++)
      {
        special_unit[i].update(i);
        for (int j = 0; j <= (设定部队结构体_uint32数 - 1); j++)
          pk::store(KEY, (KEY_索引_追加_部队起始 + (i * 设定部队结构体_uint32数 + j)), ss_unit_info_temp[j][i]);
      }
    }

    void onUnitRemove(pk::unit@ unit, pk::hex_object@ dst, int type)
    {
      ss_unit_info@ ss_unit_info = @special_unit[unit.get_id()];
      ss_unit_info.clear();
    }
  }
  Main main;
}


const int 设定部队结构体_uint32数 = 3;

array<array<uint32>> ss_unit_info_temp(设定部队结构体_uint32数, array<uint32>(部队_末, uint32(0)));

array<ss_unit_info> special_unit(部队_末);

class ss_unit_info {
  int8 order = 部队任务_末;
  int target;
  int target_type;
  int8 pos_x;
  int8 pos_y;
  bool setting = false;

  //初始化
  ss_unit_info(int id)
  {
    get_info(id);
  }

  ss_unit_info() {}

  void set_unit_data (pk::unit@ unit)
  {
    order = unit.order;
    target = unit.target;
    target_type = unit.target_type;
    pos_x = unit.pos.x;
    pos_y = unit.pos.y;
    setting = true;
  }

  void clear()
  {
    order = 部队任务_末;
    target = 0;
    target_type = 0;
    pos_x = 0;
    pos_y = 0;
    setting = false;
  }

  // bool is_has_setting()
  // {
  //   return order >= 0 and order < 部队任务_末;
  // }

  void get_info(int id)
  {
    fromInt32_0(ss_unit_info_temp[0][id]);
    fromInt32_1(ss_unit_info_temp[1][id]);
    fromInt32_2(ss_unit_info_temp[2][id]);
  }

  void update(int id)
  {
    ss_unit_info_temp[0][id] = toInt32_0();
    ss_unit_info_temp[1][id] = toInt32_1();
    ss_unit_info_temp[2][id] = toInt32_2();
  }

  uint32 toInt32_0(void)
  {
    uint8 setting_value = setting ? 1 : 0;
    uint32 x = order + (setting_value << 8) + (pos_x << 16) + (pos_y << 24);
    return x;
  }

  uint32 toInt32_1(void)
  {
    uint32 x = target;
    return x;
  }

  uint32 toInt32_2(void)
  {
    uint32 x = target_type;
    return x;
  }

  void fromInt32_0(uint32 x)
  {
    order = ((x << 24) >> 24);
    setting = (((x << 16) >> 24) == 1);
    pos_x = ((x << 8) >> 24);
    pos_y = (x << 0 >> 24);
  }

  void fromInt32_1(uint32 x)
  {
    target = ((x << 0) >> 0);
  }

  void fromInt32_2(uint32 x)
  {
    target_type = ((x << 0) >> 0);
  }
}
