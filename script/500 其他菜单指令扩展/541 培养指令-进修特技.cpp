﻿// ## 2022/02/14 # 江东新风 # 部分常量中文化 ##
// ## 2022/02/13 # 江东新风 # 重写函数，进修的特技添加对应能力和适性限制 ##
// ## 2020/09/21 # 江东新风 # ch::add_tp替换 ##
// ## 2020/09/16 # 江东新风 # 使用条件对君主行动状态进行判定 ##
// ## 2020/08/09 # 氕氘氚 # 加入【培养】菜单 ##
/**
원본 제작자 : HoneyBee
수정자 : 바바부부
설명 : 특정 무장에게 랜덤 특기를 부여함. 양자 스크립트 개조
**/

namespace 特技_进修
{
	// ================ CUSTOMIZE ================		
	const int ACTION_COST = 20;     	// (행동력 필요량: 기본 50 설정)	
	const int GOLD_COST = 1000;
	const int TP_COST = 500;    	   	// (기교P 필요량: 기본 100 설정)	

	const int KEY = pk::hash("特技进修");
	// ===========================================	
	const bool 调试模式 = false;
	class Main
	{
		
		pk::building@ building_;
		int check_result_;

		Main()
		{
			add_menu();
		}

		void add_menu()
		{
			pk::menu_item item;
			item.menu = cultivate_menu::菜单_培养;
			item.init = pk::building_menu_item_init_t(init);
			item.is_enabled = pk::menu_item_is_enabled_t(isEnabled);
			item.get_text = pk::menu_item_get_text_t(getText);
			item.get_desc = pk::menu_item_get_desc_t(getDesc);
			item.handler = pk::menu_item_handler_t(handler);
			pk::add_menu_item(item);
		}

		void init(pk::building@ building)
		{
			@building_ = @building;
			auto person_list = pk::get_idle_person_list(building_);
			check_result_ = check_avaliable(building, person_list);
		}

		string getText()
		{
			return pk::encode("进修");
		}

		bool isEnabled()
		{
			if (check_result_ != 0) return false;
			else return true;
		}

		string getDesc()
		{
			switch (check_result_)
			{
			case 1: return pk::encode("没有可执行的武将. ");
			case 2: return pk::encode(pk::format("行动力不足 (必须 {} 行动力)", ACTION_COST));
			case 3: return pk::encode(pk::format("资金不足 (至少 {} 资金)", GOLD_COST));
			case 4: return pk::encode(pk::format("技巧值不足 (至少 {} 技巧)", TP_COST));
			case 0: return pk::encode(pk::format("让武将进修学习特技.(消耗：(金{}+技巧{}+行动力{}))", GOLD_COST, TP_COST, ACTION_COST));
			default:
				return pk::encode("");
			}
			return pk::encode("");
		}

		int check_avaliable(pk::building@ building, pk::list<pk::person@> list)//之所以加入list是为了ai调用时不用重复计算，玩家菜单稍微多点操作问题不大
		{
			//pk::trace(pk::format("3.1:{}{}{}{}", pk::enemies_around(building), list.count, pk::get_district(building.get_district_id()).ap < ACTION_COST, pk::get_gold(building) < GOLD_COST));
			if (list.count == 0) return 1;
			else if (pk::get_district(building.get_district_id()).ap < ACTION_COST) return 2;
			else if (pk::get_gold(building) < GOLD_COST) return 3;
			else if (pk::get_force(building.get_force_id()).tp < TP_COST) return 4;
			else return 0;
		}

		bool handler()
		{
			if (pk::choose({ pk::encode(" 是 "), pk::encode(" 否 ") }, pk::encode(pk::format("是否让所属武将进修学习?\n(技巧点{})", TP_COST)), pk::get_person(武将_文官)) == 1)
			{
				return false;
			}

			bool person_confirm = false;

			auto person_list = pk::get_idle_person_list(building_);
			pk::person@monster;
			string monster_name = "";
			while (!person_confirm)
			{
				// 무장 선택 창을 열어서 선택하기
				auto person_sel = pk::person_selector(pk::encode("选择武将"), pk::encode("请选择要进修的武将."), person_list, 1, 1);
				if (person_sel.count == 0) return false;
				// 무장 선택
				@monster = @person_sel[0];

				// 선택된 무장의 이름
				monster_name = pk::decode(pk::get_name(monster));

				person_confirm = pk::yes_no(pk::encode(pk::format("是否让\x1b[1x{}\x1b[0x进修学习特技?", monster_name)));
			}

			ch::limit_skill_set(monster);
			monster.update();

			pk::message_box(pk::encode("我将以学习的新特技来为主公战斗."), monster);
			pk::message_box(pk::encode(pk::format("\x1b[1x{}\x1b[0x获得了新的特技\x1b[1x{}\x1b[0x.", monster_name,pk::decode(pk::get_name(pk::get_skill(monster.skill))))));

			// 행동력 감소.
			auto district = pk::get_district(building_.get_district_id());
			pk::add_ap(district, -ACTION_COST);
			ch::add_tp(pk::get_force(building_.get_force_id()), -TP_COST, building_.get_pos());
			pk::add_gold(building_,-GOLD_COST,true);
			monster.action_done = true;

			return true;
		}

	}

	Main main;
}