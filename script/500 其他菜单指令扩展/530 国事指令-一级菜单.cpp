﻿// ## 2022/06/19 # 铃 # 为新内政系统增加新的一级菜单，暂时注释掉早前的自动内政 ##
// ## 2021/09/26 # 江东新风 # 事件手动触发的一级菜单 ##
// ## 2021/01/18 # 江东新风 # 禁止拆建拼音改简体 ##
// ## 2020/12/15 # messi # 内政自动化改为太守自治 ##
// ## 2020/08/11 # 氕氘氚 # 修复太守不在时点击买卖可能出现的指針錯誤 ##
// ## 2020/08/08 # 氕氘氚 # 新增三个一级菜单 ##

namespace state_menu
{
    // ======================================================================

    const string shortcut_国事 = "e";

    // ======================================================================

    int 菜单_国事 = -1;
	
    // ======================================================================

    class Main
    {

        pk::building @building_;
        pk::force @force_;
        pk::person @taishu_;
        pk::person @kunshu_;
        pk::city @city_;
        pk::gate @gate_;
        pk::port @port_;
        pk::district @district_;

        int menu_city_id_;
        int menu_force_id_;

        Main()
        {
            pk::menu_item menu_item_国事;
            menu_item_国事.menu = 0;
            menu_item_国事.pos = 8;
            menu_item_国事.shortcut = shortcut_国事;
            menu_item_国事.init = pk::building_menu_item_init_t(init_菜单_国事);
            menu_item_国事.is_visible = pk::menu_item_is_visible_t(isVisible_菜单_国事);
            menu_item_国事.is_enabled = pk::menu_item_is_enabled_t(isEnabled_菜单_国事);
            menu_item_国事.get_text = pk::menu_item_get_text_t(getText_菜单_国事);
            menu_item_国事.get_desc = pk::menu_item_get_desc_t(getDesc_菜单_国事);
            菜单_国事 = pk::add_menu_item(menu_item_国事);
            // ch::u8debug(pk::format("guo shi menu {}", 菜单_国事));
        }

        // =============================================================================================

        void init_菜单_国事(pk::building @building)
        {
            @building_ = @building;
            @force_ = pk::get_force(building.get_force_id());
            @taishu_ = pk::get_person(pk::get_taishu_id(building));
            @kunshu_ = pk::get_person(force_.kunshu);

            if ((building_.get_id() <= 41) and (building_.get_id() >= 0))
                @city_ = pk::building_to_city(building);
        }

        bool isVisible_菜单_国事()
        {
            if (pk::is_campaign())
                return false;
            if (!setting_ex.mod_set[国事菜单扩展_开关])
                return false;
            if (building_.get_id() > 41)
                return false;
            if (building_.get_id() != kunshu_.service)
                return false;
            return true;
        }

        string getText_菜单_国事()
        {
            return pk::encode("国事");
        }

        bool isEnabled_菜单_国事()
        {
            if (building_.get_id() >= 据点_末)
                return false;
            if (pk::is_absent(kunshu_) or pk::is_unitize(kunshu_))
                return false;
            if (kunshu_.action_done == true)
                return false;
            return true;
        }

        string getDesc_菜单_国事()
        {
            if (pk::is_absent(kunshu_) or pk::is_unitize(kunshu_))
                return pk::encode("君主未在城中.");
            else if (kunshu_.action_done == true)
                return pk::encode("君主已完成行动.");
            else
                return pk::encode("执行国事相关指令.");
        }
    }

    Main main;
}
