// ## 2023/5/09 #铃# 上个版本修复未生效,再次修复 ##
// ## 2023/5/09 #铃# 降低了资源调度的资源下限(3000→1000) ##
// ## 2023/3/19 #铃# 修复了运输时计算附近兵力错误的bug ##
// ## 2023/3/19 #铃# 結合新的內政系統,新建了玩家的府资源调度功能 ##

namespace 府资源调度
{
	const bool 府资源调度_开启 = true;
	const int 兵力下限 = 1000; 
	const int 资金下限 = 1000; 
	const int 兵粮下限 = 3000; 
	const int ACTION_COST = 5; // 行动力消耗

	class Main
	{
		pk::building @building_spec;
		pk::force @force;
		pk::building @kunshu_building;
		pk::point src_pos;

		Main()
		{
			add_menu();
		}

		void add_menu()
		{
			pk::menu_item item;
			item.menu = 0;
			item.pos = 10;
			item.init = pk::building_menu_item_init_t(init_资源调度);
			item.is_visible = pk::menu_item_is_visible_t(isVisible_资源调度);
			item.is_enabled = pk::menu_item_is_enabled_t(isEnabled_资源调度);
			item.get_text = pk::menu_item_get_text_t(getText_资源调度);
			item.get_desc = pk::menu_item_get_desc_t(getDesc_资源调度);
			item.handler = pk::menu_item_handler_t(handler_资源调度);
			pk::add_menu_item(item);
		}

		void init_资源调度(pk::building @building)
		{
			@building_spec = @building;
			@force = pk::get_force(building.get_force_id());
			if (pk::is_valid_person_id(force.kunshu))
			{
				@kunshu_building = pk::get_building(pk::get_person(force.kunshu).service);
			}

			src_pos = building.get_pos();
		}

		bool isVisible_资源调度()
		{
			if (!building_spec.is_player())
				return false;

			int spec_id = ch::to_spec_id(building_spec.get_id());
			if (!ch::is_valid_spec_id(spec_id))
				return false;

			int force_id = building_spec.get_force_id();
			if (!pk::is_valid_force_id(force_id) or pk::get_current_turn_force_id() != force_id)
				return false;

			if (!pk::is_valid_district_id(kunshu_building.get_district_id()))
				return false;

			int spec_person = special_ex[spec_id].person;
			if (!pk::is_valid_person_id(spec_person))
				return false;

			return true;
		}

		bool isEnabled_资源调度()
		{

			// pk::trace("able_spec:" + check_资源调度(building)+"  buildingid " +building.get_id());
			if (check_资源调度(building_spec) == 0)
				return true;
			else
				return false;
		}

		int check_资源调度(pk::building @building)
		{
			if (building is null or !pk::is_alive(building))
				return 3;
			int spec_id = ch::to_spec_id(building.get_id());
			if (!ch::is_valid_spec_id(spec_id))
				return 4;
			if (pk::get_district(kunshu_building.get_district_id()).ap < ACTION_COST)
				return 1;

			int city_id = pk::get_building_id(building.pos);
			if (pk::get_building(city_id).get_force_id() != force.get_id())
				return 2;
			if (special_ex[spec_id].troops < 资金下限)
				return 5;

			return 0;
		}

		string getText_资源调度()
		{
			return pk::encode("资源调度");
		}

		string getDesc_资源调度()
		{
			int result = check_资源调度(building_spec);
			switch (result)
			{
			case 0:
				return pk::encode(pk::format("调度资源到主城(有损耗). (行动力 {})", ACTION_COST));
			case 1:
				return pk::encode(pk::format("行动力不足 (必须 {} 行动力)", ACTION_COST));
			case 2:
				return pk::encode("主城不是同一势力");
			case 3:
				return pk::encode("无效建筑");
			case 4:
				return pk::encode("无效ID");
			case 5:
				return pk::encode(pk::format("士兵不足 (至少 {} 人)", 兵力下限));
			default:
				return pk::encode("");
			}
			return pk::encode("");
		}

		string numberpad_t(int line, int original_value, int current_value)
		{
			return pk::encode("");
		}

		bool handler_资源调度()
		{
			int city_id = pk::get_building_id(building_spec.pos);
			BaseInfo @base_t = @base_ex[city_id];
			BuildingInfo @building_p = @building_ex[city_id];
			pk::building @city = pk::get_building(city_id);
			pk::person @kunshu = pk::get_person(pk::get_kunshu_id(city));

			int spec_id = ch::to_spec_id(building_spec.get_id());
			string src_name = ch::get_spec_name(spec_id);
			string city_name = pk::decode(pk::get_name(city));
			int person_id = special_ex[spec_id].person;

			int spec_gold = special_ex[spec_id].gold;
			int spec_food = special_ex[spec_id].food;
			int spec_troops = special_ex[spec_id].troops;
			int ap = pk::get_district(kunshu_building.get_district_id()).ap;

			int 损耗 = 计算敌方部队(building_spec) * 20 + int(计算敌方兵力(building_spec) / 1000);
			损耗 = pk::clamp(损耗, 0, 99);

			if (计算敌方部队(building_spec) == 计算有效地形(building_spec))
			{
				损耗 = 99;
				pk::message_box(pk::encode(pk::format("\x1b[2x{}\x1b[0x已无路可退,强行运输会产生大约\x1b[2x{}%\x1b[0x的损耗", src_name, 损耗)));
				bool confirm = pk::yes_no(pk::encode("是否还要运输??"));
				if (!confirm)
					return true;
			}

			if (计算敌方部队(building_spec) > 0)
				pk::message_box(pk::encode(pk::format("\x1b[2x{}\x1b[0x的附近有敌部队,强行运输会产生大约\x1b[2x{}%\x1b[0x的损耗", city_name, 损耗)));
			else
				pk::message_box(pk::encode(pk::format("\x1b[2x{}\x1b[0x的附近没有敌方部队,可以安全运输", city_name)));

			int choise = pk::choose({pk::encode("取消"), pk::encode("资金(每500金消耗1行动力)"), pk::encode("军粮(每900粮消耗1行动力)"), pk::encode("士兵(每300兵消耗1行动力)")}, pk::encode(pk::format("要从\x1b[2x{}\x1b[0x运输哪项资源到\x1b[2x{}\x1b[0x,\n\x1b[2x(损耗率约{}%)", src_name, city_name, 损耗)), kunshu);

			if (choise == 0)
			{
				return true;
			}
			if (choise == 1)
			{
				if (spec_gold < 资金下限)
				{
					pk::message_box(pk::encode(pk::format("\x1b[2x{}\x1b[0x的资金不足,最小为{}", src_name, 资金下限)));
					return true;
				}

				else
				{
					pk::int_bool gold_result = pk::numberpad(pk::encode("资源调度数量"), 0, pk::min(ap * 200, spec_gold - 资金下限), 0, pk::numberpad_t(numberpad_t));
					int gold = gold_result.first;
					int reduce = int(gold_result.first);

					if (gold == 0)
						return true;
					auto district_spec = pk::get_district(city.get_district_id());
					special_ex[spec_id].gold -= reduce;
					special_ex[spec_id].troops -= 100;
					district_spec.update();

					府兵运输队(building_spec, city, person_id, 1000, reduce, 0);

					auto district_city = pk::get_district(city.get_district_id());
					pk::add_gold(city, int(reduce * (1 - (损耗 / 100))), true);
					ch::add_troops(city, 100, true);
					district_city.update();

					int ap_reduce = int(reduce / 300);
					auto district_ap = pk::get_district(kunshu_building.get_district_id());
					pk::add_ap(district_ap, -int(ACTION_COST + ap_reduce));
					district_ap.update();

					pk::combat_text(-reduce, 3, src_pos);
					pk::combat_text(-500, 1, src_pos);
					pk::message_box(pk::encode(pk::format("从\x1b[2x{}\x1b[0x调度了{}金到\x1b[2x{}", src_name, reduce, city_name)));
				}
			}

			if (choise == 2)
			{
				if (spec_food < 兵粮下限)
				{
					pk::message_box(pk::encode(pk::format("报告,\x1b[2x{}\x1b[0x的军粮不足,最小为{}", src_name, 兵粮下限)));
					return true;
				}
				else
				{
					pk::int_bool food_result = pk::numberpad(pk::encode("资源调度数量"), 0, pk::min(ap * 800, spec_food - 兵粮下限), 0, pk::numberpad_t(numberpad_t));
					int food = food_result.first;
					int reduce = int(food_result.first);
					if (food == 0)
						return true;
					auto district_spec = pk::get_district(city.get_district_id());
					special_ex[spec_id].food -= reduce;
					special_ex[spec_id].troops -= 100;
					district_spec.update();

					府兵运输队(building_spec, city, person_id, 1000, 0, reduce);

					auto district_city = pk::get_district(city.get_district_id());
					pk::add_food(city, int(reduce * (1 - (损耗 / 100))), true);
					ch::add_troops(city, 100);
					district_city.update();

					int ap_reduce = int(reduce / 900);
					auto district_ap = pk::get_district(kunshu_building.get_district_id());
					pk::add_ap(district_ap, -int(ACTION_COST + ap_reduce));
					pk::trace("ACTION_COST:" + ACTION_COST + "  ap_reduce " + ap_reduce);

					district_ap.update();

					pk::combat_text(-reduce, 4, src_pos);
					pk::combat_text(-500, 1, src_pos);
					pk::message_box(pk::encode(pk::format("从\x1b[2x{}\x1b[0x调度了{}军粮到\x1b[2x{}", src_name, reduce, city_name)));
				}
			}

			if (choise == 3)
			{
				if (special_ex[spec_id].troops < 兵力下限)
				{
					pk::message_box(pk::encode(pk::format("报告,\x1b[2x{}\x1b[0x的府兵不足,最小为{}", src_name, 兵力下限)));
					return true;
				}
				else
				{
					pk::int_bool troops_result = pk::numberpad(pk::encode("资源调度数量"), 0, pk::min(ap * 100, spec_troops - 兵力下限), 0, pk::numberpad_t(numberpad_t));
					int troops = troops_result.first;
					int reduce = int(troops_result.first);
					if (troops == 0)
						return true;

					auto district_spec = pk::get_district(city.get_district_id());
					special_ex[spec_id].troops -= reduce;
					district_spec.update();

					府兵运输队(building_spec, city, person_id, reduce, 0, 0);

					auto district_city = pk::get_district(city.get_district_id());
					ch::add_troops(city, int(reduce * (1 - (损耗 / 100))), true);
					district_city.update();

					int ap_reduce = int(reduce / 200);
					auto district_ap = pk::get_district(kunshu_building.get_district_id());
					pk::add_ap(district_ap, -int(ACTION_COST + ap_reduce));
					district_ap.update();

					pk::combat_text(-reduce, 1, src_pos);
					pk::message_box(pk::encode(pk::format("从\x1b[2x{}\x1b[0x调度了{}士兵到\x1b[2x{}", src_name, reduce, city_name)));
				}
			}

			return true;
		}

		int 计算敌方部队(pk::building @building)
		{

			int enemy_units = 0;

			auto range = pk::range(building.get_pos(), 1, 1);
			for (int i = 0; i < int(range.length); i++)
			{
				auto unit = pk::get_unit(range[i]);
				if (pk::is_alive(unit) and pk::is_enemy(building, unit))
				{

					enemy_units++;
					// pk::trace(pk::format("range.length：{},unit.troops:{},enemy_troops:{},enemy_unit:{}", range.length, unit.troops, enemy_troops,enemy_units));
				}
			}
			return enemy_units;
		}

		int 计算有效地形(pk::building @building)
		{
			int valid_pos_count = 0;

			auto range = pk::range(building.get_pos(), 1, 1);
			for (int i = 0; i < int(range.length); i++)
			{
				pk::hex @hex = pk::get_hex(range[i]);
				int terrain_id = hex.terrain;
				if (pk::is_enabled_terrain(terrain_id))
					valid_pos_count++;
				// pk::trace(pk::format("range.length：{},unit.troops:{},enemy_troops:{},enemy_unit:{}", range.length, unit.troops, enemy_troops,enemy_units));
			}

			return valid_pos_count;
		}

		int
		计算敌方兵力(pk::building @building)
		{

			int enemy_troops = 0;
			int enemy_units = 0;

			auto range = pk::range(building.get_pos(), 1, 2);
			for (int i = 0; i < int(range.length); i++)
			{
				auto unit = pk::get_unit(range[i]);
				if (pk::is_alive(unit) and pk::is_enemy(building, unit))
				{

					enemy_troops += unit.troops;
					// enemy_units++;
					// pk::trace(pk::format("range.length：{},unit.troops:{},enemy_troops:{},enemy_unit:{}", range.length, unit.troops, enemy_troops,enemy_units));
				}
			}
			return enemy_troops;
		}

		void 府兵运输队(pk::building @building, pk::building @city, int person_id, int troops, int gold, int food)
		{

			if (!func_person_slot_available())
				return; // 沒有空余武将位则不生成

			pk::point src_pos;
			pk::point dst_pos;
			auto arr1 = pk::range(building.get_pos(), 1, 1);
			for (int i = 0; i < int(arr1.length); i++)
			{
				pk::hex @hex = pk::get_hex(arr1[i]);
				int terrain_id = hex.terrain;

				if (!hex.has_building and !hex.has_unit and pk::is_valid_terrain_id(terrain_id) and pk::is_enabled_terrain(terrain_id) and (terrain_id <= 地形_湿地 or terrain_id == 地形_荒地 or terrain_id == 地形_官道))
				{
					src_pos = arr1[i];
					break;
				}

				if (!hex.has_building and !hex.has_unit and pk::is_valid_terrain_id(terrain_id) and pk::is_enabled_terrain(terrain_id) and terrain_id < 地形_栈道 and terrain_id != 地形_川)
					src_pos = arr1[i];
			}

			auto arr2 = pk::range(building.get_pos(), 1, 1);
			for (int i = 0; i < int(arr2.length); i++)
			{
				pk::hex @hex = pk::get_hex(arr2[i]);
				int terrain_id = hex.terrain;

				if (!hex.has_building and !hex.has_unit and pk::is_valid_terrain_id(terrain_id) and pk::is_enabled_terrain(terrain_id) and terrain_id < 地形_栈道 and terrain_id != 地形_川)
					dst_pos = arr2[i];
			}

			//	break;
			pk::person @virtual_person = func_create_person(pk::get_person(person_id));
			if (!pk::is_alive(virtual_person))
			{
				pk::reset(virtual_person);
				return;
			}
			// pk::trace(pk::format("x1 :{},y1:{},x2 :{},y2 :{}", src_pos.x, src_pos.y, dst_pos.x, dst_pos.y));

			// unit_pos = get_empty_pos(building.pos, 1, 2); // 生成部队前再次核实坐标有效

			pk::com_march_cmd_info cmd_draft;
			@cmd_draft.base = building;
			cmd_draft.type = 部队类型_运输;
			cmd_draft.member[0] = virtual_person.get_id();
			cmd_draft.gold = gold;
			cmd_draft.troops = troops;
			cmd_draft.food = food;
			cmd_draft.order = 部队任务_移动;
			cmd_draft.target_pos = dst_pos;
			cmd_draft.weapon_id[0] = 1;
			cmd_draft.weapon_amount[0] = 5000;
			pk::unit @unit = pk::create_unit2(cmd_draft, src_pos, false);
			unit.energy = 100;

			pk::point next_pos = get_empty_pos(city.pos, 1, 2);
			// next_pos = pk::point(182, 24);

			int distance = pk::get_distance(unit.pos, next_pos);
			pk::trace("distance:" + distance);

			if (next_pos != unit.pos)
			{
				pk::trace("unit.pos.x:" + unit.pos.x + "unit.pos.y:" + unit.pos.y + "next_pos.x:" + next_pos.x + "next_pos.y:" + next_pos.y);
				auto paths = pk::get_path(unit, unit.pos, next_pos);
				pk::trace("distance2:" + distance);
				// if (distance < 40)
				if (paths.length != 0) pk::move(unit, paths);

				// else
				// 	for (int i = 0; i < 2; i++)
				// 	{
				// 		paths = pk::get_path(unit, unit.pos, next_pos);
				// 		pk::move(unit, paths);
				// 		// pk::trace(pk::format("移动后坐标x1 :{},y1:{},x2 :{},y2 :{}", unit.pos.x, unit.pos.y, next_pos.x, next_pos.y));
				// 		if (next_pos == unit.pos)
				// 			break;
				// 	}
			}
			// pk::set_order(unit, 部队任务_移动, city.pos);
			pk::kill(unit, true);
		}

		pk::point get_empty_pos(pk::point pos, int distance_min, int distance_max)
		{
			array<pk::point> range_pos_arr = pk::range(pos, distance_min, distance_max);
			for (int arr_index = 0; arr_index < int(range_pos_arr.length); arr_index++)
			{
				pk::point range_pos = range_pos_arr[arr_index];
				if (!pk::is_valid_pos(range_pos))
					continue;

				pk::hex @hex = pk::get_hex(range_pos);
				if (hex.has_building)
					continue;
				if (hex.has_unit)
					continue;

				int terrain_id = hex.terrain;
				if (!pk::is_valid_terrain_id(terrain_id))
					continue;
				if (!pk::is_enabled_terrain(terrain_id))
					continue;

				return range_pos;
			}

			return pk::point(-1, -1);
		}

		void kill(pk::unit @unit, int spec_id)
		{
			specialinfo @spec_t = @special_ex[spec_id];
			spec_t.troops += unit.troops;
			spec_t.gold += unit.gold;
			spec_t.food += unit.food;
			pk::kill(unit, true);
		}

		// 遍历所有武将，身??死亡或无的都可用来生成
		bool func_person_slot_available()
		{
			for (int person_id = 敌将_开始; person_id < 敌将_末; person_id++)
			{
				pk::person @person = pk::get_person(person_id);
				if (person.mibun == 身份_无)
					return true;
				if (person.mibun == 身份_死亡)
					return true;
			}
			return false;
		}

		// 统领武将生成
		pk::person @func_create_person(pk::person @person)
		{
			pk::person @virtual_person = pk::create_bandit(pk::get_person(武将_卫士));
			virtual_person.name_read = pk::encode("府兵");
			// if (virtual_person.mibun == 身份_在野) pk::set_mibun(virtual_person, 身份_一般);
			pk::set_district(virtual_person, person.get_district_id());

			virtual_person.base_stat[武将能力_统率] = pk::max(50, pk::min(80, int(person.base_stat[武将能力_统率] * 0.5f + ch::randint(5, 10))));
			virtual_person.base_stat[武将能力_武力] = pk::max(50, pk::min(80, int(person.base_stat[武将能力_武力] * 0.5f + ch::randint(5, 10))));
			virtual_person.base_stat[武将能力_智力] = pk::max(50, pk::min(80, int(person.base_stat[武将能力_智力] * 0.6f + ch::randint(5, 10))));
			for (int heishu_id = 兵种_枪兵; heishu_id < 兵种_末; heishu_id++)
				virtual_person.tekisei[heishu_id] = person.tekisei[heishu_id] + ch::randint(-2, 1);
			virtual_person.rank = pk::min(81, person.rank + 8);
			virtual_person.skill = 特技_突袭;
			virtual_person.update();

			return virtual_person;
		}

		string numberpad(int line, int original_value, int current_value)
		{
			return pk::encode("");
		}

	} Main main;
}
