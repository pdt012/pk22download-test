﻿// ## 2020/08/08 # 氕氘氚 # 从全局菜单分离到部队指令内 ##

namespace unit_menu
{

    int 菜单_部队 = -1;

    // ======================================================================

    class Main
    {
        Main()
        {

            // 部队菜单======================================================================================

            pk::menu_item menu_item_部队;

            menu_item_部队.menu = 1;
            menu_item_部队.get_text = cast<pk::menu_item_get_text_t @>(function() { return pk::encode("部队"); }); // budui
            菜单_部队 = pk::add_menu_item(menu_item_部队);
        }
    }

    Main main;
}
