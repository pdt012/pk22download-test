﻿// ## 2022/06/19 # 铃 # 为新内政系统增加新的一级菜单，暂时注释掉早前的自动内政 ##
// ## 2021/09/26 # 江东新风 # 事件手动触发的一级菜单 ##
// ## 2021/01/18 # 江东新风 # 禁止拆建拼音改简体 ##
// ## 2020/12/15 # messi # 内政自动化改为太守自治 ##
// ## 2020/08/11 # 氕氘氚 # 修复太守不在时点击买卖可能出现的指針錯誤 ##
// ## 2020/08/08 # 氕氘氚 # 新增三个一级菜单 ##

namespace district_menu
{
    // ======================================================================

    const bool 군단명령_군단거점출병 = true; // 출진조건 만족하는 거점들을 일괄출진 메뉴 활성화
    const bool 군단명령_징병순찰훈련 = true; // 징병,순찰,훈련 등을 일괄실행 하는 메뉴군 활성화
    const bool 군단명령_군단병장생산 = true; // 병장생산(단야,구사,공방,조선)을 일괄실행하는 메뉴 활성화
    const bool 군단명령_군단선택메뉴 = true; // [군단장] 메뉴 사용여부 : 군단장 소속 거점으로 화면이동

    const bool 군단명령_위임군단적용 = true;  // 对委任军团有效
    const bool 군단명령_도시실행확인 = false; // 显示詢問确认执行提示

    const string shortcut_军团 = "q";
	
    const string shortcut_军团长 = "q";
    const string shortcut_军团军事 = "w";
    const string shortcut_据点内政 = "e";
    const string shortcut_军团内政 = "r";
    const string shortcut_军团生chan = "t";
    const string shortcut_禁止拆建 = "y";

    // ======================================================================

    int 菜单_军团 = -1;

    // ======================================================================

    class Main
    {

        pk::building @building_;
        pk::force @force_;
        pk::person @taishu_;
        pk::person @kunshu_;
        pk::city @city_;
        pk::gate @gate_;
        pk::port @port_;
        pk::district @district_;

        int menu_city_id_;
        int menu_force_id_;

        Main()
        {
            if (군단명령_징병순찰훈련 or 군단명령_군단병장생산 or 군단명령_군단거점출병)
            {
                pk::menu_item menu_item_军团;
                menu_item_军团.menu = 0;
                menu_item_军团.pos = 0;
                menu_item_军团.shortcut = shortcut_军团;
                menu_item_军团.init = pk::building_menu_item_init_t(init_菜单_军团);
                menu_item_军团.is_visible = pk::menu_item_is_visible_t(isVisible_菜单_军团);
                menu_item_军团.is_enabled = pk::menu_item_is_enabled_t(isEnabled_菜单_军团);
                menu_item_军团.get_text = pk::menu_item_get_text_t(getText_菜单_军团);
                menu_item_军团.get_desc = pk::menu_item_get_desc_t(getDesc_菜单_军团);
                菜单_军团 = pk::add_menu_item(menu_item_军团);
            }

        }

        // =============================================================================================

        void init_菜单_军团(pk::building @building)
        {
            @building_ = @building;
            @force_ = pk::get_force(building.get_force_id());
            if (building_.facility <= 시설_항구)
            {
                @district_ = pk::get_district(building.get_district_id());
            }
        }

        bool isVisible_菜单_军团()
        {
            if (pk::is_campaign())
                return false;
            if (!setting_ex.mod_set[军团菜单扩展_开关])
                return false;
            if (building_.get_id() > 41)
                return false;
            if (!군단명령_위임군단적용 and district_.no > 1)
                return false;
            return true;
        }

        string getText_菜单_军团()
        {
            return pk::encode("军团命令");
        }

        bool isEnabled_菜单_军团()
        {
            if (building_.get_id() > 41)
                return false;
            return true;
        }

        string getDesc_菜单_军团()
        {
            return pk::encode("执行军团命令");
        }
 

    }

    Main main;
}
