var searchData=
[
  ['kill_0',['kill',['../namespacepk.html#a6fb63347bbbe4b7ff0c554c3e1affe05',1,'pk::kill(building @self, bool effect=true) NOEXCEPT'],['../namespacepk.html#a99271b0844cba1aceeb2574264c7da04',1,'pk::kill(force@ self, bool take_over=false, force@ by=null) NOEXCEPT'],['../namespacepk.html#a0facb90b16ac1bcb1a0761cf25a95ea0',1,'pk::kill(item @self) NOEXCEPT'],['../namespacepk.html#a481317efc7243f98738fd5f9d2dfb2c5',1,'pk::kill(person @self, person@ by=null, hex_object@ where=null, person@ successor=null, int type=0) NOEXCEPT'],['../namespacepk.html#a9ca9d3317261ea673a45e35b237f25e8',1,'pk::kill(unit @self, unit @by=null, bool melee=false, bool increase_hobaku_chance=false) NOEXCEPT']]]
];
