var searchData=
[
  ['kanshitsu_0',['kanshitsu',['../structpk_1_1person.html#a7685d82bb1bd9a28a2b9907eed557da3',1,'pk::person']]],
  ['ketsuen_1',['ketsuen',['../structpk_1_1person.html#afbbbaddce2b1753ff085a2b1f90a0705',1,'pk::person::ketsuen()'],['../structpk_1_1scenario.html#a8ea1d9655cab8f57390a869e3f9614cd',1,'pk::scenario::ketsuen()']]],
  ['kill_2',['kill',['../namespacepk.html#a6fb63347bbbe4b7ff0c554c3e1affe05',1,'pk::kill(building @self, bool effect=true) NOEXCEPT'],['../namespacepk.html#a99271b0844cba1aceeb2574264c7da04',1,'pk::kill(force@ self, bool take_over=false, force@ by=null) NOEXCEPT'],['../namespacepk.html#a0facb90b16ac1bcb1a0761cf25a95ea0',1,'pk::kill(item @self) NOEXCEPT'],['../namespacepk.html#a481317efc7243f98738fd5f9d2dfb2c5',1,'pk::kill(person @self, person@ by=null, hex_object@ where=null, person@ successor=null, int type=0) NOEXCEPT'],['../namespacepk.html#a9ca9d3317261ea673a45e35b237f25e8',1,'pk::kill(unit @self, unit @by=null, bool melee=false, bool increase_hobaku_chance=false) NOEXCEPT']]],
  ['kokugou_3',['kokugou',['../structpk_1_1force.html#a3a4b30b4bc164a6cfaa8a66cee42ed86',1,'pk::force::kokugou()'],['../structpk_1_1kokugou.html',1,'pk::kokugou']]],
  ['kouseki_4',['kouseki',['../structpk_1_1person.html#adc432ed42eef3ef0cc45903c7e80e0ba',1,'pk::person']]],
  ['kunshu_5',['kunshu',['../structpk_1_1force.html#a0421ce02d43f332365f6c06c563fb0ed',1,'pk::force']]],
  ['kvnode_6',['kvnode',['../classpk_1_1detail_1_1kvnode.html',1,'pk::detail']]]
];
