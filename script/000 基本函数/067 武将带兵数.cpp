﻿// ## 2023/05/18 # 江东新风 # 初次编写 ##
// ## 2020/07/26 ##
namespace GET_PERSON_COMMAND
{
	class Main
	{
		Main()
		{
			pk::set_func(67, pk::func67_t(callback));
		}

		int callback(pk::person@ person)
		{
			int command_num = 0;
			if (!pk::is_alive(person))
				return command_num;
			command_num = get_person_command_num(person);

			if (ch::has_skill(person, 特技_统兵)) command_num += pk::core::skill_constant_value(特技_统兵);
			int force_id = person.get_force_id();
			pk::force@ force = pk::get_force(force_id);
			if (pk::is_alive(force))
			{
				if (pk::has_tech(force, 技巧_军制改革))
					command_num += int(pk::core["military_reform"]);
			}
			return command_num;
		}

		int get_person_command_num(pk::person@ person)
		{
			int force_id = person.get_force_id();
			pk::force@ force = pk::get_force(force_id);
			if (!pk::is_alive(force))
				return 0;
			bool is_huangjing = force.kokugou == 国号_黄巾;//固定国号欸，黄巾？
			if (pk::is_kunshu(person))
			{
				int title_id = force.title;
				if (is_huangjing)
				{
					title_id = 0;
				}
				else if (title_id < 爵位_皇帝 || title_id > 爵位_无)
				{
					title_id = 爵位_无;
				}
				pk::title@ title = pk::get_title(title_id);
				if (pk::is_alive(title))
					return title.command;
			}

			int rank_id = person.rank;
			if (is_huangjing)
			{
				if (person.ketsuen == pk::get_person(武将_张角).ketsuen)
				{
					pk::title@ title = pk::get_title(爵位_皇帝);
					if (pk::is_alive(title))
						return title.command;
				}
				rank_id = 20;
			}
			else if (pk::is_normal_force(force))
			{
				if (!pk::is_valid_rank_id(rank_id))
					rank_id = 官职_无;
			}
			else
			{
				rank_id = 官职_军师将军;
			}
			pk::rank@ rank = pk::get_rank(rank_id);
			if (pk::is_alive(rank))
				return rank.command;
			else
				return 0;
		}
	}

	Main main;
}