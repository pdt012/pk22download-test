﻿// ## 2022/09/1 # 铃 # 优化人口增长的算法 ##
// ## 2022/04/29 # 铃 # 为新人口系统做适配 ##
// ## 2022/02/17 # 江东新风 # 部分人口函数移入 ##
// ## 2021/10/29 # 江东新风 # 结构体存储调用方式改进 ##
// ## 2021/10/24 # 江东新风 # 将pk::add_troops换成ch::add_troops以修正显示错误 ##
// ## 2021/10/24 # 江东新风 # get_stronghold_list改为get_base_list ##
// ## 2021/10/10 # 江东新风 # 治安增减的非城市据点函数 ##
// ## 2021/03/02 # 江东新风 # 特殊地名重建通用函数，cpp里调用kill_building都需要加上 ##
// ## 2021/02/04 # 江东新风 # 添加设置武器及马匹参数的函数 ##
// ## 2021/01/31 # 江东新风 # rand函数使用错误修复 ##
// ## 2021/01/23 # 江东新风 # get_order_change building版 ##
// ## 2021/01/14 # 江东新风 # 关掉trace ##
// ## 2021/01/13 # 江东新风 # 修复特殊设施闪退bug2 ##
// ## 2021/01/12 # 江东新风 # 修复特殊设施闪退bug ##
// ## 2021/01/11 # 江东新风 # 将特殊设施不被破坏的相关函数移入此处 ##
// ## 2021/01/06 # 江东新风 # 新增get_weapon_name函数 ##
// ## 2021/01/02 # 江东新风 # 新增rand_bool_1000和rand_bool_10000 ##
// ## 2020/12/23 # 江东新风 # 身份中文化 ##
// ## 2020/12/13 # 江东新风 # 添加获取城市港关list的函数 ##
// ## 2020/12/12 # 江东新风 # 修正参数类型错误 ##
// ## 2020/11/30 # 江东新风 # 新函数 get_force_food,get_force_gold, get_force_weapon ##
// ## 2020/10/27 # 江东新风 # 新函数 who_has_skill ##
// ## 2020/10/23 # 氕氘氚 # 取消全部ch::has_skill的组合特技判断 ##
// ## 2020/10/04 # 氕氘氚 # 再次修复宝物特技不生效的bug ##
// ## 2020/10/03 # 氕氘氚 # 修复add_tp在扣技巧时也翻倍的bug ##
// ## 2020/09/23 # 江东新风 # utf8版本u8debug ##
// ## 2020/09/21 # 氕氘氚 #修改函数has_skill, 武将行动时也生效##
// ## 2020/09/15 # 江东新风 #新增针对部队成员计算特技的has_skill版本，用于修复163移动力显示异常的bug##
// ## 2020/08/29 # 氕氘氚 #  添加函数debug
// ## 2020/08/21 # 氕氘氚 #  添加函数set_status
// ## 2020/08/18 # 氕氘氚 # 添加函数get_best_member_stat，添加get_order_change、add_public_order，特技法治影响治安下降 ##

const bool DEBUG_MODE = false;
const int DEBUG_LEVEL = -1;

namespace ch
{
	//  打印調試信息
	void trace(string trace_info)
	{
		if (DEBUG_MODE)
			pk::trace(trace_info);
	}
	void debug(string debug_info)
	{
		if (0 > DEBUG_LEVEL)
			pk::trace("[debug] " + debug_info);
	}
	void debug(int level, string debug_info)
	{
		if (level > DEBUG_LEVEL)
			pk::trace("[debug] " + debug_info);
	}

	//  打印調試信息u8版本
	void u8trace(string trace_info)
	{
		if (DEBUG_MODE)
			pk::trace(trace_info);
	}
	void u8debug(string debug_info)
	{
		if (0 > DEBUG_LEVEL)
			pk::trace("[debug] " + debug_info);
	}
	void u8debug(int level, string debug_info)
	{
		if (level > DEBUG_LEVEL)
			pk::trace("[debug] " + debug_info);
	}

	int randint(int start, int end)
	{
		return start + pk::rand(end - start + 1);
	}

	float randfloat(float start, float end)
	{
		int s = int(start * 100.f), e = int(end * 100.f);
		return (s + pk::rand(e - s + 1)) / 100.f;
	}

	bool rand_bool_1000(int rate)
	{
		rate = pk::max(0, pk::min(999, rate));
		if (int(pk::rand(1000)) < rate)
			return true;
		return false;
	}

	bool rand_bool_10000(int rate)
	{
		rate = pk::max(0, pk::min(9999, rate));
		if (int(pk::rand(10000)) < rate)
			return true;
		return false;
	}

	//  与0比較，至少为0
	int minZero(int value)
	{
		return pk::max(0, value);
	}

	//  设置上下限
	int inRange(int value, int min, int max)
	{
		return pk::max(min, pk::min(max, value));
	}
	//  设置上下限
	float inRange(float value, float min, float max)
	{
		return pk::max(min, pk::min(max, value));
	}

	uint32 set_bit(uint32 i, int pos, bool value)
	{
		uint32 t = value ? 1 : 0;
		i += t << pos;
		return i;
	}

	bool test_bit(uint32 i, int pos)
	{
		return (((i << (31 - pos)) >> 31) == 1);
	}

	void shuffle(array<int>& arr)
	{
		int n = arr.length;
		for (int i = 0; i < n; i++)
		{
			int j = pk::rand(n);//不确定随机值是否固定
			int temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
		}
	}

	float get_modify_rate(float rate)
	{
		if (rate < 1.f) return rate;
		else return sqrt(rate);
	}

	array<int> get_x_rand_int(int min, int max, int num)
	{
		array<int> temp;
		//temp.clear();
		int length = max - min;
		if (num > length) return temp;
		for (int i = min; i < max; ++i)
		{
			temp.insertLast(i);
		}

		shuffle(temp);
		array<int> arr;
		for (int i = 0; i < num; ++i)
		{
			arr.insertLast(temp[i]);
		}
		return arr;
	}

	//在给定范围内生成num个随机数，排除ignore数组里的随机数
	array<int> get_x_rand_int(int min, int max, int num, array<int> ignore)
	{
		array<int> temp;
		//temp.clear();
		int length = max - min;
		if (num > length) return temp;
		for (int i = min; i < max; ++i)
		{
			temp.insertLast(i);
		}

		shuffle(temp);
		array<int> arr;
		int count = 0;
		for (int i = min; i < max; ++i)
		{
			if (ignore.find(temp[i]) > 0) continue;
			arr.insertLast(temp[i]);
			count += 1;
			if (count == num) break;
		}
		return arr;
	}

	void sort_arr(array<int>& inout arr, array<int>& inout index)
	{
		//冒泡排序,每一趟找出最大的,总共比较次数为arr.length-1次,每次的比较次数为arr.length-1次，依次递减
		int len = arr.length;
		for (int k = 0; k < len - 1; k++) {
			for (int m = 0; m < len - k - 1; m++) {
				if (arr[m] < arr[m + 1]) {
					int val = arr[m];
					arr[m] = arr[m + 1];
					arr[m + 1] = val;
					val = index[m];
					index[m] = index[m + 1];
					index[m + 1] = val;
				}
			}
		}
	}

	bool has_skill(pk::person @person, int skill_id)
	{
		return pk::has_skill(person, skill_id);
	}

	// 新增针对部队成员计算特技的has_skill版本，用于修复163移动力显示异常的bug
	bool has_skill(const pk::detail::arrayptr<pk::person @> &in member, int skill_id)
	{
		return pk::has_skill(member, skill_id);
	}

	bool has_skill(const array<pk::person @> &member, int skill_id)
	{
		return pk::has_skill(member, skill_id);
	}

	bool has_skill(pk::unit @unit, int skill_id)
	{
		// pk::trace(pk::format("开始判断部队的特技{}持有与否",skill_id));
		if (unit is null)
		{
			// pk::trace("部队不存在");
			return false;
		}
		return unit.has_skill(skill_id);//因为unit.has_skill调用了4895c0,所以不需要改
	}

	bool has_skill(pk::list<pk::person @> list, int skill_id)
	{
		for (int i = 0; i < list.count; i++)
		{
			pk::person @person = list[i];
			if (pk::is_alive(person) and pk::has_skill(person, skill_id))
				return true;
		}
		return false;
	}
	/**
	 * @param only_not_absent: 只在武将空閑时生效
	 */
	bool has_skill(pk::building @building, int skill_id, bool only_not_absent = false)
	{
		if (building.facility > 2)
			return false;
		auto defender_list = pk::get_person_list(building, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般));
		if (defender_list.count > 0)
		{
			for (int i = 0; i < defender_list.count; i++)
			{
				pk::person @defender = defender_list[i];
				if ((!only_not_absent) or (!pk::is_unitize(defender) and !pk::is_absent(defender)))
				{
					if (pk::has_skill(defender, skill_id))
						return true;
				}
			}
		}
		return false;
	}

	bool has_skill(pk::force @force, int skill_id)
	{
		if (!pk::is_alive(force))
			return false;
		auto person_list = pk::list_to_array(pk::get_person_list(force, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般)));
		if (person_list.length > 0)
		{
			for (int i = 0; i < int(person_list.length); i++)
			{
				pk::person @person = person_list[i];
				if (pk::has_skill(person, skill_id))
					return true;
			}
		}
		return false;
	}

	int who_has_skill(pk::unit @unit, int skill_id)
	{
		if (unit is null)
			return -1;
		if (skill_id < 0 || skill_id > 250)
			return -1;
		if (skill_id <= 250)
		{
			for (int m = 0; m < 3; m++)
			{
				if (pk::is_valid_person_id(unit.member[m]))
				{
					pk::person @member_t = pk::get_person(unit.member[m]); // 隊伍中的武將
					if (member_t is null || !pk::is_alive(member_t))
						continue;
					if (has_skill(member_t, skill_id))
						return unit.member[m];
				}
			}
		}
		return -1;
	}

	int who_has_skill(pk::building @building, int skill_id, bool only_not_absent = false)
	{
		if (building.get_id() > 87)
			return -1;
		auto defender_list = pk::list_to_array(pk::get_person_list(building, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般)));
		if (defender_list.length > 0)
		{
			for (int i = 0; i < int(defender_list.length); i++)
			{
				pk::person @defender = defender_list[i];
				if ((!only_not_absent) or (!pk::is_unitize(defender) and !pk::is_absent(defender)))
				{
					if (pk::has_skill(defender, skill_id))
						return defender.get_id();
				}
			}
		}
		return -1;
	}

	int get_best_member_stat(pk::unit @unit, int stat_type)
	{
		return pk::get_best_member_stat(unit, stat_type);
	}
	int get_best_member_stat(pk::unit @unit, int skill_id, int stat_type)
	{
		return pk::get_best_member_stat(unit, skill_id, stat_type);
	}

	pk::person @get_best_stat_member(pk::building @src, int stat_type)
	{
		pk::list<pk::person @> list = pk::get_person_list(src, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般));
		return get_best_stat_member(list, stat_type);
	}

	pk::person @get_best_stat_member(pk::list<pk::person @> list, int stat_type)
	{
		if (list.count == 0)
			return null;
		int best_stat = 0;
		pk::person @best_member;
		for (int i = 0; i < list.count; ++i)
		{
			if (int(list[i].stat[stat_type]) > best_stat)
			{
				best_stat = list[i].stat[stat_type];
				@best_member = @list[i];
			}
		}
		return best_member;
	}

	int get_best_member_stat(pk::list<pk::person @> list, int stat_type)
	{
		if (list.count == 0)
			return -1;
		int best_stat = 0;
		for (int i = 0; i < list.count; ++i)
		{
			if (int(list[i].stat[stat_type]) > best_stat)
				best_stat = list[i].stat[stat_type];
		}
		return best_stat;
	}

	int get_best_member_stat(pk::list<pk::person @> list, int skill_id, int stat_type)
	{
		if (list.count == 0)
			return -1;
		int best_stat = 0;
		for (int i = 0; i < list.count; ++i)
		{
			if (has_skill(list[i], skill_id))
			{
				if (int(list[i].stat[stat_type]) > best_stat)
					best_stat = list[i].stat[stat_type];
			}
		}
		return best_stat;
	}

	int get_order_change(pk::city @city, int value)
	{
		if (value == 0)
			return 0;
		else if (value < 0)
		{
			if (has_skill(pk::city_to_building(city), 特技_法治))
				value = int(value * pk::core::skill_constant_value(特技_法治) /100.f);
		}
		return value;
	}

	int get_order_change(pk::building @building, int value)
	{
		if (value == 0)
			return 0;
		else if (value < 0)
		{
			if (has_skill(building, 特技_法治))
				value = int(value * pk::core::skill_constant_value(特技_法治) / 100.f);
		}
		return value;
	}

	int add_public_order(pk::city @city, int value, bool effect = false)
	{
		value = get_order_change(city, value);
		return pk::add_public_order(city, value, effect);
	}

	int add_public_order(pk::building @building, int value, bool effect = false)
	{
		int building_id = building.get_id();
		value = get_order_change(building, value);
		if (building_id > -1 and building_id < 据点_城市末)
		{
			value = get_order_change(building, value);
			return pk::add_public_order(pk::building_to_city(building), value, effect);
		}
		if (building_id >= 据点_城市末 and building_id < 据点_末)
		{
			value = get_order_change(building, value);
			// BaseInfo@ base_t = @base_ex[building_id];
			int p_order = base_ex[building_id].public_order;
			int change_value = ((p_order + value) >= 100) ? (100 - p_order) : (((p_order + value) <= 0) ? (-p_order) : value);
			// int change_value = ((base_t.public_order + value) >= 100) ? (100 - base_t.public_order) : (((base_t.public_order + value) <= 0) ? (-base_t.public_order) : value);
			base_ex[building_id].public_order += change_value;
			// base_t.public_order = base_t.public_order + change_value;
			//
			if (effect)
				pk::combat_text(change_value, 6, building.pos); // 1.value,2.effct_id,3.pos
			return change_value;
		}
		return 0;
	}

	int add_tp(pk::force @force, int tp, pk::point pos)
	{
		if (tp > 0)
			return pk::add_tp(force, tp * 2, pos);
		else
			return pk::add_tp(force, tp, pos);
	}

	int add_revenue_bonus(int base_id, int value)
	{
		if (base_id > -1 and base_id < 据点_末)
		{
			BaseInfo @base_t = @base_ex[base_id];
			uint16 max = base_id < 城市_末 ? 300 : 150;
			if (base_t.revenue_bonus > max)
				return 0;
			if (uint16(base_t.revenue_bonus + value) > max)
				value = max - base_t.revenue_bonus; // 人口下限
			base_t.revenue_bonus += value;
			return value;
		}
		return 0;
	}

	int add_harvest_bonus(int base_id, int value)
	{
		if (base_id > -1 and base_id < 据点_末)
		{
			BaseInfo @base_t = @base_ex[base_id];
			uint16 max = base_id < 城市_末 ? 2500 : 1250;
			if (base_t.harvest_bonus > max)
				return 0;
			if (uint16(base_t.harvest_bonus + value) > max)
				value = max - base_t.harvest_bonus; // 人口下限
			base_t.harvest_bonus += value;
			return value;
		}
		return 0;
	}

	int add_population(int base_id, int value, bool include_mil_pop = false)
	{
		if (base_id > -1 and base_id < 据点_末)
		{
			BaseInfo @base_t = @base_ex[base_id];
			int pre_population = base_t.population;
			if ((pre_population + value) < 5000)
				value = 5000 - pre_population; // 人口下限
			// if (base_id == 城市_襄阳) pk::trace(pk::format("add_population,襄阳人口：{}，value:{}", base_t.population, value));

			base_t.population += value;
			if (include_mil_pop)
				base_t.mil_pop_all += uint32(value * 0.3f);

			if (value > 0 and base_id < 城市_末) // 当人口增长超过阈值时城市升级
			{
				bool upgrade = false;
				int level = 0;
				if (pre_population < 一级城市阈值 and (pre_population + value) >= 一级城市阈值)
				{
					upgrade = true;
					level = 1;
				}
				if (pre_population < 二级城市阈值 and (pre_population + value) >= 二级城市阈值)
				{
					upgrade = true;
					level = 2;
				}
				if (pre_population < 三级城市阈值 and (pre_population + value) >= 三级城市阈值)
				{
					upgrade = true;
					level = 3;
				}
				if (pre_population < 四级城市阈值 and (pre_population + value) >= 四级城市阈值)
				{
					upgrade = true;
					level = 4;
				}
				if (upgrade)
					set_interior_land(base_id);
			}
			return value;
		}
		return 0;
	}

	float get_pop_inc_rate(int base_id, float pre_rate)
	{
		pk::building @building = pk::get_building(base_id);
		BaseInfo @base_t = @base_ex[base_id];
		// if (base_id == 城市_襄阳) pk::trace(pk::format("onNewDay 1,襄阳人口：{}", base_t.population));
		int public_order = 0;
		if (base_id < 城市_末)
			public_order = pk::get_city(base_id).public_order;
		else
			public_order = base_t.public_order;
		float porder_effect = (public_order - 70.f) / 30.f;

		float population_max = float(base_id < 城市_末 ? (pk::is_large_city(pk::get_city(base_id)) ? 大城市人口上限 : 小城市人口上限) : 港关人口上限);
		// float population_max = base_id < 城市_末 ? (pk::is_large_city(pk::get_city(base_id)) ? 1000000.f : 500000.f) : 300000.f;
		float population_t = base_t.population;
		// pk::trace(pk::format("population_max:{},population_t:{}", population_max, population_t));
		float final_rate = 0;
		float buf = 0;

		// 掌握人心科技
		if (pk::get_building(base_id).has_tech(技巧_掌握人心))
			buf += pre_rate * 0.2f;
		// 所以人口增长受到治安及人口上限影响
		// pk::trace(pk::format("城市ID：{},城市名：{},人口：{},增长率：{}", base_id, pk::decode(pk::get_name(building)), base_t.population, final_rate*100));

		if ((population_t / population_max) < 0.1f)
			final_rate = (pre_rate + buf) * 1.5 * porder_effect; // 风神季度改旬度之后,此处也要修正 铃2023/2/5.
		else if ((population_t / population_max) < 0.3f)
			final_rate = (pre_rate + buf) * 1.2 * porder_effect;
		else if ((population_t / population_max) < 0.5f)
			final_rate = (pre_rate + buf) * 1.0 * porder_effect;
		else if ((population_t / population_max) < 0.7f)
			final_rate = (pre_rate + buf) * 0.8 * porder_effect;
		else if ((population_t / population_max) < 0.9f)
			final_rate = (pre_rate + buf) * 0.6 * porder_effect;
		else if ((population_t / population_max) < 1.0f)
			final_rate = (pre_rate + buf) * 0.3 * porder_effect;
		else if ((population_t / population_max) < 1.2f)
			final_rate = (pre_rate + buf) * 0.2 * porder_effect;
		else
			final_rate = (人口基础增长率 / 20.f) * porder_effect;

		// pk::trace(pk::format("城市ID：{},城市名：{},人口：{},增长率：{}", base_id, pk::decode(pk::get_name(building)), base_t.population, final_rate*100));

		// pk::trace(pk::format("城市id：{},城市名:{},porder_effect:{},final_rate:{},buf:{}", building.get_id(),pk::decode(pk::get_name(building)),porder_effect,final_rate,buf));

		// if (pk::get_building(base_id).is_player()) pk::message_box(pk::encode(pk::format("报告,\x1b[2x{}\x1b[0x的final_rate为{},porder_effect为{},public_order为{}", pk::decode(pk::get_name(pk::get_building(base_id))), final_rate,porder_effect,public_order)));

		return final_rate;
	}

	int add_troops(pk::unit @unit, int value, bool effect = false, int type = -1)
	{
		int n = cast<pk::func58_t>(pk::get_func(58))(unit, value, type);
		if (effect)
			pk::combat_text(n, 1, unit.pos);
		return n;
	}

	int add_troops(pk::city @city, int value, bool effect = false, int type = -1)
	{
		int n = 0;
		if (type == 104 and value > 0) // 104暂定为征兵模式，伴随兵役人口变化
		{
			BaseInfo @base_t = @base_ex[city.get_id()];
			if (base_t.mil_pop_av >= 0)
			{
				value = pk::min(base_t.mil_pop_av, value); // 如果值小于可用兵役，改写value
			}
			n = cast<pk::func59_t>(pk::get_func(59))(city, value, type);
			add_mil_pop_av(city.get_id(), -n);
		}
		else if (type == 105 and value > 0) // 105暂定为募兵模式，伴随总兵役人口变化
		{
			BaseInfo @base_t = @base_ex[city.get_id()];
			if (base_t.population >= 0 and base_t.mil_pop_all >= 0)
			{
				value = pk::min(pk::min(base_t.mil_pop_all, base_t.population), int(0.5f * value)) * 2; // 如果值小于总兵役人口，改写value
			}
			n = cast<pk::func59_t>(pk::get_func(59))(city, value, type);
			add_mil_pop_all(city.get_id(), -int(0.5f * n)); // base_t.mil_pop_all -= int(0.5f * n);
			add_population(city.get_id(), -int(0.5f * n));	// base_t.population -= int(0.5f * n);
		}
		else
		{
			n = cast<pk::func59_t>(pk::get_func(59))(city, value, type);
		}

		if (effect)
			pk::combat_text(n, 1, city.pos);
		return n;
	}

	int add_troops(pk::building @building, int value, bool effect = false, int type = -1)
	{
		int n = 0;
		int building_id = building.get_id();
		if (type == 104 and value > 0) // 104暂定为征兵模式，伴随兵役人口变化
		{
			BaseInfo @base_t = @base_ex[building_id];
			if (base_t.mil_pop_av >= 0)
			{
				value = pk::min(base_t.mil_pop_av, value); // 如果值小于可用兵役，改写value
			}
			n = cast<pk::func60_t>(pk::get_func(60))(building, value, type);
			add_mil_pop_all(building_id, -n); // base_t.mil_pop_av -= n;
		}
		else if (type == 105 and value > 0) // 104暂定为征兵模式，伴随总兵役人口变化
		{
			BaseInfo @base_t = @base_ex[building_id];
			if (base_t.mil_pop_av >= 0)
			{
				value = pk::min(base_t.mil_pop_all, value); // 如果值小于总兵役人口，改写value
			}
			n = cast<pk::func60_t>(pk::get_func(60))(building, value, type);
			add_mil_pop_all(building_id, -n);
		}
		else
		{
			n = cast<pk::func60_t>(pk::get_func(60))(building, value, type);
		}

		if (effect and n != 0)
			pk::combat_text(n, 1, building.pos);
		return n;
	}

	int add_mil_pop_all(int base_id, int value) // 设定总兵役上限不超过人口的1/3
	{
		BaseInfo @base_a = @base_ex[base_id];
		if (value >= 0)
		{
			if (base_a.population <= uint32((base_a.mil_pop_all + value) * 3))
				value = int(base_a.population / 3 - base_a.mil_pop_all);
		}
		else
		{
			if ((base_a.mil_pop_all + value) < 0)
				value = -base_a.mil_pop_all;
		}
		base_a.mil_pop_all += value;
		return value;
	}

	int add_mil_pop_av(int base_id, int value) // 设定现兵役上限不超过人口的1/4
	{
		BaseInfo @base_a = @base_ex[base_id];
		int pre_value = value;
		if (value >= 0)
		{
			if (base_a.population <= uint32((base_a.mil_pop_av + value) * 4))
				value = int(base_a.population / 4 - base_a.mil_pop_av);
		}
		else
		{
			if ((base_a.mil_pop_av + value) < 0)
				value = -base_a.mil_pop_av;
		}
		base_a.mil_pop_av += value;
		return value;
	}
	/* 设置部队状态
	@param type      0 计略晕, 1 物理晕, 2 自然晕*/
	bool set_status(pk::unit @unit, pk::unit @by, int status, int time, int type, bool sound = true)
	{
		if (status == 部队状态_混乱 && type > 0 && has_skill(unit, 特技_治军)) // 治军免物理晕
		{
			trace("zhi jun");
			return false;
		}
		pk::set_status(unit, by, status, time, sound);
		return true;
	}

	int get_force_gold(pk::force @force)
	{
		// 分别获取城，港关，部队的金
		int n = 0;
		pk::list<pk::building @> building_list = pk::get_building_list();
		for (int i = 0; i < building_list.count; i++)
		{
			pk::building @building0 = building_list[i];
			int force0_id = building0.get_force_id();
			if (!pk::is_alive(building0) or force0_id != force.get_id())
				continue;
			n += pk::get_gold(building0);
		}
		pk::list<pk::unit @> unit_list = pk::get_unit_list(force);
		for (int i = 0; i < unit_list.count; i++)
		{
			pk::unit @unit0 = unit_list[i];
			if (!pk::is_alive(unit0))
				continue;
			n += unit0.gold;
		}
		return n;
	}

	int get_force_food(pk::force @force)
	{
		// 分别获取城，港关，部队的粮
		int n = 0;
		pk::list<pk::building @> building_list = pk::get_building_list();
		for (int i = 0; i < building_list.count; i++)
		{
			pk::building @building0 = building_list[i];
			int force0_id = building0.get_force_id();
			if (!pk::is_alive(building0) or force0_id != force.get_id())
				continue;
			n += pk::get_food(building0);
		}
		pk::list<pk::unit @> unit_list = pk::get_unit_list(force);
		for (int i = 0; i < unit_list.count; i++)
		{
			pk::unit @unit0 = unit_list[i];
			if (!pk::is_alive(unit0))
				continue;
			n += unit0.food;
		}
		return n;
	}

	// 只计算枪戟弩马
	int get_force_weapon(pk::force @force)
	{
		// 分别获取城，港关，部队的粮
		int n = 0;
		pk::list<pk::building @> building_list = pk::get_building_list();
		for (int i = 0; i < building_list.count; i++)
		{
			pk::building @building0 = building_list[i];
			int force0_id = building0.get_force_id();
			if (!pk::is_alive(building0) or force0_id != force.get_id())
				continue;
			n += pk::get_weapon_amount(building0, 1);
			n += pk::get_weapon_amount(building0, 2);
			n += pk::get_weapon_amount(building0, 3);
			n += pk::get_weapon_amount(building0, 4);
		}

		pk::list<pk::unit @> unit_list = pk::get_unit_list(force);
		for (int i = 0; i < unit_list.count; i++)
		{
			pk::unit @unit0 = unit_list[i];
			if (!pk::is_alive(unit0))
				continue;
			if (unit0.weapon == 兵器_枪 or unit0.weapon == 兵器_戟 or unit0.weapon == 兵器_弩 or unit0.weapon == 兵器_战马)
				n += unit0.troops;
		}
		return n;
	}

	// 只计算任意
	int get_force_weapon(pk::force @force, int weapon_id)
	{
		// 分别获取城，港关，部队的兵装
		int n = 0;
		pk::list<pk::building @> building_list = pk::get_building_list();
		for (int i = 0; i < building_list.count; i++)
		{
			pk::building @building0 = building_list[i];
			int force0_id = building0.get_force_id();
			if (!pk::is_alive(building0) or force0_id != force.get_id())
				continue;
			n += pk::get_weapon_amount(building0, weapon_id);
		}

		pk::list<pk::unit @> unit_list = pk::get_unit_list(force);
		for (int i = 0; i < unit_list.count; i++)
		{
			pk::unit @unit0 = unit_list[i];
			if (!pk::is_alive(unit0))
				continue;
			if (unit0.weapon == weapon_id)
				n += unit0.troops;
		}
		return n;
	}

	int get_force_person_count(pk::force @force)
	{
		// 分别获取城，港关，部队的粮
		int n = 0;
		pk::list<pk::person @> person_list = pk::get_person_list(force, pk::mibun_flags(身份_一般, 身份_君主, 身份_太守, 身份_都督));
		n = person_list.count;
		return n;
	}

	/*仇视系统的尝试，因为受伤致死的存在，没办法进行了
	vold kill(person@ self, person@ by = null, hex_object@ where = null, person@ successor = null, int type = 0)
	{
		if (by !is null)
		{
			//区分父，母，配偶，义兄弟，子嗣，亲爱

			pk::add_dislike();
		}
		pk::kill(self, by, where, successor, type);
	}

	bool isFriend(pk::person@ person1, pk::person@ person2)
	{
		bool result = false;

		if (pk::is_gikyoudai(person1, person2.get_id()))
			result = true;
		if (pk::is_fuufu(person1, person2.get_id()))
			result = true;
		if (pk::is_ketsuen(person1, person2.get_id()))
			result = true;
		if (pk::is_oyako(person1, person2.get_id()))
			result = true;
		return result;
	}*/

	pk::list<pk::building @> get_base_list(pk::force @force)
	{
		pk::list<pk::building @> list;
		for (int i = 0; i < 据点_末; i++)
		{
			pk::building @src = pk::get_building(i);
			if (src.get_force_id() == force.get_id())
				list.add(src);
		}
		return list;
	}

	pk::list<pk::building @> get_base_list()
	{
		pk::list<pk::building @> list;
		for (int i = 0; i < 据点_末; i++)
		{
			pk::building @src = pk::get_building(i);
			list.add(src);
		}
		return list;
	}

	pk::list<pk::building @> get_base_list(int province_id)
	{
		pk::list<pk::building @> list;
		for (int i = 0; i < 据点_末; i++)
		{
			pk::building @src = pk::get_building(i);
			if (pk::get_province_id(src.pos) == province_id)
				list.add(src);
		}
		return list;
	}

	int get_weapon_cost(int weapon_id)
	{
		return pk::get_equipment(weapon_id).gold_cost;
	}

	string get_weapon_name2(int weapon_id)
	{
		string weapon_name;
		switch (weapon_id)
		{
		case 兵器_剑:
			weapon_name = "剑";
			break;
		case 兵器_枪:
			weapon_name = "枪";
			break;
		case 兵器_戟:
			weapon_name = "戟";
			break;
		case 兵器_弩:
			weapon_name = "弩";
			break;
		case 兵器_战马:
			weapon_name = "战马";
			break;
		case 兵器_冲车:
			weapon_name = "冲车";
			break;
		case 兵器_井阑:
			weapon_name = "井阑";
			break;
		case 兵器_投石:
			weapon_name = "投石";
			break;
		case 兵器_木兽:
			weapon_name = "木兽";
			break;
		case 兵器_走舸:
			weapon_name = "走舸";
			break;
		case 兵器_楼船:
			weapon_name = "楼船";
			break;
		case 兵器_斗舰:
			weapon_name = "斗舰";
			break;
		default:
			weapon_name = "??";
			break;
		}
		return weapon_name;
	}

	string get_call(pk::person @src, pk::person @dst)
	{
		pk::msg_param msg_param(46, src, dst);
		return pk::get_msg(msg_param);
	}
	// 计谋执行部队和目标部队的台词输出:调用msg文件的台词(计谋相关内容- S11MSG01.s11文件)   계략 실행부대 및 대상부대의 대사 출력 : msg 폴더의 s11 파일 대사 소환 (계략관련 내용 - S11MSG01.s11 파일)
	void say_message(int msg_id, pk::person @p0, pk::person @p1, pk::unit @u0, pk::unit @p0_u)
	{
		pk::msg_param msg_param(msg_id, p0, p1);
		@msg_param.unit[0] = @u0;
		pk::say(pk::get_msg(msg_param), p0, p0_u);
	}

	void say_message(int msg_id, pk::person @p0, pk::person @p1, pk::building @b0, pk::building @p0_b)
	{
		pk::msg_param msg_param(msg_id, p0, p1);
		@msg_param.building[0] = @b0;
		pk::say(pk::get_msg(msg_param), p0, p0_b);
	}

	void say_message(int msg_id, int num, pk::person @p0, pk::person @p1, pk::building @b0, pk::building @p0_b)
	{
		pk::msg_param msg_param(msg_id, p0, p1);
		@msg_param.building[0] = @b0;
		msg_param.num[0] = num;
		pk::say(pk::get_msg(msg_param), p0, p0_b);
	}

	void history_log(int msg_id, int num, pk::person @p0, pk::person @p1, pk::building @b0, pk::building @p0_b)
	{
		pk::msg_param msg_param(msg_id, p0, p1);
		@msg_param.building[0] = @b0;
		msg_param.num[0] = num;
		pk::force @force = pk::get_force(p0.get_force_id());
		pk::history_log(p0.get_pos(), force.color, pk::get_msg(msg_param));
	}

	bool player_check(pk::person @person)
	{
		if (pk::is_alive(person))
		{
			pk::force @force = pk::get_force(person.get_force_id());
			if (pk::is_alive(force))
			{
				if (force.is_player())
				{
					pk::district @district = pk::get_district(person.get_district_id());
					if (pk::is_alive(district))
					{
						if (pk::is_player_controlled(district))
							return true;
					}
				}
			}
		}
		return false;
	}

	int get_attacking_near_enemy_base(pk::ai_context @context, pk::building @building)
	{
		int building_id = building.get_id();

		if (pk::is_valid_base_id(building_id))
		{
			if (pk::is_valid_city_id(building_id))
			{
				pk::city @city = pk::get_city(building_id);
				// 确认邻近城市为目标城市，满足条件执行破坏指令
				for (int i = 0; i < context.target_count; ++i)
				{
					if (context.type[i] == 1) // 类型1为攻击
					{
						int target = context.target[i];
						if (pk::is_neighbor_city(city, target))
							return target;
					}
				}
			}
			else
			{
				// 确认邻近城市为目标城市，满足条件执行破坏指令
				for (int i = 0; i < context.target_count; ++i)
				{
					if (context.type[i] == 1) // 类型1为攻击
					{
						int target = context.target[i];
						if (pk::is_neighbor_base(building_id, target))
							return target;
					}
				}
			}
		}
		return -1;
	}

	pk::list<pk::person @> get_member_list(pk::unit @unit)
	{
		pk::list<pk::person @> list;
		for (int i = 0; i < 3; ++i)
		{
			if (pk::is_valid_person_id(unit.member[i]))
			{
				pk::person @person0 = pk::get_person(unit.member[i]);
				if (pk::is_alive(person0))
					list.add(person0);
			}
		}
		return list;
	}

	void 特殊地名争夺处理(int facility_id, int force_id, pk::point pos, int pre_building_id)
	{
		if (特定地点特定设施可争夺)
		{
			int spec_id = to_spec_id(pre_building_id);
			if (is_valid_spec_id(spec_id))
			{

				if (force_id != -1)
				{
					pk::force @force0 = pk::get_force(force_id);
					if (facility_id == 设施_连弩橹 or facility_id == 设施_箭楼)
						facility_id = pk::has_tech(force0, 技巧_施设强化) ? 设施_连弩橹 : 设施_箭楼;
					else if (facility_id == 设施_阵 or facility_id == 设施_砦 or facility_id == 设施_城塞)
						facility_id = pk::has_tech(force0, 技巧_设施强化) ? (pk::has_tech(force0, 技巧_城壁强化) ? 设施_城塞 : 设施_砦) : 设施_阵;
					// pk::trace(pk::format("最终生成设施类型，id为：{}", facility_id));
				}
				pk::building @building0 = pk::create_spec(pk::get_facility(ch::get_spec_facility_id(spec_id)), pos, force_id, spec_id);
				pk::complete(building0);
				reset_spec(spec_id);
			}

		}
	}

	void 特殊地名争夺处理(pk::building @target_building, pk::unit @attacker, int &out destroyed_facility_id, int &out destroy_unit_id, int &out hp_damage, pk::point &out destroyed_building_pos, bool &out destroy_flag)
	{
		if (特定地点特定设施可争夺)
		{
			// destroyed_facility_id = -1;
			// destroy_unit_id = -1;
			// destroyed_building_pos = pk::point(-1,-1);
			// destroy_flag = false;
			// pk::trace(pk::format("hpdamage:{}",hp_damage));
			// pk::trace("全局pos1");
			if (hp_damage >= int(target_building.hp))
			{
				if (地名设施.find(target_building.facility) >= 0)
				{
					// pk::trace(pk::format("全局pos2,facility{}", target_building.facility));
					for (int i = 0; i < get_spec_end(); i++)//注意，地名末和length不一定一致
					{
						pk::point pos0 = get_spec_pos(i);
						if (pos0 == target_building.pos)
						{
							// pk::trace(pk::format("破坏时 x:{},y:{}.", target_building.pos.x, target_building.pos.y));
							if (attacker.get_force_id() < 非贼势力_末)
							{
								destroyed_facility_id = target_building.facility;
								destroyed_building_pos = target_building.pos;
								destroy_unit_id = attacker.get_id();
								destroy_flag = true;
								break;
							}
							else
							{
								hp_damage = 0;
								pk::create_effect(91, target_building.pos);
								pk::set_district(target_building, pk::get_district(attacker.get_district_id()), 0);
								target_building.hp = pk::get_max_hp(target_building);
								break;
							}
						}
					}
				}
			}
			else
				hp_damage = hp_damage;
		}
	}

	void 特殊地名设施_重建(pk::unit @unit, int &destroy_unit_id, int &destroyed_facility_id, pk::point &destroyed_building_pos, bool &destroy_flag)
	{
		if (destroy_unit_id != -1 and destroyed_building_pos.x != -1 and destroyed_facility_id != -1 and destroy_flag)
		{
			if (destroy_unit_id == unit.get_id() and 地名设施.find(destroyed_facility_id) >= 0)
			{
				pk::hex @hex0 = pk::get_hex(destroyed_building_pos);
				if (hex0.has_unit)
				{
					pk::kill(pk::get_unit(destroyed_building_pos));
				}
				if (hex0.has_building)
				{
					pk::kill(pk::get_building(destroyed_building_pos));
				}
				// pk::trace(pk::format("攻击部队id:{}.被摧毁建筑id：{}",destroy_unit.get_id(), destroyed_building.get_id()));
				// pk::trace(pk::format("建筑id:{}.建筑位置：{},{},攻击部队势力：{}",destroyed_building.facility, destroyed_building.pos.x, destroyed_building.pos.y, destroy_unit.get_force_id()));
				// auto pos = destroyed_building.pos;
				// pos.x = pos.x + 1
				pk::unit @destroy_unit = pk::get_unit(destroy_unit_id);
				pk::facility @destroy_facility = pk::get_facility(destroyed_facility_id);
				int destroy_force = -1;
				if (destroy_unit.get_force_id() < 非贼势力_末)
					destroy_force = destroy_unit.get_force_id();
				pk::create_effect(91, destroyed_building_pos);
				// pk::trace(pk::format("攻击部队id:{}. 重建势力id:{}",destroy_unit.get_id(),destroy_force));
				// pk::trace(pk::format("x:{},y:{}.",destroyed_building_pos.x,destroyed_building_pos.y));
				// pk::trace(pk::format("设施类型：{}",destroy_facility.get_id()));
				// pk::trace(pk::format("重建势力id:{}",destroy_force));
				pk::building @building0 = pk::create_building(destroy_facility, destroyed_building_pos, destroy_force);
				pk::complete(building0);
				// 初始化
				destroy_unit_id = -1;
				destroyed_facility_id = -1;
				destroyed_building_pos.x = -1;
				destroyed_building_pos.y = -1;
				destroy_flag = false;
			}
		}
	}

	void 特殊地名设施_重建(pk::unit @unit, int &destroy_unit_id, int &destroyed_facility_id, pk::point &destroyed_building_pos, bool &destroy_flag, int call_id)
	{
		if (destroy_unit_id != -1 and destroyed_building_pos.x != -1)
		{
			if (destroy_unit_id == unit.get_id() and destroy_flag)
			{
				if (地名设施.find(destroyed_facility_id) >= 0)
				{
					pk::hex @hex0 = pk::get_hex(destroyed_building_pos);
					if (hex0.has_unit)
					{
						pk::kill(pk::get_unit(destroyed_building_pos));
					}
					if (hex0.has_building)
					{
						pk::kill(pk::get_building(destroyed_building_pos));
					}
					// pk::trace(pk::format("攻击部队id:{}.被摧毁建筑id：{}",destroy_unit.get_id(), destroyed_building.get_id()));
					// pk::trace(pk::format("建筑id:{}.建筑位置：{},{},攻击部队势力：{}",destroyed_building.facility, destroyed_building.pos.x, destroyed_building.pos.y, destroy_unit.get_force_id()));
					// auto pos = destroyed_building.pos;
					// pos.x = pos.x + 1
					pk::unit @destroy_unit = pk::get_unit(destroy_unit_id);
					pk::facility @destroy_facility = pk::get_facility(destroyed_facility_id);
					int destroy_force = -1;
					if (destroy_unit.get_force_id() < 非贼势力_末)
						destroy_force = destroy_unit.get_force_id();
					pk::create_effect(91, destroyed_building_pos);
					// pk::trace(pk::format("攻击部队id:{}. 重建势力id:{}",destroy_unit.get_id(),destroy_force));
					// pk::trace(pk::format("call_id:{}",call_id));
					// pk::trace(pk::format("x:{},y:{}.",destroyed_building_pos.x,destroyed_building_pos.y));
					// pk::trace(pk::format("设施类型：{}",destroy_facility.get_id()));
					// pk::trace(pk::format("重建势力id:{}",destroy_force));

					pk::building @building0 = pk::create_building(destroy_facility, destroyed_building_pos, destroy_force);
					pk::complete(building0);
					// 初始化
					destroy_unit_id = -1;
					destroyed_facility_id = -1;
					destroyed_building_pos.x = -1;
					destroyed_building_pos.y = -1;
					destroy_flag = false;
				}
			}
		}
	}

	bool is_valid_spec_id(int spec_id)
	{
		if (spec_id >= 0 and spec_id < get_spec_end())
			return true;
		return false;
	}

	int to_spec_id(int building_id)
	{
		if (building_id >= 100 and building_id < (100 + ch::get_spec_end()))
			return (building_id - 100);
		return -1;
	}

	int to_building_id(int spec_id)
	{
		if (is_valid_spec_id(spec_id))
			return (spec_id + 100);
		return -1;
	}

	pk::building@ get_spec_building(int spec_id)
	{
		int building_id = to_building_id(spec_id);
		return pk::get_building(building_id);
	}

	int get_spec_id(pk::building@ building)
	{
		if (building is null) return -1;
		int building_id = building.get_id();
		int spec_id = to_spec_id(building_id);
		return spec_id;
	}
	
	string get_spec_name(int spec_id)
	{
		if (pk::is_new_map())
		{
			return 特殊地名设施名称2[spec_id];
		}
		return 特殊地名设施名称[spec_id];
	}

	int get_spec_end()
	{
		if (pk::is_new_map())
		{
			return 地名_末2;
		}
		return 地名_末;
	}

	pk::point get_spec_pos(int spec_id)
	{
		if (pk::is_new_map())
		{
			return pk::point(特殊地名设施2[spec_id][1], 特殊地名设施2[spec_id][2]);
		}
		return pk::point(特殊地名设施[spec_id][1], 特殊地名设施[spec_id][2]);
	}

	int get_spec_facility_id(int spec_id)
	{
		if (true)
		{
			return 设施_城塞;
		}
		else
		{
			if (pk::is_new_map())
			{
				return  特殊地名设施2[spec_id][0];
			}
			return  特殊地名设施[spec_id][0];
		}

	}

	bool reset_spec(int spec_id)
	{
		// 清空封地信息
		specialinfo @spec_t = @special_ex[spec_id];
		if (pk::is_valid_person_id(spec_t.person))
			person_ex[spec_t.person].spec_id = -1; // 清空原来武将的对应记录
		spec_t.person = -1;
		spec_t.troops = -1;
		spec_t.gold = -1;
		spec_t.food = -1;

		pk::building @spec_building = pk::get_building(spec_id + 100);
		if (int(spec_building.hp) > pk::get_max_hp(spec_building))
			spec_building.hp = pk::get_max_hp(spec_building);
		return true;
	}

	int get_spec_person_id(int spec_id)
	{
		if (!is_valid_spec_id(spec_id)) return -1;
		int person_id = special_ex[spec_id].person;
		if (pk::is_valid_person_id(person_id)) return person_id;
		return -1;
	}

	pk::person@ get_spec_person(int spec_id)
	{
		return pk::get_person(get_spec_person_id(spec_id));
	}

	string get_weapon_name(int weapon_id)
	{
		auto equp = pk::get_equipment(weapon_id);
		if (pk::is_alive(equp)) return equp.name;
		return "---";
	}

	string get_person_name(pk::person@ person, bool decode = false)
	{
		if (pk::is_alive(person))
		{
			if (decode) return pk::decode(pk::get_name(person));
			return pk::get_name(person);
		}
		return "---";
	}

	string get_person_name(int person_id,bool decode = false)
	{
		auto person = pk::get_person(person_id);
		string str = "";
		if (pk::is_alive(person)) str = pk::get_name(person);
		else str = "---";
		if (decode) str = pk::decode(str);
		return str;
	}

	string get_spec_person_name(int spec_id)
	{
		pk::person@ person = get_spec_person(spec_id);
		return get_person_name(person, false);
	}

	int get_spec_force_id(int spec_id)
	{
		pk::building@ building = get_spec_building(spec_id);
		int force_id = building.init_force;
		return force_id;
	}

	pk::force@ get_spec_force(int spec_id)
	{
		int force_id = get_spec_force_id(spec_id);
		if (pk::is_valid_force_id(force_id))
		{
			return pk::get_force(force_id);
		}
		return null;
	}

	string get_spec_force_name(int spec_id)
	{
		pk::force@ force = get_spec_force(spec_id);
		if (force !is null)
			return pk::get_name(force);
		return "---";
	}

	int get_spec_location_id(int spec_id)
	{
		pk::point pos = get_spec_pos(spec_id);
		int city_id = pk::get_city_id(pos);
		return city_id;
	}

	pk::city@ get_spec_city(int spec_id)
	{
		return pk::get_city(get_spec_location_id(spec_id));
	}

	string get_spec_city_name(int spec_id)
	{
		pk::city@ city = get_spec_city(spec_id);
		if (city !is null)
			return pk::get_name(city);
		return "---";
	}

	array<int16> get_district_able_spec_int16(pk::district @district,bool empty = false)
	{
		array<int16> temp;
		for (int16 i = 0; i < ch::get_spec_end(); ++i)
		{
			pk::point pos0 = ch::get_spec_pos(i);
			pk::building @building0 = pk::get_building(pos0);
			int district_id = district.get_id();
			if (building0 is null)
				continue;
			//建筑无军团id，所以下方判断不可行，应该根据城市来
			//if (building0.get_district_id() != district_id)
			//	continue;
			if (empty)
			{
				specialinfo @spec_t = @special_ex[i];
				if (spec_t.person != -1)
					continue;
			}
			int city_id = pk::get_city_id(pos0);
			pk::city@ location_city = pk::get_city(city_id);
			if (!pk::is_alive(location_city) or location_city.get_district_id() != district_id) continue;//排除非己方城市所属封地，避免前线分封
			temp.insertLast(i);
		}
		return temp;
	}

	array<int16> get_force_able_spec_int16(pk::force @force, bool empty = false)
	{
		array<int16> temp;
		for (int16 i = 0; i < ch::get_spec_end(); ++i)
		{
			pk::point pos0 = ch::get_spec_pos(i);
			pk::building @building0 = pk::get_building(pos0);
			int force_id = force.get_id();
			if (building0 is null)
				continue;
			if (building0.get_force_id() != force_id)
				continue;
			if (empty)
			{
				specialinfo @spec_t = @special_ex[i];
				if (spec_t.person != -1)
					continue;
			}
			int city_id = pk::get_city_id(pos0);
			pk::city@ location_city = pk::get_city(city_id);
			if (!pk::is_alive(location_city) or location_city.get_force_id() != force_id) continue;//排除非己方城市所属封地，避免前线分封
			temp.insertLast(i);
		}
		return temp;
	}

	array<int16> get_city_able_spec_int16(pk::city @city, bool empty = false)
	{
		array<int16> temp;
		for (int16 i = 0; i < ch::get_spec_end(); ++i)
		{
			pk::point pos0 = ch::get_spec_pos(i);
			pk::building @building0 = pk::get_building(pos0);
			if (building0 is null)
				continue;
			if (building0.get_force_id() != city.get_force_id())
				continue;
			if (empty)
			{
				specialinfo @spec_t = @special_ex[i];
				if (spec_t.person != -1)
					continue;
			}
			int city_id = pk::get_city_id(pos0);
			if (city.get_id() != city_id)
				continue;
			temp.insertLast(i);
		}
		return temp;
	}

	//府资源难度修正
	float get_income_difficulty_inf(pk::building@ building)
	{
		float rate = 1.0f;
		int force_id = building.get_force_id();
		if (!pk::is_valid_force_id(force_id)) return rate;
		pk::force @force = pk::get_force(force_id);
		if (force.is_player())
		{
			rate = 0.8f;
		}
		else
		{
			switch (pk::get_scenario().difficulty)
			{
			case 难易度_超级:
				rate = 1.5f;
				break;

			case 难易度_上级:
				rate = 1.2f;
				break;

			case 难易度_初级:
				rate = 1.0f;
				break;
			}
		}
				
		return rate;
	}

	void set_person_weapon(int person_id, int weapon_id)
	{
		if (!pk::is_valid_person_id(person_id))
			return;
		pk::set_person_weapon(person_id, weapon_id);
		person_ex[person_id].weapon_id = weapon_id;
	}

	void set_person_horse(int person_id, int horse_id)
	{
		if (!pk::is_valid_person_id(person_id))
			return;
		pk::set_person_horse(person_id, horse_id);
		person_ex[person_id].horse_id = horse_id;
	}

	bool get_auto_affairs_status()
	{
		if (setting_ex.auto_affairs_status != pk::get_auto_affairs_status())
		{
			pk::set_auto_affairs(setting_ex.auto_affairs_status);
		}
		return setting_ex.auto_affairs_status;
	}

	void set_auto_affairs(bool value)
	{
		pk::set_auto_affairs(value);
		setting_ex.auto_affairs_status = value;
	}

	void toggle_auto_affairs()
	{
		setting_ex.auto_affairs_status = !setting_ex.auto_affairs_status;
		pk::set_auto_affairs(!setting_ex.auto_affairs_status);
	}

	void set_interior_land(int city_id)
	{
		int level = get_city_level(city_id);
		// 内政用地升级[city_id][level - 1][];
		for (int i = 1; i <= level; ++i)
		{
			set_interior_land(city_id, i);
		}
	}

	void set_interior_land(int city_id, int level)
	{
		if (level > 0 and level <= 4)
		{
			// int population = base_ex[city_id].population;

			// 内政用地升级[city_id][level - 1][];
			for (int j = 0; j < 2; ++j)
			{
				int index = 2 * j;
				pk::point pos1;
				if (pk::is_new_map())
				{
					pos1 = pk::point(内政用地升级2[city_id][level - 1][index], 内政用地升级2[city_id][level - 1][index + 1]);
				}
				else
				{
					pos1 = pk::point(内政用地升级[city_id][level - 1][index], 内政用地升级[city_id][level - 1][index + 1]);
					
				}
				int result = pk::add_interior_land(pos1);
				// 如果此地兵舍，厩舍，锻造厂，需刷新计数-----实际上是city_dev的building信息没更新，另外，如果城市人口减少导致内政用地减少，此信息目前只能在读档后更新
				//if (true)
					//pk::trace(city_id + ':' + pk::get_new_base_name(city_id) + " => level:" + (level - 1) + ";x:" + pos1.x + ",y:" + pos1.y + ",result:" + result + "index:" + index);
			}
		}
	}

	int get_city_level(int city_id)
	{
		int population = base_ex[city_id].population;
		int level = 0;
		if (population > 四级城市阈值)
			level = 4;
		else if (population > 三级城市阈值)
			level = 3;
		else if (population > 二级城市阈值)
			level = 2;
		else if (population > 一级城市阈值)
			level = 1;
		return level;
	}

	bool is_target_item_id(int item_id)
	{
		if (item_id < 0) return false;
		if (item_id > 414) return false;
		if (item_id > 76 and item_id < 100) return false;
		if (item_id > 155 and item_id < 200) return false;
		if (item_id > 240 and item_id < 300) return false;
		if (item_id > 328 and item_id < 370) return false;
		if (item_id > 373 and item_id < 385) return false;
		if (item_id > 389 and item_id < 400) return false;
		return true;
	}
	
	string get_level_string(int level)
	{
		string level_name;
		switch (level)
		{
		case 0:
			level_name = "微";
			break;
		case 1:
			level_name = "小";
			break;
		case 2:
			level_name = "中";
			break;
		case 3:
			level_name = "大";
			break;
		case 4:
			level_name = "巨";
			break;
		}
		return level_name;
	}
	/*
	const int 据点AI_开发 = 1;
const int 据点AI_征兵 = 2;
const int 据点AI_生产 = 3;
const int 据点AI_巡察 = 4;
const int 据点AI_交易 = 5; // ?
const int 据点AI_训练 = 6;
const int 据点AI_防御 = 7;
const int 据点AI_攻击 = 8;
const int 据点AI_设置 = 9;
const int 据点AI_关卡运输 = 10; // 都市关卡间运输도시 관문 간 수송?
const int 据点AI_都市运输 = 11; // 都市间运输도시 간 수송
const int 据点AI_移动 = 12;
const int 据点AI_召唤 = 13;
const int 据点AI_人才探索 = 14;
const int 据点AI_武将登用 = 15;
const int 据点AI_褒奖 = 16;
const int 据点AI_二虎竞食 = 17;
const int 据点AI_驱虎吞狼 = 18;
const int 据点AI_流言 = 19;
const int 据点AI_他势力武将登用 = 20;
const int 据点AI_拆除 = 21;
const int 据点AI_计略 = 22;
const int 据点AI_最小人才探索 = 23; // 武将 수가 难易度 값 不足时(预计失败时无条件不做). 인공지능.xml 참고 / 무장 수가 난이도 값 미만일 때만(실패 예상 시 무조건 안함). 인공지능.xml 참고
const int 据点AI_最小武将登用 = 24; // 무장 수가 난이도 값 미만일 때만(성공 예상 시 무조건 시도). 인공지능.xml 참고
const int 据点AI_最小他势力武将登用 = 25; // 무장 수가 3명 미만일 때만. 인공지능.xml 참고
const int 据点AI_交易2 = 26; // ?
const int 据点AI_吸收合并 = 27;
	*/

	string get_cmd_name(int cmd)
	{
		string cmd_name;
		switch (cmd)
		{
		case 1:
			cmd_name = "据点AI_开发 ";
			break;
		case 2:
			cmd_name = "据点AI_征兵 ";
			break;
		case 3:
			cmd_name = "据点AI_生产 ";
			break;
		case 4:
			cmd_name = "据点AI_巡察 ";
			break;
		case 5:
			cmd_name = "据点AI_交易 ";
			break;
		case 6:
			cmd_name = "据点AI_训练 ";
			break;
		case 7:
			cmd_name = "据点AI_防御 ";
			break;
		case 8:
			cmd_name = "据点AI_攻击 ";
			break;
		case 9:
			cmd_name = "据点AI_设置 ";
			break;
		case 10:
			cmd_name = "据点AI_关卡运输 ";
			break;
		case 11:
			cmd_name = "据点AI_都市运输 ";
			break;
		case 12:
			cmd_name = "据点AI_移动 ";
			break;
		case 13:
			cmd_name = "据点AI_召唤 ";
			break;
		case 14:
			cmd_name = "据点AI_人才探索 ";
			break;
		case 15:
			cmd_name = "据点AI_武将登用 ";
			break;
		case 16:
			cmd_name = "据点AI_褒奖 ";
			break;
		case 17:
			cmd_name = "据点AI_二虎竞食 ";
			break;
		case 18:
			cmd_name = "据点AI_驱虎吞狼 ";
			break;
		case 19:
			cmd_name = "据点AI_流言 ";
			break;
		case 20:
			cmd_name = "据点AI_他势力武将登用 ";
			break;
		case 21:
			cmd_name = "据点AI_拆除 ";
			break;
		case 22:
			cmd_name = "据点AI_计略 ";
			break;
		case 23:
			cmd_name = "据点AI_最小人才探索 ";
			break;
		case 24:
			cmd_name = "据点AI_最小武将登用 ";
			break;
		case 25:
			cmd_name = "据点AI_最小他势力武将登用 ";
			break;
		case 26:
			cmd_name = "据点AI_交易2 ";
			break;
		case 27:
			cmd_name = "据点AI_吸收合并 ";
			break;
		case 30:cmd_name = "据点AI_计略破坏  "; break;
		case 31:cmd_name = "据点AI_计略鼓舞  "; break;
		case 32:cmd_name = "据点AI_计略扰乱  "; break;
		case 33:cmd_name = "据点AI_计略攻心  "; break;
		case 34:cmd_name = "据点AI_计略援军  "; break;
		case 35:cmd_name = "据点AI_计略镇静  "; break;
		case 36:cmd_name = "据点AI_计略伪报  "; break;
		case 37:cmd_name = "据点AI_计略火攻  "; break;
		case 38:cmd_name = "据点AI_计略调查  "; break;
		case 39:cmd_name = "据点AI_计略刺杀  "; break;
		case 40:cmd_name = "据点AI_计略奇袭攻具  "; break;
		case 41:cmd_name = "据点AI_计略待劳  "; break;
		case 50:cmd_name = "据点AI_募兵  "; break;
		case 51:cmd_name = "据点AI_民兵  "; break;
		case 52:cmd_name = "据点AI_招募雇佣兵  "; break;
		case 53:cmd_name = "据点AI_护送俘虏  "; break;
		case 54:cmd_name = "据点AI_屏障建设  "; break;
		case 55:cmd_name = "据点AI_城墙维修  "; break;
		case 56:cmd_name = "据点AI_部队奇袭攻具  "; break;
		case 57:cmd_name = "据点AI_支援关卡  "; break;
		case 60:cmd_name = "据点AI_紧急动员  "; break;
		case 100:cmd_name = "据点AI_联合军出征  "; break;
		case 101:cmd_name = "据点AI_分封  "; break;
		case 102:cmd_name = "据点AI_解除分封  "; break;
		case 200:cmd_name = "据点AI_末  "; break;
		default:
			cmd_name = "??";
			break;
		}
		return cmd_name;
	}

	uint get_stat_color(int type)
	{
		switch (type)
		{
		case 0:
			return 0xffffabc1;
		case 1:
			return 0xffc49df5;
		case 2:
			return 0xffd2d25f;
		case 3:
			return 0xff56dbaf;
		case 4:
			return 0xff80d1fe;
		}
		return 0xffffffff;
	}

	string get_unit_stat_name(int stat_type)
	{
		/*
		string color_text = "";
		switch (stat_type)
		{
		case 部队能力_攻击:
			color_text = "\x1b[27x"; break;
		case 部队能力_防御:
			color_text = "\x1b[27x"; break;
		case 部队能力_智力:
			color_text = "\x1b[18x"; break;
		case 部队能力_建设:
			color_text = "\x1b[28x"; break;
		case 部队能力_移动:
			color_text = "\x1b[32x"; break;
		}*/
		switch (stat_type)
		{
		case 0:
			return "攻击";
		case 1:
			return "防御";
		case 2:
			return "智力";
		case 3:
			return "建设";
		case 4:
			return "移动";
		}
		return "";
	}

	string get_stat_name(int stat_type, bool color = false)
	{

		switch (stat_type)
		{
		case 武将能力_统率:
			return "统率";
		case 武将能力_武力:
			return "武力";
		case 武将能力_智力:
			return "智力";
		case 武将能力_政治:
			return "政治";
		case 武将能力_魅力:
			return "魅力";
		}
		return "";
	}

	string get_tekisei_name(int tekisei_type)
	{
		switch (tekisei_type)
		{
		case 兵种_枪兵:
			return "枪兵";
		case 兵种_戟兵:
			return "戟兵";
		case 兵种_弩兵:
			return "弩兵";
		case 兵种_骑兵:
			return "骑兵";
		case 兵种_兵器:
			return "兵器";
		case 兵种_水军:
			return "水军";
		}
		return "";
	}


	//作为对话框背景模板，可随意调节大小
	pk::button@ create_bg_dialog(pk::dialog@dialog, pk::size size, string& title)
	{
		pk::assert(size.width >= 301);
		pk::assert(size.height >= 16);

		pk::widget@ w = dialog.create_sprite(70);//(70);//70是半透明
		w.set_pos(12, 54);
		w.set_size(size);
		@ w = dialog.create_sprite(499);//(70);//70是半透明
		w.set_pos(12, 54);
		w.set_size(size);

		@w = dialog.create_image(320);
		//dialog.add_titlebar(w.get_id(), 320);

		pk::text@t = dialog.create_text();
		t.set_pos(40, 22);
		t.set_size(pk::get_text_size(FONT_BIG, title));
		t.set_text(title);
		t.set_text_font(FONT_BIG);

		@w = dialog.create_sprite(430);
		//dialog.add_titlebar(w.get_id(), 430);
		w.set_pos(284, 36);
		w.set_size(size.width - 301, w.get_size().height);

		@w = dialog.create_image(321);
		w.set_pos(size.width - 17, 36);
		//dialog.add_titlebar(w.get_id(), 320);

		@w = dialog.create_sprite(432);
		w.set_pos(5, 64);
		w.set_size(w.get_size().width, size.height - 16);

		@w = dialog.create_sprite(431);
		w.set_pos(size.width + 11, 64);
		w.set_size(w.get_size().width, size.height - 16);

		@w = dialog.create_image(323);
		w.set_pos(5, size.height + 48);

		@w = dialog.create_sprite(433);
		w.set_pos(257, size.height + 48);
		w.set_size(size.width - 298, w.get_size().height);

		@w = dialog.create_image(322);
		w.set_pos(size.width - 41, size.height + 48);

		pk::button@ok_button = dialog.create_button(172);
		ok_button.set_pos(27, size.height + 58);
		ok_button.set_text(pk::encode("决定"));

		dialog.resize();

		return ok_button;
	}


	void limit_skill_set(pk::person@person, int min_luck = 0/*作为技能保底下限*/)
	{
		//array<array<uint8>> 可学特技;// (4, array<uint8>(160, 255));
		array<uint8>可学特技level_1;
		array<uint8>可学特技level_2;
		array<uint8>可学特技level_3;
		array<uint8>可学特技level_4;
		for (int i = 0; i < 160; ++i)
		{
			bool include = true;
			for (int j = 0; j < 11; ++j)
			{
				if (j < 武将能力_末)
				{
					if (person.base_stat[j] < 特技需求[i][j])
					{
						include = false;
						//pk::trace("特技id" + i + "能力：" + person.base_stat[j] + "不满足需求：" + 特技需求[i][j]);
						break;
					}
				}
				else
				{
					if (uint8(person.tekisei[j - 武将能力_末]) < 特技需求[i][j])
					{
						//pk::trace("特技id" + i + "适性：" + person.tekisei[j - 武将能力_末] + "不满足需求：" + 特技需求[i][j]);
						include = false;
						break;
					}
				}
			}
			if (i == 特技_内助 and person.sex == 性别_男) { include = false; }
			if (include)
			{
				pk::skill@ skill = pk::get_skill(i);
				if (pk::is_alive(skill))
				{
					int level = skill.level - 1;
					//if (调试模式) pk::trace("特技:" + pk::decode(skill.name) + "level" + skill.level);
					switch (level)
					{
					case 0: 可学特技level_1.insertLast(i);
					case 1: 可学特技level_2.insertLast(i);
					case 2: 可学特技level_3.insertLast(i);
					case 3: 可学特技level_4.insertLast(i);
					}
				}
			}
		}
		if (可学特技level_1.length == 0) 可学特技level_1.insertLast(255);
		int lucky = ch::randint(min_luck, 100);//pk::rand(100) + min_luck;
		int 二级特技阈值 = 一级特技概率;
		int 三级特技阈值 = 一级特技概率 + 二级特技概率;
		int 四级特技阈值 = 一级特技概率 + 二级特技概率 + 三级特技概率;
		if (可学特技level_3.length > 0)
		{
			if (可学特技level_4.length > 0)
			{
				//if (调试模式) pk::trace("可学特技level_1.length" + 可学特技level_1.length);
				if (lucky > 四级特技阈值) person.skill = 可学特技level_4[pk::rand(可学特技level_4.length)];
				else if (lucky > 三级特技阈值) person.skill = 可学特技level_3[pk::rand(可学特技level_3.length)];
				else if (lucky > 二级特技阈值) person.skill = 可学特技level_2[pk::rand(可学特技level_2.length)];
				else person.skill = 可学特技level_1[pk::rand(可学特技level_1.length)];
			}
			else
			{
				//if (调试模式) pk::trace("可学特技level_1.length" + 可学特技level_1.length);
				if (lucky > 三级特技阈值) person.skill = 可学特技level_3[pk::rand(可学特技level_3.length)];
				else if (lucky > 二级特技阈值) person.skill = 可学特技level_2[pk::rand(可学特技level_2.length)];
				else person.skill = 可学特技level_1[pk::rand(可学特技level_1.length)];
			}
		}
		else
		{
			//if (调试模式) pk::trace("可学特技level_1.length" + 可学特技level_1.length);
			if (lucky > 二级特技阈值) person.skill = 可学特技level_2[pk::rand(可学特技level_2.length)];
			else person.skill = 可学特技level_1[pk::rand(可学特技level_1.length)];
		}


	}

	const array<array<uint8>> 特技需求 = {
{/*000飞将*//*统率*/80,/*武力*/100,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/2,/*弩*/2,/*马*/4,/*兵器*/0,/*水军*/0},
{/*001遁走*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/80,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*002强行*//*统率*/70,/*武力*/0,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*003长驱*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/2,/*兵器*/0,/*水军*/0},
{/*004推进*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/2},
{/*005操舵*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/2},
{/*006踏破*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*007运输*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*008用毒*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*009扫讨*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*010威风*//*统率*/80,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*011昂扬*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*012连战*//*统率*/80,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/3,/*戟*/3,/*弩*/2,/*马*/3,/*兵器*/0,/*水军*/0},
{/*013急袭*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*014强袭*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/2},
{/*015乱战*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/1,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*016待伏*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*017攻城*//*统率*/70,/*武力*/70,/*智力*/60,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/3,/*水军*/0},
{/*018掎角*//*统率*/60,/*武力*/0,/*智力*/70,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*019擒拿*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*020精妙*//*统率*/70,/*武力*/0,/*智力*/80,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*021强夺*//*统率*/70,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*022攻心*//*统率*/80,/*武力*/80,/*智力*/70,/*政治*/0,/*魅力*/70,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*023驱逐*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/1,/*弩*/0,/*马*/2,/*兵器*/0,/*水军*/0},
{/*024射程*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/3,/*水军*/0},
{/*025骑射*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/3,/*兵器*/0,/*水军*/0},
{/*026辅佐*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/70,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*027不屈*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*028金刚*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/2,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*029铁壁*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/2,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*030怒发*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*031藤甲*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*032幸运*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*033血路*//*统率*/70,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*034枪将*//*统率*/70,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/3,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*035戟将*//*统率*/70,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/3,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*036弓将*//*统率*/70,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/3,/*马*/0,/*兵器*/0,/*水军*/0},
{/*037骑将*//*统率*/70,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/3,/*兵器*/0,/*水军*/0},
{/*038水将*//*统率*/70,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/3},
{/*039勇将*//*统率*/70,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/0,/*弩*/2,/*马*/0,/*兵器*/0,/*水军*/3},
{/*040神将*//*统率*/90,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/3,/*弩*/2,/*马*/2,/*兵器*/0,/*水军*/0},
{/*041斗神*//*统率*/80,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/3,/*戟*/3,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*042枪神*//*统率*/80,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/4,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*043戟神*//*统率*/80,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/4,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*044弓神*//*统率*/80,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/4,/*马*/0,/*兵器*/0,/*水军*/0},
{/*045骑神*//*统率*/80,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/4,/*兵器*/0,/*水军*/0},
{/*046工神*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/4,/*水军*/0},
{/*047水神*//*统率*/80,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/4},
{/*048霸王*//*统率*/90,/*武力*/100,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/2,/*弩*/2,/*马*/4,/*兵器*/0,/*水军*/2},
{/*049疾驰*//*统率*/70,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/3,/*兵器*/0,/*水军*/0},
{/*050射手*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/2,/*马*/0,/*兵器*/0,/*水军*/0},
{/*051猛者*//*统率*/0,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*052护卫*//*统率*/0,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*053火攻*//*统率*/0,/*武力*/0,/*智力*/70,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*054言毒*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*055机略*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*056诡计*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*057虚实*//*统率*/0,/*武力*/0,/*智力*/95,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*058妙计*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*059秘计*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*060看破*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*061洞察*//*统率*/80,/*武力*/80,/*智力*/70,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*062火神*//*统率*/0,/*武力*/0,/*智力*/95,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*063神算*//*统率*/0,/*武力*/0,/*智力*/100,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*064百出*//*统率*/0,/*武力*/0,/*智力*/85,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*065鬼谋*//*统率*/0,/*武力*/0,/*智力*/95,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*066连环*//*统率*/0,/*武力*/0,/*智力*/95,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*067深谋*//*统率*/0,/*武力*/0,/*智力*/95,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*068反计*//*统率*/0,/*武力*/0,/*智力*/90,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*069奇谋*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*070妖术*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*071鬼门*//*统率*/0,/*武力*/0,/*智力*/95,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*072规律*//*统率*/0,/*武力*/0,/*智力*/75,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*073沉着*//*统率*/0,/*武力*/0,/*智力*/75,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*074明镜*//*统率*/0,/*武力*/0,/*智力*/85,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*075奏乐*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/0,/*魅力*/90,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*076诗想*//*统率*/0,/*武力*/0,/*智力*/70,/*政治*/70,/*魅力*/70,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*077筑城*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/80,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*078屯田*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/80,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*079名声*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/0,/*魅力*/90,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*080能吏*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/80,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*081繁殖*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/80,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*082发明*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/80,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*083造船*//*统率*/0,/*武力*/0,/*智力*/70,/*政治*/70,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*084指导*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/80,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*085眼力*//*统率*/0,/*武力*/0,/*智力*/70,/*政治*/70,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*086论客*//*统率*/0,/*武力*/0,/*智力*/85,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*087积蓄*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/90,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*088劝农*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/90,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*089度支*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/90,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*090平准*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/90,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*091亲乌*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/3,/*兵器*/0,/*水军*/0},
{/*092亲羌*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/3,/*兵器*/0,/*水军*/0},
{/*093亲越*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/3,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*094亲蛮*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/3,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*095威压*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/3,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*096仁政*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/80,/*魅力*/90,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*097风水*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/80,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*098祈愿*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/70,/*魅力*/70,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*099内助*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*100八卦*//*统率*/90,/*武力*/0,/*智力*/90,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*101窥探*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*102内治*//*统率*/0,/*武力*/0,/*智力*/70,/*政治*/70,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*103戍防*//*统率*/70,/*武力*/70,/*智力*/60,/*政治*/60,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*104韬略*//*统率*/0,/*武力*/0,/*智力*/90,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*105谋略*//*统率*/0,/*武力*/0,/*智力*/90,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*106军略*//*统率*/80,/*武力*/80,/*智力*/80,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*107强募*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/0,/*魅力*/90,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*128蓄势*//*统率*/80,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*109猛骑*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/3,/*兵器*/0,/*水军*/0},
{/*110猛卒*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/2,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*111严整*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/70,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*112辎粮*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/80,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*113山战*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/2,/*弩*/2,/*马*/0,/*兵器*/0,/*水军*/0},
{/*114背水*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/1,/*戟*/1,/*弩*/1,/*马*/1,/*兵器*/0,/*水军*/0},
{/*115清野*//*统率*/70,/*武力*/70,/*智力*/60,/*政治*/70,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*116拱戍*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*117布阵*//*统率*/0,/*武力*/0,/*智力*/90,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*118要击*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*119商才*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/70,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*120兵圣*//*统率*/95,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/4,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*121兵神*//*统率*/95,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*122兵心*//*统率*/95,/*武力*/0,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*123战神*//*统率*/90,/*武力*/100,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/2,/*弩*/2,/*马*/2,/*兵器*/0,/*水军*/0},
{/*124战将*//*统率*/80,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/2,/*弩*/2,/*马*/2,/*兵器*/0,/*水军*/0},
{/*125巧变*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/1,/*戟*/1,/*弩*/1,/*马*/1,/*兵器*/0,/*水军*/0},
{/*126游侠*//*统率*/70,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*127百战*//*统率*/90,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/2,/*弩*/2,/*马*/2,/*兵器*/0,/*水军*/0},
{/*108狙击*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/3,/*马*/0,/*兵器*/0,/*水军*/0},
{/*129神臂*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/4,/*马*/0,/*兵器*/0,/*水军*/0},
{/*130治军*//*统率*/90,/*武力*/0,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*131坚城*//*统率*/80,/*武力*/80,/*智力*/60,/*政治*/60,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*132料敌*//*统率*/0,/*武力*/0,/*智力*/95,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*133陷阵*//*统率*/80,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*134恫吓*//*统率*/80,/*武力*/100,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*135破竹*//*统率*/90,/*武力*/95,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/2,/*弩*/2,/*马*/3,/*兵器*/0,/*水军*/0},
{/*136安民*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/0,/*魅力*/95,/*枪*/1,/*戟*/1,/*弩*/1,/*马*/1,/*兵器*/2,/*水军*/2},
{/*137循吏*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/90,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*138贤哲*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/90,/*魅力*/90,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*139变法*//*统率*/0,/*武力*/0,/*智力*/90,/*政治*/90,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*140法治*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/95,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*141后勤*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/80,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*142圣人*//*统率*/0,/*武力*/0,/*智力*/70,/*政治*/80,/*魅力*/80,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*141亲和*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/80,/*魅力*/95,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*144运筹*//*统率*/0,/*武力*/0,/*智力*/100,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*145激励*//*统率*/90,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/2,/*弩*/2,/*马*/2,/*兵器*/0,/*水军*/0},
{/*146伯乐*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/80,/*魅力*/90,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*147清谈*//*统率*/0,/*武力*/0,/*智力*/70,/*政治*/70,/*魅力*/80,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*148军魂*//*统率*/95,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/2,/*戟*/2,/*弩*/2,/*马*/2,/*兵器*/1,/*水军*/1},
{/*149武干*//*统率*/95,/*武力*/0,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*150摧锋*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/3,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*151重甲*//*统率*/80,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/3,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*152轻甲*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/3,/*马*/0,/*兵器*/0,/*水军*/0},
{/*153掠阵*//*统率*/80,/*武力*/90,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/4,/*兵器*/0,/*水军*/0},
{/*154重器*//*统率*/0,/*武力*/0,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/3,/*水军*/0},
{/*155水师*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/3},
{/*156调练*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*157巡查*//*统率*/70,/*武力*/70,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*158劫财*//*统率*/0,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/1,/*戟*/1,/*弩*/1,/*马*/1,/*兵器*/0,/*水军*/0},
{/*159截粮*//*统率*/0,/*武力*/80,/*智力*/0,/*政治*/0,/*魅力*/0,/*枪*/1,/*戟*/1,/*弩*/1,/*马*/1,/*兵器*/0,/*水军*/0},
{/*160育民*//*统率*/0,/*武力*/0,/*智力*/70,/*政治*/80,/*魅力*/80,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*161医者*//*统率*/0,/*武力*/0,/*智力*/80,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0},
{/*162扰敌*//*统率*/0,/*武力*/0,/*智力*/95,/*政治*/0,/*魅力*/0,/*枪*/0,/*戟*/0,/*弩*/0,/*马*/0,/*兵器*/0,/*水军*/0}
	};

	const array<array<array<int>>> 内政用地升级 = {
{/*襄平*/{/*级别1*//*x*/179,/*y*/21,/*级别1*//*x*/180,/*y*/22},{/*级别2*//*x*/180,/*y*/21,/*级别2*//*x*/179,/*y*/20},{/*级别3*//*x*/178,/*y*/20,/*级别3*//*x*/177,/*y*/19},{/*级别4*//*x*/176,/*y*/20,/*级别4*//*x*/175,/*y*/20}},
{/*北平*/{/*级别1*//*x*/150,/*y*/30,/*级别1*//*x*/150,/*y*/27},{/*级别2*//*x*/149,/*y*/26,/*级别2*//*x*/148,/*y*/26},{/*级别3*//*x*/147,/*y*/26,/*级别3*//*x*/146,/*y*/27},{/*级别4*//*x*/150,/*y*/26,/*级别4*//*x*/149,/*y*/25}},
{/*蓟*/{/*级别1*//*x*/123,/*y*/24,/*级别1*//*x*/130,/*y*/6},{/*级别2*//*x*/130,/*y*/7,/*级别2*//*x*/129,/*y*/5},{/*级别3*//*x*/129,/*y*/6,/*级别3*//*x*/128,/*y*/5},{/*级别4*//*x*/128,/*y*/6,/*级别4*//*x*/127,/*y*/4}},
{/*南皮*/{/*级别1*//*x*/141,/*y*/31,/*级别1*//*x*/140,/*y*/33},{/*级别2*//*x*/129,/*y*/35,/*级别2*//*x*/130,/*y*/36},{/*级别3*//*x*/129,/*y*/36,/*级别3*//*x*/130,/*y*/37},{/*级别4*//*x*/129,/*y*/37,/*级别4*//*x*/130,/*y*/38}},
{/*平原*/{/*级别1*//*x*/123,/*y*/56,/*级别1*//*x*/124,/*y*/57},{/*级别2*//*x*/125,/*y*/57,/*级别2*//*x*/126,/*y*/58},{/*级别3*//*x*/123,/*y*/55,/*级别3*//*x*/124,/*y*/56},{/*级别4*//*x*/125,/*y*/56,/*级别4*//*x*/126,/*y*/57}},
{/*晋阳*/{/*级别1*//*x*/101,/*y*/41,/*级别1*//*x*/100,/*y*/41},{/*级别2*//*x*/99,/*y*/41,/*级别2*//*x*/98,/*y*/41},{/*级别3*//*x*/97,/*y*/40,/*级别3*//*x*/101,/*y*/40},{/*级别4*//*x*/100,/*y*/40,/*级别4*//*x*/99,/*y*/40}},
{/*邺*/{/*级别1*//*x*/114,/*y*/43,/*级别1*//*x*/114,/*y*/42},{/*级别2*//*x*/114,/*y*/41,/*级别2*//*x*/114,/*y*/40},{/*级别3*//*x*/114,/*y*/39,/*级别3*//*x*/115,/*y*/38},{/*级别4*//*x*/113,/*y*/43,/*级别4*//*x*/113,/*y*/42}},
{/*北海*/{/*级别1*//*x*/170,/*y*/47,/*级别1*//*x*/170,/*y*/48},{/*级别2*//*x*/170,/*y*/49,/*级别2*//*x*/170,/*y*/50},{/*级别3*//*x*/169,/*y*/46,/*级别3*//*x*/169,/*y*/47},{/*级别4*//*x*/169,/*y*/48,/*级别4*//*x*/169,/*y*/49}},
{/*广陵*/{/*级别1*//*x*/182,/*y*/96,/*级别1*//*x*/182,/*y*/95},{/*级别2*//*x*/182,/*y*/94,/*级别2*//*x*/182,/*y*/93},{/*级别3*//*x*/181,/*y*/96,/*级别3*//*x*/181,/*y*/95},{/*级别4*//*x*/181,/*y*/94,/*级别4*//*x*/181,/*y*/93}},
{/*下邳*/{/*级别1*//*x*/158,/*y*/64,/*级别1*//*x*/158,/*y*/67},{/*级别2*//*x*/157,/*y*/67,/*级别2*//*x*/159,/*y*/67},{/*级别3*//*x*/155,/*y*/65,/*级别3*//*x*/155,/*y*/66},{/*级别4*//*x*/155,/*y*/67,/*级别4*//*x*/156,/*y*/68}},
{/*寿春*/{/*级别1*//*x*/148,/*y*/92,/*级别1*//*x*/147,/*y*/91},{/*级别2*//*x*/147,/*y*/92,/*级别2*//*x*/147,/*y*/93},{/*级别3*//*x*/147,/*y*/94,/*级别3*//*x*/147,/*y*/95},{/*级别4*//*x*/147,/*y*/96,/*级别4*//*x*/147,/*y*/97}},
{/*濮阳*/{/*级别1*//*x*/142,/*y*/68,/*级别1*//*x*/143,/*y*/68},{/*级别2*//*x*/144,/*y*/68,/*级别2*//*x*/143,/*y*/67},{/*级别3*//*x*/141,/*y*/67,/*级别3*//*x*/142,/*y*/67},{/*级别4*//*x*/144,/*y*/67,/*级别4*//*x*/145,/*y*/67}},
{/*陈留*/{/*级别1*//*x*/113,/*y*/73,/*级别1*//*x*/113,/*y*/72},{/*级别2*//*x*/111,/*y*/73,/*级别2*//*x*/112,/*y*/73},{/*级别3*//*x*/111,/*y*/72,/*级别3*//*x*/110,/*y*/73},{/*级别4*//*x*/110,/*y*/74,/*级别4*//*x*/110,/*y*/75}},
{/*许昌*/{/*级别1*//*x*/94,/*y*/96,/*级别1*//*x*/95,/*y*/97},{/*级别2*//*x*/95,/*y*/96,/*级别2*//*x*/95,/*y*/95},{/*级别3*//*x*/94,/*y*/95,/*级别3*//*x*/93,/*y*/94},{/*级别4*//*x*/93,/*y*/93,/*级别4*//*x*/94,/*y*/94}},
{/*汝南*/{/*级别1*//*x*/113,/*y*/102,/*级别1*//*x*/115,/*y*/112},{/*级别2*//*x*/114,/*y*/113,/*级别2*//*x*/114,/*y*/112},{/*级别3*//*x*/113,/*y*/112,/*级别3*//*x*/113,/*y*/111},{/*级别4*//*x*/112,/*y*/112,/*级别4*//*x*/112,/*y*/111}},
{/*洛阳*/{/*级别1*//*x*/76,/*y*/79,/*级别1*//*x*/82,/*y*/79},{/*级别2*//*x*/75,/*y*/79,/*级别2*//*x*/75,/*y*/80},{/*级别3*//*x*/83,/*y*/79,/*级别3*//*x*/83,/*y*/80},{/*级别4*//*x*/74,/*y*/79,/*级别4*//*x*/83,/*y*/78}},
{/*宛*/{/*级别1*//*x*/87,/*y*/95,/*级别1*//*x*/87,/*y*/96},{/*级别2*//*x*/86,/*y*/97,/*级别2*//*x*/85,/*y*/97},{/*级别3*//*x*/86,/*y*/94,/*级别3*//*x*/87,/*y*/94},{/*级别4*//*x*/82,/*y*/98,/*级别4*//*x*/83,/*y*/98}},
{/*长安*/{/*级别1*//*x*/52,/*y*/81,/*级别1*//*x*/54,/*y*/81},{/*级别2*//*x*/53,/*y*/80,/*级别2*//*x*/55,/*y*/80},{/*级别3*//*x*/52,/*y*/80,/*级别3*//*x*/56,/*y*/80},{/*级别4*//*x*/53,/*y*/79,/*级别4*//*x*/55,/*y*/79}},
{/*上庸*/{/*级别1*//*x*/51,/*y*/96,/*级别1*//*x*/52,/*y*/97},{/*级别2*//*x*/53,/*y*/97,/*级别2*//*x*/54,/*y*/98},{/*级别3*//*x*/55,/*y*/98,/*级别3*//*x*/56,/*y*/98},{/*级别4*//*x*/53,/*y*/93,/*级别4*//*x*/54,/*y*/94}},
{/*安定*/{/*级别1*//*x*/30,/*y*/66,/*级别1*//*x*/31,/*y*/65},{/*级别2*//*x*/31,/*y*/64,/*级别2*//*x*/32,/*y*/65},{/*级别3*//*x*/31,/*y*/63,/*级别3*//*x*/32,/*y*/64},{/*级别4*//*x*/31,/*y*/62,/*级别4*//*x*/32,/*y*/63}},
{/*天水*/{/*级别1*//*x*/3,/*y*/67,/*级别1*//*x*/4,/*y*/69},{/*级别2*//*x*/4,/*y*/68,/*级别2*//*x*/4,/*y*/67},{/*级别3*//*x*/4,/*y*/66,/*级别3*//*x*/4,/*y*/64},{/*级别4*//*x*/4,/*y*/63,/*级别4*//*x*/4,/*y*/62}},
{/*武威*/{/*级别1*//*x*/5,/*y*/34,/*级别1*//*x*/5,/*y*/33},{/*级别2*//*x*/3,/*y*/28,/*级别2*//*x*/3,/*y*/29},{/*级别3*//*x*/4,/*y*/29,/*级别3*//*x*/4,/*y*/30},{/*级别4*//*x*/5,/*y*/29,/*级别4*//*x*/5,/*y*/30}},
{/*建业*/{/*级别1*//*x*/193,/*y*/120,/*级别1*//*x*/193,/*y*/126},{/*级别2*//*x*/194,/*y*/121,/*级别2*//*x*/194,/*y*/126},{/*级别3*//*x*/194,/*y*/120,/*级别3*//*x*/194,/*y*/127},{/*级别4*//*x*/193,/*y*/127,/*级别4*//*x*/194,/*y*/128}},
{/*吴*/{/*级别1*//*x*/153,/*y*/138,/*级别1*//*x*/153,/*y*/141},{/*级别2*//*x*/152,/*y*/142,/*级别2*//*x*/154,/*y*/142},{/*级别3*//*x*/151,/*y*/138,/*级别3*//*x*/152,/*y*/138},{/*级别4*//*x*/154,/*y*/138,/*级别4*//*x*/155,/*y*/138}},
{/*会稽*/{/*级别1*//*x*/144,/*y*/170,/*级别1*//*x*/145,/*y*/168},{/*级别2*//*x*/145,/*y*/169,/*级别2*//*x*/145,/*y*/170},{/*级别3*//*x*/142,/*y*/170,/*级别3*//*x*/143,/*y*/170},{/*级别4*//*x*/144,/*y*/171,/*级别4*//*x*/145,/*y*/171}},
{/*庐江*/{/*级别1*//*x*/140,/*y*/119,/*级别1*//*x*/141,/*y*/119},{/*级别2*//*x*/141,/*y*/120,/*级别2*//*x*/141,/*y*/121},{/*级别3*//*x*/147,/*y*/115,/*级别3*//*x*/147,/*y*/116},{/*级别4*//*x*/148,/*y*/115,/*级别4*//*x*/148,/*y*/116}},
{/*柴桑*/{/*级别1*//*x*/110,/*y*/140,/*级别1*//*x*/110,/*y*/141},{/*级别2*//*x*/110,/*y*/142,/*级别2*//*x*/111,/*y*/142},{/*级别3*//*x*/110,/*y*/143,/*级别3*//*x*/115,/*y*/138},{/*级别4*//*x*/115,/*y*/139,/*级别4*//*x*/115,/*y*/140}},
{/*江夏*/{/*级别1*//*x*/99,/*y*/119,/*级别1*//*x*/100,/*y*/120},{/*级别2*//*x*/101,/*y*/119,/*级别2*//*x*/102,/*y*/120},{/*级别3*//*x*/100,/*y*/116,/*级别3*//*x*/101,/*y*/116},{/*级别4*//*x*/102,/*y*/117,/*级别4*//*x*/103,/*y*/117}},
{/*河内*/{/*级别1*//*x*/73,/*y*/54,/*级别1*//*x*/72,/*y*/55},{/*级别2*//*x*/71,/*y*/54,/*级别2*//*x*/70,/*y*/53},{/*级别3*//*x*/70,/*y*/52,/*级别3*//*x*/70,/*y*/51},{/*级别4*//*x*/70,/*y*/50,/*级别4*//*x*/70,/*y*/49}},
{/*襄阳*/{/*级别1*//*x*/74,/*y*/125,/*级别1*//*x*/75,/*y*/124},{/*级别2*//*x*/75,/*y*/123,/*级别2*//*x*/75,/*y*/122},{/*级别3*//*x*/75,/*y*/121,/*级别3*//*x*/75,/*y*/120},{/*级别4*//*x*/72,/*y*/125,/*级别4*//*x*/73,/*y*/125}},
{/*江陵*/{/*级别1*//*x*/85,/*y*/142,/*级别1*//*x*/88,/*y*/141},{/*级别2*//*x*/84,/*y*/143,/*级别2*//*x*/89,/*y*/140},{/*级别3*//*x*/85,/*y*/141,/*级别3*//*x*/87,/*y*/140},{/*级别4*//*x*/84,/*y*/142,/*级别4*//*x*/88,/*y*/140}},
{/*长沙*/{/*级别1*//*x*/107,/*y*/153,/*级别1*//*x*/107,/*y*/154},{/*级别2*//*x*/107,/*y*/155,/*级别2*//*x*/107,/*y*/156},{/*级别3*//*x*/106,/*y*/153,/*级别3*//*x*/106,/*y*/154},{/*级别4*//*x*/107,/*y*/152,/*级别4*//*x*/108,/*y*/152}},
{/*武陵*/{/*级别1*//*x*/78,/*y*/149,/*级别1*//*x*/82,/*y*/149},{/*级别2*//*x*/79,/*y*/148,/*级别2*//*x*/81,/*y*/148},{/*级别3*//*x*/78,/*y*/151,/*级别3*//*x*/79,/*y*/151},{/*级别4*//*x*/80,/*y*/152,/*级别4*//*x*/79,/*y*/152}},
{/*桂阳*/{/*级别1*//*x*/93,/*y*/181,/*级别1*//*x*/102,/*y*/195},{/*级别2*//*x*/103,/*y*/194,/*级别2*//*x*/103,/*y*/193},{/*级别3*//*x*/102,/*y*/194,/*级别3*//*x*/101,/*y*/194},{/*级别4*//*x*/101,/*y*/193,/*级别4*//*x*/100,/*y*/194}},
{/*零陵*/{/*级别1*//*x*/72,/*y*/190,/*级别1*//*x*/73,/*y*/189},{/*级别2*//*x*/71,/*y*/189,/*级别2*//*x*/75,/*y*/189},{/*级别3*//*x*/70,/*y*/190,/*级别3*//*x*/76,/*y*/190},{/*级别4*//*x*/69,/*y*/190,/*级别4*//*x*/77,/*y*/190}},
{/*永安*/{/*级别1*//*x*/40,/*y*/128,/*级别1*//*x*/48,/*y*/133},{/*级别2*//*x*/47,/*y*/132,/*级别2*//*x*/49,/*y*/132},{/*级别3*//*x*/48,/*y*/132,/*级别3*//*x*/49,/*y*/131},{/*级别4*//*x*/50,/*y*/131,/*级别4*//*x*/50,/*y*/132}},
{/*汉中*/{/*级别1*//*x*/30,/*y*/93,/*级别1*//*x*/20,/*y*/93},{/*级别2*//*x*/19,/*y*/92,/*级别2*//*x*/21,/*y*/92},{/*级别3*//*x*/19,/*y*/91,/*级别3*//*x*/20,/*y*/92},{/*级别4*//*x*/20,/*y*/91,/*级别4*//*x*/21,/*y*/91}},
{/*梓潼*/{/*级别1*//*x*/12,/*y*/108,/*级别1*//*x*/13,/*y*/107},{/*级别2*//*x*/13,/*y*/106,/*级别2*//*x*/14,/*y*/107},{/*级别3*//*x*/13,/*y*/105,/*级别3*//*x*/14,/*y*/106},{/*级别4*//*x*/13,/*y*/104,/*级别4*//*x*/14,/*y*/105}},
{/*江州*/{/*级别1*//*x*/36,/*y*/146,/*级别1*//*x*/37,/*y*/151},{/*级别2*//*x*/36,/*y*/151,/*级别2*//*x*/35,/*y*/150},{/*级别3*//*x*/35,/*y*/149,/*级别3*//*x*/35,/*y*/148},{/*级别4*//*x*/35,/*y*/147,/*级别4*//*x*/35,/*y*/146}},
{/*成都*/{/*级别1*//*x*/20,/*y*/132,/*级别1*//*x*/19,/*y*/130},{/*级别2*//*x*/20,/*y*/131,/*级别2*//*x*/20,/*y*/130},{/*级别3*//*x*/17,/*y*/130,/*级别3*//*x*/18,/*y*/130},{/*级别4*//*x*/19,/*y*/129,/*级别4*//*x*/20,/*y*/129}},
{/*建宁*/{/*级别1*//*x*/35,/*y*/171,/*级别1*//*x*/15,/*y*/179},{/*级别2*//*x*/16,/*y*/180,/*级别2*//*x*/15,/*y*/178},{/*级别3*//*x*/16,/*y*/179,/*级别3*//*x*/17,/*y*/179},{/*级别4*//*x*/16,/*y*/178,/*级别4*//*x*/17,/*y*/178}},
{/*云南*/{/*级别1*//*x*/15,/*y*/194,/*级别1*//*x*/14,/*y*/194},{/*级别2*//*x*/14,/*y*/195,/*级别2*//*x*/13,/*y*/196},{/*级别3*//*x*/13,/*y*/195,/*级别3*//*x*/13,/*y*/194},{/*级别4*//*x*/12,/*y*/196,/*级别4*//*x*/12,/*y*/195}}

	};

	const array<array<array<int>>> 内政用地升级2 = {
		{/*襄平*/ {/*级别1*/ /*x*/ 194, /*y*/ 30, /*级别1*/ /*x*/ 194, /*y*/ 31}, {/*级别2*/ /*x*/ 191, /*y*/ 17, /*级别2*/ /*x*/ 192, /*y*/ 17}, {/*级别3*/ /*x*/ 199, /*y*/ 9, /*级别3*/ /*x*/ 199, /*y*/ 10}, {/*级别4*/ /*x*/ 178, /*y*/ 7, /*级别4*/ /*x*/ 178, /*y*/ 8}},
		{/*北平*/ {/*级别1*/ /*x*/ 155, /*y*/ 12, /*级别1*/ /*x*/ 155, /*y*/ 13}, {/*级别2*/ /*x*/ 156, /*y*/ 0, /*级别2*/ /*x*/ 157, /*y*/ 0}, {/*级别3*/ /*x*/ 149, /*y*/ 7, /*级别3*/ /*x*/ 149, /*y*/ 8}, {/*级别4*/ /*x*/ 152, /*y*/ 13, /*级别4*/ /*x*/ 152, /*y*/ 14}},
		{/*蓟*/ {/*级别1*/ /*x*/ 125, /*y*/ 14, /*级别1*/ /*x*/ 125, /*y*/ 15}, {/*级别2*/ /*x*/ 125, /*y*/ 16, /*级别2*/ /*x*/ 125, /*y*/ 17}, {/*级别3*/ /*x*/ 131, /*y*/ 21, /*级别3*/ /*x*/ 131, /*y*/ 22}, {/*级别4*/ /*x*/ 132, /*y*/ 22, /*级别4*/ /*x*/ 132, /*y*/ 23}},
		{/*南皮*/ {/*级别1*/ /*x*/ 117, /*y*/ 30, /*级别1*/ /*x*/ 117, /*y*/ 31}, {/*级别2*/ /*x*/ 106, /*y*/ 40, /*级别2*/ /*x*/ 106, /*y*/ 41}, {/*级别3*/ /*x*/ 105, /*y*/ 40, /*级别3*/ /*x*/ 105, /*y*/ 41}, {/*级别4*/ /*x*/ 107, /*y*/ 39, /*级别4*/ /*x*/ 107, /*y*/ 40}},
		{/*平原*/ {/*级别1*/ /*x*/ 139, /*y*/ 33, /*级别1*/ /*x*/ 142, /*y*/ 35}, {/*级别2*/ /*x*/ 130, /*y*/ 42, /*级别2*/ /*x*/ 129, /*y*/ 43}, {/*级别3*/ /*x*/ 128, /*y*/ 41, /*级别3*/ /*x*/ 128, /*y*/ 42}, {/*级别4*/ /*x*/ 129, /*y*/ 41, /*级别4*/ /*x*/ 129, /*y*/ 42}},
		{/*晋阳*/ {/*级别1*/ /*x*/ 83, /*y*/ 18, /*级别1*/ /*x*/ 83, /*y*/ 19}, {/*级别2*/ /*x*/ 93, /*y*/ 30, /*级别2*/ /*x*/ 90, /*y*/ 27}, {/*级别3*/ /*x*/ 83, /*y*/ 24, /*级别3*/ /*x*/ 83, /*y*/ 25}, {/*级别4*/ /*x*/ 84, /*y*/ 24, /*级别4*/ /*x*/ 84, /*y*/ 25}},
		{/*邺*/ {/*级别1*/ /*x*/ 113, /*y*/ 44, /*级别1*/ /*x*/ 114, /*y*/ 44}, {/*级别2*/ /*x*/ 112, /*y*/ 45, /*级别2*/ /*x*/ 111, /*y*/ 45}, {/*级别3*/ /*x*/ 102, /*y*/ 59, /*级别3*/ /*x*/ 103, /*y*/ 59}, {/*级别4*/ /*x*/ 104, /*y*/ 60, /*级别4*/ /*x*/ 105, /*y*/ 59}},
		{/*北海*/ {/*级别1*/ /*x*/ 156, /*y*/ 56, /*级别1*/ /*x*/ 156, /*y*/ 57}, {/*级别2*/ /*x*/ 153, /*y*/ 57, /*级别2*/ /*x*/ 153, /*y*/ 58}, {/*级别3*/ /*x*/ 159, /*y*/ 50, /*级别3*/ /*x*/ 160, /*y*/ 50}, {/*级别4*/ /*x*/ 157, /*y*/ 55, /*级别4*/ /*x*/ 157, /*y*/ 56}},
		{/*下邳*/ {/*级别1*/ /*x*/ 163, /*y*/ 72, /*级别1*/ /*x*/ 163, /*y*/ 71}, {/*级别2*/ /*x*/ 162, /*y*/ 71, /*级别2*/ /*x*/ 162, /*y*/ 70}, {/*级别3*/ /*x*/ 162, /*y*/ 69, /*级别3*/ /*x*/ 163, /*y*/ 69}, {/*级别4*/ /*x*/ 162, /*y*/ 68, /*级别4*/ /*x*/ 163, /*y*/ 68}},
		{/*彭城*/ {/*级别1*/ /*x*/ 136, /*y*/ 82, /*级别1*/ /*x*/ 136, /*y*/ 83}, {/*级别2*/ /*x*/ 138, /*y*/ 72, /*级别2*/ /*x*/ 139, /*y*/ 71}, {/*级别3*/ /*x*/ 140, /*y*/ 71, /*级别3*/ /*x*/ 141, /*y*/ 69}, {/*级别4*/ /*x*/ 142, /*y*/ 70, /*级别4*/ /*x*/ 135, /*y*/ 82}},
		{/*淮南*/ {/*级别1*/ /*x*/ 141, /*y*/ 100, /*级别1*/ /*x*/ 141, /*y*/ 101}, {/*级别2*/ /*x*/ 142, /*y*/ 100, /*级别2*/ /*x*/ 142, /*y*/ 101}, {/*级别3*/ /*x*/ 126, /*y*/ 103, /*级别3*/ /*x*/ 126, /*y*/ 104}, {/*级别4*/ /*x*/ 140, /*y*/ 101, /*级别4*/ /*x*/ 140, /*y*/ 102}},
		{/*濮阳*/ {/*级别1*/ /*x*/ 109, /*y*/ 70, /*级别1*/ /*x*/ 109, /*y*/ 71}, {/*级别2*/ /*x*/ 111, /*y*/ 71, /*级别2*/ /*x*/ 111, /*y*/ 72}, {/*级别3*/ /*x*/ 108, /*y*/ 71, /*级别3*/ /*x*/ 108, /*y*/ 72}, {/*级别4*/ /*x*/ 128, /*y*/ 71, /*级别4*/ /*x*/ 128, /*y*/ 72}},
		{/*陈留*/ {/*级别1*/ /*x*/ 106, /*y*/ 83, /*级别1*/ /*x*/ 106, /*y*/ 84}, {/*级别2*/ /*x*/ 107, /*y*/ 83, /*级别2*/ /*x*/ 107, /*y*/ 84}, {/*级别3*/ /*x*/ 106, /*y*/ 86, /*级别3*/ /*x*/ 106, /*y*/ 85}, {/*级别4*/ /*x*/ 103, /*y*/ 84, /*级别4*/ /*x*/ 103, /*y*/ 83}},
		{/*许昌*/ {/*级别1*/ /*x*/ 102, /*y*/ 88, /*级别1*/ /*x*/ 103, /*y*/ 88}, {/*级别2*/ /*x*/ 104, /*y*/ 88, /*级别2*/ /*x*/ 104, /*y*/ 89}, {/*级别3*/ /*x*/ 103, /*y*/ 87, /*级别3*/ /*x*/ 105, /*y*/ 88}, {/*级别4*/ /*x*/ 102, /*y*/ 89, /*级别4*/ /*x*/ 103, /*y*/ 89}},
		{/*汝南*/ {/*级别1*/ /*x*/ 120, /*y*/ 96, /*级别1*/ /*x*/ 120, /*y*/ 97}, {/*级别2*/ /*x*/ 119, /*y*/ 98, /*级别2*/ /*x*/ 120, /*y*/ 98}, {/*级别3*/ /*x*/ 121, /*y*/ 96, /*级别3*/ /*x*/ 121, /*y*/ 97}, {/*级别4*/ /*x*/ 121, /*y*/ 98, /*级别4*/ /*x*/ 119, /*y*/ 99}},
		{/*洛阳*/ {/*级别1*/ /*x*/ 73, /*y*/ 79, /*级别1*/ /*x*/ 75, /*y*/ 80}, {/*级别2*/ /*x*/ 75, /*y*/ 73, /*级别2*/ /*x*/ 75, /*y*/ 74}, {/*级别3*/ /*x*/ 84, /*y*/ 71, /*级别3*/ /*x*/ 84, /*y*/ 72}, {/*级别4*/ /*x*/ 82, /*y*/ 73, /*级别4*/ /*x*/ 83, /*y*/ 72}},
		{/*宛*/ {/*级别1*/ /*x*/ 71, /*y*/ 95, /*级别1*/ /*x*/ 71, /*y*/ 96}, {/*级别2*/ /*x*/ 87, /*y*/ 94, /*级别2*/ /*x*/ 87, /*y*/ 95}, {/*级别3*/ /*x*/ 73, /*y*/ 101, /*级别3*/ /*x*/ 73, /*y*/ 102}, {/*级别4*/ /*x*/ 74, /*y*/ 102, /*级别4*/ /*x*/ 74, /*y*/ 103}},
		{/*长安*/ {/*级别1*/ /*x*/ 50, /*y*/ 69, /*级别1*/ /*x*/ 51, /*y*/ 69}, {/*级别2*/ /*x*/ 52, /*y*/ 70, /*级别2*/ /*x*/ 53, /*y*/ 70}, {/*级别3*/ /*x*/ 53, /*y*/ 68, /*级别3*/ /*x*/ 53, /*y*/ 69}, {/*级别4*/ /*x*/ 42, /*y*/ 70, /*级别4*/ /*x*/ 42, /*y*/ 71}},
		{/*广陵*/ {/*级别1*/ /*x*/ 184, /*y*/ 88, /*级别1*/ /*x*/ 184, /*y*/ 89}, {/*级别2*/ /*x*/ 179, /*y*/ 87, /*级别2*/ /*x*/ 180, /*y*/ 87}, {/*级别3*/ /*x*/ 163, /*y*/ 87, /*级别3*/ /*x*/ 163, /*y*/ 86}, {/*级别4*/ /*x*/ 164, /*y*/ 87, /*级别4*/ /*x*/ 164, /*y*/ 86}},
		{/*安定*/ {/*级别1*/ /*x*/ 32, /*y*/ 43, /*级别1*/ /*x*/ 31, /*y*/ 44}, {/*级别2*/ /*x*/ 25, /*y*/ 37, /*级别2*/ /*x*/ 26, /*y*/ 37}, {/*级别3*/ /*x*/ 43, /*y*/ 44, /*级别3*/ /*x*/ 42, /*y*/ 43}, {/*级别4*/ /*x*/ 42, /*y*/ 42, /*级别4*/ /*x*/ 44, /*y*/ 45}},
		{/*陇西*/ {/*级别1*/ /*x*/ 4, /*y*/ 63, /*级别1*/ /*x*/ 4, /*y*/ 64}, {/*级别2*/ /*x*/ 4, /*y*/ 65, /*级别2*/ /*x*/ 4, /*y*/ 66}, {/*级别3*/ /*x*/ 15, /*y*/ 67, /*级别3*/ /*x*/ 16, /*y*/ 68}, {/*级别4*/ /*x*/ 14, /*y*/ 75, /*级别4*/ /*x*/ 15, /*y*/ 74}},
		{/*武威*/ {/*级别1*/ /*x*/ 6, /*y*/ 37, /*级别1*/ /*x*/ 6, /*y*/ 38}, {/*级别2*/ /*x*/ 1, /*y*/ 21, /*级别2*/ /*x*/ 1, /*y*/ 22}, {/*级别3*/ /*x*/ 2, /*y*/ 22, /*级别3*/ /*x*/ 3, /*y*/ 22}, {/*级别4*/ /*x*/ 1, /*y*/ 23, /*级别4*/ /*x*/ 2, /*y*/ 23}},
		{/*建业*/ {/*级别1*/ /*x*/ 171, /*y*/ 102, /*级别1*/ /*x*/ 172, /*y*/ 102}, {/*级别2*/ /*x*/ 167, /*y*/ 104, /*级别2*/ /*x*/ 167, /*y*/ 109}, {/*级别3*/ /*x*/ 172, /*y*/ 103, /*级别3*/ /*x*/ 173, /*y*/ 103}, {/*级别4*/ /*x*/ 173, /*y*/ 102, /*级别4*/ /*x*/ 174, /*y*/ 103}},
		{/*吴*/ {/*级别1*/ /*x*/ 173, /*y*/ 114, /*级别1*/ /*x*/ 183, /*y*/ 122}, {/*级别2*/ /*x*/ 184, /*y*/ 123, /*级别2*/ /*x*/ 172, /*y*/ 117}, {/*级别3*/ /*x*/ 185, /*y*/ 119, /*级别3*/ /*x*/ 185, /*y*/ 120}, {/*级别4*/ /*x*/ 185, /*y*/ 121, /*级别4*/ /*x*/ 185, /*y*/ 122}},
		{/*建安*/ {/*级别1*/ /*x*/ 151, /*y*/ 167, /*级别1*/ /*x*/ 152, /*y*/ 167}, {/*级别2*/ /*x*/ 142, /*y*/ 167, /*级别2*/ /*x*/ 142, /*y*/ 168}, {/*级别3*/ /*x*/ 141, /*y*/ 167, /*级别3*/ /*x*/ 141, /*y*/ 168}, {/*级别4*/ /*x*/ 141, /*y*/ 169, /*级别4*/ /*x*/ 142, /*y*/ 169}},
		{/*庐江*/ {/*级别1*/ /*x*/ 140, /*y*/ 118, /*级别1*/ /*x*/ 139, /*y*/ 117}, {/*级别2*/ /*x*/ 137, /*y*/ 116, /*级别2*/ /*x*/ 138, /*y*/ 117}, {/*级别3*/ /*x*/ 140, /*y*/ 114, /*级别3*/ /*x*/ 141, /*y*/ 113}, {/*级别4*/ /*x*/ 141, /*y*/ 114, /*级别4*/ /*x*/ 142, /*y*/ 114}},
		{/*柴桑*/ {/*级别1*/ /*x*/ 123, /*y*/ 152, /*级别1*/ /*x*/ 123, /*y*/ 153}, {/*级别2*/ /*x*/ 123, /*y*/ 154, /*级别2*/ /*x*/ 122, /*y*/ 140}, {/*级别3*/ /*x*/ 122, /*y*/ 139, /*级别3*/ /*x*/ 123, /*y*/ 155}, {/*级别4*/ /*x*/ 124, /*y*/ 153, /*级别4*/ /*x*/ 124, /*y*/ 154}},
		{/*江夏*/ {/*级别1*/ /*x*/ 106, /*y*/ 116, /*级别1*/ /*x*/ 108, /*y*/ 115}, {/*级别2*/ /*x*/ 108, /*y*/ 116, /*级别2*/ /*x*/ 100, /*y*/ 120}, {/*级别3*/ /*x*/ 100, /*y*/ 121, /*级别3*/ /*x*/ 100, /*y*/ 122}, {/*级别4*/ /*x*/ 118, /*y*/ 119, /*级别4*/ /*x*/ 118, /*y*/ 120}},
		{/*河东*/ {/*级别1*/ /*x*/ 70, /*y*/ 55, /*级别1*/ /*x*/ 70, /*y*/ 56}, {/*级别2*/ /*x*/ 66, /*y*/ 42, /*级别2*/ /*x*/ 66, /*y*/ 43}, {/*级别3*/ /*x*/ 66, /*y*/ 44, /*级别3*/ /*x*/ 66, /*y*/ 45}, {/*级别4*/ /*x*/ 64, /*y*/ 45, /*级别4*/ /*x*/ 65, /*y*/ 45}},
		{/*襄阳*/ {/*级别1*/ /*x*/ 54, /*y*/ 109, /*级别1*/ /*x*/ 53, /*y*/ 109}, {/*级别2*/ /*x*/ 53, /*y*/ 108, /*级别2*/ /*x*/ 52, /*y*/ 109}, {/*级别3*/ /*x*/ 56, /*y*/ 111, /*级别3*/ /*x*/ 56, /*y*/ 110}, {/*级别4*/ /*x*/ 51, /*y*/ 108, /*级别4*/ /*x*/ 52, /*y*/ 108}},
		{/*江陵*/ {/*级别1*/ /*x*/ 76, /*y*/ 132, /*级别1*/ /*x*/ 76, /*y*/ 133}, {/*级别2*/ /*x*/ 76, /*y*/ 134, /*级别2*/ /*x*/ 84, /*y*/ 132}, {/*级别3*/ /*x*/ 77, /*y*/ 132, /*级别3*/ /*x*/ 77, /*y*/ 133}, {/*级别4*/ /*x*/ 83, /*y*/ 131, /*级别4*/ /*x*/ 83, /*y*/ 132}},
		{/*长沙*/ {/*级别1*/ /*x*/ 107, /*y*/ 152, /*级别1*/ /*x*/ 107, /*y*/ 153}, {/*级别2*/ /*x*/ 107, /*y*/ 154, /*级别2*/ /*x*/ 107, /*y*/ 155}, {/*级别3*/ /*x*/ 107, /*y*/ 156, /*级别3*/ /*x*/ 106, /*y*/ 152}, {/*级别4*/ /*x*/ 106, /*y*/ 153, /*级别4*/ /*x*/ 106, /*y*/ 154}},
		{/*武陵*/ {/*级别1*/ /*x*/ 80, /*y*/ 151, /*级别1*/ /*x*/ 81, /*y*/ 149}, {/*级别2*/ /*x*/ 81, /*y*/ 150, /*级别2*/ /*x*/ 82, /*y*/ 150}, {/*级别3*/ /*x*/ 60, /*y*/ 151, /*级别3*/ /*x*/ 60, /*y*/ 152}, {/*级别4*/ /*x*/ 79, /*y*/ 151, /*级别4*/ /*x*/ 80, /*y*/ 152}},
		{/*桂阳*/ {/*级别1*/ /*x*/ 86, /*y*/ 0, /*级别1*/ /*x*/ 86, /*y*/ 1}, {/*级别2*/ /*x*/ 85, /*y*/ 0, /*级别2*/ /*x*/ 85, /*y*/ 1}, {/*级别3*/ /*x*/ 89, /*y*/ 4, /*级别3*/ /*x*/ 89, /*y*/ 5}, {/*级别4*/ /*x*/ 90, /*y*/ 5, /*级别4*/ /*x*/ 90, /*y*/ 6}},
		{/*零陵*/ {/*级别1*/ /*x*/ 37, /*y*/ 10, /*级别1*/ /*x*/ 37, /*y*/ 11}, {/*级别2*/ /*x*/ 38, /*y*/ 10, /*级别2*/ /*x*/ 38, /*y*/ 11}, {/*级别3*/ /*x*/ 44, /*y*/ 15, /*级别3*/ /*x*/ 44, /*y*/ 16}, {/*级别4*/ /*x*/ 45, /*y*/ 14, /*级别4*/ /*x*/ 45, /*y*/ 15}},
		{/*永安*/ {/*级别1*/ /*x*/ 47, /*y*/ 132, /*级别1*/ /*x*/ 47, /*y*/ 133}, {/*级别2*/ /*x*/ 38, /*y*/ 127, /*级别2*/ /*x*/ 39, /*y*/ 126}, {/*级别3*/ /*x*/ 40, /*y*/ 125, /*级别3*/ /*x*/ 40, /*y*/ 126}, {/*级别4*/ /*x*/ 39, /*y*/ 127, /*级别4*/ /*x*/ 40, /*y*/ 127}},
		{/*汉中*/ {/*级别1*/ /*x*/ 31, /*y*/ 95, /*级别1*/ /*x*/ 32, /*y*/ 94}, {/*级别2*/ /*x*/ 32, /*y*/ 95, /*级别2*/ /*x*/ 32, /*y*/ 96}, {/*级别3*/ /*x*/ 30, /*y*/ 93, /*级别3*/ /*x*/ 31, /*y*/ 92}, {/*级别4*/ /*x*/ 25, /*y*/ 85, /*级别4*/ /*x*/ 24, /*y*/ 86}},
		{/*梓潼*/ {/*级别1*/ /*x*/ 11, /*y*/ 110, /*级别1*/ /*x*/ 11, /*y*/ 111}, {/*级别2*/ /*x*/ 11, /*y*/ 108, /*级别2*/ /*x*/ 11, /*y*/ 109}, {/*级别3*/ /*x*/ 15, /*y*/ 103, /*级别3*/ /*x*/ 15, /*y*/ 104}, {/*级别4*/ /*x*/ 11, /*y*/ 112, /*级别4*/ /*x*/ 12, /*y*/ 112}},
		{/*江州*/ {/*级别1*/ /*x*/ 35, /*y*/ 146, /*级别1*/ /*x*/ 35, /*y*/ 147}, {/*级别2*/ /*x*/ 35, /*y*/ 148, /*级别2*/ /*x*/ 35, /*y*/ 149}, {/*级别3*/ /*x*/ 23, /*y*/ 140, /*级别3*/ /*x*/ 24, /*y*/ 141}, {/*级别4*/ /*x*/ 23, /*y*/ 138, /*级别4*/ /*x*/ 23, /*y*/ 139}},
		{/*成都*/ {/*级别1*/ /*x*/ 3, /*y*/ 137, /*级别1*/ /*x*/ 3, /*y*/ 138}, {/*级别2*/ /*x*/ 13, /*y*/ 125, /*级别2*/ /*x*/ 13, /*y*/ 126}, {/*级别3*/ /*x*/ 13, /*y*/ 127, /*级别3*/ /*x*/ 12, /*y*/ 125}, {/*级别4*/ /*x*/ 12, /*y*/ 126, /*级别4*/ /*x*/ 12, /*y*/ 127}},
		{/*南中*/ {/*级别1*/ /*x*/ 34, /*y*/ 168, /*级别1*/ /*x*/ 34, /*y*/ 170}, {/*级别2*/ /*x*/ 34, /*y*/ 171, /*级别2*/ /*x*/ 34, /*y*/ 172}, {/*级别3*/ /*x*/ 19, /*y*/ 174, /*级别3*/ /*x*/ 19, /*y*/ 175}, {/*级别4*/ /*x*/ 18, /*y*/ 173, /*级别4*/ /*x*/ 19, /*y*/ 173}},
		{/*合浦*/ {/*级别1*/ /*x*/ 86, /*y*/ 198, /*级别1*/ /*x*/ 86, /*y*/ 199}, {/*级别2*/ /*x*/ 75, /*y*/ 190, /*级别2*/ /*x*/ 75, /*y*/ 191}, {/*级别3*/ /*x*/ 87, /*y*/ 197, /*级别3*/ /*x*/ 87, /*y*/ 198}, {/*级别4*/ /*x*/ 88, /*y*/ 198, /*级别4*/ /*x*/ 88, /*y*/ 199}}

	};
} // namespace end
