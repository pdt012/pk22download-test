﻿// ## 2023/05/06 # 江东新风 # 拆分连环特技 #

namespace 普攻攻击范围
{
	class Main
	{
		Main()
		{
			pk::set_func(71, pk::func71_t(callback));
		}

		int callback(pk::int_int& range, pk::unit@ unit, const pk::point& in pos)
		{
            
            pk::force@ force = pk::get_force(unit.get_force_id());
            //pk::trace("id" + unit.get_id() + ",x:" + pos.x + ",y" + pos.y);
            range.first = 0;
            range.second = 0;
            //距离0为近战，range.first为最近距离，range.second为最远距离
            //不要试图让剑，枪，戟的距离大于0，会出bug
            switch (pk::get_weapon_id(unit, pos))
            {
            case 兵器_弩:
                range.second = 1 + (pk::has_tech(force, 技巧_强弩)?1:0) + (unit.has_skill(特技_神臂) ? 1 : 0);
                break;
            case 兵器_战马:
                if (unit.has_skill(特技_白马) or pk::has_tech(force, 技巧_骑射))
                    range.second = 1;
                break;
            case 兵器_井阑:
                range.first = 1;
                range.second = 1;
                if (unit.has_skill(特技_射程))
                    range.second += 1;
                break;
            case 兵器_投石:
                range.first = 2;
                range.second = 2;
                if (unit.has_skill(特技_射程))
                    range.second += 1;
                break;
            case 兵器_走舸:
            case 兵器_楼船:
                range.second = 1;
                break;
            case 兵器_斗舰:
                range.second = 3;
                break;
            default:
                break;
            }
            //pk::trace("id" + unit.get_id() + ",x:" + pos.x + ",y" + pos.y + "range_min" + range.first + "range_max" + range.second);
            return range.second;
          

		}

	}

	Main main;
}