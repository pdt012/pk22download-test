﻿// ## 2023/05/03 # 铃 # 修改了难度梯度影响.优化算法. ##											
// ## 2023/01/23 # 铃 # 根据城市的人口特征,再次优化算法 ##
// ## 2022/12/29 # 铃 # 再次优化人口系统,同时优化资金算法,使得收入更平滑 ##
// ## 2022/11/02 # 铃 # 修改了资金收入算法,以适配新的人口系统 ##
// ## 2022/08/08 # 铃 # 把城市收入赋予结构体 ##
// ## 2022/02/17 # 江东新风 # 探索涨基础金收入 ##
// ## 2022/02/14 # 江东新风 # 部分常量中文化 ##
// ## 2021/10/29 # 江东新风 # 结构体存储调用方式改进 ##
// ## 2021/10/10 # 江东新风 # 人口相关设定 ##
// ## 2021/10/01 # 江东新风 # namespace的韩文改成英文 ##
// ## 2021/02/17 # 江东新风 # 城市数惩罚设置上限，调整系数 ##
// ## 2020/12/12 # 江东新风 # 修复trace参数报错 ##
// ## 2020/10/07 # 氕氘氚 # 修复造币厂未建成就生效的bug ##
// ## 2020/09/28 # 氕氘氚 # 造币允许不相邻、造币对鱼市场、大市场生效，玩家电脑分别设置倍率 ##
// ## 2020/09/05 # 氕氘氚 #变法bug修复##
// ## 2020/08/19 # 江东新风 #新特技-变法##
// ## 2020/08/10 # 氕氘氚 ##
/*
@수정자: 기마책사
@Update: '18.12.22  / 변경내용: 병기차감 스크립트 분리
*/


namespace CITY_REVENUE
{
	const bool 调试模式 = false;
	class Main
	{
		Main()
		{
			pk::set_func(150, pk::func150_t(callback));

		}

		int callback(pk::city @city)
		{

			if (city is null or !pk::is_valid_force_id(city.get_force_id()))
				return 0;

			// 城市基础收入
			int n = city.revenue;
			int population_yield = 0;
			int market_yield = 0;
			float population = 0;
			BuildingInfo @base_p = @building_ex[city.get_id()];
			float 人口税率基数 = 大城市人口上限 / 100.f;//300w/100=3w
			if (开启人口系统)
			{
				int tax_rate = 10; // 普通税率为每万人收10金,根据人口数量调整为2.5万人税10金
				BaseInfo @city_t = @base_ex[city.get_id()];
				population = pk::min(大城市人口上限, pk::max(5000.f, float(city_t.population))); // 防止意外情况溢出

				population_yield = int(population / 人口税率基数 * tax_rate);

				if (调试模式) pk::trace(pk::format("{},population：{}，gold population_yield：{}", pk::decode(pk::get_name(city)), population, population_yield));
			}
			else
			{
				population_yield = city.revenue;
			}


			// 判断有无造币厂
			bool has_造币 = false;
			if (造币谷仓无需相邻)
			{
				for (int i = 0; i < city.max_devs; i++)
				{
					// 为了避免遍历城市周围地格，所以城市信息里会存开发土地的信息，如果想要支城能具有影响收入效果，可以参考这个
					pk::building @t_building = city.dev[i].building;
					if (pk::is_alive(t_building) && t_building.facility == 시설_조폐 && t_building.completed)
					{
						has_造币 = true;
						break;
					}
				}
			}

			float basic_yield = 0;
			for (int i = 0; i < city.max_devs; i++)
			{
				pk::building @building = city.dev[i].building;
				int facility_id = -1;

				if (pk::is_alive(building))
				{
					facility_id = building.facility;
					switch (facility_id)
					{
					case 设施_市场1级:
					case 设施_大市场:
					case 设施_渔市:
					case 设施_黑市:
						if (!building.completed)
							continue;
						break;
					case 设施_市场2级:
						if (!building.completed)
							facility_id = 设施_市场1级;
						break;
					case 设施_市场3级:
						if (!building.completed)
							facility_id = 设施_市场2级;
						break;
					default:
						continue;
					}
				}

				// 내정시설 별 생산력을 더함.
				pk::facility @facility = pk::get_facility(facility_id);

				if (pk::is_alive(facility))
				{
					int y = facility.yield;
					basic_yield += y;
					// 造币加成

					if (facility_id == 设施_市场1级 or facility_id == 设施_市场2级 or facility_id == 设施_市场3级 or (造币对鱼市大市生效 and (facility_id == 设施_大市场 or facility_id == 设施_渔市)))
																												
					{
						if (造币谷仓无需相邻 && has_造币 || func_49ed70(building.get_pos(), 设施_造币))
						{
							// 变法
							if (ch::has_skill(pk::get_building(city.get_id()), 特技_变法)) // 变法
								y = int(y * pk::core::skill_constant_value(特技_变法,1) /100.f);//改成百分比好了~
							else
								y = int(y * 1.5f);
						}
						// if (city.is_player()) pk::trace(pk::format("{},造币谷仓无需相邻:{},has_造币：{},{},{}", pk::decode(pk::get_name(city)),facility_id == 设施_市场1级, facility_id == 设施_市场2级,facility_id == 设施_市场3级,int(y * 1.5f)));
					}
					market_yield = market_yield + y;
				}
			}

			if (开启人口系统)
			{
				// 每30的市场基础收入需要1万人维持，人数每低于要求1%，产量下降1%,最低倍率0.4，最高1.2
				if (true)
				{
					if (调试模式) pk::trace(pk::format("{},population:{},basic_yield:{},market_yield:{}", pk::decode(pk::get_name(city)), population, basic_yield, market_yield));
					float 人口影响倍率 = basic_yield > 0 ? pk::min(1.1f, pk::max(0.4f, ((population / 人口税率基数) / (basic_yield / 30.f)))) : 1.0f;

					market_yield = int(market_yield * 人口影响倍率);
					if (调试模式) pk::trace(pk::format("{}人口收入：{}，market_yield:{},基础收入：{}，人口影响倍率：{}", pk::decode(pk::get_name(city)), population_yield, market_yield, basic_yield, 人口影响倍率));

				}
				else
				{
				// to 风神:
				// 如果一个城市人口高但是基础收入低,会得到一个很高的人口影响倍率,从而远远盖过基础收入的影响,
				/*举个例子:
				辽东:
				人口：205254.0，market_yield:615,基础收入：800.0，人口影响倍率：0.76970255
				辽东,population:205254.0,n:1321

				广陵:
				人口：210586.0，market_yield:947,基础收入：650.0，人口影响倍率：0.9719354
				广陵,population:210586.0,n:1945

				广陵和辽东的人口仅仅差了2%不到,辽东基础收入800,多于广陵的基础收入650,但是总收入1320却远低于1945,
				*/
				// 考虑先重做一个简化版的market_yield,保留population_yield,加大population_yield的权重.

				market_yield = int(float(market_yield) * sqrt(population / 30) * 0.001) + 300;
				}

			}
			n = population_yield + market_yield;

			//n = int(n * (pk::is_large_city(city) ? 1.3 : 1));//已有人口倍率了，不再设计大都市优势

			if (调试模式) pk::trace(pk::format("{}人口收入：{},market_yield:{},市场收入：{},n1:{}", pk::decode(pk::get_name(city)),population_yield, market_yield,population_yield + market_yield,n));

			// 游戏难度修正
			n = inter::incomes_difficulty_impact2(n, city.is_player());//铃神改版的difficulty effect

			// 总倍率
			if (city.is_player())
				n = int(n * 玩家金收入倍率 / 100.f);
			else
				n = int(n * 电脑金收入倍率 / 100.f);

			n += base_ex[city.get_id()].revenue_bonus;

			if (调试模式) pk::trace(pk::format("{}人口收入：{},market_yield:{}, revenue_bonus：{}，理论收入：{},n1:{}", pk::decode(pk::get_name(city)),population_yield, market_yield, base_ex[city.get_id()].revenue_bonus, population_yield + market_yield + base_ex[city.get_id()].revenue_bonus,n));

			if (金粮收入_玩家_城市数_惩罚 and city.is_player() and !pk::is_campaign())
			{
				pk::force@ force = pk::get_force(city.get_force_id());
			 	float force_city_count = float(pk::get_city_list(force).count);
			 	n = int(n * (1.f - pk::min(0.3f, (force_city_count - 3) * 0.015f)));
			}

			// if (调试模式) pk::trace(pk::format("{},population:{},n:{}", pk::decode(pk::get_name(city)), population, n));

			// if (pk::get_day() == 21)			base_p.building_revenue = n - base_p.weapon_expense  - base_p.troops_expense  ;

			//下方函数用于获取城市收支，以便计算内政自动化各类花费---可以考虑是否有更好的写法
			//pk::trace("1base_p.building_revenue:" + base_p.building_revenue + ",n" + n + "base_t.building_revenue" + base_t.building_revenue);
			if (ch::has_skill(pk::get_building(city.get_id()), 特技_征税))
			{
				base_p.building_revenue = uint32(n * 0.5 * 3);//因为征税常数被取消，所以此处重新改入
			}
			else
			{
				base_p.building_revenue = uint32(n);
			}

			//pk::trace("2base_p.building_revenue:" + base_p.building_revenue + ",n" + n + "base_t.building_revenue" + base_t.building_revenue);
			return n;
		}

		/**
			인접 시설 검색.
		*/
		bool func_49ed70(pk::point pos, int facility_id)
		{
			for (int i = 0; i < 방향_끝; i++)
			{
				pk::point neighbor_pos = pk::get_neighbor_pos(pos, i);
				if (pk::is_valid_pos(neighbor_pos))
				{
					pk::building @building = pk::get_building(neighbor_pos);
					if (pk::is_alive(building) and building.facility == facility_id and building.completed)
						return true;
				}
			}
			return false;
		}


	}

	Main main;
}