﻿// ## 2022/09/09 # 铃 # 修复bug ##
// ## 2022/09/04 # 铃 # 为新的内政系统做了适配,把征兵值设为0 ##
// ## 2022/03/29 # 铃 # 为新的人口系统重新做了优化 ##
// ## 2021/10/29 # 江东新风 # 结构体存储调用方式改进 ##
// ## 2021/10/10 # 江东新风 # 治安相关函数也搬运至此 ##
// ## 2021/09/15 # 江东新风 # 更改pk::core[]函数为英文##
// ## 2021/02/16 # 江东新风 # 一般城市征兵倍率无法修改bug ##
// ## 2021/01/24 # 江东新风 # 将内政函数内容全搬运到此处，以便其他cpp调用 ##
// ## 2020/12/12 # 江东新风 # 修正参数类型错误 ##
// ## 2020/09/28 # 氕氘氚 ##

namespace inter
{


	int incomes_difficulty_impact(int n, bool is_player)
	{
		switch (pk::get_scenario().difficulty)
		{
		case 难易度_超级:
			if (is_player)
				n = int(n * 0.8f);
			else
				n = int(n * 1.2f);
			break;

		case 难易度_初级:
			if (is_player)
				n = int(n * 1.2f);
			break;
		}
		return n;
	}

	int incomes_difficulty_impact2(int n, bool is_player)
	{
		float 难度系数 = 1.0;
		switch (pk::get_scenario().difficulty)
		{
		case 难易度_超级:
			if (is_player)
				难度系数 = 0.8f;
			else
				难度系数 = 1.5f;
			break;
		case 难易度_上级:
			if (is_player)
				难度系数 = 0.9f;
			else
				难度系数 = 1.3f;
			break;

		case 难易度_初级:
			if (is_player)
				难度系数 = 1.0f;
			else
				难度系数 = 1.2f;
			break;
		}
		return int(n* 难度系数);
	}

}