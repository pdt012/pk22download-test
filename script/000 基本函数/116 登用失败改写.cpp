﻿// ## 2023/05/07 # 江东新风 # 新增舌战失败改写结果函数 ##

namespace EMPLOY_FAILED
{
	bool debate_result_ = false;
	pk::person@ actor_;
	pk::person@ target_;
	class Main
	{
		Main()
		{
			pk::set_func(116, pk::func116_t(callback));
		}

		bool callback(pk::person@ target, pk::person@ actor, int type, int seed)
		{
			//进入此判断的都是已经失败的情况
			if (!pk::is_alive(target) or !pk::is_alive(actor)) return false;
			//pk::trace("触发116号功能里的清谈");
			if (pk::check_mibun(target, pk::mibun_flags(身份_在野, 身份_未发现)) and ch::has_skill(actor, 特技_清谈))
			{
				//初始化
				@actor_ = actor;
				@target_ = target;
				debate_result_ = false;
				pk::scene(pk::scene_t(scene_清谈));
			}
			return debate_result_;
		}

		//清谈执行
		void scene_清谈()
		{
			if (actor_.is_player())
			{
				pk::person@kunshu0 = pk::get_person(pk::get_kunshu_id(actor_));
				pk::message_box(pk::encode(pk::format("很抱歉，对\x1b[1x{}\x1b[0x大人仕官一事，\n恕难从命......", pk::decode(pk::get_name(kunshu0)))), target_);
				if (pk::yes_no(pk::encode(pk::format("要以舌战说服吗？\n挑战者 \x1b[1x{}\x1b[0x：智力：{}\n被挑战者\x1b[1x{}\x1b[0x：智力：{}", pk::decode(pk::get_name(actor_)), actor_.stat[武将能力_智力], pk::decode(pk::get_name(target_)), target_.stat[武将能力_智力]))))
				{
					//直接写debate会闪退，必须要用scen
					if (pk::debate(actor_, target_, pk::is_player_controlled(actor_), false, true, true).first == 0)
						debate_result_ = true;
				}
			}
			else
			{
				int dif = actor_.stat[武将能力_智力] - target_.stat[武将能力_智力];
				if (dif >= 5) debate_result_ = true;
				else if (dif >= 0) debate_result_ = pk::rand_bool(50 + dif);
				//其余情况都是false了，不用写
			}
		}
	}
	Main main;
}