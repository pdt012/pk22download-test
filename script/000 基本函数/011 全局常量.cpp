
// ## 2023/04/21 # 江东新风 # 调整部分系数 ##
// ## 2022/10/11 # 铃 # 修改部分人口.城市级别等参数以适配新人口算法 ##
// ## 2021/11/03 # 江东新风 # 增长率放入301 ##
// ## 2021/11/03 # 江东新风 # 伤兵显示开关 ##
// ## 2021/02/15 # 江东新风 # 狼骑伤害系数调整 ##
// ## 2021/01/28 # messi # 优化部分特殊地名位置 ##
// ## 2021/01/05 # 江东新风 # 特殊地名设施可争夺常量移入此处，更新地名坐标 ##
// ## 2021/01/03 # 江东新风 # 特殊地名设施数组 ##
// ## 2020/12/13 # 江东新风 # 窥探常量 ##
// ## 2020/10/27 # 氕氘氚 # 删除特技名称 ##
// ## 2020/10/08 # 江东新风 # 特技_恫吓常量修改##
// ## 2020/09/28 # 氕氘氚 # 添加资源生产相关常量 ##
// ## 2020/09/23 # 江东新风 # 修改部分特技常量为简体(狙击，运筹，激励) ##
// ## 2020/08/30 # 江东新风 # 补充?山依旧逐鹿天下组合特技 ##
// ## 2020/08/24 # 氕氘氚 # 常量修改 ##
// ## 2020/08/19 # 江东新风 # 复合特技设定 ##
// ## 2020/08/16 # 氕氘氚 # 添加部分常量 ##
// ## 2020/08/14 # 江东新风 #加入光环系统常量##

//const int mod专属设定 = 2;//0表示用原版，1表示用大浪淘沙，2表示用血色，用于决定类似君主id权重，地名设施之类的，采用那种设定
//上方设定被下方两个细化设定代替
const int 地图专属设定 = 0;//0表示采用0328(月夜苍风)地图，1表示采用大浪地图----取消，改成和core里统一，设置新地图时，会读取data/dalang里的文件覆盖外面的文件
const int 血色剧本 = 15;//根据剧本区分是不是血色衣冠
const bool 特定地点特定设施可争夺 = true;
const array<int> 地名设施 = { 设施_阵, 设施_砦, 设施_城塞, 设施_弓橹, 设施_连弩橹 };
const array<int> 地名设施类型1 = { 设施_阵, 设施_砦, 设施_城塞};
const array<int> 地名设施类型2 = { 设施_弓橹, 设施_连弩橹 };
//小兵拔擢
const bool 开启小兵拔擢 = true;
const int 四级特技概率 = 20;//在武将能力满足对应特技需求时，获得对应等级特技的概率
const int 三级特技概率 = 30;//在武将能力满足对应特技需求时，获得对应等级特技的概率
const int 二级特技概率 = 30;//在武将能力满足对应特技需求时，获得对应等级特技的概率
const int 一级特技概率 = 20;//在武将能力满足对应特技需求时，获得对应等级特技的概率
//人口测试
const bool 开启人口系统 = true;
int 初始人口模式 = 0;
const int 一级城市阈值 = 500000;
const int 二级城市阈值 = 1000000;
const int 三级城市阈值 = 1500000;
const int 四级城市阈值 = 2200000;
const float 大城市人口上限 = 3000000.f;
const float 小城市人口上限 = 2000000.f;
const float 港关人口上限 = 500000.f;

const bool 开启伤兵系统 = true;
const bool 开启疑兵伏兵 = true;//取消，作为游戏中更改选项
int 据点信息行数 = 0;
const float 人口基础增长率 = 0.005f;
const int 伏兵部队数 = 4;
const int 疑兵基础概率 = 3;
const int 减少疑兵概率智力下限 = 90;//攻击方
const int 增加疑兵概率智力下限 = 70;//被攻击方
const array<int> 伏兵难度倍率修正 = {/*初级难度*/ 5, /*上级难度*/ 3, /*超级难度*/ 1 };
// 资源生产相关
const int 玩家金收入倍率 = 100;//容易 120，普通100，史诗80，传说60
const int 电脑金收入倍率 = 100;//容易 120，普通130，史诗140，传说150
const int 玩家粮收入倍率 = 100;//容易 130，普通100，史诗80，传说60
const int 电脑粮收入倍率 = 100;//容易 120，普通130，史诗140，传说150
const bool 造币谷仓无需相邻 = true;
const bool 造币对鱼市大市生效 = true;
const bool 谷仓对军屯农生效 = true;
const int 军屯农单倍收入人数 = 15000; // 每产生一倍1级军粮收入所需的军队人数

const int 港关金收入倍率 = 100;  //相对原版收入
const int 港关粮收入倍率 = 100;  //相对原版收入

const bool 金粮收入_玩家_城市数_惩罚 = true;

// 内政生产相关
const int 玩家征兵倍率 = 100;
const int 电脑征兵倍率 = 150;

const int 制造数量倍率 = 100;
const int 军马数量倍率 = 100;
const int 玩家生产倍率 = 100;
const int 电脑生产倍率 = 150;

const int 周围敌部队征兵倍率 = 50;
const int 生产时长倍率 = 100;
const int 生产时长上限 = 10;

const int 执政官经验一阶 = 300;
const int 执政官经验二阶 = 600;
const int 执政官经验三阶 = 900;


// 光环系统
const bool 基础光环 = true;//是否打开
const bool 战法暴击光环 = true;//是否打开
const bool 暴击伤害光环 = true;//是否打开
const bool 计策成功率光环 = true;//是否打开
const bool 计策识破率光环 = true;//是否打开
const bool 辅助攻击光环 = true;//是否打开
const bool 气力回复光环 = true;//是否打开

// 技巧设定 ----------------------------------------------------------------
const int 矢盾发动机率 = 30;
const int 大盾发动机率 = 30;



array<array<int>> 特技固定特技数组 =
{};

/*	    参考禁止拆建	
		array<uint32> enabled_(势力_末, uint64(-1));//长度为46的素组，每个元素初始化为-1
		// 下面这个表明有多少个uint32
		array<array<int>> min_(도시_끝, array<int>(시설_끝, -2));*/


//array<array<uint8>> ai异族请求信息(非贼势力_末, { /*请求方据点id*/-1, /*目标城市id*/-1, /*异族势力id*/势力_盗贼, /*多少旬后出兵*/-1 });


//特殊地名设施2用于大浪地图，不带2用于0328地图

const array<array<int>> 特殊地名设施2 =
{
	/*带方*/ {/*设施类型*/设施_阵, /*x*/194, /*y*/48},
	/*西安平*/ {/*设施类型*/设施_箭楼, /*x*/191, /*y*/32},
	/*襄平*/ {/*设施类型*/设施_阵, /*x*/190, /*y*/16},
	/*玄菟*/ {/*设施类型*/设施_箭楼, /*x*/198, /*y*/10},
	/*辽西*/ {/*设施类型*/设施_阵, /*x*/172, /*y*/15},
	/*徐无*/ {/*设施类型*/设施_箭楼, /*x*/156, /*y*/12},
	/*海阳*/ {/*设施类型*/设施_箭楼, /*x*/166, /*y*/15},
	/*土垠*/ {/*设施类型*/设施_阵, /*x*/155, /*y*/24},
	/*无终*/ {/*设施类型*/设施_阵, /*x*/149, /*y*/5},
	/*俊靡*/ {/*设施类型*/设施_箭楼, /*x*/155, /*y*/2},
	/*泉州*/ {/*设施类型*/设施_阵, /*x*/129, /*y*/27},
	/*雍奴*/ {/*设施类型*/设施_箭楼, /*x*/144, /*y*/22},
	/*涿郡*/ {/*设施类型*/设施_箭楼, /*x*/117, /*y*/24},
	/*安乐*/ {/*设施类型*/设施_箭楼, /*x*/115, /*y*/11},
        /*易*/ {/*设施类型*/设施_箭楼, /*x*/127, /*y*/18},
	/*代*/ {/*设施类型*/设施_阵, /*x*/100, /*y*/14},
	/*盛乐*/ {/*设施类型*/设施_箭楼, /*x*/86, /*y*/2},
	/*善无*/ {/*设施类型*/设施_箭楼, /*x*/90, /*y*/13},
	/*骆*/ {/*设施类型*/设施_阵, /*x*/70, /*y*/20},
	/*皋狼*/ {/*设施类型*/设施_阵, /*x*/71, /*y*/32},
	/*马邑*/ {/*设施类型*/设施_箭楼, /*x*/82, /*y*/18},
	/*乐平*/ {/*设施类型*/设施_箭楼, /*x*/94, /*y*/41},
	/*西河*/ {/*设施类型*/设施_阵, /*x*/79, /*y*/46},
	/*新兴*/ {/*设施类型*/设施_箭楼, /*x*/93, /*y*/21},
	/*太原*/ {/*设施类型*/设施_箭楼, /*x*/83, /*y*/39},
	/*赵*/ {/*设施类型*/设施_阵, /*x*/103, /*y*/43},
	/*巨鹿*/ {/*设施类型*/设施_阵, /*x*/109, /*y*/41},
	/*高阳*/ {/*设施类型*/设施_箭楼, /*x*/117, /*y*/29},
	/*唐*/ {/*设施类型*/设施_阵, /*x*/108, /*y*/21},
	/*博陵*/ {/*设施类型*/设施_箭楼, /*x*/116, /*y*/33},
	/*广平*/ {/*设施类型*/设施_阵, /*x*/107, /*y*/47},
	/*汲郡*/ {/*设施类型*/设施_阵, /*x*/102, /*y*/62},
	/*信都*/ {/*设施类型*/设施_阵, /*x*/115, /*y*/45},
	/*河间*/ {/*设施类型*/设施_箭楼, /*x*/123, /*y*/36},
	/*阳平*/ {/*设施类型*/设施_箭楼, /*x*/116, /*y*/53},
	/*章武*/ {/*设施类型*/设施_阵, /*x*/136, /*y*/34},
	/*乐陵*/ {/*设施类型*/设施_箭楼, /*x*/135, /*y*/55},
	/*清河*/ {/*设施类型*/设施_箭楼, /*x*/122, /*y*/51},
	/*南皮*/ {/*设施类型*/设施_箭楼, /*x*/129, /*y*/44},
	/*荏平*/ {/*设施类型*/设施_阵, /*x*/123, /*y*/59},
	/*安邑*/ {/*设施类型*/设施_阵, /*x*/67, /*y*/61},
	/*东桓*/ {/*设施类型*/设施_阵, /*x*/74, /*y*/59},
	/*襄陵*/ {/*设施类型*/设施_箭楼, /*x*/77, /*y*/54},
	/*温县*/ {/*设施类型*/设施_箭楼, /*x*/84, /*y*/63},
	/*平阳*/ {/*设施类型*/设施_阵, /*x*/64, /*y*/46},
	/*美稷*/ {/*设施类型*/设施_阵, /*x*/49, /*y*/18},
	/*三封*/ {/*设施类型*/设施_箭楼, /*x*/38, /*y*/12},
	/*临戎*/ {/*设施类型*/设施_箭楼, /*x*/35, /*y*/16},
	/*奢延*/ {/*设施类型*/设施_阵, /*x*/38, /*y*/27},
	/*龟兹*/ {/*设施类型*/设施_箭楼, /*x*/45, /*y*/28},
	/*宣威*/ {/*设施类型*/设施_箭楼, /*x*/15, /*y*/11},
	/*河套*/ {/*设施类型*/设施_阵, /*x*/26, /*y*/14},
	/*西郡*/ {/*设施类型*/设施_阵, /*x*/1, /*y*/24},
	/*仓松*/ {/*设施类型*/设施_箭楼, /*x*/10, /*y*/30},
	/*榆中*/ {/*设施类型*/设施_箭楼, /*x*/14, /*y*/47},
	/*朝那*/ {/*设施类型*/设施_箭楼, /*x*/27, /*y*/43},
	/*漆*/ {/*设施类型*/设施_阵, /*x*/42, /*y*/54},
	/*略阳*/ {/*设施类型*/设施_阵, /*x*/25, /*y*/53},
	/*定阳*/ {/*设施类型*/设施_箭楼, /*x*/54, /*y*/41},
	/*雕阴*/ {/*设施类型*/设施_阵, /*x*/50, /*y*/50},
	/*冯翊*/ {/*设施类型*/设施_箭楼, /*x*/57, /*y*/60},
	/*扶风*/ {/*设施类型*/设施_箭楼, /*x*/32, /*y*/64},
	/*五丈原*/ {/*设施类型*/设施_阵, /*x*/35, /*y*/70},
	/*槐里*/ {/*设施类型*/设施_阵, /*x*/44, /*y*/65},
	/*蓝田*/ {/*设施类型*/设施_阵, /*x*/53, /*y*/78},
	/*天水*/ {/*设施类型*/设施_箭楼, /*x*/16, /*y*/70},
	/*街亭*/ {/*设施类型*/设施_箭楼, /*x*/20, /*y*/60},
	/*南安*/ {/*设施类型*/设施_箭楼, /*x*/14, /*y*/54},
	/*武都*/ {/*设施类型*/设施_箭楼, /*x*/13, /*y*/77},
	/*阴平*/ {/*设施类型*/设施_箭楼, /*x*/7, /*y*/81},
	/*弘农*/ {/*设施类型*/设施_箭楼, /*x*/65, /*y*/75},
	/*偃师*/ {/*设施类型*/设施_阵, /*x*/83, /*y*/77},
	/*河南*/ {/*设施类型*/设施_箭楼, /*x*/76, /*y*/79},
	/*成皋*/ {/*设施类型*/设施_阵, /*x*/84, /*y*/73},
	/*平阴*/ {/*设施类型*/设施_阵, /*x*/75, /*y*/72},
	/*中牟*/ {/*设施类型*/设施_箭楼, /*x*/97, /*y*/78},
	/*官渡*/ {/*设施类型*/设施_阵, /*x*/100, /*y*/76},
	/*尉氏*/ {/*设施类型*/设施_箭楼, /*x*/102, /*y*/84},
	/*襄邑*/ {/*设施类型*/设施_箭楼, /*x*/111, /*y*/87},
	/*城父*/ {/*设施类型*/设施_箭楼, /*x*/123, /*y*/88},
	/*白马*/ {/*设施类型*/设施_阵, /*x*/109, /*y*/69},
	/*甄城*/ {/*设施类型*/设施_箭楼, /*x*/128, /*y*/70},
	/*定陶*/ {/*设施类型*/设施_箭楼, /*x*/121, /*y*/74},
	/*泰山*/ {/*设施类型*/设施_阵, /*x*/147, /*y*/64},
	/*乌巢*/ {/*设施类型*/设施_阵, /*x*/114, /*y*/74},
	/*兰陵*/ {/*设施类型*/设施_箭楼, /*x*/144, /*y*/69},
	/*吕县*/ {/*设施类型*/设施_箭楼, /*x*/147, /*y*/74},
	/*萧县*/ {/*设施类型*/设施_阵, /*x*/131, /*y*/75},
	/*相县*/ {/*设施类型*/设施_箭楼, /*x*/123, /*y*/80},
	/*虹县*/ {/*设施类型*/设施_阵, /*x*/139, /*y*/82},
	/*东海*/ {/*设施类型*/设施_阵, /*x*/161, /*y*/70},
	/*琅琊*/ {/*设施类型*/设施_阵, /*x*/155, /*y*/67},
	/*淮阴*/ {/*设施类型*/设施_箭楼, /*x*/166, /*y*/79},
	/*夏丘*/ {/*设施类型*/设施_箭楼, /*x*/155, /*y*/83},
	/*郯县*/ {/*设施类型*/设施_箭楼, /*x*/154, /*y*/74},
	/*东莞*/ {/*设施类型*/设施_阵, /*x*/152, /*y*/63},
	/*临淄*/ {/*设施类型*/设施_阵, /*x*/152, /*y*/57},
	/*东莱*/ {/*设施类型*/设施_阵, /*x*/176, /*y*/53},
	/*广饶*/ {/*设施类型*/设施_阵, /*x*/158, /*y*/51},
	/*长广*/ {/*设施类型*/设施_箭楼, /*x*/170, /*y*/52},
	/*盱眙*/ {/*设施类型*/设施_箭楼, /*x*/155, /*y*/91},
	/*海陵*/ {/*设施类型*/设施_阵, /*x*/182, /*y*/89},
	/*盐渎*/ {/*设施类型*/设施_箭楼, /*x*/179, /*y*/83},
	/*射阳*/ {/*设施类型*/设施_阵, /*x*/169, /*y*/81},
	/*舆县*/ {/*设施类型*/设施_箭楼, /*x*/165, /*y*/90},
	/*合肥*/ {/*设施类型*/设施_箭楼, /*x*/134, /*y*/106},
	/*寿春*/ {/*设施类型*/设施_箭楼, /*x*/128, /*y*/99},
	/*钟离*/ {/*设施类型*/设施_箭楼, /*x*/141, /*y*/91},
	/*东城*/ {/*设施类型*/设施_箭楼, /*x*/146, /*y*/99},
	/*浚遒*/ {/*设施类型*/设施_箭楼, /*x*/141, /*y*/107},
	/*洵水*/ {/*设施类型*/设施_箭楼, /*x*/133, /*y*/121},
	/*舒县*/ {/*设施类型*/设施_箭楼, /*x*/137, /*y*/114},
	/*皖县*/ {/*设施类型*/设施_箭楼, /*x*/141, /*y*/121},
	/*蕲春*/ {/*设施类型*/设施_箭楼, /*x*/129, /*y*/119},
	/*西陵*/ {/*设施类型*/设施_箭楼, /*x*/113, /*y*/121},
	/*云社*/ {/*设施类型*/设施_箭楼, /*x*/99, /*y*/118},
	/*随县*/ {/*设施类型*/设施_阵, /*x*/101, /*y*/112},
	/*安陆*/ {/*设施类型*/设施_箭楼, /*x*/107, /*y*/116},
	/*安丰*/ {/*设施类型*/设施_箭楼, /*x*/121, /*y*/109},
	/*汝阴*/ {/*设施类型*/设施_箭楼, /*x*/120, /*y*/99},
	/*上蔡*/ {/*设施类型*/设施_箭楼, /*x*/111, /*y*/101},
	/*舞阳*/ {/*设施类型*/设施_箭楼, /*x*/91, /*y*/99},
	/*长社*/ {/*设施类型*/设施_阵, /*x*/90, /*y*/86},
	/*长平*/ {/*设施类型*/设施_箭楼, /*x*/107, /*y*/100},
	/*鄢陵*/ {/*设施类型*/设施_箭楼, /*x*/105, /*y*/90},
	/*陈*/ {/*设施类型*/设施_箭楼, /*x*/114, /*y*/92},
	/*舞阴*/ {/*设施类型*/设施_箭楼, /*x*/84, /*y*/97},
	/*宛*/ {/*设施类型*/设施_阵, /*x*/77, /*y*/98},
	/*南乡*/ {/*设施类型*/设施_阵, /*x*/62, /*y*/99},
	/*鲁阳*/ {/*设施类型*/设施_箭楼, /*x*/82, /*y*/89},
	/*骊县*/ {/*设施类型*/设施_箭楼, /*x*/70, /*y*/97},
	/*西城*/ {/*设施类型*/设施_箭楼, /*x*/46, /*y*/101},
	/*成固*/ {/*设施类型*/设施_箭楼, /*x*/32, /*y*/93},
	/*沔阳*/ {/*设施类型*/设施_箭楼, /*x*/17, /*y*/87},
	/*葭萌*/ {/*设施类型*/设施_阵, /*x*/21, /*y*/109},
	/*白水*/ {/*设施类型*/设施_箭楼, /*x*/19, /*y*/98},
	/*涪县*/ {/*设施类型*/设施_箭楼, /*x*/12, /*y*/119},
	/*瓦口*/ {/*设施类型*/设施_箭楼, /*x*/35, /*y*/111},
	/*摩天岭*/ {/*设施类型*/设施_阵, /*x*/5, /*y*/103},
	/*汶山*/ {/*设施类型*/设施_阵, /*x*/0, /*y*/118},
	/*广汉*/ {/*设施类型*/设施_箭楼, /*x*/24, /*y*/125},
	/*汉嘉*/ {/*设施类型*/设施_箭楼, /*x*/2, /*y*/139},
	/*犍为*/ {/*设施类型*/设施_箭楼, /*x*/14, /*y*/144},
	/*越巂*/ {/*设施类型*/设施_阵, /*x*/6, /*y*/151},
	/*江州*/ {/*设施类型*/设施_阵, /*x*/22, /*y*/148},
	/*枳县*/ {/*设施类型*/设施_箭楼, /*x*/35, /*y*/151},
	/*临江*/ {/*设施类型*/设施_箭楼, /*x*/34, /*y*/140},
	/*垫江*/ {/*设施类型*/设施_箭楼, /*x*/24, /*y*/140},
	/*南浦*/ {/*设施类型*/设施_箭楼, /*x*/40, /*y*/134},
	/*宕渠*/ {/*设施类型*/设施_箭楼, /*x*/41, /*y*/125},
	/*鱼复*/ {/*设施类型*/设施_阵, /*x*/54, /*y*/126},
	/*白帝*/ {/*设施类型*/设施_箭楼, /*x*/49, /*y*/131},
        /*建平*/ {/*设施类型*/设施_箭楼, /*x*/56, /*y*/134},
	/*江阳*/ {/*设施类型*/设施_箭楼, /*x*/25, /*y*/156},
	/*牂牁*/ {/*设施类型*/设施_阵, /*x*/34, /*y*/169},
	/*滇池*/ {/*设施类型*/设施_箭楼, /*x*/16, /*y*/178},
	/*兴古*/ {/*设施类型*/设施_箭楼, /*x*/36, /*y*/189},
        /*万寿*/ {/*设施类型*/设施_箭楼, /*x*/51, /*y*/167},
	/*房陵*/ {/*设施类型*/设施_箭楼, /*x*/61, /*y*/111},
	/*中庐*/ {/*设施类型*/设施_箭楼, /*x*/81, /*y*/115},
	/*新城*/ {/*设施类型*/设施_阵, /*x*/69, /*y*/119},
	/*编县*/ {/*设施类型*/设施_阵, /*x*/81, /*y*/123},
	/*临沮*/ {/*设施类型*/设施_箭楼, /*x*/75, /*y*/121},
	/*信陵*/ {/*设施类型*/设施_箭楼, /*x*/63, /*y*/129},
	/*枝江*/ {/*设施类型*/设施_箭楼, /*x*/75, /*y*/136},
	/*当阳*/ {/*设施类型*/设施_阵, /*x*/79, /*y*/131},
	/*乌林*/ {/*设施类型*/设施_箭楼, /*x*/101, /*y*/133},
	/*华容口*/ {/*设施类型*/设施_箭楼, /*x*/87, /*y*/133},
	/*宜都*/ {/*设施类型*/设施_箭楼, /*x*/67, /*y*/138},
	/*零阳*/ {/*设施类型*/设施_阵, /*x*/66, /*y*/146},
	/*邵陵*/ {/*设施类型*/设施_箭楼, /*x*/77, /*y*/165},
	/*龙刚*/ {/*设施类型*/设施_阵, /*x*/58, /*y*/172},
	/*洞庭*/ {/*设施类型*/设施_箭楼, /*x*/77, /*y*/156},
        /*油江口*/ {/*设施类型*/设施_箭楼, /*x*/85, /*y*/148},
	/*郁林*/ {/*设施类型*/设施_箭楼, /*x*/64, /*y*/180},
	/*始安*/ {/*设施类型*/设施_阵, /*x*/71, /*y*/179},
	/*武城*/ {/*设施类型*/设施_箭楼, /*x*/86, /*y*/194},
	/*四会*/ {/*设施类型*/设施_箭楼, /*x*/109, /*y*/197},
	/*临贺*/ {/*设施类型*/设施_箭楼, /*x*/94, /*y*/188},
	/*始兴*/ {/*设施类型*/设施_箭楼, /*x*/105, /*y*/185},
	/*湘东*/ {/*设施类型*/设施_阵, /*x*/108, /*y*/171},
	/*巴陵*/ {/*设施类型*/设施_箭楼, /*x*/102, /*y*/149},
	/*安成*/ {/*设施类型*/设施_箭楼, /*x*/125, /*y*/168},
	/*下隽*/ {/*设施类型*/设施_箭楼, /*x*/105, /*y*/152},
	/*耒阳*/ {/*设施类型*/设施_箭楼, /*x*/101, /*y*/171},
	/*夏口*/ {/*设施类型*/设施_箭楼, /*x*/118, /*y*/131},
	/*浔阳*/ {/*设施类型*/设施_箭楼, /*x*/129, /*y*/133},
	/*蒲圻*/ {/*设施类型*/设施_阵, /*x*/109, /*y*/143},
	/*蔡望*/ {/*设施类型*/设施_箭楼, /*x*/121, /*y*/159},
	/*阳新*/ {/*设施类型*/设施_箭楼, /*x*/124, /*y*/141},
	/*南海*/ {/*设施类型*/设施_阵, /*x*/120, /*y*/195},
	/*新罗*/ {/*设施类型*/设施_箭楼, /*x*/141, /*y*/172},
	/*南康*/ {/*设施类型*/设施_箭楼, /*x*/122, /*y*/182},
	/*吴兴*/ {/*设施类型*/设施_阵, /*x*/160, /*y*/149},
	/*余涂*/ {/*设施类型*/设施_箭楼, /*x*/145, /*y*/153},
	/*东阳*/ {/*设施类型*/设施_箭楼, /*x*/164, /*y*/139},
	/*夷洲*/ {/*设施类型*/设施_阵, /*x*/193, /*y*/178},
	/*临海*/ {/*设施类型*/设施_箭楼, /*x*/174, /*y*/142},
	/*句章*/ {/*设施类型*/设施_箭楼, /*x*/190, /*y*/119},
	/*富春*/ {/*设施类型*/设施_箭楼, /*x*/177, /*y*/125},
	/*娄县*/ {/*设施类型*/设施_阵, /*x*/184, /*y*/107},
	/*于潜*/ {/*设施类型*/设施_阵, /*x*/169, /*y*/127},
	/*乌程*/ {/*设施类型*/设施_箭楼, /*x*/176, /*y*/113},
	/*宣城*/ {/*设施类型*/设施_阵, /*x*/161, /*y*/126},
	/*丹阳*/ {/*设施类型*/设施_阵, /*x*/163, /*y*/113},
	/*阳羡*/ {/*设施类型*/设施_箭楼, /*x*/169, /*y*/110},
	/*丹徒*/ {/*设施类型*/设施_箭楼, /*x*/169, /*y*/100}
};

const array<string>特殊地名设施名称2 =
{
	"带方",
	"西安平",
	"襄平",
	"玄菟",
	"辽西",
	"徐无",
	"海阳",
	"土垠",
	"无终",
	"俊靡",
	"泉州",
	"雍奴",
	"涿郡",
	"安乐",
	"易",
	"代",
	"盛乐",
	"善无",
	"骆",
	"皋狼",
	"马邑",
	"乐平",
	"西河",
	"新兴",
	"太原",
	"赵",
	"巨鹿",
	"高阳",
	"唐",
	"博陵",
	"广平",
	"汲郡",
	"信都",
	"河间",
	"阳平",
	"章武",
	"乐陵",
	"清河",
	"南皮",
	"荏平",
	"安邑",
	"东桓",
	"襄陵",
	"温县",
	"平阳",
	"美稷",
	"三封",
        "临戎",
	"奢延",
	"龟兹",
	"宣威",
	"河套",
	"西郡",
	"仓松",
	"榆中",
	"朝那",
	"漆",
	"略阳",
	"定阳",
	"雕阴",
	"冯翊",
	"扶风",
        "五丈原",
	"槐里",
	"蓝田",
	"天水",
	"街亭",
	"南安",
	"武都",
	"阴平",
	"弘农",
	"偃师",
	"河南",
	"成皋",
	"平阴",
	"中牟",
	"官渡",
	"尉氏",
	"襄邑",
	"城父",
	"白马",
	"甄城",
	"定陶",
	"泰山",
	"乌巢",
	"兰陵",
	"吕县",
	"萧县",
	"相县",
	"虹县",
	"东海",
	"琅琊",
	"淮阴",
	"夏丘",
	"郯县",
	"东莞",
	"临淄",
	"东莱",
	"广饶",
	"长广",
	"盱眙",
	"海陵",
	"盐渎",
	"射阳",
	"舆县",
	"合肥",
	"寿春",
	"钟离",
	"东城",
	"浚遒",
	"洵水",
	"舒县",
	"皖县",
	"蕲春",
	"西陵",
	"云社",
	"随县",
	"安陆",
	"安丰",
	"汝阴",
	"上蔡",
	"舞阳",
	"长社",
	"长平",
	"鄢陵",
	"陈",
	"舞阴",
	"宛",
	"南乡",
	"鲁阳",
	"骊县",
	"西城",
	"城固",
	"沔阳",
	"葭萌",
	"白水",
	"涪县",
	"瓦口",
	"摩天岭",
	"汶山",
	"广汉",
	"汉嘉",
	"犍为",
	"越巂",
	"江州",
	"枳县",
	"临江",
	"垫江",
	"南浦",
	"宕渠",
	"鱼复",
	"白帝",
	"建平",
	"江阳",
	"牂牁",
	"滇池",
	"兴古",
	"万寿",
	"房陵",
	"中庐",
	"新城",
	"编县",
	"临沮",
	"信陵",
	"枝江",
	"当阳",
	"乌林",
	"华容口",
	"宜都",
	"零阳",
	"邵陵",
	"龙刚",
	"洞庭",
	"油江口",
	"郁林",
	"始安",
	"武城",
	"四会",
	"贺临",
	"始兴",
	"湘东",
	"巴陵",
	"安成",
	"下隽",
	"耒阳",
	"夏口",
	"浔阳",
	"蒲圻",
	"蔡望",
	"阳新",
	"南海",
	"新罗",
	"南康",
	"吴兴",
	"余涂",
	"东阳",
	"夷洲",
	"临海",
	"句章",
	"富春",
	"娄县",
	"于潜",
	"乌程",
	"宣城",
	"丹阳",
	"阳羡",
	"丹徒"
};

//目前数量221
const array<array<int>> 特殊地名设施 =
{
	/*金城*/ {/*设施类型*/设施_阵, /*x*/1, /*y*/48},
	/*陇西*/ {/*设施类型*/设施_阵, /*x*/4, /*y*/65},
	/*武都*/ {/*设施类型*/设施_阵, /*x*/8, /*y*/79},
	/*阴平*/ {/*设施类型*/设施_阵, /*x*/2, /*y*/91},
	/*五丈原*/ {/*设施类型*/设施_箭楼, /*x*/35, /*y*/71},
	/*涪陵*/ {/*设施类型*/设施_阵, /*x*/36, /*y*/156},
	/*牂牁*/ {/*设施类型*/设施_阵, /*x*/34, /*y*/169},
	/*华容*/ {/*设施类型*/设施_箭楼, /*x*/92, /*y*/134},
	/*河东*/ {/*设施类型*/设施_阵, /*x*/47, /*y*/50},
	/*乌巢*/ {/*设施类型*/设施_箭楼, /*x*/118, /*y*/68},
	/*野王*/ {/*设施类型*/设施_箭楼, /*x*/74, /*y*/59},
	/*易京*/ {/*设施类型*/设施_箭楼, /*x*/136, /*y*/26},
	/*雍丘*/ {/*设施类型*/设施_箭楼, /*x*/113, /*y*/85},
	/*陈*/ {/*设施类型*/设施_阵, /*x*/108, /*y*/100},
	/*临淄*/ {/*设施类型*/设施_阵, /*x*/160, /*y*/57},
	/*琅邪*/ {/*设施类型*/设施_阵, /*x*/158, /*y*/70},
	/*石亭*/ {/*设施类型*/设施_箭楼, /*x*/143, /*y*/111},
	/*巨鹿*/ {/*设施类型*/设施_阵, /*x*/112, /*y*/41},
	/*涪县*/ {/*设施类型*/设施_箭楼, /*x*/25, /*y*/117},
	/*巴邱*/ {/*设施类型*/设施_箭楼, /*x*/105, /*y*/152},
	/*彭城*/ {/*设施类型*/设施_阵, /*x*/148, /*y*/79},
	/*襄垣*/ {/*设施类型*/设施_箭楼, /*x*/78, /*y*/52},
	/*丸都*/ {/*设施类型*/设施_箭楼, /*x*/184, /*y*/5},
	/*昌黎*/ {/*设施类型*/设施_阵, /*x*/153, /*y*/2},
	/*渔阳*/ {/*设施类型*/设施_阵, /*x*/127, /*y*/5},
	/*涿*/ {/*设施类型*/设施_阵, /*x*/115, /*y*/26},
	/*河间*/ {/*设施类型*/设施_阵, /*x*/130, /*y*/31},
	/*乐陵*/ {/*设施类型*/设施_阵, /*x*/140, /*y*/44},
	/*清河*/ {/*设施类型*/设施_阵, /*x*/130, /*y*/47},
	/*雁门*/ {/*设施类型*/设施_阵, /*x*/90, /*y*/20},
	/*太原*/ {/*设施类型*/设施_阵, /*x*/84, /*y*/26},
	/*长子*/ {/*设施类型*/设施_箭楼, /*x*/90, /*y*/51},
	/*东莱*/ {/*设施类型*/设施_阵, /*x*/169, /*y*/50},
	/*即墨*/ {/*设施类型*/设施_箭楼, /*x*/164, /*y*/47},
	/*东武*/ {/*设施类型*/设施_箭楼, /*x*/157, /*y*/60},
	/*东阿*/ {/*设施类型*/设施_箭楼, /*x*/131, /*y*/67},
	/*定陶*/ {/*设施类型*/设施_箭楼, /*x*/124, /*y*/80},
	/*中牟*/ {/*设施类型*/设施_箭楼, /*x*/93, /*y*/83},
	/*颍川*/ {/*设施类型*/设施_阵, /*x*/92, /*y*/94},
	/*宋*/ {/*设施类型*/设施_阵, /*x*/122, /*y*/100},
	/*阳安*/ {/*设施类型*/设施_箭楼, /*x*/104, /*y*/101},
	/*朝歌*/ {/*设施类型*/设施_箭楼, /*x*/80, /*y*/62},
	/*平阳*/ {/*设施类型*/设施_阵, /*x*/67, /*y*/39},
	/*河南*/ {/*设施类型*/设施_阵, /*x*/75, /*y*/78},
	/*商*/ {/*设施类型*/设施_箭楼, /*x*/66, /*y*/85},
	/*郿*/ {/*设施类型*/设施_箭楼, /*x*/43, /*y*/74},
	/*赤岸*/ {/*设施类型*/设施_箭楼, /*x*/24, /*y*/77},
	/*榆中*/ {/*设施类型*/设施_箭楼, /*x*/10, /*y*/53},
	/*令居*/ {/*设施类型*/设施_箭楼, /*x*/19, /*y*/37},
	/*张掖*/ {/*设施类型*/设施_阵, /*x*/6, /*y*/33},
	/*酒泉*/ {/*设施类型*/设施_阵, /*x*/11, /*y*/28},
	/*寻阳*/ {/*设施类型*/设施_箭楼, /*x*/146, /*y*/118},
	/*阜陵*/ {/*设施类型*/设施_箭楼, /*x*/146, /*y*/97},
	/*丹阳*/ {/*设施类型*/设施_阵, /*x*/164, /*y*/124},
	/*武昌*/ {/*设施类型*/设施_阵, /*x*/109, /*y*/143},
	/*南昌*/ {/*设施类型*/设施_阵, /*x*/115, /*y*/141},
	/*穰县*/ {/*设施类型*/设施_箭楼, /*x*/68, /*y*/99},
	/*新城*/ {/*设施类型*/设施_阵, /*x*/68, /*y*/118},
	/*宜都*/ {/*设施类型*/设施_阵, /*x*/77, /*y*/121},
	/*安陆*/ {/*设施类型*/设施_箭楼, /*x*/110, /*y*/118},
	/*衡阳*/ {/*设施类型*/设施_箭楼, /*x*/80, /*y*/164},
	/*魏兴*/ {/*设施类型*/设施_阵, /*x*/57, /*y*/98},
	/*昆阳*/ {/*设施类型*/设施_箭楼, /*x*/99, /*y*/96},
	/*益阳*/ {/*设施类型*/设施_箭楼, /*x*/74, /*y*/161},
	/*安阳*/ {/*设施类型*/设施_箭楼, /*x*/47, /*y*/103},
	/*汉寿*/ {/*设施类型*/设施_箭楼, /*x*/27, /*y*/107},
	/*汉昌*/ {/*设施类型*/设施_箭楼, /*x*/36, /*y*/111},
	/*江油*/ {/*设施类型*/设施_箭楼, /*x*/14, /*y*/104},
	/*都安*/ {/*设施类型*/设施_箭楼, /*x*/1, /*y*/118},
	/*广汉*/ {/*设施类型*/设施_阵, /*x*/22, /*y*/132},
	/*临江*/ {/*设施类型*/设施_箭楼, /*x*/34, /*y*/136},
	/*江阳*/ {/*设施类型*/设施_阵, /*x*/19, /*y*/145},
	/*朱提*/ {/*设施类型*/设施_阵, /*x*/23, /*y*/160},
	/*且兰*/ {/*设施类型*/设施_箭楼, /*x*/51, /*y*/167},
	/*宛温*/ {/*设施类型*/设施_箭楼, /*x*/37, /*y*/185},
	/*台登*/ {/*设施类型*/设施_箭楼, /*x*/5, /*y*/144},
	/*章陵*/ {/*设施类型*/设施_箭楼, /*x*/105, /*y*/116},
	/*舞阴*/ {/*设施类型*/设施_箭楼, /*x*/98, /*y*/105},
	/*犍为*/ {/*设施类型*/设施_阵, /*x*/11, /*y*/139},
	/*义阳*/ {/*设施类型*/设施_阵, /*x*/93, /*y*/107},
	/*六安*/ {/*设施类型*/设施_箭楼, /*x*/135, /*y*/109},
	/*匡亭*/ {/*设施类型*/设施_箭楼, /*x*/109, /*y*/70},
	/*封丘*/ {/*设施类型*/设施_箭楼, /*x*/106, /*y*/74},
	/*耒阳*/ {/*设施类型*/设施_箭楼, /*x*/106, /*y*/180},
	/*临武*/ {/*设施类型*/设施_箭楼, /*x*/87, /*y*/186},
	/*建平*/ {/*设施类型*/设施_阵, /*x*/47, /*y*/133},
	/*邯郸*/ {/*设施类型*/设施_箭楼, /*x*/107, /*y*/48},
	/*庐陵*/ {/*设施类型*/设施_阵, /*x*/127, /*y*/180},
	/*戈阳*/ {/*设施类型*/设施_阵, /*x*/128, /*y*/104},
	/*梁*/ {/*设施类型*/设施_阵, /*x*/123, /*y*/87},
	/*武功*/ {/*设施类型*/设施_箭楼, /*x*/46, /*y*/68},
	/*荣阳*/ {/*设施类型*/设施_阵, /*x*/90, /*y*/76},
	/*上圭*/ {/*设施类型*/设施_箭楼, /*x*/19, /*y*/77},
	/*汉嘉*/ {/*设施类型*/设施_阵, /*x*/0, /*y*/137},
	/*扶风*/ {/*设施类型*/设施_阵, /*x*/54, /*y*/80},
	/*冯翔*/ {/*设施类型*/设施_阵, /*x*/49, /*y*/71},
	/*新平*/ {/*设施类型*/设施_阵, /*x*/36, /*y*/40},
	/*阴盘*/ {/*设施类型*/设施_箭楼, /*x*/29, /*y*/61},
	/*槐里*/ {/*设施类型*/设施_箭楼, /*x*/41, /*y*/69},
	/*阿阳*/ {/*设施类型*/设施_箭楼, /*x*/10, /*y*/60},
	/*乌枝*/ {/*设施类型*/设施_箭楼, /*x*/20, /*y*/55},
	/*滇池*/ {/*设施类型*/设施_阵, /*x*/17, /*y*/188},
	/*蕲春*/ {/*设施类型*/设施_箭楼, /*x*/122, /*y*/112},
	/*临沮*/ {/*设施类型*/设施_箭楼, /*x*/62, /*y*/118},
	/*秭归*/ {/*设施类型*/设施_箭楼, /*x*/60, /*y*/122},
	/*辰阳*/ {/*设施类型*/设施_箭楼, /*x*/66, /*y*/164},
	/*沅陵*/ {/*设施类型*/设施_箭楼, /*x*/75, /*y*/167},
	/*烝阳*/ {/*设施类型*/设施_箭楼, /*x*/76, /*y*/174},
	/*充*/ {/*设施类型*/设施_箭楼, /*x*/61, /*y*/151},
	/*始安*/ {/*设施类型*/设施_箭楼, /*x*/58, /*y*/182},
	/*都梁*/ {/*设施类型*/设施_箭楼, /*x*/61, /*y*/170},
	/*临烝*/ {/*设施类型*/设施_箭楼, /*x*/80, /*y*/186},
	/*汉宁*/ {/*设施类型*/设施_箭楼, /*x*/118, /*y*/181},
	/*曲汉*/ {/*设施类型*/设施_箭楼, /*x*/116, /*y*/193},
	/*临贺*/ {/*设施类型*/设施_阵, /*x*/104, /*y*/194},
	/*临湘*/ {/*设施类型*/设施_箭楼, /*x*/106, /*y*/171},
	/*南城*/ {/*设施类型*/设施_箭楼, /*x*/123, /*y*/174},
	/*云社*/ {/*设施类型*/设施_箭楼, /*x*/101, /*y*/112},
	/*溧阳*/ {/*设施类型*/设施_箭楼, /*x*/184, /*y*/125},
	/*句容*/ {/*设施类型*/设施_箭楼, /*x*/190, /*y*/122},
	/*海盐*/ {/*设施类型*/设施_箭楼, /*x*/157, /*y*/138},
	/*钱唐*/ {/*设施类型*/设施_箭楼, /*x*/155, /*y*/145},
	/*始新*/ {/*设施类型*/设施_箭楼, /*x*/151, /*y*/134},
	/*黟*/ {/*设施类型*/设施_箭楼, /*x*/161, /*y*/142},
	/*富春*/ {/*设施类型*/设施_箭楼, /*x*/147, /*y*/149},
	/*勤*/ {/*设施类型*/设施_箭楼, /*x*/155, /*y*/160},
	/*永宁*/ {/*设施类型*/设施_箭楼, /*x*/146, /*y*/165},
	/*太末*/ {/*设施类型*/设施_箭楼, /*x*/140, /*y*/159},
	/*育阳*/ {/*设施类型*/设施_箭楼, /*x*/79, /*y*/102},
	/*夏丘*/ {/*设施类型*/设施_箭楼, /*x*/156, /*y*/86},
	/*盱台*/ {/*设施类型*/设施_箭楼, /*x*/160, /*y*/81},
	/*郯*/ {/*设施类型*/设施_箭楼, /*x*/164, /*y*/71},
	/*东莞*/ {/*设施类型*/设施_阵, /*x*/152, /*y*/63},
	/*离狐*/ {/*设施类型*/设施_箭楼, /*x*/119, /*y*/75},
	/*燕*/ {/*设施类型*/设施_阵, /*x*/122, /*y*/72},
	/*鄄城*/ {/*设施类型*/设施_箭楼, /*x*/136, /*y*/69},
	/*昌邑*/ {/*设施类型*/设施_箭楼, /*x*/134, /*y*/77},
	/*鲁*/ {/*设施类型*/设施_阵, /*x*/146, /*y*/74},
	/*萧*/ {/*设施类型*/设施_箭楼, /*x*/141, /*y*/82},
	/*砀*/ {/*设施类型*/设施_箭楼, /*x*/136, /*y*/84},
	/*鄢陵*/ {/*设施类型*/设施_箭楼, /*x*/103, /*y*/87},
	/*酸枣*/ {/*设施类型*/设施_箭楼, /*x*/95, /*y*/76},
	/*尉氏*/ {/*设施类型*/设施_箭楼, /*x*/99, /*y*/81},
	/*围*/ {/*设施类型*/设施_箭楼, /*x*/107, /*y*/85},
	/*长社*/ {/*设施类型*/设施_箭楼, /*x*/93, /*y*/89},
	/*阳夏*/ {/*设施类型*/设施_箭楼, /*x*/112, /*y*/92},
	/*新汲*/ {/*设施类型*/设施_箭楼, /*x*/104, /*y*/96},
	/*阳翟*/ {/*设施类型*/设施_箭楼, /*x*/90, /*y*/87},
	/*召陵*/ {/*设施类型*/设施_箭楼, /*x*/109, /*y*/103},
	/*汝阴*/ {/*设施类型*/设施_阵, /*x*/118, /*y*/104},
	/*上蔡*/ {/*设施类型*/设施_箭楼, /*x*/104, /*y*/108},
	/*朗陵*/ {/*设施类型*/设施_箭楼, /*x*/115, /*y*/111},
	/*鲁阳*/ {/*设施类型*/设施_箭楼, /*x*/85, /*y*/88},
	/*顺阳*/ {/*设施类型*/设施_箭楼, /*x*/73, /*y*/91},
	/*棘阳*/ {/*设施类型*/设施_箭楼, /*x*/77, /*y*/97},
	/*筑阳*/ {/*设施类型*/设施_箭楼, /*x*/62, /*y*/99},
	/*锡*/ {/*设施类型*/设施_箭楼, /*x*/47, /*y*/98},
	/*汉丰*/ {/*设施类型*/设施_箭楼, /*x*/50, /*y*/126},
	/*宕渠*/ {/*设施类型*/设施_箭楼, /*x*/41, /*y*/125},
	/*固陵*/ {/*设施类型*/设施_箭楼, /*x*/41, /*y*/132},
	/*北井*/ {/*设施类型*/设施_箭楼, /*x*/54, /*y*/123},
	/*钟离*/ {/*设施类型*/设施_箭楼, /*x*/137, /*y*/88},
	/*当涂*/ {/*设施类型*/设施_箭楼, /*x*/141, /*y*/90},
	/*下蔡*/ {/*设施类型*/设施_箭楼, /*x*/133, /*y*/95},
	/*安丰*/ {/*设施类型*/设施_阵, /*x*/130, /*y*/100},
	/*成皋*/ {/*设施类型*/设施_箭楼, /*x*/82, /*y*/72},
	/*新郑*/ {/*设施类型*/设施_箭楼, /*x*/84, /*y*/79},
	/*侯氏*/ {/*设施类型*/设施_箭楼, /*x*/79, /*y*/79},
	/*平阴*/ {/*设施类型*/设施_箭楼, /*x*/74, /*y*/74},
	/*漆*/ {/*设施类型*/设施_箭楼, /*x*/37, /*y*/59},
	/*渝糜*/ {/*设施类型*/设施_箭楼, /*x*/24, /*y*/66},
	/*下辨*/ {/*设施类型*/设施_箭楼, /*x*/14, /*y*/75},
	/*兴势*/ {/*设施类型*/设施_箭楼, /*x*/35, /*y*/85},
	/*蓝田*/ {/*设施类型*/设施_箭楼, /*x*/49, /*y*/77},
	/*三水*/ {/*设施类型*/设施_箭楼, /*x*/24, /*y*/43},
	/*休屠*/ {/*设施类型*/设施_箭楼, /*x*/1, /*y*/21},
	/*仓松*/ {/*设施类型*/设施_箭楼, /*x*/9, /*y*/46},
	/*祖厉*/ {/*设施类型*/设施_箭楼, /*x*/15, /*y*/47},
	/*褒中*/ {/*设施类型*/设施_箭楼, /*x*/31, /*y*/83},
	/*成固*/ {/*设施类型*/设施_箭楼, /*x*/22, /*y*/90},
	/*安阳*/ {/*设施类型*/设施_箭楼, /*x*/32, /*y*/91},
	/*德阳*/ {/*设施类型*/设施_箭楼, /*x*/28, /*y*/130},
	/*焚道*/ {/*设施类型*/设施_箭楼, /*x*/5, /*y*/151},
	/*资中*/ {/*设施类型*/设施_箭楼, /*x*/12, /*y*/125},
	/*汉安*/ {/*设施类型*/设施_箭楼, /*x*/17, /*y*/129},
	/*武阳*/ {/*设施类型*/设施_箭楼, /*x*/4, /*y*/132},
	/*南安*/ {/*设施类型*/设施_箭楼, /*x*/0, /*y*/142},
	/*垫江*/ {/*设施类型*/设施_箭楼, /*x*/19, /*y*/136},
	/*符节*/ {/*设施类型*/设施_箭楼, /*x*/26, /*y*/140},
	/*会无*/ {/*设施类型*/设施_箭楼, /*x*/4, /*y*/169},
	/*平夷*/ {/*设施类型*/设施_箭楼, /*x*/22, /*y*/168},
	/*姑复*/ {/*设施类型*/设施_箭楼, /*x*/6, /*y*/178},
	/*弄栋*/ {/*设施类型*/设施_箭楼, /*x*/10, /*y*/183},
	/*双柏*/ {/*设施类型*/设施_箭楼, /*x*/23, /*y*/195},
	/*贲古*/ {/*设施类型*/设施_箭楼, /*x*/53, /*y*/195},
	/*安邑*/ {/*设施类型*/设施_箭楼, /*x*/66, /*y*/52},
	/*界休*/ {/*设施类型*/设施_箭楼, /*x*/91, /*y*/57},
	/*云中*/ {/*设施类型*/设施_阵, /*x*/81, /*y*/34},
	/*雍奴*/ {/*设施类型*/设施_箭楼, /*x*/131, /*y*/21},
	/*中山*/ {/*设施类型*/设施_阵, /*x*/116, /*y*/31},
	/*高阳*/ {/*设施类型*/设施_阵, /*x*/125, /*y*/26},
	/*婴陶*/ {/*设施类型*/设施_箭楼, /*x*/111, /*y*/34},
	/*房子*/ {/*设施类型*/设施_箭楼, /*x*/103, /*y*/43},
	/*荡阴*/ {/*设施类型*/设施_箭楼, /*x*/109, /*y*/58},
	/*阴安*/ {/*设施类型*/设施_箭楼, /*x*/117, /*y*/60},
	/*曲梁*/ {/*设施类型*/设施_箭楼, /*x*/114, /*y*/52},
	/*博平*/ {/*设施类型*/设施_箭楼, /*x*/125, /*y*/50},
	/*乐平*/ {/*设施类型*/设施_阵, /*x*/127, /*y*/59},
	/*西平昌*/ {/*设施类型*/设施_箭楼, /*x*/138, /*y*/48},
	/*东光*/ {/*设施类型*/设施_箭楼, /*x*/134, /*y*/41},
	/*信都*/ {/*设施类型*/设施_箭楼, /*x*/127, /*y*/40},
	/*豫章*/ {/*设施类型*/设施_阵, /*x*/122, /*y*/188},
	/*乐成*/ {/*设施类型*/设施_箭楼, /*x*/134, /*y*/34},
	/*章武*/ {/*设施类型*/设施_箭楼, /*x*/140, /*y*/31},
	/*徐无*/ {/*设施类型*/设施_箭楼, /*x*/136, /*y*/12},
	/*土垠*/ {/*设施类型*/设施_箭楼, /*x*/142, /*y*/22},
	/*令支*/ {/*设施类型*/设施_箭楼, /*x*/153, /*y*/12},
	/*辽阳*/ {/*设施类型*/设施_箭楼, /*x*/172, /*y*/3},
	/*三江口*/ {/*设施类型*/设施_箭楼, /*x*/123, /*y*/128},
	/*吴兴*/ {/*设施类型*/设施_阵, /*x*/140, /*y*/171},
	/*阳信*/ {/*设施类型*/设施_箭楼, /*x*/142, /*y*/39}
};

const array<string>特殊地名设施名称 =
{
	"金城",
	"陇西",
	"武都",
	"阴平",
	"五丈原",
	"涪陵",
	"牂牁",
	"华容",
	"河东",
	"乌巢",
	"野王",
	"易京",
	"雍丘",
	"陈",
	"临淄",
	"琅邪",
	"石亭",
	"巨鹿",
	"涪县",
	"巴邱",
	"彭城",
	"襄垣",
	"丸都",
	"昌黎",
	"渔阳",
	"涿",
	"河间",
	"乐陵",
	"清河",
	"雁门",
	"太原",
	"长子",
	"东莱",
	"即墨",
	"东武",
	"东阿",
	"定陶",
	"中牟",
	"颍川",
	"宋",
	"阳安",
	"朝歌",
	"平阳",
	"河南",
	"商",
	"郿",
	"赤岸",
	"榆中",
	"令居",
	"张掖",
	"酒泉",
	"寻阳",
	"阜陵",
	"丹阳",
	"武昌",
	"南昌",
	"穰县",
	"新城",
	"宜都",
	"安陆",
	"衡阳",
	"魏兴",
	"昆阳",
	"益阳",
	"安阳",
	"汉寿",
	"汉昌",
	"江油",
	"都安",
	"广汉",
	"临江",
	"江阳",
	"朱提",
	"且兰",
	"宛温",
	"台登",
	"章陵",
	"舞阴",
	"犍为",
	"义阳",
	"六安",
	"匡亭",
	"封丘",
	"耒阳",
	"临武",
	"建平",
	"邯郸",
	"庐陵",
	"戈阳",
	"梁",
	"武功",
	"荣阳",
	"上圭",
	"汉嘉",
	"扶风",
	"冯翔",
	"新平",
	"阴盘",
	"槐里",
	"阿阳",
	"乌枝",
	"滇池",
	"蕲春",
	"临沮",
	"秭归",
	"辰阳",
	"沅陵",
	"烝阳",
	"充",
	"始安",
	"都梁",
	"临烝",
	"汉宁",
	"曲汉",
	"临贺",
	"临湘",
	"南城",
	"云社",
	"溧阳",
	"句容",
	"海盐",
	"钱唐",
	"始新",
	"黟",
	"富春",
	"勤",
	"永宁",
	"太末",
	"育阳",
	"夏丘",
	"盱台",
	"郯",
	"东莞",
	"离狐",
	"燕",
	"鄄城",
	"昌邑",
	"鲁",
	"萧",
	"砀",
	"鄢陵",
	"酸枣",
	"尉氏",
	"围",
	"长社",
	"阳夏",
	"新汲",
	"阳翟",
	"召陵",
	"汝阴",
	"上蔡",
	"朗陵",
	"鲁阳",
	"顺阳",
	"棘阳",
	"筑阳",
	"锡",
	"汉丰",
	"宕渠",
	"固陵",
	"北井",
	"钟离",
	"当涂",
	"下蔡",
	"安丰",
	"成皋",
	"新郑",
	"侯氏",
	"平阴",
	"漆",
	"渝糜",
	"下辨",
	"兴势",
	"蓝田",
	"三水",
	"休屠",
	"仓松",
	"祖厉",
	"褒中",
	"成固",
	"安阳",
	"德阳",
	"焚道",
	"资中",
	"汉安",
	"武阳",
	"南安",
	"垫江",
	"符节",
	"会无",
	"平夷",
	"姑复",
	"弄栋",
	"双柏",
	"贲古",
	"安邑",
	"界休",
	"云中",
	"雍奴",
	"中山",
	"高阳",
	"婴陶",
	"房子",
	"荡阴",
	"阴安",
	"曲梁",
	"博平",
	"乐平",
	"西平昌",
	"东光",
	"信都",
	"豫章",
	"乐成",
	"章武",
	"徐无",
	"土垠",
	"令支",
	"辽阳",
	"三江口",
	"吴兴",
	"阳信"
};

