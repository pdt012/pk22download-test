// ## 2023/07/25 # 黑店小小二 # 添加draw_box，draw_face函数 ##


/*
绘制窗体工具类
draw_box: 创建普通盒子
draw_face: 创建头像。头像带花纹
*/
namespace draw_dialog_tool
{
  /*
    0 image 框
    2 sprite9 角
    5 button 大按钮
    6 text 文本
    7 custon 图片
    8 button 按钮
    9 sprite0 线
  */
 
  class Main
  {
    
    pk::text@ map_bottom_text;

    Main()
    {
      pk::bind(210, pk::trigger210_t(dialog_init));
    }

    void dialog_init(pk::dialog@ dlg, int dlg_id)
    {
      if (dlg_id == 119) // 大地图底部文字
      {
          if (dlg.find_child(5391) !is null)
        {
          //pk::info('获取到text');
          @ map_bottom_text = dlg.find_child(5391);
        }
      }
    }
  }

  Main main;

  /**
   * 创建普通盒子
   * @param {@Dialog} dialog     必传。窗体对象。
   * @param {Int} box_size       必传。窗体大小。窗体尺寸包含线宽高跟角大小。box_size[0]为宽度。box_size[1]为高度。
   * @param {Array<int>} origin     非必传。窗体坐标原点，即左上角的位置
   * @param {Array<int>} line_type  非必传。线风格。按上下左右排序，默认按原版风格
   * @param {Array<int>} angle_type 非必传。角大小。按左上，右上，左下，右下排序，默认按原版风格
   * @param {Int} line_size  非必传。线大小。用于横线的高度，竖线的宽度。默认跟原版一致8尺寸单位
   * @param {Int}angle_size  非必传。角大小。默认跟原版一致8尺寸单位
   * @return {Array<int>}    返回实际内容尺寸。{宽, 高}
   */
  array<int> draw_box(
    pk::dialog@ dialog,
    array<int> box_size = { /*width*/0, /*height*/0 },
    array<int> origin = { 0, 0 },
    array<int> line_type = { /*上*/428, /*下*/428, /*左*/435, /*右*/435 },
    array<int> angle_type = { /*左上*/316, /*右上*/314, /*左下*/315, /*右下*/313 },
    int line_size = 8,
    int angle_size = 8
  )
  {
    if (dialog is null) return {};
    if (box_size[0] <= 0 || box_size[1] <= 0) return {};
    if (line_type.length == 0) return {};
    
    int line_hor_width = int(box_size[0] - 2 * angle_size);
    int line_hor_height = line_size;
    int line_vert_width = line_size;
    int line_vert_height = int(box_size[1] - 2 * angle_size);
    int origin_x = origin[0];
    int origin_y = origin[1];

    
    pk::sprite0@ sp0;

    pk::size angle_size_data = pk::size(angle_size, angle_size);

    if (angle_type.length > 0)
    {
      pk::image@ im0;
     //左上
      @ im0 = dialog.create_image(angle_type[0]);
      im0.set_pos(origin_x, origin_y);
      im0.set_size(angle_size_data);
      // 右上
      @ im0 = dialog.create_image(angle_type[1]);
      im0.set_pos(origin_x + angle_size + line_hor_width, origin_y);
      im0.set_size(angle_size_data);
      // 左下
      @ im0 = dialog.create_image(angle_type[2]);
      im0.set_pos(origin_x, origin_y + angle_size + line_vert_height);
      im0.set_size(angle_size_data);
      // 右下
      @ im0 = dialog.create_image(angle_type[3]);
      im0.set_pos(origin_x + angle_size + line_hor_width, origin_y + angle_size + line_vert_height);
      im0.set_size(angle_size_data); 
    }
    

    // 上横线
    @ sp0 = dialog.create_sprite0(line_type[0]);
    sp0.set_pos(origin_x + angle_size, origin_y);
    sp0.set_size(line_hor_width, line_hor_height);
    // 下横线
    @ sp0 = dialog.create_sprite0(line_type[1]);
    // (原点横坐标 + 角大小，原点纵坐标 + 竖线高度 + 角大小/上横线高度)
    sp0.set_pos(origin_x + angle_size, origin_y + line_vert_height + line_hor_height);
    sp0.set_size(line_hor_width, line_hor_height);
    //左竖线
    @ sp0 = dialog.create_sprite0(line_type[2]);
    sp0.set_pos(origin_x, origin_y + angle_size);
    sp0.set_size(line_vert_width, line_vert_height);
    //右竖线
    @ sp0 = dialog.create_sprite0(line_type[3]);
    sp0.set_pos(origin_x + line_vert_width + line_hor_width, origin_y + angle_size);
    sp0.set_size(line_vert_width, line_vert_height);

    return { box_size[0] - angle_size, box_size[1] - angle_size };
  }


  /**
   * 绘制头像（带花纹）
   * @param {@Dialog} dialog      必传。窗体对象。
   * @param {Array<int>} box_size 必传。盒子尺寸{宽度，高度}
   * @param {Array<int>} box_pos  非必传。盒子位置{x，y}
   * @param {Int} face_value      头像内容
   * @param {Int} face_type       头像类别
   * @param {Int} face_border_style 盒子边框花纹
   * @param {Int} face_padding      盒子内边距
   * @return pk::face@               头像类
   */
  pk::face@ draw_face(
    pk::dialog@ dialog,
    array<int> box_size = { /*width*/100, /*height*/100 },
    array<int> box_pos = { /*x*/0, /*y*/0 },
    int face_value = -1,
    int face_type = 2,
    int face_border_style = 375,
    int face_padding = 10
  )
  {
    pk::sprite9@ bg0;
    pk::face@ fac0;
    @ bg0 = dialog.create_sprite9(face_border_style);
    bg0.set_pos(box_pos[0], box_pos[1]);
    bg0.set_size(box_size[0], box_size[1]);

    @ fac0 = dialog.create_face();
    pk::size img_size = pk::size(box_size[0] - face_padding, box_size[1] - face_padding);
    fac0.set_face_size(img_size);
    fac0.set_pos(box_pos[0] + int(face_padding / 2), box_pos[1] + int(face_padding / 2));
    fac0.set_face_type(face_type);
    fac0.set_face(face_value);
    return @fac0;
  }

  /**
   * 设置底部内容提示
   * @param {String} 提示内容
   */
  void set_map_desc(string desc)
  {
    main.map_bottom_text.set_text(pk::encode(desc));
  }
}
