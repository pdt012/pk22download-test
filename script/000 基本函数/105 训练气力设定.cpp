﻿// ## 2023/05/03 # 江东新风 # 函数主体自305迁回 ##
// ## 2022/11/01 # 铃 # 在新的自动化内政中,训练也成为自动化,为新的内政系统设定参数和算法 ##
// ## 2022/08/14 # 铃 # 在新的人口系统中,训练的逻辑也发生了很大变化,重做相关函数。 ##
// ## 2021/10/01 # 江东新风 # namespace的韩文改成英文 ##
// ## 2021/09/15 # 江东新风 # 更改pk::core[]函数为英文##
// ## 2020/10/23 #江东新风#新特技 调练##
// ## 2020/07/26 ##
namespace DRILL
{
	// ================ 自动内政全局常量================

	class Main
	{
		Main()
		{
			pk::set_func(105, pk::func105_t(callback));
		}

		int callback(pk::building @building, const pk::detail::arrayptr<pk::person @> &in actors)
		{
	
			pk::list<pk::person@> actor_list;
			for (int i = 0; i < actors.length; i++)
			{
				actor_list.add(actors[i]);
			}

			return get_drill_energy_change(building, actor_list);
		}

		float get_exp_eff(int person_id)
		{
			if (pk::is_valid_person_id(person_id))
			{
				personinfo @person_t = @person_ex[person_id];
				if (person_t.drill_exp < 执政官经验一阶)
					return 1.0f;
				if (person_t.drill_exp < 执政官经验二阶)
					return 1.1f;
				if (person_t.drill_exp < 执政官经验三阶)
					return 1.2f;
				return 1.3f;
			}
			return 1.0f;
		}
	}

	Main main;

	int get_drill_energy_change(pk::building @building, pk::list<pk::person@> actors)
	{
		if (!pk::is_alive(building) or actors[0] is null)
			return 0;

		float n = 0;
		int max = pk::INT32_MIN;
		bool flag = false;

		for (int i = 0; i < actors.count; i++)
		{
			pk::person @actor = actors[i];
			if (pk::is_alive(actor))
			{
				int s = actor.stat[int(pk::core["train.stat"])];
				n += s;
				max = pk::max(max, s);
				if (ch::has_skill(actor, 特技_调练))
					flag = true;
			}
		}

		float exp_eff = 1.0f;
		float absent_eff = 1.0f;
		float expend_eff = 1.0f;
		if (ch::get_auto_affairs_status() and actors.count == 1)//只有在人数为1，开启自动内政，且有执政官经验时享受加成
		{
			exp_eff = main.get_exp_eff(actors[0].get_id());
			if (pk::is_absent(actors[0])) absent_eff = 0.8f;
			else if (actors[0].action_done) absent_eff = 0.9f;
			BuildingInfo @base_p = @building_ex[actors[0].service];

			float rate = ch::get_modify_rate(float(base_p.train_effic) / 100);
			expend_eff = pk::min(1.5f, rate);
		}

		n = int((n + max) * exp_eff * absent_eff * expend_eff / pk::min(building.get_troops() / 2000 + 20, 100) + 3);
		// 연병소가 있다면 1.5배
		if (building.facility == 设施_都市 and pk::has_facility(pk::building_to_city(building), 设施_练兵所))
			n = n * 1.5f;
		if (flag)
			n = n * pk::core::skill_constant_value(特技_调练) / 100;

		// 兵临城下训练效果仅有一半
		if (pk::enemies_around(building))
		{
			n = int(n * 0.5);
		}

		return int(n);
	}
}