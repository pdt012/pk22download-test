﻿// ## 2020/08/08 # 氕氘氚 # 去除部队一级菜单 ##
// ## 2022/06/19 # 铃 # 为新内政系统增加新的一级菜单，暂时注释掉早前的自动内政 ##
// ## 2021/09/26 # 江东新风 # 事件手动触发的一级菜单 ##
// ## 2021/01/18 # 江东新风 # 禁止拆建拼音改简体 ##
// ## 2020/12/15 # messi # 内政自动化改为太守自治 ##
// ## 2020/08/11 # 氕氘氚 # 修复太守不在时点击买卖可能出现的指針錯誤 ##
// ## 2020/08/08 # 氕氘氚 # 新增三个一级菜单 ##

namespace global_menu
{
    // ======================================================================


    const string shortcut_护送付虏 = "Q";
    const string shortcut_平障建设 = "W";
    const string shortcut_城墙维修 = "E";
    const string shortcut_焚城 = "R";

    const string shortcut_选拔 = "Q";
    const string shortcut_寻宝 = "W";
    const string shortcut_仙人探索 = "E";
    const string shortcut_连续探索 = "R";

    const string shortcut_成立联军 = "8";

    // ======================================================================


    int 菜单_开局部署 = -1;

    // ======================================================================

    class Main
    {

        pk::building @building_;
        pk::force @force_;
        pk::person @taishu_;
        pk::person @kunshu_;
        pk::city @city_;
        pk::gate @gate_;
        pk::port @port_;
        pk::district @district_;

        int menu_city_id_;
        int menu_force_id_;

        Main()
        {
            // shift-右键菜单================================================================================
            pk::menu_item menu_开局部署;
            menu_开局部署.menu = 2;
            menu_开局部署.get_text = cast<pk::menu_item_get_text_t@>(function() { return pk::encode("开局部署"); });
            menu_开局部署.get_desc = cast<pk::menu_item_get_desc_t@>(function() { return pk::encode("开局时重新部署武将或势力"); });

            菜单_开局部署 = pk::add_menu_item(menu_开局部署);
        }

   

    }

    Main main;
}
