﻿// ## 2023/05/30 # 江东新风 # 攻占后计算过慢，改为原算法 ##
// ## 2023/05/17 # 江东新风 # 解决短期多次攻占，人口锐减问题 ##
// ## 2023/03/09 # 铃 # 发生两次攻破后会出现人口锐减且无法回归的问题,要改的话太麻烦了.累加的话又会导致人口波动特别大,先取消掉后面再想办法 ##
// ## 2023/03/09 # 铃 # 攻破城市时重置所有的内政官.
// ## 2021/10/29 # 江东新风 # 结构体存储调用方式改进 ##
// ## 2021/10/28 # 江东新风 # 单港关存活设定变更 ##
// ## 2021/10/10 # 江东新风 # 据点攻占后的人口增减 ##
// ## 2021/10/01 # 江东新风 # namespace的韩文改成英文 ##
// ## 2021/02/18 # 江东新风 # 调整吸收比例为魅力相关，最大20% ##
// ## 2021/02/18 # 江东新风 # 尝试修复单港关bug ##
// ## 2021/02/08 # 江东新风 # 单港关时，势力灭亡条件的修正，城市被占领后留有港关也可存活 ##
// ## 2021/01/24 # 江东新风 # 关闭单城市灭亡 ##
// ## 2021/01/14 # 江东新风 # 关掉trace ##
// ## 2021/01/05 # 江东新风 # 特殊地名设施争夺的完善2 ##
// ## 2020/12/26 # 江东新风 # 关掉trace ##
// ## 2020/12/25 # 江东新风 # 据点灭亡修正 ##
// ## 2020/12/23 # 江东新风 # 加入据点灭亡开关 ##
// ## 2020/12/17 # 江东新风 # 实验添加据点为势力最后据点时，势力灭亡，配合港口势力不灭亡的设定 ##
// ## 2020/08/14 # 江东新风 # 新特技-安民 ##
// ## 2020/08/05 # messi # 調低?領据点获取物資的参数 ##
// ## 2020/08/01 ##
namespace KANRAKU
{
	class Main
	{
		Main()
		{
			pk::set_func(168, pk::func168_t(callback));
		}

		void callback(pk::building@ base, pk::unit@ attacker)
		{
			pk::person@ leader;

			if (pk::is_alive(attacker))
				@leader = pk::get_person(attacker.leader);

			int n = 5;
			int charisma = 20;

			if (pk::is_alive(leader))
			{
				charisma = leader.stat[武将能力_魅力];
				n = pk::max(charisma / 10, 5);
			}
			//破城后重置所有的内政官
			BaseInfo @base_t = @base_ex[base.get_id()];			
			base_t.recruit_person = -1;
			base_t.inspections_person = -1;
			base_t.drill_person = -1;
			base_t.produce_person = -1;

			func_人口处理(base,attacker,base_t, charisma);

			if (开启小兵拔擢)
			{
				if (pk::rand_bool(50)) batt::generate_talent_seed(leader);
			}

			pk::set_gold(base, pk::get_gold(base) * n / 100);
			pk::set_food(base, pk::get_food(base) * n / 100);
			pk::set_troops(base, pk::get_troops(base) * n / 100);
			for (int i = 0; i < 兵器_末; i++)
				pk::set_weapon_amount(base, i, pk::get_weapon_amount(base, i) * n / 100);

			pk::city@ city = pk::building_to_city(base);

			//如果不是城市的话，就到此为止 도시가 아닌경우 여기서 종료
			if (!pk::is_alive(city))
				return;

			int city_id = city.get_id();
			int city_force_id = city.get_force_id();
			pk::list<pk::building@> city_devs;
			pk::list<pk::building@> buildings = pk::get_building_list();
			int building_id = -1;
			int spec_id = -1;
			for (int i = 0; i < buildings.size; i++)
			{
				pk::building@ building = buildings[i];
				if (pk::is_alive(building) and pk::get_city_id(building.get_pos()) == city_id)
				{
					switch (pk::get_facility_type(building))
					{
					case 设施类型_内政设施:
						if (building.completed)
						{
							//已建成的内政设施如果不是铜雀台，将被随意破坏。 건설 완료된 내정시설은 동작대가 아니라면 무작위로 파괴.
							if (building.facility != 设施_铜雀台)//铜雀台
								city_devs.push_back(building);
						}
						else
						{
							//如果正在建设中的铜雀台受到破坏，铜雀宝物也将消失 건설중인 동작대가 파괴된 경우 동작 보물도 사라짐.
							if (building.facility == 设施_铜雀台)
							{
								pk::item@ item = pk::get_item(宝物_铜雀);
								if (pk::is_alive(item))
									pk::kill(item);
							}
							//正在建设中的内政设施全部遭到破坏。 건설중인 내정시설은 모두 파괴.
							else
							{
								pk::kill(building, false);
							}
						}
						break;

					case 设施类型_军事设施://军事设施
						building_id = building.get_id();
						spec_id = ch::to_spec_id(building_id);
						//处理地名设施
						if (ch::is_valid_spec_id(spec_id))
						{
							if (特定地点特定设施可争夺)
							{
								if (city_force_id == building.get_force_id())//只处理与被攻占城市相同势力的地名设施
								{
									//pk::trace(pk::format("pos 1,building_id:{}",building_id ));
									//pk::set_district(building, pk::get_district(attacker.get_district_id()), /*占领*/0);
									building.init_force = attacker.get_force_id();
									ch::reset_spec(spec_id);
								}
							}
						}
						else
						{
							//占领区域内城市所属势力以及其他势力的军事设施都将遭到破坏 구역 내 도시를 점령하고 있던 세력과 같은 세력의 군사시설은 모두 파괴
							if (pk::is_valid_force_id(city_force_id) and city_force_id == building.get_force_id())
								pk::kill(building, false);
						}
						break;
					}
				}
			}

			n = city_devs.size;
			if (pk::is_valid_normal_force_id(attacker.get_force_id()))
				n = n - pk::min(charisma / 20, n);
			city_devs.shuffle();
			for (int i = 0; i < n; i++)
				pk::kill(city_devs[i], false);
		}

		void func_人口处理(pk::building@ base, pk::unit@ attacker, BaseInfo@ base_t, int charisma)
		{
			if (开启人口系统)
			{
				int base_id = base.get_id();
				//攻击方将领魅力的影响
				float leader_eff;
				if (charisma < 30) leader_eff = -0.3f;
				else if (charisma < 40) leader_eff = -0.2f;
				else if (charisma < 50) leader_eff = -0.1f;
				else if (charisma < 80) leader_eff = 0;
				else if (charisma < 90) leader_eff = 0.1f;
				else if (charisma < 100) leader_eff = 0.2f;
				else leader_eff = 0.3f;
				if (ch::has_skill(attacker, 特技_安民)) leader_eff += 0.1f;
				if (base_id > -1 and base_id < 据点_城市末)
				{
					//BaseInfo@ base_t = @base_ex[base_id];
					//考虑短期多次攻占影响，则应引入return_timer作为参数，此数字大，代表短期内曾被攻占，应减少人口损失
					float mutiple_capture_eff = (base_t.return_timer > 1 ? 0.1f : 1.0f);
					int reduce_population = pk::min(base_t.population - 5000, int(base_t.population * (1 - 0.5f - leader_eff) * mutiple_capture_eff));

					base_t.return_pop = uint32(pk::min(800000, base_t.return_pop + reduce_population));//返回人口的设定
					base_t.population = base_t.population - reduce_population;

					int reduce_mil_pop_all = int(base_t.mil_pop_all * (1 - 0.5f - leader_eff) * mutiple_capture_eff);
					int reduce_mil_pop_av = int(base_t.mil_pop_av * (1 - 0.5f - leader_eff) * mutiple_capture_eff);

					base_t.mil_pop_all -= reduce_mil_pop_all;
					base_t.mil_pop_av -= reduce_mil_pop_av;
					//添加据点被攻占后标志，达成条件后可产生人口恢复buf
					// 9回合城市建筑量达7个，触发buf，人口回归
					base_t.return_timer = 9;
					base_t.return_buf = false;

				}
				else if (base_id >= 据点_关卡开始 and base_id < 据点_据点末)
				{
					//BaseInfo@ base_t = @base_ex[base_id];
				}

			}

		}
	}

	Main main;
}