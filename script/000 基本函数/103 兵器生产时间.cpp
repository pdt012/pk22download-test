﻿// ## 2023/05/03 # 江东新风 # 函数主体改写 ##
// ## 2022/11/01 # 铃 # 在新的自动化内政中,生产也成为自动化,为新的内政系统设定参数和算法 ##
// ## 2022/08/14 # 铃 # 在新的人口系统中,生产的逻辑也发生了很大变化,重做相关函数。 ##
// ## 2021/10/11 # 江东新风 # 造船和工坊对应特技错误 ##
// ## 2021/10/01 # 江东新风 # namespace的韩文改成英文 ##
// ## 2021/09/15 # 江东新风 # 更改pk::core[]函数为英文##
// ## 2020/10/23 #江东新风#同步马术书大神的更新，添加城市数量惩罚，修复战役模式情况的nullptr错误##
// ## 2020/08/24 # 氕氘氚 # 常量修改 ##
// ## 2020/08/16 #江东新风#has_skill函数替換##
/*
@수정자 : 기마책사
@Update: '20.8.29  // 수정내용: 유저_도시수_패널티 추가, 캠페인에서는 커스텀 세팅 사용 불가하도록 수정
*/

namespace PRODUCE_TIME_COST
{
	const bool 玩家_城市数_惩罚 = false; // 유저세력에 대해서 도시수에 비례하여 생산량 디버프 (도시당 1% 감소)
	//---------------------------------------------------------------------------



	class Main
	{
		Main()
		{
			pk::set_func(103, pk::func103_t(callback));
		}

		int callback(const pk::detail::arrayptr<pk::person @> &in actors, int weapon_id)
		{
			pk::list<pk::person@> actor_list;
			for (int i = 0; i < actors.length; i++)
			{
				actor_list.add(actors[i]);
			}
			return get_produce_time(actor_list, weapon_id);
		}

		float get_exp_eff(int person_id, int weapon_id)
		{
			if (pk::is_valid_person_id(person_id))
			{
				personinfo @person_t = @person_ex[person_id];
				int exp = 0;
				if (weapon_id >= 兵器_走舸) exp = person_t.ship_exp;
				else  exp = person_t.siege_exp;

				if (exp < 执政官经验一阶)
					return 1.0f;
				if (exp < 执政官经验二阶)
					return 1.1f;
				if (exp < 执政官经验三阶)
					return 1.2f;
				return 1.3f;
			}
			return 1.0f;
		}

	} Main main;

	int get_produce_time(pk::list<pk::person@> actors, int weapon_id)
	{

		if (!pk::is_valid_equipment_id(weapon_id))
			return 0;
		if (weapon_id < 兵器_冲车)
			return 0;

		int n = 0, sum = 0, max = 0, skill_id = -1;
		bool has_skill = false;

		if (weapon_id == 兵器_冲车 or weapon_id == 兵器_井阑 or weapon_id == 兵器_投石 or weapon_id == 兵器_木兽)
			skill_id = int(pk::core["weapon_produce.workshop_skill"]);
		else if (weapon_id == 兵器_楼船 or weapon_id == 兵器_斗舰)
			skill_id = int(pk::core["weapon_produce.shipyard_skill"]);

		for (int i = 0; i < actors.count; i++)
		{
			pk::person @actor = actors[i];
			if (pk::is_alive(actor))
			{
				int s = actor.stat[int(pk::core["weapon_produce.stat"])];
				sum = sum + s;
				max = pk::max(max, s);
				if (ch::has_skill(actor, skill_id))
					has_skill = true;
			}
		}

		int top = 生产时长上限;

		if (weapon_id >= 兵器_走舸)
		{
			pk::person @actor = actors[0];
			if (pk::is_alive(actor))
			{
				pk::building @building = pk::get_building(actor.service);
				if (pk::is_alive(building))
				{
					pk::city @city = pk::building_to_city(building);
					if (pk::has_facility(city, 设施_练兵所))
					{
						top = pk::max(生产时长上限 - 2, 3);
					}
				}
			}
		}


		if (n < 2)
			n = 2;


		float exp_eff = 1.0f;
		float absent_eff = 1.0f;
		float expend_eff = 1.0f;
		if (ch::get_auto_affairs_status() and actors.count == 1)//只有在人数为1，开启自动内政，且有执政官经验时享受加成
		{
			exp_eff = main.get_exp_eff(actors[0].get_id(),weapon_id);
			if (pk::is_absent(actors[0])) absent_eff = 0.8f;
			else if (actors[0].action_done) absent_eff = 0.9f;
			BuildingInfo @base_p = @building_ex[actors[0].service];
			int effic = 0;
			if (weapon_id < 兵器_走舸) effic = base_p.punch_effic;
			else  effic = base_p.boat_effic;
			float rate = ch::get_modify_rate(float(effic) / pk::get_equipment(weapon_id).gold_cost);
			expend_eff = pk::min(1.5f, rate);
		}



		n = top - pk::max(int((sum + max)* exp_eff - 200), 24) / 24;
		
		n = n * 生产时长倍率 / 100;

		// 관련 특기를 가지고 있다면 반감
		if (has_skill) n = n * pk::core::skill_constant_value(skill_id) / 100;//直接会根据兵器id选择对应特技了

			

		n = pk::max(n, 1); // overflow 방지용 ('18.11.17)
		return n;
	}
}