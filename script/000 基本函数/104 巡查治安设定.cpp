﻿// ## 2023/05/03 # 江东新风 # 函数主体自305迁回 ##
// ## 2021/10/10 # 江东新风 # 将函数内容全搬运到305，以便其他cpp调用 ##
// ## 2021/10/01 # 江东新风 # namespace的韩文改成英文 ##
// ## 2021/09/15 # 江东新风 # 更改pk::core[]函数为英文##
// ## 2021/03/05 # 白马叔叔 # 修正巡查特技倍率 ##
// ## 2020/10/27 # 江东新风 # 新te ##
// ## 2020/07/26 ##
namespace INSPECTIONS
{
	class Main
	{
		Main()
		{
			pk::set_func(104, pk::func104_t(callback));
		}

		int callback(pk::building@ building, const pk::detail::arrayptr<pk::person@>& in actors)
		{
			//pk::trace("actors size:" + pk::Sizeof(actors));
			if (actors[0] !is null)//这里判断的话，其他地方调用内政函数还得再内政函数里再判断一次
			{
				pk::list<pk::person@> actor_list;
				for (int i = 0; i < actors.length; i++)
				{
					actor_list.add(actors[i]);
				}

				return get_inspections_order_inc(building, actor_list);
			}
			return 0;
		}	

		float get_exp_eff(int person_id)
		{
			if (pk::is_valid_person_id(person_id))
			{
				personinfo @person_t = @person_ex[person_id];
				if (person_t.inspection_exp < 执政官经验一阶)
					return 1.0f;
				if (person_t.inspection_exp < 执政官经验二阶)
					return 1.1f;
				if (person_t.inspection_exp < 执政官经验三阶)
					return 1.2f;
				return 1.3f;
			}
			return 1.0f;
		}
	}

	Main main;

	int get_inspections_order_inc(pk::building @building, pk::list<pk::person @> actors)
	{
		if (pk::is_alive(building) and actors[0] !is null)
		{
			bool flag = false;
			if (building.get_id() < 据点_末)
			{
				int n = 0;
				int sum = 0;
				int max = 0;
				for (int i = 0; i < actors.count; i++)
				{
					pk::person @actor = actors[i];
					if (pk::is_alive(actor))
					{
						int s = actor.stat[int(pk::core["inspection.stat"])];
						sum += s;
						max = pk::max(max, s);
						if (ch::has_skill(actor, 特技_巡查))
							flag = true;
					}
				}

				float exp_eff = 1.0f;
				float absent_eff = 1.0f;
				float expend_eff = 1.0f;
				if (ch::get_auto_affairs_status() and actors.count == 1)//只有在人数为1，开启自动内政，且有执政官经验时享受加成
				{
					exp_eff = main.get_exp_eff(actors[0].get_id());
					if (pk::is_absent(actors[0])) absent_eff = 0.8f;
					else if (actors[0].action_done) absent_eff = 0.9f;
					BuildingInfo @base_p = @building_ex[actors[0].service];
					float rate = ch::get_modify_rate(float(base_p.porder_effic) / 100);
					expend_eff = pk::min(1.5f, rate);
				}


				//三个能力100,10+2，三个120 12+2，三个70,7+2,1个120 4+2，1个70 2+2
				//改后，3个100,400/37 10+1, 120*2/37+1
				n = int((sum + max)* exp_eff * absent_eff * expend_eff / 37 + 1);//原来是 sum/28+2


				if (pk::enemies_around(building))
					n = n / 2;
				if (flag)
					n = n * pk::core::skill_constant_value(特技_巡查) / 100;

				int max_order = 100;
				int order = 0;
				if (building.get_id() < 城市_末)
					order = pk::building_to_city(building).public_order;
				else
					order = base_ex[building.get_id()].public_order;
				if (order + n > max_order)
					n = max_order - order;
				return int(n);
			}
		}

		return 0;
	}
}