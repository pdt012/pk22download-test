﻿// ## 2023/05/03 # 铃 # 简化部分函数 重新排版 ##
// ## 2023/04/03 # 铃 # 增加势力影响.城市战力.相邻敌军实力等多个逻辑..##
// ## 2023/03/29 # 铃 # 除了外壳没变,几乎重写了所有逻辑.##
// ## 2023/03/27 # 江东新风 # 初次编写 ##

namespace AI_ATTACK_CITY_WEIGHT
{
    const int 难度影响玩家受攻击权重 = 5;

    class Main
    {
        Main()
        {

            pk::set_func(263, pk::func263_t(callback));
            pk::set_func(264, pk::func264_t(callback2));
        }

        pk::building @callback2(pk::ai_context @context, pk::building @building)
        {
            // 玩家军团处理
            if (building.is_player())
                return null; // return s11::fxx::func_5e5e00(building);

            int src_building_id = building.get_id(); // f38fc14

            int src_force_id = building.get_force_id();         // f38fc10
            pk::force @src_force = pk::get_force(src_force_id); // f38fc1c

            //  获取周边据点列表
            pk::list<pk::building @> building_list = get_near_base_list(src_building_id); // s11::g_data->_99c51c8.clear();

            if (building_list.count <= 0) // if (s11::fxx::func_5faee0(&s11::g_data->_9a8f044, 0, &s11::g_data->_99c51c8, src_building_id) <= 0)
                return null;

            int src_city_id = pk::get_city_id(building.pos);         // f38fc18
            int src_province_id = pk::get_province_id(building.pos); // f38fc08

            pk::list<pk::building @> same_city;     // f38fc80
            pk::list<pk::building @> same_province; // f38fc60
            pk::list<pk::building @> normal;        // f38fca0

            //  周边据点分类
            for (int i = 0; i < building_list.count; ++i) //(auto i = s11::g_data->_99c51c8.begin(); i; i++)
            {
                pk::building @base = building_list[i];
                if (!pk::is_alive(base))
                    continue;

                //  确认如果不是空建筑，是否可以攻击相关势力。
                int force_id = base.get_force_id();

                // 不知道這個是什麼鬼函數?結果也很奇怪
                //  if (force_id >= 0 && force_id < 势力_末)
                //  {
                //      if (!pk::is_attackable_force(src_force_id, force_id)) // 确认是可攻击的势力--!!!!得重写
                //          continue;
                //  }

                // 같은 구역 同一区域
                // if (pk::get_city_id(base.pos) == src_city_id)
                //   same_city.add(base);

                // 新地圖的州數據不對,先跳過
                //  if (pk::get_province_id(base.pos) == src_province_id && bool(pk::core["ai.attack.same_province_first"])) // if (s11::fxx::func_487e20(building) == src_province_id)
                //      same_province.add(base);
                // else
                normal.add(base);
            }
            //  building_list.clear();

            // if (same_city.count > 0)
            //  building_list = same_city;

            // 新地圖的州數據不對,先跳過
            //  else if (same_province.count > 0)
            //      building_list = same_province;

            // else
            //   building_list = normal;

            // 출진 가능한 총 병력 수 可出动的总兵力数
            uint src_troops;
            if (src_building_id < 城市_末)
                src_troops = pk::get_max_marchable_troops(building); // s11::fxx::func_5fae20(&s11::g_data->_9a8f044, 0, building); // f38fc04---需重写或者调用
            if (src_building_id >= 城市_末 and src_building_id < 据点_末)
                src_troops = building.get_troops();
            int character = 1;
            int kunshu_id = context.force.kunshu;

            // 直接把func_254函数写在这里
            pk::person @kunshu = pk::get_person(kunshu_id);
            character = building.is_player() ? 性格_冷静 : kunshu.character;
            src_troops = src_troops * (character + 3);

            pk::list<pk::force @> player_force;   // f38fc40
            pk::list<pk::building @> player_city; // f38fc20

            bool hard = pk::get_scenario().difficulty == 难易度_超级; // f38fc0c

            // 특급 난이도일 경우 컴퓨터 부대가 공격중인 플레이어 세력과 도시 확인
            // 如果是特级难度，电脑部队正在攻击的玩家势力和城市确认
            if (hard)
            {
                for (int i = 0; i < 部队_末; i++)
                {
                    pk::unit @unit = pk::get_unit(i);
                    if (!pk::is_alive(unit))
                        continue;

                    // 플레이어이거나 일반 부대가 아님 跳过玩家部队及非普通类型部队
                    if (unit.is_player() || pk::get_scenario().ai_table.unit[i].deploy_type != 1)
                        continue;

                    pk::building @dst_building = pk::get_building(pk::get_scenario().ai_table.unit[i].deploy_target); // 目标据点？
                    if (!pk::is_alive(dst_building))
                        continue;

                    pk::force @dst_force = pk::get_force(dst_building.get_force_id());
                    if (!pk::is_alive(dst_force))
                        continue;

                    // 목표 세력이 플레이어가 아님 跳过目标据点的势力非玩家势力的
                    if (!dst_force.is_player())
                        continue;

                    if (!player_force.contains(dst_force))
                        player_force.add(dst_force);

                    if (building.facility == 设施_城市 && bool(pk::core["ai.attack.player_first"])) // this_.player_first_)//需要重新改？
                    {
                        if (!player_city.contains(dst_building))
                            player_city.add(dst_building);
                    }
                }
            }

            array<int8> weight(据点_末, 0);
            int max_weight_building_id = -1;
            int max_weight = -1;

            for (int i = 0; i < building_list.count; ++i) //(auto i = s11::g_data->_99c51c8.begin(); i; i++)
            {
                pk::building @dst_base = building_list[i];
                int dst_base_id = dst_base.get_id(); // f38fcc0
                // pk::trace(" 早期出發城市名:" + pk::decode(pk::get_name(building)) + " 到達城市名:" + pk::decode(pk::get_name(pk::get_building(dst_base_id))) + "目標城市兵力:" + pk::get_max_marchable_troops(dst_base));

                if (!pk::is_alive(dst_base))
                    continue;

                uint troops;
                if (dst_base_id < 城市_末)
                    troops = pk::get_max_marchable_troops(dst_base); // s11::fxx::func_5fae20(&s11::g_data->_9a8f044, 0, building); // f38fc04---需重写或者调用
                if (dst_base_id >= 城市_末 and dst_base_id < 据点_末)
                    troops = dst_base.get_troops();

                // // 특급일 경우 공격받고 있는 플레이어 도시에 한해 병력확인 안함
                // // 如果是特级难度，或攻击的是玩家城市，则不确认兵力
                // // 如果兵力滿,就會大家都呆住,因此改为0.5倍,这样的话即使目标有15万,8万的城市也可以出兵.
                // 换成战力对比放在后面了
                // if (!hard || !player_city.contains(dst_base))
                // {
                //     if (int(src_troops) < int(troops * 0.5))
                //         continue;
                // }

                // 如果自身兵力不足15000,就暂停进攻
                if (int(building.get_troops()) < 15000)
                    continue;

                // 경로 확인 确认路径
                int path = pk::get_route(src_building_id, dst_base_id, src_force_id, true); // s11::fxx::func_5f8aa0(&s11::g_data->_9a8f044, 0, src_building_id, building_id, src_force_id, TRUE);
                // pk::trace(" 早期出發城市名:" + pk::decode(pk::get_name(building)) + " 到達城市名:" + pk::decode(pk::get_name(pk::get_building(dst_base_id))) + "路径" + path);
                if (path == 路径_无)
                    continue;

                // 거리 가중치 距离加权
                int distance = pk::get_building_distance(src_building_id, dst_base_id, src_force_id); // s11::fxx::func_5f8130(&s11::g_data->_9a8f044, 0, src_building_id, building_id, src_force_id);
                weight[dst_base_id] = 3 - distance * 3;                                               // distance从1x改成x3

                // 경로 별 가중치 按路径加权
                if (path == 路径_一般)
                    weight[dst_base_id] += int(pk::core["ai.attack.weight.route.other"]); // this_.route_weight_[0]; // weight[building_id] += 4;
                else if (path == 路径_海洋)
                    weight[dst_base_id] += int(pk::core["ai.attack.weight.route.sea"]); // this_.route_weight_[1]; // weight[building_id] += 2;
                else
                    weight[dst_base_id] += int(pk::core["ai.attack.weight.route.land"]); // this_.route_weight_[2]; // weight[building_id] += 1;

                // 海洋和陆地的路径只影响2实在太少了.轻易不要垮海或者走栈道
                if (path == 路径_一般)
                    weight[dst_base_id] += 0;
                else if (path == 路径_海洋)
                    weight[dst_base_id] += -10;
                else if (path == 路径_栈道)
                {
                    if (building.has_tech(技巧_难所行军))
                        weight[dst_base_id] += 0;
                    else
                        weight[dst_base_id] += -20;
                }
                else
                    weight[dst_base_id] += -20;

                // 우호도 가중치 友好度加权
                int force_id = dst_base.get_force_id();
                if (force_id >= 0 && force_id < 势力_末)
                {
                    if (src_force.relations[force_id] < uint(pk::core["ai.attack.weight.relations_target"])) // s11::fxx::func_4819d0(src_force, 0, force_id) < this_.relations_target_) // if (src_force->func_4819d0(force_id) < 20)
                    {
                        weight[dst_base_id] += int(pk::core["ai.attack.weight.relations"]); // weight[building_id] += 8;
                    }
                    else
                    {
                        if (dst_base.is_player())
                        {
                            int divide = 25;
                            switch (pk::get_scenario().difficulty)
                            {
                            case 难易度_上级:
                                divide = 20;
                                break;
                            case 难易度_超级:
                                divide = 10;
                                break;
                            }
                            weight[dst_base_id] += pk::max(pk::get_scenario().ai_table.force[src_force_id].player_border_rank / divide, 4); //
                        }
                    }
                }

                if (dst_base.facility == 设施_城市)
                {
                    // 같은 구역 도시 최우선 同一区域城市最优先
                    if (pk::get_city_id(dst_base.pos) == src_city_id)
                    {
                        for (int j = 0; j < int(weight.length); j++)
                            weight[j] = 0; // 清零
                        weight[dst_base_id] = 1;
                        break;
                    }

                    // city的距离是没意义的,相邻的城市也可能距离很远.应该用pk::is_neighbor_base
                    //  城市以邻近城市为目标增加5，关口或港口以城市为目标增加2。
                    pk::city @city = pk::building_to_city(dst_base);
                    if (src_building_id >= 0 && src_building_id < 城市_末)
                    {
                        if (pk::is_neighbor_base(dst_base_id, src_building_id))
                            weight[dst_base_id] += 6;
                    }

                    // 好像代码和注释的意思不同，这里已经限定了设施_城市，所以应该不存在小城市的情况
                    //  else
                    //  {
                    //      weight[dst_base_id] += 2;
                    //  }

                    // 군주별 가중치(최대 4)  各君主权重值(最多18)
                    // weight[building_id] += s11::fxx::func_5dd6e0(city, src_force);
                    weight[dst_base_id] += callback(city, src_force);

                    // 已经通过势力和战力平衡过了,不需要再算一次
                    //  빈 도시 空城
                    //  if (force_id < 0 || force_id >= 势力_末)
                    //      weight[dst_base_id] += int(pk::core["ai.attack.weight.empty_city_weight"]); // this_.empty_city_weight_; // weight[building_id] += 2;
                }
                else
                {
                    // 很奇怪的邏輯?
                    //  같은 소속 거점  同一所属据点
                    //  if (pk::get_city_id(dst_base.pos) == src_city_id)
                    //  {
                    //      if (dst_base.get_troops() < 1000)
                    //          weight[dst_base_id] += 4;
                    //  }

                    // 同一區域內小城市優先攻擊
                    if (pk::get_city_id(dst_base.pos) == src_city_id)
                        weight[dst_base_id] += 10;

                    // 各君主权重值(最多10) 君主對區域的偏好
                    // weight[building_id] += s11::fxx::func_5dd6e0(city, src_force);
                    weight[dst_base_id] += callback_base(dst_base, src_force);

                    // 빈 거점 空据点
                    if (force_id < 0 || force_id >= 势力_末)
                        weight[dst_base_id] += int(pk::core["ai.attack.weight.empty_gate_weight"]); // this_.empty_gate_weight_; // weight[building_id] += 0;
                }

                // 相邻空据点优先级更加提高,(自带空城的值是0,因此又写了一边)
                if (pk::is_neighbor_base(dst_base_id, src_building_id) and (force_id < 0 || force_id >= 势力_末 || troops < 2000))
                    weight[dst_base_id] += 20;
                // int c_city_count = pk::get_city_count(pk::get_force(force_id));
                // int c_troops = pk::get_troops(pk::get_force(force_id));


                //  在特级难度时，受到攻击的玩家据点
                if (hard)
                {
                    pk::force @force = pk::get_force(force_id);
                    if (pk::is_alive(force))
                    {
                        if (force.is_player() && player_force.contains(force))
                            weight[dst_base_id] += 10;
                    }
                }

                // 目标城市的人口越多,权重越高.
                // 对于大城市而已.100万人口城市+3,20万人口城市-3.
                // 对于小城市而已.50万人口城市+2,5万人口城市-3.
                BaseInfo @base_t = @base_ex[dst_base_id];
                int 人口权重 = 0;
                if (dst_base_id < 城市_末)
                    人口权重 = int((sqrt(base_t.population) - 700) / 100);
                if (dst_base_id >= 城市_末)
                    人口权重 = int((sqrt(base_t.population) - 500) / 100);

                weight[dst_base_id] += 人口权重;

                // 尽量避免和大势力对抗
                pk::list<pk::force @> force_list;
                force_list.clear();
                force_list = pk::get_force_list();

                force_list.sort(function(a, b) {
                    int stat_a = pk::get_city_count(a);
                    int stat_b = pk::get_city_count(b);
                    return (stat_a > stat_b);
                });

                int rank = force_list.index_of(pk::get_force(force_id));
                if (force_id > 0)
                {
                    if (!pk::get_force(force_id).is_player())
                    {
                        weight[dst_base_id] += int(rank / force_list.count * 5);
                    }
                }

                if (hard)
                    weight[dst_base_id] += 难度影响玩家受攻击权重;

                // 根据目标城市的战力分配权重
                auto src_ilban_list = pk::list_to_array(pk::get_person_list((building), pk::mibun_flags(身分_一般, 身分_都督, 身分_太守, 身分_君主)));
                int 己方总战力 = 1;
                if (src_ilban_list.length >= 1)
                {
                    for (int k = 0; k < int(src_ilban_list.length); k++)
                    {
                        pk::person @ilban = src_ilban_list[k];
                        if (!pk::is_unitize(ilban) and !pk::is_absent(ilban))
                            己方总战力 += int(ilban.stat[武将能力_武力] * UNIT_ATTR::攻武占比 / 100 + ilban.stat[武将能力_统率] * UNIT_ATTR::攻统占比 / 100);
                        己方总战力 += int(ilban.stat[int(pk::core["光环.基础光环关联属性"])] - int(pk::core["光环.基础光环阈值"]) >= 0 ? 2 : 0);
                    }
                }

                // 根据目标城市的战力分配权重
                auto dst_ilban_list = pk::list_to_array(pk::get_person_list((pk::get_building(dst_base_id)), pk::mibun_flags(身分_一般, 身分_都督, 身分_太守, 身分_君主)));
                int 目标总战力 = 1;
                if (dst_ilban_list.length >= 1)
                {
                    for (int k = 0; k < int(dst_ilban_list.length); k++)
                    {
                        pk::person @ilban = dst_ilban_list[k];
                        if (!pk::is_unitize(ilban) and !pk::is_absent(ilban))
                            目标总战力 += int(ilban.stat[武将能力_武力] * UNIT_ATTR::攻武占比 / 100 + ilban.stat[武将能力_统率] * UNIT_ATTR::攻统占比 / 100);
                        目标总战力 += int(ilban.stat[int(pk::core["光环.基础光环关联属性"])] - int(pk::core["光环.基础光环阈值"]) >= 0 ? 2 : 0);
                    }
                }

                // 如果己方战力明显不足,就不要出征
                if (int(src_troops * 己方总战力 / 100) < int(troops * 0.8 * 目标总战力 / 100))
                    continue;

                weight[dst_base_id] += int((1000 - pk::min(目标总战力, 2000)) / 300);

                // 如果一个目标城市和其他城市都不相邻(地图边缘)则加权`
                // 如果一个目标城市的相邻,和出发城市的势力的敌对越多,则降权.
                // 一个目标城市的相邻,和出发城市相邻的己方城市越多,则加权.
                // 举例:南阳有6个相邻,除襄阳之外,的5个城市都是曹操的,刘备在襄阳要打南阳,则会被降权5分.
                int count_neighbor = 0;
                int neighbor_rank = 0;
                for (int j = 0; j < 6; j++)
                {
                    int neighbor_base_id = pk::get_city(pk::get_city_id(dst_base.pos)).neighbor[j];
                    // 空相邻则+1,(后方城市)
                    if (neighbor_base_id == -1)
                    {
                        count_neighbor++;
                        continue;
                    }
                    // 计数和自己敌对的相邻据点.敌对则-1,如果兵力接近上限则不减分.
                    if (neighbor_base_id > -1 and pk::is_enemy(building, pk::get_building(neighbor_base_id)) and pk::get_troops(building) < int(pk::get_max_troops(building) * 0.99))
                        count_neighbor -= 1;
                    // 附近有空城则+1
                    if (pk::get_building(neighbor_base_id).get_force_id() == -1)
                        count_neighbor += 1;
                    // 计数和自己敌对的相邻据点.如果不仅敌对而且是排名前1/3的势力则再-1,如果兵力接近上限则不减分.
                    if (pk::get_building(neighbor_base_id).get_force_id() > -1)
                    {
                        neighbor_rank = force_list.index_of(pk::get_force(pk::get_building(neighbor_base_id).get_force_id()));
                        if (neighbor_base_id > -1 and pk::is_enemy(building, pk::get_building(neighbor_base_id)) and float(neighbor_rank) / float(force_list.count) < 0.3 and pk::get_troops(building) < int(pk::get_max_troops(building) * 0.99))
                            count_neighbor -= 1;
                    }
                    // 计数和自己敌对的相邻据点.己方则+1
                    if (neighbor_base_id > -1 and building.get_force_id() == pk::get_building(neighbor_base_id).get_force_id())
                        count_neighbor += 1;
                }
                weight[dst_base_id] += count_neighbor;

                // 势力不够强的时候,不要去打不适合的城市
                // 不能因为仅有一个相邻而破坏自己的战略.除非兵力已满.
                src_force_id = building.get_force_id();
                if (pk::get_city_count(pk::get_force(src_force_id)) < 5 and weight[dst_base_id] < 0 and pk::get_troops(building) < int(pk::get_max_troops(building) * 0.99))
                    continue;

                if (pk::get_city_count(pk::get_force(src_force_id)) < 3 and weight[dst_base_id] < 10 and pk::get_troops(building) < int(pk::get_max_troops(building) * 0.6))
                    continue;

                // pk::trace(" 出发城市名:" + pk::decode(pk::get_name(building)) + " 到达城市名:" + pk::decode(pk::get_name(pk::get_building(dst_base_id))) + "目标城市权重:" + weight[dst_base_id] + "路径" + path);
                if (weight[dst_base_id] > max_weight)
                {
                    max_weight_building_id = dst_base_id;
                    max_weight = weight[dst_base_id];
                }

                if (max_weight_building_id == -1)
                    continue;
            }
/*
            for (int i = 0; i < 据点_末; ++i)
            {
                
                if(weight[i]!=0) pk::trace(pk::get_new_base_name(src_building_id) +  "to" + pk::get_new_base_name(i) + "weight" + weight[i]);
            }
            for (int i = 0; i < building_list.count; ++i)
            {
                int dst_id = building_list[i].get_id();
                pk::trace(pk::get_new_base_name(src_building_id) + "to" + pk::get_new_base_name(dst_id) + "weight" + weight[dst_id]);
            }*/
            building_list.clear();

            // pk::trace(" 最终返回据点名:" + pk::decode(pk::get_name(pk::get_building(max_weight_building_id))));
            if (max_weight_building_id == -1)
                return null;
            return pk::get_building(max_weight_building_id);

            // 这个函数怎么能放在这里?应该放在for里面,放在外面逻辑不对的
            //  // 下方函数好像没法随机选择相同权重的目标
            //  int max_weight_building_id = -1;
            //  int max_weight = -1;
            //  for (int i = 0; i < int(weight.length); i++)
            //  {
            //      if (weight[i] > max_weight)
            //          max_weight_building_id = i;
            //      pk::trace(" i:" + i + " weight[i]:" + weight[i] + " max_weight:" + max_weight);
            //  }

            // @building = pk::get_building(max_weight_building_id); // s11::fxx::func_5f89d0(&s11::g_data->_9a8f044, 0, weight.data(), weight.size())此函数用来选权重最大的据点id，直接用上方函数代替了
            // if (!pk::is_alive(building))
            //     return null;
            // pk::trace(" 最终返回据点名:" + pk::decode(pk::get_name(building)));

            // return building;
        }

        pk::list<pk::building @> get_near_base_list(int src_base_id)
        {
            pk::building @src_base = pk::get_building(src_base_id);
            int src_force_id = src_base.get_force_id();
            pk::force @src_force = pk::get_force(src_force_id);
            pk::list<pk::building @> list;
            for (int i = 0; i < 据点_末; ++i)
            {

                if (i == src_base_id)
                    continue;
                pk::building @dst_base = pk::get_building(i);
                if (dst_base.get_force_id() == src_force_id)
                    continue;
                if (!pk::is_enemy(src_base, dst_base) and dst_base.get_force_id() != -1)
                    continue; // 这个判断不知道是否合适
                bool is_city = false;
                if (i < 城市_末)
                    is_city = true;
                int distance = pk::get_building_distance(src_base_id, i, src_force_id);
                //if (src_base_id == 城市_汉中 and i == 城市_梓潼)
                //{
                //    pk::trace(pk::get_new_base_name(i) + "distance" + distance);
                //}
                //梓潼和汉中的距离为4（中间隔着2个关和一个白水）
                if (is_city and distance <= 3)
                    list.add(pk::get_building(i));
                if (!is_city and distance <= 1)
                    list.add(pk::get_building(i));
            }
            return list;
        }


        //实际上这个函数里不需要加入城市权重，因为城市据点不会进入该判断
        int callback_base(pk::building @building, pk::force @force)
        {
            int kunshu_id = force.kunshu;
            int building_id = building.get_id();
            int weight = 0;
            //原版和血色都没有这设定，可以先默认返回0
            if (pk::is_new_map())
            {
                switch (kunshu_id)
                {
                case 武将_袁煕:
                case 武将_袁尚:
                case 武将_袁绍:
                case 武将_袁谭:
                    switch (building_id)
                    {

                    case 新据点_北平:
                    case 新据点_常山:
                    case 新据点_蓟:
                    case 新据点_上党:
                    case 新据点_晋阳: // 冀州北部和幽州
                        weight = 5;
                        break;
                    case 新据点_渤海:
                    case 新据点_邺:
                    case 新据点_上谷:
                    case 新据点_平原:
                    case 新据点_黎阳:
                    case 新据点_中山: // 冀州南部
                        weight = 10;
                        break;
                    default:
                        break;
                    }
                    break;
                case 武将_曹彰:
                case 武将_曹植:
                case 武将_曹操:
                case 武将_曹丕:
                case 武将_曹叡: // 添加后期君主曹叡
                    switch (building_id)
                    {
                    case 新据点_新野:
                    case 新据点_襄阳:
                    case 新据点_江夏:
                    case 新据点_黎阳:

                        weight = -20;
                        break;
                    case 新据点_彭城: // 兖州
                    case 新据点_下邳: // 徐州
                    case 新据点_汝南:
                    case 新据点_洛阳:
                    case 新据点_虎牢关:

                        weight = 5;
                        break;
                    case 新据点_陈留:
                    case 新据点_许昌:
                    case 新据点_濮阳:
                    case 新据点_南阳:
                    case 新据点_谯: // 豫州
                        weight = 10;
                        break;
                    default:
                        break;
                    }
                    break;
                case 武将_孙坚:
                case 武将_孙权:
                case 武将_孙策:
                    switch (building_id)
                    {
                    case 新据点_华容:
                    case 新据点_新野:
                    case 新据点_苍梧:

                        weight = -20;
                        break;
                    case 新据点_会稽: // 扬州南部
                    case 新据点_建安:
                    case 新据点_庐江:
                    case 新据点_临川:
                    case 新据点_淮南: // 淮南
                        weight = 5;
                        break;
                    case 新据点_建业:
                    case 新据点_吴:
                    case 新据点_柴桑:
                    case 新据点_鄱阳:
                    case 新据点_赤壁:
                    case 新据点_豫章:
                    case 新据点_曲阿: // 扬州
                        weight = 10;
                        break;
                    default:
                        break;
                    }
                    break;
                case 武将_刘备:
                case 武将_刘禅:
                case 武将_刘封: // 添加继承人
                    switch (building_id)
                    {

                    case 新据点_樊城:
                    case 新据点_赤壁:
                    case 新据点_弋阳:
                    case 新据点_鄱阳:
                    case 新据点_庐江:
                        weight = -20;

                        break;
                    case 新据点_梓潼:
                    case 新据点_成都:
                    case 新据点_阆中:
                    case 新据点_巴郡:
                    case 新据点_合浦:
                    case 新据点_苍梧: // 交州
                        weight = 5;
                        break;
                    case 新据点_襄阳:
                    case 新据点_江陵:
                    case 新据点_临沅:
                    case 新据点_零陵:
                    case 新据点_桂阳:
                    case 新据点_夷陵:
                    case 新据点_武陵:
                    case 新据点_华容: // 荆州
                    case 新据点_巴东: // 益州
                        weight = 10;
                        break;
                    default:
                        break;
                    }
                    break;
                }
            }

            return weight;
        }

        int callback(pk::city @city, pk::force @force)
        {
            if (pk::get_scenario().no == 血色剧本) return 0;//血色进攻倾向应重写--TODO
            int kunshu_id = force.kunshu;
            int city_id = city.get_id();
            int weight = 0;
            //血色无法采用原版的，因为君主id不一样，那就先默认为返回0
            if (!pk::is_new_map())
            {
                switch (kunshu_id)
                {
                case 武将_袁熙:
                case 武将_袁尚:
                case 武将_袁绍:
                case 武将_袁谭:
                    switch (city_id)
                    {
                    case 城市_襄平:
                    case 城市_蓟:
                    case 城市_晋阳:
                        weight = 1;
                        break;
                    case 城市_北平:
                    case 城市_平原:
                        weight = 2;
                        break;
                    case 城市_南皮:
                    case 城市_邺:
                        weight = 4;
                        break;
                    default:
                        break;
                    }
                case 武将_曹彰:
                case 武将_曹植:
                case 武将_曹操:
                case 武将_曹丕:
                    switch (city_id)
                    {
                    case 城市_邺:
                    case 城市_濮阳:
                    case 城市_汝南:
                    case 城市_汉中:
                        weight = 1;
                        break;
                    case 城市_陈留:
                    case 城市_许昌:
                    case 城市_洛阳:
                        weight = 4;
                        break;
                    case 城市_宛:
                    case 城市_长安:
                        weight = 2;
                        break;
                    default:
                        break;
                    }
                case 武将_孙坚:
                case 武将_孙权:
                case 武将_孙策:
                    switch (city_id)
                    {
                    case 城市_建业:
                    case 城市_吴:
                        weight = 4;
                        break;
                    case 城市_会稽:
                    case 城市_庐江:
                    case 城市_柴桑:
                        weight = 2;
                        break;
                    case 城市_江夏:
                    case 城市_长沙:
                        weight = 1;
                        break;
                    default:
                        break;
                    }
                case 武将_刘备:
                    switch (city_id)
                    {
                    case 城市_江夏:
                    case 城市_长沙:
                    case 城市_武陵:
                        weight = 1;
                        break;
                    case 城市_襄阳:
                    case 城市_江陵:
                    case 城市_永安:
                    case 城市_汉中:
                    case 城市_梓潼:
                    case 城市_江州:
                        weight = 2;
                        break;
                    case 城市_成都:
                        weight = 4;
                        break;
                    default:
                        break;
                    }
                }
            }
            else if (pk::is_new_map())
            {
                switch (kunshu_id)
                {
                case 武将_袁煕:
                case 武将_袁尚:
                case 武将_袁绍:
                case 武将_袁谭:
                    switch (city_id)
                    {
                    case 新据点_北海:
                    case 新据点_长安:
                    case 新据点_彭城:
                        weight = -30;

                        break;
                    case 新据点_北平:
                    case 新据点_蓟:
                    case 新据点_晋阳: // 冀州北部和幽州
                        weight = 5;
                        break;
                    case 新据点_渤海:
                    case 新据点_邺:
                    case 新据点_中山:
                        weight = 20;
                        break;
                    default:
                        break;
                    }
                    break;
                case 武将_曹彰:
                case 武将_曹植:
                case 武将_曹操:
                case 武将_曹丕:
                case 武将_曹叡: // 添加后期君主曹叡
                    switch (city_id)
                    {
                    case 新据点_襄阳:
                    case 新据点_江陵:
                    case 新据点_江夏:
                        weight = -30;

                        break;

                    case 新据点_彭城:
                    case 新据点_下邳: // 徐州
                    case 新据点_濮阳:
                    case 新据点_南阳:

                        weight = 5;
                        break;
                    case 新据点_汝南:
                    case 新据点_许昌:
                    case 新据点_陈留:

                        weight = 20;
                        break;
                    default:
                        break;
                    }
                    break;
                case 武将_孙坚:
                case 武将_孙权:
                case 武将_孙策:
                    switch (city_id)
                    {
                    case 新据点_江陵:
                    case 新据点_襄阳:
                    case 新据点_南阳:
                        weight = -30;

                        break;
                    case 新据点_建安: // 扬州南部
                    case 新据点_庐江:
                    case 新据点_江夏:
                    case 新据点_淮南: // 淮南
                        weight = 5;
                        break;
                    case 新据点_建业:
                    case 新据点_吴:
                    case 新据点_柴桑:
                        weight = 20;
                        break;
                    default:
                        break;
                    }
                    break;
                case 武将_刘备:
                case 武将_刘禅:
                case 武将_刘封: // 添加继承人
                    switch (city_id)
                    {
                    case 新据点_江夏:
                    case 新据点_南阳:
                    case 新据点_柴桑:
                        weight = -30;

                        break;
                    case 新据点_梓潼:
                    case 新据点_合浦:
                    case 新据点_武陵: // 荆北

                        weight = 5;
                        break;
                    case 新据点_襄阳:
                    case 新据点_江陵:
                    case 新据点_成都:
                    case 新据点_巴东: // 益州
                    case 新据点_巴郡:
                        weight = 20;
                        break;
                    default:
                        break;
                    }
                    break;
                }
            }
            

            return weight;
        }
    }

    Main main;
}