﻿// ## 2023/3/21 # 铃 # 优化新的兵役人口增长.##
// ## 2023/3/16 # 铃 # 新增灾难流民系统.##
// ## 2023/3/14 # 铃 # 新增小城市的丰收系统,大小城市共享所属地区城市的丰收.疫病属性,但特技不共享..##
// ## 2023/3/11 # 铃 # 新增了战争时人口迁移的函数.##
// ## 2023/3/2 # 铃 # 新增了府的人口设定.##
// ## 2023/2/11 # 铃 # 新增了战争时人口迁移的函数.##
// ## 2023/2/05 # 铃 # 修复了一些问题,1.对人口较低的城市,直接增加了2%的增长,和之前的千分之5差距过大.4.空城低治安的时候人口不应该下降 5.小城市增长太低##
// ## 2022/10/09 # 铃 # 给了两套人口默认值.重新设定了初始人口数值了 ##
// ## 2022/08/21 # 铃 # 删除了可用兵役的设定，把人口相关的信息显示移动到954内政自动化中 ##
// ## 2022/03/17 # 江东新风 # 文字往下掉问题 ##
// ## 2022/02/17 # 江东新风 # 人口上限修正 ##
// ## 2022/02/14 # 江东新风 # 部分常量中文化 ##
// ## 2022/02/12 # 江东新风 # 丰收不起效bug，伤兵恢复下限 ##
// ## 2021/10/29 # 江东新风 # 结构体存储调用方式改进 ##
// ## 2021/10/10 # 江东新风 # 人口设定扩展到非城市据点，蝗灾，疫病，丰收，掌握人心的影响 ##
// ## 2021/10/07 # 江东新风 # 基础人口，人口增长，总兵役人口，可用兵役等设定 ##
// ## 2021/01/19 # 江东新风 # 构思 ##
namespace 人口系统
{
	/// ============================================ < 人口自定义项-放到301 > ============================================ ///

	/// ========================================================================================================= ///
	const array<int> 各城市人口 = {91650, 56550, 334100, 455000, 559000, 383500, 1671150, 364000, 461500, 62400, 282100, 346450, 656500, 385450, 303550, 1741350, 289250, 445250, 250900, 52650, 162500, 199550, 487500, 461500, 479700, 117000, 430300, 253500, 123500, 147550, 500500, 490750, 110500, 118950, 296400, 130000, 136500, 279500, 78000, 975000, 247000, 305500};
	const bool 调试模式 = false;
	class Main
	{
		Main()
		{
			pk::bind2(102, pk::trigger102_t(onGameStart), -1);
			pk::bind(106, pk::trigger106_t(onLoad)); // 在读档前
			pk::bind(108, pk::trigger108_t(onNewMonth));
			pk::bind(107, pk::trigger107_t(onNewDay2));

			// 把人口相关的信息显示移动到954内政自动化中 ##
			//  pk::bind(108, pk::trigger108_t(onNewMonth));
			//  pk::bind(120, pk::trigger120_t(func_信息显示_人口信息), 999);//数字越大越优先

			pk::bind(171, pk::trigger171_t(onUnitRemove));
			pk::set_func(64, pk::func64_t(City_Level));
		}

		int City_Level(pk::city @city) //
		{
			int city_id = city.get_id();
			int population = base_ex[city_id].population;
			// pk::trace("城市" + pk::get_new_base_name(city_id) + "人口" + population);
			if (population > 四级城市阈值)
				return 2;
			else if (population > 三级城市阈值)
				return 1;
			else
				return 0;
		}

		// 开局时设定基础人口数据
		void onGameStart()
		{
			if (!开启人口系统)
				return;
			if (pk::get_scenario().loaded)
			{
				set_interior_land();
			}
			else
			{
				int scen_no = pk::get_scenario().no;
				for (int city_id = 0; city_id < 城市_末; ++city_id)
				{

					BaseInfo @city_t = @base_ex[city_id];
					city_t.population = 剧本人口[city_id][scen_no];
					city_t.mil_pop_all = 剧本兵役[city_id][scen_no];
					city_t.mil_pop_av = 剧本兵役[city_id][scen_no] / 10;
					// if (city_id == 城市_襄阳) pk::trace(pk::format("onGameStart,襄阳人口：{}", city_t.population));
				}
				for (int i = 城市_末; i < 据点_末; ++i)
				{
					pk::building@ base = pk::get_building(i);
					int city_id = pk::get_city_id(base.pos);
					BaseInfo @base_t = @base_ex[i];
					base_t.population = 剧本兵役[city_id][scen_no];//以对应城市的兵役代替人口
					base_t.mil_pop_all = 剧本兵役[city_id][scen_no]/5;
					base_t.mil_pop_av = 剧本兵役[city_id][scen_no] / 50;
				}
				/*
				if (初始人口模式 == 0)
				{
					for (int city_id = 0; city_id < 据点_末; ++city_id)
					{
						BaseInfo @city_t = @base_ex[city_id];
						city_t.population = 虚构模式人口[city_id][0];
						city_t.mil_pop_all = 虚构模式人口[city_id][1];
						city_t.mil_pop_av = 虚构模式人口[city_id][2];
						// if (city_id == 城市_襄阳) pk::trace(pk::format("onGameStart,襄阳人口：{}", city_t.population));
					}
				}*/

				// 给府设定人口
				for (int i = 0; i < ch::get_spec_end(); ++i)
				{

					pk::point pos = ch::get_spec_pos(i);
					pk::building @building = pk::get_building(pos);
					if (building is null) continue;
					// pk::trace(pk::format("onGameStart,地名：{} :{}是否存活:{}", ch::get_spec_name(i),i,pk::is_alive(building)));

					int spec_id = ch::to_spec_id(building.get_id());

					if (spec_id == -1)
						continue;
					// pk::trace(pk::format("onGameStart,地名：{},spec_id :{},i :{}",  ch::get_spec_name(spec_id),spec_id,i));

					int city_id = pk::get_building_id(building.pos);

					BaseInfo @base_t = @base_ex[pk::get_building_id(building.pos)];
					special_ex[spec_id].population = int(sqrt(base_t.population) * 20) + int(base_t.population * 0.01) + pk::rand(2000);
					if (special_ex[spec_id].population < 8000)
					{
						special_ex[spec_id].population += 5000 + pk::rand(3000);
						continue;
					}

					if (special_ex[spec_id].population < 15000)
					{
						special_ex[spec_id].population += 3000 + pk::rand(2000);
						continue;
					}
				}
			}

		} // onGameStart()

		void onLoad(int file_id)
		{
			for (int i = 0; i < 城市_末; ++i)
			{
				pk::city @city = pk::get_city(i);
				for (int j = 14; j < 22; ++j)
				{
					city.dev[j].pos = pk::point(-1, -1);
				}
			}
		}

		void onNewMonth()
		{
			if (!开启人口系统)
				return;
			// 人口增长
			for (int base_id = 0; base_id < 据点_末; base_id++)
			{
				pk::building @building0 = pk::get_building(base_id);

				BaseInfo @base_t = @base_ex[base_id];

				// if (base_id > 1) pk::trace(pk::format("城市ID：{},城市名：{},人口：{},据点末：{}", base_id, pk::decode(pk::get_name(building0)), base_t.population,据点_末));

				float final_rate = ch::get_pop_inc_rate(base_id, 人口基础增长率);

				// 直接获取据点的pos,大小城市共享所属地区城市的丰收.疫病属性,但特技不共享.

				// 空城不用考虑人口增减;
				int force_id = building0.get_force_id();
				if (force_id == -1)
					continue;

				pk::force @force = pk::get_force(force_id);

				pk::person @kunshu = pk::get_person(force.kunshu);

				int city_id = pk::get_city_id(building0.pos);
				pk::city @city0 = pk::get_city(city_id);

				 

				bool 丰收状态 = city0.housaku;
				bool 疫病状态 = city0.ekibyou;
				bool 蝗灾状态 = city0.inago;
				// if (building0.is_player()) pk::message_box(pk::encode(pk::format("报告,\x1b[2x{}\x1b[0x的final_rate为{}", pk::decode(pk::get_name(building0)), final_rate)));

				// 君主的加成和难度的加成
				float 君主修正 = 1 + ((kunshu.stat[武将能力_魅力] * 2.0 + kunshu.stat[武将能力_政治]) / 3.0 - 75) * 0.003;
				final_rate = final_rate * 君主修正;

				if (pk::get_scenario().difficulty != 难易度_初级 and !building0.is_player())
					final_rate = final_rate * 1.0;
				if (pk::get_scenario().difficulty != 难易度_上级 and !building0.is_player())
					final_rate = final_rate * 1.1;
				if (pk::get_scenario().difficulty != 难易度_超级 and !building0.is_player())
					final_rate = final_rate * 1.2;

				// pk::trace("君主修正" + 君主修正 + "千分增长率" + final_rate);

				// 育民类特技不再叠加,有育民则以育民计算.
				if (ch::has_skill(building0, 特技_育民))
				{
					final_rate += final_rate * pk::core::skill_constant_value(特技_育民)/100; // 育民
				}
				//考虑特技单一性，取消安民和祈愿的非描述加成
				//else if (ch::has_skill(building0, 特技_安民) or ch::has_skill(building0, 特技_祈愿))
				//{
				//	final_rate = final_rate * 1.2f; // 安民祈愿
				//}

				if (city0.housaku)
					final_rate = final_rate * 2; // 丰收状态增长率翻倍

				else if (city0.ekibyou)
					final_rate -= 人口基础增长率 * 2.0f;
				else if (city0.inago)
					final_rate -= 人口基础增长率 * 2.0f;

				array<array<float>> 抽丁比 = {
					/*三丁抽一*/{0.33,500,0},
					/*二丁抽一*/{0.5,333,-0.25},
					/*五丁抽一*/{0.2,833,0.3}
				};
				int r = 0;
				if (base_t.mil_pop_all < 10000) r = 1;
				if (base_t.mil_pop_all > 50000) r = 2;

				//三国志9的设计是兵役增长只和民心有关，根据全国人口算出全国总兵役，然后再根据民心分配给据点
				// 考虑到每个城市每年获得2-5万的士兵,兵役人口如果增长过低或者比例过低,兵役人口会迅速降低,后期游戏性大打折扣.因此重新设计参数.
				int mil_gain = int((base_t.population + base_t.mil_pop_all*2) * final_rate * (1 + 抽丁比[r][2]) * 抽丁比[r][0] + base_t.population / 抽丁比[r][1] * (1 + 抽丁比[r][2])); // 3丁抽一,总兵役人口算增量10%,额外加上每个月500人多一个兵役人口)
				//pk::trace(pk::get_new_base_name(base_id) + "mil_gain" + mil_gain + "三丁抽一：" + int(base_t.population * final_rate * 0.33f) + "xx人口一兵" + int(base_t.population / 500.f));
				// pk::trace(pk::format("城市ID：{},城市名：{},人口：{},兵力增长：{}", base_id, pk::decode(pk::get_name(building0)), base_t.population, mil_gain));

				// int temp = int(base_t.population * (1 + final_rate));
				// int temp2 = int((temp - base_t.population) * 0.2f + base_t.population / 600.f); // 总兵役人口算增量,额外加上每600人多一个兵役人口

				// pk::trace(pk::format("城市ID：{},城市名：{},人口：{},增长率：{}", base_id, pk::decode(pk::get_name(building0)), base_t.population, final_rate*1000));

				// pk::trace(pk::format("被攻击方人口1，{}", base_t.population));

				// base_t.population = uint32(temp);
				//pk::trace(pk::format("{}{},新增兵役:{}兵役人口：{},人口：{},增长率：{}", base_id,pk::decode(pk::get_name(building0)), mil_gain,base_t.mil_pop_all, base_t.population,final_rate * 1000));
				ch::add_population(base_id, int((base_t.population + base_t.mil_pop_all * 2) * final_rate * (1+ 抽丁比[r][2]) - mil_gain));
				ch::add_mil_pop_all(base_id, mil_gain);

				// if (base_id == 城市_襄阳) pk::trace(pk::format("onNewDay 3,襄阳人口：{}", base_t.population));
				// if (pk::is_first_month_of_quarter() and base_id < 城市_末) pk::trace(pk::format("{},人口：{}，金收入：{},粮收入：{}",pk::decode(pk::get_name(pk::get_city(base_id))), base_t.population, get_city_revenue(base_id), get_city_harvest(base_id)));

				int has_enemy_type = has_enemy(pk::get_building(base_id));
				//0无敌军，1：3格内敌军，2:一格内敌军
				float enemy_inf = has_enemy_type == 2 ? -0.05f : (has_enemy_type == 1 ? -0.01f : 0.f);

				if (has_enemy_type != 0)
				{
					//难民逃散到其他邻近城市去了
					ch::add_population(base_id, int(base_t.population * enemy_inf));
					ch::add_mil_pop_all(base_id, int(base_t.population * enemy_inf * 0.2));

					// 战争流民
					pk::list<pk::building @> neighbor_building_list;
					for (int neighbor_id = 0; neighbor_id < 城市_末; ++neighbor_id)
					{
						pk::building @building = pk::get_building(neighbor_id);
						if (neighbor_id == base_id)
							continue;

						int distance = pk::get_building_distance(neighbor_id, base_id);
						if (distance > 1)
							continue;

						if (!pk::is_valid_person_id(pk::get_kunshu_id(building)))
							continue;

						// 如果目标城市也在被攻击,则跳过.
						if (has_enemy(pk::get_building(neighbor_id)) != 0)
							continue;

						neighbor_building_list.add(building);
					}

					if (neighbor_building_list.count > 1)
					{
						neighbor_building_list.sort(function(a, b) {
							pk::force @force_a = pk::get_force(a.get_force_id());
							pk::force @force_b = pk::get_force(b.get_force_id());

							pk::person @kunshu_a = pk::get_person(force_a.kunshu);
							pk::person @kunshu_b = pk::get_person(force_b.kunshu);

							return kunshu_a.stat[武将能力_魅力] > kunshu_b.stat[武将能力_魅力];
						});

						ch::add_population(neighbor_building_list[0].get_id(), int(base_t.population * -enemy_inf * 0.8));
						ch::add_population(neighbor_building_list[1].get_id(), int(base_t.population * -enemy_inf * 0.2));

						BaseInfo @neighbor_base_t = @base_ex[neighbor_building_list[0].get_id()];

						if (int(base_t.population * -enemy_inf) > 10000)
						{
							string t = pk::format("\x1b[2x{}\x1b[0x受到攻击,人心惶惶,\x1b[2x{}\x1b[0x人口向\x1b[2x{}\x1b[0x和\x1b[2x{}\x1b[0x逃难", pk::decode(pk::get_name(building0)), int(base_t.population * -enemy_inf), pk::decode(pk::get_name(neighbor_building_list[0])), pk::decode(pk::get_name(neighbor_building_list[1])));

							pk::history_log(building0.pos, pk::get_force(building0.get_force_id()).color, pk::encode(t));
						}
					}
					// pk::trace(pk::format("{},魅力最高人口，{}", neighbor_building_list[0].get_id(), pk::decode(pk::get_name(neighbor_building_list[0])), neighbor_base_t.population));
				}

				// 灾难流民
				if (疫病状态 or 蝗灾状态)
				{
					ch::add_population(base_id, int(base_t.population * enemy_inf));
					ch::add_mil_pop_all(base_id, int(base_t.population * enemy_inf * 0.3));

					pk::list<pk::building @> neighbor_building_list;
					for (int neighbor_id = 0; neighbor_id < 城市_末; ++neighbor_id)
					{
						pk::building @building = pk::get_building(neighbor_id);
						if (neighbor_id == base_id)
							continue;

						int distance = pk::get_building_distance(neighbor_id, base_id);

						// 逃难距离更远
						if (distance > 2)
							continue;

						if (!pk::is_valid_person_id(pk::get_kunshu_id(building)))
							continue;

						// 如果目标城市也受到灾害则跳过.
						if (pk::building_to_city(building).ekibyou or pk::building_to_city(building).inago)
							continue;

						neighbor_building_list.add(building);
					}

					if (neighbor_building_list.count > 1)
					{
						neighbor_building_list.sort(function(a, b) {
							pk::force @force_a = pk::get_force(a.get_force_id());
							pk::force @force_b = pk::get_force(b.get_force_id());

							pk::person @kunshu_a = pk::get_person(force_a.kunshu);
							pk::person @kunshu_b = pk::get_person(force_b.kunshu);

							return kunshu_a.stat[武将能力_魅力] > kunshu_b.stat[武将能力_魅力];
						});

						ch::add_population(neighbor_building_list[0].get_id(), int(base_t.population * 人口基础增长率 * 2.0f * 0.8));
						ch::add_population(neighbor_building_list[1].get_id(), int(base_t.population * 人口基础增长率 * 2.0f * 0.2));

						BaseInfo @neighbor_base_t = @base_ex[neighbor_building_list[0].get_id()];

						if (int(base_t.population * 人口基础增长率 * 10.0f * 0.8) > 5000)
						{

							string t = pk::format("\x1b[2x{}\x1b[0x发生灾害,人民无法生存,约\x1b[2x{}\x1b[0x人口向\x1b[2x{}\x1b[0x和\x1b[2x{}\x1b[0x逃难", pk::decode(pk::get_name(building0)), int(base_t.population * 人口基础增长率 * 2.0f), pk::decode(pk::get_name(neighbor_building_list[0])), pk::decode(pk::get_name(neighbor_building_list[1])));

							pk::history_log(building0.pos, pk::get_force(building0.get_force_id()).color, pk::encode(t));
						}
					}
					// pk::trace(pk::format("{},魅力最高人口，{}", neighbor_building_list[0].get_id(), pk::decode(pk::get_name(neighbor_building_list[0])), neighbor_base_t.population));
				}
			}

			// 伤兵恢复
			for (int base_id = 0; base_id < 据点_末; ++base_id)
			{
				BaseInfo @base_t = @base_ex[base_id];
				if (base_t.wounded > 0)
				{
					pk::building @building0 = pk::get_building(base_id);
					int heal_num = int(base_t.wounded * (pk::enemies_around(pk::get_building(base_id)) ? 0.1 : 0.3));
					if (ch::has_skill(building0, 特技_医者))
						heal_num = int(base_t.wounded * (pk::enemies_around(pk::get_building(base_id)) ? 0.2 : 0.5));
					if (调试模式)
						pk::trace(pk::format("伤兵恢复，wounded:{},heal_num：{}", base_t.wounded, heal_num));
					heal_num = pk::min(base_t.wounded, pk::max(300, heal_num)); // 设定伤兵最低恢复数
					base_t.wounded -= uint32(heal_num);
					ch::add_troops(building0, heal_num, true);
				}
			}

			// 州逃兵分配
			for (int province_id = 0; province_id < 州_末; ++province_id)
			{
				// 人口平均分配到城市，或据点？
				pk::list<pk::building @> province_base = ch::get_base_list(province_id);
				pk::list<pk::city @> province_city = pk::get_city_list(pk::get_province(province_id));
				float count = province_city.count + (province_base.count - province_city.count) * 0.5f;
				settinginfo @set_t = @setting_ex;
				float per_deserter = set_t.province_deserter[province_id] * 0.5f / count; // 每旬分配总量的50%
				for (int i = 0; i < province_base.count; ++i)
				{
					int base_id = province_base[i].get_id();
					if (base_id < 城市_末)
					{
						set_t.province_deserter[province_id] -= uint32(per_deserter);
						BaseInfo @base_t = @base_ex[base_id];
						ch::add_population(base_id, uint32(per_deserter * 0.4f));
						// base_t.population += uint32(per_deserter*0.4f);
						ch::add_mil_pop_all(base_id, uint32(per_deserter * 0.6f));
						if (调试模式 and base_id == 城市_永安)
							pk::trace(pk::format("deserter,永安人口：{},总兵役人口：{}", base_t.population, base_t.mil_pop_all));
					}
					else
					{
						set_t.province_deserter[province_id] -= uint32(0.5f * per_deserter);
						BaseInfo @base_t = @base_ex[base_id];
						ch::add_population(base_id, uint32(0.5f * per_deserter * 0.4f));
						ch::add_mil_pop_all(base_id, uint32(0.5f * per_deserter * 0.6f));
						if (调试模式 and base_id == 城市_永安)
							pk::trace(pk::format("deserter 2,永安人口：{},总兵役人口：{}", base_t.population, base_t.mil_pop_all));
					}
				}
			}
		}

		void onNewDay2()
		{
			if (!开启人口系统)
				return;

			// 发生两次攻破后会出现人口锐减且无法回归的问题,要改的话太麻烦了.累加的话又会导致人口波动特别大,先取消掉后面再想办法
			return_pop_conduct();

			mil_pop_change();

			set_interior_land();
		}

		void return_pop_conduct()
		{
			if (!开启人口系统) return;
			for (int base_id = 0; base_id < 据点_末; ++base_id)
			{
				BaseInfo@ base_t = @base_ex[base_id];
				if (base_t.return_timer >= 0 and base_t.return_timer != 255)
				{
					if (!base_t.return_buf)
					{
						if (base_id < 据点_城市末 and pk::get_city(base_id).num_devs > 9 and pk::get_city(base_id).public_order > 89)
						{
							base_t.return_timer = 5;//此时作为buf计数
							base_t.return_buf = true;
						}
						if (base_id >= 据点_城市末 and base_id < 据点_末)
						{
							if (base_t.public_order > 89)
							{
								base_t.return_timer = 5;//此时作为buf计数
								base_t.return_buf = true;
							}
							//小城以什么判断呢？也引入治安概念？
						}
					}

					// + (city_t.return_buf?0.08f:0.f)
					if (base_t.return_buf)
					{
						//if (base_id == 城市_襄阳) pk::trace(pk::format("onNewDay2 return_buf,襄阳人口：{}", base_t.population));
						int temp = int(base_t.population + (base_t.return_pop * 0.3f));//每回合返回总返回人口的30%
						int temp2 = int((temp - base_t.population) * 0.3f);//总兵役人口算增量
						base_t.return_pop -= uint32(base_t.return_pop * 0.3f);
						//base_t.population = uint32(temp);
						ch::add_population(base_id, uint32(base_t.return_pop * 0.3f));
						ch::add_mil_pop_all(base_id, temp2);
						//base_t.mil_pop_all += uint32(temp2);
					}
					base_t.return_timer -= 1;

				}
			}
		}

		void mil_pop_change()
		{
			for (int base_id = 0; base_id < 据点_末; ++base_id)
			{
				BaseInfo@ base_t = @base_ex[base_id];
				//可用兵役过多时，不转化，附加设定，总兵役提供2倍人口增长
				if (base_t.mil_pop_av > 60000) continue;
				if (base_t.mil_pop_av > base_t.mil_pop_all and base_t.mil_pop_av > 50000) continue;
				if (base_t.mil_pop_av > (base_t.mil_pop_all*4/5) and base_t.mil_pop_av > 55000) continue;
				
				//每旬总兵役向可用兵役转化，基础值在1200?受到兵舍等级(0.2,0.4,0.7)，太守魅力(0.3)，都市名声影响(0.5)？
				pk::building@ building0 = pk::get_building(base_id);
				int taishu_id = pk::get_taishu_id(building0);
				if (taishu_id != -1)
				{
					
					
					pk::person@ taishu = pk::get_person(taishu_id);
					//减少能力和特技对转化的影响，方便调控
					float inf_a = 1.0f;
					if (taishu.stat[武将能力_魅力] < 35) inf_a -= 0.15f;
					else if (taishu.stat[武将能力_魅力] < 45) inf_a -= 0.1f;
					else if (taishu.stat[武将能力_魅力] < 55) inf_a -= 0.05f;
					else if (taishu.stat[武将能力_魅力] < 85) inf_a = inf_a;
					else if (taishu.stat[武将能力_魅力] < 95) inf_a += 0.05f;
					else if (taishu.stat[武将能力_魅力] < 105) inf_a += 0.1f;
					else  inf_a += 0.15f;

					if (ch::has_skill(building0, 特技_名声, true)) inf_a += 0.3f;

					if (base_id < 城市_末)
					{
						pk::city@ city0 = pk::get_city(base_id);
						float level = func_5c4600(city0);
						if (level == 1.5f) inf_a += 0.9f;
						else if (level == 1.2f)  inf_a += 0.6f;
						else if (level == 1.0f)  inf_a += 0.3f;
						else inf_a = inf_a;

					}
					else
					{
						inf_a += 0.2f;//小城默认兵舍为1级
					}

					if (base_t.mil_pop_all > 30000 and base_t.mil_pop_av < 5000)
					{
						inf_a += 0.5f;
					}

					int exp_mil = 200 + int(600.f * inf_a);//总兵役向可用兵役转化公式
					//pk::trace(pk::format("{}:inf_a:{},exp_mil：{}", pk::decode(pk::get_name(building0)), inf_a, exp_mil));
					
					if (调试模式) pk::trace(pk::format("before base_t.mil_pop_all:{},base_t.mil_pop_av：{}", base_t.mil_pop_all, base_t.mil_pop_av));
					if (exp_mil >= int(base_t.mil_pop_all))
					{
						exp_mil = base_t.mil_pop_all;
						base_t.mil_pop_all = 0;
						ch::add_mil_pop_av(base_id, exp_mil); //base_t.mil_pop_av += exp_mil;
					}
					else
					{
						ch::add_mil_pop_all(base_id, -exp_mil); //base_t.mil_pop_all -= exp_mil;
						ch::add_mil_pop_av(base_id, exp_mil); //base_t.mil_pop_av += exp_mil;
					}
					if (调试模式) pk::trace(pk::format("after base_t.mil_pop_all:{},base_t.mil_pop_av：{}", base_t.mil_pop_all, base_t.mil_pop_av));


					//境内敌军影响人口及总兵役
					int has_enemy_type = has_enemy(pk::get_building(base_id));
					float enemy_inf = has_enemy_type == 2 ? -0.03f : (has_enemy_type == 1 ? -0.02f : 0.f);
					if (has_enemy_type != 0)
					{
						//BaseInfo@ base_t = @base_ex[base_id];
						int temp = int(base_t.population * enemy_inf);//境内敌军对人口的影响
						int temp2 = int(temp * 0.15f);
						ch::add_population(base_id, temp);
						ch::add_mil_pop_all(base_id, temp2);;//境内敌军也会小幅度影响总兵役人口
						//pk::trace(pk::format("enemy_inf:{},影响后人口：{},影响总兵役量：{}", enemy_inf, temp, temp2));				
					}
					//if (base_id == 城市_襄阳) pk::trace(pk::format("onNewDay2 enemy_inf,襄阳人口：{}", base_t.population));
				}


			}
		}
		// 把人口相关的信息显示移动到内政自动化中

		// void func_信息显示_人口信息()
		// {
		// 	// 光标指的坐标
		// 	if (!开启人口系统)
		// 		return;
		// 	pk::point cursor_pos = pk::get_cursor_hex_pos();
		// 	if (!pk::is_valid_pos(cursor_pos))
		// 		return;

		// 	// 光标上指示的建筑物
		// 	pk::building @building = pk::get_building(cursor_pos);
		// 	if (building is null || building.facility > 2)
		// 		return; // 城港官才显示

		// 	// if (!building.is_player()) return;
		// 	// if (!pk::is_player_controlled(building)) return;
		// 	// if (building.get_force_id() != pk::get_current_turn_force_id()) return;
		// 	// pk::trace("人口");

		// 	string building_name = pk::decode(pk::get_name(building));

		// 	string title = pk::format("据点信息(\x1b[1x{}\x1b[0x)", building_name);

		// 	int middle = int(pk::get_resolution().width) / 2;
		// 	int left = middle - 200;
		// 	int right = middle + 200;
		// 	int top = 5 + (pk::get_player_count() == 0 ? 100 : 0);
		// 	int bottom = top + 80;
		// 	int base_id = building.get_id();
		// 	// pk::draw_rect(pk::rectangle(left, top, right, bottom), 0xff00ccff);
		// 	BaseInfo @base_t = @base_ex[base_id];
		// 	BuildingInfo @base_p = @building_ex[base_id];

		// 	// if (base_id == 城市_襄阳) pk::trace(pk::format("func_信息显示_人口信息 0,襄阳人口：{}", base_t.population));
		// 	if (base_t.return_timer > 0 and base_t.return_timer != 255)
		// 	{
		// 		// pk::trace(pk::format("return time:{}", base_t.return_timer));
		// 		title = title + "(\x1b[2x新攻占\x1b[0x)";
		// 		if (调试模式)
		// 			title += base_t.return_pop;
		// 	}

		// 	if (false)
		// 	{
		// 		// pk::trace(pk::format("return time:{}", base_t.return_timer));
		// 		settinginfo @set_t = @setting_ex;

		// 		title = title + "州逃兵:" + "\x1b[2x" + set_t.province_deserter[pk::get_province_id(building.pos)] + "\x1b[0x";
		// 	}

		// 	pk::draw_text(pk::encode(title), pk::point(left + 5, top + 5), 0xffffffff, FONT_BIG, 0xff000000);

		// 	if (false and base_id >= 据点_城市末 and base_id < 据点_末)
		// 	{
		// 		// BaseInfo@ base_t = @base_ex[base_id];
		// 		string info_治安 = pk::format("治安: \x1b[1x{}\x1b[0x", base_t.public_order);
		// 		pk::draw_text(pk::encode(info_治安), pk::point(left + 10, top + 40 + 据点信息行数 * 20), 0xffffffff, FONT_SMALL, 0xff000000);
		// 		据点信息行数 += 1;
		// 	}

		// 	if (base_id < 据点_末)
		// 	{
		// 		// if (base_id == 城市_襄阳) pk::trace(pk::format("func_信息显示_人口信息,襄阳人口：{}", base_t.population));
		// 		// BaseInfo@ base_t = @base_ex[base_id];
		// 		// pk::point leftdown = pk::point(middle + 140, top + 40 + (据点信息行数 + 3) * 20 +5);

		// 		// pk::draw_filled_rect(pk::rectangle(pk::point(left, top), leftdown), ((0xff / 2) << 24) | 0x010101);//((0xff / 2) << 24) | 0x777777

		// 		string info_人口 = pk::format("人口: \x1b[1x{}\x1b[0x", base_t.population);
		// 		int level = ch::get_city_level(base_id);
		// 		string level_name = ch::get_level_string(level);
		// 		string info_规模 = pk::format("城市规模: \x1b[1x{}\x1b[0x", level_name);
		// 		string info_可用兵役 = pk::format("可用兵役: \x1b[1x{}\x1b[0x", base_t.mil_pop_av);
		// 		string info_总兵役 = pk::format("总兵役: \x1b[1x{}\x1b[0x", base_t.mil_pop_all);
		// 		string info_伤兵 = pk::format("伤兵: \x1b[16x{}\x1b[0x", base_t.wounded);
		// 		string info_额外城防 = pk::format("额外城防: \x1b[16x{}\x1b[0x", base_p.city_defense);
		// 		string info_征兵开支 = pk::format("征兵开支: \x1b[1x{}\x1b[0x", base_p.troops_effic);
		// 		string info_基础征兵 = pk::format("基础征兵/百元: \x1b[1x{}\x1b[0x", int(base_t.mil_pop_all / 36.0f));
		// 		string info_生产开支 = pk::format("生产开支: \x1b[1x{}\x1b[0x", base_p.produce_effic);
		// 		string info_基础产量 = pk::format("基础产量/百元: \x1b[1x{}\x1b[0x", int(sqrt(base_t.population / 10) + sqrt(building.troops)));
		// 		string info_筑城开支 = pk::format("筑城开支: \x1b[1x{}\x1b[0x", base_p.repair_effic);
		// 		string info_筑城增量 = pk::format("基础增量/百元: \x1b[1x{}\x1b[0x", int(sqrt(base_t.population / 10) + sqrt(building.troops)));

		// 		pk::draw_text(pk::encode(info_人口), pk::point(left + 20, top + 40 + 据点信息行数 * 20), 0xffffffff, FONT_SMALL, 0xff000000);
		// 		pk::draw_text(pk::encode(info_规模), pk::point(middle - 50, top + 40 + 据点信息行数 * 20), 0xffffffff, FONT_SMALL, 0xff000000);
		// 		pk::draw_text(pk::encode(info_总兵役), pk::point(left + 20, top + 40 + (据点信息行数 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
		// 		pk::draw_text(pk::encode(info_可用兵役), pk::point(middle - 50, top + 40 + (据点信息行数 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
		// 		pk::draw_text(pk::encode(info_伤兵), pk::point(left + 20, top + 40 + (据点信息行数 + 2) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
		// 		pk::draw_text(pk::encode(info_额外城防), pk::point(middle - 50, top + 40 + (据点信息行数 + 2) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
		// 		pk::draw_text(pk::encode(info_征兵开支), pk::point(left + 20, top + 40 + (据点信息行数 + 3) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
		// 		pk::draw_text(pk::encode(info_基础征兵), pk::point(middle - 50, top + 40 + (据点信息行数 + 3) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
		// 		pk::draw_text(pk::encode(info_生产开支), pk::point(left + 20, top + 40 + (据点信息行数 + 4) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
		// 		pk::draw_text(pk::encode(info_基础产量), pk::point(middle - 50, top + 40 + (据点信息行数 + 4) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
		// 		pk::draw_text(pk::encode(info_筑城开支), pk::point(left + 20, top + 40 + (据点信息行数 + 5) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
		// 		pk::draw_text(pk::encode(info_筑城增量), pk::point(middle - 50, top + 40 + (据点信息行数 + 5) * 20), 0xffffffff, FONT_SMALL, 0xff000000);

		// 		// 据点信息行数 += 3;
		// 	}
		// }

		void onUnitRemove(pk::unit @unit, pk::hex_object @dst, int type)
		{
			// pk::trace(pk::format("onUnitRemove0", 1));
			if (dst is null)
				return;
			if (type == 1)
			{
				pk::building @dst_building = dst.get_type_id() == pk::building::type_id ? dst : null; // 将hex_object转为建筑指针
				// pk::trace(pk::format("onUnitRemove0,id:{}", dst_building.get_id()));
				if (!pk::is_alive(dst_building))
					return;
				// pk::trace(pk::format("onUnitRemove1", 1));
				int base_id = dst_building.get_id();
				int unit_id = unit.get_id();

				BaseInfo @base_t = @base_ex[base_id];
				unitinfo @unit_t = @unit_ex[unit_id];
				// if (base_id == 城市_襄阳) pk::trace(pk::format("onUnitRemove,襄阳伤兵：{}", base_t.wounded));
				base_t.wounded += pk::max(0, unit_t.wounded);
				unit_t.wounded = 0;
			}
			if (type == 0)
			{
				// pk::trace("remove type 0");
				pk::building @dst_building = dst.get_type_id() == pk::building::type_id ? dst : null;
				pk::unit @dst_unit = dst.get_type_id() == pk::unit::type_id ? dst : null;
				// 分为部队击破部队，据点击破部队，其他建筑击破部队，同时需区分是否友军,（区分是否异族势力）
				if (dst_unit !is null)
				{
					// unit 是被灭方
					// pk::trace(pk::format("{}击破{}",pk::decode(pk::get_name(dst_unit)), pk::decode(pk::get_name(unit))));
					unitinfo @unit_t = @unit_ex[unit.get_id()];
					int unit_wounded = unit_t.wounded;
					unit_t.wounded = 0;

					// pk::trace(pk::format("{}击破{}, 伤兵：{}", pk::decode(pk::get_name(dst_unit)), pk::decode(pk::get_name(unit)), unit_wounded));
					if (pk::is_enemy(unit, dst_unit))
					{

						unitinfo dst_t(dst_unit.get_id());
						dst_t.wounded += uint16(0.5f * unit_wounded);
						dst_t.update(dst_unit.get_id());
						int unit_service = pk::get_service(unit);
						if (unit_service > -1 and unit_service < 据点_末)
						{
							BaseInfo @base_t = @base_ex[unit_service];
							base_t.wounded += uint16(0.5f * unit_wounded);
						}
					}
					else
					{
						int unit_service = pk::get_service(unit);
						if (unit_service != -1)
						{
							BaseInfo @base_t = @base_ex[unit_service];
							base_t.wounded += uint16(unit_wounded);
						}
					}
				}
				if (dst_building !is null)
				{
					// unit 是被灭方
					unitinfo @unit_t = @unit_ex[unit.get_id()];
					int unit_wounded = unit_t.wounded;
					unit_t.wounded = 0;

					if (pk::is_enemy(unit, dst_building))
					{
						int dst_building_id = dst_building.get_id();
						if (pk::is_valid_base_id(dst_building_id))
						{
							BaseInfo @dst_t = @base_ex[dst_building.get_id()];
							dst_t.wounded += uint16(0.5f * unit_wounded);
						}
						int unit_service = pk::get_service(unit);
						if (pk::is_valid_base_id(unit_service))
						{
							BaseInfo @base_t = @base_ex[unit_service];
							base_t.wounded += uint16(0.5f * unit_wounded);
						}
					}
					else
					{
						int unit_service = pk::get_service(unit);
						if (pk::is_valid_base_id(unit_service))
						{
							BaseInfo @base_t = @base_ex[unit_service];
							base_t.wounded += uint16(unit_wounded);
						}
					}
				}
			}
		}

		int has_enemy(pk::building @base)
		{
			// 1格内敌部队数
			bool has_enemy_units1 = false;
			// 3格内敌部队数
			bool has_enemy_units3 = false;

			auto range = pk::range(base.get_pos(), 1, 3 + (base.facility == 设施_都市 ? 1 : 0));
			for (int i = 0; i < int(range.length); i++)
			{
				auto unit = pk::get_unit(range[i]);
				if (pk::is_alive(unit))
				{
					int distance = pk::get_distance(base.get_pos(), range[i]);
					if (pk::is_enemy(base, unit))
					{
						if (distance <= 1)
						{
							has_enemy_units1 = true;
							break;
						}
						if (distance <= 3)
						{
							has_enemy_units3 = true;
						}
					}
					else
					{
					}
				}
			}

			if (has_enemy_units1)
				return 2;
			if (has_enemy_units3)
				return 1;
			return 0;
		}

		// 可以通过返回值判断是几级兵舍
		float func_5c4600(pk::city @city)
		{
			int level1 = 0, level2 = 0;
			for (int i = 0; i < int(city.max_devs); i++)
			{
				pk::building @building = city.dev[i].building;
				if (pk::is_alive(building))
				{
					switch (building.facility)
					{
					case 시설_병영:
						building.completed ? level1++ : 0;
						break;
					case 시설_병영2단:
						building.completed ? level2++ : level1++;
						break;
					case 시설_병영3단:
						building.completed ? 0 : level2++;
						break;
					}
				}
			}
			if (int(city.barracks_counter) > level1 + level2)
				return 1.5f;
			if (int(city.barracks_counter) > level1)
				return 1.2f;
			if (level1 > 0)
				return 1.f;
			return 0.f;
		}

		int get_city_revenue(int city_id)
		{
			return cast<pk::func150_t>(pk::get_func(150))(pk::get_city(city_id));
		}

		void set_interior_land()
		{
			// 根据人口规模调整内政用地
			for (int city_id = 0; city_id < 城市_末; ++city_id)
			{
				ch::set_interior_land(city_id);
			}
		}

	} // class Main

	Main main;

	// 人口数据格式为: PK2.2地名 - 新地图地名{总人口,兵役人口,可用兵役}
	// 由于原始数据中方差太大,考虑游戏性和平衡性,此数据对人口过多和过少的城市进行了修正,所有关隘(虎牢关)和战争地名(赤壁)均保留15000,
	// 总人口3500万,大致符合东汉末年黄巾起义之后人口设定

	const array<array<int>> 剧本人口 = {
{415267,417811,451919,526690,526690,600299,792089,552530,493566,526690,659728,456108,417811,552530,0,552530},
{458558,982812,463895,496080,496080,577303,755283,462857,484555,496080,633888,464051,982812,462857,0,462857},
{1104986,1238356,1295819,1320456,1320456,1412020,1594808,1163428,1319018,1320456,1469907,1110942,1238356,1163428,0,1163428},
{740743,1158543,1230339,1286781,1286781,1330325,1529276,1927063,898568,1286781,1401483,796630,1158543,1927063,0,1927063},
{439627,757100,781069,854975,854975,891785,1069543,1041008,495209,854975,951378,472874,757100,1041008,0,1041008},
{1041278,1161704,1189025,1221299,1221299,1183943,1377914,1061468,1215251,1221299,1258243,1064749,1161704,1061468,0,1061468},
{1417155,1615374,1665301,1736386,1736386,2285604,2423176,2510869,1778427,1736386,2341433,1443512,1615374,2510869,0,2510869},
{847358,929175,940619,980023,980023,1077088,1245389,654397,968538,980023,1139956,752976,929175,654397,0,654397},
{966374,1072147,1050420,1039341,1039341,1014842,1181621,1022696,1053314,1039341,1081635,971065,1072147,1022696,0,1022696},
{1080428,1208368,1223954,1291111,1291111,1390090,1596970,1049177,1267890,1291111,1454616,1084549,1208368,1049177,0,1049177},
{825695,903676,992069,981020,981020,1083170,1248402,1132963,994550,981020,1153152,951406,903676,1132963,0,1132963},
{892188,893925,1016092,1057645,1057645,1151776,1331591,878786,1045149,1057645,1217737,881044,893925,878786,0,878786},
{1073306,1199841,1235666,1265196,1265196,1366088,1520744,1075860,1260328,1265196,1426348,1052284,1199841,1075860,0,1075860},
{1323259,1502209,1474708,1673827,1673827,2011461,2189528,1270332,1586090,1673827,2066223,1325496,1502209,1270332,0,1270332},
{1065562,1191334,1168007,1156869,1156869,1260734,1459315,1036930,1171735,1156869,1323448,1074368,1191334,1036930,0,1036930},
{1463681,1671734,78005,791562,791562,1078101,1238367,1412390,393107,791562,1138942,1714652,1671734,1412390,0,1412390},
{919418,1015024,994065,1034274,1034274,1129110,1325450,859448,1022792,1034274,1199177,888108,1015024,859448,0,859448},
{1352813,1537727,1549618,1778090,1778090,2137773,2292729,1489109,1676091,1778090,2186381,1420126,1537727,1489109,0,1489109},
{566957,595865,580534,569691,569691,650521,849094,619032,580096,569691,722002,570219,595865,619032,0,619032},
{399238,399133,386444,422164,422164,507357,689922,466913,407863,422164,563892,399958,399133,466913,0,466913},
{769396,836094,857356,895448,895448,996977,1175693,829726,883987,895448,1055787,798098,836094,829726,0,829726},
{375517,371548,393327,419808,419808,500008,695023,421234,410205,419808,559747,386733,371548,421234,0,421234},
{1041278,1161704,1184812,1370915,1370915,1706462,1863950,1153947,1287398,1370915,1771821,1121770,1161704,1153947,0,1153947},
{1102843,1236206,1253834,1299785,1299785,1424126,1581851,1098595,1287398,1299785,1484149,1112564,1236206,1098595,0,1098595},
{849336,931145,943564,992003,992003,1104538,1304024,928664,975521,992003,1161295,868221,931145,928664,0,928664},
{820991,897822,918126,962134,962134,1061928,1272585,626941,947680,962134,1134892,728896,897822,626941,0,626941},
{1180761,1329668,1304484,1362098,1362098,1480667,1661164,1301865,1344264,1362098,1541535,1256989,1329668,1301865,0,1301865},
{627599,667925,683570,671782,671782,768902,945900,679479,683862,671782,837211,631480,667925,679479,0,679479},
{1129712,1268570,1296901,1339036,1339036,1432951,1608881,1166593,1328879,1339036,1494034,1128282,1268570,1166593,0,1166593},
{1457839,1664656,1702866,1881868,1881868,2232178,2405980,1509626,1806117,1881868,2293563,1469963,1664656,1509626,0,1509626},
{634849,675978,702405,760858,760858,854653,1047521,659749,737533,760858,923633,658145,675978,659749,0,659749},
{1196430,1348392,1322956,1364300,1364300,1477322,1676509,1166593,1354182,1364300,1537097,1200839,1348392,1166593,0,1166593},
{458972,468728,455104,486260,486260,500008,665408,455579,474777,486260,558091,453399,468728,455579,0,455579},
{983182,1091743,1069753,1058665,1058665,1222682,1419483,974343,1072784,1058665,1292825,997273,1091743,974343,0,974343},
{1128634,1266404,1242069,1296530,1296530,1383531,1553887,1139246,1279800,1296530,1454616,1136983,1266404,1139246,0,1139246},
{439627,446256,432934,422164,422164,410209,593316,464478,431449,422164,464330,441988,446256,464478,0,464478},
{721821,779432,801581,840654,840654,933254,1103309,735959,828277,840654,1001721,736196,779432,735959,0,735959},
{454424,463887,492169,531700,531700,621809,820454,507157,516705,531700,685851,453191,463887,507157,0,507157},
{917696,1013002,1045350,1093509,1093509,1200649,1368611,935573,1077926,1093509,1260330,916175,1013002,935573,0,935573},
{1454338,1659942,1712298,1897715,1897715,2154044,2304824,1481154,1818197,1897715,2214888,1497750,1659942,1481154,0,1481154},
{420108,423302,410289,399531,399531,389446,574626,492270,408643,399531,443162,423895,423302,492270,0,492270},
{852305,935090,915204,904193,904193,876496,1075310,879756,917139,904193,943701,865964,935090,879756,0,879756}
	};

	const array<array<int>> 剧本兵役 = {
	{38664,38783,40334,43544,43544,46487,53399,44599,42152,43544,48734,40521,38783,44599,0,44599},
{40630,59482,40865,42259,42259,45588,52144,40820,41766,42259,47770,40872,59482,40820,0,40820},
{63070,66768,68300,68946,68946,71297,75771,64717,68909,68946,72743,63240,66768,64717,0,64717},
{51639,64581,66552,68061,68061,69203,74198,83291,56875,68061,71030,53552,64581,83291,0,83291},
{39782,52206,53026,55478,55478,56660,62051,61217,42222,55478,58523,41259,52206,61217,0,61217},
{61225,64669,65425,66307,66307,65285,70430,61816,66143,66307,67302,61912,64669,61816,0,61816},
{71426,76258,77427,79063,79063,90709,93399,95074,80014,79063,91810,72087,76258,95074,0,95074},
{55231,57836,58191,59397,59397,62269,66958,48536,59048,59397,64061,52064,57836,48536,0,48536},
{58982,62126,61494,61168,61168,60443,65221,60677,61578,61168,62401,59125,62126,60677,0,60677},
{62366,65955,66379,68176,68176,70741,75822,61457,67560,68176,72364,62485,65955,61457,0,61457},
{54520,57037,59761,59427,59427,62445,67039,63864,59836,59427,64430,58524,57037,63864,0,63864},
{56673,56728,60480,61705,61705,64392,69236,56246,61339,61705,66210,56318,56728,56246,0,56246},
{62160,65722,66696,67488,67488,70127,73991,62234,67358,67488,71657,61548,65722,62234,0,62234},
{69019,73538,72862,77625,77625,85095,88782,67625,75564,77625,86246,69078,73538,67625,0,67625},
{61935,65488,64844,64534,64534,67369,72481,61097,64948,64534,69024,62191,65488,61097,0,61097},
{72589,77577,16757,53381,53381,62299,66769,71306,37618,53381,64032,78566,77577,71306,0,71306},
{57531,60449,59821,61019,61019,63755,69076,55623,60679,61019,65704,56543,60449,55623,0,55623},
{69786,74403,74690,80007,80007,87726,90850,73217,77678,80007,88718,71501,74403,73217,0,73217},
{45177,46315,45715,45286,45286,48392,55287,47207,45698,45286,50982,45307,46315,47207,0,47207},
{37911,37906,37298,38984,38984,42737,49836,40998,38318,38984,45055,37945,37906,40998,0,40998},
{52629,54862,55556,56776,56776,59909,65057,54653,56412,56776,61650,53601,54862,54653,0,54653},
{36767,36572,37629,38875,38875,42426,50020,38941,38428,38875,44889,37312,36572,38941,0,38941},
{61225,64669,65309,70251,70251,78378,81915,64453,68078,70251,79865,63548,64669,64453,0,64453},
{63009,66710,67184,68404,68404,71602,75463,62888,68078,68404,73095,63286,66710,62888,0,62888},
{55295,57897,58282,59759,59759,63058,68516,57820,59261,59759,64658,55907,57897,57820,0,57820},
{54365,56852,57491,58853,58853,61829,67685,47507,58409,58853,63918,51225,56852,47507,0,47507},
{65197,69186,68528,70025,70025,73009,77331,68459,69565,70025,74495,67269,69186,68459,0,68459},
{47532,49036,49606,49177,49177,52612,58354,49458,49617,49177,54899,47679,49036,49458,0,49458},
{63772,67578,68328,69430,69430,71823,76105,64805,69166,69430,73338,63732,67578,64805,0,64805},
{72444,77412,78296,82308,82308,89642,93067,73720,80635,82308,90867,72745,77412,73720,0,73720},
{47806,49330,50285,52336,52336,55468,61409,48734,51527,52336,57663,48675,49330,48734,0,48734},
{65628,69672,69011,70081,70081,72927,77688,64805,69821,70081,74387,65749,69672,64805,0,64805},
{40648,41078,40476,41839,41839,42426,48943,40497,41342,41839,44823,40400,41078,40497,0,40497},
{59493,62691,62057,61734,61734,66344,71485,59225,62145,61734,68221,59918,62691,59225,0,59225},
{63742,67520,66868,68319,68319,70574,74793,64041,67876,68319,72364,63977,67520,64041,0,64041},
{39782,40081,39478,38984,38984,38428,46216,40891,39410,38984,40885,39889,40081,40891,0,40891},
{50976,52971,53718,55012,55012,57963,63023,51472,54605,55012,60051,51481,52971,51472,0,51472},
{40446,40865,42092,43750,43750,47312,54347,42729,43129,43750,49689,40391,40865,42729,0,42729},
{57477,60388,61345,62742,62742,65744,70192,58035,62293,62742,67358,57430,60388,58035,0,58035},
{72357,77303,78512,82654,82654,88059,91089,73021,80904,82654,89295,73429,77303,73021,0,73021},
{38889,39037,38432,37925,37925,37443,45482,42097,38355,37925,39942,39064,39037,42097,0,42097},
{55392,58020,57399,57053,57053,56172,62218,56277,57460,57053,58286,55834,58020,56277,0,56277}
	};
	const array<array<int>> 史实模式人口 = {
		/*0襄平-辽东*/ {380574, 38952, 16317},
		/*1北平-北平*/ {639299, 63255, 17751},
		/*2蓟-蓟*/ {1281089, 136787, 15595},
		/*3南皮-中山*/ {652491, 83610, 17870},
		/*4平原-渤海*/ {1126165, 129062, 19687},
		/*5晋阳-晋阳*/ {231983, 32664, 14888},
		/*6邺-邺*/ {897242, 64138, 18046},
		/*7北海-北海*/ {855777, 96197, 18732},
		/*8广陵-下邳*/ {616271, 72919, 17639},
		/*9下邳-彭城*/ {499298, 51033, 17011},
		/*10寿春-淮南*/ {638874, 63208, 17736},
		/*11濮阳-濮阳*/ {605229, 62803, 17601},
		/*12陈留-陈留*/ {867516, 86390, 18796},
		/*13许昌-许昌*/ {1430351, 132261, 20753},
		/*14汝南-汝南*/ {2108028, 237558, 22519},
		/*15洛阳-洛阳*/ {1022272, 118030, 19343},
		/*16宛-南阳*/ {2438863, 239873, 23291},
		/*17长安-长安*/ {582155, 56906, 15635},
		/*18上庸-广陵*/ {415870, 59541, 16513},
		/*19安定-安定*/ {59283, 26587, 2195},
		/*20天水-陇西*/ {61479, 26831, 2277},
		/*21武威-武威*/ {65124, 27236, 2412},
		/*22建业-建业*/ {638874, 73208, 17736},
		/*23吴-吴*/ {707887, 74209, 18069},
		/*24会稽-建安*/ {863632, 88181, 16060},
		/*25庐江-庐江*/ {428330, 49814, 16604},
		/*26柴桑-柴桑*/ {1662408, 161378, 27126},
		/*27江夏-江夏*/ {267775, 36419, 15473},
		/*28新野-河东*/ {800725, 82302, 27434},
		/*29襄阳-襄阳*/ {531254, 55694, 18564},
		/*30江陵-江陵*/ {723598, 84844, 18281},
		/*31长沙-长沙*/ {1057051, 128561, 19520},
		/*32武陵-武陵*/ {254480, 26053, 15351},
		/*33桂阳-云中*/ {268727, 26525, 18175},
		/*34零陵-朔方*/ {78530, 19836, 5945},
		/*35永安-巴东*/ {365226, 28358, 16119},
		/*36汉中-汉中*/ {267206, 26467, 15489},
		/*37梓潼-梓潼*/ {501794, 51310, 17103},
		/*38江州-巴郡*/ {1089620, 118846, 19615},
		/*39成都-成都*/ {1353266, 131474, 20491},
		/*40建宁-南中*/ {894532, 86059, 12019},
		/*41云南-合浦*/ {864415, 38490, 13496},
		/*42壶关-上党*/ {127403, 11666, 6555},
		/*43虎牢关-虎牢关*/ {15000, 1666, 555},
		/*44潼关-潼关*/ {15000, 1666, 555},
		/*45函谷关-函谷关*/ {15000, 1666, 555},
		/*46武关-上洛*/ {15000, 1666, 555},
		/*47阳平关-阳平关*/ {15000, 1666, 555},
		/*48大散关-北地*/ {52530, 5836, 1945},
		/*49葭萌关-剑阁*/ {15000, 1666, 555},
		/*50涪水关-阆中*/ {15000, 1666, 555},
		/*51绵竹关-绵竹*/ {15000, 1666, 555},
		/*52柳城-昌黎*/ {430173, 50019, 16673},
		/*53代-平原*/ {491455, 57939, 9313},
		/*54山越-黎阳*/ {552093, 53565, 17855},
		/*55上党-常山*/ {508959, 53217, 17739},
		/*56东平-城阳*/ {453044, 50338, 16779},
		/*57任城-雁门*/ {57342, 6371, 2123},
		/*58淮阴-零陵*/ {551347, 67927, 19309},
		/*59堂邑-桂阳*/ {520578, 61175, 17058},
		/*60小沛-濡须*/ {60012, 6668, 2222},
		/*61谯-济北*/ {235957, 35661, 5220},
		/*62官渡-上郡*/ {28609, 6734, 2244},
		/*63上洛-轘辕关*/ {15000, 1666, 555},
		/*64乌桓-河内*/ {829918, 25546, 8515},
		/*65陈仓-苍梧*/ {465232, 20581, 6860},
		/*66鲜卑-上谷*/ {60609, 6734, 2244},
		/*67西城-上庸*/ {464843, 50538, 6846},
		/*68合肥-定襄*/ {52530, 5836, 1945},
		/*69宛陵-五原*/ {55509, 6167, 2055},
		/*70考城-曲阿*/ {362798, 36977, 2325},
		/*71白水-会稽*/ {487468, 50829, 6943},
		/*72查读-临淮*/ {493188, 49243, 3081},
		/*73建安-豫章*/ {668299, 74255, 11418},
		/*74赤壁-赤壁*/ {15000, 1666, 555},
		/*75南蛮-鄱阳*/ {208874, 23208, 7736},
		/*76西羌-谯*/ {674762, 60529, 10176},
		/*77永昌-弋阳*/ {162798, 16977, 2325},
		/*78北地-新野*/ {128568, 16507, 2169},
		/*79樊城-樊城*/ {114445, 16049, 2016},
		/*80夷陵-华容*/ {15000, 1666, 555},
		/*81新野-临川*/ {325357, 27039, 2346},
		/*82麦城-金城*/ {52530, 5836, 1945},
		/*83越巂-陈仓*/ {15000, 1666, 555},
		/*84街亭-朱提*/ {37674, 6408, 2136},
		/*85雒城-临沅*/ {251466, 6829, 2276},
		/*86阆中-夷陵*/ {62895, 6988, 2329}

	};

	// 人口数据格式为: PK2.2地名 - 新地图地名{总人口,兵役人口,可用兵役}
	// 进一步修正了城市间差异,所有关隘(虎牢关)和战争地名(赤壁)均保留15000,
	// 总人口3500万,大致符合东汉末年黄巾起义之后人口设定

	const array<array<int>> 虚构模式人口 = {
		/*0襄平-辽东*/ {493525, 49341, 25548},
		/*1北平-北平*/ {539649, 62876, 26647},
		/*2蓟-蓟*/ {524143, 47950, 24976},
		/*3南皮-中山*/ {346215, 72288, 26736},
		/*4平原-渤海*/ {848967, 89813, 28062},
		/*5晋阳-晋阳*/ {385317, 12903, 24403},
		/*6邺-邺*/ {968008, 63314, 26867},
		/*7北海-北海*/ {640066, 77539, 27373},
		/*8广陵-下邳*/ {628023, 67509, 26562},
		/*9下邳-彭城*/ {565288, 56476, 26085},
		/*10寿春-淮南*/ {639437, 62853, 26635},
		/*11濮阳-濮阳*/ {822372, 62651, 26534},
		/*12陈留-陈留*/ {1045124, 73480, 27420},
		/*13许昌-许昌*/ {1156778, 90919, 28812},
		/*14汝南-汝南*/ {561524, 121850, 30013},
		/*15洛阳-洛阳*/ {808860, 85889, 27816},
		/*16宛-南阳*/ {1249349, 122442, 30523},
		/*17长安-长安*/ {10610393, 109637, 25008},
		/*18上庸-广陵*/ {515904, 61003, 25701},
		/*19安定-安定*/ {194785, 20290, 9370},
		/*20天水-陇西*/ {198360, 20662, 9544},
		/*21武威-武威*/ {204155, 21266, 9822},
		/*22建业-建业*/ {1123437, 127642, 26635},
		/*23吴-吴*/ {673088, 68103, 26884},
		/*24会稽-建安*/ {743454, 74238, 25346},
		/*25庐江-庐江*/ {523575, 55798, 25771},
		/*26柴桑-柴桑*/ {931475, 100430, 32940},
		/*27江夏-江夏*/ {313976, 47709, 24878},
		/*28新野-河东*/ {515866, 71721, 33126},
		/*29襄阳-襄阳*/ {983097, 58999, 27250},
		/*30江陵-江陵*/ {880517, 72820, 27041},
		/*31长沙-长沙*/ {822504, 89639, 27943},
		/*32武陵-武陵*/ {403568, 40352, 24780},
		/*33桂阳-云中*/ {214711, 40716, 26963},
		/*34零陵-朔方*/ {224186, 24794, 15421},
		/*35永安-巴东*/ {325184, 33873, 25392},
		/*36汉中-汉中*/ {313536, 40672, 24891},
		/*37梓潼-梓潼*/ {566699, 56629, 26156},
		/*38江州-巴郡*/ {635079, 86185, 28011},
		/*39成都-成都*/ {1230640, 90648, 28629},
		/*40建宁-南中*/ {576638, 73340, 21926},
		/*41云南-合浦*/ {743791, 49047, 23234},
		/*42壶关-上党*/ {185548, 27002, 16193},
		/*43虎牢关-虎牢关*/ {15000, 1666, 555},
		/*44潼关-潼关*/ {15000, 1666, 555},
		/*45函谷关-函谷关*/ {15000, 1666, 555},
		/*46武关-上洛*/ {15000, 1666, 555},
		/*47阳平关-阳平关*/ {15000, 1666, 555},
		/*48大散关-北地*/ {93355, 19098, 8820},
		/*49葭萌关-剑阁*/ {97980, 10204, 4712},
		/*50涪水关-阆中*/ {97980, 10204, 4712},
		/*51绵竹关-绵竹*/ {97980, 10204, 4712},
		/*52柳城-昌黎*/ {154701, 55912, 25825},
		/*53代-平原*/ {180831, 60176, 19301},
		/*54山越-黎阳*/ {164424, 57860, 26725},
		/*55上党-常山*/ {170731, 57672, 26638},
		/*56东平-城阳*/ {138468, 56090, 25907},
		/*57任城-雁门*/ {191570, 19955, 9215},
		/*58淮阴-零陵*/ {194022, 65157, 27791},
		/*59堂邑-桂阳*/ {177209, 61834, 26121},
		/*60小沛-濡须*/ {195979, 20414, 9428},
		/*61谯-济北*/ {188603, 47210, 14450},
		/*62官渡-上郡*/ {135314, 20515, 9474},
		/*63上洛-轘辕关*/ {15000, 1666, 555},
		/*64乌桓-河内*/ {128799, 39958, 18455},
		/*65陈仓-苍梧*/ {145663, 35865, 16565},
		/*66鲜卑-上谷*/ {196951, 20515, 9474},
		/*67西城-上庸*/ {245435, 56202, 16548},
		/*68合肥-定襄*/ {183355, 19098, 8820},
		/*69宛陵-五原*/ {188483, 19633, 9066},
		/*70考城-曲阿*/ {181862, 48074, 9644},
		/*71白水-会稽*/ {158551, 56363, 16665},
		/*72查读-临淮*/ {161819, 55477, 11101},
		/*73建安-豫章*/ {253996, 68124, 21371},
		/*74赤壁-赤壁*/ {15000, 1666, 555},
		/*75南蛮-鄱阳*/ {165622, 38085, 17591},
		/*76西羌-谯*/ {217151, 61507, 20175},
		/*77永昌-弋阳*/ {122786, 32574, 9644},
		/*78北地-新野*/ {186851, 32120, 9315},
		/*79樊城-樊城*/ {170638, 31671, 8980},
		/*80夷陵-华容*/ {15000, 1666, 555},
		/*81新野-临川*/ {156321, 41109, 9687},
		/*82麦城-金城*/ {183355, 19098, 8820},
		/*83越巂-陈仓*/ {15000, 1666, 555},
		/*84街亭-朱提*/ {155278, 20012, 9243},
		/*85雒城-临沅*/ {221171, 20659, 9541},
		/*86阆中-夷陵*/ {200631, 20899, 9652},

	};

}
