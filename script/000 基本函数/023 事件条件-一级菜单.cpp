﻿// ## 2022/06/19 # 铃 # 为新内政系统增加新的一级菜单，暂时注释掉早前的自动内政 ##
// ## 2021/09/26 # 江东新风 # 事件手动触发的一级菜单 ##
// ## 2021/01/18 # 江东新风 # 禁止拆建拼音改简体 ##
// ## 2020/12/15 # messi # 内政自动化改为太守自治 ##
// ## 2020/08/11 # 氕氘氚 # 修复太守不在时点击买卖可能出现的指針錯誤 ##
// ## 2020/08/08 # 氕氘氚 # 新增三个一级菜单 ##

namespace event_menu
{
    // ======================================================================

 //   const string shortcut_自动内政 = "z";

    // ======================================================================

    int 菜单_事件 = -1;

    // ======================================================================

    class Main
    {

        pk::building @building_;
        pk::force @force_;
        pk::person @taishu_;
        pk::person @kunshu_;
        pk::city @city_;
        pk::gate @gate_;
        pk::port @port_;
        pk::district @district_;

        int menu_city_id_;
        int menu_force_id_;

        Main()
        {

            pk::menu_item menu_item_事件;
            menu_item_事件.menu = 0;
            menu_item_事件.pos = 10;
            menu_item_事件.init = pk::building_menu_item_init_t(init_菜单_事件);
            menu_item_事件.is_visible = pk::menu_item_is_visible_t(isVisible_菜单_事件);
            menu_item_事件.is_enabled = pk::menu_item_is_enabled_t(isEnabled_菜单_事件);
            menu_item_事件.get_text = pk::menu_item_get_text_t(getText_菜单_事件);
            menu_item_事件.get_desc = pk::menu_item_get_desc_t(getDesc_菜单_事件);
            菜单_事件 = pk::add_menu_item(menu_item_事件);

        }

        // =============================================================================================

        void init_菜单_事件(pk::building @building)
        {
            @building_ = @building;
            @taishu_ = pk::get_person(pk::get_taishu_id(building));
            @kunshu_ = pk::get_person(pk::get_kunshu_id(building));
        }

        bool isVisible_菜单_事件()
        {
            if (pk::is_campaign())
                return false;
            if (!setting_ex.mod_set[事件菜单扩展_开关])
                return false;
            if (building_.get_id() >= 据点_末)
                return false;
            if (building_.get_id() != kunshu_.service)
                return false;
            return true;
        }

        string getText_菜单_事件()
        {
            return pk::encode("事件");
        }

        bool isEnabled_菜单_事件()
        {
            if (building_.get_id() >= 据点_末)
                return false;
            return true;
        }

        string getDesc_菜单_事件()
        {
            return pk::encode("决定触发事件.");
        }
        // =============================================================================================

    }

    Main main;
}
