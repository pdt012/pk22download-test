﻿// ## 2023/05/11 # 江东新风 # 新增ai君主强度设定，为其他mod做铺垫 ##

namespace AI_KUNSHU_STR
{
	//const bool 君主强度原版设定 = false;//true就是原版311设定，false目前是血色衣冠设定，后期mod作者可以把false的情况改成其他mod的设定
	class Main
	{
		Main()
		{
			pk::set_func(265, pk::func265_t(callback));
		}

		int callback(int kunshu_id, int value)
		{
			//就是原版311设定，else目前是血色衣冠设定，后期mod作者可以把else的情况改成其他mod的设定
			if (pk::get_scenario().no != 血色剧本)
			{
				switch (kunshu_id)
				{
				case 武将_曹操:
				case 武将_孙策:
				case 武将_曹叡:
				case 武将_曹丕:
				case 武将_刘备:
					value = value + 2000;
					break;
				case 武将_孙坚:
				case 武将_孙权:
					value = value + 1900;
					break;
				case 武将_诸葛亮:
				case 武将_周瑜:
				case 武将_曹彰:
				case 武将_司马懿:
					value = value + 1800;
					break;
				case 武将_董卓:
				case 武将_吕布:
					value = value + 1700;
					break;
				case 武将_张角:
				case 武将_袁绍:
					value = value + 1600;
					break;
				case 武将_马超:
				case 武将_公孙瓒:
					value = value + 1500;
					break;
				case 武将_刘谌:
				case 武将_刘封:
					value = value + 1400;
					break;
				case 武将_孙登:
				case 武将_刘禅:
					value = value + 1300;
					break;
				case 武将_袁术:
					value = value + 1200;
					break;
				case 武将_钟会:
				case 武将_邓艾:
				case 武将_关羽:
				case 武将_陆逊:
					value = value + 1100;
					break;
				case 武将_司马师:
				case 武将_司马昭:
				case 武将_司马炎:
					value = value + 1000;
					break;
				case 武将_刘璋:
				case 武将_刘表:
				case 武将_王朗:
				case 武将_严白虎:
				case 武将_刘繇:
				case 武将_陶谦:
					value = 0;
					break;
				default:
					break;
				}
				if (pk::get_kunshu_id(pk::get_person(武将_诸葛亮)) == kunshu_id) value = value + 1000;
			}
			else if (pk::get_scenario().no == 血色剧本)
			{
				switch (kunshu_id)
				{
				case 血色_刘邦://11
				case 血色_刘秀:
				case 血色_李世民:
				case 血色_朱元璋:
					value = value*2 + 2000;
					break;
				case 血色_嬴政://10
				case 血色_刘彻:
				case 血色_杨坚:
				case 血色_李渊:
				case 血色_朱棣:
					value = value + 2000;
					break;
				case 血色_刘询://9
				case 血色_赵光义:
				case 血色_李隆基:
				case 血色_武曌:
				case 血色_赵匡胤:
					value = value + 1500; 
					break;
				case 血色_曹操://8
				case 血色_石勒:
				case 血色_苻坚:
				case 血色_拓跋焘:
				case 血色_刘裕:
				case 血色_宇文邕:
				case 血色_李存勖:
				case 血色_柴荣:
				case 血色_楚熊侣:
				case 血色_晋重耳:
				case 血色_秦稷:
				case 血色_项籍:
				case 血色_司马昭:
					value + ((value >= 1000) ? 1200 : 800); 
					break;
				case 血色_曹丕://7
				case 血色_司马懿:
				case 血色_司马师:
				case 血色_石虎:
				case 血色_刘聪:
				case 血色_拓跋虔://血色_拓跋宏:
				case 血色_朱温:
				case 血色_李嗣源:
				case 血色_石敬瑭:
				case 血色_刘知远:
				case 血色_郭威:
				case 血色_齐小白:
				case 血色_宇文泰:
				case 血色_项梁:
					value = value = value + ((value >= 1000) ? 1000 : 500); 
					break;
				case 血色_孙权://6
				case 血色_刘备:
				case 血色_慕容垂:
				case 血色_刘曜:
				case 血色_姚兴:
				case 血色_拓跋珪:
				case 血色_萧道成:
				case 血色_萧衍:
				case 血色_高欢:
				case 血色_高澄:
				case 血色_高洋:
				case 血色_秦任好:
				case 血色_秦渠梁:
				case 血色_田因齐:
				case 血色_魏斯:
					value =  value + 1000;
					break;
				case 血色_孙策://5
				case 血色_袁绍:
				case 血色_李雄:
				case 血色_慕容皝:///
				case 血色_姚苌:
				case 血色_苻健:
				case 血色_赫连勃勃:
				case 血色_陈霸先:
				case 血色_王建:
				case 血色_孟知祥:
				case 血色_徐知诰:
				case 血色_徐温:
				case 血色_杨行密:
				case 血色_吴阖闾:
				case 血色_吴夫差:
				case 血色_越勾践:
				case 血色_燕职:
				case 血色_田单:
				case 血色_赵毋恤:
				case 血色_赵雍:
				case 血色_李克用:
					value = value + 500;
					break;
				case 血色_慕容廆://
				case 血色_慕容德://4
				case 血色_姚弋仲:
				case 血色_苻洪:
				case 血色_乞伏乾归:
				case 血色_沮渠蒙逊:
				case 血色_李暠:
				case 血色_高季兴:
				case 血色_马殷:
				case 血色_钱镠:
				case 血色_吕光:
				case 血色_刘渊:
				case 血色_李密:
				case 血色_郑成功:
				case 血色_周行逢:
				case 血色_冼英:
					value = value + 200;
					break;
				case 血色_孙坚://3
				case 血色_姚襄:
				case 血色_苻登:
				case 血色_李特:
				case 血色_李流:
				case 血色_公孙瓒:
				case 血色_吕布:
				case 血色_朱瑾:
					value = value/2;
					break;
				case 血色_侯景://2
				case 血色_安禄山:
				case 血色_史思明:
				case 血色_吴三桂:
					value = value/4;
					break;
				case 血色_杨广://1
				case 血色_秃发褥檀:
				case 血色_李从珂:
				case 血色_王莽:
				case 血色_王世充:
				case 血色_桓玄:
				case 血色_冉闵:
					value = 0;
					break;
				default:
					break;
				}
				//pk::trace(pk::decode(pk::get_name(pk::get_person(kunshu_id))) + ",str:" + value);
			}
	
			return value;
		}
	}

	Main main;
}