﻿// ## 2023/08/21 # 江东新风 # 初次编写 #

namespace Get_Object_Sight
{
	const int 城市视野 = 4;
	const int 据点视野 = 3;
	const int 部队视野 = 2;
	const int 部队特技视野 = 2;
	const int 军事设施视野 = 2;
	const int 内政设施视野 = 1;
	class Main
	{
		Main()
		{
			pk::set_func(72, pk::func72_t(callback));
		}

		int callback(pk::map_object@ object)
		{
            
			int type_id = object.get_type_id();
			pk::building @building = type_id == pk::building::type_id ? object : null;
			pk::facility@ facility;
			pk::unit@ unit = type_id == pk::unit::type_id ? object : null;
			uint range = 0;
			switch (type_id) {
			case pk::city::type_id:
				return 城市视野;
			case pk::gate::type_id:
				return 据点视野;
			case pk::port::type_id:
				return 据点视野;
			case pk::unit::type_id:
				return unit.has_skill(特技_窥探) ? (部队视野 + 部队特技视野) : 部队视野;//特技_窥探
			case pk::building::type_id:
				switch (building.facility) {
				case 设施_城市:
					return 城市视野;
				case 设施_关卡:
					return 据点视野;
				case 设施_港口:
					return 据点视野;
				default:
					@facility = pk::get_facility(building.facility);
					if (facility !is null)
					{
						switch (facility.type) {
						case 设施类型_军事设施:
							return 军事设施视野;
						case 设施类型_内政设施:
							return 内政设施视野;
						}
						break;
					}

				}
				break;
			}
			return 0;
          

		}

	}

	Main main;
}