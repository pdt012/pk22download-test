﻿// ## 2023/08/30 # 黑店小小二 # 武将名称decode##
// ## 2023/05/09 # 加入宝物组为其他时，宝物数量可多个的设定##
// ## 2023/04/13 # 授予没收函数外置##
//1.授予没收时识别多余宝物收归国库，对应忠诚变化和语言变化调整--√
//2.授予没收后宝物替换提示--√
//3.得改set_item_owner函数（加入国库情况）--√，另外探索发现宝物的列表(4ce900)是否要改，授予没收列表（4ce970）--√,全宝物展示（62512f）--√
//4.宝物在国库时在各种界面（全道具一览，授予没收界面）的显示（xx国库？，字数）(4c6b4c)(全道具列表界面的国库显示做不到，子窗口dlg137可以,还有授予没收界面的国库显示)--√×
//5.另外因为只在授予没收时限定了宝物数量，所以还得注意718宝物自动授予的问题，还有就是剧本初始设定的时候宝物的问题，还有并不限制玩家通过作弊把宝物都改到一个人身上（但可以通过宝物特技只生效每个组价值最高宝物的特技来限制）

//6.玉玺和铜雀是否要自动设定成国库，貌似俘虏时有宝物是铜雀和玉玺的处理---铜雀玉玺可以给君主，因为是其他类型宝物，且不可被授予没收--√
//7.身上道具在界面的显示--得等pk3
//8.查看国库宝物的按钮--暂时不做，授予没收界面能看。--√×
//9.掠夺特技抢了道具(已知原版设定是归君主，那直接改成给国库)--√跟俘虏敌方没收道具（4bf39b?），授予的情况默认新换旧，掠夺时和俘虏武将应该默认给国库--√
//10.宝物所属势力之后，那势力灭亡宝物归宿变化也得安排（国库宝物归取代势力）---√
//reset_item 4a9b00函数的更改--此函数不需要改，因为传入的owner必然是人物指针，不会传入国库---√
//需要将授予没收时，owner是国库的情况考虑进去---√
//van改完道具所属，存档，载入，所属没变
namespace Award_Action
{
	class Main
	{
		Main()
		{
			pk::set_func(115, pk::func115_t(callback));
			pk::bind(131, pk::trigger131_t(onForceRemove));
		}

		void onForceRemove(pk::force@ force, bool merge, pk::force@ by)
		{
			if (pk::is_alive(by))
			{
				int force_id = force.get_id();
				int by_id = by.get_id();
				if (!pk::is_valid_person_id(by_id)) return;
				pk::list<pk::item@> list = get_force_item_list(force_id);
				if (list.count > 0)
				{
					for (int i = 0; i < list.count; ++i)
					{
						pk::item@ item = pk::get_item(i);
						pk::set_item_owner(list[i], by_id + 武将_末/*pk::get_kunshu_id(info.target)*/, -1);
					}
				}
			}

		}

		bool callback(const pk::award_cmd_info& in info)
		{
			//pk::trace("award 000");
			if (!pk::award_check_1(info.base) || !pk::award_check_2(info))//
				return false;
			int force_id = info.base.get_force_id();
			pk::force@ force = pk::get_force(force_id);
			if (pk::is_alive(force))
			{
				pk::person@ kunshu = pk::get_person(force.kunshu);
				if (!pk::is_alive(kunshu))
					return false;
				int owner_id = info.item.owner;
				int owner_force_id = owner_id - 武将_末;
				
				if (pk::is_valid_force_id(owner_force_id))
				{
					owner_id = pk::get_force(owner_force_id).kunshu;
				}
				pk::person@ owner = pk::get_person(owner_id);
				pk::person@ speeker = @owner;//先默认是所有者说话，当不是所有者说话时，调整指针
				if (!pk::is_alive(owner))
					return false;
				int value = info.item.value;

				//此处插入检测宝物是否重复，如重复，默认回收旧宝物至国库，赐予新宝物
				//可能赠与宝物的价值低于被替换的宝物，导致被授予武将亏了，此时要在语言上体现
				//其中按功能分为坐骑，兵器，道具
				//坐骑：马
				//兵器：剑，长柄
				//道具：暗器，弓，书籍
				//特殊：玉玺，铜雀（这个不限制也不展示？）
				//其他坐骑，兵器，道具最多各一件

				//获取目标武将同种宝物列表，如数量大于0，则考虑移除现有所有同种宝物，再授予对应宝物
				int group_id = pk::item_type_to_group(info.item.type);
				pk::list<pk::item@> src_list = get_item_list(info.target, group_id);
				bool need_remove = false;
				if (group_id != 宝物组_其他 and src_list.count > 0) need_remove = true;
				int loss_value = 0;//因替换宝物导致的宝物价值损失
				if (need_remove)
				{
					for (int i = 0; i < src_list.count; ++i)
					{
						loss_value += src_list[i].value;
					}
				}
				//pk::trace(pk::format("count:{},need_remove:{}",src_list.count,need_remove));
				//v30 = value;
				if (pk::is_player_controlled(info.base))//is_player_controlled_building
				{
					//玩家操作才有各种对话框和声音
					//初始所需变量
					pk::item@ item = @info.item;
					int msg_id = -1;
					//s11::MsgParam msg0;//v9
					pk::msg_param msg_param1(-1, null, null);//a1
					pk::msg_param msg_param2(-1, null, null);//v33
					//s11::MsgParam msg3;//v31

					int sound = -1;
					pk::person@ target = @info.target;

					//分类讨论1.所有者是君主;2.获得者是君主;
					//3 获得者忠诚大于100 3.1所有者事后忠诚小于100---只需显示所有者怨言;3.2所有者事后忠诚大于100,--只显示获得者感激
					//4 获得者忠诚小于100 4.1所有者事后忠诚大于100--只需显示获得者感激，4.2所有者事后忠诚小于100--只显示所有者怨言
					if (owner.get_id() == kunshu.get_id() or pk::is_valid_force_id(owner.get_id()-势力_末))
					{
						msg_param1.id = 5117;// 謝啦！\n拿到好東西了
						@msg_param1.person[0] = @target;
						@msg_param1.person[1] = @kunshu;
						//s11::fxx::func_599490(msg3, 0, 5117, info->target, kunshu);
						//msg1 = *msg3;//s11::fxx::func_49cd00(&msg1, 0, msg3);
						@speeker = @target;
						if (target.loyalty < 100)
						{
							sound = 6;
							msg_param2.id = 5116;//已將{}賜與{}，\n{}的忠誠度因此而上升了。
							@msg_param2.person[0] = @target;
							@msg_param2.item[0] = @item;

							//s11::fxx::func_4b55a0(msg0, 0, 5116, target, info->item);//已將{}賜與{}，\n{}的忠誠度因此而上升了。
							//msg2 = *msg0;//s11::fxx::func_49cd00(&msg2, 0, msg0);
						}
						pk::trace("1");
						//goto LABEL_31;
					}
					else
					{
						if (target.get_id() == kunshu.get_id())
						{
							msg_param1.id = 8514;//為此忍氣吞聲，\n有辱武人之名……！
							@msg_param1.person[0] = @owner;
							@msg_param1.person[1] = @kunshu;
							@msg_param1.item[0] = @item;
							//s11::fxx::func_4a5be0(msg3, 0, 8514, owner, kunshu, info->item);
							//msg1 = *msg3;//s11::fxx::func_49cd00(&msg1, 0, msg3);

							if (owner.loyalty > 0 && owner.loyalty - value < 100)
							{
								msg_param2.id = 8522;// 已將{}的{}沒收。\n{}的忠誠下降了。
								@msg_param2.person[0] = @target;
								@msg_param2.person[1] = @owner;
								@msg_param2.item[0] = @item;
								sound = 7;
								//@speeker = @owner;
							}
							//pk::trace("2");
							//goto LABEL_31;
						}
						else
						{
							if (target.loyalty >= 100)//获得者忠诚初始大于100，所有者事后忠诚小于100
							{
								if (owner.loyalty > 0 && owner.loyalty - value < 100)//被夺宝武将事后忠诚小于100时的怨言
								{
									msg_param1.id = 8514;//為此忍氣吞聲，\n有辱武人之名……！
									@msg_param1.person[0] = @owner;
									@msg_param1.person[1] = @kunshu;
									@msg_param1.item[0] = @item;

									msg_param2.id = 8520;// 已將{}的{}沒收。\n{}的忠誠下降了。
									@msg_param2.person[0] = @target;
									@msg_param2.person[1] = @owner;
									@msg_param2.item[0] = @item;
									sound = 7;
									//pk::trace("3.1");

								}
								else//被夺宝武将事后忠诚大于100时，展示获得者感激，但不提示忠诚上升
								{
									msg_param1.id = 5117;// 謝啦！\n拿到好東西了
									@msg_param1.person[0] = @target;
									@msg_param1.person[1] = @owner;
									//s11::fxx::func_599490(msg3, 0, 5117, info->target, kunshu);
									//msg1 = *msg3;//s11::fxx::func_49cd00(&msg1, 0, msg3);
									@speeker = @target;


									sound = 6;
									//pk::trace("3.2");
								}

							}
							else
							{
								if (owner.loyalty > 0 && owner.loyalty - value < 100)//获得者忠诚初始小于100，所有者事后忠诚小于100
								{
									msg_param1.id = 8514;//為此忍氣吞聲，\n有辱武人之名……！
									@msg_param1.person[0] = @owner;
									@msg_param1.person[1] = @kunshu;
									@msg_param1.item[0] = @item;

									sound = 7;
									msg_param2.id = 8519;//已將{}的{}授與{}。\n{}的忠誠上昇了。\n{}的忠誠下降了。
									//pk::trace("4.1");
								}
								else//被夺宝武将事后忠诚大于100时，无怨言
								{
									msg_param1.id = 5117;//謝啦！\n拿到好東西了！
									@msg_param1.person[0] = @target;
									@msg_param1.person[1] = @kunshu;
									@msg_param1.item[0] = @item;

									msg_param2.id = 8521;// 已將{}的{}沒收。\n{}的忠誠下降了。

									sound = 6;
									//pk::trace("4.2");
									//goto LABEL_17;
								}
								@msg_param2.person[0] = @target;
								@msg_param2.person[1] = @owner;
								@msg_param2.item[0] = @item;

							}
						}


						//LABEL_17:
						//s11::fxx::func_49cd00(&msg2, 0, *msg3);//sub_49CD00((char*)&v33, v13);
					}

					//LABEL_31:所有判定的归宿，是否显示所有者和获得者的话，及音效
					if (msg_param1.id != -1)
					{
						if (speeker.get_id() == info.target.get_id() and need_remove and (value - loss_value) < 0) pk::message_box(pk::encode("如果可以的话，我还是想要回我原来的宝物..."), info.target);
						else pk::message_box(pk::get_msg(msg_param1), speeker);

						//s11::fxx::func_49baf0 = get_message_char_49baf0(&a1);
						//s11::fxx::func_4f5eb0(s11::fxx::func_49baf0(&msg1), target, 0, 1);//message_box，获得者的话，类似謝啦！\n拿到好東西了！或者所有者的怨言
					}
					if (sound != -1) pk::play_se(sound);
					//s11::fxx::func_4d0f00(&s11::g_data->_9118210, 0, sound, 0, -1.0, 0, 0);//播放音效
					if (msg_param2.id != -1)
					{
						//此处可能会存在两个武将忠诚都下降的情况·顺序
						//已將{}的{}授與{}。\n{}的忠誠上昇了。\n{}的忠誠下降了。
						//已將{}的{}授與{}。\n{}的忠誠下降了。
						//已將{}的{}授與{}。\n{}的忠誠上昇了。
						//已將{}的{}沒收。\n{}的忠誠下降了。
						pk::message_box(pk::get_msg(msg_param2));
						//s11::fxx::func_4f5eb0(s11::fxx::func_49baf0(&msg2), 0, 0, 1);//message_box，对应旁白，类似已將{}賜與{}，\n{}的忠誠度因此而上升了。
						//v22 = get_message_char_49baf0(&v33);
						//message_box_4F5EB0(v22, 0, 0, 1);
					}
					if (need_remove)//授予没收后宝物替换提示
					{
						//因道具重复，xx身上的xx已收归国库
						string desc = "";
						int num = 0;
						for (int i = 0; i < src_list.count; ++i)
						{
							if (num != 0) desc += pk::decode(",");
							num += 1;
							desc += pk::decode(pk::get_name(src_list[i]));

							
						}
						pk::message_box(pk::encode(pk::format("因道具重复，\x1b[2x{}\x1b[0x身上的\x1b[1x{}\x1b[0x已收归势力国库。",pk::decode(pk::get_name(info.target)),desc)));
					}
					//value = v30;
				}

				//label_38玩家和电脑共有的效果

				pk::add_loyalty(info.target, value - loss_value);
				if (owner.get_id() != kunshu.get_id()) pk::add_loyalty(owner, -value);//add_loyalty

				pk::set_item_owner(info.item, info.target.get_id(), -1);// s11::fxx::func_4a12d0(&s11::g_data->_799735c, 0, info->item, info->target->get_id(), -1);//set_item_belong----需加入函数
				if (need_remove)
				{
					for (int i = 0; i < src_list.count; ++i)
					{
						pk::set_item_owner(src_list[i], info.target.get_force_id()+武将_末/*pk::get_kunshu_id(info.target)*/, -1);//得改set_item_owner函数，另外探索发现宝物的列表(4ce900)是否要改
					}
				}
				auto district = pk::get_district(info.base.get_district_id());
				pk::add_ap(district, -10);
				//s11::fxx::func_574430(14, (int)info);//此函数无意义？
				return true;

			}
			return false;
		}

		bool check_owner(int owner_id,int force_id)
		{
			return false;
		}

		pk::list<pk::item@> get_force_item_list(int force_id, int group_id = -1)//获取国库宝物的函数
		{
			pk::list<pk::item@> list;
			if (!pk::is_valid_force_id(force_id)) return list;
			for (int i = 0; i < 扩展宝物_末; ++i)
			{

				pk::item@ item = pk::get_item(i);

				if (!pk::is_alive(item)) continue;
				//pk::trace(pk::encode(pk::get_name(item)) + ": " + group_id + ",item_group:" + pk::item_type_to_group(item.type));
				if (item.owner != (force_id + 武将_末)) continue;
				if (group_id != -1)
				{
					if (pk::item_type_to_group(item.type) != group_id) continue;
				}
				list.add(item);
			}
			return list;
		}

		pk::list<pk::item@> get_item_list(pk::person@ src,int group_id = -1)
		{
			pk::list<pk::item@> list;
			if (!pk::is_alive(src)) return list;
			int src_id = src.get_id();
			for (int i = 0; i < 扩展宝物_末; ++i)
			{
				
				pk::item@ item = pk::get_item(i);
				
				if (!pk::is_alive(item)) continue;
				//pk::trace(pk::encode(pk::get_name(item)) + ": " + group_id + ",item_group:" + pk::item_type_to_group(item.type));
				if (item.owner != src_id) continue;
				if (group_id != -1)
				{
					if (pk::item_type_to_group(item.type) != group_id) continue;
				}
				list.add(item);
			}
			return list;
		}
	}

	Main main;
}