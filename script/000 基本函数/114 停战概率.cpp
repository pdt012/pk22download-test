﻿// ## 2023/05/07 # 江东新风 # 新增停战概率函数 ##

namespace CEASEFIRE_CHANCE
{
	class Main
	{
		Main()
		{
			pk::set_func(114, pk::func114_t(callback));
		}

		//2失败，1辩论，0成功, 返回值2是军师预测成功与否
		pk::int_bool callback(const pk::ceasefire_cmd_info& in info)
		{
			bool accurate = true;
			pk::person@ target_kunshu = pk::get_person(info.target.kunshu);

			if (!pk::is_alive(target_kunshu)) return pk::int_bool(2, accurate);

			int actor_force_id = info.actor.get_force_id();
			int target_force_id = info.target.get_id();
			pk::force@ src_force = pk::get_force(actor_force_id);

			int actor_id = info.actor.get_id();

			int aishou_distance = pk::get_aishou_distance(target_kunshu, info.actor.get_id());
			//int person_politics = int(info.actor.stat[武将能力_政治]);
			int period = info.period;//是旬数，1月等于3旬
			int relations = info.target.relations[actor_force_id];

			int n = 0;
			int g = 0;
			int difficulty = pk::get_scenario().difficulty;

			n += func_5b3f40(info.actor, target_kunshu, 10, -15, 20);//0
			n += int(info.gold / 111);//1000元加9点，10000元加90点，确定为等差
			n -= int((aishou_distance * aishou_distance) / 90);//5b7311 edi
			n += int(sqrt(info.actor.stat[武将能力_政治]) * 9.f);//5b7304

			bool debuf = false;
			//己方是玩家且在外交禁止期间时的停战难度加成
			if (pk::is_alive(src_force) && src_force.is_player() && src_force.diplomacy_ban_timer>0)
			{
				//if (difficulty == 难易度_初级) n = 3 * n / 4;
				//else n = 2 * n / 3;
				debuf = true;
			}
			else//己方是玩家且不在外交禁止期间，或者己方不是玩家
			{
				if (!info.target.is_player())
				{
					//判断除了此势力外是否还有其他可进攻势力，没有的话也参考label16处理
					//force_id_4917a0 = get_force_id_4917a0(force_490fd0);
					//v12 = get_force_id_4917a0(this->target);
					if (pk::is_attackable_force(target_force_id, actor_force_id))
					{
						debuf = true;
						for (int i = 0; i < 非贼势力_末; ++i)
						{
							if (i == target_force_id) continue;
							if (i == actor_force_id) continue;
							pk::force@ dst = pk::get_force(i);
								
							if (!pk::is_alive(dst)) continue;
							if (!pk::is_attackable_force(target_force_id, i)) continue;
							debuf = false;
							break;
						}
					}
				}
			}

			if (debuf)
			{
				if (difficulty == 难易度_初级) n = 3 * n / 4;
				else n = 2 * n / 3;
			}

			if (difficulty == 难易度_上级)
				n = int(n * 0.8f);
			else if (difficulty == 难易度_超级)
				n = int(n * 0.7f);

			g = 4 * period / 3 - relations / 4 + 80;//g越大越难以成功

			if (n * 2 > g) accurate = false;

			if (n > g) return pk::int_bool(0, accurate);

			if (ch::has_skill(info.actor, 特技_论客))
			{
				// 특급 모드 플레이어일 경우에만 20%, 나머지 100% 설전
				//只有特级模式玩家是20%的机会，其余难度的100%会展开舌战
				// if (difficulty != 难易度_超级 or !info.actor.is_player() or pk::randbool(20)) return pk::int_bool(1, accurate);
				if (!info.actor.is_player() or pk::rand_bool(pk::core::skill_constant_value(特技_论客, difficulty))) return pk::int_bool(1, accurate);
			}

	
			if (n <= g - 10)
				return pk::int_bool(2, accurate);;
			return pk::int_bool(1, accurate);;


		}

	}

	Main main;

	//停战用
	int func_5b3f40(pk::person@ actor, pk::person@ target_kunshu, int bad, int good, int best)
	{
		int actor_id = actor.get_id();
		if (pk::is_gikyoudai(target_kunshu, actor_id) or pk::is_fuufu(target_kunshu, actor_id)) return best;
		if (pk::is_like(target_kunshu, actor_id)) return good;
		if (pk::is_dislike(target_kunshu, actor_id)) return bad;
		return 0;
	}

}