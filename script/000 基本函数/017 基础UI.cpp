﻿
// ## 2023/06/06 # 江东新风 # 测试ui ##


//城市信息窗口等的dlg在游戏初始化时就已经init了
//146城市信息 149关卡，港口
//76部队小信息，77建筑小信息 81设施耐久条
namespace UI
{
    //modal: new_dialog, open close delete
    //modeless: set_visible, 摧毁由parent进行
    //428列表横线 435列表竖线
    const array<array<int>> dlg_78_data = {
   {2,-1,70,9,12,54,792,616,0},
{0,-1,320,1,0,0,284,64,0},
{6,2906,-1,-1,40,22,168,24,24},
{9,2906,430,1,284,36,491,32,0},
{0,2906,321,1,775,36,40,28,0},
{9,2906,432,1,5,64,16,600,0},
{9,2906,431,1,803,64,16,600,0},
{9,2906,433,1,257,664,494,64,0},
{0,2906,323,1,5,664,252,60,0},
{5,2913,172,8,22,10,88,44,0},
{5,2913,180,8,110,10,96,44,0},
{0,2906,322,1,751,664,64,48,0},
{9,-1,426,1,22,72,772,73,0},
{9,2917,428,1,4,-4,763,8,0},
{9,2918,428,1,0,75,763,8,0},
{9,2918,435,1,-8,8,8,67,0},
{9,2918,435,1,763,8,8,67,0},
{0,2918,313,1,763,75,8,8,0},
{0,2918,314,1,763,0,8,8,0},
{0,2918,316,1,-8,0,8,8,0},
{0,2918,315,1,-8,75,8,8,0},
{8,2917,238,24,11,41,66,24,0},
{8,2926,238,24,76,0,66,24,0},
{8,2926,238,24,152,0,66,24,0},
{8,2926,238,24,228,0,66,24,0},
{8,2926,238,24,304,0,66,24,0},
{8,2926,238,24,380,0,66,24,0},
{8,2926,238,24,456,0,66,24,0},
{8,2926,238,24,532,0,66,24,0},
{8,2926,238,24,608,0,66,24,0},
{8,2926,238,24,684,0,66,24,0},
{8,2917,238,24,11,9,66,24,0},
{8,2936,238,24,76,0,66,24,0},
{8,2936,238,24,152,0,66,24,0},
{8,2936,238,24,228,0,66,24,0},
{8,2936,238,24,304,0,66,24,0},
{8,2936,238,24,380,0,66,24,0},
{8,2936,238,24,456,0,66,24,0},
{8,2936,238,24,532,0,66,24,0},
{8,2936,238,24,608,0,66,24,0},
{8,2936,238,24,684,0,66,24,0},
{7,-1,-1,0,21,156,774,462,0},
{8,-1,214,24,205,626,84,36,0},
{8,-1,190,24,21,626,84,36,0},
{8,-1,190,24,113,626,84,36,0}
    };

    class BaseParam
    {
        string title;	// 标题
        int min;		// 最少选择
        int max;		// 最多选择
        string desc; // 描述
        array<int16> items();		// 要被选择的列表
        array<int16> selected_items();//默认选中，最多10
        array<int16> select_item_id();//最终选中

        BaseParam()
        {
            reset();
        }

        void reset()
        {
            title = "";	// 标题
            min = -1;		// 最少选择
            max = -1;		// 最多选择
            desc = ""; // 描述
            items.resize(0);		// 要被选择的列表            
            selected_items.resize(0); //默认选中，最多10
            select_item_id.resize(0);//最终选中
        }
    };

    class SpecParam:BaseParam
    {
        SpecParam()
        {
            reset();
        }
        void reset()
        {
            array<int16>a(地名_末, -1);

            items.resize(地名_末);// 要被选择的列表
            selected_items.resize(地名_末); //默认选中，最多10
            
            items = a;		// 要被选择的列表            
            selected_items = a; //默认选中，最多10
        }
    };

    class AutoParam:BaseParam
    {
        AutoParam()
        {
            reset();
        }

        void reset()
        {
            array<int16>a(300, -1);

            items.resize(300);// 要被选择的列表
            selected_items.resize(300); //默认选中，最多10
            
            items = a;		// 要被选择的列表            
            selected_items = a; //默认选中，最多10
            //select_item_id.resize(0);//最终选中
        }
    };


    class Main
    {
        pk::text@ title_text_ = @null;
        pk::button@ ok_button_ = @null;
        pk::button@ esc_button_ = @null;
        array<pk::button@>  btn_(23, null);//前20是上方按钮组，后3是一并选择，一并取消，地图
        array<int>  btnid_(23, -1);
        pk::listview@ lv_ = @null;
        SpecParam specparam_;
        AutoParam autoparam_;

        pk::dialog@ dlg_selector_ = @null;
        //array<int16> sel_spec_id();// = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14 };
        Main()
        {
            pk::bind(210, pk::trigger210_t(dialog_init));;
            //新增的对话框应该在游戏初始化时生成，在鼠标移入时设置文本和显示对话框          
        }

        void dialog_init(pk::dialog@ dlg, int dlg_id)
        {
            //pk::trace(pk::format("dlg id:{},p:0x{:x}", dlg_id, dlg.get_p()));
            //dlg是没有对应的widget_id的
            if (dlg_id == 2)//2开始菜单，游戏左上方114
            {
                hook2(dlg);
            }
        }

        void hook2(pk::dialog@ dialog)
        {
            //init_dlg_from_arr(dialog, dlg_78_data, 2905);
            //create_spec_listview(dialog, pk::point(500,50));
        }

        //作为对话框背景模板，可随意调节大小
        array<pk::button@> dlg_320_ctor(pk::dialog@dialog, pk::size size, string& title,bool title_bar = true)
        {
            pk::assert(size.width >= 301);
            pk::assert(size.height >= 16);

            pk::widget@ w = dialog.create_sprite(70);//(70);//70是半透明
            w.set_pos(12, 54);
            w.set_size(size);
            @ w = dialog.create_sprite(499);//(70);//70是半透明
            w.set_pos(12, 54);
            w.set_size(size);

            @w = dialog.create_image(320);
            if(title_bar) dialog.add_titlebar(w.get_id(), 320);

            pk::text@t = dialog.create_text();
            t.set_pos(40, 22);
            t.set_size(pk::get_text_size(FONT_BIG, title));
            t.set_text(title);
            t.set_text_font(FONT_BIG);

            @w = dialog.create_sprite(430);
            if (title_bar) dialog.add_titlebar(w.get_id(), 430);
            w.set_pos(284, 36);
            w.set_size(size.width - 301, w.get_size().height);

            @w = dialog.create_image(321);
            w.set_pos(size.width - 17, 36);
            if (title_bar) dialog.add_titlebar(w.get_id(), 320);

            @w = dialog.create_sprite(432);
            w.set_pos(5, 64);
            w.set_size(w.get_size().width, size.height - 16);

            @w = dialog.create_sprite(431);
            w.set_pos(size.width + 11, 64);
            w.set_size(w.get_size().width, size.height - 16);

            @w = dialog.create_image(323);
            w.set_pos(5, size.height + 48);

            @w = dialog.create_sprite(433);
            w.set_pos(257, size.height + 48);
            w.set_size(size.width - 298, w.get_size().height);

            @w = dialog.create_image(322);
            w.set_pos(size.width - 41, size.height + 48);

            pk::button@ok_button = dialog.create_button(172);
            ok_button.set_pos(27, size.height + 58);
            ok_button.set_text(pk::encode("决定"));

            pk::button@esc_button = dialog.create_button(180);
            esc_button.set_pos(115, size.height + 58);
            esc_button.set_text(pk::encode("返回"));

            dialog.resize();

            return { ok_button,esc_button };
        }

        void create_spec_listview(pk::dialog@ dialog, pk::point pos, SpecParam@ param)
        {
            int end = -1;
            int iend = pk::min(param.items.length, 地名_末);
            for (int i = 0; i < iend; ++i)
            {

                if (param.items[i] == -1) end = i + 1;
                if (i == iend - 1) end = iend;//都不为-1的情况
            }

            int line_count = end;
            pk::trace("end:" + end);
            int column_count = 8;
            int style = LV_STYLE_VSCROLL | LV_STYLE_HSCROLL | LV_STYLE_CHECKBOX | LV_STYLE_COLUMNHEADER;
            auto lv = dialog.create_listview(column_count, param.min, param.max, style);// bit32.bor(LV_STYLE_VSCROLL, LV_STYLE_HSCROLL, LV_STYLE_CHECKBOX, LV_STYLE_COLUMNHEADER));
            @lv_ = @lv;
            auto lv_sorter = pk::listview_sorter(0, LV_SORT_ASC);
            lv.set_pos(pos);
            lv.set_size(774, 492);
            /* */
                        //auto test = pk::listview_column("col1", 50);
                        //pk::trace(test.text + test.width + test.align + test.default_order);
                        //初始化数据
                        //sel_spec_id = specparam_.items;

                        /*
                        file f;
                        // Open the file in 'read' mode
                        if (f.open("file.txt", "r") >= 0)
                        {
                            // Read the whole file into the string buffer
                            string str = f.readString(f.getSize());
                            f.close();
                        }*/
            pk::detail::funcref func0 = cast<pk::get_column_data_t@>(function(listview, column) {
                if (column == 0)
                    return pk::listview_column(pk::encode("县城"), 50, LV_ALIGN_CENTER, LV_SORT_ASC);
                else if (column == 1)
                    return pk::listview_column(pk::encode("所在"), 50, LV_ALIGN_CENTER, LV_SORT_ASC);
                else if (column == 2)
                    return pk::listview_column(pk::encode("势力"), 75, LV_ALIGN_CENTER, LV_SORT_ASC);
                else if (column == 3)
                    return pk::listview_column(pk::encode("将军"), 75, LV_ALIGN_CENTER, LV_SORT_ASC);
                else if (column == 4)
                    return pk::listview_column(pk::encode("人口"), 70, LV_ALIGN_CENTER, LV_SORT_ASC);
                else if (column == 5)
                    return pk::listview_column(pk::encode("府兵"), 70, LV_ALIGN_CENTER, LV_SORT_ASC);
                else if (column == 6)
                    return pk::listview_column(pk::encode("兵粮"), 70, LV_ALIGN_CENTER, LV_SORT_ASC);
                else if (column == 7)
                    return pk::listview_column(pk::encode("耐久"), 100, LV_ALIGN_CENTER, LV_SORT_ASC);
                return  pk::listview_column("");
            });
            lv.get_column_data(func0);


            pk::detail::funcref func1 = cast<pk::get_item_data_t@>(function(listview, column, row) {
                switch (column)
                {
                case 0:
                    return pk::listview_item(pk::encode(ch::get_spec_name(main.specparam_.items[row])));
                case 1:
                    return pk::listview_item(ch::get_spec_city_name(main.specparam_.items[row]));
                case 2:
                    return pk::listview_item(ch::get_spec_force_name(main.specparam_.items[row]));
                case 3:
                    return pk::listview_item(ch::get_spec_person_name(main.specparam_.items[row]));
                case 4:
                    return pk::listview_item(pk::format("{}", special_ex[main.specparam_.items[row]].population));
                case 5:
                    return pk::listview_item(pk::format("{}", special_ex[main.specparam_.items[row]].troops));
                case 6:
                    return pk::listview_item(pk::format("{}", special_ex[main.specparam_.items[row]].food));
                case 7:
                    return pk::listview_item(pk::format("{}/{}", ch::get_spec_building(main.specparam_.items[row]).hp, pk::get_max_hp(ch::get_spec_building(main.specparam_.items[row]))));
                default:
                    return pk::listview_item("---");
                }
                return pk::listview_item("---");
            });
            lv.get_item_data(func1);


            pk::detail::funcref func2 = cast<pk::get_comparison_value_t@>(function(listview, column, row, asc) {
                if (column == 0)
                    return main.specparam_.items[row];
                if (column == 1)
                    return ch::get_spec_location_id(main.specparam_.items[row]);
                if (column == 2)
                    return ch::get_spec_force_id(main.specparam_.items[row]);
                if (column == 3)
                    return ch::get_spec_person_id(main.specparam_.items[row]);
                if (column == 4)
                    return special_ex[main.specparam_.items[row]].population;
                if (column == 5)
                    return special_ex[main.specparam_.items[row]].troops;
                if (column == 6)
                    return special_ex[main.specparam_.items[row]].food;
                return 0;
            });
            lv.get_comparison_value(func2);

            lv.set_column({ 0, 1, 2,3,4,5,6,7 });//设置可见列
            lv.set_item_count(line_count);
            lv.set_sorter(lv_sorter);// --마지막 정렬 정보를 복구합니다.
            lv.sort();
            /*
            pk::detail::funcref func3 = cast<pk::listview_on_item_clicked_t@>(function(lv_,index,selected) {
                if (selected)
                {
                    main.specparam_.select_item_id[0] = main.specparam_.items[index];
                    pk::trace("index:" + index + "select_item_id" + main.specparam_.select_item_id[0]);
                }
                return;
                //main.dlg_selector_.close(1);
                //main.reset_dlg_value();
            });*/

            pk::detail::funcref func3 = cast<pk::listview_on_item_clicked_t@>(function(a, index, selected) {
                if (selected and main.specparam_.max == 1)//只有单选时触发
                {
                    main.specparam_.select_item_id.insertLast(main.specparam_.items[index]);
                    pk::trace("index:" + index + "select_item_id" + main.specparam_.select_item_id[0]);
                    main.dlg_selector_.close(1);
                }
                return;
            });
            lv.on_item_clicked(func3);/**/
        }

        pk::listview_sorter lv_autoarmy_sorter = pk::listview_sorter(6, LV_SORT_DSC);
        pk::listview@ create_autoarmy_listview(pk::dialog@ dialog, pk::point pos, AutoParam@ param, pk::size size = pk::size(774, 492),bool out_set = false)
        {
            int end = -1;
            int iend = pk::min(param.items.length, 300);
            for (int i = 0; i < iend; ++i)
            {

                if (param.items[i] == -1) end = i + 1;
                if (i == iend - 1) end = iend;//都不为-1的情况
            }

            int line_count = end;
            pk::trace("end:" + end);
            int column_count = 8;
            int style = LV_STYLE_VSCROLL | LV_STYLE_HSCROLL | LV_STYLE_CHECKBOX | LV_STYLE_COLUMNHEADER;
            auto lv = dialog.create_listview(column_count, param.min, param.max, style);// bit32.bor(LV_STYLE_VSCROLL, LV_STYLE_HSCROLL, LV_STYLE_CHECKBOX, LV_STYLE_COLUMNHEADER));
            @lv_ = @lv;
            
            lv.set_pos(pos);
            lv.set_size(size);
            /* */
                        //auto test = pk::listview_column("col1", 50);
                        //pk::trace(test.text + test.width + test.align + test.default_order);
                        //初始化数据
                        //sel_spec_id = autoparam_.items;

                        /*
                        file f;
                        // Open the file in 'read' mode
                        if (f.open("file.txt", "r") >= 0)
                        {
                            // Read the whole file into the string buffer
                            string str = f.readString(f.getSize());
                            f.close();
                        }*/
            pk::detail::funcref func0 = cast<pk::get_column_data_t@>(function(listview, column) {
                if (column == 0)
                    return pk::listview_column(pk::encode("顺序"), 50, LV_ALIGN_CENTER, LV_SORT_ASC);
                else if (column == 1)
                    return pk::listview_column(pk::encode("主将"), 75, LV_ALIGN_CENTER, LV_SORT_ASC);
                else if (column == 2)
                    return pk::listview_column(pk::encode("副将1"), 75, LV_ALIGN_CENTER, LV_SORT_ASC);
                else if (column == 3)
                    return pk::listview_column(pk::encode("副将2"), 75, LV_ALIGN_CENTER, LV_SORT_ASC);
                else if (column == 4)
                    return pk::listview_column(pk::encode("陆上"), 70, LV_ALIGN_CENTER, LV_SORT_ASC);
                else if (column == 5)
                    return pk::listview_column(pk::encode("水上"), 70, LV_ALIGN_CENTER, LV_SORT_ASC);
                else if (column == 6)
                    return pk::listview_column(pk::encode("频次"), 70, LV_ALIGN_CENTER, LV_SORT_ASC);
                return  pk::listview_column("");
            });
            lv.get_column_data(func0);

            if (out_set)
            {

            }
            else
            {
                pk::detail::funcref func1 = cast<pk::get_item_data_t@>(function(listview, column, row) {
                    switch (column)
                    {
                    case 0:
                        return pk::listview_item(pk::encode(pk::format("{}", row)));
                    case 1:
                        return pk::listview_item(ch::get_person_name(pk::get_person(autoarmy_ex[main.autoparam_.items[row]].member[0])));
                    case 2:
                        return pk::listview_item(ch::get_person_name(pk::get_person(autoarmy_ex[main.autoparam_.items[row]].member[1])));
                    case 3:
                        return pk::listview_item(ch::get_person_name(pk::get_person(autoarmy_ex[main.autoparam_.items[row]].member[2])));
                    case 4:
                        return pk::listview_item(ch::get_weapon_name(autoarmy_ex[main.autoparam_.items[row]].ground_weapon));
                    case 5:
                        return pk::listview_item(ch::get_weapon_name(autoarmy_ex[main.autoparam_.items[row]].sea_weapon));
                    case 6:
                        return pk::listview_item(pk::encode(pk::format("{}", autoarmy_ex[main.autoparam_.items[row]].freq)));
                    default:
                        return pk::listview_item("---");
                    }
                    return pk::listview_item("---");
                });
                lv.get_item_data(func1);


                pk::detail::funcref func2 = cast<pk::get_comparison_value_t@>(function(listview, column, row, asc) {
                    if (column == 0)
                        return row;
                    if (column == 1)
                        return autoarmy_ex[main.autoparam_.items[row]].member[0];
                    if (column == 2)
                        return autoarmy_ex[main.autoparam_.items[row]].member[1];
                    if (column == 3)
                        return autoarmy_ex[main.autoparam_.items[row]].member[2];
                    if (column == 4)
                        return autoarmy_ex[main.autoparam_.items[row]].ground_weapon;
                    if (column == 5)
                        return autoarmy_ex[main.autoparam_.items[row]].sea_weapon;
                    if (column == 6)
                        return autoarmy_ex[main.autoparam_.items[row]].freq;
                    return 0;
                });
                lv.get_comparison_value(func2);

            }
  
            pk::detail::funcref func3 = cast<pk::listview_on_sort_t@>(function(listview, column, order) {

                main.lv_autoarmy_sorter = listview.get_sorter();//获取最后的排列顺序（那用处是啥？）마지막 정렬 정보를 얻습니다.
            });


            lv.set_column({ 0, 1, 2,3,4,5,6 });//设置可见列
            lv.set_item_count(line_count);
            lv.set_sorter(lv_autoarmy_sorter);// --마지막 정렬 정보를 복구합니다.
            lv.sort();
            /*
            pk::detail::funcref func3 = cast<pk::listview_on_item_clicked_t@>(function(lv_,index,selected) {
                if (selected)
                {
                    main.autoparam_.select_item_id[0] = main.autoparam_.items[index];
                    pk::trace("index:" + index + "select_item_id" + main.autoparam_.select_item_id[0]);
                }
                return;
                //main.dlg_selector_.close(1);
                //main.reset_dlg_value();
            });*/
            /*
            pk::detail::funcref func3 = cast<pk::listview_on_item_clicked_t@>(function(a, index, selected) {
                if (selected and main.autoparam_.max == 1)//只有单选时触发
                {
                    if (main.dlg_selector_ !is null)
                    {
                        main.autoparam_.select_item_id.insertLast(main.autoparam_.items[index]);
                        pk::trace("index:" + index + "select_item_id" + main.autoparam_.select_item_id[0]);
                        main.dlg_selector_.close(1);
                    }

                }
                return;
            });
            lv.on_item_clicked(func3);*/

            return lv;
        }

        int get_comparison_value(int column, int row)
        {
            return -1;
        }

        void sort_autoarmy_arr()
        {
            //autoarmy_ex.sort(function(a, b) { return autoarmy_ex[a].freq < autoarmy_ex[b].freq; });
        }

        //此函数用于设置通用基础属性及储存组件信息
        pk::dialog@ init_dlg_from_arr(pk::dialog@ new_dialog, array<array<int>>arr, int start_id, bool one_line = true)
        {


            //int start_id = 5595;
            int id = 0;
            int text_index = 0;
            int button_index = 0;
            for (int i = 0; i < int(arr.length); ++i)
            {
                //if (i == (5526 - start_id) or i == (5527 - start_id) or i == (5528 - start_id)) continue;
                pk::widget@ widget = new_dialog.create_widget(arr[i][0], arr[i][2]);
                int parent = arr[i][1];
                int parent2 = -1;
                if (parent != -1) parent2 = arr[parent - start_id][1];

                int x = arr[i][4] + ((parent == -1) ? 0 : ((parent2 == -1) ? arr[parent - start_id][4] : arr[parent2 - start_id][4] + arr[parent - start_id][4]));
                int y = arr[i][5] + ((parent == -1) ? 0 : ((parent2 == -1) ? arr[parent - start_id][5] : arr[parent2 - start_id][5] + arr[parent - start_id][5]));
                int width = arr[i][6];
                int height = arr[i][7];

                if (one_line)
                {
                    if (i == 16 or i == 15)
                    {
                        height -= 30;
                    }
                    if (i == 14 or i == 17 or i == 20)
                    {
                        y -= 30;
                    }
                }



                if (widget !is null)
                {
                    //pk::trace(pk::format("{},{},{},{},0x{:x}", x, y, width, height, widget.get_p()));
                    widget.set_pos(x, y);
                    widget.set_size(width, height);
                }

                if (widget !is null)
                {
                    int type_id = arr[i][0];
                    if (type_id == 5)
                    {
                        if ((i + start_id) == 2914)
                        {
                            @ok_button_ = @widget;
                        }
                        if ((i + start_id) == 2915)
                        {
                            @esc_button_ = @widget;
                        }
                    }
                    else if (type_id == 8)
                    {
                        @btn_[button_index] = @widget;
                        btnid_[button_index] = widget.get_id();
                        button_index += 1;
                    }
                    else if (type_id == 6)
                    {
                        @title_text_ = @widget;
                    }
                }

            }


            return new_dialog;
        }

        pk::dialog@ create_listview_dlg()
        {
            pk::dialog@ dialog = pk::new_dialog(false);//pk::get_frame().create_dialog();//

            @dlg_selector_ = @dialog;
            pk::size dialog_size = pk::size(900, 750);
            dialog.set_pos(pk::get_resolution().width / 2 - dialog_size.width / 2, pk::get_resolution().height / 2 - dialog_size.height / 2);
            dialog.set_size(dialog_size);
            init_dlg_from_arr(dialog, dlg_78_data, 2905);
            return dialog;
        }

        void set_basic_dlg(BaseParam@ param)
        {
            bool is_view = false;
            if (param.min < 1 and param.max < 1)
            {
                is_view = true;
            }

            
            for (uint i = 0; i < btn_.length; ++i)
            {
                if (i == 10) continue;
                btn_[i].set_visible(false);
            }
            title_text_.set_text(param.title);
            title_text_.set_text_font(FONT_BIG);
            ok_button_.set_text(pk::encode("决定"));
            esc_button_.set_text(pk::encode("返回"));

            //不需要一并选择时，隐藏对应按钮，拉长listview
            if (is_view or (!btn_[20].is_visible() and !btn_[21].is_visible() and !btn_[22].is_visible()))
            {
                pk::size size = lv_.get_size();
                lv_.set_size(size.width, size.height + 44);
            }
            
            if (is_view)
            {
                ok_button_.set_visible(false);
                esc_button_.set_pos(ok_button_.get_pos());
            }

        }

        ////////////////////////////////////////////////////////////////////////autoarmy_dlg相关/////////////////////////////////////////////////////////////////////////////////////////
        void set_autoarmy_dlg(AutoParam@ param)
        {
            set_basic_dlg(param);

            bool is_view = false;
            if (param.min < 1 and param.max < 1)
            {
                is_view = true;
            }

            btn_[10].set_text(pk::encode("组队"));

            //设置返回键功能
            pk::detail::funcref func0 = cast<pk::button_on_released_t@>(function(button) {
                main.dlg_selector_.close(1);
            });
            esc_button_.on_button_released(func0);

            if (!is_view)
            {
                //设置决定键功能
                pk::detail::funcref func1 = cast<pk::button_on_released_t@>(function(button) {
                    auto arr = main.lv_.get_selected_items();
                    int end = pk::min(地名_末, arr.length);
                    for (int i = 0; i < end; ++i)
                    {
                        int16 select_item_id = main.autoparam_.items[int16(arr[i])];
                        main.autoparam_.select_item_id.insertLast(select_item_id);
                    }

                    if (end > 0) pk::trace("select_item_id" + main.autoparam_.select_item_id[0] + ":" + ch::get_spec_name(main.autoparam_.select_item_id[0]));

                    main.dlg_selector_.close(1);
                });
                ok_button_.on_button_released(func1);

            }

        }

        array<int16> autoarmy_selector_t(string title, string desc, array<int16> items, int min, int max, array<int16> selected_items = array<int16>())
        {
            if (items.length != 0)
            {
                AutoParam param;
                param.reset();
                param.title = title;
                param.desc = desc;
                param.items = items;
                param.selected_items = selected_items;
                param.max = max;
                param.min = min;
                autoparam_ = param;

                pk::dialog@ dlg = autoarmy_selector_ctor(param);
                dlg.open();
                dlg.delete();
            }
            //auto arr = 

            main.reset_dlg_value();

            return main.autoparam_.select_item_id;
        }

        pk::dialog@ autoarmy_selector_ctor(AutoParam@ param)
        {
            //array<int> arr(地名_末, -1);
            pk::dialog@ dialog = create_listview_dlg();
            create_autoarmy_listview(dialog, pk::point(21, 126), param);

            set_autoarmy_dlg(param);

            return dialog;
        }

        ////////////////////////////////////////////////////////////////////////spec_dlg相关/////////////////////////////////////////////////////////////////////////////////////////
        void set_spec_dlg(SpecParam@ param)
        {
            set_basic_dlg(param);

            bool is_view = false;
            if (param.min < 1 and param.max < 1)
            {
                is_view = true;
            }

            btn_[10].set_text(pk::encode("县城"));

            //设置返回键功能
            pk::detail::funcref func0 = cast<pk::button_on_released_t@>(function(button) {
                main.dlg_selector_.close(1);
            });
            esc_button_.on_button_released(func0);

            if (!is_view)
            {
                //设置决定键功能
                pk::detail::funcref func1 = cast<pk::button_on_released_t@>(function(button) {
                    auto arr = main.lv_.get_selected_items();
                    int end = pk::min(地名_末, arr.length);
                    for (int i = 0; i < end; ++i)
                    {
                        int16 spec_id = main.specparam_.items[int16(arr[i])];
                        main.specparam_.select_item_id.insertLast(spec_id);
                    }

                    if (end > 0) pk::trace("select_item_id" + main.specparam_.select_item_id[0] + ":" + ch::get_spec_name(main.specparam_.select_item_id[0]));

                    main.dlg_selector_.close(1);
                });
                ok_button_.on_button_released(func1);

            }

        }

        array<int16> spec_selector_t(string title, string desc, array<int16> items, int min, int max, array<int16> selected_items = array<int16>())
        {
            if (items.length != 0)
            {
                SpecParam param;
                param.reset();
                param.title = title;
                param.desc = desc;
                param.items = items;
                param.selected_items = selected_items;
                param.max = max;
                param.min = min;
                specparam_ = param;

                pk::dialog@ dlg = spec_selector_ctor(param);
                dlg.open();
                dlg.delete();
            }
            //auto arr = 
         
            main.reset_dlg_value();

            return main.specparam_.select_item_id;
        }

        pk::dialog@ spec_selector_ctor(SpecParam@ param)
        {
            //array<int> arr(地名_末, -1);
            pk::dialog@ dialog = create_listview_dlg();
            create_spec_listview(dialog, pk::point(21, 126), param);

            set_spec_dlg(param);

            return dialog;
        }

        void reset_dlg_value()
        {
            pk::text@ title_text_ = @null;
            pk::button@ ok_button_ = @null;
            pk::button@ esc_button_ = @null;
            array<pk::button@>  a(23, null);//前20是上方按钮组，后3是一并选择，一并取消，地图
            array<int>  b(23, -1);
            btn_ = a;//前20是上方按钮组，后3是一并选择，一并取消，地图
            btnid_ = b;

            pk::listview@ lv_ = @null;

            pk::dialog@ dlg_selector_ = @null;

            //sel_spec_id.resize(0);

            //specparam_.reset();//在初始化时重置
        }
    }

    Main main;

    class autoarmylist
    {

    };

    //用于外部调用
    array<int16> spec_selector(string title, string desc, array<int16> items, int min, int max, array<int16> selected_items = array<int16>())
    {
        return main.spec_selector_t(title, desc, items, min, max, selected_items);
    }

    //用于外部调用
    array<int16> autoarmy_selector(string title, string desc, array<int16> items, int min, int max, array<int16> selected_items = array<int16>())
    {
        return main.autoarmy_selector_t(title, desc, items, min, max, selected_items);
    }

    pk::listview@ autoarmy_listview(pk::dialog@ dialog, pk::point pos,pk::size size, string title, string desc, array<int16> items, int min, int max, array<int16> selected_items = array<int16>(),bool out_set = false)
    {
        AutoParam param;
        param.reset();
        param.title = title;
        param.desc = desc;
        param.items = items;
        param.selected_items = selected_items;
        param.max = max;
        param.min = min;
        main.autoparam_ = param;

       return main.create_autoarmy_listview(dialog, pos, param,size);

    }

    array<pk::button@> create_dlg_320(pk::dialog@dialog, pk::size size, string& title)
    {
        return main.dlg_320_ctor(dialog,size,title);
    }
}