﻿// ## 2023/05/06 # 江东新风 # 拆分连环特技 #

namespace 战法攻击范围
{
	class Main
	{
		Main()
		{
			pk::set_func(70, pk::func70_t(callback));
		}

		int callback(pk::tactics_info& tactics_info, pk::unit@ unit, const pk::point& in pos)
		{

            if (unit.type == 部队类型_运输)
                return 0;
            int force_id = unit.get_force_id();
            pk::force@ force = pk::get_force(force_id);
            int unit_weapon = pk::get_weapon_id(unit, pos);

            pk::equipment@ equipment = pk::get_equipment(unit_weapon);
            int avaliable_tactics_count = 0;
            //posa = &info->range[0].max_range;
            for (int i = 0; i < 战法_末; ++i)
            {
                if (equipment.tactics[i])
                {
                    pk::tactics@ tactics = pk::get_tactics(i);
                    int tekisei = tactics.tekisei;
                    if (tekisei <= pk::get_tekisei(unit, unit_weapon))// 检测战法所需适性是否达标
                    {
                        tactics_info.energy_available[i] = true;// a1的前4个字节的每1位储存战法适性是否满足，5-8字节每一位储存战法所需气力是否达标，
                        int energy_cost = tactics.energy_cost;
                        if (unit.get_energy() >= energy_cost)// 检测战法所需气力是否达标
                        {
                            tactics_info.tekisei_available[i] = true;
                            int min_range = tactics.min_range;
                            int max_range = tactics.max_range;
                            int range_buf = 0;
                            //不要尝试给近战战法加射程，会跳出
                            switch (i)
                            {
                            case 战法_火矢:
                            case 战法_贯矢:
                            case 战法_乱射:
                                range_buf += pk::has_tech(unit, 技巧_强弩) ? 1 : 0;
                                range_buf += unit.has_skill(特技_神臂) ? 1 : 0;
                                break;
                            case 战法_攻城火矢:
                            case 战法_攻城投石:
                                range_buf = unit.has_skill(特技_射程) ? 1 : 0;
                                break;
                            default:
                                break;
                            }
                           max_range += range_buf;
                            //pk::trace("id:" + i +",min_range" + min_range + ",max_range" + max_range + " tactics_info.energy_available[i]" + tactics_info.energy_available[i] + "tactics_info.tekisei_available[i]" + tactics_info.tekisei_available[i]);
                            tactics_info.min_range[i] = min_range;
                            tactics_info.max_range[i] = max_range;

                            avaliable_tactics_count += 1;
                        }
                    }
                }
            }


            return avaliable_tactics_count;

		}

	}

	Main main;
}