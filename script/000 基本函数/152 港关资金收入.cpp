﻿// ## 2023/5/03 # 铃 # 修改了小城市的算法,现在小城市只有市场收入才会继承大城市,自己依然有人丁税收的收入. ##
// ## 2023/1/23 # 铃 # 根据小城市的收入特征,再次优化算法 ##
// ## 2022/12/29 # 铃 # 再次优化人口系统,同时优化资金算法,使得收入更平滑 ##
// ## 2022/11/2 # 铃 # 修改了资金收入算法,以适配新的人口系统 ##
// ## 2022/08/08 # 铃 # 把城市收入赋予结构体 ##
// ## 2022/02/17 # 江东新风 # 人口设定调整 ##
// ## 2021/10/01 # 江东新风 # namespace的韩文改成英文 ##
// ## 2021/02/17 # 江东新风 # 城市数惩罚设置上限，调整系数 ##
// ## 2021/02/12 # 氕氘氚 # 解决港关非所属时收入的问题##
// ## 2020/12/12 # 江东新风 # 修复trace参数报错 ##
// ## 2020/10/23 #江东新风#同步马术书大神的更新，添加城市数量惩罚 ##
// ## 2020/09/28 # 氕氘氚 # 添加固定收入 ##
/*
@수정자: 기마책사
@Update: '18.12.22  / 변경내용: 병기차감 스크립트 분리
@Update: '19.2.23   / 변경내용: 게임 내에서 x0.2 자동적용되어서 스크립트에서 제외
*/

namespace GATE_REVENUE
{
	const bool 调试模式 = false;
	class Main
	{
		Main()
		{
			pk::set_func(152, pk::func152_t(callback));
			pk::bind(100, pk::trigger100_t(onTitleScreen));
		}

		void onTitleScreen()
		{
			pk::set_independent_income(true); // 开启港关独立收入（会同时让无势力港关有收入，需在后方排除）
		}

		int callback(pk::building @building, int city_revenue)
		{

			if (!pk::is_alive(building))
				return 0;
			if (!pk::is_valid_normal_force_id(building.get_force_id()))
				return 0;

			int n = 0;
			int base_id = building.get_id();
			BuildingInfo @base_p = @building_ex[base_id];

			n = int(city_revenue * 0.2f * 港关金收入倍率 / 100.f);

			if (开启人口系统)
			{
				int population = 0;
				BaseInfo @base_t = @base_ex[base_id];

				int tax_rate = 10; // 普通税率为每万人收10金
				population = base_t.population;
				n +=  int(population / 10000.f * tax_rate);
			}
			else
				n = n + 500;

			// 游戏难度修正
			n = inter::incomes_difficulty_impact2(n, building.is_player());
			// 总倍率
			if (building.is_player())
				n = int(n * 玩家金收入倍率 / 100.f);
			else
				n = int(n * 电脑金收入倍率 / 100.f);
			/**/
			n += base_ex[building.get_id()].revenue_bonus;
			
			// if (金粮收入_玩家_城市数_惩罚 and building.is_player() and !pk::is_campaign())
			// {
			//	pk::force @force = pk::get_force(building.get_force_id());
			//	float force_city_count = float(pk::get_city_list(force).count);
			//	n = int(n * (1.f - pk::min(0.3f, (force_city_count - 3) * 0.015f)));
			// }
			if (调试模式)
				pk::trace("对应城市收入2：" + city_revenue + "n:" + n);
			n = pk::clamp(n, int(0.05f * city_revenue), int(1.0f * city_revenue)); // 港关最高收入为对应城市收入的100%
			// pk::trace("资金"+n);
			// ch::debug(2, pk::format("n t ={}", n));
			// if (pk::get_day() == 21)			base_p.building_revenue = n - base_p.weapon_expense  - base_p.troops_expense  ;

			base_p.building_revenue = n;

			return n;
		}

		//---------------------------------------------------------------------------------------

	}

	Main main;
}