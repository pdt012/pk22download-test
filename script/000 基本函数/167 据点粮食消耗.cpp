﻿// ## 2023/04/26 # 江东新风 # 取消特技改特效 ##
// ## 2023/04/20 # 江东新风 # 取消坚城耗粮减少，如有需要应组合特技 ##
// ## 2022/09/28 # 铃 # 大幅增加了城市被包围时粮食的消耗 ##
// ## 2022/08/14 # 铃 # 适配了新的城防体系,重做了战争时期资源的消耗情况 ##
// ## 2022/02/14 # 江东新风 # 部分常量中文化 ##
// ## 2021/10/01 # 江东新风 # namespace的韩文改成英文 ##
// ## 2020/12/12 # 江东新风 # 修复trace参数报错 ##
// ## 2020/10/29 #江东新风# 同步马术书大神的更新 ##
// ## 2020/08/16 #江东新风#has_skill函数替換##
// ## 2020/07/26 ##
namespace BUILDING_FOOD_USE
{
	//---------------------------------------------------------------------------

	const bool 兵粮焚烧 = true;
	const int 基础耗粮 = 25;  //1000人一旬的基础耗粮
	//const int 屯田耗粮倍率 = 50;

	//---------------------------------------------------------------------------

	class Main
	{
		Main()
		{
			pk::set_func(167, pk::func167_t(callback));
		}

		int callback(pk::building@ building)
		{
			if (!pk::is_alive(building) or !pk::is_valid_force_id(building.get_force_id()))
				return 0;

			float n = 0;

			if (兵粮焚烧 and building.is_on_fire())
			{
				pk::person@ taishu = pk::get_person(pk::get_taishu_id(building));
				int pol = taishu !is null ? taishu.stat[武将能力_政治] : 0;
				n = n + (6.f - (pol / 20.f)) * pk::get_food(building) / 100.f;
			}

			//当附近有敌人时，增加每万人500点粮草消耗.
			if (pk::enemies_around(building))
				n = n + pk::get_troops(building) * 基础耗粮 / 1000 + pk::get_troops(building) / 10000 * 500;    
			else
				n = n + pk::get_troops(building) * 基础耗粮 / 1000;

			if (n <= 0 and pk::get_troops(building) > 0)
				return 1;

			// 둔전 특기 보유시 병량 감소량 반감 拥有屯田特技时兵量减少减半
			if (ch::has_skill(building, 特技_屯田))
				return int(n * pk::core::skill_constant_value(特技_屯田) / 100.f);

			return int(n);
		}
	}

	Main main;
}