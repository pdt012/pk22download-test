﻿// ## 2023/03/01 # 铃 # 新增对敌军科技的判断,新增对恶劣地形的判断,

namespace UNIT_MOVE_INFO
{
	class Main
	{
		Main()
		{
			pk::set_func(68, pk::func68_t(callback));
		}

		bool callback(pk::unit @unit, pk::move_info& option)
		{
			if (unit.has_skill(特技_飞将) or unit.has_skill(特技_遁走)) option.use_ground_zoc = false;

			if (unit.has_skill(特技_推进))option.use_sea_zoc = false;

			if (unit.has_tech(技巧_难所行军) || unit.has_skill(特技_踏破))
				option.ignore_cliffroad_damage = true;

			if (unit.has_skill(特技_解毒))
				option.ignore_poision_damage = true;

			return true;
		}
	}

	Main main;
}