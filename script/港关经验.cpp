﻿// ## 2022/03/27 # 江东新风 # 降低经验获得频率 ##
// ## 2020/12/24 # 江东新风 # 修复trace参数报错 ##
// ## 2020/11/10 # messi # 搬运naver武将在港关及其范围内防守会获得经验 ##
namespace 港关经验
{
    //----------------------------------------------------------------
    const bool 开启港关经验 = true;  // 항구 거점에 대해서 효과 적용 여부

    const int exp_0 = 2; //통솔
    const int exp_1 = 2; //무력

    //-----------------------------------------------------------------

    class Main
    {
        Main()
        {
            pk::bind(108, pk::trigger108_t(onNewMonth));
        }

        void onNewMonth()     // 턴별
        {
            // 게임시작시에는 발동안함
            if (pk::get_elapsed_days() <= 0) return;

            func_building_effect();

        }


        //--------------------------------------------------------------

        void func_building_effect()
        {
            if (开启港关经验)
            {
                auto mibun_flags = pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般);
                for (int i = 0; i < 非贼武将_末; ++i)
                {
                    auto person = pk::get_person(i);

                    if (person !is null and pk::check_mibun(person, mibun_flags) and (person.service >= 城市_末 and person.service < 据点_港口末) and !pk::is_unitize(person) and !pk::is_absent(person))
                    {

                        pk::add_stat_exp(person, 武将能力_统率, exp_0);
                        pk::add_stat_exp(person, 武将能力_武力, exp_1);
                        /*此函数判断敌军是否包围增加不少运算量，不要了
                        if (pk::enemies_around(building))
                        {
                            pk::add_stat_exp(person, 武将能力_统率, 2);
                            pk::add_stat_exp(person, 武将能力_武力, 2);
                        }*/
                    }
                }
            }
        }

    }

    Main main;
}