﻿// ## 2023/3/19 #铃# 結合新的內政系統设计了AI安置流民 ##

namespace AI安置流民
{

	// ================ CUSTOMIZE ================

	const int AI安置流民_发动时期 = 1;
	const int AI安置流民_发动概率_城市 = 0;

	const int pop_gain = 10000;
	const int gold_num = 3000;

	// ===========================================

	class Main
	{

		Main()
		{
			pk::bind(107, pk::trigger107_t(callback));
		}

		void callback()
		{

			for (int i = 0; i < 据点_末; i++)
			{
				int buildingid = i;
				pk::building @building = pk::get_building(i);

				if (!building.is_player() and pk::get_taishu_id(building) >= 0)
				{

					if (AI安置流民_发动时期 == 0)
						Yumin(buildingid);
					else if (AI安置流民_发动时期 == 1 and (pk::get_day() == 1))
						Yumin(buildingid);
					else if (AI安置流民_发动时期 == 2 and (pk::get_day() == 1) and pk::is_first_month_of_quarter(pk::get_month()))
						Yumin(buildingid);
					else if (AI安置流民_发动时期 == 3 and (pk::get_day() == 1) and pk::get_month() == 1)
						Yumin(buildingid);
				}
			}
		}

		void Yumin(int buildingid)
		{

			int force_id;
			int pop_gain_final;


			pk::building @building = pk::get_building(buildingid);
			BaseInfo @base_t = @base_ex[buildingid];

			force_id = building.get_force_id();
			pk::force @force0 = pk::get_force(force_id);

				if (buildingid < 42)
					pop_gain_final = pop_gain;
				else
					pop_gain_final = int(pop_gain * 0.5);

			if (ch::has_skill(building, 特技_安民))
			{
				pop_gain_final = int(pop_gain_final * 1.5f); // 安民
			}

			else if (ch::has_skill(building, 特技_安民) or ch::has_skill(building, 特技_祈愿))
			{
				pop_gain_final = int(pop_gain_final * 1.2f); // 安民祈愿
			}

			pop_gain_final += pk::rand(2000); //增加随机数

			// 钱越多触发概率越高,小城減半.兵役人口不足時,大幅增加概率
			// 安置的同時增加30%兵役人口.
			if (pk::rand_bool(AI安置流民_发动概率_城市 + int(log(pk::get_gold(building)) * 0.3) + (base_t.mil_pop_all < 30000 ? 3 : 0)) and pk::get_gold(building) > 15000 and !pk::enemies_around(building))
			{

				pk::add_gold(building, -gold_num, true);
				ch::add_population(building.get_id(), pop_gain_final);
				ch::add_mil_pop_all(building.get_id(), int(pop_gain_final * 0.3));
				pk::history_log(building.pos, pk::get_force(building.get_force_id()).color, pk::encode(pk::format("\x1b[1x{}安置了流民：\x1b[1x{}人", pk::decode(pk::get_name(building)), int(pop_gain_final), 0)));
			}

		} // void Yumin()

		int func_附近_敌城市数(pk::city @city, int 距离)
		{
			int enemy_city_count = 0;

			array<pk::city @> cities = pk::list_to_array(pk::get_city_list());
			for (int i = 0; i < int(cities.length); i++)
			{
				pk::city @neighbor_city = cities[i];

				int distance = pk::get_city_distance(city.get_id(), neighbor_city.get_id());

				if (distance > 距离)
					continue;
				if (!pk::is_enemy(city, neighbor_city))
					continue;

				enemy_city_count++;
			}

			return enemy_city_count;
		}

	} // class

	Main main;
}