﻿// ## 2023/2/1 #铃# 新建AI加固城防 ##

namespace 加固城防_AI
{

	// ================ CUSTOMIZE ================

	const int 加固城防_发动时期 = 3;
	const int 加固城防_发动概率_城市 = 5;

	const int def_dec = 500;
	const int gold_num = 5000;

	// ===========================================

	class Main
	{

		Main()
		{
			pk::bind(107, pk::trigger107_t(callback));
		}

		void callback()
		{

			pk::array<pk::force @> force_list = pk::list_to_array(pk::get_force_list());

			for (int i = 0; i < int(force_list.length); i++)
			{

				pk::force @force = force_list[i];

				if (加固城防_发动时期 == 0)
					build(force);
				else if (加固城防_发动时期 == 1 and (pk::get_day() == 1))
					build(force);
				else if (加固城防_发动时期 == 2 and (pk::get_day() == 1) and pk::is_first_month_of_quarter(pk::get_month()))
					build(force);
				else if (加固城防_发动时期 == 3 and (pk::get_day() == 1) and pk::get_month() == 1)
					build(force);
			}
		}

		void build(pk::force @force)
		{

			int building_id;
			int force_id;

			pk::array<pk::city @> city_list = pk::list_to_array(pk::get_city_list(force));

			for (int i = 0; i < int(city_list.length); i++)
			{

				pk::city @city_ = city_list[i];
				pk::building @building_ = pk::city_to_building(city_);
				building_id = building_.get_id();
				force_id = building_.get_force_id();
				pk::force @force0 = pk::get_force(force_id);
				BuildingInfo @base_p = @building_ex[building_id];

				// 钱越多触发概率越高
				if (pk::rand_bool(加固城防_发动概率_城市 + int(sqrt(pk::get_gold(building_)) * 0.02)))
				{
					if (building_.is_player())
						return;

					if (pk::get_gold(building_) > 15000 and func_附近_敌城市数(city_, 2)>0 and base_p.city_defense < 3000)
					{

						pk::add_gold(city_, -gold_num, true);
						base_p.city_defense += def_dec;
						pk::history_log(building_.pos, pk::get_force(building_.get_force_id()).color, pk::encode(pk::format("\x1b[1x{}实施了加固城市,增加了\x1b[1x{}点城防", pk::decode(pk::get_name(building_)), int(def_dec), 0)));

						// 用额外城防去修复防御
						if (int(building_.hp) - int(pk::get_max_hp(building_)) < 0)
						{
							int 可修复防御 = pk::get_max_hp(building_) - building_.hp;
							int 修复防御 = pk::min(可修复防御, base_p.city_defense);
							base_p.city_defense -= 修复防御;
							pk::add_hp(building_, 修复防御, true);
						}
					}
				}
			} // for

		} // void build()

		int func_附近_敌城市数(pk::city @city, int 距离)
		{
			int enemy_city_count = 0;

			array<pk::city @> cities = pk::list_to_array(pk::get_city_list());
			for (int i = 0; i < int(cities.length); i++)
			{
				pk::city @neighbor_city = cities[i];

				int distance = pk::get_city_distance(city.get_id(), neighbor_city.get_id());

				if (distance > 距离)
					continue;
				if (!pk::is_enemy(city, neighbor_city))
					continue;

				enemy_city_count++;
			}

			return enemy_city_count;
		}

	} // class

	Main main;
}