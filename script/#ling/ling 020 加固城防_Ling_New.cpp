
// ## 2022/08/03 # 铃 # 新增了主动筑城的指令##

namespace 加固城防
{

	// ================ CUSTOMIZE ================

	const int TP_COST = 50;
	const int ACTION_COST = 100;
	const int def_dec = 1000;
	const int gold_num = 3000;

	// ===========================================

	const int KEY = pk::hash("加固城防");

	class Main
	{

		pk::building @building_;
		pk::force @force_;
		pk::person @taishu_;
		pk::person @farmer_;
		pk::person @merchant_;
		pk::person @officer_;
		pk::person @civilian_;
		pk::city @city_;

		Main()
		{
			pk::menu_item item;
			item.menu = 101;
			item.pos = 9;
			item.shortcut = "C";
			item.init = pk::building_menu_item_init_t(init);
			item.is_visible = pk::menu_item_is_visible_t(isVisible);
			item.is_enabled = pk::menu_item_is_enabled_t(isEnabled);
			item.get_text = pk::menu_item_get_text_t(getText);
			item.get_desc = pk::menu_item_get_desc_t(getDesc);
			item.handler = pk::menu_item_handler_t(handler);
			pk::add_menu_item(item);
		}

		void init(pk::building @building)
		{
			@building_ = @building;
			@force_ = pk::get_force(building.get_force_id());
			@taishu_ = pk::get_person(pk::get_taishu_id(building));
			@officer_ = pk::get_person(武将_武将);
			@city_ = pk::building_to_city(building);
		}

		bool isVisible()
		{
			// pk::trace("taishu_.service" + taishu_.service + "building_.get_id()" + building_.get_id());
			if (taishu_.service != building_.get_id())
				return false;
			return true;
		}

		bool isEnabled()
		{
			if (pk::is_absent(taishu_) or pk::is_unitize(taishu_))
				return false;
			else if (taishu_.action_done == true)
				return false;
			else if (force_.tp < TP_COST)
				return false;
			else if (pk::get_gold(building_) < gold_num)
				return false;
			// else if (pk::get_max_hp(building_) > pk::get_max_hp(building_) / 2) return false;
			else if (pk::get_district(pk::get_district_id(force_, 1)).ap < ACTION_COST)
				return false;
			return true;
		}

		string getText()
		{
			return pk::encode("加固城防");
		}

		string getDesc()
		{
			if (pk::is_absent(taishu_) or pk::is_unitize(taishu_))
				return pk::encode("太守不在.");
			else if (taishu_.action_done == true)
				return pk::encode("太守已行动.");
			else if (force_.tp < TP_COST)
				return pk::encode(pk::format("技巧不足.(需要{}技巧)", TP_COST));
			else if (pk::get_district(pk::get_district_id(force_, 1)).ap < ACTION_COST)
				return pk::encode(pk::format("行动力不足(需要{}行动力)", ACTION_COST));
			else if (pk::get_gold(building_) < gold_num)
				return pk::encode(pk::format("金钱不足(需要{}金)", gold_num));
			else
				return pk::encode(pk::format("实施加固城防.(需要{}技巧, {}行动力, {}金)", TP_COST, ACTION_COST , gold_num));
		}

		bool handler()
		{

			if (pk::choose(pk::encode("要实施加固城防吗?"), {pk::encode(" 是 "), pk::encode(" 否 ")}) == 1)
				return false;
			pk::scene(pk::scene_t(scene_Event));
			auto district = pk::get_district(pk::get_district_id(force_, 1));
			pk::add_ap(district, -ACTION_COST);

			taishu_.action_done == true;

			return true;
		}

		void scene_Event()
		{

			int n = pk::rand(10);

			if (n < 10)
			{
				pk::fade(0);
				pk::sleep();
				pk::background(8);
				pk::fade(255);

				pk::message_box(pk::encode("为了保护城市，是时候加固城防了!"), taishu_);
				pk::message_box(pk::encode("连年征战，城防已经有些不足，请务必加筑城市防御."), officer_);
				pk::message_box(pk::encode("是的！如此方能立足一方."), taishu_);

				pk::message_box(pk::encode(pk::format("城市花费\x1b[1x{}\x1b[0x金来加固城防.", gold_num)));
				pk::message_box(pk::encode(pk::format("城防加固了\x1b[1x{}\x1b[0x.", 1000)));
				pk::int_bool deal1;
				int base_id = building_.get_id();

				BuildingInfo @base_p = @building_ex[base_id];

				pk::add_gold(building_, -gold_num, true);
				base_p.city_defense += def_dec;

				// 用额外城防去修复防御
				if (int(building_.hp) - int(pk::get_max_hp(building_)) < 0)
				{
					int 可修复防御 = pk::get_max_hp(building_) - building_.hp;
					int 修复防御 = pk::min(可修复防御, base_p.city_defense);
					base_p.city_defense -= 修复防御;
					pk::add_hp(building_, 修复防御, true);
				}

				pk::fade(0);
				pk::sleep();
				pk::background(-1);
				pk::fade(255);
			}

		} // scene_Event()

	}

	Main main;
}