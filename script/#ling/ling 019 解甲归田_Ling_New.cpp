// ## 2022/12/3 # 铃 # 修复bug
// ## 2022/08/14 # 铃 # 适配了新的内政系统
// ## 2022/03/17 # 铃 # 新增了解甲归田的指令##

namespace 解甲归田
{

	// ================ CUSTOMIZE ================

	const int TP_COST = 5; 	
	const int ACTION_COST = 100;  


// ===========================================

	const int KEY = pk::hash("解甲归田");

	class Main
	{

		pk::building@ building_;
		pk::force@ force_;
		pk::person@ kunshu_;
		pk::person@ farmer_;
		pk::person@ merchant_;
		pk::person@ officer_;
		pk::person@ civilian_;
		pk::city@ city_;

		Main()
		{
			pk::menu_item item;
			item.menu = 100;
			item.pos = 9;
			item.shortcut = "C";
			item.init = pk::building_menu_item_init_t(init);
			item.is_visible = pk::menu_item_is_visible_t(isVisible);
			item.is_enabled = pk::menu_item_is_enabled_t(isEnabled);
			item.get_text = pk::menu_item_get_text_t(getText);
			item.get_desc = pk::menu_item_get_desc_t(getDesc);
			item.handler = pk::menu_item_handler_t(handler);
			pk::add_menu_item(item);
		}

		void init(pk::building@ building)
		{
			@building_ = @building;
			@force_ = pk::get_force(building.get_force_id());
			@kunshu_ = pk::get_person(pk::get_kunshu_id(building));
			@farmer_ = pk::get_person(무장_농민);
			@officer_ = pk::get_person(무장_무관);
			@merchant_ = pk::get_person(무장_상인);
			@civilian_ = pk::get_person(무장_문관);
			@city_ = pk::building_to_city(building);
		}

		bool isVisible()
		{
			//pk::trace("kunshu_.service" + kunshu_.service + "building_.get_id()" + building_.get_id());
			if (kunshu_.service != building_.get_id()) return false;
			return true;
		}

		bool isEnabled()
		{
			if (pk::is_absent(kunshu_) or pk::is_unitize(kunshu_)) return false;
			else if (kunshu_.action_done == true) return false;
			else if (force_.tp < TP_COST) return false;
			else if (pk::get_district(pk::get_district_id(force_, 1)).ap < ACTION_COST) return false;
			return true;
		}

		string getText()
		{
			return pk::encode("解甲归田");
		}

		string getDesc()
		{
			if (pk::is_absent(kunshu_) or pk::is_unitize(kunshu_))
				return pk::encode("君主不在.");
			else if (kunshu_.action_done == true)
				return pk::encode("君主已行动.");
			else if (force_.tp < TP_COST)
				return pk::encode(pk::format("技巧不足.(需要{}技巧)", TP_COST));
			else if (pk::get_district(pk::get_district_id(force_, 1)).ap < ACTION_COST)
				return pk::encode(pk::format("行动力不足(需要{}行动力)", ACTION_COST));
			else
				return pk::encode(pk::format("实施解甲归田.(需要{}技巧, {}行动力)", TP_COST, ACTION_COST));
		}

		bool handler()
		{

			if (pk::choose(pk::encode("要实施解甲归田吗?"), { pk::encode(" 是 "), pk::encode(" 否 ") }) == 1)
				return false;
			pk::scene(pk::scene_t(scene_Event));
			auto district = pk::get_district(pk::get_district_id(force_, 1));
			pk::add_ap(district, -ACTION_COST);

			kunshu_.action_done == true;

			return true;

		}


		void scene_Event()
		{

			int n = pk::rand(10);

			if (n < 10)
			{
				pk::fade(0);
				pk::sleep();
				pk::background(9);
				pk::fade(255);

				pk::message_box(pk::encode("土地荒芜，应该让士兵回乡种田."), kunshu_);
				pk::message_box(pk::encode("连年征战，有许多土地已经无人耕种，主公此举意义深远."), officer_);
				pk::message_box(pk::encode("是的！如此方能立足一方."), kunshu_);

				BaseInfo @building_t = @base_ex[building_.get_id()];
				pk::int_bool deal0;
				int max = building_.troops;

			    deal0 = pk::numberpad(pk::encode("士兵量"), 0, max, 0, pk::numberpad_t(numberpad_t));
				int 解甲量  = deal0.first;


				pk::message_box(pk::encode(pk::format("城市把\x1b[1x{}\x1b[0x士兵退伍还乡.", int(解甲量))));
				pk::message_box(pk::encode(pk::format("城市人口增加了\x1b[1x{}\x1b[0x.", int(解甲量 * 1.2))));
				pk::message_box(pk::encode(pk::format("城市获得了\x1b[1x{}金\x1b[0x.", int(解甲量 * 0.1))));
				
				pk::add_troops(building_, -解甲量, true);
				pk::add_gold(building_, int(解甲量 * 0.1), true);
				building_t.mil_pop_all += int(解甲量);	  //兵役人口相应增加
				building_t.population += int(解甲量 * 1.5); //人口相应增加

				pk::fade(0);
				pk::sleep();
				pk::background(-1);
				pk::fade(255);
			}

		} // scene_Event()

		string numberpad_t(int line, int original_value, int current_value)
		{
			return pk::encode("");
		}
	}

	Main main;
}