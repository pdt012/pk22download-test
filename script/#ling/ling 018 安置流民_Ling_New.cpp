// ## 2022/12/3 # 铃 # 根据现在的人口系统做了新的安置流民功能 ##

namespace 安置流民
{

	// ================ CUSTOMIZE ================

	const int TP_COST = 100;
	const int ACTION_COST = 20;

	const int pop_gain = 10000;
	const int gold_num = 3000;

	// ===========================================

	const int KEY = pk::hash("安置流民");

	class Main
	{

		pk::building @building_;
		pk::force @force;
		pk::person @taishu;
		pk::person @farmer;
		int building_id = -1;

		Main()
		{
			pk::menu_item item;
			item.menu = 100;
			item.pos = 8;
			item.shortcut = "k";
			item.init = pk::building_menu_item_init_t(init);
			item.is_visible = pk::menu_item_is_visible_t(isVisible);
			item.is_enabled = pk::menu_item_is_enabled_t(isEnabled);
			item.get_text = pk::menu_item_get_text_t(getText);
			item.get_desc = pk::menu_item_get_desc_t(getDesc);
			item.handler = pk::menu_item_handler_t(handler);
			pk::add_menu_item(item);
		}

		void init(pk::building @building)
		{
			@building_ = @building;
			building_id = building.get_id();
			@force = pk::get_force(building.get_force_id());
			@taishu = pk::get_person(pk::get_taishu_id(building));
			@farmer = pk::get_person(무장_농민);
		}

		bool isVisible()
		{
			// pk::trace("taishu.service" + taishu.service + "building_.get_id()" + building_.get_id());
			if (building_id == -1)
				return false;
			if (pk::get_taishu_id(building_) == -1)
				return false;
			return true;
		}

		bool isEnabled()
		{
			if (pk::is_absent(taishu) or pk::is_unitize(taishu))
				return false;
			else if (taishu.action_done == true)
				return false;
			else if (pk::get_gold(building_) < gold_num)
				return false;
			else if (force.tp < TP_COST)
				return false;
			else if (pk::get_district(pk::get_district_id(force, 1)).ap < ACTION_COST)
				return false;
			return true;
		}

		string getText()
		{
			return pk::encode("安置流民");
		}

		string getDesc()
		{
			if (pk::is_absent(taishu) or pk::is_unitize(taishu))
				return pk::encode("太守不在.");
			else if (taishu.action_done == true)
				return pk::encode("太守已行动.");
			else if (pk::get_gold(building_) < gold_num)
				return pk::encode(pk::format("金钱不足.(需要{}金)", gold_num));
			else if (force.tp < TP_COST)
				return pk::encode(pk::format("技巧不足.(需要{}技巧)", TP_COST));
			else if (pk::get_district(pk::get_district_id(force, 1)).ap < ACTION_COST)
				return pk::encode(pk::format("行动力不足(需要{}行动力)", ACTION_COST));
			else
				return pk::encode(pk::format("实施安置流民.(需要{}技巧, {}行动力)", TP_COST, ACTION_COST));
		}

		bool handler()
		{

			if (pk::choose(pk::encode("要实施安置流民吗?"), {pk::encode(" 是 "), pk::encode(" 否 ")}) == 1)
				return false;
			pk::scene(pk::scene_t(scene_Event));
			auto district = pk::get_district(pk::get_district_id(force, 1));
			pk::add_ap(district, -ACTION_COST);

			taishu.action_done == true;

			return true;
		}

		void scene_Event()
		{
			int forceid;
			int pop_gain_final;

			int n = pk::rand(10);

			if (n <= 10)
			{
				pk::fade(0);
				pk::sleep();
				pk::background(16);
				pk::fade(255);

				pk::message_box(pk::encode("百姓们，我已经找到了一片新的土地，那里有肥沃的田地。"), taishu);
				pk::message_box(pk::encode("我邀请你们跟随我，去那里安居乐业，重建我们的家园。你们愿意跟我一起走吗？"), taishu);
				pk::message_box(pk::encode("谢谢大人，我们愿跟随您!"), farmer);

				if (building_id < 42)
					pop_gain_final = pop_gain;
				else
					pop_gain_final = int(pop_gain * 0.5);

				if (ch::has_skill(building_, 特技_安民))
				{
					pop_gain_final = int(pop_gain_final * 1.5f); // 安民
				}

				else if (ch::has_skill(building_, 特技_安民) or ch::has_skill(building_, 特技_祈愿))
				{
					pop_gain_final = int(pop_gain_final * 1.2f); // 安民祈愿
				}

				pk::message_box(pk::encode(pk::format("城市花费\x1b[1x{}\x1b[0x金来安置流民.", gold_num)));
				pk::message_box(pk::encode(pk::format("安置了\x1b[1x{}\x1b[0x人流民.", pop_gain_final)));

				pk::add_gold(building_, -gold_num, true);
				pk::add_tp(force, -TP_COST, force.get_pos());

				ch::add_population(building_id, pop_gain_final);

				pk::fade(0);
				pk::sleep();
				pk::background(-1);
				pk::fade(255);
			}

		} // scene_Event()

	}

	Main main;
}