﻿// ## 2023/06/26 #江东新风# 将AI对应功能合并入该文件 ##
// ## 2021/10/01 # 江东新风 # 关闭大杀四方动画 ##
// ## 2021/09/26 # 江东新风 # 战术的动画效果测试，暂不更新，必须写到对应指令cpp下 ##
// ## 2021/01/01 # 江东新风 # 战神特技也能大杀四方 ##
// ## 2020/08/16 #江东新风#has_skill函数替換##
// ## 2020/08/07 # 氕氘氚 # 规则、代碼优化 ##
// ## 2020/08/05 # 江东新风 # 兵种不符时，无法发动对应特技 ##
// ## 2020/08/05 # messi # 重新设计台詞 ##
// ## 2020/07/26 ##
/*
@제작자: HoneyBee
@설명: 통솔, 무력이 동시에 높은 무장이 특정 특기를 소유한 경우에 활용할 수 있는 부대 메뉴


※ CHAMPION TYPE 부대의 효과

(1) 특기 <앙양>을 소유 시 주위에 있는 적군 부대 기력 흡수 (<앙양> 특기를 보유한 적군부대는 면역)
(2) 특기 <위풍>을 소유 시 주위에 있는 적군 부대 기력 감소 및 자신보다 무력이 낮은 적부대를 일정확률로 혼란 (<위풍> 특기를 보유한 적군부대는 면역)
(3) 특기 [창장 or 창신]을 소유한 창병부대 주위에 있는 적군 기병부대 병력 및 기력 감소 (단, 해당 특기 소유자의 무력 이상인 적군 무장의 부대는 면역)
(4) 특기 [극장 or 극신 or 친월 or 친만]를 소유한 극병부대 주위에 있는 적군 창병부대 병력 및 기력 감소 (단, 해당 특기 소유자의 무력 이상인 적군 무장의 부대는 면역)
(5) 특기 [질주 or 기장 or 기신 or 친강 or 친오]를 소유한 기병부대 주위에 있는 적군 극병부대 병력 및 기력 감소 (단, 해당 특기 소유자의 무력 이상인 적군 무장의 부대는 면역)
(6) 특기 [패왕 or 용장 or 맹장 or 신장 or 비장 or 투신]을 소유한 부대 주위에 있는
	적군부대 병력,기력 감소 및 자신보다 무력이 낮은 적부대를 일정확률로 혼란
   ([패왕 or 용장 or 맹장 or 신장 or 비장 or 투신] 특기를 보유한 적군부대는 면역)


*/

namespace 部队指令_部队战术
{

	// ================ CUSTOMIZE ================


		//	全局设置-部队作战技能
	// =============== AICUSTOMIZE ===============
		// 이미 부대전술메뉴가 존재하므로 효과는 AI만으로 설정하는 것을 권장
	const bool 开启战术发动 = true;
	const bool 仅AI有效 = true;  			// true =AI만 효과적용, false =유저와 AI 모두 효과 적용 
	const int 发动时机 = 0;        	   	   		// 0: 매턴,  1: 매월 1일,  2: 매분기 첫달1일
	const int 发动机率 = 10;      			// 효과가 발휘되는 확률 (기본 10%)
	const int 气力条件 = 80;
	// ===========================================


	class Main
	{
		pk::unit@ src_unit;
		pk::point src_pos_;
		pk::person@ src_leader;
		Main()
		{
			// 부대 메뉴 추가
			add_menu_unit_order();
			//AI部队战术
			pk::bind(107, pk::trigger107_t(callback));
		}
		////////////////////////////////////////////////////////////////////////////AI功能相关//////////////////////////////////////////////////////////////////////////////////////////

		void callback()
		{
			// 발동시기 매턴
			if (发动时机 == 0)
			{
				effect_战术();
			}
			// 매월 1일
			else if (发动时机 == 1 and (pk::get_day() == 1))
			{
				effect_战术();
			}
			// 매분기 첫달이면서 1일	
			else if (发动时机 == 2 and (pk::get_day() == 1) and pk::is_first_month_of_quarter(pk::get_month()))
			{
				effect_战术();
			}

		}

		// 유저 또는 AI만 적용 시 판단함수
		bool only_AI_unit(pk::unit@ unit)
		{
			if (仅AI有效 and unit.is_player()) return false;
			return true;
		}

		void effect_战术()
		{
			if (!开启战术发动) return;

			auto list = pk::list_to_array(pk::get_unit_list());

			for (int i = 0; i < int(list.length); i++)
			{
				if (!pk::rand_bool(发动机率)) continue;
				pk::unit@ src = list[i];
				if (src.status != 部队状态_通常) continue;
				pk::person@ src_leader = pk::get_person(src.leader);
				pk::point pos = src.get_pos();

				if (only_AI_unit(src) && src.energy > 气力条件)
				{
					// 특기 <앙양>을 소유 시 주위에 있는 적군 부대 기력 흡수 (단, 해당 특기 소유자의 무력 이상인 적군 무장의 부대는 면역)
					if (src.has_skill(特技_昂扬))
					{
						global_func_昂扬奋战(pos, @src, @src_leader);
					}

					// 특기 <위풍>을 소유 시 주위에 있는 적군 부대 기력 감소 및 일정확률로 혼란 (단, 해당 특기 소유자의 무력 이상인 적군 무장의 부대는 면역)
					else if (src.has_skill(特技_威风))
					{
						global_func_威风屈敌(pos, @src, @src_leader);
					}

					// 특기 [창장 or 창신]을 소유한 창병부대 주위에 있는 적군 기병부대 병력 및 기력 감소 (단, 해당 특기 소유자의 무력 이상인 적군 무장의 부대는 면역)
					else if ((src.has_skill(特技_枪将) or src.has_skill(特技_枪神) or src.has_skill(特技_斗神))
						and src.type_id == 兵器_枪)
					{
						global_func_枪兵冲锋(pos, @src, @src_leader);
					}

					// 특기 [극장 or 극신 or 친월 or 친만]을 소유한 극병부대 주위에 있는 적군 창병부대 병력 및 기력 감소 (단, 해당 특기 소유자의 무력 이상인 적군 무장의 부대는 면역)
					else if ((src.has_skill(特技_戟将) or src.has_skill(特技_戟神) or src.has_skill(特技_亲越) or src.has_skill(特技_亲蛮))
						and src.type_id == 兵器_戟)
					{
						global_func_戟兵破甲(pos, @src, @src_leader);
					}

					// 특기 [기장 or 기신]을 소유한 기병부대 주위에 있는 적군 극병부대 병력 및 기력 감소 (단, 해당 특기 소유자의 무력 이상인 적군 무장의 부대는 면역)
					else if ((src.has_skill(特技_疾驰) or src.has_skill(特技_骑将) or src.has_skill(特技_骑神) or src.has_skill(特技_亲羌) or src.has_skill(特技_亲乌))
						and src.type_id == 兵器_战马)
					{
						global_func_骑兵突袭(pos, @src, @src_leader);
					}

					// 특기 [패왕 or 용장 or 맹장 or 신장 or 비장 or 투신]을 소유한 부대 주위에 있는 적군부대 병력 감소 및 혼란 (단, 해당 특기 소유자의 무력 이상인 적군 무장의 부대는 면역)
					else if (src.has_skill(特技_霸王) or src.has_skill(特技_勇将) or src.has_skill(特技_猛者)
						or src.has_skill(特技_神将) or src.has_skill(特技_飞将) or src.has_skill(特技_战神))
					{
						global_func_大杀四方(pos, @src, @src_leader);
					}

				} // (only_AI_unit(src))

			} // for 

		} // effect_战术

		////////////////////////////////////////////////////////////////////////////菜单功能相关////////////////////////////////////////////////////////////////////////////////////////

		void add_menu_unit_order()
		{
			if (开启_部队战术_玩家)
			{
				pk::menu_item 部队;
				int 部队_上位菜单;

				部队.menu = 1;
				部队.get_text = cast<pk::menu_item_get_text_t@>(function() { return pk::encode("部队战术"); });
				部队.init = cast<pk::unit_menu_item_init_t@>(function(unit, src_pos) { @main.src_unit = @unit; main.src_pos_ = src_pos; });
				部队.is_visible = cast<pk::menu_item_is_visible_t@>(function() { return (main.src_unit).type == 部队类型_战斗; });
				部队_上位菜单 = pk::add_menu_item(部队);

				if (开启_昂扬奋战)
				{
					pk::menu_item 战术_昂扬奋战;
					战术_昂扬奋战.menu = 部队_上位菜单;
					战术_昂扬奋战.init = pk::unit_menu_item_init_t(init);
					战术_昂扬奋战.get_text = pk::menu_item_get_text_t(getText_战术_昂扬奋战);
					战术_昂扬奋战.get_desc = pk::menu_item_get_desc_t(getDesc_战术_昂扬奋战);
					战术_昂扬奋战.is_visible = pk::menu_item_is_visible_t(isVisible);
					战术_昂扬奋战.is_enabled = pk::menu_item_is_enabled_t(isEnabled_战术_昂扬奋战);
					战术_昂扬奋战.handler = pk::unit_menu_item_handler_t(handler_战术_昂扬奋战);
					pk::add_menu_item(战术_昂扬奋战);
				}

				if (开启_威风屈敌)
				{
					pk::menu_item 战术_威风屈敌;
					战术_威风屈敌.menu = 部队_上位菜单;
					战术_威风屈敌.init = pk::unit_menu_item_init_t(init);
					战术_威风屈敌.get_text = pk::menu_item_get_text_t(getText_威风屈敌);
					战术_威风屈敌.get_desc = pk::menu_item_get_desc_t(getDesc_威风屈敌);
					战术_威风屈敌.is_visible = pk::menu_item_is_visible_t(isVisible);
					战术_威风屈敌.is_enabled = pk::menu_item_is_enabled_t(isEnabled_威风屈敌);
					战术_威风屈敌.handler = pk::unit_menu_item_handler_t(handler_威风屈敌);
					pk::add_menu_item(战术_威风屈敌);
				}

				if (开启_枪兵冲锋)
				{
					pk::menu_item 战术_枪兵冲锋;
					战术_枪兵冲锋.menu = 部队_上位菜单;
					战术_枪兵冲锋.init = pk::unit_menu_item_init_t(init);
					战术_枪兵冲锋.get_text = pk::menu_item_get_text_t(getText_战术_枪兵冲锋);
					战术_枪兵冲锋.get_desc = pk::menu_item_get_desc_t(getDesc_战术_枪兵冲锋);
					战术_枪兵冲锋.is_visible = pk::menu_item_is_visible_t(isVisible);
					战术_枪兵冲锋.is_enabled = pk::menu_item_is_enabled_t(isEnabled_战术_枪兵冲锋);
					战术_枪兵冲锋.handler = pk::unit_menu_item_handler_t(handler_战术_枪兵冲锋);
					pk::add_menu_item(战术_枪兵冲锋);
				}

				if (开启_戟兵破甲)
				{
					pk::menu_item 战术_戟兵破甲;
					战术_戟兵破甲.menu = 部队_上位菜单;
					战术_戟兵破甲.init = pk::unit_menu_item_init_t(init);
					战术_戟兵破甲.get_text = pk::menu_item_get_text_t(getText_战术_戟兵破甲);
					战术_戟兵破甲.get_desc = pk::menu_item_get_desc_t(getDesc_战术_戟兵破甲);
					战术_戟兵破甲.is_visible = pk::menu_item_is_visible_t(isVisible);
					战术_戟兵破甲.is_enabled = pk::menu_item_is_enabled_t(isEnabled_战术_戟兵破甲);
					战术_戟兵破甲.handler = pk::unit_menu_item_handler_t(handler_战术_戟兵破甲);
					pk::add_menu_item(战术_戟兵破甲);
				}

				if (开启_骑兵突袭)
				{
					pk::menu_item 战术_骑兵突袭;
					战术_骑兵突袭.menu = 部队_上位菜单;
					战术_骑兵突袭.init = pk::unit_menu_item_init_t(init);
					战术_骑兵突袭.get_text = pk::menu_item_get_text_t(getText_战术_骑兵突袭);
					战术_骑兵突袭.get_desc = pk::menu_item_get_desc_t(getDesc_战术_骑兵突袭);
					战术_骑兵突袭.is_visible = pk::menu_item_is_visible_t(isVisible);
					战术_骑兵突袭.is_enabled = pk::menu_item_is_enabled_t(isEnabled_战术_骑兵突袭);
					战术_骑兵突袭.handler = pk::unit_menu_item_handler_t(handler_战术_骑兵突袭);
					pk::add_menu_item(战术_骑兵突袭);
				}

				if (开启_大杀四方)
				{
					pk::menu_item 战术_大杀四方;
					战术_大杀四方.menu = 部队_上位菜单;
					战术_大杀四方.init = pk::unit_menu_item_init_t(init);
					战术_大杀四方.get_text = pk::menu_item_get_text_t(getText_战术_大杀四方);
					战术_大杀四方.get_desc = pk::menu_item_get_desc_t(getDesc_战术_大杀四方);
					战术_大杀四方.is_visible = pk::menu_item_is_visible_t(isVisible);
					战术_大杀四方.is_enabled = pk::menu_item_is_enabled_t(isEnabled_战术_大杀四方);
					战术_大杀四方.handler = pk::unit_menu_item_handler_t(handler_战术_大杀四方);
					pk::add_menu_item(战术_大杀四方);
				}

			}

		} // add_menu_unit_order

		void init(pk::unit@ unit, pk::point src_pos)
		{
			@src_unit = @unit;
			src_pos_ = src_pos;
			@src_leader = pk::get_person(src_unit.leader);
		}

		bool isVisible()
		{
			if (pk::is_campaign()) return false;
			if (!setting_ex.mod_set[战术战略妙计_开关]) return false;
			return true;
		}

		//---------------------------------------------------------------------------
		// 부대전술메뉴 : 战术_昂扬奋战 
		//---------------------------------------------------------------------------

		string getText_战术_昂扬奋战()
		{
			return pk::encode(pk::format("昂扬奋战 ({})", ENERGY_COST_昂扬奋战));  //angyang zhanhsu
		}

		string getDesc_战术_昂扬奋战()
		{
			if (src_unit.energy < ENERGY_COST_昂扬奋战)
				return pk::encode("气力不足.");
			else if (src_unit.troops < 部队战术_兵力条件)
				return pk::encode("兵力不足.");
			else if (!ch::has_skill(src_unit, 特技_昂扬))
				return pk::encode("没有特技昂扬.");
			else if ((src_leader.stat[武将能力_统率] < 部队战术_统率条件) or (src_leader.stat[武将能力_武力] < 部队战术_武力条件))
				return pk::encode("统率/武力不符合条件.");
			else
				return pk::encode(pk::format("使用昂扬奋战战术.(气力至少 {} 以上)", ENERGY_COST_昂扬奋战));
		}

		bool isVisible_战术_昂扬奋战()
		{
			return true;
		}

		bool isEnabled_战术_昂扬奋战()
		{
			if (src_unit.energy < ENERGY_COST_昂扬奋战) return false;
			else if (src_unit.troops < 部队战术_兵力条件) return false;
			else if (!ch::has_skill(src_unit, 特技_昂扬)) return false;
			else if ((src_leader.stat[武将能力_统率] < 部队战术_统率条件) or (src_leader.stat[武将能力_武力] < 部队战术_武力条件)) return false;
			return true;
		}

		bool handler_战术_昂扬奋战(pk::point dst_pos)
		{
			global_func_昂扬奋战(src_pos_, @src_unit, @src_leader);

			global_func_发动完成结算(@src_unit, ENERGY_COST_昂扬奋战);

			//pk::scene(pk::scene_t(scen_昂扬奋战));

			return true;
		}

		void scen_昂扬奋战()
		{
			//pk::fade(0);
			//pk::background(-1);
			pk::background("mediaEX/animation/109-1.png");
			//pk::play_se(16);
			pk::sleep(300);
			pk::background("mediaEX/animation/110-1.png");
			pk::sleep(300);
			pk::background("mediaEX/animation/111-1.png");
			pk::sleep(300);
			pk::background("mediaEX/animation/112-1.png");
			pk::sleep(300);
			//pk::background(5);
		}


		//---------------------------------------------------------------------------
		// 부대전술메뉴 : 战术_威风屈敌 
		//---------------------------------------------------------------------------

		string getText_威风屈敌()
		{
			return pk::encode(pk::format("威风屈敌 ({})", ENERGY_COST_威风屈敌));  //战术_威风屈敌
		}

		string getDesc_威风屈敌()
		{
			if (src_unit.energy < ENERGY_COST_威风屈敌)
				return pk::encode("气力不足.");
			else if (src_unit.troops < 部队战术_兵力条件)
				return pk::encode("兵力不足.");
			else if (!ch::has_skill(src_unit, 特技_威风))
				return pk::encode("没有特技威风.");
			else if ((src_leader.stat[武将能力_统率] < 部队战术_统率条件) or (src_leader.stat[武将能力_武力] < 部队战术_武力条件))
				return pk::encode("统率/武力不符合条件.");
			else
				return pk::encode(pk::format("使用威风屈敌战术.(气力至少 {} 以上)", ENERGY_COST_威风屈敌));
		}

		bool isVisible_威风屈敌()
		{
			return true;
		}

		bool isEnabled_威风屈敌()
		{
			if (src_unit.energy < ENERGY_COST_威风屈敌) return false;
			else if (src_unit.troops < 部队战术_兵力条件) return false;
			else if (!ch::has_skill(src_unit, 特技_威风)) return false;
			else if ((src_leader.stat[武将能力_统率] < 部队战术_统率条件) or (src_leader.stat[武将能力_武力] < 部队战术_武力条件)) return false;
			return true;
		}

		bool handler_威风屈敌(pk::point dst_pos)
		{
			global_func_威风屈敌(src_pos_, @src_unit, @src_leader);

			global_func_发动完成结算(@src_unit, ENERGY_COST_威风屈敌);

			return true;
		}


		//---------------------------------------------------------------------------
		// 부대전술메뉴 : 战术_枪兵冲锋 
		//---------------------------------------------------------------------------

		string getText_战术_枪兵冲锋()
		{
			return pk::encode(pk::format("枪兵冲锋 ({})", ENERGY_COST_枪兵冲锋));  //qiangjiang
		}

		string getDesc_战术_枪兵冲锋()
		{
			if (src_unit.energy < ENERGY_COST_枪兵冲锋)
				return pk::encode("气力不足.");
			else if (src_unit.weapon != 兵器_枪)
				return pk::encode("兵种不符, 无法发动!");
			else if (src_unit.troops < 部队战术_兵力条件)
				return pk::encode("兵力不足.");
			else if (!ch::has_skill(src_unit, 特技_枪将) and !ch::has_skill(src_unit, 特技_枪神) and !ch::has_skill(src_unit, 特技_斗神))
				return pk::encode("没有枪兵系特技.");
			else if ((src_leader.stat[武将能力_统率] < 部队战术_统率条件) or (src_leader.stat[武将能力_武力] < 部队战术_武力条件))
				return pk::encode("统率/武力不符合条件.");
			else
				return pk::encode(pk::format("使用枪兵冲锋战术(敌骑军气力/兵力减少).(气力至少 {} 以上)", ENERGY_COST_枪兵冲锋));
		}

		bool isVisible_战术_枪兵冲锋()
		{
			return true;
		}

		bool isEnabled_战术_枪兵冲锋()
		{
			if (src_unit.energy < ENERGY_COST_枪兵冲锋) return false;
			else if (src_unit.weapon != 兵器_枪) return false;
			else if (src_unit.troops < 部队战术_兵力条件) return false;
			else if (!ch::has_skill(src_unit, 特技_枪将) and !ch::has_skill(src_unit, 特技_枪神) and !ch::has_skill(src_unit, 特技_斗神)) return false;
			else if ((src_leader.stat[武将能力_统率] < 部队战术_统率条件) or (src_leader.stat[武将能力_武力] < 部队战术_武力条件)) return false;
			return true;
		}

		bool handler_战术_枪兵冲锋(pk::point dst_pos)
		{
			global_func_枪兵冲锋(src_pos_, @src_unit, @src_leader);

			global_func_发动完成结算(@src_unit, ENERGY_COST_枪兵冲锋);
			
			//pk::scene(pk::scene_t(scen_枪兵冲锋));

			return true;
		}

		void scen_枪兵冲锋()
		{
			//pk::fade(0);
			//pk::background(-1);
			pk::background("mediaEX/animation/9-1.png");
			//pk::play_se(16);
			pk::sleep(300);
			pk::background("mediaEX/animation/10-1.png");
			pk::sleep(300);
			pk::background("mediaEX/animation/11-1.png");
			pk::sleep(300);
			pk::background("mediaEX/animation/12-1.png");
			pk::sleep(300);
			//pk::background(5);
		}

		//---------------------------------------------------------------------------
		// 부대전술메뉴 : 战术_戟兵破甲 
		//---------------------------------------------------------------------------

		string getText_战术_戟兵破甲()
		{
			return pk::encode(pk::format("戟兵破甲 ({})", ENERGY_COST_戟兵破甲));  //jibing
		}

		string getDesc_战术_戟兵破甲()
		{
			if (src_unit.energy < ENERGY_COST_戟兵破甲)
				return pk::encode("气力不足.");
			else if (src_unit.weapon != 兵器_戟)
				return pk::encode("兵种不符, 无法发动!");
			else if (src_unit.troops < 部队战术_兵力条件)
				return pk::encode("兵力不足.");
			else if (!ch::has_skill(src_unit, 特技_戟将) and !ch::has_skill(src_unit, 特技_戟神) and !ch::has_skill(src_unit, 特技_斗神) and !ch::has_skill(src_unit, 特技_亲越) and !ch::has_skill(src_unit, 特技_亲蛮))
				return pk::encode("没有戟兵系特技.");
			else if ((src_leader.stat[武将能力_统率] < 部队战术_统率条件) or (src_leader.stat[武将能力_武力] < 部队战术_武力条件))
				return pk::encode("统率/武力不符合条件.");
			else
				return pk::encode(pk::format("使用戟兵破甲战术(敌枪兵气力/兵力减少).(气力至少 {} 以上)", ENERGY_COST_戟兵破甲));
		}

		bool isVisible_战术_戟兵破甲()
		{
			return true;
		}

		bool isEnabled_战术_戟兵破甲()
		{
			if (src_unit.energy < ENERGY_COST_戟兵破甲) return false;
			else if (src_unit.weapon != 兵器_戟) return false;
			else if (src_unit.troops < 部队战术_兵力条件) return false;
			else if (!ch::has_skill(src_unit, 特技_戟将) and !ch::has_skill(src_unit, 特技_戟神) and !ch::has_skill(src_unit, 特技_斗神) and !ch::has_skill(src_unit, 特技_亲越) and !ch::has_skill(src_unit, 特技_亲蛮)) return false;
			else if ((src_leader.stat[武将能力_统率] < 部队战术_统率条件) or (src_leader.stat[武将能力_武力] < 部队战术_武力条件)) return false;
			return true;
		}

		bool handler_战术_戟兵破甲(pk::point dst_pos)
		{
			global_func_戟兵破甲(src_pos_, @src_unit, @src_leader);

			global_func_发动完成结算(@src_unit, ENERGY_COST_戟兵破甲);

			return true;
		}


		//---------------------------------------------------------------------------
		// 부대전술메뉴 : 战术_骑兵突袭 
		//---------------------------------------------------------------------------

		string getText_战术_骑兵突袭()
		{
			return pk::encode(pk::format("骑兵突袭 ({})", ENERGY_COST_骑兵突袭));  //qibing zhanshu
		}

		string getDesc_战术_骑兵突袭()
		{
			if (src_unit.energy < ENERGY_COST_骑兵突袭)
				return pk::encode("气力不足.");
			else if (src_unit.weapon != 兵器_战马)
				return pk::encode("兵种不符, 无法发动!");
			else if (src_unit.troops < 部队战术_兵力条件)
				return pk::encode("兵力不足.");
			else if (!ch::has_skill(src_unit, 特技_疾驰) and !ch::has_skill(src_unit, 特技_骑将) and !ch::has_skill(src_unit, 特技_骑神) and !ch::has_skill(src_unit, 特技_亲羌) and !ch::has_skill(src_unit, 特技_亲乌))
				return pk::encode("没有骑兵系特技.");
			else if ((src_leader.stat[武将能力_统率] < 部队战术_统率条件) or (src_leader.stat[武将能力_武力] < 部队战术_武力条件))
				return pk::encode("统率/武力不符合条件.");
			else
				return pk::encode(pk::format("使用骑兵突袭战术(敌戟军气力/兵力减少).(气力至少 {} 以上)", ENERGY_COST_骑兵突袭));
		}

		bool isVisible_战术_骑兵突袭()
		{
			return true;
		}

		bool isEnabled_战术_骑兵突袭()
		{
			if (src_unit.energy < ENERGY_COST_骑兵突袭) return false;
			else if (src_unit.weapon != 兵器_战马) return false;
			else if (src_unit.troops < 部队战术_兵力条件) return false;
			else if (!ch::has_skill(src_unit, 特技_疾驰) and !ch::has_skill(src_unit, 特技_骑将) and !ch::has_skill(src_unit, 特技_骑神) and !ch::has_skill(src_unit, 特技_亲羌) and !ch::has_skill(src_unit, 特技_亲乌)) return false;
			else if ((src_leader.stat[武将能力_统率] < 部队战术_统率条件) or (src_leader.stat[武将能力_武力] < 部队战术_武力条件)) return false;
			return true;
		}

		bool handler_战术_骑兵突袭(pk::point dst_pos)
		{
			global_func_骑兵突袭(src_pos_, @src_unit, @src_leader);

			global_func_发动完成结算(@src_unit, ENERGY_COST_骑兵突袭);
			//pk::scene(pk::scene_t(scen_骑兵突袭));
			return true;
		}

		void scen_骑兵突袭()
		{
			//pk::fade(0);
			//pk::background(-1);
			pk::background("mediaEX/animation/17-1.png");
			//pk::play_se(16);
			pk::sleep(300);
			pk::background("mediaEX/animation/18-1.png");
			pk::sleep(300);
			pk::background("mediaEX/animation/19-1.png");
			pk::sleep(300);
			pk::background("mediaEX/animation/20-1.png");
			pk::sleep(300);
			//pk::background(5);
		}
		//---------------------------------------------------------------------------
		// 부대전술메뉴 : 战术_大杀四方 
		//---------------------------------------------------------------------------

		string getText_战术_大杀四方()
		{
			return pk::encode(pk::format("大杀四方 ({})", ENERGY_COST_大杀四方));  //yongjiang zhanshu
		}

		string getDesc_战术_大杀四方()
		{
			if (src_unit.energy < ENERGY_COST_大杀四方)
				return pk::encode("气力不足.");
			else if (src_unit.troops < 部队战术_兵力条件)
				return pk::encode("兵力不足.");
			else if (!ch::has_skill(src_unit, 特技_霸王) and !ch::has_skill(src_unit, 特技_勇将) and !ch::has_skill(src_unit, 特技_猛者) and !ch::has_skill(src_unit, 特技_神将) and !ch::has_skill(src_unit, 特技_飞将) and !ch::has_skill(src_unit, 特技_斗神) and !ch::has_skill(src_unit, 特技_战神))
				return pk::encode("没有猛将系特技.");
			else if ((src_leader.stat[武将能力_统率] < 部队战术_统率条件) or (src_leader.stat[武将能力_武力] < 部队战术_武力条件))
				return pk::encode("统率/武力不符合条件.");
			else
				return pk::encode(pk::format("依靠猛将大杀四方的战术(敌军气力/兵力减少/混乱).(气力至少 {} 以上)", ENERGY_COST_大杀四方));
		}

		bool isVisible_战术_大杀四方()
		{
			return true;
		}

		bool isEnabled_战术_大杀四方()
		{
			if (src_unit.energy < ENERGY_COST_大杀四方) return false;
			else if (src_unit.troops < 部队战术_兵力条件) return false;
			else if (!ch::has_skill(src_unit, 特技_霸王) and !ch::has_skill(src_unit, 特技_勇将) and !ch::has_skill(src_unit, 特技_猛者) and !ch::has_skill(src_unit, 特技_神将) and !ch::has_skill(src_unit, 特技_飞将) and !ch::has_skill(src_unit, 特技_战神)) return false;
			else if ((src_leader.stat[武将能力_统率] < 部队战术_统率条件) or (src_leader.stat[武将能力_武力] < 部队战术_武力条件)) return false;
			return true;
		}

		bool handler_战术_大杀四方(pk::point dst_pos)
		{
			global_func_大杀四方(src_pos_, @src_unit, @src_leader);

			global_func_发动完成结算(@src_unit, ENERGY_COST_大杀四方);
			
			//pk::scene(pk::scene_t(scen_大杀四方));
			/*变换兵种
			src_unit.weapon = 兵器_木兽;
			src_unit.stock[0].id = 兵器_木兽;
			pk::create_effect(59, src_unit.pos);//59,75
			//src_unit.update();
			array<pk::point>steps;
			steps.insertLast(src_unit.pos);
			pk::move(src_unit, steps);
			src_unit.stock[0].amount = 1;*/
			return true;
		}

		void scen_大杀四方()
		{
			//pk::fade(255);

			  //pk::background(-1);
			pk::play_bgm_file(pk::game_dir + "mediaEX/se/se_h001_spear.ogg", false, 100);
			pk::background("mediaEX/animation/9-1.png");

			pk::sleep(500);
			pk::background("mediaEX/animation/10-1.png");
			pk::sleep(500);
			pk::background("mediaEX/animation/11-1.png");
			pk::sleep(500);
			pk::background("mediaEX/animation/12-1.png");
			pk::sleep(500);
			//pk::background(5);
		}
	} // class Main

	Main main;

} // namespace