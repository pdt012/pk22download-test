﻿// ## 2023/06/26 #江东新风# 将AI对应功能合并入该文件 ##
// ## 2020/08/16 #江东新风# has_skill函数替換 ##
// ## 2020/08/07 # 氕氘氚 # 规则、代碼优化 ##
// ## 2020/08/05 # messi # 重新设计台詞 ##
// ## 2020/07/26 ##
/*
@제작자: HoneyBee
@설명: 부대 지력이 적부대의 지력보다 높은 경우와 낮은 경우에 각기 정해진 성공률이 반영되는 SCRIPT


※ 효과 참고
- 적부대의 지력보다 높은 경우 별도 성공률 적용
- 적부대의 지력보다 낮은 경우 별도 성공률 적용
- 범위에 있는 모든 적군 부대에게 계략으로 피해를 줍니다. (단, 특정 특기를 소유한 적부대는 면역)
- 지력 경험치 상승 (묘책을 실행하는 부대에 속한 모든 무장들)
- 귀모계 효과: 범위 내 적군부대 혼란
- 허보계 효과: 범위 내 적군부대 위보
- 신화계 효과: 범위 내 적군부대 화계 + 기력 감소 + 혼란
- 팔진도 효과: 범위 내 [아군부대 기력/상태 회복/미행동 전환] and [적군부대 기력 감소 + 혼란]
- 허허실실 효과: 범위 내 [적군부대 병력 감소 + 혼란] or [적군부대 병력 감소 + 위보]
- 속전고수 효과: 범위 내 [아군부대 병력/기력 회복/미행동 전환] and [적군부대 병량/기력 감소]

*/

namespace 部队指令_部队妙计
{

	// ================ CUSTOMIZE ================

		//	全局设置-部队作战技能

	// =============== AICUSTOMIZE ===============
		///  TEST를 위한 목적으로 존재하는 변수이며, 효과_AI만 = true로 설정하는 것을 권장 
	const bool 仅AI有效 = true;  				// true =AI만 효과적용, false =유저와 AI 모두 효과 적용 

	const int 发动时机 = 0;        	   	   		// 0: 매턴,  1: 매월 1일,  2: 매분기 첫달1일

	const int 发动机率 = 10;      					// 효과가 발동되는 확률 (기본 10%)

	const int 气力条件 = 80;
	// ===========================================

	class Main
	{
		pk::unit@ src_unit;
		pk::point src_pos_;
		pk::person@ src_leader;
		Main()
		{
			// 부대 메뉴 추가
			add_menu_unit_order();
			//用于ai对应功能
			pk::bind(107, pk::trigger107_t(callback));
		}
		////////////////////////////////////////////////////////////////////////////AI功能相关//////////////////////////////////////////////////////////////////////////////////////////


		void callback()
		{
			// 발동시기 매턴
			if (发动时机 == 0)
			{
				effect_妙计();
			}
			// 매월 1일
			else if (发动时机 == 1 and (pk::get_day() == 1))
			{
				effect_妙计();
			}
			// 매분기 첫달이면서 1일	
			else if (发动时机 == 2 and (pk::get_day() == 1) and pk::is_first_month_of_quarter(pk::get_month()))
			{
				effect_妙计();
			}
		}

		// 유저 또는 AI만 적용 시 판단함수
		bool only_AI_unit(pk::unit@ unit)
		{
			if (仅AI有效 and unit.is_player()) return false;
			return true;
		}

		void effect_妙计()
		{
			auto list = pk::list_to_array(pk::get_unit_list());

			for (int i = 0; i < int(list.length); i++)
			{
				if (!pk::rand_bool(发动机率)) 	continue;

				pk::unit@ src_unit = list[i];
				pk::person@ src_leader = pk::get_person(src_unit.leader);
				pk::point pos = src_unit.get_pos();
				//pk::trace(pk::format("{}的部队妙计判断",pk::decode(pk::get_name(src_unit))));

				if (only_AI_unit(src_unit) && src_unit.status == 部队状态_通常 && src_unit.energy >= 气力条件 && src_unit.troops >= 部队妙计_兵力条件)
				{
					//pk::trace(pk::format("{}的部队可以执行部队妙计",pk::decode(pk::get_name(src_unit))));

					if (开启_神火计 && src_unit.has_skill(特技_火神))
					{
						global_func_神火计(pos, @src_unit, @src_leader);
					}

					if (开启_八阵图 && src_unit.has_skill(特技_神算))
					{
						global_func_八阵图(pos, @src_unit, @src_leader);
					}

					if (开启_虚虚实实 && src_unit.has_skill(特技_虚实))
					{
						global_func_虚虚实实(pos, @src_unit, @src_leader);
						//pk::trace(pk::format("{}的执行了虚虚实实",pk::decode(pk::get_name(src_unit))));
					}

					if (开启_速战固守 && src_unit.has_skill(特技_深谋))
					{
						global_func_速战固守(pos, @src_unit, @src_leader);
					}

				}
			}
		}


		////////////////////////////////////////////////////////////////////////////菜单功能相关////////////////////////////////////////////////////////////////////////////////////////
		
		void add_menu_unit_order()
		{
			if (开启部队妙计_玩家)
			{
				pk::menu_item 部队;
				int 部队_上位菜单;

				部队.menu = 1;
				部队.get_text = cast<pk::menu_item_get_text_t@>(function() { return pk::encode("妙计"); });  //miaoji
				部队.init = cast<pk::unit_menu_item_init_t@>(function(unit, src_pos) { @main.src_unit = @unit; main.src_pos_ = src_pos; });
				部队.is_visible = cast<pk::menu_item_is_visible_t@>(function() { return (main.src_unit).type == 部队类型_战斗; });
				部队_上位菜单 = pk::add_menu_item(部队);

				// 묘책메뉴 : 귀모계(혼란)
				if (开启_范围混乱)
				{
					pk::menu_item 部队_混乱计;
					部队_混乱计.menu = 部队_上位菜单;
					部队_混乱计.init = pk::unit_menu_item_init_t(init);
					部队_混乱计.get_text = pk::menu_item_get_text_t(getText_部队_混乱计);
					部队_混乱计.get_desc = pk::menu_item_get_desc_t(getDesc_部队_混乱计);
					部队_混乱计.is_visible = pk::menu_item_is_visible_t(isVisible);
					部队_混乱计.is_enabled = pk::menu_item_is_enabled_t(isEnabled_部队_混乱计);
					部队_混乱计.handler = pk::unit_menu_item_handler_t(handler_部队_混乱计);
					pk::add_menu_item(部队_混乱计);
				}

				// 묘책메뉴 : 허보계(위보)
				if (开启_范围伪报)
				{
					pk::menu_item 部队_伪报计;
					部队_伪报计.menu = 部队_上位菜单;
					部队_伪报计.init = pk::unit_menu_item_init_t(init);
					部队_伪报计.get_text = pk::menu_item_get_text_t(getText_部队_伪报计);
					部队_伪报计.get_desc = pk::menu_item_get_desc_t(getDesc_部队_伪报计);
					部队_伪报计.is_visible = pk::menu_item_is_visible_t(isVisible);
					部队_伪报计.is_enabled = pk::menu_item_is_enabled_t(isEnabled_部队_伪报计);
					部队_伪报计.handler = pk::unit_menu_item_handler_t(handler_部队_伪报计);
					pk::add_menu_item(部队_伪报计);
				}

				// 묘책메뉴 : 신화계(화계 + 혼란)
				if (开启_神火计)
				{
					pk::menu_item 部队_神火计;
					部队_神火计.menu = 部队_上位菜单;
					部队_神火计.init = pk::unit_menu_item_init_t(init);
					部队_神火计.get_text = pk::menu_item_get_text_t(getText_部队_神火计);
					部队_神火计.get_desc = pk::menu_item_get_desc_t(getDesc_部队_神火计);
					部队_神火计.is_visible = pk::menu_item_is_visible_t(isVisible_部队_神火计);
					部队_神火计.is_enabled = pk::menu_item_is_enabled_t(isEnabled_部队_神火计);
					部队_神火计.handler = pk::unit_menu_item_handler_t(handler_部队_神火计);
					pk::add_menu_item(部队_神火计);
				}

				// 묘책메뉴 : 팔진도 [아군부대 기력/상태 회복] and [적군부대 기력 감소 + 혼란]
				if (开启_八阵图)
				{
					pk::menu_item 部队_八阵图;
					部队_八阵图.menu = 部队_上位菜单;
					部队_八阵图.init = pk::unit_menu_item_init_t(init);
					部队_八阵图.get_text = pk::menu_item_get_text_t(getText_部队_八阵图);
					部队_八阵图.get_desc = pk::menu_item_get_desc_t(getDesc_部队_八阵图);
					部队_八阵图.is_visible = pk::menu_item_is_visible_t(isVisible_部队_八阵图);
					部队_八阵图.is_enabled = pk::menu_item_is_enabled_t(isEnabled_部队_八阵图);
					部队_八阵图.handler = pk::unit_menu_item_handler_t(handler_部队_八阵图);
					pk::add_menu_item(部队_八阵图);
				}

				// 묘책메뉴 : 허허실실 [적군부대 병력 감소 + 혼란] or [적군부대 병력 감소 + 위보]
				if (开启_虚虚实实)
				{
					pk::menu_item 部队_虚虚实实;
					部队_虚虚实实.menu = 部队_上位菜单;
					部队_虚虚实实.init = pk::unit_menu_item_init_t(init);
					部队_虚虚实实.get_text = pk::menu_item_get_text_t(getText_部队_虚虚实实);
					部队_虚虚实实.get_desc = pk::menu_item_get_desc_t(getDesc_部队_虚虚实实);
					部队_虚虚实实.is_visible = pk::menu_item_is_visible_t(isVisible_部队_虚虚实实);
					部队_虚虚实实.is_enabled = pk::menu_item_is_enabled_t(isEnabled_部队_虚虚实实);
					部队_虚虚实实.handler = pk::unit_menu_item_handler_t(handler_部队_虚虚实实);
					pk::add_menu_item(部队_虚虚实实);
				}

				// 묘책메뉴 : 속전고수 [아군부대 병력/기력 회복] and [적군부대 병량/기력 감소]
				if (开启_速战固守)
				{
					pk::menu_item 部队_速战固守;
					部队_速战固守.menu = 部队_上位菜单;
					部队_速战固守.init = pk::unit_menu_item_init_t(init);
					部队_速战固守.get_text = pk::menu_item_get_text_t(getText_部队_速战固守);
					部队_速战固守.get_desc = pk::menu_item_get_desc_t(getDesc_部队_速战固守);
					部队_速战固守.is_visible = pk::menu_item_is_visible_t(isVisible_部队_速战固守);
					部队_速战固守.is_enabled = pk::menu_item_is_enabled_t(isEnabled_部队_速战固守);
					部队_速战固守.handler = pk::unit_menu_item_handler_t(handler_部队_速战固守);
					pk::add_menu_item(部队_速战固守);
				}

			}

		} // add_menu_unit_order

		void init(pk::unit@ unit, pk::point src_pos)
		{
			@src_unit = @unit;
			src_pos_ = src_pos;
			@src_leader = pk::get_person(src_unit.leader);
		}

		bool isVisible()
		{
			if (pk::is_campaign()) return false;
			if (!setting_ex.mod_set[战术战略妙计_开关]) return false;
			return true;
		}

		//---------------------------------------------------------------------------
		// 묘책메뉴 : 귀모계(혼란) [대규모 혼란] 
		//---------------------------------------------------------------------------

		string getText_部队_混乱计()
		{
			return pk::encode(pk::format("范围混乱 ({})", global_func_最终气力消耗(@src_unit, ENERGY_COST_范围混乱)));
		}

		string getDesc_部队_混乱计()
		{
			if (src_unit.energy < global_func_最终气力消耗(@src_unit, ENERGY_COST_范围混乱))
				return pk::encode("气力不足.");
			else if (src_unit.troops < 部队妙计_兵力条件)
				return pk::encode("兵力不足.");
			else
				return pk::encode(pk::format("使用范围混乱.(气力至少 {} 以上)", global_func_最终气力消耗(@src_unit, ENERGY_COST_范围混乱)));
		}

		bool isEnabled_部队_混乱计()
		{
			if (src_unit.energy < global_func_最终气力消耗(@src_unit, ENERGY_COST_范围混乱)) return false;
			else if (src_unit.troops < 部队妙计_兵力条件) return false;
			return true;
		}

		bool handler_部队_混乱计(pk::point dst_pos)
		{
			global_func_范围混乱(src_pos_, @src_unit, @src_leader);

			// 실행 부대 기력 감소
			pk::add_energy(src_unit, -global_func_最终气力消耗(@src_unit, ENERGY_COST_范围混乱), true);

			// 부대 무장들의 지력 경험치 상승
			pk::add_stat_exp(src_unit, 武将能力_智力, 部队妙计_增加经验);

			// 행동완료
			src_unit.action_done = true;
			if (int(pk::option["San11Option.EnableInfiniteAction"]) != 0)
				src_unit.action_done = false;

			return true;
		}


		//---------------------------------------------------------------------------
		// 묘책메뉴 : 허보계(위보) [대규모 위보] 
		//---------------------------------------------------------------------------

		string getText_部队_伪报计()
		{
			return pk::encode(pk::format("范围伪报 ({})", global_func_最终气力消耗(@src_unit, ENERGY_COST_范围伪报)));
		}

		string getDesc_部队_伪报计()
		{
			if (src_unit.energy < global_func_最终气力消耗(@src_unit, ENERGY_COST_范围伪报))
				return pk::encode("气力不足.");
			else if (src_unit.troops < 部队妙计_兵力条件)
				return pk::encode("兵力不足.");
			else
				return pk::encode(pk::format("使用范围伪报.(气力至少 {} 以上)", global_func_最终气力消耗(@src_unit, ENERGY_COST_范围伪报)));
		}

		bool isEnabled_部队_伪报计()
		{
			if (src_unit.energy < global_func_最终气力消耗(@src_unit, ENERGY_COST_范围伪报)) return false;
			else if (src_unit.troops < 部队妙计_兵力条件) return false;
			return true;
		}

		bool handler_部队_伪报计(pk::point dst_pos)
		{
			global_func_范围伪报(src_pos_, @src_unit, @src_leader);

			// 실행 부대 기력 감소
			pk::add_energy(src_unit, -global_func_最终气力消耗(@src_unit, ENERGY_COST_范围伪报), true);

			// 부대 무장들의 지력 경험치 상승
			pk::add_stat_exp(src_unit, 武将能力_智力, 部队妙计_增加经验);

			// 행동완료
			src_unit.action_done = true;
			if (int(pk::option["San11Option.EnableInfiniteAction"]) != 0)
				src_unit.action_done = false;

			return true;
		}


		//---------------------------------------------------------------------------
		// 묘책메뉴 : 신화계(화계 + 혼란) [대규모 화계 + 혼란] 
		//---------------------------------------------------------------------------

		string getText_部队_神火计()
		{
			return pk::encode(pk::format("神火计 ({})", global_func_最终气力消耗(@src_unit, ENERGY_COST_神火计)));
		}

		string getDesc_部队_神火计()
		{
			if (src_unit.energy < global_func_最终气力消耗(@src_unit, ENERGY_COST_神火计))
				return pk::encode("气力不足.");
			else if (!ch::has_skill(src_unit, 特技_火神))
				return pk::encode("没有特技火神");
			else if (src_unit.troops < 部队妙计_兵力条件)
				return pk::encode("兵力不足.");
			else
				return pk::encode(pk::format("使用神火计.(气力至少 {} 以上)", global_func_最终气力消耗(@src_unit, ENERGY_COST_神火计)));
		}

		bool isVisible_部队_神火计()
		{
			if (pk::is_campaign()) return false;
			if (!setting_ex.mod_set[战术战略妙计_开关]) return false;
			return true;
		}

		bool isEnabled_部队_神火计()
		{
			if (src_unit.energy < global_func_最终气力消耗(@src_unit, ENERGY_COST_神火计)) return false;
			else if (!ch::has_skill(src_unit, 特技_火神)) return false;
			else if (src_unit.troops < 部队妙计_兵力条件) return false;
			return true;
		}

		bool handler_部队_神火计(pk::point dst_pos)
		{
			global_func_神火计(src_pos_, @src_unit, @src_leader);

			// 실행 부대 기력 감소
			pk::add_energy(src_unit, -global_func_最终气力消耗(@src_unit, ENERGY_COST_神火计), true);

			// 부대 무장들의 지력 경험치 상승
			pk::add_stat_exp(src_unit, 武将能力_智力, 部队妙计_增加经验);

			// 행동완료
			src_unit.action_done = true;
			if (int(pk::option["San11Option.EnableInfiniteAction"]) != 0)
				src_unit.action_done = false;

			return true;
		}


		//---------------------------------------------------------------------------
		// 묘책메뉴 : 팔진도 [아군부대 기력/상태 회복] and [적군부대 기력 감소 + 혼란]
		//---------------------------------------------------------------------------

		string getText_部队_八阵图()
		{
			return pk::encode(pk::format("八阵图 ({})", global_func_最终气力消耗(@src_unit, ENERGY_COST_八阵图)));
		}

		string getDesc_部队_八阵图()
		{
			if (src_unit.energy < global_func_最终气力消耗(@src_unit, ENERGY_COST_八阵图))
				return pk::encode("气力不足.");
			else if (!ch::has_skill(src_unit, 特技_神算))
				return pk::encode("没有特技神算.");
			else if (src_unit.troops < 部队妙计_兵力条件)
				return pk::encode("兵力不足.");
			else
				return pk::encode(pk::format("使用八阵图.(气力至少 {} 以上)", global_func_最终气力消耗(@src_unit, ENERGY_COST_八阵图)));
		}

		bool isVisible_部队_八阵图()
		{
			if (pk::is_campaign()) return false;
			if (!setting_ex.mod_set[战术战略妙计_开关]) return false;
			return true;
		}

		bool isEnabled_部队_八阵图()
		{
			if (src_unit.energy < global_func_最终气力消耗(@src_unit, ENERGY_COST_八阵图)) return false;
			else if (!ch::has_skill(src_unit, 特技_神算)) return false;
			else if (src_unit.troops < 部队妙计_兵力条件) return false;
			return true;
		}

		bool handler_部队_八阵图(pk::point dst_pos)
		{
			global_func_八阵图(src_pos_, @src_unit, @src_leader);

			// 실행 부대 기력 감소
			pk::add_energy(src_unit, -global_func_最终气力消耗(@src_unit, ENERGY_COST_八阵图), true);

			// 부대 무장들의 지력 경험치 상승
			pk::add_stat_exp(src_unit, 武将能力_智力, 部队妙计_增加经验);

			// 행동완료
			src_unit.action_done = true;
			if (int(pk::option["San11Option.EnableInfiniteAction"]) != 0)
				src_unit.action_done = false;

			return true;
		}


		//---------------------------------------------------------------------------
		// 묘책메뉴 : 허허실실 [적군부대 병력 감소 + 혼란] or [적군부대 병력 감소 + 위보]
		//---------------------------------------------------------------------------

		string getText_部队_虚虚实实()
		{
			return pk::encode(pk::format("虚虚实实 ({})", global_func_最终气力消耗(@src_unit, ENERGY_COST_虚虚实实)));
		}

		string getDesc_部队_虚虚实实()
		{
			if (src_unit.energy < global_func_最终气力消耗(@src_unit, ENERGY_COST_虚虚实实))
				return pk::encode("气力不足.");
			else if (!ch::has_skill(src_unit, 特技_虚实))
				return pk::encode("没有特技虚实.");
			else if (src_unit.troops < 部队妙计_兵力条件)
				return pk::encode("兵力不足.");
			else
				return pk::encode(pk::format("使用虚虚实实.(气力至少 {} 以上)", global_func_最终气力消耗(@src_unit, ENERGY_COST_虚虚实实)));
		}

		bool isVisible_部队_虚虚实实()
		{
			if (pk::is_campaign()) return false;
			if (!setting_ex.mod_set[战术战略妙计_开关]) return false;
			return true;
		}

		bool isEnabled_部队_虚虚实实()
		{
			if (src_unit.energy < global_func_最终气力消耗(@src_unit, ENERGY_COST_虚虚实实)) return false;
			else if (!ch::has_skill(src_unit, 特技_虚实)) return false;
			else if (src_unit.troops < 部队妙计_兵力条件) return false;
			return true;
		}

		bool handler_部队_虚虚实实(pk::point dst_pos)
		{
			global_func_虚虚实实(src_pos_, @src_unit, @src_leader);

			// 실행 부대 기력 감소
			pk::add_energy(src_unit, -global_func_最终气力消耗(@src_unit, ENERGY_COST_虚虚实实), true);

			// 부대 무장들의 지력 경험치 상승
			pk::add_stat_exp(src_unit, 武将能力_智力, 部队妙计_增加经验);

			// 행동완료
			src_unit.action_done = true;
			if (int(pk::option["San11Option.EnableInfiniteAction"]) != 0)
				src_unit.action_done = false;

			return true;
		}


		//---------------------------------------------------------------------------
		// 묘책메뉴 : 속전고수 [아군부대 병력/기력 회복] and [적군부대 병량/기력 감소]
		//---------------------------------------------------------------------------

		string getText_部队_速战固守()
		{
			return pk::encode(pk::format("速战固守 ({})", global_func_最终气力消耗(@src_unit, ENERGY_COST_速战固守)));
		}

		string getDesc_部队_速战固守()
		{
			if (src_unit.energy < global_func_最终气力消耗(@src_unit, ENERGY_COST_速战固守))
				return pk::encode("气力不足.");
			else if (!ch::has_skill(src_unit, 特技_深谋))
				return pk::encode("没有特技深谋.");
			else if (src_unit.troops < 部队妙计_兵力条件)
				return pk::encode("兵力不足.");
			else
				return pk::encode(pk::format("使用速战固守.(气力至少 {} 以上)", global_func_最终气力消耗(@src_unit, ENERGY_COST_速战固守)));
		}

		bool isVisible_部队_速战固守()
		{
			if (pk::is_campaign()) return false;
			if (!setting_ex.mod_set[战术战略妙计_开关]) return false;
			return true;
		}

		bool isEnabled_部队_速战固守()
		{
			if (src_unit.energy < global_func_最终气力消耗(@src_unit, ENERGY_COST_速战固守)) return false;
			else if (!ch::has_skill(src_unit, 特技_深谋)) return false;
			else if (src_unit.troops < 部队妙计_兵力条件) return false;
			return true;
		}

		bool handler_部队_速战固守(pk::point dst_pos)
		{
			global_func_速战固守(src_pos_, @src_unit, @src_leader);

			// 실행 부대 기력 감소
			pk::add_energy(src_unit, -global_func_最终气力消耗(@src_unit, ENERGY_COST_速战固守), true);

			// 부대 무장들의 지력 경험치 상승
			pk::add_stat_exp(src_unit, 武将能力_智力, 部队妙计_增加经验);

			// 행동완료
			src_unit.action_done = true;
			if (int(pk::option["San11Option.EnableInfiniteAction"]) != 0)
				src_unit.action_done = false;

			return true;
		}


	} // class Main

	Main main;
}