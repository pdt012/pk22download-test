

namespace 据点耐久重设
{
    class Main
    {

        pk::building @building_;

        int city_hp_rise = 2000;
        pk::building @city_;

        int port_hp_rise = 2000;
        pk::building @port_;

        // 重设关隘的耐久
        int gate_hp = 10000;
        pk::building @gate_;

        // 关隘列表
        array<int> gate_list = {新据点_轩辕关, 新据点_绵竹关, 新据点_剑阁关, 新据点_阳平关, 新据点_函谷关, 新据点_虎牢关, 新据点_雁门};

        Main()
        {
            pk::bind(102, pk::trigger102_t(callback));
        }

        void callback()
        {
            if (pk::get_scenario().loaded)
                return;

            update();

            return;
        }

        void update()
        {
            if (city_hp_rise > 0)
            {
                for (int i = 0; i < 城市_末; i++)
                {
                    @city_ = pk::get_building(i);

                    pk::building_to_city(pk::get_building(i)).max_hp = pk::building_to_city(pk::get_building(i)).max_hp + city_hp_rise;
                    city_.hp = city_.hp + city_hp_rise;

                    city_.update();
                }
            }

            for (int i = 城市_末; i < 据点_末; i++)
            {
                if (gate_list.find(i) > 0)
                {
                    @gate_ = pk::get_building(i);
                    if (gate_.facility == 设施_关隘)
                    {

                        pk::building_to_gate(pk::get_building(i)).max_hp = gate_hp;
                        gate_.hp = gate_hp;

                        gate_.update();
                    }
                }
                else
                {
                    @port_ = pk::get_building(i);
                    if (port_.facility == 设施_港口)
                    {
                        pk::building_to_port(pk::get_building(i)).max_hp = pk::building_to_port(pk::get_building(i)).max_hp + port_hp_rise;
                        port_.hp = port_.hp + port_hp_rise;

                        port_.update();
                    }
                }
            }
        }

    }

    Main main;
}