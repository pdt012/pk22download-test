﻿// ## 2023/07/26 # 黑店小小二 # get_effic, set_effic 方法增加对筑城开支类型。
// ## 2023/06/19 # 黑店小小二 # 结构体增加get_effic, set_effic, set_choose, get_choose, get_choose_name 方法
// ## 2022/05/03 # 铃 # 新做结构体，用于存储自动化内政的临时信息和新城防系统的计算 ##

namespace 据点内政结构体
{
	/// KEY index 的类型是 (uint16),所以最高支持65535,
	const int KEY_索引_追加_据点起始 = 0; // 索引追加建筑结构体 uint32数量为30个，据点共86个。总计2580

	const int KEY = pk::hash("新据点结构体");

	class Main
	{
		Main()
		{
			pk::bind(102, pk::trigger102_t(剧本初始化_结构体_信息读取), -1);
			pk::bind(105, pk::trigger105_t(儲存_结构体_信息储存));
			pk::bind(111, pk::trigger111_t(onTurnStart));
		}

		void 儲存_结构体_信息储存(int file_id) // 儲存
		{
			for (int i = 0; i < 据点_末; i++)
			{
				building_ex[i].update(i);
				for (int j = 0; j < (新据点结构体_uint32数); j++)
					pk::store(KEY, (KEY_索引_追加_据点起始 + (i * 新据点结构体_uint32数 + j)), building_ex_info_temp[j][i]);
			}
		}

		void 剧本初始化_结构体_信息读取()
		{
			// 重新开始游戏时，初始化数据
			if (!pk::get_scenario().loaded)
			{
				// pk::trace("first day");
				for (int i = 0; i < 据点_末; ++i)
				{
					building_ex[i].boat_person = -1;
					building_ex[i].weapon_person = -1;
					building_ex[i].horse_person = -1;
					building_ex[i].punch_person = -1;
					building_ex[i].recruit_person = -1;
					building_ex[i].inspections_person = -1;
					building_ex[i].drill_person = -1;

					building_ex[i].porder_effic = 0;	 // 巡查开支
					building_ex[i].train_effic = 0;		 // 训练开支
					building_ex[i].city_defense = 0;	 // 额外城防
					building_ex[i].repair_effic = 0;	 // 筑城开支
					building_ex[i].troops_effic = 0;	 // 征兵开支
					building_ex[i].weapon_effic = 0;	 // 兵装开支
					building_ex[i].building_revenue = 0; // 据点下一旬收入
					building_ex[i].building_policy = 0;	 // 据点方针
					building_ex[i].policy_expense = 0;	 // 据点方针开支比例
					building_ex[i].weapon_expense = 0;	 // 兵装维护费
					building_ex[i].troops_expense = 0;	 // 军饷

					building_ex[i].porder_gain = 0;	 // 巡查增量
					building_ex[i].train_gain = 0;	 // 训练增量
					building_ex[i].repair_gain = 0;	 // 筑城增量
					building_ex[i].troops_gain = 0;	 // 征兵增量
					building_ex[i].spear_gain = 0;	 // 枪增量
					building_ex[i].halberd_gain = 0; // 戟增量
					building_ex[i].bow_gain = 0;	 // 弩增量
					building_ex[i].horse_gain = 0;	 // 马增量
					building_ex[i].well_gain = 0;	 // 井栏增量
					building_ex[i].punch_gain = 0;	 // 冲车增量
					building_ex[i].boat_gain = 0;	 // 船增量

					building_ex[i].well = 0;		  // 井栏量(用于存储临时产量)
					building_ex[i].punch = 0;		  // 冲车量(用于存储临时产量)
					building_ex[i].boat = 0;		  // 舰船量(用于存储临时产量)
					building_ex[i].weapon_choose = 0; // 选择哪种兵装
					building_ex[i].arms_choose = 0;	  // 选择哪种工器
					building_ex[i].horse_effic = 0;	  // 马开支
					building_ex[i].punch_effic = 0;	  // 攻具开支

					building_ex[i].boat_effic = 0; // 船开支

					building_ex[i].rank_type = {内政自动化_招兵, 内政自动化_兵装, 内政自动化_巡查, 内政自动化_训练, 内政自动化_攻具, 内政自动化_育马, 内政自动化_造船};
				}
			}

			if (pk::get_scenario().loaded)
			{
				for (int i = 0; i < 据点_末; i++)
				{
					for (int j = 0; j < (新据点结构体_uint32数); j++)
						building_ex_info_temp[j][i] = uint32(pk::load(KEY, (KEY_索引_追加_据点起始 + (i * 新据点结构体_uint32数 + j)), 0));
					BuildingInfo base_t(i);
					building_ex[i] = base_t;
					// building_ex[i].porder = 0;
					// building_ex[i].train = 0;
				}
			}
		}

		void onTurnStart(pk::force @force)
		{
			for (int i = 0; i < 据点_末; ++i)
			{
				int force_id = pk::get_building(i).get_force_id();
				if (force_id == force.get_id())
				{

					// 此处为新的回合重新赋值用

					// base_ex[i].mobilize_done = false;
				}
			}
		}
	}

	Main main;
}
const int 新据点结构体_uint32数 = 60;

const int 内政自动化_巡查 = 0;
const int 内政自动化_训练 = 1;
const int 内政自动化_招兵 = 2;
const int 内政自动化_兵装 = 3;
const int 内政自动化_育马 = 4;
const int 内政自动化_攻具 = 5;
const int 内政自动化_造船 = 6;
const int 内政自动化_筑城 = 7;

array<array<uint32>> building_ex_info_temp(新据点结构体_uint32数, array<uint32>(据点_末, uint32(0)));
array<BuildingInfo> building_ex(据点_末);

// 存档时将结构体转为数组再存
// 读档时将数组转换为结构体读取

/// struct 和 class没有本质区别，只是class默认类外不可访问。不过pk2.2class是可以类外访问的
// 将多个信息组合成两个int64

class BuildingInfo
{
	uint32 porder_effic;	 // 巡查开支
	uint32 train_effic;		 // 训练开支
	uint32 city_defense;	 // 额外城防
	uint32 repair_effic;	 // 筑城开支
	uint32 troops_effic;	 // 征兵开支
	uint32 weapon_effic;	 // 兵装开支
	uint32 building_revenue; // 据点下一旬收入
	uint32 building_policy;	 // 据点方针
	uint32 policy_expense;	 // 据点方针开支比例
	uint32 weapon_expense;	 // 兵装维护费
	uint32 troops_expense;	 // 军饷

	uint32 porder_gain;	 // 巡查增量
	uint32 train_gain;	 // 训练增量
	uint32 repair_gain;	 // 筑城增量
	uint32 troops_gain;	 // 征兵增量
	uint32 spear_gain;	 // 枪增量
	uint32 halberd_gain; // 戟增量
	uint32 bow_gain;	 // 弩增量
	uint32 horse_gain;	 // 马增量
	uint32 well_gain;	 // 井栏增量
	uint32 punch_gain;	 // 冲车增量
	uint32 boat_gain;	 // 船增量

	uint32 well;		  // 井栏量(用于存储临时产量)
	uint32 punch;		  // 冲车量(用于存储临时产量)
	uint32 boat;		  // 舰船量(用于存储临时产量)
	uint32 weapon_choose; // 选择哪种兵装
	uint32 arms_choose;	  // 选择哪种工器

	int weapon_person;	// 兵装执行官
	int horse_person;	// 马执行官
	int punch_person;	// 攻具执行官
	uint32 horse_effic; // 马开支
	uint32 punch_effic; // 攻具开支

	int recruit_person;		// 征兵执行官
	int inspections_person; // 巡查执行官
	int drill_person;		// 训练执行官

	uint32 boat_effic; // 船开支
	int boat_person;   // 造船执行官

	array<int> rank_type = {内政自动化_招兵, 内政自动化_兵装, 内政自动化_巡查, 内政自动化_训练, 内政自动化_攻具, 内政自动化_育马, 内政自动化_造船};

	// 初始化
	BuildingInfo(int base_id)
	{
		get_info(base_id);
	}

	BuildingInfo() {}

	void get_info(int base_id)
	{
		fromInt32_0(building_ex_info_temp[0][base_id]);
		fromInt32_1(building_ex_info_temp[1][base_id]);
		fromInt32_2(building_ex_info_temp[2][base_id]);
		fromInt32_3(building_ex_info_temp[3][base_id]);
		fromInt32_4(building_ex_info_temp[4][base_id]);
		fromInt32_5(building_ex_info_temp[5][base_id]);
		fromInt32_6(building_ex_info_temp[6][base_id]);
		fromInt32_7(building_ex_info_temp[7][base_id]);
		fromInt32_8(building_ex_info_temp[8][base_id]);
		fromInt32_9(building_ex_info_temp[9][base_id]);
		fromInt32_10(building_ex_info_temp[10][base_id]);
		fromInt32_11(building_ex_info_temp[11][base_id]);
		fromInt32_12(building_ex_info_temp[12][base_id]);
		fromInt32_13(building_ex_info_temp[13][base_id]);
		fromInt32_14(building_ex_info_temp[14][base_id]);
		fromInt32_15(building_ex_info_temp[15][base_id]);
		fromInt32_16(building_ex_info_temp[16][base_id]);
		fromInt32_17(building_ex_info_temp[17][base_id]);
		fromInt32_18(building_ex_info_temp[18][base_id]);
		fromInt32_19(building_ex_info_temp[19][base_id]);
		fromInt32_20(building_ex_info_temp[20][base_id]);
		fromInt32_21(building_ex_info_temp[21][base_id]);
		fromInt32_22(building_ex_info_temp[22][base_id]);
		fromInt32_23(building_ex_info_temp[23][base_id]);
		fromInt32_24(building_ex_info_temp[24][base_id]);
		fromInt32_25(building_ex_info_temp[25][base_id]);
		fromInt32_26(building_ex_info_temp[26][base_id]);

		fromInt32_27(building_ex_info_temp[27][base_id]);
		fromInt32_28(building_ex_info_temp[28][base_id]);
		fromInt32_29(building_ex_info_temp[29][base_id]);
		fromInt32_30(building_ex_info_temp[30][base_id]);
		fromInt32_31(building_ex_info_temp[31][base_id]);
		fromInt32_32(building_ex_info_temp[32][base_id]);
		fromInt32_33(building_ex_info_temp[33][base_id]);
		fromInt32_34(building_ex_info_temp[34][base_id]);
		fromInt32_35(building_ex_info_temp[35][base_id]);
		fromInt32_36(building_ex_info_temp[36][base_id]);
		fromInt32_37(building_ex_info_temp[37][base_id]);
	}

	void update(int base_id)
	{
		building_ex_info_temp[0][base_id] = toInt32_0();
		building_ex_info_temp[1][base_id] = toInt32_1();
		building_ex_info_temp[2][base_id] = toInt32_2();
		building_ex_info_temp[3][base_id] = toInt32_3();
		building_ex_info_temp[4][base_id] = toInt32_4();
		building_ex_info_temp[5][base_id] = toInt32_5();
		building_ex_info_temp[6][base_id] = toInt32_6();
		building_ex_info_temp[7][base_id] = toInt32_7();
		building_ex_info_temp[8][base_id] = toInt32_8();
		building_ex_info_temp[9][base_id] = toInt32_9();
		building_ex_info_temp[10][base_id] = toInt32_10();
		building_ex_info_temp[11][base_id] = toInt32_11();
		building_ex_info_temp[12][base_id] = toInt32_12();
		building_ex_info_temp[13][base_id] = toInt32_13();
		building_ex_info_temp[14][base_id] = toInt32_14();
		building_ex_info_temp[15][base_id] = toInt32_15();
		building_ex_info_temp[16][base_id] = toInt32_16();
		building_ex_info_temp[17][base_id] = toInt32_17();
		building_ex_info_temp[18][base_id] = toInt32_18();
		building_ex_info_temp[19][base_id] = toInt32_19();
		building_ex_info_temp[20][base_id] = toInt32_20();
		building_ex_info_temp[21][base_id] = toInt32_21();
		building_ex_info_temp[22][base_id] = toInt32_22();
		building_ex_info_temp[23][base_id] = toInt32_23();
		building_ex_info_temp[24][base_id] = toInt32_24();
		building_ex_info_temp[25][base_id] = toInt32_25();
		building_ex_info_temp[26][base_id] = toInt32_26();

		building_ex_info_temp[27][base_id] = toInt32_27();
		building_ex_info_temp[28][base_id] = toInt32_28();
		building_ex_info_temp[29][base_id] = toInt32_29();
		building_ex_info_temp[30][base_id] = toInt32_30();
		building_ex_info_temp[31][base_id] = toInt32_31();

		building_ex_info_temp[32][base_id] = toInt32_32();
		building_ex_info_temp[33][base_id] = toInt32_33();
		building_ex_info_temp[34][base_id] = toInt32_34();
		building_ex_info_temp[35][base_id] = toInt32_35();
		building_ex_info_temp[36][base_id] = toInt32_36();
		building_ex_info_temp[37][base_id] = toInt32_37();
	}

	/*get_effic, set_effic, set_choose, get_choose */

	void set_charge(int person_id, int 内政自动化_项目)
	{
		switch (内政自动化_项目)
		{
		case 内政自动化_巡查:
			inspections_person = person_id;
			break;
		case 内政自动化_训练:
			drill_person = person_id;
			break;
		case 内政自动化_招兵:
			recruit_person = person_id;
			break;
		case 内政自动化_兵装:
			weapon_person = person_id;
			break;
		case 内政自动化_育马:
			horse_person = person_id;
			break;
		case 内政自动化_攻具:
			punch_person = person_id;
			break;
		case 内政自动化_造船:
			boat_person = person_id;
			break;
		}
	}

	int get_charge(int 内政自动化_项目)
	{
		int person_id = -1;
		switch (内政自动化_项目)
		{
		case 内政自动化_巡查:
			person_id = inspections_person;
			break;
		case 内政自动化_训练:
			person_id = drill_person;
			break;
		case 内政自动化_招兵:
			person_id = recruit_person;
			break;
		case 内政自动化_兵装:
			person_id = weapon_person;
			break;
		case 内政自动化_育马:
			person_id = horse_person;
			break;
		case 内政自动化_攻具:
			person_id = punch_person;
			break;
		case 内政自动化_造船:
			person_id = boat_person;
			break;
		}
		return person_id;
	}

	string get_charge_name(int 内政自动化_项目)
	{
		string charge_name = '';
		switch (内政自动化_项目)
		{
		case 内政自动化_巡查:
			charge_name = '巡查执行官';
			break;
		case 内政自动化_训练:
			charge_name = '训练执行官';
			break;
		case 内政自动化_招兵:
			charge_name = '招兵执行官';
			break;
		case 内政自动化_兵装:
			charge_name = '兵装生产官';
			break;
		case 内政自动化_育马:
			charge_name = '育马执行官';
			break;
		case 内政自动化_攻具:
			charge_name = '攻具生产官';
			break;
		case 内政自动化_造船:
			charge_name = '造船执行官';
			break;
		}
		return charge_name;
	}

	int get_effic(int 内政自动化_项目)
	{
		int effic = 0;
		switch (内政自动化_项目)
		{
		case 内政自动化_巡查:
			effic = porder_effic;
			break;
		case 内政自动化_训练:
			effic = train_effic;
			break;
		case 内政自动化_招兵:
			effic = troops_effic;
			break;
		case 内政自动化_兵装:
			effic = weapon_effic;
			break;
		case 内政自动化_育马:
			effic = horse_effic;
			break;
		case 内政自动化_攻具:
			effic = punch_effic;
			break;
		case 内政自动化_造船:
			effic = boat_effic;
			break;
		case 内政自动化_筑城:
			effic = repair_effic;
			break;
		}
		return effic;
	}

	void set_effic(int effic, int 内政自动化_项目)
	{
		switch (内政自动化_项目)
		{
			case 内政自动化_巡查:
				porder_effic = effic;
				break;
			case 内政自动化_训练:
				train_effic = effic;
				break;
			case 内政自动化_招兵:
				troops_effic = effic;
				break;
			case 内政自动化_兵装:
				weapon_effic = effic;
				break;
			case 内政自动化_育马:
				horse_effic = effic;
				break;
			case 内政自动化_攻具:
				punch_effic = effic;
				break;
			case 内政自动化_造船:
				boat_effic = effic;
				break;
			case 内政自动化_筑城:
				repair_effic = effic;
				break;
		}
	}

	int get_choose(int 内政自动化_项目)
	{
		int choose = -1;
		switch (内政自动化_项目)
		{
			case 内政自动化_兵装:
				choose = weapon_effic;
				break;
			case 内政自动化_攻具:
				choose = punch_effic;
				break;
			default:
				choose = -1;
		}
		return choose;
	}
	void set_choose(int choose, int 内政自动化_项目)
	{
		switch (内政自动化_项目)
		{
			case 内政自动化_兵装:
				weapon_choose = choose;
				break;
			case 内政自动化_攻具:
				arms_choose = choose;
				break;
		}
	}

	string get_choose_name(int base_id, int 内政自动化_项目)
	{
		if (内政自动化_项目 == 内政自动化_兵装)
		{
			array<string> weapon_list = { '平衡', '枪', '戟', '弩' };
			return weapon_list[weapon_choose];
		}
		else if (内政自动化_项目 == 内政自动化_攻具)
		{
			pk::building@ building = pk::get_building(base_id);
			array<string> arms_list = { '平衡', pk::has_tech(building, 技巧_投石开发) ? '投石' : '冲车' , pk::has_tech(building, 技巧_木兽开发) ? '木兽' : '井栏' };
			return arms_list[arms_choose];
		}
		return '';
	}

	// 储存的时候应是PersonInfo.toInt32_0,作为一个单位储存
	uint32 toInt32_0(void)
	{
		return porder_effic;
	}

	uint32 toInt32_1(void)
	{
		return train_effic;
	}
	uint32 toInt32_2(void)
	{
		return city_defense;
	}

	uint32 toInt32_3(void)
	{
		return repair_effic;
	}
	uint32 toInt32_4(void)
	{
		return troops_effic;
	}

	uint32 toInt32_5(void)
	{
		return weapon_effic;
	}

	uint32 toInt32_6(void)
	{
		return building_revenue;
	}

	uint32 toInt32_7(void)
	{
		return building_policy;
	}

	uint32 toInt32_8(void)
	{
		return policy_expense;
	}

	uint32 toInt32_9(void)
	{
		return weapon_expense;
	}

	uint32 toInt32_10(void)
	{
		return troops_expense;
	}

	uint32 toInt32_11(void)
	{
		return porder_gain;
	}

	uint32 toInt32_12(void)
	{
		return train_gain;
	}

	uint32 toInt32_13(void)
	{
		return repair_gain;
		;
	}

	uint32 toInt32_14(void)
	{
		return troops_gain;
	}

	uint32 toInt32_15(void)
	{
		return spear_gain;
	}
	uint32 toInt32_16(void)
	{
		return halberd_gain;
	}

	uint32 toInt32_17(void)
	{
		return bow_gain;
	}

	uint32 toInt32_18(void)
	{
		return horse_gain;
	}

	uint32 toInt32_19(void)
	{
		return well_gain;
	}

	uint32 toInt32_20(void)
	{
		return punch_gain;
	}

	uint32 toInt32_21(void)
	{
		return boat_gain;
	}
	uint32 toInt32_22(void)
	{
		return well;
	}

	uint32 toInt32_23(void)
	{
		return punch;
	}

	uint32 toInt32_24(void)
	{
		return boat;
	}
	uint32 toInt32_25(void)
	{
		return weapon_choose;
	}

	uint32 toInt32_26(void)
	{
		return arms_choose;
	}

	uint32 toInt32_27(void)
	{
		return weapon_person;
	}
	uint32 toInt32_28(void)
	{
		return horse_person;
	}

	uint32 toInt32_29(void)
	{
		return punch_person;
	}

	uint32 toInt32_30(void)
	{
		return horse_effic;
	}
	uint32 toInt32_31(void)
	{
		return punch_effic;
	}
	uint32 toInt32_32(void)
	{
		return recruit_person;
	}

	uint32 toInt32_33(void)
	{
		return inspections_person;
	}
	uint32 toInt32_34(void)
	{
		return drill_person;
	}

	uint32 toInt32_35(void)
	{
		return boat_effic;
	}

	uint32 toInt32_36(void)
	{
		uint32 x = 0;
		for (int i = 0; i < int(rank_type.length); i += 1)
		{
			uint8 temp = rank_type[i];
			x += temp << (i * 4);
		}
		return x;
	}

	uint32 toInt32_37(void)
	{
		return boat_person;
	}

	// 读取后解析
	void fromInt32_0(uint32 x)
	{
		porder_effic = x;
	}

	void fromInt32_1(uint32 x)
	{
		train_effic = x;
	}
	void fromInt32_2(uint32 x)
	{
		city_defense = x;
	}

	void fromInt32_3(uint32 x)
	{
		repair_effic = x;
	}
	void fromInt32_4(uint32 x)
	{
		troops_effic = x;
	}

	void fromInt32_5(uint32 x)
	{
		weapon_effic = x;
	}

	void fromInt32_6(uint32 x)
	{
		building_revenue = x;
	}

	void fromInt32_7(uint32 x)
	{
		building_policy = x;
	}
	void fromInt32_8(uint32 x)
	{
		policy_expense = x;
	}

	void fromInt32_9(uint32 x)
	{
		weapon_expense = x;
	}
	void fromInt32_10(uint32 x)
	{
		troops_expense = x;
	}
	void fromInt32_11(uint32 x)
	{
		porder_gain = x;
	}
	void fromInt32_12(uint32 x)
	{
		train_gain = x;
	}
	void fromInt32_13(uint32 x)
	{
		repair_gain = x;
	}
	void fromInt32_14(uint32 x)
	{
		troops_gain = x;
	}
	void fromInt32_15(uint32 x)
	{
		spear_gain = x;
	}
	void fromInt32_16(uint32 x)
	{
		halberd_gain = x;
	}
	void fromInt32_17(uint32 x)
	{
		bow_gain = x;
	}
	void fromInt32_18(uint32 x)
	{
		horse_gain = x;
	}
	void fromInt32_19(uint32 x)
	{
		well_gain = x;
	}

	void fromInt32_20(uint32 x)
	{
		punch_gain = x;
	}

	void fromInt32_21(uint32 x)
	{
		boat_gain = x;
	}
	void fromInt32_22(uint32 x)
	{
		well = x;
	}
	void fromInt32_23(uint32 x)
	{
		punch = x;
	}
	void fromInt32_24(uint32 x)
	{
		boat = x;
	}

	void fromInt32_25(uint32 x)
	{
		weapon_choose = x;
	}

	void fromInt32_26(uint32 x)
	{
		arms_choose = x;
	}

	void fromInt32_27(uint32 x)
	{
		weapon_person = x;
	}
	void fromInt32_28(uint32 x)
	{
		horse_person = x;
	}
	void fromInt32_29(uint32 x)
	{
		punch_person = x;
	}

	void fromInt32_30(uint32 x)
	{
		horse_effic = x;
	}

	void fromInt32_31(uint32 x)
	{
		punch_effic = x;
	}

	void fromInt32_32(uint32 x)
	{
		recruit_person = x;
	}

	void fromInt32_33(uint32 x)
	{
		inspections_person = x;
	}

	void fromInt32_34(uint32 x)
	{
		drill_person = x;
	}

	void fromInt32_35(uint32 x)
	{
		boat_effic = x;
	}

	void fromInt32_36(uint32 x)
	{
		for (int i = 0; i < int(rank_type.length); i += 1)
		{
			rank_type[i] = ((x << (28 - i * 4)) >> 28);
		}
	}

	void fromInt32_37(uint32 x)
	{
		boat_person = x;
	}
}
