﻿// ## 2023/05/11 # 江东新风 # 仅在执政官功能开启时显示和执行 ##
// ## 2023/05/10 # 江东新风 # 重置条件优化 ##
// ## 2022/08/08 # 铃 # 基于全面自动化功能,设置选择执行官的面板

namespace 内政自动设置
{

	const bool 调试模式 = false;
	;
	const bool 开启完全自动内政 = true;

	const int KEY = pk::hash("内政自动化设置");

	class Main
	{

		pk::func168_t @prev_callback_168;

		///	<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

		Main()
		{
			/// 内政自动化的显示信息
			// 当鼠标放在据点上时会在左上角自动显示内政自动化的设定信息
			pk::bind(120, pk::trigger120_t(信息显示_城市内政自动化_信息));
			pk::bind(112, pk::trigger112_t(回合结束_状态复位));
			pk::bind(112, pk::trigger112_t(回合结束_AI设定));

			@prev_callback_168 = cast<pk::func168_t @>(pk::get_func(168));
			pk::reset_func(168);
			pk::set_func(168, pk::func168_t(func168));
		}

		void func168(pk::building @base, pk::unit @attacker)
		{
			if (ch::get_auto_affairs_status()) return;
			prev_callback_168(base, attacker);

			// 거점 급변사태 예외 처리 : 거점 제압 시
			城市攻占_状态复位(base, attacker);
		}

		void 回合结束_AI设定(pk::force @force)
		{
			if (!ch::get_auto_affairs_status()) return;
			if (pk::is_player_controlled(force))
				return;

			if (force.get_id() > 41)
				return;

			for (int i = 0; i < 据点_末; i++)
			{
				int building_id = i;
				pk::building @building = pk::get_building(i);

				int building_force_id = building.get_force_id();
				int force_id = force.get_id();

				// pk::message_box(pk::encode(pk::format("报告,\x1b[2x{}\x1b[0x的兵器产量{}", pk::decode(pk::get_name(building)), 123)));

				// if (pk::get_person(building_p.recruit_person).service != building.get_id())  building_p.recruit_person = -1;
				if ((!building.is_player()) and building_force_id == force_id)
				{
					pk::building @base = pk::get_building(building_id);
					pk::list<pk::person @> person_list; // = pk::get_person_list(pk::get_building(city_id), pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般));

					BuildingInfo @building_p = @building_ex[base.get_id()];
					array<int> rank_type = building_p.rank_type;

					for (int index = 0; index < int(rank_type.length); index += 1)
					{
						int type = rank_type[index];
						// 获取ai武将排序
						building_p.set_charge(-1, type);
						person_list = get_person_list(base, type);
						if (person_list.count > 0)
						{
							for (int p = 0; p < person_list.count; p += 1)
							{
								pk::person @person = person_list[p];
								if (index == 0)
								{
									building_p.set_charge(person.get_id(), type);
									break;
								}
								else
								{
									// if (building.get_id() == 13)
									//  pk::trace(pk::format("城市id：{},城市名:{},人物ID:{},type:{}", building.get_id(), pk::decode(pk::get_name(building)), person.get_id(), type));

									bool checkout_role = true;
									for (int rank = 0; rank < index; rank += 1)
									{
										checkout_role = (checkout_role and person.get_id() != building_p.get_charge(rank_type[rank]));
									}
									if (checkout_role)
									{
										building_p.set_charge(person.get_id(), type);
										// pk::trace(pk::format("城市id：{},城市名:{},人物ID:{},type:{}", building.get_id(),pk::decode(pk::get_name(building)),person.get_id(),building_p.well_gain,type));
										break;
									}
								}
							}
						}
					}
					// pk::message_box(pk::encode(pk::format("报告,\x1b[2x{}\x1b[0x的兵器产量{}", pk::decode(pk::get_name(building)),pk::get_name(pk::get_person(building_p.recruit_person)))));
				}
			}
		}

		void 城市攻占_状态复位(pk::building @building, pk::unit @attacker)
		{

			int building_id = building.get_id();
			if (building_id >= 据点_末)
				return;

			// BaseInfo @building_p = @base_ex[building_id];
			BuildingInfo @building_p = @building_ex[building_id];

			if ((attacker.is_player() or 调试模式))
			{
				array<int> rank_type = building_p.rank_type;
				for (int type = 0; type < int(rank_type.length); type += 1)
				{
					int person_id = building_p.get_charge(rank_type[type]);
					if (person_id == -1)
						continue;

					pk::person @person = pk::get_person(person_id);

					if (pk::is_valid_person_id(person_id))
					{

						if (person.service != building_id or !pk::is_alive(person) or person.mibun == 身份_俘虏 or person.mibun == 身份_在野 or person.mibun == 身份_死亡)
						{
							// pk::trace("temp" + person.get_force_id() + "temp" + building.get_force_id());

							if (building.is_player() and person.get_force_id() == building.get_force_id())
								pk::history_log(building.pos, pk::get_force(building.get_force_id()).color, pk::encode(pk::format("{}的\x1b[1x{}\x1b[16x{}\x1b[1x被取消\x1b[1x", pk::decode(pk::get_name(building)), building_p.get_charge_name(rank_type[type]), pk::decode(pk::get_name(person)))));
							building_p.set_charge(-1, rank_type[type]);
						}
					}
				}
			}
		}

		void 回合结束_状态复位(pk::force @force)
		{
			if (!ch::get_auto_affairs_status()) return;
			// if (!force.is_player()) return;	// 计算机势力除外 컴퓨터 세력 제외
			
			for (int i = 0; i < 据点_末; i++)
			{
				int building_id = i;
				pk::building @building = pk::get_building(i);
				// BaseInfo @building_p = @base_ex[building_id];
				BuildingInfo @building_p = @building_ex[building_id];

				if ((building.is_player() or 调试模式))
				{
					array<int> rank_type = building_p.rank_type;
					for (int type = 0; type < int(rank_type.length); type += 1)
					{
						int person_id = building_p.get_charge(rank_type[type]);
						if (!pk::is_valid_person_id(person_id))
							continue;

						pk::person @person = pk::get_person(person_id);
						int force_id = person.get_force_id();
						if (!pk::is_normal_force(force_id) or person.service != building_id or !pk::check_mibun(person, pk::mibun_flags(身份_一般, 身份_太守, 身份_都督, 身份_君主)))
						{
							pk::trace("temp" + person.get_force_id() + "temp" + building.get_force_id());

							if (building.is_player() and person.get_force_id() == building.get_force_id())
								pk::history_log(building.pos, pk::get_force(building.get_force_id()).color, pk::encode(pk::format("{}的\x1b[1x{}\x1b[16x{}\x1b[1x被取消\x1b[1x", pk::decode(pk::get_name(building)), building_p.get_charge_name(rank_type[type]), pk::decode(pk::get_name(person)))));
							building_p.set_charge(-1, rank_type[type]);

							switch (rank_type[type])
							{
							case 内政自动化_巡查:
								building_p.porder_effic = 0;
								break;
							case 内政自动化_训练:
								building_p.train_effic = 0;
								break;
							case 内政自动化_招兵:
								building_p.troops_effic = 0;
								break;
							case 内政自动化_兵装:
								building_p.weapon_effic = 0;
								break;
							case 内政自动化_育马:
								building_p.horse_effic = 0;
								break;
							case 内政自动化_攻具:
								building_p.punch_effic = 0;
								break;
							case 内政自动化_造船:
								building_p.boat_effic = 0;
								break;
							}
						}
					}
				}
			}
		}

		// 内政自动化显示信息
		void 信息显示_城市内政自动化_信息()
		{
			if (!ch::get_auto_affairs_status()) return;
			// 光标指引的坐标
			pk::point cursor_pos = pk::get_cursor_hex_pos();
			if (!pk::is_valid_pos(cursor_pos))
				return;

			// 光标上指示的建筑物
			pk::building @building = pk::get_building(cursor_pos);
			if (building is null)
				return;

			// pk::building @city = @building;
			//  确认城市坐标是否有效
			pk::point building_pos = building.get_pos();
			if (!pk::is_valid_pos(building_pos))
				return;

			// 确认城市是否是玩家的，是否归玩家控制，及是否城市控制势力id是当前回合的执行势力id
			// if (!building.is_player() and !调试模式)
			// 	return;
			// if (!pk::is_player_controlled(building) and !调试模式)
			// 	return;
			// if (building.get_force_id() != pk::get_current_turn_force_id() and !调试模式)
			// 	return;
			if (building.get_id() == -1)
				return;
			if (building.get_id() > 据点_末)
				return;

			// 显示内政自动化信息
			BaseInfo @base_t = @base_ex[building.get_id()];
			func_内政自动化_信息显示(building, pk::get_current_turn_force_id());

			// bool调试 =true;
			// if ((调试 or !pk::is_fog_set() or building.is_player() or pk::get_player_count() == 0 or (pk::is_fog_set() and base_t.spy_lv[pk::get_current_turn_force_id()] > 0)))
			// {
			// 	func_内政自动化_信息显示(building, pk::get_current_turn_force_id());
			// }
		}

		///	<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

		void func_内政自动化_信息显示(pk::building @building, int force_id)
		{
			string building_name = pk::decode(pk::get_name(building));
			string title = pk::format("自动化内政(\x1b[1x{}\x1b[0x)", building_name);
			int building_id = building.get_id();
			pk::point src_pos = building.pos;
			int city_id = pk::get_city_id(building.pos);
			pk::city @city = pk::get_city(city_id);

			//内政自动::calculate_building(pk::get_building(building_id));

			BuildingInfo @building_p = @building_ex[building.get_id()];

			// pk::history_log(building.pos, pk::get_force(building.get_force_id()).color, pk::encode(pk::format("\x1b[1x{}：\x1b[1x{}", pk::decode(pk::get_name(building)), building_p.produce_effic,0)));
			// pk::history_log(building.pos, pk::get_force(building.get_force_id()).color, pk::encode(pk::format("\x1b[1x{}：\x1b[1x{}", pk::decode(pk::get_name(building)), building_p.building_revenue,0)));

			int 高度 = 0;
			int 右移 = 245;
			int 文字位移 = 110;
			int 名字位移 = 38;
			string building_policy_info;
			string building_battle_info;
			string weapon_type;
			switch (building_p.building_policy)
			{
			case 0:
				building_policy_info = "无";
				break;
			case 1:
				building_policy_info = "积极备战";
				break;
			case 2:
				building_policy_info = "后方生产";
				break;
			case 3:
				building_policy_info = "均衡分配";
				break;
			case 4:
				building_policy_info = "城下作战";
				break;
			default:
				building_policy_info = "无";
				break;
			}

			switch (building_p.weapon_choose)
			{
			case 0:
				weapon_type = "平衡";
				break;
			case 1:
				weapon_type = "枪";
				break;
			case 2:
				weapon_type = "戟";
				break;
			case 3:
				weapon_type = "弩";
				break;
			default:
				weapon_type = "无";
				break;
			}

			string 冲车系 = "冲车";
			string 井栏系 = "井栏";

			if (pk::has_tech(building, 技巧_投石开发))
				井栏系 = "投石";
			if (pk::has_tech(building, 技巧_木兽开发))
				冲车系 = "木兽";

			string arm_type;
			switch (building_p.arms_choose)
			{
			case 0:
				arm_type = "平衡";
				break;
			case 1:
				arm_type = 井栏系;
				break;
			case 2:
				arm_type = 冲车系;
				break;
			default:
				arm_type = "无";
				break;
			}

			if (func_附近_敌城市数(city, 1) >= 1)
				building_battle_info = "前线";

			else if (func_附近_敌城市数(city, 2) >= 1)
				building_battle_info = "警戒";

			else if (func_附近_敌城市数(city, 2) == 0)
				building_battle_info = "后方";

			if (pk::enemies_around(building))
				building_battle_info = "作战";

			if (building.get_force_id() == -1)
				building_battle_info = "空城";

			int width = int(pk::get_resolution().width) - 250;
			pk::point left_down = pk::point(int(pk::get_resolution().width) - 20, 85 + (25) * 18 + (开启完全自动内政 ? 48 * 3 : 0) + 40 + 5);
			pk::draw_filled_rect(pk::rectangle(pk::point(width - 5, 15), left_down), ((0xff / 2) << 24) | 0x010101);
			pk::draw_text(pk::encode(title), pk::point(width, 25), 0xffffffff, FONT_BIG, 0xff000000);
			pk::draw_text(pk::encode(pk::format("都市方针: \x1b[1x{}\x1b[0x", building_policy_info)), pk::point(width, 75 + (高度 - 1) * 10), 0xffffffff, FONT_BIG, 0xff000000);
			//pk::draw_text(pk::encode(pk::format("方针开支: \x1b[1x{}\x1b[0x{}", building_p.policy_expense, "%")), pk::point(width, 75 + (高度 + 1) * 30), 0xffffffff, FONT_BIG, 0xff000000);

			pk::draw_text(pk::encode(pk::format(" \x1b[16x{}\x1b[0x", building_battle_info)), pk::point(width + 120, 80 + (高度 + 1) * 30), 0xffffffff, FONT_SMALL, 0xff000000);

			pk::point rightup = pk::point(int(pk::get_resolution().width) - 右移, 75 + (高度 + 1) * 20);
			pk::point leftdown = pk::point(int(pk::get_resolution().width) - 右移 + 32, 75 + (高度 + 1) * 20 + 40);
			auto rect = pk::rectangle(rightup, leftdown);

			高度 = 高度 + 3;
			pk::draw_text(pk::encode("\x1b[2x招兵内政官"), pk::point(width, 75 + (高度 + 0) * 20), 0xffffffff, FONT_SMALL, 0xff000000);

			pk::draw_text(pk::encode(pk::format("招兵开支: \x1b[1x{}\x1b[0x", int(building_p.troops_effic))), pk::point(width + 文字位移, 75 + (高度 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("招兵增量: \x1b[1x{}\x1b[0x", building_p.troops_gain)), pk::point(width + 文字位移, 75 + (高度 + 2) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			if (pk::is_valid_person_id(building_p.recruit_person))
			{
				pk::draw_text(pk::encode(pk::format("\x1b[1x{}\x1b[0x", pk::decode(pk::get_name(pk::get_person(building_p.recruit_person))))), pk::point(width + 名字位移, 75 + (高度 * 20 + 30)), 0xffffffff, FONT_SMALL, 0xff000000);
				rightup = pk::point(int(pk::get_resolution().width) - 右移, 75 + (高度 + 1) * 20);
				leftdown = pk::point(int(pk::get_resolution().width) - 右移 + 32, 75 + (高度 + 1) * 20 + 40);
				rect = pk::rectangle(rightup, leftdown);
				if (pk::is_valid_person_id(building_p.recruit_person))
					pk::draw_face(FACE_SMALL, pk::get_person(building_p.recruit_person).face, rect, FACE_R);
			}

			高度 = 高度 + 4;
			pk::draw_text(pk::encode("巡查内政官"), pk::point(width, 70 + (高度 + 0) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("巡查开支: \x1b[1x{}\x1b[0x", int(building_p.porder_effic))), pk::point(width + 文字位移, 70 + (高度 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("巡查增量: \x1b[1x{}\x1b[0x", building_p.porder_gain)), pk::point(width + 文字位移, 70 + (高度 + 2) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			if (pk::is_valid_person_id(building_p.inspections_person))
			{
				pk::draw_text(pk::encode(pk::format("\x1b[1x{}\x1b[1x", pk::decode(pk::get_name(pk::get_person(building_p.inspections_person))))), pk::point(width + 名字位移, 75 + (高度 * 20 + 30)), 0xffffffff, FONT_SMALL, 0xff000000);
				rightup = pk::point(int(pk::get_resolution().width) - 右移, 75 + (高度 + 1) * 20);
				leftdown = pk::point(int(pk::get_resolution().width) - 右移 + 32, 75 + (高度 + 1) * 20 + 40);
				rect = pk::rectangle(rightup, leftdown);
				if (pk::is_valid_person_id(building_p.inspections_person))
					pk::draw_face(FACE_SMALL, pk::get_person(building_p.inspections_person).face, rect, FACE_R);
			}

			高度 = 高度 + 4;
			pk::draw_text(pk::encode("训练内政官"), pk::point(width, 75 + (高度 + 0) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("训练开支: \x1b[1x{}\x1b[0x", int(building_p.train_effic))), pk::point(width + 文字位移, int(75 + (高度 + 0.5) * 20)), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("训练增量: \x1b[1x{}\x1b[0x", building_p.train_gain)), pk::point(width + 文字位移, int(75 + (高度 + 1.5) * 20)), 0xffffffff, FONT_SMALL, 0xff000000);
			if (pk::is_valid_person_id(building_p.drill_person))
			{
				pk::draw_text(pk::encode(pk::format("\x1b[1x{}\x1b[0x", pk::decode(pk::get_name(pk::get_person(building_p.drill_person))))), pk::point(width + 名字位移, 75 + (高度 * 20 + 30)), 0xffffffff, FONT_SMALL, 0xff000000);
				rightup = pk::point(int(pk::get_resolution().width) - 右移, 75 + (高度 + 1) * 20);
				leftdown = pk::point(int(pk::get_resolution().width) - 右移 + 32, 75 + (高度 + 1) * 20 + 40);
				rect = pk::rectangle(rightup, leftdown);
				if (pk::is_valid_person_id(building_p.drill_person))
					pk::draw_face(FACE_SMALL, pk::get_person(building_p.drill_person).face, rect, FACE_R);
			}

			高度 = 高度 + 4;
			pk::draw_text(pk::encode("兵装生产官"), pk::point(width, 70 + (高度 + 0) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("生产兵装: \x1b[1x{}\x1b[0x", weapon_type)), pk::point(width + 文字位移, 70 + (高度 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("兵装开支: \x1b[1x{}\x1b[0x", int(building_p.weapon_effic))), pk::point(width + 文字位移, 70 + (高度 + 0) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format(" 枪 产量: \x1b[1x{}\x1b[0x", building_p.spear_gain)), pk::point(width + 文字位移, 70 + (高度 + 2) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format(" 戟 产量: \x1b[1x{}\x1b[0x", building_p.halberd_gain)), pk::point(width + 文字位移, 70 + (高度 + 3) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format(" 弩 产量: \x1b[1x{}\x1b[0x", building_p.bow_gain)), pk::point(width + 文字位移, 70 + (高度 + 4) * 20), 0xffffffff, FONT_SMALL, 0xff000000);

			if (pk::is_valid_person_id(building_p.weapon_person))
			{
				pk::draw_text(pk::encode(pk::format("\x1b[1x{}\x1b[0x", pk::decode(pk::get_name(pk::get_person(building_p.weapon_person))))), pk::point(width + 名字位移, 75 + (高度 * 20 + 30)), 0xffffffff, FONT_SMALL, 0xff000000);
				rightup = pk::point(int(pk::get_resolution().width) - 右移, 75 + (高度 + 1) * 20);
				leftdown = pk::point(int(pk::get_resolution().width) - 右移 + 32, 75 + (高度 + 1) * 20 + 40);
				rect = pk::rectangle(rightup, leftdown);
				if (pk::is_valid_person_id(building_p.weapon_person))
					pk::draw_face(FACE_SMALL, pk::get_person(building_p.weapon_person).face, rect, FACE_R);
			}

			高度 = 高度 + 5;
			pk::draw_text(pk::encode("育马生产官"), pk::point(width, 75 + (高度 + 0) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("育马开支: \x1b[1x{}\x1b[0x", int(building_p.horse_effic))), pk::point(width + 文字位移, int(75 + (高度 + 0.5) * 20)), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("育马增量: \x1b[1x{}\x1b[0x", building_p.horse_gain)), pk::point(width + 文字位移, int(75 + (高度 + 1.5) * 20)), 0xffffffff, FONT_SMALL, 0xff000000);
			if (pk::is_valid_person_id(building_p.horse_person))
			{
				pk::draw_text(pk::encode(pk::format("\x1b[1x{}\x1b[0x", pk::decode(pk::get_name(pk::get_person(building_p.horse_person))))), pk::point(width + 名字位移, 75 + (高度 * 20 + 30)), 0xffffffff, FONT_SMALL, 0xff000000);
				rightup = pk::point(int(pk::get_resolution().width) - 右移, 75 + (高度 + 1) * 20);
				leftdown = pk::point(int(pk::get_resolution().width) - 右移 + 32, 75 + (高度 + 1) * 20 + 40);
				rect = pk::rectangle(rightup, leftdown);
				if (pk::is_valid_person_id(building_p.horse_person))
					pk::draw_face(FACE_SMALL, pk::get_person(building_p.horse_person).face, rect, FACE_R);
			}

			高度 = 高度 + 4;
			pk::draw_text(pk::encode("攻具生产官"), pk::point(width, 75 + (高度 + 0) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("生产攻具: \x1b[1x{}\x1b[0x", arm_type)), pk::point(width + 文字位移, 75 + (高度 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("攻具开支: \x1b[1x{}\x1b[0x", int(building_p.punch_effic))), pk::point(width + 文字位移, 75 + (高度 + 0) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("攻具产量: \x1b[1x\x1b[1x{}\x1b[0x{}", building_p.punch_gain / 10.0, "%")), pk::point(width + 文字位移, 75 + (高度 + 2) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("井栏进度: \x1b[1x{}\x1b[0x{}", (building_p.well / 10), "%")), pk::point(width + 文字位移, 75 + (高度 + 3) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("冲车进度: \x1b[1x{}\x1b[0x{}", int(building_p.punch / 10), "%")), pk::point(width + 文字位移, 75 + (高度 + 4) * 20), 0xffffffff, FONT_SMALL, 0xff000000);

			if (pk::is_valid_person_id(building_p.punch_person))
			{
				pk::draw_text(pk::encode(pk::format("\x1b[1x{}\x1b[0x", pk::decode(pk::get_name(pk::get_person(building_p.punch_person))))), pk::point(width + 名字位移, 75 + (高度 * 20 + 30)), 0xffffffff, FONT_SMALL, 0xff000000);
				rightup = pk::point(int(pk::get_resolution().width) - 右移, 75 + (高度 + 1) * 20);
				leftdown = pk::point(int(pk::get_resolution().width) - 右移 + 32, 75 + (高度 + 1) * 20 + 40);
				rect = pk::rectangle(rightup, leftdown);
				if (pk::is_valid_person_id(building_p.punch_person))
					pk::draw_face(FACE_SMALL, pk::get_person(building_p.punch_person).face, rect, FACE_R);
			}

			高度 = 高度 + 5;
			pk::draw_text(pk::encode("造船生产官"), pk::point(width, 75 + (高度 + 0) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("船 开支: \x1b[1x{}\x1b[0x", int(building_p.boat_effic))), pk::point(width + 文字位移, int(75 + (高度 + 0.5) * 20)), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("船 产量: \x1b[1x\x1b[1x{}\x1b[0x{}", building_p.boat_gain / 10.0, "%")), pk::point(width + 文字位移, int(75 + (高度 + 1.5) * 20)), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(pk::format("舰船进度: \x1b[1x{}\x1b[0x{}", int(building_p.boat / 10), "%")), pk::point(width + 文字位移, int(75 + (高度 + 2.5) * 20)), 0xffffffff, FONT_SMALL, 0xff000000);

			if (pk::is_valid_person_id(building_p.boat_person))
			{
				pk::draw_text(pk::encode(pk::format("\x1b[1x{}\x1b[0x", pk::decode(pk::get_name(pk::get_person(building_p.boat_person))))), pk::point(width + 名字位移, 75 + (高度 * 20 + 30)), 0xffffffff, FONT_SMALL, 0xff000000);
				rightup = pk::point(int(pk::get_resolution().width) - 右移, 75 + (高度 + 1) * 20);
				leftdown = pk::point(int(pk::get_resolution().width) - 右移 + 32, 75 + (高度 + 1) * 20 + 40);
				rect = pk::rectangle(rightup, leftdown);
				if (pk::is_valid_person_id(building_p.boat_person))
					pk::draw_face(FACE_SMALL, pk::get_person(building_p.boat_person).face, rect, FACE_R);
			}
		}

		int cmd_stat;
		int cmd_skill;
		pk::list<pk::person @> sort_person_list(pk::building @base, int stat, int skill)
		{
			cmd_stat = stat;
			cmd_skill = skill;
			pk::list<pk::person @> actors;

			// 실행가능 무장 확인
			pk::list<pk::person @> list_person = pk::get_person_list(base, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般));
			if (list_person.count == 0)
				return actors;

			// 需要排除其他已经设定过的内政官

			// if (调试模式)
			// pk::message_box(pk::encode(pk::format("排序前，目标数量不为0", 1)));

			list_person.sort(function(a, b) {
				if (main.cmd_skill >= 0)
				{
					bool a_skill = ch::has_skill(a, main.cmd_skill);
					bool b_skill = ch::has_skill(b, main.cmd_skill);
					if (a_skill and !b_skill)
						return true;
					if (!a_skill and b_skill)
						return false;

					return a.stat[main.cmd_stat] > b.stat[main.cmd_stat];
				}

				return a.stat[main.cmd_stat] > b.stat[main.cmd_stat];
			});

			return list_person;
		}

		int func_附近_敌城市数(pk::city @city, int 距离)
		{
			int enemy_city_count = 0;

			array<pk::city @> cities = pk::list_to_array(pk::get_city_list());
			for (int i = 0; i < int(cities.length); i++)
			{
				pk::city @neighbor_city = cities[i];

				int distance = pk::get_city_distance(city.get_id(), neighbor_city.get_id());

				if (distance > 距离)
					continue;
				if (!pk::is_enemy(city, neighbor_city))
					continue;

				enemy_city_count++;
			}

			return enemy_city_count;
		}

	} Main main;
	// 根据规则获取武将
	pk::list<pk::person @> get_person_list(pk::building @base, int type)
	{
		int param1; // 排序规则1
		int param2; // 排序规则2
		switch (type)
		{
		case 内政自动化_巡查:
			param1 = int(pk::core["inspection.stat"]);
			param2 = -1;
			break;

		case 内政自动化_训练:
			param1 = int(pk::core["train.stat"]);
			param2 = -1;
			break;

		case 内政自动化_招兵:
			param1 = int(pk::core["recruit.stat"]);
			param2 = int(pk::core["recruit.skill"]);
			break;

		case 内政自动化_兵装:
			param1 = int(pk::core["weapon_produce.stat"]);
			param2 = int(pk::core["weapon_produce.smith_skill"]);
			break;

		case 内政自动化_育马:
			param1 = int(pk::core["weapon_produce.stat"]);
			param2 = 特技_繁殖;
			break;

		case 内政自动化_攻具:
			param1 = int(pk::core["weapon_produce.stat"]);
			param2 = 特技_发明;
			break;

		case 内政自动化_造船:
			param1 = int(pk::core["weapon_produce.stat"]);
			param2 = 特技_造船;
			break;
		}
		return main.sort_person_list(base, param1, param2);
	}

	void toggle_charge(int force_id, int city_id, int 内政自动化_项目)
	{
		string 执行武将名称 = "无";
		string 前执行武将名称 = "无";
		string 项目名称 = "";
		array<string> 项目名称_清单 = {"巡查执行官", "训练执行官", "招兵执行官", "兵装执行官", "育马执行官", "攻具执行官", "造船执行官"};
		// int id = (内政自动化_项目 == 内政自动化_兵装) ? 3 : 内政自动化_项目;
		// 项目名称 = 项目名称_清单[id];
		BuildingInfo @building_p = @building_ex[city_id];
		int person_id = building_p.get_charge(内政自动化_项目);
		pk::building @base = pk::get_building(city_id);
		if (pk::is_valid_person_id(person_id))
			执行武将名称 = pk::decode(pk::get_name(pk::get_person(person_id)));
		前执行武将名称 = 执行武将名称;
		// 当前{}执行武将为{}，是否要变更执行官

		if (内政自动化_项目 == 0)
			项目名称 = "巡查执行官";
		if (内政自动化_项目 == 1)
			项目名称 = "训练执行官";
		if (内政自动化_项目 == 2)
			项目名称 = "招兵执行官";
		if (内政自动化_项目 == 3)
			项目名称 = "兵装执行官";
		if (内政自动化_项目 == 4)
			项目名称 = "育马执行官";
		if (内政自动化_项目 == 5)
			项目名称 = "攻具执行官";
		if (内政自动化_项目 == 6)
			项目名称 = "造船执行官";

		bool confirm = pk::yes_no(pk::encode(pk::format("当前{}为\x1b[1x{}\x1b[0x，是否要变更执行官?", 项目名称, 执行武将名称)));
		if (confirm)
		{
			pk::list<pk::person @> person_list; // = pk::get_person_list(pk::get_building(city_id), pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般));
			// 10 巡查，33训练 16招兵，5兵装
			int list_type = 1;
			switch (内政自动化_项目)
			{
			case 内政自动化_巡查:
				list_type = 10;
				break;

			case 内政自动化_训练:
				list_type = 33;
				break;

			case 内政自动化_招兵:
				list_type = 16;
				break;

			case 内政自动化_兵装:
				list_type = 5;
				break;

			case 内政自动化_育马:
				list_type = 5;
				break;

			case 内政自动化_攻具:
				list_type = 5;
				break;

			case 内政自动化_造船:
				list_type = 5;
				break;
			}
			switch (内政自动化_项目)
			{
			case 内政自动化_巡查:
			{
				person_list = get_person_list(base, 内政自动化_巡查);
				break;
			}
			case 内政自动化_训练:
			{
				person_list = get_person_list(base, 内政自动化_训练);
				break;
			}
			case 内政自动化_招兵:
			{
				person_list = get_person_list(base, 内政自动化_招兵);
				break;
			}
			case 内政自动化_兵装:
			{
				person_list = get_person_list(base, 内政自动化_兵装);
				break;
			}
			case 内政自动化_育马:
			{
				person_list = get_person_list(base, 内政自动化_育马);
				break;
			}
			case 内政自动化_攻具:
			{
				person_list = get_person_list(base, 内政自动化_攻具);
				break;
			}
			case 内政自动化_造船:
			{
				person_list = get_person_list(base, 内政自动化_造船);
				break;
			}
			}
			pk::list<pk::person @> person_sel = pk::person_selector(pk::encode("执行官选择"), pk::encode(pk::format("选择{}.", 项目名称)), person_list, 1, 1, person_list, list_type);
			// if (person_sel.count == 0)
			//	return; // 未选擇时或取消时结束

			if (person_sel.count == 0 and 内政自动化_项目 == 内政自动化_巡查 and building_p.inspections_person > -1)
			{
				bool confirm1 = pk::yes_no(pk::encode(pk::format("当前{}为\x1b[1x{}\x1b[0x，是否要撤销执行官?", 项目名称, 执行武将名称)));
				if (confirm1)
				{
					building_p.inspections_person = -1;
					pk::message_box(pk::encode("巡查官任务取消"));
				}
			}
			else if (person_sel.count == 0 and 内政自动化_项目 == 内政自动化_训练 and building_p.drill_person > -1)
			{
				bool confirm2 = pk::yes_no(pk::encode(pk::format("当前{}为\x1b[1x{}\x1b[0x，是否要撤销执行官?", 项目名称, 执行武将名称)));
				if (confirm2)
				{
					building_p.drill_person = -1;
					pk::message_box(pk::encode("训练官任务取消"));
				}
			}
			else if (person_sel.count == 0 and 内政自动化_项目 == 内政自动化_招兵 and building_p.recruit_person > -1)
			{
				bool confirm3 = pk::yes_no(pk::encode(pk::format("当前{}为\x1b[1x{}\x1b[0x，是否要撤销执行官?", 项目名称, 执行武将名称)));
				if (confirm3)
				{
					building_p.recruit_person = -1;
					pk::message_box(pk::encode("招兵官任务取消"));
				}
			}
			else if (person_sel.count == 0 and 内政自动化_项目 == 内政自动化_兵装 and building_p.weapon_person > -1)
			{
				bool confirm4 = pk::yes_no(pk::encode(pk::format("当前{}为\x1b[1x{}\x1b[0x，是否要撤销执行官?", 项目名称, 执行武将名称)));
				if (confirm4)
				{
					building_p.weapon_person = -1;
					pk::message_box(pk::encode("兵装生产官任务取消"));
				}
			}
			else if (person_sel.count == 0 and 内政自动化_项目 == 内政自动化_育马 and building_p.horse_person > -1)
			{
				bool confirm5 = pk::yes_no(pk::encode(pk::format("当前{}为\x1b[1x{}\x1b[0x，是否要撤销执行官?", 项目名称, 执行武将名称)));
				if (confirm5)
				{
					building_p.horse_person = -1;
					pk::message_box(pk::encode("育马内政官任务取消"));
				}
			}
			else if (person_sel.count == 0 and 内政自动化_项目 == 内政自动化_攻具 and building_p.punch_person > -1)
			{
				bool confirm6 = pk::yes_no(pk::encode(pk::format("当前{}为\x1b[1x{}\x1b[0x，是否要撤销执行官?", 项目名称, 执行武将名称)));
				if (confirm6)
				{
					building_p.punch_person = -1;
					pk::message_box(pk::encode("攻具生产官任务取消"));
				}
			}
			else if (person_sel.count == 0 and 内政自动化_项目 == 内政自动化_造船 and building_p.boat_person > -1)
			{
				bool confirm7 = pk::yes_no(pk::encode(pk::format("当前{}为\x1b[1x{}\x1b[0x，是否要撤销执行官?", 项目名称, 执行武将名称)));
				if (confirm7)
				{
					building_p.boat_person = -1;
					pk::message_box(pk::encode("船舶生产官任务取消"));
				}
			}
			else if (person_sel.count == 0)
				return; // 未选擇时或取消时结束
			person_id = person_sel[0].get_id();
			building_p.set_charge(person_id, 内政自动化_项目);

			if (force_id == pk::get_current_turn_force_id())
			{
				// string city_name = pk::decode(pk::get_name(pk::city_to_building(pk::get_city(city_id))));
				string city_name = pk::decode(pk::get_name(pk::get_building(city_id)));
				string person_name = pk::decode(pk::get_name(pk::get_person(person_id)));
				// string toggle_msg = pk::format("将{}由\x1b[1x{}\x1b[0x改为:\x1b[1x{}\x1b[0x", city_name, 前执行武将名称, person_name);
				string toggle_msg;
				// 如果该武将有其他任职，将自动取消

				if (内政自动化_项目 != 内政自动化_巡查 and building_p.inspections_person == person_id)
				{
					toggle_msg += "原巡查官" + person_name + "任务取消";
					building_p.inspections_person = -1;
				}

				else if (内政自动化_项目 != 内政自动化_训练 and building_p.drill_person == person_id)
				{
					toggle_msg += "原训练官" + person_name + "任务取消";
					building_p.drill_person = -1;
				}
				else if (内政自动化_项目 != 内政自动化_招兵 and building_p.recruit_person == person_id)
				{
					toggle_msg += "原招兵执行官" + person_name + "任务取消";
					building_p.recruit_person = -1;
				}
				else if (内政自动化_项目 != 内政自动化_兵装 and building_p.weapon_person == person_id)
				{
					toggle_msg += "原兵装生产官" + person_name + "任务取消";
					building_p.weapon_person = -1;
				}
				else if (内政自动化_项目 != 内政自动化_育马 and building_p.horse_person == person_id)
				{
					toggle_msg += "原育马官" + person_name + "任务取消";
					building_p.horse_person = -1;
				}
				else if (内政自动化_项目 != 内政自动化_攻具 and building_p.punch_person == person_id)
				{
					toggle_msg += "原攻具生产官" + person_name + "任务取消";
					building_p.punch_person = -1;
				}
				else if (内政自动化_项目 != 内政自动化_造船 and building_p.boat_person == person_id)
				{
					toggle_msg += "原造船执行官" + person_name + "任务取消";
					building_p.boat_person = -1;
				}
				pk::message_box(pk::encode(toggle_msg));
			}
		}
	}

}
