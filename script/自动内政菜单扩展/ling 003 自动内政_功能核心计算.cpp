/*
## 2023/07/26 # 黑店小小二 # 修复筑城开支设定取错字段 ##
## 2023/07/25 # 黑店小小二 # 自动任命官员及自动设定分支逻辑处理移动到此；calculate_building2增加update_data参数等 ##
## 2023/05/11 # 江东新风 # 仅在执政官功能开启时显示和执行 ##
## 2023/5/10 # 江东新风 # 收益计算函数重写
## 2023/1/19 # 铃 # 修复数个bug
## 2022/12/3 # 铃 # 修复数个bug
## 2022/8/8 # 铃 # 全面自动化内政包括以下功能
新增功能:

a.匹配20220328版本的人口系统,所有的生产能力.招兵基准.财务收支都和人口密切挂钩.城防
b.匹配20220328版本的伤兵系统,新增回归兵系统
c.基于San14的挂机流玩法,所有招兵.训练.生产.造船等均为自动进行.
d.新增额外城防系统,可以通过不断的发展提高额外城防,在受到攻击时,优先扣除额外城防

设定细节:
1.计算人口:人口拥有增长率,增长率和治安.前线与否.战斗与否.城防.设施数量等等挂钩.
2.招兵自动化算法:每旬根据城市里人物属性加成,自动获得潜在兵役的一部分比例
3.兵装生产自动化算法:每旬根据城市里人物属性加成,自动获得部分兵装,特殊城市还有特殊加成.前线与否
4.伤兵恢复系统:城市和部队在作战中会产生伤兵,伤兵回归城市后会计算为城市伤兵,城市会缓慢恢复伤兵,部队溃散也会有一小部分伤兵成为回归伤兵.缓慢回归城市
5.额外城防可以增加城市内部队防御力,可以影响城市人口增量.
*/

namespace 内政自动
{

	// ================ CUSTOMIZE ================
	const int RANK_TP_COST = 50; // 自动任命官员技巧要求
	const int RANK_ACTION_COST = 100; // 自动任命官员行动力要求

	const int EFFIC_TP_COST = 0; // 自动设定开支技巧要求
	const int EFFIC_ACTION_COST = 100; // 自动设定开支行动力要求

	class Main
	{

		pk::building @building_;
		pk::force @force_;
		pk::person @taishu_;

		Main()
		{
			pk::bind(102, pk::trigger102_t(StartSet), 1);
			pk::bind(107, pk::trigger107_t(callback));
			pk::bind(120, pk::trigger120_t(callback2), 999); // 数字越大越优先
		}

		// 开局时设定基础数据
		void StartSet()
		{
			if (!pk::get_scenario().loaded)
			{
				for (int city_id = 0; city_id < 据点_末; ++city_id)
				{

					BaseInfo @base_t = @base_ex[city_id];
					BuildingInfo @building_p = @building_ex[city_id];

					building_p.policy_expense = 0;
					building_p.building_policy = 0;
					building_p.punch_effic = 0;
					building_p.boat_effic = 0;
					building_p.horse_effic = 0;
					building_p.weapon_effic = 0;
					building_p.troops_effic = 0;
					building_p.repair_effic = 0;
					building_p.porder_effic = 0;
					building_p.train_effic = 0;
				}
			}
			else
			{
				//因为读档时城市收入不会更新，而数据计算又需要收入支持，所以加入此函数
				for (int city_id = 0; city_id < 城市_末; ++city_id)
				{
					BuildingInfo @building_p = @building_ex[city_id];
					building_p.building_revenue = cast<pk::func150_t>(pk::get_func(150))(pk::get_city(city_id));
					
				}
				//剧本读取时需要获取所有城市的
				//pk::trace("set_auto_IA_order 0 城市_邺" + building_ex[城市_邺].troops_effic);
				pk::building@ building = pk::get_building(城市_邺);
				set_auto_IA_order(building);
				//pk::trace("set_auto_IA_order 1 城市_邺" + building_ex[城市_邺].troops_effic);
			}
		}

		void callback()
		{
			if (!ch::get_auto_affairs_status()) return;
			for (int i = 0; i < 据点_末; ++i)
			{
				auto building = pk::get_building(i);
				set_auto_IA_order(building);
				calculate_building2(building);
				//calculate_building(building);
				//reward_building(building);
			}
		}

		void callback2()
		{
			if (!ch::get_auto_affairs_status()) return;
			//信息显示_顶部面板();//干掉，现要求信息显示全写在400文件
		}
	} // class Main

	Main main;

	// update_data - 数值变更后，是否立刻更新城池资源属性
	void calculate_building2(pk::building@ base, bool update_data = true)
	{
		//还得考虑开支的影响以及是否在城中
		int force_id = base.get_force_id();
		if (!pk::is_valid_force_id(force_id)) return;

		BuildingInfo @building_p = @building_ex[base.get_id()];
		pk::list<pk::person@> actors;
		bool 兵临城下 = pk::enemies_around(base);
		if (pk::is_valid_person_id(building_p.recruit_person) and !兵临城下)
		{
			actors.add(pk::get_person(building_p.recruit_person));
			
			building_p.troops_gain = RECRUIT_TROOPS_CHANGE::get_recruit_num(base, actors,0);
			//pk::trace("building_p.recruit_person:" + building_p.recruit_person +","+ building_p.troops_gain);
			if (update_data) ch::add_troops(base, building_p.troops_gain, true);
			//治安变化
			int p_order_change = RECRUIT_PUBLIC_ORDER_CHANGE::get_recruit_order_dec(base, actors, building_p.troops_gain);
			if (update_data) ch::add_public_order(base, p_order_change, true);
		}
		if (pk::is_valid_person_id(building_p.inspections_person))
		{
			actors.clear();
			actors.add(pk::get_person(building_p.inspections_person));
			building_p.porder_gain = INSPECTIONS::get_inspections_order_inc(base, actors);

			if (update_data) ch::add_public_order(base, building_p.porder_gain, true);
		}
		if (pk::is_valid_person_id(building_p.drill_person))
		{
			actors.clear();
			actors.add(pk::get_person(building_p.drill_person));
			building_p.train_gain = DRILL::get_drill_energy_change(base, actors);

			if (update_data) pk::add_energy(base, building_p.train_gain, true);
		}
		if (pk::is_valid_person_id(building_p.weapon_person))
		{
			actors.clear();
			actors.add(pk::get_person(building_p.weapon_person));
			int weapon_gain= PRODUCE::cal_produce_gain(base, actors, 兵器_枪);//枪戟弩实际上一样
			//pk::trace("building_p.weapon_person:" + building_p.weapon_person + ",weapon_gain：" + weapon_gain);
			int spear_gain = 0;
			int pike_gain = 0;
			int crossbow_gain = 0;
			if (building_p.weapon_choose == 0)//均衡
			{				
				
				building_p.spear_gain = int(weapon_gain / 3);
				building_p.halberd_gain = int(weapon_gain / 3);
				building_p.bow_gain = int(weapon_gain / 3);
				if (update_data)
				{
					pk::add_weapon_amount(base, 兵器_枪, building_p.spear_gain, true);
					pk::add_weapon_amount(base, 兵器_戟, building_p.halberd_gain, true);
					pk::add_weapon_amount(base, 兵器_弩, building_p.bow_gain, true);
				}
			}
			if (building_p.weapon_choose == 1)//重视枪
			{
				building_p.spear_gain = weapon_gain;
				if (update_data) pk::add_weapon_amount(base, 兵器_枪, building_p.spear_gain, true);
			}
			if (building_p.weapon_choose == 2)//重视戟
			{
				building_p.halberd_gain = weapon_gain;
				if (update_data) pk::add_weapon_amount(base, 兵器_戟, building_p.halberd_gain, true);
			}
			if (building_p.weapon_choose == 3)//重视弩
			{
				building_p.bow_gain = weapon_gain;
				if (update_data) pk::add_weapon_amount(base, 兵器_弩, building_p.bow_gain, true);
			}
		}
		if (pk::is_valid_person_id(building_p.horse_person))
		{
			actors.clear();
			actors.add(pk::get_person(building_p.horse_person));
			building_p.horse_gain = PRODUCE::cal_produce_gain(base, actors, 兵器_战马);//枪戟弩实际上一样

			if (update_data) pk::add_weapon_amount(base, 兵器_战马, building_p.horse_gain, true);
		}
		if (pk::is_valid_person_id(building_p.punch_person))
		{
			actors.clear();
			actors.add(pk::get_person(building_p.punch_person));

			int day_inc = 1000 / PRODUCE_TIME_COST::get_produce_time(actors, 兵器_冲车)+1;//注意，此函数不传入city
			if (building_p.arms_choose == 0)
			{
				building_p.well_gain += int(day_inc / 2);
				building_p.punch_gain += int(day_inc / 2);
			}
			else
			{
				if (building_p.arms_choose == 1)
					building_p.well_gain += int(day_inc);
				if (building_p.arms_choose == 2)
					building_p.punch_gain += int(day_inc);
			}

			if (building_p.well_gain > 1000)
			{
				if (pk::has_tech(base, 技巧_投石开发))
				{
					if (update_data) pk::add_weapon_amount(base, 兵器_投石, 1, true);
				}
				else
				{
					if (update_data) pk::add_weapon_amount(base, 兵器_井阑, 1, true);
				}
				building_p.well_gain -= 1000;
			}

			if (building_p.punch_gain > 1000)
			{
				if (pk::has_tech(base, 技巧_木兽开发)) 
				{
					if (update_data) pk::add_weapon_amount(base, 兵器_木兽, 1, true);
				}
				else
				{
					if (update_data) pk::add_weapon_amount(base, 兵器_冲车, 1, true);
				}
				building_p.punch_gain -= 1000;
			}
		}
		if (pk::is_valid_person_id(building_p.boat_person))
		{
			actors.clear();
			actors.add(pk::get_person(building_p.boat_person));
			int day_inc = 1000 / PRODUCE_TIME_COST::get_produce_time(actors, 兵器_楼船) + 1;

			if (building_p.boat_gain > 1000)
			{
				if (pk::has_tech(base, 技巧_投石开发))
				{
					if (update_data) pk::add_weapon_amount(base, 兵器_斗舰, 1, true);
				}
				else 
				{
					if (update_data) pk::add_weapon_amount(base, 兵器_楼船, 1, true);
				}
				building_p.boat_gain -= 1000;
			}
		}
	}

	void set_auto_IA_order(pk::building @building)
	{
		if (building.get_force_id() == -1)
			return;
		BaseInfo @base_t = @base_ex[building.get_id()];
		BuildingInfo @building_p = @building_ex[building.get_id()];
		// AI开支设定
		if ((pk::get_taishu_id(building) != -1) and (!building.is_player()) and building.get_id() >= 0 and building.get_id() < 87)
		{

			int person_count = pk::get_person_list(building, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般)).count;

			// 先计算总开支.
			building_p.policy_expense = 100;
			building_p.policy_expense = int(sqrt(pk::get_gold(building)) / 10.f) * 10;
			building_p.policy_expense = pk::clamp(building_p.policy_expense, 0, 200); // 防止溢出,最大值为200.

			// 如果没什么需要做的,就降低总开支到50
			if ((pk::is_valid_person_id(building_p.weapon_person) and pk::get_weapon_amount(building, 1) + pk::get_weapon_amount(building, 2) + pk::get_weapon_amount(building, 3) == pk::get_max_weapon_amount(building, 1) + pk::get_max_weapon_amount(building, 2) + pk::get_max_weapon_amount(building, 3)) or (pk::is_valid_person_id(building_p.recruit_person) and (base_t.mil_pop_av < 2000 or building.troops == pk::get_max_troops(building))))
				building_p.policy_expense = pk::clamp(building_p.policy_expense, 0, 50);

			// 每次重置
			building_p.repair_effic = 0;
			building_p.porder_effic = 0;
			building_p.train_effic = 0;
			building_p.troops_effic = 0;
			building_p.weapon_effic = 0;
			building_p.horse_effic = 0;
			building_p.punch_effic = 0;
			building_p.boat_effic = 0;

			int building_id = building.get_id();
			float 剩余比例 = 1.0;

			pk::point src_pos = building.pos;
			int city_id = pk::get_city_id(building.pos);

			pk::city @AI_city = pk::get_city(city_id);

			// 计算治安
			int order = 0;
			if (building_id >= 据点_城市末 and building_id < 据点_末)
				order = base_ex[building_id].public_order;
			if (building_id >= 0 and building_id < 据点_城市末)
				order = pk::building_to_city(building).public_order;

			// 前线城市,优先分配招兵..
			if (func_附近_敌城市数(AI_city, 1) >= 1)
			{
				building_p.building_policy = 1;

				building_p.rank_type = { 内政自动化_招兵, 内政自动化_训练, 内政自动化_巡查, 内政自动化_兵装, 内政自动化_育马, 内政自动化_攻具, 内政自动化_造船 };
			}

			// 后方城市,优先分配生产..
			if (func_附近_敌城市数(AI_city, 1) == 0)
			{
				building_p.building_policy = 2;
				building_p.rank_type = { 内政自动化_兵装, 内政自动化_育马, 内政自动化_招兵, 内政自动化_巡查, 内政自动化_攻具, 内政自动化_造船, 内政自动化_训练 };
			}
			// 治安太低的时候优先治安.
			if (order < 80)
			{
				if (func_附近_敌城市数(AI_city, 1) >= 1)
				{
					building_p.building_policy = 1;
					building_p.rank_type = { 内政自动化_巡查, 内政自动化_招兵, 内政自动化_训练, 内政自动化_兵装, 内政自动化_育马, 内政自动化_攻具, 内政自动化_造船 };
				}
				if (func_附近_敌城市数(AI_city, 1) == 0)
				{
					building_p.building_policy = 2;
					building_p.rank_type = { 内政自动化_巡查, 内政自动化_兵装, 内政自动化_育马, 内政自动化_招兵, 内政自动化_攻具, 内政自动化_造船, 内政自动化_训练 };
				}
			}

			// 当人手不够,且治安接近满,且为小城的时候,训练优先级放到最后
			if (order > 95 and building_id >= 城市_末 and person_count <= 3)
			{
				array<int> temp = building_p.rank_type;
				int index = temp.find(内政自动化_巡查);

				building_p.rank_type.removeAt(index);
				building_p.rank_type.insertLast(内政自动化_巡查);
			}

			// 当人手不够,且训练满了的时候,且为小城的时候,训练优先级放到最后
			if (building.energy == pk::get_max_energy(building) and building_id >= 城市_末 and person_count <= 3)
			{
				array<int> temp = building_p.rank_type;
				int index = temp.find(内政自动化_训练);

				building_p.rank_type.removeAt(index);
				building_p.rank_type.insertLast(内政自动化_训练);
			}

			// 当兵装接近满了的时候,兵装优先级放到最后
			if (pk::get_weapon_amount(building, 1) + pk::get_weapon_amount(building, 2) + pk::get_weapon_amount(building, 3) >= (pk::get_max_weapon_amount(building, 1) + pk::get_max_weapon_amount(building, 2) + pk::get_max_weapon_amount(building, 3)) * 0.95)

			{
				array<int> temp = building_p.rank_type;
				int index = temp.find(内政自动化_兵装);

				building_p.rank_type.removeAt(index);
				building_p.rank_type.insertLast(内政自动化_兵装);
			}

			// 当无兵可征时,招兵优先级放到最后
			if (base_t.mil_pop_av < 3000 or building.troops == pk::get_max_troops(building))
			{
				array<int> temp = building_p.rank_type;
				int index = temp.find(内政自动化_招兵);

				building_p.rank_type.removeAt(index);
				building_p.rank_type.insertLast(内政自动化_招兵);
			}

			// 作战城市,无论如何分配巡查训练..
			if (pk::enemies_around(building))
			{
				building_p.building_policy = 4;

				int index1 = building_p.rank_type.find(内政自动化_训练);
				building_p.rank_type.removeAt(index1);
				building_p.rank_type.insertAt(0, 内政自动化_训练);

				int index2 = building_p.rank_type.find(内政自动化_巡查);
				building_p.rank_type.removeAt(index2);
				building_p.rank_type.insertAt(0, 内政自动化_巡查);
			}

			//pk::trace("据点：" + pk::get_new_base_name(building.get_id()) + "设置开支");
			for (int i = 0; i < int(building_p.rank_type.length); i++)
			{

				// 当作战状态时,只考虑第前两个.
				if (pk::enemies_around(building) and i > 1)
					break;

				// 如果只有N个人,只进行前N次循环
				if (i >= person_count)
					break;

				// if (building.get_id() == 22)
				// pk::trace(pk::format("城市id：{},城市名:{},i:{},方针开支:{},巡查开支:{}", building.get_id(), pk::decode(pk::get_name(building)), i, building_p.policy_expense, building_p.porder_effic));
				剩余比例 = AI设置内政开支(building_p.rank_type[i], 剩余比例, building, i, person_count);

				// 如果能完全分配,剩下的用于筑城,如果未能完整分配,则无法分配筑城(避免只有1-2个人内政,剩下很多钱筑城的奇怪情况).
				if (person_count - (building_p.rank_type.length) >= 0)
					building_p.repair_effic = int(剩余比例 * building_p.policy_expense * building_p.building_revenue * 0.01 / 3);
			}
		}

	}

	void calculate_building(pk::building @building)
	{
		if (building.get_force_id() == -1)
			return;
		BaseInfo @base_t = @base_ex[building.get_id()];
		BuildingInfo @building_p = @building_ex[building.get_id()];
		pk::force @force = pk::get_force(building.get_force_id());
		pk::person @kunshu = pk::get_person(force.kunshu);

		float 最终修正 = 1.0;
		float 君主能力修正 = 1.0;
		float 势力方针修正 = 1.0;

		float 难度系数 = 1.0;
		switch (pk::get_scenario().difficulty)
		{
		case 难易度_超级:
			if (building.is_player())
				难度系数 = 0.7;
			else
				难度系数 = 1.3;
			break;
		case 难易度_上级:
			if (building.is_player())
				难度系数 = 0.8;
			else
				难度系数 = 1.2;
			break;

		case 难易度_初级:
			if (building.is_player())
				难度系数 = 1.0;
			else
				难度系数 = 1.0;
			break;
		}

		// 君主能力的修正
		君主能力修正 = sqrt(kunshu.stat[武将能力_政治] + kunshu.stat[武将能力_魅力]) * 0.1;

		// 势力的方针修正
		// 势力方针修正 = get_policy_buf(force.policy);

		最终修正 = 难度系数 * 君主能力修正;

		// pk::trace(pk::format("城市id：{},城市名:{},难度修正:{},君主能力修正:{},势力方针修正:{},最终修正:{}", building.get_id(),pk::decode(pk::get_name(building)),难度修正,君主能力修正,势力方针修正,最终修正));

		// 初始值赋值
		float 招兵效率 = 0.0;
		float 兵装效率 = 0.0;
		float 育马效率 = 0.0;
		float 攻具效率 = 0.0;
		float 造船效率 = 0.0;
		float 筑城效率 = 0.0;
		float 巡查效率 = 0.0;
		float 训练效率 = 0.0;

		// AI开支设定
		if ((pk::get_taishu_id(building) != -1) and (!building.is_player()) and building.get_id() >= 0 and building.get_id() < 87)
		{

			int person_count = pk::get_person_list(building, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般)).count;

			// 先计算总开支.
			building_p.policy_expense = 100;
			building_p.policy_expense = int(sqrt(pk::get_gold(building)) / 10.f) * 10;
			building_p.policy_expense = pk::clamp(building_p.policy_expense, 0, 200); // 防止溢出,最大值为200.

			// 如果没什么需要做的,就降低总开支到50
			if ((pk::is_valid_person_id(building_p.weapon_person) and pk::get_weapon_amount(building, 1) + pk::get_weapon_amount(building, 2) + pk::get_weapon_amount(building, 3) == pk::get_max_weapon_amount(building, 1) + pk::get_max_weapon_amount(building, 2) + pk::get_max_weapon_amount(building, 3)) or (pk::is_valid_person_id(building_p.recruit_person) and (base_t.mil_pop_av < 2000 or building.troops == pk::get_max_troops(building))))
				building_p.policy_expense = pk::clamp(building_p.policy_expense, 0, 50);

			// 每次重置
			building_p.repair_effic = 0;
			building_p.porder_effic = 0;
			building_p.train_effic = 0;
			building_p.troops_effic = 0;
			building_p.weapon_effic = 0;
			building_p.horse_effic = 0;
			building_p.punch_effic = 0;
			building_p.boat_effic = 0;

			int building_id = building.get_id();
			float 剩余比例 = 1.0;

			pk::point src_pos = building.pos;
			int city_id = pk::get_city_id(building.pos);

			pk::city @AI_city = pk::get_city(city_id);

			// 计算治安
			int order = 0;
			if (building_id >= 据点_城市末 and building_id < 据点_末)
				order = base_ex[building_id].public_order;
			if (building_id >= 0 and building_id < 据点_城市末)
				order = pk::building_to_city(building).public_order;

			// 前线城市,优先分配招兵..
			if (func_附近_敌城市数(AI_city, 1) >= 1)
			{
				building_p.building_policy = 1;

				building_p.rank_type = {内政自动化_招兵, 内政自动化_训练, 内政自动化_巡查, 内政自动化_兵装, 内政自动化_育马, 内政自动化_攻具, 内政自动化_造船};
			}

			// 后方城市,优先分配生产..
			if (func_附近_敌城市数(AI_city, 1) == 0)
			{
				building_p.building_policy = 2;
				building_p.rank_type = {内政自动化_兵装, 内政自动化_育马, 内政自动化_招兵, 内政自动化_巡查, 内政自动化_攻具, 内政自动化_造船, 内政自动化_训练};
			}
			// 治安太低的时候优先治安.
			if (order < 80)
			{
				if (func_附近_敌城市数(AI_city, 1) >= 1)
				{
					building_p.building_policy = 1;
					building_p.rank_type = {内政自动化_巡查, 内政自动化_招兵, 内政自动化_训练, 内政自动化_兵装, 内政自动化_育马, 内政自动化_攻具, 内政自动化_造船};
				}
				if (func_附近_敌城市数(AI_city, 1) == 0)
				{
					building_p.building_policy = 2;
					building_p.rank_type = {内政自动化_巡查, 内政自动化_兵装, 内政自动化_育马, 内政自动化_招兵, 内政自动化_攻具, 内政自动化_造船, 内政自动化_训练};
				}
			}

			// 当人手不够,且治安接近满,且为小城的时候,训练优先级放到最后
			if (order > 95 and building_id >= 城市_末 and person_count <= 3)
			{
				array<int> temp = building_p.rank_type;
				int index = temp.find(内政自动化_巡查);

				building_p.rank_type.removeAt(index);
				building_p.rank_type.insertLast(内政自动化_巡查);
			}

			// 当人手不够,且训练满了的时候,且为小城的时候,训练优先级放到最后
			if (building.energy == pk::get_max_energy(building) and building_id >= 城市_末 and person_count <= 3)
			{
				array<int> temp = building_p.rank_type;
				int index = temp.find(内政自动化_训练);

				building_p.rank_type.removeAt(index);
				building_p.rank_type.insertLast(内政自动化_训练);
			}

			// 当兵装接近满了的时候,兵装优先级放到最后
			if (pk::get_weapon_amount(building, 1) + pk::get_weapon_amount(building, 2) + pk::get_weapon_amount(building, 3) >= (pk::get_max_weapon_amount(building, 1) + pk::get_max_weapon_amount(building, 2) + pk::get_max_weapon_amount(building, 3)) * 0.95)

			{
				array<int> temp = building_p.rank_type;
				int index = temp.find(内政自动化_兵装);

				building_p.rank_type.removeAt(index);
				building_p.rank_type.insertLast(内政自动化_兵装);
			}

			// 当无兵可征时,招兵优先级放到最后
			if (base_t.mil_pop_av < 3000 or building.troops == pk::get_max_troops(building))
			{
				array<int> temp = building_p.rank_type;
				int index = temp.find(内政自动化_招兵);

				building_p.rank_type.removeAt(index);
				building_p.rank_type.insertLast(内政自动化_招兵);
			}

			// 作战城市,无论如何分配巡查训练..
			if (pk::enemies_around(building))
			{
				building_p.building_policy = 4;

				int index1 = building_p.rank_type.find(内政自动化_训练);
				building_p.rank_type.removeAt(index1);
				building_p.rank_type.insertAt(0, 内政自动化_训练);

				int index2 = building_p.rank_type.find(内政自动化_巡查);
				building_p.rank_type.removeAt(index2);
				building_p.rank_type.insertAt(0, 内政自动化_巡查);
			}

			for (int i = 0; i < int(building_p.rank_type.length); i++)
			{

				// 当作战状态时,只考虑第前两个.
				if (pk::enemies_around(building) and i > 1)
					break;

				// 如果只有N个人,只进行前N次循环
				if (i >= person_count)
					break;

				// if (building.get_id() == 22)
				// pk::trace(pk::format("城市id：{},城市名:{},i:{},方针开支:{},巡查开支:{}", building.get_id(), pk::decode(pk::get_name(building)), i, building_p.policy_expense, building_p.porder_effic));
				剩余比例 = AI设置内政开支(building_p.rank_type[i], 剩余比例, building, i, person_count);

				// 如果能完全分配,剩下的用于筑城,如果未能完整分配,则无法分配筑城(避免只有1-2个人内政,剩下很多钱筑城的奇怪情况).
				if (person_count - (building_p.rank_type.length) >= 0)
					building_p.repair_effic = int(剩余比例 * building_p.policy_expense * building_p.building_revenue * 0.01 / 3);
			}
		}

		int troops_base_gain = int((sqrt(building_p.troops_effic) + building_p.troops_effic * 0.03) * 最终修正);
		int repair_base_gain = int((sqrt(building_p.repair_effic) + building_p.repair_effic * 0.03) * 最终修正);
		int porder_base_gain = int((sqrt(building_p.porder_effic) + building_p.porder_effic * 0.03) * 最终修正);
		int train_base_gain = int((sqrt(building_p.train_effic) + building_p.train_effic * 0.03) * 最终修正);

		int weapon_base_gain = int((sqrt(building_p.weapon_effic) + building_p.weapon_effic * 0.03) * 最终修正);
		int horse_base_gain = int((sqrt(building_p.horse_effic) + building_p.horse_effic * 0.03) * 最终修正);
		int punch_base_gain = int((sqrt(building_p.punch_effic) + building_p.punch_effic * 0.03) * 最终修正);
		int boat_base_gain = int((sqrt(building_p.boat_effic) + building_p.boat_effic * 0.03) * 最终修正);

		// if(building.get_id()  == 13) pk::trace(pk::format("外部函数,城市id：{},城市名:{},兵装开支:{},马开支:{}", building.get_id(),pk::decode(pk::get_name(building)),building_p.weapon_effic ,building_p.horse_effic));

		// 城市人口系统和之招兵系统/生产系统绑定,底子依然是人口数量,生产能力也基于人口数量
		// 通过城市里面太守和普通人的加总双属性增加内政效果.

		if (pk::get_taishu_id(building) != -1 and building.get_id() >= 0 and building.get_id() < 87)
		{
			auto ilban_list = pk::list_to_array(pk::get_person_list(building, pk::mibun_flags(身分_一般)));
			pk::person @taishu = pk::get_person(pk::get_taishu_id(building));
			pk::person @inspections_person = pk::get_person(building_p.inspections_person);
			pk::person @drill_person = pk::get_person(building_p.drill_person);
			pk::person @recruit_person = pk::get_person(building_p.recruit_person);
			pk::person @weapon_person = pk::get_person(building_p.weapon_person);
			pk::person @horse_person = pk::get_person(building_p.horse_person);
			pk::person @punch_person = pk::get_person(building_p.punch_person);
			pk::person @boat_person = pk::get_person(building_p.boat_person);

			if (!pk::is_valid_person_id(building_p.recruit_person))
				building_p.troops_effic = 0;
			if (!pk::is_valid_person_id(building_p.weapon_person))
				building_p.weapon_effic = 0;
			if (!pk::is_valid_person_id(building_p.horse_person))
				building_p.horse_effic = 0;
			if (!pk::is_valid_person_id(building_p.punch_person))
				building_p.punch_effic = 0;
			if (!pk::is_valid_person_id(building_p.inspections_person))
				building_p.porder_effic = 0;
			if (!pk::is_valid_person_id(building_p.drill_person))
				building_p.train_effic = 0;
			if (!pk::is_valid_person_id(building_p.boat_person))
				building_p.boat_effic = 0;

			// pk::trace(pk::format("城市id：{},城市名:{},武将ID:{}", building.get_id(), pk::decode(pk::get_name(building)),building_p.recruit_person));
			//  pk::trace(pk::format("城市id：{},城市名:{},武将名:{},招兵效率:{},魅力:{}", building.get_id(), pk::decode(pk::get_name(building)), pk::decode(pk::get_name(recruit_person)), 招兵效率, recruit_person.stat[武将能力_魅力]));

			if (pk::is_valid_person_id(building_p.recruit_person))
			{
				float 在城中 = 0;
				if (!pk::is_absent(recruit_person))
					在城中 = 1.0;
				else
					在城中 = 0.8;
				招兵效率 += 在城中 * (float((pk::max(recruit_person.stat[武将能力_魅力] - 30, 0)) * 0.01 + 0.5) + (ch::has_skill(recruit_person, 特技_名声) ? 0.5 : 0));
			}

			if (pk::is_valid_person_id(building_p.weapon_person))
			{
				float 在城中 = 0;
				if (!pk::is_absent(weapon_person))
					在城中 = 1.0;
				else
					在城中 = 0.8;
				兵装效率 += 在城中 * (float((pk::max(weapon_person.stat[武将能力_政治] - 30, 0)) * 0.01 + 0.5) + (ch::has_skill(weapon_person, 特技_能吏) ? 0.5 : 0));
			}

			if (pk::is_valid_person_id(building_p.horse_person))
			{
				float 在城中 = 0;
				if (!pk::is_absent(horse_person))
					在城中 = 1.0;
				else
					在城中 = 0.8;
				育马效率 += 在城中 * (float((pk::max(horse_person.stat[武将能力_政治] - 30, 0)) * 0.01 + 0.5) + (ch::has_skill(horse_person, 特技_繁殖) ? 0.5 : 0));
			}

			if (pk::is_valid_person_id(building_p.punch_person))
			{
				float 在城中 = 0;
				if (!pk::is_absent(punch_person))
					在城中 = 1.0;
				else
					在城中 = 0.8;
				攻具效率 += 在城中 * (float((pk::max(punch_person.stat[武将能力_政治] - 30, 0)) * 0.01 + 0.5) + (ch::has_skill(punch_person, 特技_发明) ? 0.5 : 0));
			}

			if (pk::is_valid_person_id(building_p.boat_person))
			{
				float 在城中 = 0;
				if (!pk::is_absent(boat_person))
					在城中 = 1.0;
				else
					在城中 = 0.8;
				造船效率 += 在城中 * (float((pk::max(boat_person.stat[武将能力_政治] - 30, 0)) * 0.01 + 0.5) + (ch::has_skill(boat_person, 特技_造船) ? 0.5 : 0));
			}

			if (pk::is_valid_person_id(building_p.inspections_person))
			{
				float 在城中 = 0;
				if (!pk::is_absent(inspections_person))
					在城中 = 1.0;
				else
					在城中 = 0.8;
				巡查效率 += 在城中 * (float((pk::max(inspections_person.stat[武将能力_武力] - 30, 0)) * 0.01 + 0.5) + (ch::has_skill(inspections_person, 特技_巡查) ? 0.5 : 0) + (ch::has_skill(inspections_person, 特技_辅佐) ? 0.5 : 0));
			}

			if (pk::is_valid_person_id(building_p.drill_person))
			{
				float 在城中 = 0;
				if (!pk::is_absent(drill_person))
					在城中 = 1.0;
				else
					在城中 = 0.8;
				训练效率 += 在城中 * (float((pk::max(drill_person.stat[武将能力_统率] - 30, 0)) * 0.01 + 0.5) + (ch::has_skill(drill_person, 特技_调练) ? 0.5 : 0) + (ch::has_skill(drill_person, 特技_奏乐) ? 0.5 : 0));
			}

			if (0 < ilban_list.length)
			{
				for (int i = 0; i < int(ilban_list.length); i++)
				{
					pk::person @ilban = ilban_list[i];
					if (!pk::is_unitize(ilban) and !pk::is_absent(ilban))
					{
						筑城效率 += float((pk::max(ilban.stat[武将能力_武力] - 30, 0)) * 0.01 * 0.1 + 0.1) + (ch::has_skill(ilban, 特技_坚城) ? 0.5 : 0) + (ch::has_skill(ilban, 特技_筑城) ? 0.5 : 0);
					}
				}
			}
		}
		// pk::trace(pk::format("城市id：{},城市名:{},base_gain:{},building_p.policy_expense:{},building_p.troops_effic:{}", building.get_id(),pk::decode(pk::get_name(building)),base_gain,building_p.policy_expense,building_p.troops_effic));

		if (building.get_id() < 42) // 城市判定
		{
			pk::city @city = pk::building_to_city(building);

			// 适当平衡大小城市的收支.
			int level = ch::get_city_level(building.get_id());

			// 统计设施数量
			int 兵舍数量 = 0;
			int 锻冶数量 = 0;
			int 厩舍数量 = 0;
			int 工房数量 = 0;
			int 造船数量 = 0;
			int 练兵所数量 = 0;

			for (int i = 0; i < int(city.max_devs); i++)
			{
				pk::building @building2 = city.dev[i].building;
				if (pk::is_alive(building2))
				{
					switch (building2.facility)
					{
					case 设施_兵营3级:
						兵舍数量++;
						break;
					case 设施_锻冶3级:
						锻冶数量++;
						break;
					case 设施_厩舍3级:
						厩舍数量++;
						break;
					case 设施_工房:
						工房数量++;
						break;
					case 设施_造船:
						造船数量++;
						break;
					case 设施_练兵所:
						练兵所数量++;
						break;
					}
				}
			}

			招兵效率 = 招兵效率 * (0.2 + sqrt(兵舍数量) * 0.8);
			兵装效率 = 兵装效率 * (0.2 + sqrt(锻冶数量) * 0.8);
			育马效率 = 育马效率 * (0.2 + sqrt(厩舍数量) * 0.8);
			攻具效率 = 攻具效率 * (0.2 + sqrt(工房数量) * 0.8);
			造船效率 = 造船效率 * (0.2 + sqrt(造船数量) * 0.8);
			训练效率 = 训练效率 * (0.8 + sqrt(练兵所数量) * 0.6);
		}

		else if (building.get_id() <= 87) // 小城市判定
		{
			// 适当平衡大小城市的收支.
			int level = ch::get_city_level(building.get_id());

			招兵效率 = 招兵效率;
			兵装效率 = 兵装效率;
			育马效率 = 育马效率;
			攻具效率 = 攻具效率;
			造船效率 = 造船效率;
			训练效率 = 训练效率;
		}

		building_p.troops_gain = int(招兵效率 * troops_base_gain * 35);

		int 兵装产量 = int(兵装效率 * int((sqrt(base_t.population * 0.1) + sqrt(building.troops)) * weapon_base_gain * 0.03));
		int 枪产量 = 兵装产量;
		int 戟产量 = 兵装产量;
		int 弩产量 = 兵装产量;

		int 战马产量 = int(育马效率 * int((sqrt(base_t.population * 0.1) + sqrt(building.troops)) * horse_base_gain * 0.05));
		int 攻具产量 = int(攻具效率 * int((sqrt(base_t.population * 0.1) + sqrt(building.troops)) * punch_base_gain * 0.02));
		int 舰船产量 = int(造船效率 * int((sqrt(base_t.population * 0.1) + sqrt(building.troops)) * boat_base_gain * 0.02));

		// if (building.get_id() == 13)
		//	pk::trace(pk::format("外部函数,城市id：{},城市名:{},攻具效率:{},造船效率:{},攻具产量:{},舰船产量:{}", building.get_id(), pk::decode(pk::get_name(building)), 攻具效率, 造船效率, punch_base_gain, boat_base_gain));

		// 从兵器编号推导到兵种编号,兵器:1是枪 2是戟 3是弩 4是骑兵 5是兵器 11是水军 ,分别对应的兵种的1 2 3 4 5
		//  city.tokusan[pk::equipment_id_to_heishu(11)] 完全等价于 city.tokusan[5]
		// 普通城市有工房和造船的情况下,大约每一季一个兵器,特产城市大约每个季度3个兵器.
		// 对于城市特产产量,枪戟马弩均为2倍,对于水军和兵器,生产能力足够的情况下,每个季度大约产出1个

		if (building.get_id() < 42) // 城市判定
		{
			pk::city @city = pk::building_to_city(building);
			if (city.tokusan[pk::equipment_id_to_heishu(兵器_枪)])
				枪产量 = int(枪产量 * 2);
			if (city.tokusan[pk::equipment_id_to_heishu(兵器_戟)])
				戟产量 = int(戟产量 * 2);
			if (city.tokusan[pk::equipment_id_to_heishu(兵器_弩)])
				弩产量 = int(弩产量 * 2);
			if (city.tokusan[pk::equipment_id_to_heishu(兵器_战马)])
				战马产量 = int(战马产量 * 2);
			if (city.tokusan[pk::equipment_id_to_heishu(5)])
				攻具产量 = int(攻具产量 * 2);
			if (city.tokusan[pk::equipment_id_to_heishu(11)])
				舰船产量 = int(舰船产量 * 2);
		}

		building_p.spear_gain = 0;
		building_p.halberd_gain = 0;
		building_p.bow_gain = 0;
		building_p.horse_gain = 0;
		building_p.well_gain = 0;
		building_p.punch_gain = 0;
		building_p.boat_gain = 0;

		pk::list<pk::person@> test_list;

		test_list.add(pk::get_person(building_p.weapon_person));
		int weapon_produce = PRODUCE::cal_produce_gain(building, test_list, 兵器_枪);
		//pk::trace("枪产量：1：" + 枪产量 + ",weapon_produce:" + weapon_produce);

		@test_list[0] = @pk::get_person(building_p.recruit_person);
		int recruit_num = RECRUIT_TROOPS_CHANGE::get_recruit_num(building, test_list);
		//pk::trace("征兵量：1：" + int(招兵效率 * troops_base_gain * 30) + ",recruit_num:" + recruit_num);

		if (!building.is_player() or building_p.weapon_choose == 0)
		{
			building_p.spear_gain = int(枪产量);
			building_p.halberd_gain = int(戟产量);
			building_p.bow_gain = int(弩产量);
			building_p.horse_gain = int(战马产量);
		}

		if (!building.is_player() or building_p.arms_choose == 0)
		{
			building_p.well_gain = int(攻具产量 / 2);
			building_p.punch_gain = int(攻具产量 / 2);
		}

		if (building.is_player() and building_p.weapon_choose > 0)
		{
			if (building_p.weapon_choose == 1)
				building_p.spear_gain = int(枪产量 * 3);
			if (building_p.weapon_choose == 2)
				building_p.halberd_gain = int(戟产量 * 3);
			if (building_p.weapon_choose == 3)
				building_p.bow_gain = int(弩产量 * 3);
		}

		if (building.is_player() and building_p.arms_choose > 0)
		{
			if (building_p.arms_choose == 1)
				building_p.well_gain = int(攻具产量);
			if (building_p.arms_choose == 2)
				building_p.punch_gain = int(攻具产量);
		}

		building_p.boat_gain = int(舰船产量);

		// 自动巡查
		// 巡查不应该有过大差距,给+1点补偿.





		bool 兵临城下 = pk::enemies_around(building);

		building_p.porder_gain = int(ceil(porder_base_gain * 巡查效率 * (兵临城下 ? 0.5 : 1)));

		building_p.porder_gain = pk::clamp(building_p.porder_gain, 0, 30); // 防止溢出,最大值为30.

		// 自动训练

		building_p.train_gain = int(ceil(train_base_gain * 训练效率  * (兵临城下 ? 0.5 : 1)));

		//训练的效果和兵力挂钩.
		building_p.train_gain = int(building_p.train_gain * 10.0f / pow(building.troops + 100 , 0.2f)) ;

		building_p.train_gain = pk::clamp(building_p.train_gain, 0, 30); // 防止溢出,最大值为30.

		// 自动筑城
		building_p.repair_gain = int(ceil(repair_base_gain  * 筑城效率));

		// building_p.repair_gain = pk::clamp(building_p.repair_gain, 0, 30); // 防止溢出,最大值为30.
		// pk::trace(pk::format("城市id：{},城市名:{},筑城效率:{},,筑城开支:{},筑城增量:{},城防为:{}", building.get_id(),pk::decode(pk::get_name(building)),building_p.repair_effic,building_p.repair_gain,building_p.city_defense));
		// pk::message_box(pk::encode(pk::format("报告,\x1b[2x{}\x1b[0x的巡查增量为{}", pk::decode(pk::get_name(building)), building_p.porder_gain)));

		// pk::message_box(pk::encode(pk::format("报告主公,\x1b[2x{}\x1b[0x的人口是{}.", pk::decode(pk::get_name(building)),base_t.population)));
		// 伤兵恢复系统 伤兵会以每回合10%的速度恢复,最小为30.
		// 回归伤兵系统 回归伤兵会以每回合10%的速度回归,最小为30.
		/*
			if (pk::get_troops(building) < pk::get_max_troops(building) and int(兵临城下) == 0)
			{
				int 伤兵回复量 = 0;
				if (base_t.wounded > 0)
					pk::max(int(base_t.wounded * 0.1), 30);
				base_t.wounded -= pk::min(伤兵回复量, base_t.wounded);
				ch::add_troops(building, int(伤兵回复量), true);
			}

			int 伤兵回归量 = pk::min(int(base_t.return_pop * 0.1), 30);
			base_t.return_pop -= pk::min(伤兵回归量, base_t.return_pop);
			base_t.wounded += 伤兵回归量;
	   */
	}

	void reward_building(pk::building @building)
	{
		BaseInfo @base_t = @base_ex[building.get_id()];
		BuildingInfo @building_p = @building_ex[building.get_id()];
		pk::force @force0 = pk::get_force(building.get_force_id());

		int final_revenue = pk::clamp(int(building_p.building_revenue / 3), 0, 10000);
		int base_gain = int(sqrt(final_revenue) * 30);

		bool 兵临城下 = pk::enemies_around(building);
		int 总开支 = int((building_p.troops_effic + building_p.repair_effic + building_p.porder_effic + building_p.train_effic + building_p.weapon_effic + building_p.horse_effic + building_p.punch_effic + building_p.boat_effic));

		if (building.get_id() >= 0 and building.get_id() < 87)
		{
			if (pk::get_troops(building) < pk::get_max_troops(building) and !兵临城下 and building_p.troops_effic > 0)
			{
				if (int(pk::get_gold(building)) - 总开支 > 0)
				{
					if (building_p.troops_gain <= base_t.mil_pop_all)
					{
						ch::add_troops(building, building_p.troops_gain, true);
						// 招兵的同时减少兵役人口,和1.2倍的人口
						base_t.mil_pop_all -= building_p.troops_gain;			// 兵役人口相应扣除
						base_t.population -= int(building_p.troops_gain * 1.2); // 人口相应扣除
						pk::add_gold(building, -building_p.troops_effic, true);
						pk::add_energy(building, -int(building_p.troops_gain * 0.002), true);		// 士气降低0.2%
						ch::add_public_order(building, -int(building_p.troops_gain * 0.002), true); // 治安降低0.2%
					}
					else
					{
						if (building.is_player() and base_t.mil_pop_all < 500)
						{
							pk::message_box(pk::encode(pk::format("报告,\x1b[2x{}\x1b[0x的兵役人口不足以进行招兵,请重新分配.现兵役人口{}", pk::decode(pk::get_name(building)), base_t.mil_pop_all)));
						}
					}
				}
				else
				{
					if (building.is_player())
					{
						pk::message_box(pk::encode(pk::format("报告,\x1b[2x{}\x1b[0x的资金本旬不足以进行自动化内政,请重新分配资金.现有资金{}", pk::decode(pk::get_name(building)), pk::get_gold(building))));
					}
				}
			}

			if (!兵临城下)
			{
				if (int(pk::get_gold(building)) - 总开支 > 0)
				{

					if (pk::get_weapon_amount(building, 1) < pk::get_max_weapon_amount(building, 1))
						pk::add_weapon_amount(building, 兵器_枪, building_p.spear_gain, true);
					if (pk::get_weapon_amount(building, 2) < pk::get_max_weapon_amount(building, 2))
						pk::add_weapon_amount(building, 兵器_戟, building_p.halberd_gain, true);
					if (pk::get_weapon_amount(building, 3) < pk::get_max_weapon_amount(building, 3))
						pk::add_weapon_amount(building, 兵器_弩, building_p.bow_gain, true);
					if (pk::get_weapon_amount(building, 4) < pk::get_max_weapon_amount(building, 4))
						pk::add_weapon_amount(building, 兵器_战马, building_p.horse_gain, true);

					building_p.well += building_p.well_gain;
					building_p.punch += building_p.punch_gain;
					building_p.boat += building_p.boat_gain;

					pk::add_gold(building, -building_p.weapon_effic, true);
					pk::add_gold(building, -building_p.horse_effic, true);
					pk::add_gold(building, -building_p.punch_effic, true);
					pk::add_gold(building, -building_p.boat_effic, true);

					if (building_p.well > 1000)
					{
						if (pk::has_tech(building, 技巧_投石开发))
						{
							pk::add_weapon_amount(building, 兵器_投石, 1, true);
						}
						else
						{
							pk::add_weapon_amount(building, 兵器_井阑, 1, true);
						}
						building_p.well -= 1000;
					}

					if (building_p.punch > 1000)
					{
						if (pk::has_tech(building, 技巧_木兽开发))
						{
							pk::add_weapon_amount(building, 兵器_木兽, 1, true);
						}
						else
						{
							pk::add_weapon_amount(building, 兵器_冲车, 1, true);
						}
						building_p.punch -= 1000;
					}

					if (building_p.boat > 1000)
					{
						if (pk::has_tech(building, 技巧_投石开发))
						{
							pk::add_weapon_amount(building, 兵器_斗舰, 1, true);
						}
						else
						{
							pk::add_weapon_amount(building, 兵器_楼船, 1, true);
						}

						building_p.boat -= 1000;
					}

					// pk::add_gold(building, -building_p.produce_effic * base_gain / 100, true);
				}
			}

			// 自动巡查
			if (building_p.porder_effic > 0)
			{
				if (int(pk::get_gold(building)) - 总开支 > 0)
				{
					ch::add_public_order(building, int(building_p.porder_gain), true);
					pk::add_gold(building, -building_p.porder_effic, true);
				}
			}

			// 自动训练
			if (building_p.train_effic > 0)
			{
				if (int(pk::get_gold(building)) - 总开支 > 0)
				{
					pk::add_energy(building, int(building_p.train_gain), true);
					pk::add_gold(building, -building_p.train_effic, true);
				}
			}

			// AI不会分配小城市,因此小城市每回合给2点士气补偿.
			if (building.get_id() >= 城市_末 and !building.is_player())
				pk::add_energy(building, 2, true);

			// AI不会分配小城市,因此小城市每回合给2点治安补偿.
			if (building.get_id() >= 城市_末 and !building.is_player())
				ch::add_public_order(building, 2, true);

			// 城防系统
			if (building_p.repair_effic > 0 and building_p.city_defense - pk::get_max_hp(building) < 0)
			{
				if (int(pk::get_gold(building)) - 总开支 > 0)
				{
					if (building.hp + building_p.city_defense < 15000)
					{
						building_p.city_defense += int(building_p.repair_gain * (兵临城下 ? 0 : 1));
						pk::add_gold(building, -building_p.repair_effic, true);
					}
				}
			}
			building_p.city_defense = pk::clamp(building_p.city_defense, 0, pk::get_max_hp(building)); // 防止溢出,额外城防最大为原始最大城防.

			if ((int(building.hp) - int(pk::get_max_hp(building)) < 0) and (building_p.city_defense - (pk::get_max_hp(building) - building.hp) > 0))
			{
				int 可修复防御 = pk::get_max_hp(building) - building.hp;
				int 修复防御 = pk::min(可修复防御, building_p.city_defense);
				building_p.city_defense -= 修复防御;
				pk::add_hp(building, 修复防御, true);
			}

			building.update();

			// 内政官加功绩
			pk::person @inspections_person = pk::get_person(building_p.inspections_person);
			pk::person @drill_person = pk::get_person(building_p.drill_person);
			pk::person @recruit_person = pk::get_person(building_p.recruit_person);
			pk::person @weapon_person = pk::get_person(building_p.weapon_person);
			pk::person @horse_person = pk::get_person(building_p.horse_person);
			pk::person @punch_person = pk::get_person(building_p.punch_person);
			pk::person @boat_person = pk::get_person(building_p.boat_person);

			//为了平衡功绩增加,功绩和开支的具体数值直接挂钩
			//效果也会随着功绩增加而增加

			if (pk::is_valid_person_id(building_p.inspections_person))
				pk::add_kouseki(inspections_person, building_p.porder_effic);

			if (pk::is_valid_person_id(building_p.recruit_person))
				pk::add_kouseki(recruit_person, building_p.troops_effic);

			if (pk::is_valid_person_id(building_p.drill_person))
				pk::add_kouseki(drill_person, building_p.train_effic);

			if (pk::is_valid_person_id(building_p.horse_person))
				pk::add_kouseki(horse_person, building_p.horse_effic);

			if (pk::is_valid_person_id(building_p.weapon_person))
				pk::add_kouseki(weapon_person, building_p.weapon_effic);

			if (pk::is_valid_person_id(building_p.punch_person))
				pk::add_kouseki(punch_person, building_p.punch);

			if (pk::is_valid_person_id(building_p.boat_person))
				pk::add_kouseki(boat_person, building_p.boat_effic);
		}
	}

	void 信息显示_顶部面板()
	{
		pk::point cursor_pos = pk::get_cursor_hex_pos();
		if (!pk::is_valid_pos(cursor_pos))
			return;

		// 光标上指示的建筑物
		pk::building @building = pk::get_building(cursor_pos);
		if (building is null || building.facility > 2)
			return; // 城港官才显示

		// if (!building.is_player()) return;
		// if (!pk::is_player_controlled(building)) return;
		// if (building.get_force_id() != pk::get_current_turn_force_id()) return;
		// pk::trace("人口");

		string building_name = pk::decode(pk::get_name(building));

		string title = pk::format("据点信息(\x1b[1x{}\x1b[0x)", building_name);

		int middle = int(pk::get_resolution().width) / 2;
		int left = middle - 200;
		int right = middle + 200;
		int top = 5 + (pk::get_player_count() == 0 ? 100 : 0);
		int bottom = top + 80;
		int base_id = building.get_id();
		// pk::draw_rect(pk::rectangle(left, top, right, bottom), 0xff00ccff);
		BaseInfo @base_t = @base_ex[base_id];
		BuildingInfo @building_p = @building_ex[base_id];
		string base_policy_info;

		int final_revenue = pk::clamp(int(building_p.building_revenue / 3), 0, 10000);
		int base_gain = int(sqrt(final_revenue) * 30);

		int 总开支 = int((building_p.troops_effic + building_p.repair_effic + building_p.porder_effic + building_p.train_effic + building_p.weapon_effic + building_p.horse_effic + building_p.punch_effic + building_p.boat_effic));

		switch (building_p.building_policy)
		{
		case 0:
			base_policy_info = "无";
			break;
		case 1:
			base_policy_info = "积极备战";
			break;
		case 2:
			base_policy_info = "后方生产";
			break;
		case 3:
			base_policy_info = "均衡分配";
			break;
		case 4:
			base_policy_info = "城下作战";
			break;
		default:
			base_policy_info = "无";
			break;
		}

		// pk::draw_text(pk::encode(title), pk::point(left + 5, top + 5), 0xffffffff, FONT_BIG, 0xff000000);

		// if (false and base_id >= 据点_城市末 and base_id < 据点_末)
		// {
		// 	// BaseInfo@ building_p = @base_ex[base_id];
		// 	string info_治安 = pk::format("治安: \x1b[1x{}\x1b[0x", building_p.public_order);
		// 	pk::draw_text(pk::encode(info_治安), pk::point(left + 10, top + 40 + 信息行数 * 20), 0xffffffff, FONT_SMALL, 0xff000000);
		// 	信息行数 += 1;
		// }

		//if (base_id < 据点_末 and (!pk::is_fog_set() or building.is_player() or pk::get_player_count() == 0 or (pk::is_fog_set() and base_t.spy_lv[pk::get_current_turn_force_id()] > 0)))
		if (true)
		{
			// 内政自动::calculate_building(pk::get_building(base_id));
			//  if (base_id == 城市_襄阳) pk::trace(pk::format("func_信息显示_人口信息,襄阳人口：{}", building_p.population));
			//  BaseInfo@ building_p = @base_ex[base_id];
			//  pk::point leftdown = pk::point(middle + 140, top + 40 + (信息行数 + 3) * 20 +5);

			// pk::draw_filled_rect(pk::rectangle(pk::point(left, top), leftdown), ((0xff / 2) << 24) | 0x010101);//((0xff / 2) << 24) | 0x777777

			string info_人口 = pk::format("人口:\x1b[1x{}\x1b[0x", base_t.population);
			building_name = pk::decode(pk::get_name(building));
			title = pk::format("城市信息(\x1b[1x{}\x1b[0x)", building_name);
			int level = ch::get_city_level(base_id);
			string info_预期金;
			int 信息行数 = 0;

			// pk::history_log(building.pos, pk::get_force(building.get_force_id()).color, pk::encode(pk::format("\x1b[1x{}的预期金小于0，请谨慎分配。预期金：\x1b[1x{}", building_name, int(building_p.troops_expense),0)));

			string level_name = ch::get_level_string(level);
			string info_规模 = pk::format("城市规模:\x1b[1x{}\x1b[0x", level_name);
			string info_总兵役 = pk::format("总兵役:\x1b[1x{}\x1b[0x", base_t.mil_pop_all);
			string info_伤兵 = pk::format("伤兵:\x1b[16x{}\x1b[0x", base_t.wounded);
			// string info_额外城防 = pk::format("额外城防:\x1b[16x{}\x1b[0x", building_p.city_defense);
			string info_城市士气 = pk::format("城市士气:\x1b[16x{}\x1b[0x", building.energy);
			string info_总城防 = pk::format("总城防:\x1b[16x{}\x1b[0x", building_p.city_defense + building.hp);
			string info_筑城开支 = pk::format("筑城开支:\x1b[1x{}\x1b[0x", building_p.repair_effic);
			string info_筑城量 = pk::format("筑城量:\x1b[1x{}\x1b[0x", building_p.repair_gain);
			string info_金收入 = pk::format("月度总收入:\x1b[1x{}\x1b[0x", int(building_p.building_revenue));
			string info_内政开支 = pk::format("旬内政开支:\x1b[1x{}\x1b[0x", 总开支);
			string info_军饷 = pk::format("旬军饷开支:\x1b[1x{}\x1b[0x", int(building_p.troops_expense));
			string info_维护费 = pk::format("兵装维护费:\x1b[1x{}\x1b[0x", int(building_p.weapon_expense));

			// 显示治安
			base_id = building.get_id();
			string info_治安;
			if (base_id >= 据点_城市末 and base_id < 据点_末)
				info_治安 = pk::format("治安:\x1b[1x{}\x1b[0x", base_ex[base_id].public_order);
			if (base_id >= 0 and base_id <= 据点_城市末)
				info_治安 = pk::format("治安:\x1b[1x{}\x1b[0x", pk::building_to_city(building).public_order);

			// 绘制阴影和标题
			middle = int(pk::get_resolution().width) / 2;
			left = middle - 200;
			right = middle + 200;
			top = 5;
			bottom = top + 80;
			pk::point leftdown = pk::point(middle + 100, top + 40 + 6 * 20 + 5);

			pk::draw_filled_rect(pk::rectangle(pk::point(left, top), leftdown), ((0xff / 2) << 24) | 0x010101); //((0xff / 2) << 24) | 0x777777
			pk::draw_text(pk::encode(title), pk::point(left + 5, top + 5), 0xffffffff, FONT_BIG, 0xff000000);

			// 绘制主要信息
			pk::draw_text(pk::encode(info_治安), pk::point(middle - 10, top + 8), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_人口), pk::point(left + 20, top + 40 + (信息行数 + 0) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_规模), pk::point(middle - 30, top + 40 + (信息行数 + 0) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_总兵役), pk::point(left + 20, top + 40 + (信息行数 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_伤兵), pk::point(middle - 30, top + 40 + (信息行数 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_城市士气), pk::point(left + 20, top + 40 + (信息行数 + 2) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			// pk::draw_text(pk::encode(info_额外城防), pk::point(left + 20, top + 40 + (信息行数 + 2) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_总城防), pk::point(middle - 30, top + 40 + (信息行数 + 2) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_筑城开支), pk::point(left + 20, top + 40 + (信息行数 + 3) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_筑城量), pk::point(middle - 30, top + 40 + (信息行数 + 3) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_金收入), pk::point(left + 20, top + 40 + (信息行数 + 4) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_内政开支), pk::point(middle - 30, top + 40 + (信息行数 + 4) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_维护费), pk::point(left + 20, top + 40 + (信息行数 + 5) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_军饷), pk::point(middle - 30, top + 40 + (信息行数 + 5) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			/*
			pk::draw_text(pk::encode(info_招兵开支), pk::point(left + 20, top + 40 + (信息行数 + 3) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_基础招兵), pk::point(middle - 30, top + 40 + (信息行数 + 3) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_生产开支), pk::point(left + 20, top + 40 + (信息行数 + 4) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_基础产量), pk::point(middle - 30, top + 40 + (信息行数 + 4) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_筑城开支), pk::point(left + 20, top + 40 + (信息行数 + 5) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_筑城增量), pk::point(middle - 30, top + 40 + (信息行数 + 5) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_治安开支), pk::point(left + 20, top + 40 + (信息行数 + 6) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_治安增量), pk::point(middle - 30, top + 40 + (信息行数 + 6) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_训练开支), pk::point(left + 20, top + 40 + (信息行数 + 7) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_训练增量), pk::point(middle - 30, top + 40 + (信息行数 + 7) * 20), 0xffffffff, FONT_SMALL, 0xff000000);

			pk::draw_text(pk::encode(info_城市方针), pk::point(left + 20, top + 40 + (信息行数 + 9) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_方针开支), pk::point(middle - 30, top + 40 + (信息行数 + 9) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			 k::draw_text(pk::encode(info_开支1), pk::point(left + 20, top + 40 + (信息行数 + 10) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_开支2), pk::point(middle - 30, top + 40 + (信息行数 + 10) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
			*/

			// 信息行数 += 3;
		}
	}

	bool is_frontline_base(pk::building @building)
	{
		for (int i = 0; i < 据点_末; i++)
		{
			if (pk::is_neighbor_base(building.get_id(), i) and pk::is_enemy(building, pk::get_building(i)))
				return true;
		}
		array<pk::point> arr = pk::range(building.pos, 1, 6);
		for (int i = 0; i < int(arr.length); i++)
		{
			pk::unit @unit0 = pk::get_unit(arr[i]);
			if (unit0 !is null)
			{
				if (pk::is_enemy(building, unit0))
					return true;
			}
		}
		return false;
	}

	int func_附近_敌城市数(pk::city @city, int 距离)
	{
		int enemy_city_count = 0;

		array<pk::city @> cities = pk::list_to_array(pk::get_city_list());
		for (int i = 0; i < int(cities.length); i++)
		{
			pk::city @neighbor_city = cities[i];

			int distance = pk::get_city_distance(city.get_id(), neighbor_city.get_id());

			if (distance > 距离)
				continue;
			if (!pk::is_enemy(city, neighbor_city))
				continue;

			enemy_city_count++;
		}

		return enemy_city_count;
	}

	float AI设置内政开支(int 项目, float 剩余比例, pk::building @building, int num, int person_count)
	{
		float 开支比例 = 0;
		if (person_count == 1)
			开支比例 = 1;
		else if (person_count == 2)
			开支比例 = 0.6;
		else if (person_count == 3)
			开支比例 = 0.5;
		else
			开支比例 = sqrt(num + 2) * 0.12;
		float 开支 = 剩余比例;
		switch (项目)
		{
		case 内政自动化_招兵:
			开支 = AI招兵设置(building, 开支比例, 剩余比例);
			break;
		case 内政自动化_训练:
			开支 = AI训练设置(building, 开支比例, 剩余比例);
			break;
		case 内政自动化_巡查:
			开支 = AI巡查设置(building, 开支比例, 剩余比例);
			break;
		case 内政自动化_兵装:
			开支 = AI兵装设置(building, 开支比例, 剩余比例);
			break;
		case 内政自动化_育马:
			开支 = AI育马设置(building, 开支比例, 剩余比例);
			break;
		case 内政自动化_攻具:
			开支 = AI攻具设置(building, 开支比例, 剩余比例);
			break;
		case 内政自动化_造船:
			开支 = AI造船设置(building, 开支比例, 剩余比例);
			break;
		default:
			break;
		}
		//pk::trace("项目" + 项目 + ",开支" + 开支);
		return 开支;
	}

	float AI招兵设置(pk::building @building, float ratio, float surplus)
	{
		int building_id = building.get_id();
		BaseInfo @base_t = @base_ex[building.get_id()];
		BuildingInfo @building_p = @building_ex[building.get_id()];

		building_p.troops_effic = 0;
		int 总兵装 = pk::get_weapon_amount(building, 1) + pk::get_weapon_amount(building, 2) + pk::get_weapon_amount(building, 3) + pk::get_weapon_amount(building, 4);
		int 兵装兵力差 = 总兵装 - building.troops;
		float 兵力人口比 = building.troops / float(base_t.population); // 人口越少,养兵能力越差,收支最终会崩溃,因此要考虑比值

		if (兵力人口比 > 0.1)
			ratio *= pk ::clamp(1 - 兵力人口比 * 2.0, 0.0, 1.0);
		//pk::trace("项目招兵" + ",开支1:" + ratio);
		// 如果兵役人口少,或者兵力已经很多,或者兵多兵装少,则降低开支
		if (base_t.mil_pop_av < 5000 or building.troops > int(pk::get_max_troops(building) * 0.8) or 兵装兵力差 < 0)
			ratio *= 0.5;
		//pk::trace("项目招兵" + ",开支2:" + ratio);
		// 如果兵役足够,或者兵力太低,或者兵少兵装多,则增加开支
		if ((base_t.mil_pop_av > 10000 and building.troops < int(pk::get_max_troops(building) * 0.2)) or (building.troops < int(pk::get_max_troops(building) * 0.8) and 兵装兵力差 > 50000))
			ratio *= 1.5;
		//pk::trace("项目招兵" + ",开支3:" + ratio);
		// 满兵或者无兵役人口.或无官员则把剩余比例顺延
		if (pk::is_valid_person_id(building_p.recruit_person) and base_t.mil_pop_av > 3000 and building.troops < pk::get_max_troops(building))
		{
			//pk::trace("surplus" + surplus + ",ratio:" + ratio + ",building_p.policy_expense:" + building_p.policy_expense + ",building_p.building_revenue:" + building_p.building_revenue);
			building_p.troops_effic = int(surplus * ratio * building_p.policy_expense * building_p.building_revenue * 0.01 / 3);
			return surplus * (1 - ratio);
		}
		else
			return surplus;
	}

	float AI训练设置(pk::building @building, float ratio, float surplus)
	{
		int building_id = building.get_id();
		BaseInfo @base_t = @base_ex[building.get_id()];
		BuildingInfo @building_p = @building_ex[building.get_id()];

		building_p.train_effic = 0;
		if (building.energy < 90)
			ratio *= 1.2;
		else if (building.energy < 95)
			ratio *= 0.8;
		else
			ratio *= 0.5;
		// 满士气.无官员则把剩余比例顺延
		if (pk::is_valid_person_id(building_p.inspections_person) and building.energy != pk::get_max_energy(building))
		{
			building_p.train_effic = int(surplus * ratio * building_p.policy_expense * building_p.building_revenue * 0.01 / 3);
			return surplus * (1 - ratio);
		}

		else
			return surplus;
	}

	float AI巡查设置(pk::building @building, float ratio, float surplus)
	{
		int building_id = building.get_id();
		BaseInfo @base_t = @base_ex[building.get_id()];
		BuildingInfo @building_p = @building_ex[building.get_id()];

		int order = 0;
		if (building_id >= 据点_城市末 and building_id < 据点_末)
			order = base_ex[building_id].public_order;
		if (building_id >= 0 and building_id < 据点_城市末)
			order = pk::building_to_city(building).public_order;

		building_p.porder_effic = 0;

		if (order < 50)
			ratio *= 2.0;
		else if (order < 70)
			ratio *= 1.5;
		else if (order > 95)
			ratio *= 0.5;
		else
			ratio *= 0.6;

		if (pk::is_valid_person_id(building_p.inspections_person) and order < 99)
		{
			building_p.porder_effic = int(surplus * ratio * building_p.policy_expense * building_p.building_revenue * 0.01 / 3);
			// pk::trace(pk::format("2次城市id：{},城市名:{},order:{},巡查开支:{},剩余:{},比例:{},最终值:{}", building.get_id(),pk::decode(pk::get_name(building)),order ,building_p.porder_effic,surplus,ratio,int(surplus * ratio * building_p.policy_expense * building_p.building_revenue * 0.01 / 3)));

			return surplus * (1 - ratio);
		}

		else
			return surplus;
	}

	float AI兵装设置(pk::building @building, float ratio, float surplus)
	{
		int building_id = building.get_id();
		BaseInfo @base_t = @base_ex[building.get_id()];
		BuildingInfo @building_p = @building_ex[building.get_id()];

		building_p.weapon_effic = 0;

		int 总兵装 = pk::get_weapon_amount(building, 1) + pk::get_weapon_amount(building, 2) + pk::get_weapon_amount(building, 3) + pk::get_weapon_amount(building, 4);
		int 兵装兵力差 = 总兵装 - building.troops;

		// 如果兵装严重不足,则大幅增加开支
		if (兵装兵力差 < -10000)
			ratio *= 2.0;

		// 如果兵装不足,则增加开支
		if (兵装兵力差 < 20000)
			ratio *= 1.5;

		// 如果兵装太多,兵力不足,则减少开支
		if (兵装兵力差 > 50000 and building.troops < int(pk::get_max_troops(building) * 0.3))
			ratio *= 0.5;

		// 无官员则把剩余比例顺延,剩下的钱有多少用多少.
		// 兵装选择的优化在兵装选择环节.
		if (pk::is_valid_person_id(building_p.weapon_person) and pk::get_weapon_amount(building, 1) + pk::get_weapon_amount(building, 2) + pk::get_weapon_amount(building, 3) < pk::get_max_weapon_amount(building, 1) + pk::get_max_weapon_amount(building, 2) + pk::get_max_weapon_amount(building, 3))
		{
			building_p.weapon_effic = int(surplus * ratio * building_p.policy_expense * building_p.building_revenue * 0.01 / 3);
			return surplus * (1 - ratio);
		}

		else
			return surplus;
	}

	float AI育马设置(pk::building @building, float ratio, float surplus)
	{
		int building_id = building.get_id();
		BaseInfo @base_t = @base_ex[building.get_id()];
		BuildingInfo @building_p = @building_ex[building.get_id()];

		building_p.horse_effic = 0;

		// 无官员则把剩余比例顺延,剩下的钱有多少用多少.
		// 兵装选择的优化在兵装选择环节.
		if (pk::is_valid_person_id(building_p.horse_person) and pk::get_weapon_amount(building, 4) < pk::get_max_weapon_amount(building, 4))
		{
			building_p.horse_effic = int(surplus * ratio * building_p.policy_expense * building_p.building_revenue * 0.01 / 3);

			// if(building_id  == 13) pk::trace(pk::format("育马函数内部,城市id：{},城市名:{},surplus :{},ratio :{},方针开支:{},马开支:{}", building.get_id(),pk::decode(pk::get_name(building)),surplus , ratio,building_p.policy_expense ,building_p.horse_effic));

			return surplus * (1 - ratio);
		}

		else
			return surplus;
	}

	float AI攻具设置(pk::building @building, float ratio, float surplus)
	{
		int building_id = building.get_id();
		BaseInfo @base_t = @base_ex[building.get_id()];
		BuildingInfo @building_p = @building_ex[building.get_id()];

		building_p.punch_effic = 0;

		// 无官员则把剩余比例顺延,剩下的钱有多少用多少.
		// 兵装选择的优化在兵装选择环节.
		if (pk::is_valid_person_id(building_p.punch_person))
		{
			building_p.punch_effic = int(surplus * ratio * building_p.policy_expense * building_p.building_revenue * 0.01 / 3);
			return surplus * (1 - ratio);
		}

		else
			return surplus;
	}

	float AI造船设置(pk::building @building, float ratio, float surplus)
	{
		int building_id = building.get_id();
		BaseInfo @base_t = @base_ex[building.get_id()];
		BuildingInfo @building_p = @building_ex[building.get_id()];

		building_p.boat_effic = 0;

		// 无官员则把剩余比例顺延,剩下的钱有多少用多少.
		// 兵装选择的优化在兵装选择环节.
		if (pk::is_valid_person_id(building_p.boat_person))
		{
			building_p.boat_effic = int(surplus * ratio * building_p.policy_expense * building_p.building_revenue * 0.01 / 3);
			return surplus * (1 - ratio);
		}

		else
			return surplus;
	}

	float AI筑城设置(pk::building @building, float ratio, float surplus)
	{
		int building_id = building.get_id();
		BaseInfo @base_t = @base_ex[building.get_id()];
		BuildingInfo @building_p = @building_ex[building.get_id()];

		// 筑城略有不同,筑城是基于所有武将的,没有特定官员.如果完全人就跳过.
		if (pk::get_idle_person_list(pk::get_building(building_id)).count > 1)
		{
			building_p.repair_effic = int(surplus * ratio * building_p.policy_expense * building_p.building_revenue * 0.01 / 3);
			return surplus * (1 - ratio);
		}

		else
			return surplus;
	}

	float get_policy_buf(int policy)
	{
		float buf;
		switch (policy)
		{
		case 方针_中华统一:
			buf = 1.2f;
			break;
		case 方针_地方统一:
			buf = 1.1;
			break;
		case 方针_州统一:
			buf = 1.1f;
			break;
		case 方针_现状维持:
			buf = 1.0f;
			break;
		case 方针_吴越割据:
			buf = 1.0f;
			break;
		case 方针_巴蜀割据:
			buf = 1.0f;
			break;
		default:
			buf = 1.0f;
			break;
		}

		return buf;
	}

	// 根据规则获取武将
	pk::list<pk::person @> get_person_list(pk::building @base, int type)
	{
		int param1; // 排序规则1
		int param2; // 排序规则2
		switch (type)
		{
		case 内政自动化_巡查:
			param1 = int(pk::core["inspection.stat"]);
			param2 = -1;
			break;

		case 内政自动化_训练:
			param1 = int(pk::core["train.stat"]);
			param2 = -1;
			break;

		case 内政自动化_招兵:
			param1 = int(pk::core["recruit.stat"]);
			param2 = int(pk::core["recruit.skill"]);
			break;

		case 内政自动化_兵装:
			param1 = int(pk::core["weapon_produce.stat"]);
			param2 = int(pk::core["weapon_produce.smith_skill"]);
			break;

		case 内政自动化_育马:
			param1 = int(pk::core["weapon_produce.stat"]);
			param2 = 特技_繁殖;
			break;

		case 内政自动化_攻具:
			param1 = int(pk::core["weapon_produce.stat"]);
			param2 = 特技_发明;
			break;

		case 内政自动化_造船:
			param1 = int(pk::core["weapon_produce.stat"]);
			param2 = 特技_造船;
			break;
		}
		return sort_person_list(base, param1, param2);
	}

	int cmd_stat;
	int cmd_skill;
	pk::list<pk::person @> sort_person_list(pk::building @base, int stat, int skill)
	{
		cmd_stat = stat;
		cmd_skill = skill;
		pk::list<pk::person @> actors;

		pk::list<pk::person @> list_person = pk::get_person_list(base, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般));
		if (list_person.count == 0) return actors;
		// 需要排除其他已经设定过的执行者

		// if (调试模式)
		// pk::message_box(pk::encode(pk::format("排序前，目标数量不为0", 1)));

		list_person.sort(function(a, b) {
			if (cmd_skill >= 0)
			{
				bool a_skill = ch::has_skill(a, cmd_skill);
				bool b_skill = ch::has_skill(b, cmd_skill);
				if (a_skill and !b_skill)
					return true;
				if (!a_skill and b_skill)
					return false;

				return a.stat[cmd_stat] > b.stat[cmd_stat];
			}

			return a.stat[cmd_stat] > b.stat[cmd_stat];
		});

		return list_person;
	}

	void 自动任命官员(pk::force@ force, pk::building @building)
	{
		pk::person @taishu = pk::get_person(pk::get_taishu_id(building));
		if (pk::get_taishu_id(building) == -1) { pk::message_box(pk::encode("当前城池无太守")); return ; } // 无太守
		if (taishu.service != building.get_id()) { pk::message_box(pk::encode("太守不在城里")); return ; } // 太守不在城里
		auto district = pk::get_district(pk::get_district_id(force, 1));
		if (district.ap < RANK_ACTION_COST) { pk::message_box(pk::encode(pk::format("军团行动力不足，需要\x1b[27x{}\x1b[0x行动力", RANK_ACTION_COST))); return ; }// 军团行动力不足
		if (force.tp < RANK_TP_COST) { pk::message_box(pk::encode(pk::format("技巧不足，需要\x1b[27x{}\x1b[0x技巧", RANK_TP_COST))); return ; } // 技巧不足
      	if (pk::choose(pk::encode("要自动任命内政官吗?"), {pk::encode(" 是 "), pk::encode(" 否 ")}) == 1) return;
        
		int building_id = building.get_id();
		BaseInfo @base_t = @base_ex[building_id];

		pk::building @base = pk::get_building(building_id);
		pk::list<pk::person @> person_list;

		BuildingInfo @building_p = @building_ex[base.get_id()];
		array<int> rank_type = building_p.rank_type;

		for (int index = 0; index < int(rank_type.length); index += 1)
		{
			int type = rank_type[index];
			building_p.set_charge(-1, type);
			// 获取ai武将排序
			person_list = get_person_list(base, type);
			if (person_list.count > 0)
			{
				for (int p = 0; p < person_list.count; p += 1)
				{
					pk::person @person = person_list[p];
					if (index == 0)
					{
						building_p.set_charge(person.get_id(), type);
						break;
					}
					else
					{
						bool checkout_role = true;
						for (int rank = 0; rank < index; rank += 1)
						{
							checkout_role = (checkout_role and person.get_id() != building_p.get_charge(rank_type[rank]));
						}
						if (checkout_role)
						{
							building_p.set_charge(person.get_id(), type);
							// pk::trace(pk::format("城市id：{},城市名:{},人物ID:{},type:{}", building.get_id(),pk::decode(pk::get_name(building)),person.get_id(),building_p.well_gain,type));
							break;
						}
					}
				}
			}
		}

		pk::message_box(pk::encode("已完成自动内政官的任命。"), taishu);
		taishu.action_done == true;

	    pk::add_ap(district, -int(RANK_ACTION_COST));
	    pk::add_tp(force, -RANK_TP_COST, building.get_pos());
	    district.update();

	    calculate_building2(building, false);
	    信息显示_顶部面板();
	}

	void 自动设定分支(pk::force@ force, pk::building @building)
	{
		int base_id = building.get_id();
		BuildingInfo @building_p = @building_ex[base_id];
		auto district = pk::get_district(pk::get_district_id(force, 1));
		if (district.ap < EFFIC_ACTION_COST) { pk::message_box(pk::encode(pk::format("军团行动力不足，需要\x1b[27x{}\x1b[0x行动力", EFFIC_ACTION_COST))); return ; }// 军团行动力不足
		if (force.tp < EFFIC_TP_COST) if (force.tp < RANK_TP_COST) { pk::message_box(pk::encode(pk::format("技巧不足，需要\x1b[27x{}\x1b[0x技巧", EFFIC_TP_COST))); return ; } // 技巧不足
		
		pk::person @kunshu = pk::get_person(pk::get_kunshu_id(building));
		array<string> select_item = {
			pk::encode("不设定"),
			pk::encode("积极备战"),
			pk::encode("后方生产"),
			pk::encode("均衡分配"),
			pk::encode("城下作战")
		};

		int choise = pk::choose(select_item, pk::encode("城市设定为哪个方针呢？"), kunshu);

		if (choise == 0) return;
		string policy = pk::decode(select_item[choise]);

		pk::message_box(pk::encode(pk::format("\x1b[2x{}\x1b[0x花费多少百分比的收入用于 \x1b[16x{}\x1b[0x？", pk::decode(pk::get_name(building)), policy)));

		pk::int_bool deal2 = pk::numberpad(pk::encode("内政开支"), 0, 150, 0, cast<pk::numberpad_t @>(function(line, original_value, current_value) { return ""; }));

		building_p.policy_expense = deal2.first;
		building_p.building_policy = choise;
		array<array<double>> effect = {
			/*weapon_effic*/ { /*积极备战*/0.1 * 0.01, /*后方生产*/0.2 * 0.01, /*均衡分配*/0.15 * 0.01, /*城下作战*/0.0 * 0.01 },
			/*horse_effic */ { /*积极备战*/0.1 * 0.01, /*后方生产*/0.2 * 0.01, /*均衡分配*/0.15 * 0.01, /*城下作战*/0.0 * 0.01 },
			/*punch_effic */ { /*积极备战*/0.1 * 0.01, /*后方生产*/0.2 * 0.01, /*均衡分配*/0.15 * 0.01, /*城下作战*/0.0 * 0.01 },
			/*troops_effic*/ { /*积极备战*/0.3 * 0.01, /*后方生产*/0.1 * 0.01, /*均衡分配*/0.15 * 0.01, /*城下作战*/0.0 * 0.01 },
			/*repair_effic*/ { /*积极备战*/0.1 * 0.01, /*后方生产*/0.1 * 0.01, /*均衡分配*/0.15 * 0.01, /*城下作战*/0.3 * 0.01 },
			/*porder_effic*/ { /*积极备战*/0.1 * 0.01, /*后方生产*/0.1 * 0.01, /*均衡分配*/0.15 * 0.01, /*城下作战*/0.3 * 0.01 },
			/*train_effic*/  { /*积极备战*/0.2 * 0.01, /*后方生产*/0.1 * 0.01, /*均衡分配*/0.15 * 0.01, /*城下作战*/0.3 * 0.01 },
			/*weapon_effic*/ { /*积极备战*/0.1 * 0.01, /*后方生产*/0.1 * 0.01, /*均衡分配*/0.10 * 0.01, /*城下作战*/0.0 * 0.01 },
		};

		building_p.weapon_effic = int(effect[0][choise] * building_p.policy_expense * building_p.building_revenue);
		building_p.horse_effic  = int(effect[1][choise] * building_p.policy_expense * building_p.building_revenue);
		building_p.punch_effic  = int(effect[2][choise] * building_p.policy_expense * building_p.building_revenue);
		building_p.troops_effic = int(effect[3][choise] * building_p.policy_expense * building_p.building_revenue);
		building_p.repair_effic = int(effect[4][choise] * building_p.policy_expense * building_p.building_revenue);
		building_p.porder_effic = int(effect[5][choise] * building_p.policy_expense * building_p.building_revenue);
		building_p.train_effic  = int(effect[6][choise] * building_p.policy_expense * building_p.building_revenue);
		building_p.weapon_effic = int(effect[7][choise] * building_p.policy_expense * building_p.building_revenue);

		pk::message_box(pk::encode(pk::format("报告主公,\x1b[2x{}\x1b[0x城市方针开支设定为 \x1b[16x{}\x1b[0x.", pk::decode(pk::get_name(building)), policy)));
		calculate_building2(building, false);
		信息显示_顶部面板();
		pk::add_ap(district, -int(EFFIC_ACTION_COST));
		pk::add_tp(force, -EFFIC_TP_COST, building.get_pos());
		district.update();
	}

	string numberpad_t(int line, int original_value, int current_value)
	{
		return pk::encode("");
	}
}
// namespace
