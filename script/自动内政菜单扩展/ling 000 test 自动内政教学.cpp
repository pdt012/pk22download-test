﻿// ## 2023/05/12 # 江东新风 # mod地图如果已经把五废港用起来，那需要在此设置调整ai ##


namespace 自动内政教学
{

	class Main
	{
		pk::force@ force_;
		pk::person@ gunshi_;
		string title_name_;
		Main()
		{
			pk::bind(206, pk::trigger206_t(callback)); // 爵位晋升剧情后触发
			pk::bind(108, pk::trigger108_t(onNewMonth));
		}

		void onNewMonth()
		{
			//无玩家时，默认开启自动内政
			if (pk::get_player_count() == 0 and !ch::get_auto_affairs_status())
			{
				ch::set_auto_affairs(true);
			}
		}

		void callback(int pre_title, pk::force@ force)//此时的pre_title是爵位还没晋升前的状态
		{
			if (!pk::is_player_controlled(force)) return;
			pk::person@kunshu = pk::get_person(force.kunshu);
			pk::trace("title:" + pre_title + "force:" + force.get_id());
			
			if (pre_title >= 爵位_州刺史 and !ch::get_auto_affairs_status())//应该用即将受封的爵位来
			{
				@ force_ = @force;
				@gunshi_ = pk::is_valid_person_id(force.gunshi) ? pk::get_person(force.gunshi) : pk::get_person(武将_文官);
				title_name_ = pk::decode(pk::get_name(pk::get_title(pre_title-1)));
				pk::scene(pk::scene_t(scene_自动内政教学));
				return;
			}
		}

		void scene_自动内政教学()
		{
			//pk::person@ gunshi = pk::get_person(武将_周瑜);
			pk::message_box(pk::encode(pk::format("恭喜主公进位\x1b[01x{}\x1b[0x！现今，主公已拥有众多城市，进位\x1b[01x{}\x1b[0x后，事务繁多，想必无法事必躬亲。", title_name_, title_name_)), gunshi_);
			pk::message_box(pk::encode("臣建议将城池内的各项内政事务(\x1b[02x征兵生产巡查训练\x1b[00x)指定专人负责，主公您只需指明方向便可。"), gunshi_);
			pk::message_box(pk::encode("如此一来，主公便可将工作的重心更多的放在一统天下之上，想必更有利于我方势力的发展~"), gunshi_);
			string confirm_desc = "请问，是否要开启\x1b[01x自动内政\x1b[0x功能呢？";
			int confirm_value = pk::choose(pk::encode(confirm_desc), { pk::encode("开启"), pk::encode("不开启") , pk::encode("查看说明") });
			if (confirm_value == 2)
			{
				pk::message_box(pk::encode("好的，那下面由臣来简单介绍下全新的自动内政系统吧。"), gunshi_);
				pk::message_box(pk::encode("\x1b[01x自动内政系统\x1b[0x将代替原来的\x1b[02x太守自治\x1b[0x功能，两者的区别在于，自动内政每项事务\x1b[16x只需一名专属的执政官\x1b[0x。"), gunshi_);
				pk::message_box(pk::encode("另外无论该执政官是否行动，或是出门在外(交由副手)，\x1b[01x均可产生对应内政收益\x1b[0x。"), gunshi_);
				pk::background("mediaEX/background/autoafair/IA1.png");
				pk::message_box(pk::encode("如上图，自动内政的对应\x1b[02x信息面板\x1b[0x会\x1b[01x替换\x1b[0x原太守自治的信息面板，显示在城市信息的右侧，在此可以了解到内政操作的分配。"), gunshi_);
				pk::message_box(pk::encode("而原太守自治的\x1b[02x城市菜单\x1b[0x也会被劝降的自动内政菜单\x1b[01x替换\x1b[0x，用以完成和自动内政相关的分配工作。"), gunshi_);
				pk::background("mediaEX/background/autoafair/IA2.png");
				pk::message_box(pk::encode("城市正上方的据点信息会\x1b[02x新增四条\x1b[0x和自动内政相关的\x1b[01x信息条目\x1b[0x，如上图。"), gunshi_);
				pk::message_box(pk::encode("同时，需要提醒主公，既然主公决定将内政事务放手给麾下的能臣，那以后也就无需过多插手内政事务的具体执行。"), gunshi_);
				pk::background("mediaEX/background/autoafair/IA3.png");
				pk::message_box(pk::encode("所以，如上图所示。征兵生产巡查训练的\x1b[01x指令\x1b[0x在开启自动内政后将\x1b[29x不可使用\x1b[0x。"), gunshi_);
				confirm_value = pk::choose(pk::encode(confirm_desc), { pk::encode("开启"), pk::encode("不开启") });
			}
			if (confirm_value == 0)
			{
				ch::set_auto_affairs(true);//开启后数据需进存档，以后获取时从存档获取数据
				pk::message_box(pk::encode("已开启自动内政。"), gunshi_);
			}
			else pk::message_box(pk::encode("可能现在还不是时候，那便请主公再多操心点内政事务吧。以后时机合适，臣再提议此事。"), gunshi_);

		}
	}

	Main main;
}