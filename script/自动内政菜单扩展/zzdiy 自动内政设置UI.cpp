// ## 2023/07/27 # 黑店小小二 # 完善UI ##
// ## 2023/06/16 # 黑店小小二 # 自动内政任命窗口 ##

/*
自动内政任命窗口
巡查： 长官 + 开支
训练： 长官 + 开支
招兵： 长官 + 开支
兵装： 长官 + 开支 + 兵装选择
育马： 长官 + 开支
攻具： 长官 + 开支 + 攻具选择
造船： 长官 + 开支

*/

namespace AUTO_AFFAIRS_SETTING_UI
{
  const int 内政自动化_末 = 7;
  class Main
  {
    pk::dialog@ affairs_dialog;
    array<array<pk::button@>> button_list(7, array<pk::button@>(3));
    array<pk::face@> face_list(7, null);
    pk::button@ repair_effic_btn;

    array<array<int>> data = {
      {/*type*/内政自动化_巡查, /*person_id*/ -1, /*effic*/ 0},
      {/*type*/内政自动化_训练, /*person_id*/ -1, /*effic*/ 0},
      {/*type*/内政自动化_招兵, /*person_id*/ -1, /*effic*/ 0},
      {/*type*/内政自动化_兵装, /*person_id*/ -1, /*effic*/ 0, /*choose*/ 0},
      {/*type*/内政自动化_育马, /*person_id*/ -1, /*effic*/ 0},
      {/*type*/内政自动化_攻具, /*person_id*/ -1, /*effic*/ 0, /*choose*/ 0},
      {/*type*/内政自动化_造船, /*person_id*/ -1, /*effic*/ 0}
    };

    int repair_effic = 0;

    array<string> label_text = {'武将', '开支', '选择'};

    pk::building @building_;
    pk::force @force_;
    pk::person @kunshu_;
    pk::city @city_;

    int menu_city_id_;
    int menu_force_id_;
    int base_id;

    Main()
    {
      add_menu();
      pk::bind(102, pk::trigger102_t(onGameInit));
    }

    void onGameInit()
    {
      create_auto_affairs_dialog(0);
    }

    void add_menu()
    {
      pk::menu_item 自动内政设置;
      自动内政设置.menu = auto_IA_menu::菜单_自动内政;
      自动内政设置.shortcut = "A";
      自动内政设置.init = pk::building_menu_item_init_t(init);
      自动内政设置.get_image_id = pk::menu_item_get_image_id_t(getImageID_1249);
      自动内政设置.is_visible = pk::menu_item_is_visible_t(isVisible);
      自动内政设置.get_text = cast<pk::menu_item_get_text_t @>(function() { return pk::encode("自动内政设置"); });
      自动内政设置.get_desc = cast<pk::menu_item_get_desc_t @>(function() { return pk::encode("设置自动内政官及配置"); });
      自动内政设置.handler = pk::menu_item_handler_t(handler_自动内政设置);
      pk::add_menu_item(自动内政设置);
    }

    void init(pk::building @building)
    {
      @building_ = @building;
      @force_ = pk::get_force(building.get_force_id());
      pk::building @city = @building;
      menu_city_id_ = city.get_id();
      menu_force_id_ = building.get_force_id();
      base_id = building_.get_id();
    }

    int getImageID_1249()
    {
      return 1249;
    }

    bool isVisible()
    {
      if (!ch::get_auto_affairs_status())return false;
      if (building_.get_id() >= 据点_末)
        return false;
      return menu_city_id_ != -1 and menu_force_id_ == pk::get_current_turn_force_id();
    }

    bool handler_自动内政设置()
    {
      
      create_auto_affairs_dialog(menu_city_id_);
      affairs_dialog.set_visible(true);
      update_show_data();
      // create_person_dia(affairs_dialog, menu_city_id_);
      return true;
    }

    void create_auto_affairs_dialog(int city_id)
    {
      pk::size dialog_size = pk::size(900, 776);
      pk::frame@ frame = pk::get_frame();
      pk::dialog@ new_dialog = frame.create_dialog(true);
      @affairs_dialog = @new_dialog;
      new_dialog.set_pos(pk::get_resolution().width / 2 - dialog_size.width / 2, 5);
      new_dialog.set_size(dialog_size);
      new_dialog.set_visible(false);
      //5392 内容区
      pk::sprite9@ bg0 = new_dialog.create_sprite9(70);
      bg0.set_pos(12, 54);
      bg0.set_size(840, 674);
      //5393
      pk::image@ im0 = new_dialog.create_image(527);
      im0.set_pos(720, 389);
      im0.set_size(120, 184);
      //5394
      @ im0 = new_dialog.create_image(319);
      im0.set_pos(608, 477);
      im0.set_size(112, 96);

      //5395
      @ im0 = new_dialog.create_image(320);
      im0.set_pos(0, 0);
      im0.set_size(284, 64);
      //5396
      @ bg0 = new_dialog.create_sprite9(70);
      bg0.set_pos(21, 70);
      bg0.set_size(822, 642);

      //5399
      @ im0 = new_dialog.create_image(323);
      im0.set_pos(5, 722);
      im0.set_size(252, 60);
      //5400
      @ im0 = new_dialog.create_image(322);
      im0.set_pos(799, 722);
      im0.set_size(64, 48);
      //5401
      @ im0 = new_dialog.create_image(321);
      im0.set_pos(823, 36);
      im0.set_size(40, 28);
      //5402
      pk::sprite0@ sp0 = new_dialog.create_sprite0(430);
      sp0.set_pos(284, 36);
      sp0.set_size(539, 32);
      //5403
      @ sp0 = new_dialog.create_sprite0(431);
      sp0.set_pos(851, 64);
      sp0.set_size(16, 658);
      //5404
      @ sp0 = new_dialog.create_sprite0(432);
      sp0.set_pos(5, 64);
      sp0.set_size(16, 658);
      //5405
      @ sp0 = new_dialog.create_sprite0(433);
      sp0.set_pos(257, 722);
      sp0.set_size(542, 64);
    
      array<int> box_size = draw_dialog_tool::draw_box(new_dialog, array<int> = { 827, 547 }, array<int> = { 18, 68 });

      array<int> box_size_small = draw_dialog_tool::draw_box(new_dialog, array<int> = { 827, 50 }, array<int> = { 18, 68 + box_size[1] + 8 });

      pk::button@ bt0 = new_dialog.create_button(238);
      @ bt0 = new_dialog.create_button(180);
      bt0.set_pos(27, 732);
      bt0.set_size(96, 44);
      bt0.set_text(pk::encode("关闭"));
      pk::detail::funcref func0 = cast<pk::button_on_pressed_t@>(function(button) {
        main.affairs_dialog.set_visible(false);
        // 还原数据
        main.data = {
          {/*type*/内政自动化_巡查, /*person_id*/ -1, /*effic*/ 0},
          {/*type*/内政自动化_训练, /*person_id*/ -1, /*effic*/ 0},
          {/*type*/内政自动化_招兵, /*person_id*/ -1, /*effic*/ 0},
          {/*type*/内政自动化_兵装, /*person_id*/ -1, /*effic*/ 0, /*choose*/ 0},
          {/*type*/内政自动化_育马, /*person_id*/ -1, /*effic*/ 0},
          {/*type*/内政自动化_攻具, /*person_id*/ -1, /*effic*/ 0, /*choose*/ 0},
          {/*type*/内政自动化_造船, /*person_id*/ -1, /*effic*/ 0}
        };
        
        for (int i = 0; i < int(main.face_list.length); i += 1)
        {
          main.face_list[i].set_face(-1);
        }
        // array<array<pk::button@>> button_list(7, array<pk::button@>(3));
        // array<pk::face@> face_list(7, null);
        // main.button_list = button_list;
        // main.face_list = face_list;
        main.update_show_data(true);
      });
      bt0.on_button_pressed(func0);

      // @ bt0 = new_dialog.create_button(172);
      // bt0.set_pos(115, 632);
      // bt0.set_size(88, 44);
      // bt0.set_text(pk::encode(""));


      pk::text@ tx0 = new_dialog.create_text();
      tx0.set_pos(40, 22);
      tx0.set_size(96, 24);
      tx0.set_text(pk::encode("自动内政官设置"));
      tx0.set_text_font(FONT_BIG);

      // 内容区
      // 7个，分两排，
      // 每个盒子 90 * 160
      // 盒子分为标题 跟 武将信息区
      // 标题 90 *  22，即铺满
      // 武将信息区跟道具情报页（137-> 6477）一致
      // 武将信息区 86 * 128


      // top 线条
      // 官职名称栏目高度40，宽度均分611 - 3*8 / 4 = 148
      // 607 / 2 = 303.5 + 68

      // 内容区实际高度 607 - 68 - 8 = 531
      // 内容去实际宽度 837 - 18 - 8 = 811
      // 单个宽度 = 811 - 8 * 3 = 787 / 3 = 262
      // 3 横线
      for (int i = 0; i < 3; i += 1)
      {
        array<int> y_list = { 68 + 8 + 40, 68 + 8 + 261, 68 + 8 + 261 + 8 + 40 }; // 手动计算
        @ sp0 = new_dialog.create_sprite0(428);
        sp0.set_size(box_size[0], 8);
        sp0.set_pos(21, y_list[i]);
      }

      // // 3 竖线
      for (int i = 0; i < 3; i += 1)
      {
        int x = 18 + ((199 + 8) * (i + 1));
        @ sp0 = new_dialog.create_sprite0(435);
        sp0.set_size(8, box_size[1]);
        sp0.set_pos(x, 71);
      }

      BuildingInfo @base_p = @building_ex[0];
      // // 官职名称
      for (int i = 0; i < 7; i += 1)
      {
        int x = 18 + 199 * ((i <= 3 ? i : i - 4)) + 8 * ((i <= 3 ? i : i - 4) + 1);
        int y = i > 3 ? 345 : 76;
        @tx0 = new_dialog.create_text();
        tx0.set_pos(x, y);
        tx0.set_size(199, 40);
        tx0.set_text(pk::encode(base_p.get_charge_name(i)));
        tx0.set_text_font(FONT_BIG);
        tx0.set_text_align(0x4|0x20);
      }

      create_person_dia(new_dialog);
    }

    void create_person_dia(pk::dialog@ new_dialog)
    {
      pk::sprite9@ bg0;
      pk::button@ bt0;
      pk::text@ text0;

      for (int i = 0; i < int(data.length); i += 1)
      {
        /*
          绘制武将信息
        */
        // 头像区
        int face_x = 18 + 199 * ((i <= 3 ? i : i - 4)) + 8 * ((i <= 3 ? i : i - 4) + 1) + 50;
        int face_y = i <= 3 ? 134 : 403;
        @face_list[i] = draw_dialog_tool::draw_face(new_dialog, { 100, 100 }, { face_x, face_y }, -1);

        // 按钮区
        int btn_x = 18 + 199 * ((i <= 3 ? i : i - 4)) + 8 * ((i <= 3 ? i : i - 4) + 1);
        for (int btn_i = 1; btn_i < int(data[i].length); btn_i += 1)
        {
          // 269 + 120
          int btn_y = (i <= 3 ? 134 : 403) + 105 + ((btn_i - 1) * 31);// 239 + ((btn_i - 1) * 31) + (i > 3 ? 389 : 0);
          array<int> btn_data = data[i];
          string label = label_text[btn_i - 1];
          if (btn_data[0] == 内政自动化_兵装 && btn_i == 3) label = '兵装' + label;
          if (btn_data[0] == 内政自动化_攻具 && btn_i == 3) label = '攻具' + label;
          // 6481
          @ bg0 = new_dialog.create_sprite9(393);
          bg0.set_pos(btn_x + 10, btn_y);  // y: 100 + 134 + 10; x: 26 + 10
          bg0.set_size(70, 30);

          @ text0 = new_dialog.create_text();
          text0.set_pos(btn_x + 10, btn_y);  // y: 100 + 134 + 10; x: 26 + 10
          text0.set_size(70, 30);
          text0.set_text(pk::encode(label));
          text0.set_text_align(0x4|0x20);

          string btn_text = '';
          if (btn_i == 1) btn_text = '';
          else if (btn_i == 2) btn_text = formatInt(data[i][btn_i]);
          else if (btn_i == 3)
          {
            BuildingInfo @base_p = @building_ex[menu_city_id_];
            string choose_name = base_p.get_choose_name(0, btn_data[0]);
            btn_text = pk::encode(btn_data[0] == -1 ? '无' : choose_name);
          }

          @ bt0 = new_dialog.create_button(1526);
          bt0.set_pos(btn_x + 10 + 80, btn_y);  // y: 100 + 134 + 10; x: 26 + 10
          bt0.set_size(84, 30);
          bt0.set_text(btn_text);
          bt0.set_text_align(0x4|0x20);

          @button_list[i][btn_i - 1] = @bt0;

          pk::detail::funcref func0;
          pk::detail::funcref func1;
          pk::detail::funcref func2;
          pk::detail::funcref func3;
          pk::detail::funcref func4;
          pk::detail::funcref func5;
          pk::detail::funcref func6;

          if (btn_i == 1) // 武将
          {
            func0 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle_charge(main.menu_force_id_, main.menu_city_id_, 0);
              main.update_show_data();
            });
            func1 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle_charge(main.menu_force_id_, main.menu_city_id_, 1);
              main.update_show_data();
            });
            func2 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle_charge(main.menu_force_id_, main.menu_city_id_, 2);
              main.update_show_data();
            });
            func3 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle_charge(main.menu_force_id_, main.menu_city_id_, 3);
              main.update_show_data();
            });
            func4 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle_charge(main.menu_force_id_, main.menu_city_id_, 4);
              main.update_show_data();
            });
            func5 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle_charge(main.menu_force_id_, main.menu_city_id_, 5);
              main.update_show_data();
            });
            func6 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle_charge(main.menu_force_id_, main.menu_city_id_, 6);
              main.update_show_data();
            });
          }

          if (btn_i == 2) // 支出
          {
            func0 = cast<pk::button_on_pressed_t@>(function(button) {
              main.change_effic(main.menu_force_id_, main.menu_city_id_, 0);
              main.update_show_data();
            });
            func1 = cast<pk::button_on_pressed_t@>(function(button) {
              main.change_effic(main.menu_force_id_, main.menu_city_id_, 1);
              main.update_show_data();
            });
            func2 = cast<pk::button_on_pressed_t@>(function(button) {
              main.change_effic(main.menu_force_id_, main.menu_city_id_, 2);
              main.update_show_data();
            });
            func3 = cast<pk::button_on_pressed_t@>(function(button) {
              main.change_effic(main.menu_force_id_, main.menu_city_id_, 3);
              main.update_show_data();
            });
            func4 = cast<pk::button_on_pressed_t@>(function(button) {
              main.change_effic(main.menu_force_id_, main.menu_city_id_, 4);
              main.update_show_data();
            });
            func5 = cast<pk::button_on_pressed_t@>(function(button) {
              main.change_effic(main.menu_force_id_, main.menu_city_id_, 5);
              main.update_show_data();
            });
            func6 = cast<pk::button_on_pressed_t@>(function(button) {
              main.change_effic(main.menu_force_id_, main.menu_city_id_, 6);
              main.update_show_data();
            });
          }

          if (btn_i == 3) // choose
          {
            func0 = cast<pk::button_on_pressed_t@>(function(button) {
              main.set_choose(main.menu_force_id_, main.menu_city_id_, 0);
              main.update_show_data();
            });
            func1 = cast<pk::button_on_pressed_t@>(function(button) {
              main.set_choose(main.menu_force_id_, main.menu_city_id_, 1);
              main.update_show_data();
            });
            func2 = cast<pk::button_on_pressed_t@>(function(button) {
              main.set_choose(main.menu_force_id_, main.menu_city_id_, 2);
              main.update_show_data();
            });
            func3 = cast<pk::button_on_pressed_t@>(function(button) {
              main.set_choose(main.menu_force_id_, main.menu_city_id_, 3);
              main.update_show_data();
            });
            func4 = cast<pk::button_on_pressed_t@>(function(button) {
              main.set_choose(main.menu_force_id_, main.menu_city_id_, 4);
              main.update_show_data();
            });
            func5 = cast<pk::button_on_pressed_t@>(function(button) {
              main.set_choose(main.menu_force_id_, main.menu_city_id_, 5);
              main.update_show_data();
            });
            func6 = cast<pk::button_on_pressed_t@>(function(button) {
              main.set_choose(main.menu_force_id_, main.menu_city_id_, 6);
              main.update_show_data();
            });
          }

          switch (i)
          {
            case 0:
              bt0.on_button_pressed(func0);
              break;
            case 1:
              bt0.on_button_pressed(func1);
              break;
            case 2:
              bt0.on_button_pressed(func2);
              break;
            case 3:
              bt0.on_button_pressed(func3);
              break;
            case 4:
              bt0.on_button_pressed(func4);
              break;
            case 5:
              bt0.on_button_pressed(func5);
              break;
            case 6:
              bt0.on_button_pressed(func6);
              break;
          }
        }
      }

      // array<int> box_size_small = draw_dialog_tool::draw_box(new_dialog, array<int> = { 191, 50 }, array<int> = { 26, 68 + 539 + 21 }, array<int> = { 427, 427, 434, 434 }, array<int> = { 424, 421, 425, 422 }, 8, 8);

      @ bg0 = new_dialog.create_sprite9(393);
      bg0.set_pos(36, 625);  // 45
      bg0.set_size(70, 30);
      @ text0 = new_dialog.create_text();
      text0.set_pos(36, 625);  // 45
      text0.set_size(70, 30);
      text0.set_text(pk::encode('筑城开支'));
      text0.set_text_align(0x4|0x20);

      @ bt0 = new_dialog.create_button(1526);
      bt0.set_pos(36 + 80, 625);  // 
      bt0.set_size(84, 30);
      bt0.set_text(formatInt(repair_effic));
      bt0.set_text_align(0x4|0x20);

      @repair_effic_btn = @bt0;
      pk::detail::funcref func0 = cast<pk::button_on_pressed_t@>(function(button) {
        main.change_effic(main.menu_force_id_, main.menu_city_id_, 7);
        main.update_show_data();
      });
      bt0.on_button_pressed(func0);

      // 819
      @ bt0 = new_dialog.create_button(238);
      bt0.set_pos(26 + 531, 68 + 539 + 21 + 40);
      bt0.set_size(126, 40);
      bt0.set_text(pk::encode("自动任命官员"));
      func0 = cast<pk::button_on_pressed_t@>(function(button) {
        // draw_dialog_tool::set_map_desc('自动任命官员提示');
        main.set_city_auto_rank();
        main.update_show_data();
      });
      bt0.on_button_pressed(func0);

      @ bt0 = new_dialog.create_button(238);
      bt0.set_pos(26 + 531 + 136, 68 + 539 + 21 + 40);
      bt0.set_size(126, 40);
      bt0.set_text(pk::encode("自动设置开支"));

      pk::detail::funcref func1 = cast<pk::button_on_pressed_t@>(function(button) {
        // draw_dialog_tool::set_map_desc('自动设定分支提示');
        main.set_city_auto_effic();
        main.update_show_data();
      });
      bt0.on_button_pressed(func1);
    }

    string numberpad_t(int line, int original_value, int current_value)
    {
      return pk::encode("");
    }

    void update_show_data(bool reset = false)
    {
      BuildingInfo @base_p = @building_ex[menu_city_id_];
      if (reset)
      {
        data = {
          {/*type*/内政自动化_巡查, /*person_id*/ -1, /*effic*/ 0},
          {/*type*/内政自动化_训练, /*person_id*/ -1, /*effic*/ 0},
          {/*type*/内政自动化_招兵, /*person_id*/ -1, /*effic*/ 0},
          {/*type*/内政自动化_兵装, /*person_id*/ -1, /*effic*/ 0, /*choose*/ 0},
          {/*type*/内政自动化_育马, /*person_id*/ -1, /*effic*/ 0},
          {/*type*/内政自动化_攻具, /*person_id*/ -1, /*effic*/ 0, /*choose*/ 0},
          {/*type*/内政自动化_造船, /*person_id*/ -1, /*effic*/ 0}
        };
        repair_effic = 0;
      }
      else
      {
        // 先把数据塞进data, 操作过程中不会影响数据
        // 内政自动化_巡查
        data[0] = { 0, base_p.inspections_person, base_p.porder_effic };
        // 内政自动化_训练
        data[1] = { 1, base_p.drill_person, base_p.train_effic };
        // 内政自动化_招兵
        data[2] = { 2, base_p.recruit_person, base_p.troops_effic };
        // 内政自动化_兵装
        data[3] = { 3, base_p.weapon_person, base_p.weapon_effic, base_p.weapon_choose };
        // 内政自动化_育马
        data[4] = { 4, base_p.horse_person, base_p.horse_effic };
        // 内政自动化_攻具
        data[5] = { 5, base_p.punch_person, base_p.punch_effic, base_p.arms_choose };
        // 内政自动化_造船
        data[6] = { 6, base_p.boat_person, base_p.boat_effic };

        repair_effic = base_p.repair_effic;
      }

      // button_list
      for (int i = 0; i < int(button_list.length); i += 1)
      {
        int person_id = data[i][1];
        pk::person@ person = pk::is_valid_person_id(person_id) ? pk::get_person(person_id) : null;
        // pk::info('i:' + i + ';id:' + face_list[i].get_id());
        face_list[i].set_face(person !is null ? person.face : -1);
        for (int b = 0; b < int(button_list[i].length); b += 1)
        {
          if (b == 0) button_list[i][b].set_text(person !is null ? pk::get_name(person) : ''); // 武将名
          else
          {
            if (button_list[i][b] !is null)
            {
              if (b == 1) button_list[i][b].set_text(formatInt(data[i][b + 1]));
              else {
                string choose_name = base_p.get_choose_name(menu_city_id_, data[i][0]);
                string btn_text = pk::encode(data[i][0] == -1 ? '无' : choose_name);
                button_list[i][b].set_text(btn_text);
              }
            }
          }
        }
      }

      repair_effic_btn.set_text(formatInt(repair_effic));
    }

    void toggle_charge(int force_id, int city_id, int 内政自动化_项目)
    {
      BuildingInfo @building_p = @building_ex[city_id];
      string 执行武将名称 = "无";
      string 前执行武将名称 = "无";
      string 项目名称 = building_p.get_charge_name(内政自动化_项目);
      int person_id = building_p.get_charge(内政自动化_项目);
      pk::building @base = pk::get_building(city_id);
      if (pk::is_valid_person_id(person_id))
        执行武将名称 = pk::decode(pk::get_name(pk::get_person(person_id)));
      前执行武将名称 = 执行武将名称;

      // 10 巡查，33训练 16招兵，5兵装
      array<int> select_person_type = { /*内政自动化_巡查*/10, /*内政自动化_训练*/33, /*内政自动化_招兵*/16, /*内政自动化_兵装*/5, /*内政自动化_育马*/5, /*内政自动化_攻具*/5, /*内政自动化_造船*/5 };
      int list_type = select_person_type[内政自动化_项目];

      pk::list<pk::person @> person_list = 内政自动设置::get_person_list(base, 内政自动化_项目);

      pk::list<pk::person @> person_sel = pk::person_selector(pk::encode("执行官选择"), pk::encode(pk::format("选择{}.", 项目名称)), person_list, 1, 1, person_list, list_type);

      if (person_sel.count == 0)
      {
        if (person_id < 0) return;
        if (person_id > -1)
        {
          bool confirm1 = pk::yes_no(pk::encode(pk::format("当前{}为\x1b[1x{}\x1b[0x，是否要撤销执行官?", 项目名称, 执行武将名称)));
          if (confirm1)
          {
            building_p.set_charge(-1, 内政自动化_项目);
            pk::message_box(pk::encode(pk::format("{}任务取消", 项目名称)));
          }
        }
      }

      person_id = person_sel[0].get_id();
      building_p.set_charge(person_id, 内政自动化_项目);

      if (force_id == pk::get_current_turn_force_id())
      {
        string city_name = pk::decode(pk::get_name(pk::get_building(city_id)));
        string person_name = pk::decode(pk::get_name(pk::get_person(person_id)));
        string toggle_msg;
        // 如果该武将有其他任职，将自动取消
        for (int i = 0; i <= 内政自动化_造船; i += 1)
        {
          if (内政自动化_项目 != i and building_p.get_charge(i) == person_id)
          {
            building_p.set_charge(-1, i);
            toggle_msg += "原\x1b[27x" + building_p.get_charge_name(i) + '\x1b[0x' + person_name + "任务取消";
          }
        }

        pk::message_box(pk::encode(toggle_msg));
      }
    }

    void change_effic(int force_id, int city_id, int 内政自动化_项目)
    {
      BuildingInfo @building_p = @building_ex[city_id];
      int person_id = building_p.get_charge(内政自动化_项目);
      string 项目名称 = building_p.get_charge_name(内政自动化_项目);
      if (person_id == -1 and 内政自动化_项目 != 内政自动化_筑城)
      {
        pk::message_box(pk::encode(pk::format("请先设置\x1b[27x{}\x1b[0x再设定开支", 项目名称)));
        return ;
      }

      pk::int_bool deal = pk::numberpad(pk::encode("开支"), 0, 开支级别(building_), 0, pk::numberpad_t(numberpad_t));

      if (deal.second)
      {
        if (int(deal.first) > int(building_p.building_revenue))
        {
          bool confirm = pk::yes_no(pk::encode(pk::format("设定的内政开支值{}已经超过旬收入\x1b[1x{}\x1b[0x，是否要设定?", deal.first, building_p.building_revenue)));
          if (confirm) building_p.set_effic(deal.first, 内政自动化_项目);
          return;
        }
        else building_p.set_effic(deal.first, 内政自动化_项目);
      }

      内政自动::calculate_building2(pk::get_building(base_id), false);
      内政自动::信息显示_顶部面板();
    }

    int 开支级别(pk::building @building)
    {
      int base_id = building_.get_id();
      int level = ch::get_city_level(base_id);
      if (level == 0)
        return 500;
      else if (level == 1)
        return 800;
      else if (level == 2)
        return 1000;
      else if (level == 3)
        return 1200;
      else if (level == 4)
        return 1500;
      else
        return 500;
    }

    void set_choose(int force_id, int city_id, int 内政自动化_项目)
    {
      string title;
      array<string> select_data;
      if (内政自动化_项目 == 内政自动化_兵装)
      {
        title = pk::encode("请选择要生产的兵装");
        select_data = {
          pk::encode("平衡"),
          pk::encode(" 枪 "),
          pk::encode(" 戟 "),
          pk::encode(" 弩 ")
        };
      }
      if (内政自动化_项目 == 内政自动化_攻具)
      {
        string 冲车系 = "冲车";
        string 井栏系 = "井栏";

        title = pk::encode("请选择要生产的攻具");
        if (pk::has_tech(pk::get_force(force_id), 技巧_投石开发))
          井栏系 = "投石";
        if (pk::has_tech(pk::get_force(force_id), 技巧_木兽开发))
          冲车系 = "木兽";
        select_data = {
          pk::encode("平衡"),
          pk::encode(井栏系),
          pk::encode(冲车系)
        };
      }
      int choose = pk::choose(title, select_data);

      BuildingInfo @building_p = @building_ex[city_id];
      building_p.set_choose(choose, 内政自动化_项目);

      内政自动::calculate_building2(pk::get_building(city_id), false);
      内政自动::信息显示_顶部面板();
    }

    void set_city_auto_rank()
    {
      内政自动::自动任命官员(force_, building_);
    }

    void set_city_auto_effic()
    {
      内政自动::自动设定分支(force_, building_);
    }
  }

  Main main;
}
