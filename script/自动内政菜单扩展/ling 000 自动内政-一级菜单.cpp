﻿// ## 2022/06/19 # 铃 # 为新内政系统增加新的一级菜单，暂时注释掉早前的自动内政 ##
// ## 2021/09/26 # 江东新风 # 事件手动触发的一级菜单 ##
// ## 2021/01/18 # 江东新风 # 禁止拆建拼音改简体 ##
// ## 2020/12/15 # messi # 内政自动化改为太守自治 ##
// ## 2020/08/11 # 氕氘氚 # 修复太守不在时点击买卖可能出现的指針錯誤 ##
// ## 2020/08/08 # 氕氘氚 # 新增三个一级菜单 ##

namespace auto_IA_menu
{
    // ======================================================================

    const string shortcut_自动内政 = "z";

    // ======================================================================
    int 菜单_自动内政 = -1;

    int 自动化_展开_当前 = 0;
    const int 自动化_展开_顺序 = 1;//用于810的内政自动化，810在执政官模式开启时会关闭，所以不冲突
    const int 自动化_展开_许可 = 2;
    const int 自动化_展开_标准 = 3;
    const int 自动化_展开_生产 = 4;
    const int 自动化_展开_负责 = 5;

    const int 自动化_展开_招兵 = 1;
    const int 自动化_展开_巡查 = 2;
    const int 自动化_展开_训练 = 3;
    const int 自动化_展开_兵装 = 4;
    const int 自动化_展开_育马 = 5;
    const int 自动化_展开_攻具 = 6;
    const int 自动化_展开_造船 = 7;
    const int 自动化_展开_筑城 = 8;
    const int 自动化_展开_运输 = 9;
    // ======================================================================

    class Main
    {

        pk::building @building_;
        pk::force @force_;
        pk::person @taishu_;
        pk::person @kunshu_;
        pk::city @city_;
        pk::gate @gate_;
        pk::port @port_;
        pk::district @district_;

        int menu_city_id_;
        int menu_force_id_;

        Main()
        {
            //此菜单用于风铃的内政自动化---稍加改动，让菜单同时支持太守自治和内政自动化
            pk::menu_item menu_item_自动内政;
            menu_item_自动内政.menu = 0;
            menu_item_自动内政.pos = 1;
            menu_item_自动内政.shortcut = shortcut_自动内政;
            menu_item_自动内政.init = pk::building_menu_item_init_t(init_菜单_自动内政);
            menu_item_自动内政.is_visible = pk::menu_item_is_visible_t(isVisible_菜单_自动内政);
            menu_item_自动内政.is_enabled = pk::menu_item_is_enabled_t(isEnabled_菜单_自动内政);
            menu_item_自动内政.get_text = pk::menu_item_get_text_t(getText_菜单_自动内政);
            menu_item_自动内政.get_desc = pk::menu_item_get_desc_t(getDesc_菜单_自动内政);
            菜单_自动内政 = pk::add_menu_item(menu_item_自动内政);

        }

        // =============================================================================================

        void init_菜单_自动内政(pk::building @building)
        {
            @building_ = @building;
            @force_ = pk::get_force(building.get_force_id());
            @taishu_ = pk::get_person(pk::get_taishu_id(building));
            @kunshu_ = pk::get_person(force_.kunshu);

            if ((building_.get_id() <= 41) and (building_.get_id() >= 0))
                @city_ = pk::building_to_city(building);

            menu_city_id_ = (city_ !is null) ? city_.get_id() : -1;
            menu_force_id_ = building.get_force_id();
        }

        bool isVisible_菜单_自动内政()
        {
            if (ch::get_auto_affairs_status())
            {
                if (pk::is_campaign())
                    return false;
                if (!setting_ex.mod_set[自动内政扩展_开关])
                    return false;
                if (building_.get_id() >= 据点_末)
                    return false;
            }
            else
            {
                if (pk::is_campaign())
                    return false;
                if (!setting_ex.mod_set[自动内政扩展_开关])
                    return false;
                if (building_.get_id() >= 城市_末)
                    return false;
                if (!pk::is_player_controlled(pk::get_city(menu_city_id_)))
                    return false;
                return menu_city_id_ != -1 and menu_force_id_ == pk::get_current_turn_force_id();
            }

            return true;
        }

        string getText_菜单_自动内政()
        {
            if (ch::get_auto_affairs_status())
                return pk::encode("自动内政");
            else return pk::encode("太守自治");
        }

        bool isEnabled_菜单_自动内政()
        {
            if (building_.get_id() >= 据点_末)
                return false;

            return true;
        }

        string getDesc_菜单_自动内政()
        {
            if (ch::get_auto_affairs_status())
                return pk::encode("执行自动内政相关指令.");
            else return pk::encode("太守自治方针选项");
        }

        // =============================================================================================


    }

    Main main;
}
