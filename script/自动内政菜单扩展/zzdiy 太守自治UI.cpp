
// ## 2023/06/22 # 黑店小小二 # 太守自治UI ##

/*
征兵 开关 治安 资金 兵粮比
训练 开关 士气
巡查 开关 治安
褒奖 开关 忠诚
运输 开关 

模式选择
一：巡察-训练-征兵-褒奖-运输
二：巡察-征兵-训练-褒奖-运输
三：征兵-巡察-训练-褒奖-运输
四：征兵-训练-巡察-褒奖-运输
五：训练-征兵-巡察-褒奖-运输
六：训练-巡察-征兵-褒奖-运输
 */

namespace taishu_autonomy__UI
{
	const int 内政自动化_巡察 = 0;
	const int 内政自动化_训练 = 1;
	const int 内政自动化_征兵 = 2;
	const int 内政自动化_褒赏 = 3;
	const int 内政自动化_运输 = 4;

	class Main {
		pk::dialog@ taishu_autonomy_dialog;
		array<pk::button@> mode_select_btns(6, null);
		array<pk::sprite9@> mode_select_bg(6, null);
		array<pk::text@> mode_select_text(6, null);
		array<array<pk::button@>> button_list(6, array<pk::button@>(4));
		array<string> type_list = { '征兵', '训练', '巡查', '褒奖', '运输' };
		array<array<string>> action_list = {
			{'开关', '治安', '资金', '兵粮比'},
			{'开关', '士气'},
			{'开关', '治安'},
			{'开关', '忠诚'},
			{'开关'}
		};
		array<string> mode_list = {
			'巡察-训练-征兵-褒奖-运输',
			'巡察-征兵-训练-褒奖-运输',
			'征兵-巡察-训练-褒奖-运输',
			'征兵-训练-巡察-褒奖-运输',
			'训练-征兵-巡察-褒奖-运输',
			'训练-巡察-征兵-褒奖-运输'
		};
		array<array<int>> data = {
			/*征兵*/ { /*开关*/0, /*治安*/0, /*资金*/0, /*兵粮比*/0 },
			/*训练*/ { /*开关*/0, /*士气*/0 },
			/*巡查*/ { /*开关*/0, /*治安*/0 },
			/*褒奖*/ { /*开关*/0, /*忠诚*/0 },
			/*运输*/ { /*开关*/0 }
		};
		int mode_select = 0; // 已选择模式

		pk::force @force_;
    pk::person @kunshu_;
    pk::city @city_;
    pk::building @building_;

    int menu_city_id_;
    int menu_force_id_;

    int line_height = 8;
    int item_width = 156;

		Main()
		{
			// ch::set_auto_affairs(false); // 开发期间强制关闭
			add_menu_城市内政自动化();
			pk::bind(102, pk::trigger102_t(onGameInit));
		}

		void onGameInit()
    {
      // create_taishu_autonomy_dialog();
    }

    void create_taishu_autonomy_dialog()
    {

      pk::size dialog_size = pk::size(900, 800);
      // pk::frame@ frame = pk::get_frame();
      pk::dialog@ new_dialog = pk::new_dialog(true);
      @taishu_autonomy_dialog = @new_dialog;
      new_dialog.set_pos(pk::get_resolution().width / 2 - dialog_size.width / 2, 5);
      new_dialog.set_size(dialog_size);
      new_dialog.set_visible(false);
      //5392 内容区
      pk::sprite9@ bg0 = new_dialog.create_sprite9(70);
      bg0.set_pos(12, 54);
      bg0.set_size(840, 574);
      //5393
      pk::image@ im0 = new_dialog.create_image(527);
      im0.set_pos(720, 389);
      im0.set_size(120, 184);
      //5394
      @ im0 = new_dialog.create_image(319);
      im0.set_pos(608, 477);
      im0.set_size(112, 96);

      //5395
      @ im0 = new_dialog.create_image(320);
      im0.set_pos(0, 0);
      im0.set_size(284, 64);
      //5396
      @ bg0 = new_dialog.create_sprite9(70);
      bg0.set_pos(21, 70);
      bg0.set_size(822, 542);

      //5399
      @ im0 = new_dialog.create_image(323);
      im0.set_pos(5, 622);
      im0.set_size(252, 60);
      //5400
      @ im0 = new_dialog.create_image(322);
      im0.set_pos(799, 622);
      im0.set_size(64, 48);
      //5401
      @ im0 = new_dialog.create_image(321);
      im0.set_pos(823, 36);
      im0.set_size(40, 28);
      //5402
      pk::sprite0@ sp0 = new_dialog.create_sprite0(430);
      sp0.set_pos(284, 36);
      sp0.set_size(539, 32);
      //5403
      @ sp0 = new_dialog.create_sprite0(431);
      sp0.set_pos(851, 64);
      sp0.set_size(16, 558);
      //5404
      @ sp0 = new_dialog.create_sprite0(432);
      sp0.set_pos(5, 64);
      sp0.set_size(16, 558);
      //5405
      @ sp0 = new_dialog.create_sprite0(433);
      sp0.set_pos(257, 622);
      sp0.set_size(542, 64);
      //5406
      @ im0 = new_dialog.create_image(316);
      im0.set_pos(18, 68);
      im0.set_size(8, 8);
      //5407
      @ im0 = new_dialog.create_image(315);
      im0.set_pos(18, 607);
      im0.set_size(8, 8);
      //5408
      @ im0 = new_dialog.create_image(313);
      im0.set_pos(837, 607);
      im0.set_size(8, 8);
      //5409
      @ im0 = new_dialog.create_image(314);
      im0.set_pos(837, 68);
      im0.set_size(8, 8);
      //5410
      @ sp0 = new_dialog.create_sprite0(428);
      sp0.set_pos(26, 68);
      sp0.set_size(811, 8);
      //5411
      @ sp0 = new_dialog.create_sprite0(428);
      sp0.set_pos(26, 607);
      sp0.set_size(811, 8);
      //5412
      @ sp0 = new_dialog.create_sprite0(435);
      sp0.set_pos(18, 76);
      sp0.set_size(8, 531);
      //5413
      @ sp0 = new_dialog.create_sprite0(435);
      sp0.set_pos(837, 76);
      sp0.set_size(8, 531);

      pk::button@ bt0 = new_dialog.create_button(238);
      @ bt0 = new_dialog.create_button(180);
      bt0.set_pos(27, 632);
      bt0.set_size(96, 44);
      bt0.set_text(pk::encode("关闭"));
      pk::detail::funcref func0 = cast<pk::button_on_pressed_t@>(function(button) {
        main.taishu_autonomy_dialog.set_visible(false);
        // 还原数据
      });
      bt0.on_button_pressed(func0);

      pk::text@ tx0 = new_dialog.create_text();
      tx0.set_pos(40, 22);
      tx0.set_size(96, 24);
      tx0.set_text(pk::encode("太守自治"));
      tx0.set_text_font(FONT_BIG);

      // top 线条
      // 官职名称栏目高度40，宽度均分611 - 3*8 / 4 = 148
      // 607 / 2 = 303.5 + 68

      // 内容区实际高度 607 - 68 - 8 = 531
      // 内容去实际宽度 837 - 18 - 8 = 811
      // 单个宽度 = 811 - 8 * 4 = 779 / 5 = 156
      // 3 横线
      for (int i = 0; i < 2; i += 1)
      {
        array<int> y_list = { 68 + line_height + 40, 68 + line_height + 261 }; // 手动计算
        @ sp0 = new_dialog.create_sprite0(428);
        sp0.set_size(818, line_height);
        sp0.set_pos(21, y_list[i]);
      }

      // // 3 竖线
      for (int i = 0; i < 4; i += 1)
      {
        int x = 18 + ((item_width + line_height) * (i + 1));
        @ sp0 = new_dialog.create_sprite0(435);
        sp0.set_size(line_height, 261 + line_height);
        sp0.set_pos(x, 71);
      }

      // // 名称
      for (int i = 0; i < int(type_list.length); i += 1)
      {
        int x = 18 + item_width * i + line_height * (i + 1);
        @tx0 = new_dialog.create_text();
        tx0.set_pos(x, 76);
        tx0.set_size(item_width, 40);
        tx0.set_text(pk::encode(type_list[i]));
        tx0.set_text_font(FONT_BIG);
        tx0.set_text_align(0x4|0x20);
      }

      // 模式选择
      @ bt0 = new_dialog.create_button(238);
			bt0.set_pos(18 + 20, 68 + line_height + 261 + 20);
			bt0.set_size(108, 38);
			bt0.set_text(pk::encode("模式选择"));

			for (int i = 0; i < int(mode_list.length); i += 1)
			{
				int x = 18 + 20;
				int y = 38 + (68 + line_height + 261 + 20) + 5 * (i + 1) + 30 * i;

				@ bt0 = new_dialog.create_button(214);
				bt0.set_pos(x, y);
				bt0.set_size(78, 30);
				bt0.set_text(pk::encode("选择"));
				@mode_select_btns[i] = @bt0;

				func0 = cast<pk::button_on_pressed_t@>(function(button) {
        	main.mode_select = 0;
          main.update_select_mode();
        });
        pk::detail::funcref func1 = cast<pk::button_on_pressed_t@>(function(button) {
        	main.mode_select = 1;
          main.update_select_mode();
        });
        pk::detail::funcref func2 = cast<pk::button_on_pressed_t@>(function(button) {
        	main.mode_select = 2;
          main.update_select_mode();
        });
        pk::detail::funcref func3 = cast<pk::button_on_pressed_t@>(function(button) {
        	main.mode_select = 3;
          main.update_select_mode();
        });
        pk::detail::funcref func4 = cast<pk::button_on_pressed_t@>(function(button) {
        	main.mode_select = 4;
          main.update_select_mode();
        });
        pk::detail::funcref func5 = cast<pk::button_on_pressed_t@>(function(button) {
        	main.mode_select = 5;
          main.update_select_mode();
        });
        switch (i)
        {
          case 0:
            bt0.on_button_pressed(func0);
            break;
          case 1:
            bt0.on_button_pressed(func1);
            break;
          case 2:
            bt0.on_button_pressed(func2);
            break;
          case 3:
            bt0.on_button_pressed(func3);
            break;
          case 4:
            bt0.on_button_pressed(func4);
            break;
          case 5:
            bt0.on_button_pressed(func5);
            break;
        }

				@ bg0 = new_dialog.create_sprite9(393); // true: 357 false 393
        bg0.set_pos(x + 78 + 20, y);  // y: 100 + 134 + 10; x: 26 + 10
        bg0.set_size(70, 30);

        @mode_select_bg[i] = @bg0;

				pk::text @text0 = new_dialog.create_text();
        text0.set_pos(x + 78 + 20, y);
        text0.set_size(70, 30);
        text0.set_text(pk::encode('模式' + i));
        text0.set_text_align(0x4|0x20);
        @mode_select_text[i] = @text0;

        @text0 = new_dialog.create_text();
        text0.set_pos(x + 78 + 20 + 70 + 20 + 20, y);
        text0.set_size(250, 30);
        text0.set_text(pk::encode(mode_list[i]));
        text0.set_text_align(0x20);

			}
    }

    void add_menu_城市内政自动化()
		{
			if (ch::get_auto_affairs_status()) return;
			pk::menu_item 内政自动化;
			内政自动化.menu = auto_IA_menu::菜单_自动内政;
			内政自动化.shortcut = 'Z';
			内政自动化.init = pk::building_menu_item_init_t(init);
			内政自动化.get_image_id = pk::menu_item_get_image_id_t(getImageID_1249);
			内政自动化.is_visible = pk::menu_item_is_visible_t(isVisible);
			内政自动化.get_text = cast<pk::menu_item_get_text_t@>(function() { return pk::encode("太守自治设置"); });
			内政自动化.get_desc = cast<pk::menu_item_get_desc_t@>(function() { return pk::encode("设置太守自治参数"); });
			内政自动化.handler = pk::menu_item_handler_t(handler_太守自治设置);
			pk::add_menu_item(内政自动化);
		}

		void init(pk::building @building)
    {
      @building_ = @building;
      menu_city_id_ = building_.get_id();
      menu_force_id_ = building.get_force_id();
    }

    int getImageID_1249()
    {
      return 1249;
    }

    bool isVisible()
    {
      if (ch::get_auto_affairs_status()) return false;
			if (building_.get_id() >= 城市_末) return false;
			if (!pk::is_player_controlled(pk::get_city(menu_city_id_))) return false;	// 플레이어 위임군단 도시인 경우 제외

			return menu_city_id_ != -1 and menu_force_id_ == pk::get_current_turn_force_id();
    }

    bool handler_太守自治设置()
    {
      BaseInfo@ base_p = @base_ex[menu_city_id_];
      // 征兵
      data[0] = { base_p.can_recruit ? 1 : 0, base_p.recruit_std_porder, base_p.recruit_std_gold, base_p.recruit_std_foodtroop };
      // 训练
      data[1] = { base_p.can_drill ? 1 : 0, base_p.drill_std };
      // 巡查
      data[2] = { base_p.can_inspections ? 1 : 0, base_p.inspections_std };
      // 褒奖
      data[3] = { base_p.can_reward ? 1 : 0, base_p.reward_std };
      // 运输
      data[4] = { base_p.can_transport ? 1 : 0 };

      create_taishu_autonomy_dialog();
      mode_select = base_p.auto_sequence;
      taishu_autonomy_dialog.set_visible(true);
      update_select_mode();
      create_city_setting(taishu_autonomy_dialog, menu_city_id_);
      return true;
    }

    string numberpad_t(int line, int original_value, int current_value)
		{
			return pk::encode("");
		}

    void update_select_mode()
    {
    	pk::sprite9@ bg0;
      // mode_select
    	for (int i = 0; i < int(mode_list.length); i += 1)
    	{
    		mode_select_btns[i].set_text(pk::encode('选择'));
    		taishu_autonomy_dialog.remove_child(mode_select_bg[i].get_id());
    		taishu_autonomy_dialog.remove_child(mode_select_text[i].get_id());

    		int x = 18 + 20;
				int y = 38 + (68 + line_height + 261 + 20) + 5 * (i + 1) + 30 * i;

				@ bg0 = taishu_autonomy_dialog.create_sprite9(mode_select == i ? 357 : 393);
        bg0.set_pos(x + 78 + 20, y);
        bg0.set_size(70, 30);
        @mode_select_bg[i] = @bg0;

        pk::text @text0 = taishu_autonomy_dialog.create_text();
        text0.set_pos(x + 78 + 20, y);
        text0.set_size(70, 30);
        text0.set_text(pk::encode('模式' + i));
        text0.set_text_align(0x4|0x20);
        @mode_select_text[i] = @text0;
    	}

    	mode_select_btns[mode_select].set_text(pk::encode('已选择'));

      BaseInfo@ base_p = @base_ex[menu_city_id_];
      base_p.auto_sequence = uint8(mode_select);
    }

    void create_city_setting(pk::dialog@ new_dialog, int city_id)
    {
    	pk::sprite9@ bg0;
      pk::face@ fac0;
      pk::button@ bt0;

    	for (int i = 0; i < int(action_list.length); i += 1)
    	{
    		int x = 18 + item_width * i + line_height * (i + 1);
    		int y = 68 + line_height + 40 + line_height;
    		int btn_h = 30;

    		for (int b = 0; b < int(action_list[i].length); b += 1)
    		{
    			int btn_x = x + 10;
	    		int btn_y = y + 10 + (btn_h + 10) * b;

	        @ bg0 = new_dialog.create_sprite9(393);
	        bg0.set_pos(btn_x, btn_y);
	        bg0.set_size(60, btn_h);

	        pk::text@ text0 = new_dialog.create_text();
	        text0.set_pos(btn_x, btn_y);  // y: 100 + 134 + 10; x: 26 + 10
	        text0.set_size(60, btn_h);
	        text0.set_text(pk::encode(action_list[i][b]));
	        text0.set_text_align(0x4|0x20);

	        string btn_text = '';
          if (b == 0) btn_text = data[i][b] == 0 ? '关闭' : '开启';
          else btn_text = formatInt(data[i][b]);

	        @ bt0 = new_dialog.create_button(214);
          bt0.set_pos(btn_x + 65, btn_y);  // y: 100 + 134 + 10; x: 26 + 10
          bt0.set_size(75, 30);
          bt0.set_text(pk::encode(btn_text));
          bt0.set_text_align(0x4|0x20);

          @button_list[i][b] = @bt0;

          pk::detail::funcref func0;
          pk::detail::funcref func1;
          pk::detail::funcref func2;
          pk::detail::funcref func3;
          pk::detail::funcref func4;

          if (b == 0) // 开关
          {
            func0 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle(main.menu_force_id_, main.menu_city_id_, 内政自动化_征兵);
              main.updateShowData();
            });
            func1 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle(main.menu_force_id_, main.menu_city_id_, 内政自动化_训练);
              main.updateShowData();
            });
            func2 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle(main.menu_force_id_, main.menu_city_id_, 内政自动化_巡察);
              main.updateShowData();
            });
            func3= cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle(main.menu_force_id_, main.menu_city_id_, 内政自动化_褒赏);
              main.updateShowData();
            });
            func4 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle(main.menu_force_id_, main.menu_city_id_, 内政自动化_运输);
              main.updateShowData();
            });
          }

          if (b == 1) // 各个类型不一样
          {
          	func0 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle_params(main.menu_force_id_, main.menu_city_id_, 内政自动化_征兵);
              main.updateShowData();
            });
            func1 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle_params(main.menu_force_id_, main.menu_city_id_, 内政自动化_训练);
              main.updateShowData();
            });
            func2 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle_params(main.menu_force_id_, main.menu_city_id_, 内政自动化_巡察);
              main.updateShowData();
            });
            func3= cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle_params(main.menu_force_id_, main.menu_city_id_, 内政自动化_褒赏);
              main.updateShowData();
            });
          }
          if (b == 2) // 征兵-资金
          {
          	func0 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle_params(main.menu_force_id_, main.menu_city_id_, 内政自动化_征兵, 2);
              main.updateShowData();
            });
          }
          if (b == 3) // 征兵-兵粮比
          {
          	func0 = cast<pk::button_on_pressed_t@>(function(button) {
              main.toggle_params(main.menu_force_id_, main.menu_city_id_, 内政自动化_征兵, 3);
              main.updateShowData();
            });
          }

          switch (i)
          {
            case 0:
              bt0.on_button_pressed(func0);
              break;
            case 1:
              bt0.on_button_pressed(func1);
              break;
            case 2:
              bt0.on_button_pressed(func2);
              break;
            case 3:
              bt0.on_button_pressed(func3);
              break;
            case 4:
              bt0.on_button_pressed(func4);
              break;
          }
    		}
    	}
    }

    void updateShowData()
    {
      BaseInfo@ base_p = @base_ex[menu_city_id_];
      // 征兵
      data[0] = { base_p.can_recruit ? 1 : 0, base_p.recruit_std_porder, base_p.recruit_std_gold, base_p.recruit_std_foodtroop };
      // 训练
      data[1] = { base_p.can_drill ? 1 : 0, base_p.drill_std };
      // 巡查
      data[2] = { base_p.can_inspections ? 1 : 0, base_p.inspections_std };
      // 褒奖
      data[3] = { base_p.can_reward ? 1 : 0, base_p.reward_std };
      // 运输
      data[4] = { base_p.can_transport ? 1 : 0 };

      for (int i = 0; i < int(data.length); i += 1)
      {
        for (int b = 0; b < int(data[i].length); b += 1)
        {

        	string btn_text = '';
          if (b == 0) btn_text = data[i][b] == 0 ? '关闭' : '开启';
          else btn_text = formatInt(data[i][b]);
          button_list[i][b].set_text(pk::encode(btn_text));
        }
      }
    }

    void toggle(int force_id, int city_id, int 内政自动化_项目)
		{
			BaseInfo@ base_t = @base_ex[city_id];
			switch (内政自动化_项目)
			{
				case 内政自动化_巡察: base_t.can_inspections = !base_t.can_inspections; break;
				case 内政自动化_训练: base_t.can_drill = !base_t.can_drill; break;
				case 内政自动化_征兵: base_t.can_recruit = !base_t.can_recruit; break;
				case 内政自动化_褒赏: base_t.can_reward = !base_t.can_reward; break;
				case 内政自动化_运输: base_t.can_transport = !base_t.can_transport; break;
			}
		}

		void toggle_params(int force_id, int city_id, int 内政自动化_项目, int params = 1)
		{
			BaseInfo@ base_t = @base_ex[city_id];
			int max_num;
			int default_num;
			if (params == 1)
			{
				max_num = 99;
				if (内政自动化_项目 == 内政自动化_征兵) max_num = 100;
				if (内政自动化_项目 == 内政自动化_训练) max_num = pk::has_tech(pk::get_force(force_id), 技巧_熟练兵) ? int(pk::core["部队.熟练兵最大气力"]) : int(pk::core["部队.最大气力"]);
				switch (内政自动化_项目)
				{
					case 内政自动化_巡察: default_num = base_t.inspections_std; break;
					case 内政自动化_训练: default_num = base_t.drill_std; break;
					case 内政自动化_征兵: default_num = base_t.recruit_std_porder; break;
					case 内政自动化_褒赏: default_num = base_t.reward_std; break;
				}
			}
			if (params == 2) // 征兵-资金
			{
				max_num = 100000;
				default_num = base_t.recruit_std_gold;
			}
			if (params == 3) // 征兵-兵粮比
			{
				max_num = 1000;
				default_num = base_t.recruit_std_foodtroop;
			}
			pk::int_bool deal = pk::numberpad(pk::encode("内政标准"), 0, max_num, default_num, pk::numberpad_t(numberpad_t));

      if (deal.second)
      {
      	if (params == 1)
      	{
      		switch (内政自动化_项目)
					{
						case 内政自动化_巡察: base_t.inspections_std = deal.first; break;
						case 内政自动化_训练: base_t.drill_std = deal.first; break;
						case 内政自动化_征兵: base_t.recruit_std_porder = deal.first; break;
						case 内政自动化_褒赏: base_t.reward_std = deal.first; break;
					}
      	}
      	else if (params == 2)
      	{
      		base_t.recruit_std_gold = deal.first;
      	}
      	else
      	{
      		base_t.recruit_std_foodtroop = deal.first;
      	}
      }
		}
	}
	Main main;
}
