
namespace 募兵测试
{
	// ================ CUSTOMIZE ================

	const int TP_COST = 100; 	 // 기교P 필요량
	const int ACTION_COST = 50;  // 행동력 필요량
	const int energy_ = 0; 	 // 기력 상승 수치
	const int 募兵倍率 = 100;

	// ===========================================
	class Main
	{
		///1.先随便定义几个全局变量，这里就定义了建筑指针，势力指针，武将指针，城市指针这几个变量，这里定义的变量可以在此命名空间里通用
		///指针的格式基本上是pk::building@ 或者pk::person@这样pk::xxx(指针的英文拼写)@，然后@后面写的是你要定义的指针希望他叫什么，比方
		///说pk::building@ base;这样就定义了一个叫做base的建筑指针，这个指针在初始化后就可以直接拿来用。完整的定义加初始化的过程应该是
		///pk::building@ base = pk::get_buiding(0);这里的0就是建筑id，也就是襄平(具体建筑id和建筑的对应关系可以在van里看)，这样就获得了
		///襄平的建筑指针。
		pk::building@ building_;
		pk::force@ force_;
		pk::person@ taishu_;
		pk::city@ city_;

		Main()
		{
			pk::menu_item item;//定义一个菜单按钮的变量，变量名为item(当然也可以写成其他名称如item_募兵测试等)
			item.menu = 101;//定义该菜单按钮所在的菜单位置，对应关系如下
			//一级菜单：0建筑 1部队 2用户(按住SHIFT) 二级菜单(在一级菜单下)：100 城市，101 军事，102 人才，103 外交，104 计谋， 105君主，113 战法 115 计谋。

			item.init = pk::building_menu_item_init_t(init);//这段代码表示菜单初始化过程中要干什么，菜单初始化发生在点击菜单之前
			item.is_enabled = pk::menu_item_is_enabled_t(isEnabled);
			item.get_text = pk::menu_item_get_text_t(getText);
			item.get_desc = pk::menu_item_get_desc_t(getDesc);
			item.handler = pk::menu_item_handler_t(handler);
			pk::add_menu_item(item);
			//运行这段代码要执行的内容的填写处，这个cpp主要是为城市菜单添加了一个募兵的按钮
		}

		void init(pk::building@ building)
		{
			//点击城市菜单初始化的变量，变量需要初始化后才能使用
			@building_ = @building;
			@force_ = pk::get_force(building.get_force_id());
			@taishu_ = pk::get_taishu_id(building) == -1 ? null : pk::get_person(pk::get_taishu_id(building));
			@city_ = pk::building_to_city(building);
		}

		bool isEnabled()
		{
			//此处是判定菜单在上面情况下可以点击
			if (taishu_ is null) return false;
			else if (taishu_.action_done == true) return false;
			return true;
		}

		string getText()
		{
			//此处填写的是菜单的在游戏中的显示名称
			return pk::encode("募兵改");
		}

		string getDesc()
		{
			//此处填写的是鼠标指向菜单按钮后，屏幕最下方显示的文字，一般和前面的isEnabled一起使用，用一样的判断条件
			if (taishu_ is null)
				return pk::encode("无太守无法募兵.");
			else if (taishu_.action_done == true)
				return pk::encode("太守已行动.");
			else
				return pk::encode(pk::format("募集士兵.(技巧点 {}, 行动力 {})", TP_COST, ACTION_COST));
		}

		bool handler()
		{
			//此处填写的点击菜单后要执行的操作

			string taishu_name = pk::decode(pk::get_name(taishu_));
			pk::person@ young_man = pk::get_person(무장_청년);

			pk::message_box(pk::encode(pk::format("\x1b[1x{}\x1b[0x大人, 我们前来相助.请接受我们吧.", taishu_name)), young_man);
			pk::message_box(pk::encode("哈哈哈!托各位的福, 我感觉得到了千军万马."), taishu_);

			int troops_ = 2000 + 20 * taishu_.stat[武将能力_统率];
			
			// 기교 감소 및 병력, 기력 상승.
			pk::message_box(pk::encode(pk::format("技巧点减少了\x1b[1x{}\x1b[0x.", TP_COST)));
			ch::add_tp(force_, -TP_COST, force_.get_pos());
			pk::message_box(pk::encode(pk::format("兵力增加了 \x1b[1x{}\x1b[0x.", troops_)));
			ch::add_troops(building_, troops_, true);
			pk::message_box(pk::encode(pk::format("士气增加了 \x1b[1x{}\x1b[0x.", energy_)));
			pk::add_energy(building_, energy_, true);
			// 행동력 감소.
			auto district = pk::get_district(pk::get_district_id(force_, 1));
			pk::add_ap(district, -ACTION_COST);

			taishu_.action_done = true;

			return true;
		}

	}

	Main main;
}