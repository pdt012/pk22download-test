/*
  作者：黑店小小二
  科技加强包
  思路设计来源于淡然大佬（大浪淘沙mod作者）跟GaZeLle大佬（风林火山mod作者）,已取得授权
  本扩展包完全免费。如需使用，请联系作者本人 ┓( ´∀` )┏
*/

// ## 2022/08/19 # 茕茕 # 此包修改，请联系作者黑店小小二，私修出错概不理会 ##
// ## 2022/05/21 # 黑店小小二 # 完成科技加强 ##

namespace 科技加强包 {

  bool 开启_科技加强包 = true;
  bool 玩家生效 = true;


  class Main {

    int 防卫强化减气 = 10;
    int 防卫强化影响范围 = 3;

    pk::func53_t@ prev_callback_53;
    pk::func150_t@ prev_callback_150;
    pk::func150_t@ prev_callback_151;
    pk::func162_t@ prev_callback_162;
    pk::func209_t@ prev_callback_209;

    array<string> tech = {
      /*枪兵锻练*/"枪兵战法的威力增加",
      /*兵粮袭击*/"枪兵在攻击时能夺取敌部队的部分兵粮",
      /*奇袭*/"在森林地形，枪兵攻击对方不会遭到反击",
      /*精锐枪兵*/"枪兵的攻击、防御、移动皆大幅强化",

      /*戟兵锻练*/"戟兵战法的威力增加",
      /*矢盾*/"面对弓矢攻击，有30％的防御概率",
      /*大盾*/"面对普通攻击，有30％的防御概率",
      /*精锐戟兵*/"戟兵的攻击、防御、移动皆大幅强化",

      /*弩兵锻练*/"弩兵战法的威力增加",
      /*应射*/"弩兵被弓矢攻击时，可发动反击",
      /*强弩*/"弩兵及城市反击的射程增加１格",
      /*精锐弩兵*/"弩兵的攻击、防御、移动皆大幅强化",

      /*骑兵锻练*/"骑兵战法的威力增加",
      /*良马产出*/"骑兵的移动力增加",
      /*骑射*/"骑兵可以使用弓矢攻击",
      /*精锐骑兵*/"骑兵的攻击、防御、移动皆大幅强化",

      /*熟练兵*/"兵士的气力上限增加20",
      /*难所行军*/"可以通过间道和浅滩，过栈道受到的损伤降低",
      /*军制改革*/"编成部队的兵士上限增加3000",
      /*云梯*/"部队对据点的攻击力增加",

      /*车轴强化*/"兵器的移动力增加",
      /*石造建筑*/"可建造障害物「石壁」、施设「石兵八阵」",
      /*投石开发*/"可建造兵器「投石、斗舰」、施设「投石台」",
      /*霹雳*/"投石攻击时、对目标周围的部队造成连锁伤害",

      /*工兵育成*/"每回合建筑耐久度的回复量增加",
      /*施设强化*/"可建造军事设施「砦、连弩橹」",
      /*城壁强化*/"可建造军事设施「城塞」、据点的耐久增加",
      /*防卫强化*/"据点的反击能力强化",

      /*木兽开发*/"可建造功城兵器「木兽」",
      /*神火计*/"火计的施放距离增为３格",
      /*火药炼成*/"可以制造「火炎种、火炎球」",
      /*爆药炼成*/"可以建造「业火种、业火球」。火的威力强化",

      /*木牛流马*/"输送队的移动力增加",
      /*城关扩张*/"关隘和支城的钱、粮、兵士、兵器上限增加",
      /*政令整备*/"治安下降速度减慢",
      /*人心掌握*/"所属武将的忠诚度下降减慢",
    };

    Main()
    {
      pk::bind(102, 1, pk::trigger102_t(onLoadScenario));
      pk::bind(108, pk::trigger108_t(onMonthStart));
      pk::bind(111, pk::trigger111_t(onTurnStart));
      pk::bind(175, pk::trigger175_t(onUnitDestory));

      @prev_callback_53 = cast<pk::func53_t@>(pk::get_func(53));
      pk::reset_func(53);
      pk::set_func(53, pk::func53_t(func53));

      @prev_callback_150 = cast<pk::func150_t@>(pk::get_func(150));
      pk::reset_func(150);
      pk::set_func(150, pk::func150_t(func150));

      @prev_callback_151 = cast<pk::func151_t@>(pk::get_func(151));
      pk::reset_func(151);
      pk::set_func(151, pk::func150_t(func151));

      @prev_callback_162 = cast<pk::func162_t@>(pk::get_func(162));
      pk::reset_func(162);
      pk::set_func(162, pk::func162_t(func162));

      @prev_callback_209 = cast<pk::func209_t@>(pk::get_func(209));
      pk::reset_func(209);
      pk::set_func(209, pk::func209_t(func209));

    }

    bool is_player_effect(pk::force@ force)
    {
      return 玩家生效 or !pk::is_player_controlled(force);
    }

    void onLoadScenario()
    {
      if (开启_科技加强包)
      {
        pk::get_tech(技巧_奇袭).desc = pk::encode("森地形枪兵不被反击、枪兵攻击带减气");

        pk::get_tech(技巧_大盾).desc = pk::encode("30％的概率无视普攻、30%的概率避免减气");

        pk::get_tech(技巧_强弩).desc = pk::encode("弩兵及城市反击的射程增加、弩战法耗气减少");

        pk::get_tech(技巧_骑射).desc = pk::encode("骑兵可用弓矢攻击、灭队后敌将高概率受伤");

        pk::get_tech(技巧_防卫强化).desc = pk::encode(pk::format("提高据点反击伤害、{}格敌军几率掉气", 防卫强化影响范围));

        pk::get_tech(技巧_港关扩张).desc = pk::encode("提升据点资源容量、城市粮月收入增加20%");

        pk::get_tech(技巧_木牛流马).desc = pk::encode("输送队移动力增加、城市钱月收入提升20%");

        pk::get_tech(技巧_政令整备).desc = pk::encode("治安下降速度减慢、行动力回复提升20%");

        pk::get_tech(技巧_掌握人心).desc = pk::encode("武将忠诚不易下降、每月提升据点治安与士气");
      }
      else
      {
        pk::get_tech(技巧_奇袭).desc = pk::encode(tech[技巧_奇袭]);
        pk::get_tech(技巧_大盾).desc = pk::encode(tech[技巧_大盾]);
        pk::get_tech(技巧_强弩).desc = pk::encode(tech[技巧_强弩]);
        pk::get_tech(技巧_骑射).desc = pk::encode(tech[技巧_骑射]);
        pk::get_tech(技巧_防卫强化).desc = pk::encode(tech[技巧_防卫强化]);
        pk::get_tech(技巧_港关扩张).desc = pk::encode(tech[技巧_港关扩张]);
        pk::get_tech(技巧_木牛流马).desc = pk::encode(tech[技巧_木牛流马]);
        pk::get_tech(技巧_政令整备).desc = pk::encode(tech[技巧_木牛流马]);
        pk::get_tech(技巧_掌握人心).desc = pk::encode(tech[技巧_掌握人心]);
      }
    }

    void onTurnStart(pk::force@ force)
    {
      if (is_player_effect(force) and pk::has_tech(force, 技巧_防卫强化))
      {
        pk::list<pk::city@> city_list = pk::get_city_list(force);
        for (int i = 0; i < int(city_list.count); i++)
        {
          pk::city@ city = city_list[i];
          array<pk::point> rings = pk::range(city.get_pos(), 1, 防卫强化影响范围);
          for (int m = 0; m < int(rings.length);m++)
          {
            pk::point dst_pos = rings[m];
            pk::unit@ dst = pk::get_unit(dst_pos);
            if (dst !is null and dst.get_force_id() != force.get_force_id() and !force.ally[dst.get_force_id()])
            {
              if (pk::rand_bool(150 - pk::get_person(dst.leader).stat[武将能力_统率])) pk::add_energy(dst, -防卫强化减气, true);
            }
          }
        }
      }
    }

    int func53(pk::unit@ unit, pk::tactics@ tactic, int type)
    {
      int result = prev_callback_53(unit, tactic, type);
      pk::force@ force = pk::get_force(unit.get_force_id());
      if (is_player_effect(force) and pk::has_tech(force, 技巧_强弩) and unit.weapon == 兵器_弩)
      {
        result -= int(result * 0.1f);
      }
      return result;
    }

    int func150(pk::city@ city)
    {
      int result = prev_callback_150(city);
      pk::force@ force = pk::get_force(city.get_force_id());
      if (is_player_effect(force) and pk::has_tech(force, 技巧_木牛流马)) result += int(0.2f * result);
      return result;
    }

    int func151(pk::city@ city)
    {
      int result = prev_callback_151(city);
      pk::force@ force = pk::get_force(city.get_force_id());
      if (is_player_effect(force) and pk::has_tech(force, 技巧_港关扩张)) result += int(0.2f * result);
      return result;
    }

    int func162(pk::district@ district)
    {
      int result = prev_callback_162(district);
      pk::force@ force = pk::get_force(district.get_force_id());
      if (is_player_effect(force) and pk::has_tech(force, 技巧_政令整备) and district.no == 1)
      {
        return result += int(result * 0.2f);
      }
      return result;
    }

    void func209(pk::damage_info& info, pk::unit@ attacker, int tactics_id, const pk::point& in target_pos, int type, int critical, bool ambush, int rettype)
    {
      prev_callback_209(info, attacker, tactics_id, target_pos, type, critical, ambush, rettype);

      if (开启_科技加强包)
      {
        pk::force@ force = pk::get_force(attacker.get_force_id());
        pk::unit@ target = pk::get_unit(target_pos);
        if (is_player_effect(force) and attacker.weapon == 兵器_枪) 技巧_奇袭_加强(info, force);
        if (target !is null and target.weapon == 兵器_戟) 技巧_大盾_加强(info, target);
      }
    }

    void onMonthStart()
    {
      pk::list<pk::force@> force_list = pk::get_force_list();
      for (int i = 0; i < int(force_list.count); i++)
      {
        pk::force@ force = force_list[i];
        if (force.get_id() < 非贼势力_末 and is_player_effect(force) and pk::has_tech(force, 技巧_掌握人心))
        {
          pk::list<pk::city@> city_list = pk::get_city_list(force);
          for (int m = 0; m < int(city_list.count); m++)
          {
            pk::city@ city = city_list[m];
            pk::add_energy(city, 10, true);
            ch::add_public_order(city, 5, true);
          }
        }
      }
    }

    void onUnitDestory(pk::unit@ attacker, pk::unit@ target)
    {
      pk::force@ force = pk::get_force(attacker.get_force_id());
      if (is_player_effect(force) and pk::has_tech(force, 技巧_骑射) and attacker.weapon == 兵器_战马)
      {
        for (int m = 0; m < 3; m++)
        {
          if (pk::is_valid_person_id(target.member[m]))
          {
            pk::person@ member_t = pk::get_person(target.member[m]);
            if (member_t is null || !pk::is_alive(member_t)) continue;
            if (pk::rand_bool(100)) member_t.shoubyou = member_t.shoubyou < 伤病_濒死 ? (member_t.shoubyou + 1) : 伤病_濒死;
          }
        }
      }
    }

    void 技巧_奇袭_加强(pk::damage_info& info, pk::force@ force)
    {
      if (pk::has_tech(force, 技巧_奇袭)) info.energy_damage += 5;
    }

    void 技巧_大盾_加强(pk::damage_info& info, pk::unit@ unit)
    {
      pk::force@ force = pk::get_force(unit.get_force_id());
      if (is_player_effect(force) and pk::has_tech(force, 技巧_大盾) and pk::rand_bool(30)) info.energy_damage = 0;
    }
  }
  Main main;
}
