// ## 2023/03/31 #铃 # 从大浪1.06中摘出来,翻译部分注释,补全历史版本信息 ,
// ## 2022/07/18 #广汉#  增加多个随机事件，特技-仁政，安民，祈愿才能使用仁政##
// ## 2022/03/31 # 茕茕# 添加仁政 ##

/*
@제작자: HoneyBee
@설명: 폭정을 행할 수 있는 메뉴 SCRIPT

※ 동탁, 손호 등.. 삼국지의 유명 폭군들의 CONCEPT을 반영한 SCRIPT입니다.

(1) 이벤트 - 금 or 군량을 획득
(2) 도시 초토화 - 군주가 소속된 도시를 공백지로 변경 (세력의 도시가 2개 이상인 경우만 가능)
(3) 도시 초토화를 사용하면 후한황제를 옹립한 세력의 도시일 경우, 후한황제를 해당 도시에 두고 이동함 (참고사항)
(4) 폭정을 행한 횟수에 따라서 군주의 상성, 야망, 말투가 변경됨
(5) 폭정을 행한 횟수에 따라서 군주의 별명이 추가 및 변경됨 (소인 -> 악인 -> 폭군)
*/


namespace 仁政
{

	// ================ CUSTOMIZE ================

	const int TP_COST = 250; 	  
	const int ACTION_COST = 70;  

	const int safety_ = 5;      
	const int city_hp = 100;      

	const array<int> food_ = { 10000, 8000, 5000 }; 	  // 백성들에게 약탈한 군량 수치 --长歌逍遥--食物
	const array<int> gold_ = { 2000, 1800, 1500 }; 	  // 백성들에게 약탈한 금 수치 --长歌逍遥羡仙-金钱
	const array<int> troops_ = { 10000, 12500, 15000 };  //长歌逍遥羡仙--新增军队人数
	const int ADD_WEAPON_1 = 15000; 
	const int ADD_WEAPON_2 = 2; 
	const int pop_dec_ = 5000;  //仁政固定新增人口常量，修改值变小点：5000
	const int food_num = 20000;  //聚养消耗兵粮
	const int gold_num = 2000;  //聚养金钱消耗
	const int invest_gold=800;  //投资金钱
	const int cost_food=5000;  //花费粮食
	const int cost_troops=3000;   //减少士兵数量

// ===========================================

	const int KEY = pk::hash("폭정");

	class Main
	{

		pk::building@ building_;
		pk::force@ force_;
		pk::person@ kunshu_;
		pk::person@ farmer_;
		pk::person@ merchant_;
		pk::person@ officer_;
		pk::person@ civilian_;
		pk::city@ city_;
		bool has_skill;
		
		

		Main()
		{
			pk::menu_item item;
			item.menu = 100;
			item.pos = 9;
			item.shortcut = "9";
			item.init = pk::building_menu_item_init_t(init);
			item.is_visible = pk::menu_item_is_visible_t(isVisible);
			item.is_enabled = pk::menu_item_is_enabled_t(isEnabled);
			item.get_text = pk::menu_item_get_text_t(getText);
			item.get_desc = pk::menu_item_get_desc_t(getDesc);
			item.handler = pk::menu_item_handler_t(handler);
			pk::add_menu_item(item);
		}

		void init(pk::building@ building)
		{
			@building_ = @building;
			@force_ = pk::get_force(building.get_force_id());
			@kunshu_ = pk::get_person(pk::get_kunshu_id(building));
			@farmer_ = pk::get_person(무장_농민);
			@officer_ = pk::get_person(무장_무관);
			@merchant_ = pk::get_person(무장_상인); //商人
			@civilian_ = pk::get_person(무장_문관);
			@city_ = pk::building_to_city(building);
			has_skill=has_skill_();  //设置函数判断是否有相应的函数
		}

		bool isVisible()  //判断君主是否闲置
		{
			//pk::trace("kunshu_.service" + kunshu_.service + "building_.get_id()" + building_.get_id());
			if (kunshu_.service != building_.get_id()) return false;  //判断君主所属
			return true;
		}

		bool isEnabled()  //满足条件
		{
			int city_gold=city_.gold;
			int city_fold=city_.food;
			if (pk::is_absent(kunshu_) or pk::is_unitize(kunshu_)) return false;
			else if (kunshu_.action_done == true) return false;
			else if (force_.tp < TP_COST) return false;
			else if (pk::get_district(pk::get_district_id(force_, 1)).ap < ACTION_COST) return false;
			else if (city_gold<3000 or city_fold<10000) return false; //避免出现把城市金钱和粮食清空的状况
			else if (!has_skill) return false;
			return true;
		}

		string getText() //功能界面
		{
			return pk::encode("仁政");
		}

		string getDesc()  //判断消耗条件
		{
			int city_gold=city_.gold; //获取当前城市的金钱
			int city_fold=city_.food; //获取当前城市的粮食
			if (pk::is_absent(kunshu_) or pk::is_unitize(kunshu_))
				return pk::encode("君主不在.");
			else if (kunshu_.action_done == true)
				return pk::encode("君主已行动.");
			else if (force_.tp < TP_COST)
				return pk::encode(pk::format("技巧不足.(需要{}技巧)", TP_COST));
			else if (pk::get_district(pk::get_district_id(force_, 1)).ap < ACTION_COST)
				return pk::encode(pk::format("行动力不足(需要{}行动力)", ACTION_COST));
			else if (city_gold<3000 or city_fold<10000)
				return pk::encode("城市金钱不得小于3000，粮食不得小于10000");
			else if(!has_skill)
				return pk::encode("城中武将需要拥有安民、仁政、祈愿特技之一");
			else
				return pk::encode(pk::format("实施仁政.(需要{}技巧, {}行动力)", TP_COST, ACTION_COST));
		}
		
		bool has_skill_()
		{
			pk::list<pk::person@> building_person_list=pk::get_idle_person_list(building_);
			bool person_has_skill=false;
			for(int i=0;i<building_person_list.count;i++)
			{
				pk::person@person_=building_person_list[i];
				int skill_id=person_.skill; //获取特技名称，有用来判断安民、仁政、祈愿
				if(skill_id==特技_安民 or skill_id==特技_仁政 or skill_id==特技_祈愿)  //三个挂钩
				{
					person_has_skill=true;
					break;
				}
			}
			return person_has_skill;
		}

		bool handler()
		{

			if (pk::choose(pk::encode("要实施仁政吗?"), { pk::encode(" 是 "), pk::encode(" 否 ") }) == 1)
				return false;


			array<string> select_list =
			{
				pk::encode("仁政"),    // 0
				pk::encode("取消")  //2
			};

			int select_command = pk::choose(pk::encode("请选择要实施的仁政."), select_list);
			if (select_command == 0)
			{
				pk::scene(pk::scene_t(scene_Event));
				switch(pk::rand(3))
				{
					case 0:
						if (pk::is_in_screen(city_.get_pos())) pk::say(pk::encode("王者行仁政，无敌于天下！"), pk::get_person(kunshu_.get_id()), city_);
						break;
					case 1:
						if (pk::is_in_screen(city_.get_pos())) pk::say(pk::encode("仁风所感，虎狼出境！"), pk::get_person(kunshu_.get_id()), city_);
						break;
					case 2:
						if (pk::is_in_screen(city_.get_pos())) pk::say(pk::encode("以仁服人，何人不服！"), pk::get_person(kunshu_.get_id()), city_);
						break;
				}
			}
			else if (select_command == 1)   //取消
			{
				pk::encode("取消仁政.");
				return false;
			}

			if ((kunshu_.azana_read != pk::encode("廉君")) and (kunshu_.azana_read != pk::encode("明君"))
				and (kunshu_.azana_read != pk::encode("圣君")))
			{
				kunshu_.azana_read = pk::encode("廉君");
				kunshu_.aishou = 140; // 상성을 140으로 변경 
				kunshu_.update();
			}

			else if (kunshu_.azana_read == pk::encode("廉君"))
			{
				kunshu_.azana_read = pk::encode("明君");
				kunshu_.ambition = 야망_매우높음;
				kunshu_.update();
			}

			else if (kunshu_.azana_read == pk::encode("明君"))
			{
				kunshu_.azana_read = pk::encode("圣君");
				kunshu_.tone = 말투_난폭;
				kunshu_.update();
			}

			// 행동력 감소.
			auto district = pk::get_district(pk::get_district_id(force_, 1));
			pk::add_ap(district, -ACTION_COST);

			// 군주 행동 완료 
			kunshu_.action_done == true;

			return true;

		}

		void scene_Event()  //巡视民间
		{
			int n = pk::rand(100);//120概率
			/*
			收粮---8%
			赈粮---8%
			减税---8%
			赈金---8%
			扩兵---8%
			加兵装--10%
			投资商业---2%
			投资农业---2%
			轻徭薄赋---8%
			赈济灾民---8%（金，粮）
			减金钱---10%
			减兵装---10%
			育民---10%

			*/
            int people=1000+pk::rand(1001);//增加点随机变量因素给人口带来不定向性，人口上限取值为（5000，6000），削弱点，后面有修养生息增加人口了
			if (n <8)  
			{
				pk::fade(0); //画面亮度0-255，越小越暗
				pk::sleep();  //睡眠停止待命专用，一般用于发生剧情
				pk::background(16);
				pk::fade(255); //画面最亮，剧情应该有暗-亮-暗-亮

				pk::message_box(pk::encode("额，你们在干什么?"), kunshu_);
				pk::message_box(pk::encode("近来庄稼长得好，村民们都在庆祝呢."), farmer_);
				pk::message_box(pk::encode("粮食居然丰收这么好，那下个月的粮食提前上缴."), kunshu_);
				pk::message_box(pk::encode("接下来每个季节只上缴一次粮食"), kunshu_);
				pk::message_box(pk::encode("谢谢大人，粮食即可奉上"), farmer_);

				pk::background(16);  //农田背景
				pk::message_box(pk::encode("立即把粮食运回官仓!"), kunshu_);
				pk::message_box(pk::encode("遵命!"), officer_);
				pk::message_box(pk::encode("再见，大人."), farmer_);

				int food_num = 10000+pk::rand(5001); //自定义随机变量--长歌逍遥羡仙（10000，15000）

				pk::message_box(pk::encode(pk::format("兵粮增加了\x1b[1x{}\x1b[0x.", food_num)));
				pk::add_food(city_, food_num, true);
				pk::add_tp(force_, -TP_COST, force_.get_pos());
				pk::add_public_order(city_, +safety_, true);
                pk::message_box(pk::encode("报！附近的难民听闻主上实施仁政，成群结伴从四面赶来！"), officer_);
                 pk::message_box(pk::encode("感谢大人，收留我等草民！我等必效忠于明君！"), farmer_);
                pk::message_box(pk::encode(pk::format("人口增加了\x1b[1x{}\x1b[0x.", pop_dec_+people)));
				ch::add_population(city_.get_id(), +(pop_dec_+people));//给人口带来不定向性--长歌逍遥羡仙

				pk::fade(0);
				pk::sleep();
				pk::background(-1);
				pk::fade(255);
			}

			else if (n < 16)
			{
				pk::fade(0);
				pk::sleep();
				pk::background(16);
				pk::fade(255);

				pk::message_box(pk::encode("额，你们在干什么?"), kunshu_);
				pk::message_box(pk::encode("近来庄稼长得较差，村民们正在为此发愁."), farmer_);
				pk::message_box(pk::encode("今年行情居然这么差，那不能苦了百姓生活."), kunshu_);
				pk::message_box(pk::encode("督粮官，开仓放粮"), kunshu_);
				pk::message_box(pk::encode("谢谢大人救人于水火之中"), farmer_);

				pk::background(16);
				pk::message_box(pk::encode("督粮官，立即把所带粮食全部分发下去"), kunshu_);
				pk::message_box(pk::encode("遵命!"), officer_);
				pk::message_box(pk::encode("谢主恩赐！"), farmer_);

				int food_num = -food_[pk::rand(3)];

				pk::message_box(pk::encode(pk::format("城市中兵粮\x1b[1x{}\x1b[0x.", food_num)));
				pk::add_food(city_, food_num, true);
				pk::add_tp(force_, -TP_COST, force_.get_pos());
				pk::add_public_order(city_, +safety_, true);
				pk::message_box(pk::encode("报！附近的难民听闻主上实施仁政，成群结伴从四面赶来！"), officer_);
                pk::message_box(pk::encode("感谢大人，收留我等草民！我等必效忠于明君！"), farmer_);
                pk::message_box(pk::encode(pk::format("人口增加了\x1b[1x{}\x1b[0x.", pop_dec_+people)));
				ch::add_population(city_.get_id(), +(pop_dec_+people));//给人口带来不定向性--长歌逍遥羡仙
				

				pk::fade(0);
				pk::sleep();
				pk::background(-1);
				pk::fade(255);
			}

			else if (n <24)
			{
				pk::fade(0);
				pk::sleep();
				pk::background(4);
				pk::fade(255);

				pk::message_box(pk::encode("街上好像很热闹."), kunshu_);
				pk::message_box(pk::encode("哈哈哈!请进。"), merchant_);
				pk::message_box(pk::encode("生意好吗?"), kunshu_);
				pk::message_box(pk::encode("你看，这里客人很多，生意很好。"), merchant_);
				pk::message_box(pk::encode("嗯. 如此看来下个月的税金也可以上缴了"), kunshu_);
				pk::message_box(pk::encode("发布下去，凡上缴当月税金后，代替季节税金"), kunshu_);
				pk::message_box(pk::encode("感谢大人（又减少两个月税啦！！）"), merchant_);

				pk::background(4);  //街道背景
				pk::message_box(pk::encode("立即把上缴税金拿进国库!"), kunshu_);
				pk::message_box(pk::encode("遵命!"), officer_);
				pk::message_box(pk::encode("明君微税必然赢得百姓爱戴."), merchant_);

				int gold_num = 2000+pk::rand(1001);//金钱（2000,3000）

				pk::message_box(pk::encode(pk::format("金因此增加了\x1b[1x{}\x1b[0x。", gold_num)));
				pk::add_gold(city_, gold_num, true);
				pk::add_tp(force_, -TP_COST, force_.get_pos());
				pk::add_public_order(city_, +safety_, true);
				pk::message_box(pk::encode("报！附近的难民听闻主上实施仁政，成群结伴从四面赶来！"), officer_);
                                      pk::message_box(pk::encode("感谢大人，收留我等草民！我等必效忠于明君！"), farmer_);
                                      pk::message_box(pk::encode(pk::format("人口增加了\x1b[1x{}\x1b[0x.", pop_dec_+people)));
				ch::add_population(city_.get_id(), +(pop_dec_+people));//给人口带来不定向性--长歌逍遥羡仙

				pk::fade(0);
				pk::sleep();
				pk::background(-1);
				pk::fade(255);
			}

			else if (n <32)
			{
				pk::fade(0);
				pk::sleep();
				pk::background(4);
				pk::fade(255);

				pk::message_box(pk::encode("街上看起来非常萧条."), kunshu_);
				pk::message_box(pk::encode("是主公吗？"), merchant_);
				pk::message_box(pk::encode("商家最近情况好吗?"), kunshu_);
				pk::message_box(pk::encode("连年战火，百姓流离，早已无法正常运营小店了"), merchant_);
				pk::message_box(pk::encode("什么！百姓居然如此艰苦，传令！砸开国库，银两统统散发给百姓安居！"), kunshu_);
				pk::message_box(pk::encode("昔日战火，今日我亲自给百姓筑房！通知各官员，都来给我为百姓筑房"), kunshu_);
				pk::message_box(pk::encode("主公真能如此吗？我等必带动村民回家"), merchant_);

				pk::background(4);
				pk::message_box(pk::encode("传令将士，务必将百姓安顿，违令者斩!"), kunshu_);
				pk::message_box(pk::encode("遵命!"), officer_);
				pk::message_box(pk::encode("我等必效忠于明君！"), merchant_);
				int gold_num = 300+pk::rand(300);

				pk::message_box(pk::encode(pk::format("城市中金\x1b[1x{}\x1b[0x。", gold_num)));
				pk::add_gold(city_, -gold_num, true);
				pk::add_tp(force_, -TP_COST, force_.get_pos());
				pk::add_public_order(city_, +safety_, true);
				pk::message_box(pk::encode("报！附近的难民听闻主上实施仁政，成群结伴从四面赶来！"), officer_);
                pk::message_box(pk::encode("感谢大人，收留我等草民！我等必效忠于明君！"), farmer_);
                pk::message_box(pk::encode(pk::format("人口增加了\x1b[1x{}\x1b[0x.", pop_dec_+people)));
				ch::add_population(city_.get_id(), +(pop_dec_+people));//给人口带来不定向性--长歌逍遥羡仙

				pk::fade(0);
				pk::sleep();
				pk::background(-1);
				pk::fade(255);
			}

			else if (n < 40)
			{
				pk::fade(0);
				pk::sleep();
				pk::background(4);
				pk::fade(255);

				pk::message_box(pk::encode("近来形势严峻，需要扩充兵力."), kunshu_);
				pk::message_box(pk::encode("主公可以散粮募兵，城中仍有足够壮丁可以充军"), officer_);
				pk::message_box(pk::encode("可以，待我亲自观察完民情，再做决定"), kunshu_);
				pk::message_box(pk::encode("大人，近来生意火爆，国民富裕呀！"), merchant_);
				pk::message_box(pk::encode("大人，如今小民已足以养活全家七口人了，粮食富裕呀！"), farmer_);
				pk::message_box(pk::encode("有此良态，足以募兵充军"), kunshu_);
			    pk::background(场景_宫廷1); //场景背景
				pk::message_box(pk::encode("传令，征兵后，给兵役家人送去足够的粮食"), kunshu_);
				pk::message_box(pk::encode("遵命!"), officer_);
				pk::message_box(pk::encode("后代交于明公，我等无悔"), farmer_);
				int troops_num = troops_[pk::rand(3)];
				int food_num = -food_[pk::rand(3)];
			    pk::message_box(pk::encode(pk::format("城市的兵力加了\x1b[1x{}\x1b[0x.", troops_num)));
				pk::message_box(pk::encode(pk::format("城市中兵粮\x1b[1x{}\x1b[0x.", food_num)));
				pk::add_food(city_, food_num, true);
				pk::add_troops(city_, troops_num, true);
				pk::add_tp(force_, -TP_COST, force_.get_pos());
				pk::fade(0);
				pk::sleep();
				pk::background(-1);
				pk::fade(255);
			}

			else if (n < 50)  //10%
			{
				pk::fade(0);
				pk::sleep();
				pk::background(4);
				pk::fade(255);
				pk::message_box(pk::encode("近来形势严峻，急需大批军备物资."), kunshu_);
				pk::message_box(pk::encode("主公可以向城中商人大量购买军备"), officer_);
				pk::message_box(pk::encode("传令，高价向城市中商人购买军备物资"), kunshu_);
				pk::message_box(pk::encode("大人，此处物资居多，足以充实军备"), merchant_);
				pk::message_box(pk::encode("好！"), kunshu_);
				int gold_num = -gold_[pk::rand(2)];
				pk::message_box(pk::encode(pk::format("城市的枪，戟，弩各增加了\x1b[1x{}\x1b[0x.", ADD_WEAPON_1)));
	                                                pk::add_weapon_amount(city_, 병기_창, ADD_WEAPON_1, true);
				pk::add_weapon_amount(city_, 병기_극, ADD_WEAPON_1, true);
				pk::add_weapon_amount(city_, 병기_노, ADD_WEAPON_1, true);

					if (pk::has_tech(force_, 기교_투석개발))
					{
						pk::message_box(pk::encode(pk::format("城市的投石加了\x1b[1x{}\x1b[0x.", ADD_WEAPON_2)));
						pk::add_weapon_amount(city_, 병기_투석, ADD_WEAPON_2, true);	
						pk::message_box(pk::encode(pk::format("城市的楼船加了\x1b[1x{}\x1b[0x.", ADD_WEAPON_2)));
						pk::add_weapon_amount(city_, 병기_누선, ADD_WEAPON_2, true);
						pk::message_box(pk::encode(pk::format("城市的斗舰加了\x1b[1x{}\x1b[0x.", ADD_WEAPON_2)));
						pk::add_weapon_amount(city_, 병기_투함, ADD_WEAPON_2, true);
					}
					
					if (pk::has_tech(force_, 기교_목수개발)) //技术_木工开发？
					{
						pk::message_box(pk::encode(pk::format("城市的木兽加了\x1b[1x{}\x1b[0x.", ADD_WEAPON_2)));
						pk::add_weapon_amount(city_, 병기_목수, ADD_WEAPON_2, true);	

					} 
					
					if (!pk::has_tech(force_, 기교_투석개발))//기교_투석개발 技术_投资开发？
					{
						pk::message_box(pk::encode(pk::format("城市的井阑加了\x1b[1x{}\x1b[0x.", ADD_WEAPON_2)));
						pk::add_weapon_amount(city_, 병기_정란, ADD_WEAPON_2, true);	
					}
					
					if (!pk::has_tech(force_, 기교_목수개발))
					{
						pk::message_box(pk::encode(pk::format("城市的冲车加了\x1b[1x{}\x1b[0x.", ADD_WEAPON_2)));
						pk::add_weapon_amount(city_, 병기_충차, ADD_WEAPON_2, true);	
					}
				pk::message_box(pk::encode(pk::format("城市中金\x1b[1x{}\x1b[0x。", gold_num)));
				pk::add_gold(city_, gold_num, true);
				pk::add_tp(force_, -TP_COST, force_.get_pos());
				pk::fade(0);
				pk::sleep();
				pk::background(-1);
				pk::fade(255);
			}
			else if(n<52)  //投资商业--不建议使用--在没有达到建筑金钱收益上限有效
			{
				int base_id = building_.get_id();
				//自定义金钱收入随机量
				int add_invest_gold=100+kunshu_.stat[武将能力_政治]*2;//范围300左右吧，资金消耗低点
				int rand_invest_gold=invest_gold-pk::rand(100)-300;//保持400-500资金代价
				pk::fade(0);
				pk::sleep();
				pk::background(4); //先显示背景再亮？
				pk::fade(255);
				pk::message_box(pk::encode("是时候开始促进发展商业了，传令，去将商人找来，我有要事商量!"), kunshu_);
				pk::message_box(pk::encode("是！主公."), officer_);
				pk::message_box(pk::encode("大人！你找我有何要事吩咐？"), merchant_);
				pk::message_box(pk::encode("你来啦！我想花一笔资金投资重建商业。你可有意向？"), kunshu_);
				pk::message_box(pk::encode("大人，小民愿意为大人效力！"), merchant_);
				pk::message_box(pk::encode("嗯,真期待大街繁华的景象..."), kunshu_);
				pk::message_box(pk::encode(pk::format("花了\x1b[1x{}\x1b[0x金投资商业.", rand_invest_gold)));  //花费了多少资金？
				pk::add_gold(city_, -rand_invest_gold, true);
				pk::add_tp(force_, -TP_COST, force_.get_pos()); //消耗了技巧
                pk::message_box(pk::encode(pk::format("城市金钱收入增加了\x1b[1x{}\x1b[0x",add_invest_gold)));
				ch::add_revenue_bonus(base_id,+add_invest_gold); //这里增加金钱收入
				pk::fade(0);
				pk::sleep();
				pk::background(-1); //清除场景
				pk::fade(255);
			}
			
			else if(n<54)  //劝课农桑--不建议使用--在没有达到建筑粮食收益上限有效
			{
				int base_id = building_.get_id();
				int rand_add_people=3000+pk::rand(3000); //提高一点收入
				int rand_troops=cost_troops-1500+pk::rand(1501);//1500-3000,减少士兵数量
				int rand_add_food=cost_food-4000+pk::rand(2001);//增加粮食资源收入(1000,3000)
				pk::fade(0);
				pk::sleep();
				pk::background(场景_宫廷1); //场景背景 
				pk::fade(255);

				pk::message_box(pk::encode("去将负责农耕官员找来，我有政务发布"), kunshu_);
				pk::message_box(pk::encode("是！主公"), officer_);
				pk::message_box(pk::encode("主公召我前来，有何吩咐？"), civilian_);
				pk::message_box(pk::encode("军营中有部分士兵不太合适从军了，我想安排他们务农，你来负责吧，好生对待他们."), kunshu_);
				pk::message_box(pk::encode("是，主公，在下必定处理妥当."), civilian_);
				pk::fade(0);
				pk::background(16); //先显示背景再亮？
				pk::fade(255);
				pk::message_box(pk::encode("主公，属下已经安排好"), civilian_);
				pk::message_box(pk::encode("我们要重视民耕发展，与百姓共同发展."), kunshu_);
				pk::message_box(pk::encode("莫非是主公？拜谢主公为吾等生计安排，吾等草民愿效死力！"), farmer_);
				pk::message_box(pk::encode(pk::format("减少了\x1b[1x{}\x1b[0x士兵人数.",rand_troops)));
				pk::message_box(pk::encode(pk::format("城市花费\x1b[1x{}\x1b[0x金来安置士兵.",rand_add_people/30)));
				pk::message_box(pk::encode(pk::format("城市增加了\x1b[1x{}\x1b[0x人口.",rand_add_people)));  //较为弱点
				pk::add_gold(city_, - rand_add_people/30, true);
				pk::add_troops(city_,-rand_troops, true);
				pk::add_tp(force_, -TP_COST, force_.get_pos());
				ch::add_population(city_.get_id(), +(rand_add_people)); //这里决定增加人口
				pk::message_box(pk::encode(pk::format("城市粮食收入增加了\x1b[1x{}\x1b[0x.",rand_add_food)));
				ch::add_harvest_bonus(base_id, +rand_add_food);
				pk::fade(0);
				pk::sleep();
				pk::background(-1); //清除场景
				pk::fade(255);
			}
			else if(n<62)  //轻徭薄赋
			{
				int rand_add_people=4000+pk::rand(4000); //提高一点收入
			    int rand_troops=cost_troops-2000+pk::rand(2001);//3000以内
				pk::fade(0);
				pk::sleep();
				pk::background(场景_宫廷1); //场景背景 //先显示背景再亮？
				pk::fade(255);

				pk::message_box(pk::encode("连年征战，民不聊生，我欲减轻百姓负担，实行轻薄徭役政策，你下去作安排吧"), kunshu_);
				pk::message_box(pk::encode("主公，有何吩咐？"), civilian_);
				pk::message_box(pk::encode("军营中父子同在，父归，兄弟同在，兄归，有伤残者，不留，不留者给三个月饷，你下去作安排吧."), kunshu_);
				pk::message_box(pk::encode("主公圣明！"), civilian_);
				pk::message_box(pk::encode("民能载舟，亦能覆舟"), kunshu_);
			    pk::message_box(pk::encode("是，主公，在下必定处理妥当。"), civilian_);
				pk::fade(0);
				pk::background(4); //场景背景
				pk::fade(255);
				pk::message_box(pk::encode("各位！大人有吩咐，减轻徭役，大家可以安心在此安居乐业."), officer_);
				pk::message_box(pk::encode("拜谢吾主，草民愿效力."), farmer_);
				
				pk::message_box(pk::encode(pk::format("减少了\x1b[1x{}\x1b[0x士兵人数.",rand_troops)));
				pk::message_box(pk::encode(pk::format("城市花费\x1b[1x{}\x1b[0x金来安置士兵.", rand_add_people/25)));  //消耗跟随人口多少
				pk::message_box(pk::encode(pk::format("城市花费\x1b[1x{}\x1b[0x粮来安置士兵.", rand_add_people/2)));  //消耗跟随人口多少
				pk::message_box(pk::encode(pk::format("人口增加了\x1b[1x{}\x1b[0x人", rand_add_people)));
				pk::add_food(city_, -rand_add_people/2, true);
				pk::add_gold(city_, -rand_add_people/25, true);
				pk::add_troops(city_,-rand_troops, true);
				pk::add_tp(force_, -TP_COST, force_.get_pos());
				pk::add_energy(city_,10, true); //士气
				ch::add_population(city_.get_id(), +(rand_add_people)); //这里决定增加人口（3000,8000）
				pk::fade(0);
				pk::sleep();
				pk::background(-1); //清除场景
				pk::fade(255);
			}
			
			else if(n<70)  //赈济灾民
			{
				int rand_food=pk::rand(5000);  //随机粮食数
				int rand_add_people=4000+pk::rand(4000); //提高一点收入
				int rand_safety=safety_+pk::rand(5);
				pk::fade(0);
				pk::sleep();
				pk::background(场景_宫廷1); //先显示背景再亮？
				pk::fade(255);

				pk::message_box(pk::encode("前段时间为何来了一大批灾民？"), kunshu_);
				pk::message_box(pk::encode("大人，是附近的郡县发生旱灾、战乱，这些难民逃难过来的."), officer_);
				pk::message_box(pk::encode("这么重要的事情怎么不跟我报告？去，开放粮仓，赈济灾民."), kunshu_);
				pk::message_box(pk::encode("民能载舟，亦能覆舟啊！我亲自去赈济."), kunshu_);
				pk::fade(0);
				pk::background(4);
				pk::fade(255);
				pk::message_box(pk::encode("莫非这是大人？竟然亲自前来为我等草民赈放粮食."), farmer_);
				pk::message_box(pk::encode("各位，这世道艰难，我只能做到这些。望各位不要鄙弃我这等心意."), kunshu_);
				pk::message_box(pk::encode("拜谢吾主，我等愿效力."), farmer_);

				pk::message_box(pk::encode(pk::format("城市花费\x1b[1x{}\x1b[0x粮安置难民.", cost_food+rand_food)));
				pk::message_box(pk::encode(pk::format("城市花费\x1b[1x{}\x1b[0x金安置难民.", invest_gold)));
				pk::add_food(city_, -(cost_food+rand_food), true);
				pk::add_gold(city_, -invest_gold, true);
				pk::add_tp(force_, -TP_COST, force_.get_pos());
				pk::message_box(pk::encode(pk::format("安置难民，治安下降了\x1b[1x{}\x1b[0x.", rand_safety)));
				pk::add_public_order(city_, -rand_safety, true); //安置难民减治安
                pk::message_box(pk::encode(pk::format("人口增加了\x1b[1x{}\x1b[0x人",  rand_add_people)));
				ch::add_population(city_.get_id(), +rand_add_people); //这里决定增加人口随机大小（5000,8000）很合理的数字，免得增加人口过多
				pk::fade(0);
				pk::sleep();
				pk::background(-1); //清除场景
				pk::fade(255);
			}
			else if(n<80)  //加点debuff，减金钱
			{
				int rand_gold=300+pk::rand(300);  //随机金钱数--安置难民
				//int rand_add_people=4000+pk::rand(4000); //提高一点收入
				int rand_safety=safety_+pk::rand(5);
				pk::fade(0);
				pk::sleep();
				pk::background(场景_宫廷1); //先显示背景再亮？
				pk::fade(255);
				pk::message_box(pk::encode("前段时间为何来了一大批灾民？"), kunshu_);
				pk::message_box(pk::encode("大人，是附近的郡县发生旱灾、战乱，这些难民逃难过来的."), officer_);
				pk::message_box(pk::encode("这么重要的事情怎么不跟我报告？去，取一些金钱过来安置灾民."), kunshu_);
				pk::message_box(pk::encode("民能载舟，亦能覆舟啊！我亲自去指挥官吏安置灾民."), kunshu_);
				pk::fade(0);
				pk::background(4);
				pk::fade(255);
				pk::message_box(pk::encode("莫非这是大人？竟然亲自前来！."), farmer_);
				pk::message_box(pk::encode("各位，这世道艰难，我只能做到这些。望各位不要鄙弃我这等心意."), kunshu_);
				pk::message_box(pk::encode("拜谢吾主，我等愿效力."), farmer_);
				pk::message_box(pk::encode(pk::format("城市花费\x1b[1x{}\x1b[0x金安置难民.", rand_gold)));
				pk::add_gold(city_, -rand_gold, true);
				pk::add_tp(force_, -TP_COST, force_.get_pos());
				pk::message_box(pk::encode(pk::format("安置难民，治安下降了\x1b[1x{}\x1b[0x.", rand_safety)));
				pk::add_public_order(city_, -rand_safety, true); //安置难民减治安
				pk::fade(0);
				pk::sleep();
				pk::background(-1); //清除场景
				pk::fade(255);
			}
			else if(n<90)  //加点debuff,减兵装
			{
				int building_charge1 = pk::get_weapon_amount(building_,1); //枪
				int building_charge2 = pk::get_weapon_amount(building_,2); //戟
				int building_charge3 = pk::get_weapon_amount(building_,3); //驽
				int RE_WEAPON_1=3000+pk::rand(5000); //减少兵装
				int RE_WEAPON_2=3000+pk::rand(5000); //减少兵装
				int RE_WEAPON_3=3000+pk::rand(5000); //减少兵装
				if(building_charge1<RE_WEAPON_1)
					RE_WEAPON_1=building_charge1;
				if(building_charge2<RE_WEAPON_2)
					RE_WEAPON_2=building_charge2;
				if(building_charge3<RE_WEAPON_3)
					RE_WEAPON_3=building_charge3;
				pk::fade(0);
				pk::sleep();
				pk::background(场景_宫廷1);
				pk::fade(255);
				pk::message_box(pk::encode("近来天下太平，城中囤积太多军备,维持军费昂高."), kunshu_);
				pk::message_box(pk::encode("主公可以向城中民兵发放军备，可使其保卫家乡."), civilian_);
				pk::message_box(pk::encode("传令，将城市中囤积的军备发放给民兵."), kunshu_);
				pk::message_box(pk::encode("遵命!"), civilian_);
				pk::fade(0);
				pk::background(4);
				pk::fade(255);
				pk::message_box(pk::encode("各位，大人有命，给各位发放兵装用于保家卫国."), civilian_);
				pk::message_box(pk::encode("谢主恩赐！我等必辜负大人的期待！"), officer_);
				int safety_city =3+pk::rand(8);
				pk::message_box(pk::encode(pk::format("城市的枪减少了\x1b[1x{}\x1b[0x，戟减少了\x1b[1x{}\x1b[0x，弩减少了\x1b[1x{}\x1b[0x.", RE_WEAPON_1,RE_WEAPON_2,RE_WEAPON_3)));
	            pk::add_weapon_amount(city_, 병기_창, -RE_WEAPON_1, true);
				pk::add_weapon_amount(city_, 병기_극, -RE_WEAPON_2, true);
				pk::add_weapon_amount(city_, 병기_노, -RE_WEAPON_3, true);
				pk::message_box(pk::encode(pk::format("城市中治安上升了\x1b[1x{}\x1b[0x。", safety_city)));
				pk::add_public_order(city_, safety_city, true); 
				pk::add_tp(force_, -TP_COST, force_.get_pos());
				pk::fade(0);
				pk::sleep();
				pk::background(-1);
				pk::fade(255);
			}
			else if(n<100)  //修养生息---相当育民效果
			{
				int add_people=10000+pk::rand(15001);//自定义一个随机人口增加数跟增加人口常量 5000+5000+rand(15000)->max：25000
				int extra_gold=pk::rand(500);//提高点代价，最高2500
				pk::fade(0);
				pk::sleep();
				pk::background(16); //先显示背景再亮？
				pk::fade(255);
				pk::message_box(pk::encode("是时候着手休养生息了，今日开始养精蓄锐"), kunshu_);
				pk::message_box(pk::encode("连年征战，民不聊生，主公务必发动官员开始重筑百姓家园，重耕土地."), officer_);
				pk::message_box(pk::encode("是的！如此方能立足一方."), kunshu_);
				pk::message_box(pk::encode("民能载舟，亦能覆舟"), kunshu_);
				pk::message_box(pk::encode("莫非这是主公？亲自前来拜访我等草民."), farmer_);
				pk::message_box(pk::encode("今日，我已经下令暂停开战，重视民耕发展，与百姓共同发展"), kunshu_);
				pk::message_box(pk::encode("拜谢吾主，草民愿效力."), farmer_);
				pk::message_box(pk::encode(pk::format("收到馈粮\x1b[1x{}\x1b[0x.", food_num)));
				pk::message_box(pk::encode(pk::format("城市花费\x1b[1x{}\x1b[0x金来安置百姓.", gold_num+extra_gold)));
				pk::add_food(city_, food_num, true);
				pk::add_gold(city_, -(gold_num+extra_gold), true);
				pk::add_tp(force_, -TP_COST, force_.get_pos());
				pk::add_public_order(city_, +safety_, true);
                pk::message_box(pk::encode(pk::format("人口增加了\x1b[1x{}\x1b[0x人", add_people)));
				ch::add_population(city_.get_id(), add_people); //这里决定增加人口随机大小（10000,25000）很合理的数字，免得增加人口过多
				pk::fade(0);
				pk::sleep();
				pk::background(-1); //清除场景
				pk::fade(255);
			}
			else
				return;
		}//scene 		
	}//class 
	Main main;
}