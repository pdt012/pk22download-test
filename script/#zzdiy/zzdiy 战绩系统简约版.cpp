 /*
  作者：黑店小小二
  本扩展包完全免费。如需使用，请联系作者本人 ┓( ´∀` )┏
  封装通用方法并记录部队伤害系数
 */

// ## v1.0 2023/08/30 ## 黑店小小二 # 战绩显示放到此文件

namespace 战绩系统简约版 {

  class Main
  {

    Main()
    {
      pk::bind(120, pk::trigger120_t(onUnitInfo));
    }

    void onUnitInfo()
    {
      pk::point cursor_pos = pk::get_cursor_hex_pos();
      if (!pk::is_valid_pos(cursor_pos)) return;

      pk::unit@ unit = pk::get_unit(cursor_pos);
      if (unit is null) return;

      bool is_player = unit.is_player();

      int current = pk::get_elapsed_days();

      string unit_name = pk::decode(pk::get_name(unit));

      string title = pk::format("部队特殊信息(\x1b[1x{}\x1b[0x)", unit_name);

      array<bool> 效果影响列表 = { };

      array<string> 效果内容列表 = { };

      array<string> 展示文案 = { };

      for (int i = 0; i < int(效果影响列表.length); i++)
      {
        if (效果影响列表[i]) 展示文案.insertLast(效果内容列表[i]);
      }

      int len1 = int(展示文案.length);

      pk::list<pk::person@> member = ch::get_member_list(unit);
      sc_personinfo@ sc_person_leader = @person_sc[unit.leader];

      int width = int(pk::get_resolution().width) - 280;
      pk::point left_down = pk::point(int(pk::get_resolution().width) - 10, 75 + len1 * 35);
      pk::draw_filled_rect(pk::rectangle(pk::point(width - 5, 15), left_down), ((0xff / 2) << 24) | 0x010101);
      pk::draw_text(pk::encode(title), pk::point(width, 20), 0xffffffff, FONT_BIG, 0xff000000);

      for (int i = 0; i < int(展示文案.length); i++)
      {
        pk::draw_text(pk::encode(展示文案[i]), pk::point(width, 55 + i * 45), 0xffffffff, FONT_SMALL, 0xff000000);
      }

      if (len1 == 0) pk::draw_text(pk::encode('(无)'), pk::point(width, 65), 0xffffffff, FONT_SMALL, 0xff000000);

      string title2 = "武将特殊信息";

      int person_offset_top = left_down.y;
      pk::point left_down2 = pk::point(int(pk::get_resolution().width) - 10, person_offset_top + 30 + (int(member.count)) * 100) + (int(member.count) == 1 ? 20 : 0);

      pk::draw_filled_rect(pk::rectangle(pk::point(width - 5, person_offset_top), left_down2), ((0xff / 2) << 24) | 0x010101);

      pk::draw_text(pk::encode(title2), pk::point(width, person_offset_top + 20), 0xffffffff, FONT_BIG, 0xff000000);

      person_offset_top += 30;

      for (int i = 0; i < int(member.count); i++)
      {
        int offset_top = person_offset_top + i * 50;
        pk::person@ person = member[i];
        int person_id = person.get_id();
        sc_personinfo@ sc_person = @person_sc[person_id];

        pk::point rightup = pk::point(width + 10, offset_top + (i + 1) * 40);
        pk::point leftdown = pk::point(width + 42, offset_top + 40 + (i + 1) * 40);
        auto rect = pk::rectangle(rightup, leftdown);

        pk::draw_face(FACE_SMALL, person.face, rect, FACE_L);

        string 武将姓名 = pk::format("\x1b[1x{}\x1b[0x", pk::decode(pk::get_name(person)));
        string 武将杀敌数 = pk::format("击溃部队数：\x1b[1x{}\x1b[0x", sc_person.kill_unit);
        string 武将杀兵数 = pk::format("击杀士兵数：\x1b[1x{}\x1b[0x", sc_person.troops_damage);
        string 武将被杀数 = pk::format("被击杀次数：\x1b[1x{}\x1b[0x(部队:\x1b[19x{}\x1b[0x)", sc_person.kill_destroyed, sc_person.kill_unit_destroyed);

        int face_top = offset_top + (i + 1) * 20 + (i * 20);

        pk::draw_text(pk::encode(武将姓名), pk::point(width + 70, face_top + 0), 0xffffffff, FONT_SMALL, 0xff000000);
        pk::draw_text(pk::encode(武将杀敌数), pk::point(width + 70, face_top + 20), 0xffffffff, FONT_SMALL, 0xff000000);
        pk::draw_text(pk::encode(武将杀兵数), pk::point(width + 70, face_top + 40), 0xffffffff, FONT_SMALL, 0xff000000);
        pk::draw_text(pk::encode(武将被杀数), pk::point(width + 70, face_top + 60), 0xffffffff, FONT_SMALL, 0xff000000);
      }

    }
  }
  Main main;
}