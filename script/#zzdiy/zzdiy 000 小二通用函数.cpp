 /*
  作者：黑店小小二
  本扩展包完全免费。如需使用，请联系作者本人 ┓( ´∀` )┏
  封装通用方法并记录部队伤害系数
  注：结构体都为敏感数据，最好不要有修改
 */
// ## v6.3 2023/08/30 ## 黑店小小二 # 剔除功绩设定以及无效设定
// ## v6.2 2023/01/02 ## 黑店小小二 # 修复重启游戏数据丢失bug
// ## v6.1 2022/12/23 ## 黑店小小二 # 杀敌新增功绩，比例20:1
// ## v6.0 2022/11/02 ## 黑店小小二 # 梳理特技系统
// ## v5.0 2022/10/22 ## 黑店小小二 # 梳理功绩系统
// ## v4.0 2022/09/22 ## 黑店小小二 # 拆分神术相关函数跟战绩系统，重命名为小二通用函数（xe）
// ## v3.2 2022/09/12 ## 黑店小小二 # 统一兵力扣减计算
// ## v3.1 2022/06/07 ## 黑店小小二 # 调整结构体数据，节约空间
// ## v3.0 2022/06/27 ## 黑店小小二 # 重新规划部队伤害系数
// ## v2.0 2022/05/03 ## 黑店小小二 # 修复自定义武将头像不正确bug.
// ## v1.0 2022/04/16 ## 黑店小小二 # 完成初版

namespace xe {
  const int KEY = pk::hash("小二");
  const int KEY_索引_追加_武将起始 = 0;

  array<string> temp_list = { pk::encode("农民兵"), pk::encode("突袭者"), pk::encode("府兵"), pk::encode("雇佣兵"), pk::encode("贾诩小队") };

  void set_person_death(pk::person@ person, int num)
  {
    person.death += num;
    if (person.death - person.birth <= 0)
    {
      person.estimated_death = true;
      pk::unit@ unit = pk::get_unit(pk::hex_object_id_to_unit_id(person.location));
      if (unit is null) pk::kill(unit);
      pk::wait(1000);
      pk::kill(person);
    }
  }

  void unit_done(pk::unit@ unit)
  {
    unit.action_done = true;
    if (int(pk::option["San11Option.EnableInfiniteAction"]) != 0)
      unit.action_done = false;
  }

  void set_unit_damage(pk::unit@ attacker, int troops_damage, int type = 0)
  {
    troops_damage = troops_damage > 0 ? troops_damage : (troops_damage * -1);
    int member_count = pk::get_member_count(attacker);
    // 0 - 攻击伤害 1-计略伤害 2-陷阱伤害
    /*
      伤害计算分摊：
        1人： 1
        2人：0.7 : 0.3
        3人：0.5 : 0.25 : 0.25
    */
    for (int i = 0; i < member_count; i++)
    {
      if (pk::is_valid_person_id(attacker.member[i]))
      {
        pk::person@ person = pk::get_person(attacker.member[i]);
        if (temp_list.find(person.name_read) < 0)
        {
          sc_personinfo@ sc_person = @person_sc[attacker.member[i]];
          if (type == 0)
          {
            if (i == 0)
            {
              if (member_count == 1)
              {
                sc_person.troops_damage += troops_damage;
                pk::add_kouseki(person, int(troops_damage / 20));
              }
              if (member_count == 2)
              {
                sc_person.troops_damage += int(0.7 * troops_damage);
                pk::add_kouseki(person, int(0.7 * troops_damage / 20));
              }
              if (member_count == 3)
              {
                sc_person.troops_damage += int(0.5 * troops_damage);
                pk::add_kouseki(person, int(0.5 * troops_damage / 20));
              }
            }
            else
            {
              if (member_count == 2)
              {
                sc_person.troops_damage += int(0.3 * troops_damage);
                pk::add_kouseki(person, int(0.3 * troops_damage / 20));
              }
              if (member_count == 3)
              {
                sc_person.troops_damage += int(0.25 * troops_damage);
                pk::add_kouseki(person, int(0.25 * troops_damage / 20));
              }
            }
          }
          else
          {
            sc_person.troops_damage += int(troops_damage / pk::get_member_count(attacker));
            pk::add_kouseki(person, int(troops_damage / pk::get_member_count(attacker) / 20));
          }
        }
      }
    }
  }

  void set_unit_kill(pk::unit@ attacker, int count = 1)
  {
    for (int i = 0; i < 3; i++)
    {
      if (pk::is_valid_person_id(attacker.member[i]))
      {
        sc_personinfo@ sc_person = @person_sc[attacker.member[i]];
        sc_person.kill_unit += count;
      }
    }
  }

  void add_troops(pk::unit@ target, int value, int type = 14, pk::unit@ unit = null)
  {
    int infact_value = 0;
    int unit_troops = target.troops;
    if (value < 0)
    {
      infact_value = unit_troops > (-1 * value) ? value : (unit_troops * -1);
      set_unit_damage(unit, infact_value);
    }
    ch::add_troops(target, value, true, type);
    if (target is null or target.troops == 0)
    {
      pk::create_effect(0x3a, target.get_pos());
      pk::kill(target);
      set_unit_kill(unit);
      pk::add_stat_exp(unit, 武将能力_统率, 3);
    }
  }

  bool is_new_face(int person_id)
  {
    int face_id = pk::get_person(person_id).face;
    if (face_id >= 1300 and face_id <= 1373) return true;
    else if (face_id >= 1400 and face_id <= 1485) return true;
    else if (face_id >= 1600 and face_id <= 1632) return true;
    else if (face_id >= 1700 and face_id <= 1732) return true;
    else return false;
  }

  bool checkoutName(string person_name, string checkout_name)
  {
    return person_name == pk::encode(checkout_name) or pk::decode(person_name) == checkout_name;
  }

  class Main
  {
    pk::func209_t@ prev_callback_209;
    pk::func212_t@ prev_callback_212;
    pk::func213_t@ prev_callback_213;
    pk::func214_t@ prev_callback_214;
    pk::func215_t@ prev_callback_215;

    Main()
    {
      pk::bind(102, 10, pk::trigger102_t(onLoad));
      pk::bind(105, pk::trigger105_t(onSave));

      pk::bind(151, 999, pk::trigger151_t(onPersonDied));
      pk::bind(171, 999, pk::trigger171_t(onUnitRemove));
      pk::bind(175, 999, pk::trigger175_t(onUnitDestory));

      @prev_callback_209 = cast<pk::func209_t@>(pk::get_func(209));
      pk::reset_func(209);
      pk::set_func(209, pk::func209_t(func209));

      @prev_callback_212 = cast<pk::func212_t@>(pk::get_func(212));
      pk::reset_func(212);
      pk::set_func(212, pk::func212_t(func212));

      @prev_callback_213 = cast<pk::func213_t@>(pk::get_func(213));
      pk::reset_func(213);
      pk::set_func(213, pk::func213_t(func213));

      @prev_callback_214 = cast<pk::func214_t@>(pk::get_func(214));
      pk::reset_func(214);
      pk::set_func(214, pk::func214_t(func214));

      @prev_callback_215 = cast<pk::func215_t@>(pk::get_func(215));
      pk::reset_func(215);
      pk::set_func(215, pk::func215_t(func215));
    }

    void onLoad()
    {
      //重新开始游戏时，初始化数据
      if (!pk::get_scenario().loaded)
      {
        for (int i = 0; i < 武将_末; i += 1)
        {
          person_sc[i].kill_unit = 0;
          person_sc[i].troops_damage = 0;
          person_sc[i].kill_destroyed = 0;
          person_sc[i].kill_unit_destroyed = 0;
        }
      }
      if (pk::get_scenario().loaded)
      {
        for (int i = 0; i < 武将_末; i++)
        {
          for (int j = 0; j <= (通用武将结构体_uint32数 - 1); j++)
          {
            sc_person_info_temp[j][i] = uint32(pk::load(KEY, (KEY_索引_追加_武将起始 + (i * 通用武将结构体_uint32数 + j)), 0));
          }
          sc_personinfo person_t(i);
          person_sc[i] = person_t;
        }
      }
    }

    void onSave(int file_id)
    {
      for (int i = 0; i < 武将_末; i++)
      {
        person_sc[i].update(i);
        for (int j = 0; j <= (通用武将结构体_uint32数 - 1); j++)
          pk::store(KEY, (KEY_索引_追加_武将起始 + (i * 通用武将结构体_uint32数 + j)), sc_person_info_temp[j][i]);
      }
    }

    void onPersonDied(pk::person@ dead, pk::person@ by, pk::hex_object@ where, pk::person@ succ, int type, int rettype)
    {
      sc_personinfo@ sc_person = @person_sc[dead.get_id()];
      sc_person.kill_unit = 0;
      sc_person.troops_damage = 0;
      sc_person.kill_destroyed = 0;
      sc_person.kill_unit_destroyed = 0;
    }

    void onUnitRemove(pk::unit@ unit, pk::hex_object@ dst, int type)
    {
      // 被消灭部队 伤害来源 类型
      pk::unit@ dst_unit = pk::hex_object_to_unit(dst);
      for (int i = 0; i < 3; i++)
      {
        if (pk::is_valid_person_id(unit.member[i]))
        {
          pk::person@ member = pk::get_person(unit.member[i]);
          sc_personinfo@ sc_person = @person_sc[unit.member[i]];
          if (type == 0)
          {
            if (temp_list.find(member.name_read) >= 0)
            {
              sc_person.kill_unit = 0;
              sc_person.troops_damage = 0;
              sc_person.kill_destroyed = 0;
              sc_person.kill_unit_destroyed = 0;
            }
            else if (type == 1)
            {
              sc_person.kill_destroyed += 1;
              if (dst_unit !is null) sc_person.kill_unit_destroyed += 1;
            }
          }
        }
      }
    }

    void onUnitDestory(pk::unit@ attacker, pk::unit@ target)
    {
      for (int i = 0; i < 3; i++)
      {
        if (pk::is_valid_person_id(attacker.member[i]))
        {
          sc_personinfo@ sc_person = @person_sc[attacker.member[i]];
          sc_person.kill_unit += 1;
        }
      }
    }

    void func209(pk::damage_info& info, pk::unit@ attacker, int tactics_id, const pk::point& in target_pos, int type, int critical, bool ambush, int rettype)
    {
      prev_callback_209(info, attacker, tactics_id, target_pos, type, critical, ambush, rettype);
      if (rettype != 15 and attacker !is null)
      {
        set_unit_damage(attacker, info.troops_damage);
      }
    }

    void func212(pk::damage_info& info, pk::unit@ attacker, pk::hex_object@ sub_target)
    {
      prev_callback_212(info, attacker, sub_target);
      if (attacker !is null) set_unit_damage(attacker, info.troops_damage);
    }

    void func213(pk::damage_info& info, int trap, pk::unit@ attacker, pk::hex_object@ target, bool critical)
    {
      prev_callback_213(info, trap, attacker, target, critical);
      if (attacker !is null) set_unit_damage(attacker, info.troops_damage, 2);
    }

    void func214(pk::damage_info& info, pk::unit@ attacker, pk::hex_object@ target, bool critical)
    {
      prev_callback_214(info, attacker, target, critical);
      if (attacker !is null) set_unit_damage(attacker, info.troops_damage, 1);
    }

    void func215(pk::damage_info& info, pk::unit@ attacker, pk::hex_object@ target, bool critical)
    {
      prev_callback_215(info, attacker, target, critical);
      if (attacker !is null) set_unit_damage(attacker, info.troops_damage, 1);
    }
  }
  Main main;
}


const int 通用武将结构体_uint32数 = 4;

array<array<uint32>> sc_person_info_temp(通用武将结构体_uint32数, array<uint32>(武将_末, uint32(0)));

array<sc_personinfo> person_sc(武将_末);

class sc_personinfo {
  uint16 person_id_t;

  int32 kill_unit = 0; // 击破部队数
  uint32 troops_damage = 0; // 士兵伤害数
  int32 kill_destroyed = 0; // 被击溃总次数
  int32 kill_unit_destroyed = 0; // 被部队溃灭次数

  //初始化
  sc_personinfo(int person_id)
  {
    person_id_t = person_id;
    get_info(person_id);
  }

  //下面这种写法用于定义全局变量，先声明，然后用get_info获取信息后使用
  sc_personinfo() {}

  void get_info(int person_id)
  {
    fromInt32_0(sc_person_info_temp[0][person_id]);
    fromInt32_1(sc_person_info_temp[1][person_id]);
    fromInt32_2(sc_person_info_temp[2][person_id]);
    fromInt32_3(sc_person_info_temp[3][person_id]);
  }

  void update(int person_id)
  {
    sc_person_info_temp[0][person_id] = toInt32_0();
    sc_person_info_temp[1][person_id] = toInt32_1();
    sc_person_info_temp[2][person_id] = toInt32_2();
    sc_person_info_temp[3][person_id] = toInt32_3();
  }

  void update_data(int type, int value, bool reset = false)
  {
    switch (type)
    {
      case 0:
        kill_unit = reset ? 0 : (kill_unit + value);
        break;
      case 1:
        troops_damage = reset ? 0 : (troops_damage + value);
        break;
      case 2:
        kill_destroyed = reset ? 0 : (kill_destroyed + value);
        break;
      case 3:
        kill_unit_destroyed = reset ? 0 : (kill_unit_destroyed + value);
        break;
    }
  }

  uint32 toInt32_0(void)
  {
    uint32 kill_unit_value = kill_unit;
    uint32 x = kill_unit_value;
    return x;
  }

  uint32 toInt32_1(void)
  {
    //if (person_id_t == 武将_关羽) pk::info('troops_dramge:' + troops_damage);
    uint32 troops_damage_value = troops_damage;
    uint32 x = troops_damage_value;
    return x;
  }

  uint32 toInt32_2(void)
  {
    uint32 kill_destroyed_value = kill_destroyed;
    uint32 x = kill_destroyed_value;
    return x;
  }

  uint32 toInt32_3(void)
  {
    uint32 kill_unit_destroyed_value = kill_unit_destroyed;
    uint32 x = kill_unit_destroyed_value;
    return x;
  }

  void fromInt32_0(uint32 x)
  {
    kill_unit = ((x << 0) >> 0);
  }

  void fromInt32_1(uint32 x)
  {
    //if (person_id_t == 武将_关羽) pk::info('troops_dramge:' + x);
    troops_damage = ((x << 0) >> 0);
  }

  void fromInt32_2(uint32 x)
  {
    kill_destroyed = ((x << 0) >> 0);
  }

  void fromInt32_3(uint32 x)
  {
    kill_unit_destroyed = ((x << 0) >> 0);
  }
}
