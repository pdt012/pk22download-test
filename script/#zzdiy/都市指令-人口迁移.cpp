/***CUSTOM
@name 人口迁移
@class 人口迁移
@author 广汉郑子瑾
@version 1.0
@QQ：2021483850（本人） #有bug,请找兔神修复
@date 2022/08/03 22:43
内容： （就是实现人口迁移）
机制：
	根据人口迁移：可选择迁移人口规模（最高10万）
	根据移动的城市达到的人口会有损耗（每回合：5-10%）
	
### 基于外交指令--支援兵力构思

###12.26 ###黑店小小二 ### 征的广汉大佬同意后，结合兔神的平衡建议进行调整
###8.22 ###新增移民剧情
###8.22 ###暴政移民实现,新增物资转移，新增焚城效果，新增武将负面影响
***/

namespace 人口迁移
{
	//-----------------------
	const int GOLD_COST=1500;
	const int ACTION_COST=70;
	const int TP_COST=300;
	const int MAX_POPULATION=250000;
	const int MIN_POPULATION=150000;
	//-----------------------
	
	class Main
	{
		pk::force@ force_;  //敌我势力
		pk::district@ district_;  //军团
		pk::building@ menu_base, selected_base;//建筑
		pk::city@ city_,selected_city_;
		pk::person@ kunshu_,gunshi_,taishu_;  //我方君主,军师
		pk::random random(pk::rand());
		array<int> level_populations = { MIN_POPULATION, 一级城市阈值, 二级城市阈值, 三级城市阈值, 四级城市阈值 };
		
		Main()
		{
			add_menu();
		}
		
		void add_menu()
		 {	
			pk::menu_item 人口_迁移;
			人口_迁移.menu = 100;  //城市
			人口_迁移.shortcut = "M";
			人口_迁移.init = pk::building_menu_item_init_t(init);
			人口_迁移.is_visible = pk::menu_item_is_visible_t(isVisible);
			人口_迁移.is_enabled = pk::menu_item_is_enabled_t(isEnabled);
			人口_迁移.get_text = pk::menu_item_get_text_t(getText);
			人口_迁移.get_desc = pk::menu_item_get_desc_t(getDesc);
			人口_迁移.handler = pk::menu_item_handler_t(handler);
			pk::add_menu_item(人口_迁移);	
		 }
		 
		 //---init---
		 void init(pk::building@ building)
		{
			@force_ = pk::get_force(building.get_force_id());
			@district_ = pk::get_district(building.get_district_id());
			@menu_base = @building;
			@taishu_ = pk::get_person(pk::get_taishu_id(building));
			@kunshu_ = pk::get_person(pk::get_kunshu_id(building));
			@gunshi_ = pk::get_person(force_.gunshi) !is null?pk::get_person(force_.gunshi):pk::get_person(武将_文官); //获取军师
			@city_ = pk::building_to_city(menu_base);
		}
		
		//---isVisible---
		bool isVisible()
		{
			BaseInfo@ base_t = @base_ex[menu_base.get_id()]; //获取单独一个
			// if (base_t.population < MIN_POPULATION) // 去掉这个比较好，可以让玩家知道这个功能
			// 	return false;
			if (force_.get_id() != pk::get_current_turn_force_id())
				return false;
			return true;
		}
		//---isEnabled---
		bool isEnabled()
		{
			BaseInfo@ base_t = @base_ex[menu_base.get_id()]; //获取单独一个
			
			if(pk::get_city_list(force_).count<2)
				return false;
			
			if (!pk::is_alive(taishu_))
				return false;
			
			if (taishu_.action_done)
				return false;
			
			if (kunshu_.action_done)
				return false;
			
			if (gunshi_.action_done)
				return false;
			
			if (pk::is_absent(taishu_) or pk::is_absent(kunshu_) or pk::is_absent(gunshi_)) 
				return false; 
			
			if (pk::is_unitize(taishu_) or pk::is_unitize(kunshu_) or pk::is_unitize(gunshi_))  
				return false;  //去掉出征的
			
			if (int(district_.ap) < ACTION_COST)
				return false;

			if (int(force_.tp) < TP_COST)
				return false;

			if (pk::get_gold(menu_base) < GOLD_COST)
				return false;

			if (pk::enemies_around(menu_base))
				return false;
			
			if (base_t.population < MIN_POPULATION)
				return false;
			
			return true;
		}
		//---getText---
		string getText()
		{
			return pk::encode("迁民");
		}
		//---getDesc---
		string getDesc()
		{
			BaseInfo@ base_t = @base_ex[menu_base.get_id()]; //获取单独一个
			
			if(pk::get_city_list(force_).count<2)
				return pk::encode("势力单城无法迁民.");
			
			if (!pk::is_alive(taishu_))
				return pk::encode("据点内无太守.");

			if (taishu_.action_done)
				return pk::encode("据点内的太守已经完成行动.");
			
			if (kunshu_.action_done)
				return pk::encode("君主已经完成行动.");
			
			if (gunshi_.action_done)
				return pk::encode("军师已经完成行动.");
			
			if (pk::is_absent(taishu_) or pk::is_absent(kunshu_) or pk::is_absent(gunshi_)) 
				return  pk::encode("君主、军师、太守需要闲空状态."); 
			
			if (pk::is_unitize(taishu_) or pk::is_unitize(kunshu_) or pk::is_unitize(gunshi_))  
				return pk::encode("君主、军师、太守不得出征.");   //去掉出征的

			if (int(district_.ap) < ACTION_COST)
				return pk::encode("行动力不足.（需70行动力）");

			if (int(force_.tp) < TP_COST)
				return pk::encode("技巧点不足.（需300技巧）");

			if (pk::get_gold(menu_base) < GOLD_COST)
				return pk::encode("金钱不足.（需1500金钱）");

			if (pk::enemies_around(menu_base))
				return pk::encode("据点周边存在敌军部队.");
			
			if (base_t.population< MIN_POPULATION)
				return pk::encode("据点人口低于最低要求(150000).");

			return pk::encode(pk::format("自势力据点人口迁移到同势力其他据点.(金钱:{},技巧点:{},行动力:{})",GOLD_COST,TP_COST,ACTION_COST));
		}
		//---handler---
		bool handler()
		{
			pk::list<pk::building@> selectable_base_list = get_selectable_base_list();
			if (int(selectable_base_list.count) == 0) pk::message_box(pk::encode("没有可以迁移人口的据点目标."));
			else
			{
				array<string> select_list =
				{
					pk::encode("抚民迁移"), // 0
					pk::encode("暴政迁移"),  //1
					pk::encode("取消")  //2
				};
				int select_command = pk::choose(pk::encode("请选择要实施的迁民方式."), select_list);
				if(select_command == 0)
				{
					pk::scene(pk::scene_t(scene_Event1));
				}
				else if(select_command ==1)
				{
					pk::scene(pk::scene_t(scene_Event2));
					暴政负面影响();
				}
				else
					return true;
			}
			
			//结束添加行动后的状态
			return true;
		}
		
		void scene_Event1()//简单实现抚民迁移
		{
				//实现功能--迁移人口
				string selector_title = pk::encode("选择据点");
				string selector_desc = pk::encode("请选择人口迁移的其他据点.");
				array<pk::building@> selected_building_arr = pk::list_to_array(pk::building_selector(selector_title, selector_desc, selectable_base_list, 1, 1));
				if (int(selected_building_arr.length) > 0)
				{
					BaseInfo@ menu_base_t = @base_ex[menu_base.get_id()]; //迁移据点
					BaseInfo@ selected_base_t = @base_ex[selected_building_arr[0].get_id()]; //目标据点
					string numberpad_title = pk::encode("迁民规模");

					int level = ch::get_city_level(menu_base.get_id());
					int level_max_population = menu_base_t.population - level_populations[level];
					int support_limit = pk::min(250000, level_max_population);
					// int support_limit = pk::min(selected_base_t.population, MAX_POPULATION);
					// pk::int_bool numberpad_value = pk::numberpad(numberpad_title, MIN_POPULATION, support_limit, MIN_POPULATION, pk::numberpad_t(pad_callback));
					pk::int_bool numberpad_value = pk::numberpad(numberpad_title, 1, support_limit, MIN_POPULATION, pk::numberpad_t(pad_callback));
					if (numberpad_value.second)
					{
						@selected_base = selected_building_arr[0];
						//@selected_force = pk::get_force(selected_base.get_force_id());
						string selected_base_name = "\x1b[1x" + pk::decode(pk::get_name(selected_base)) + "\x1b[0x";  //被选择的建筑
						string force_name = "\x1b[2x" + pk::decode(pk::get_name(force_)) + "军\x1b[0x";
						string base_name =  "\x1b[1x" + pk::decode(pk::get_name(menu_base)) + "\x1b[0x"; 
						int support_amount = numberpad_value.first;
						string confirm_desc = "是否从城市"+ base_name + "向城市" + selected_base_name + "迁移人口?\n(人口迁移规模:\x1b[1x" + support_amount + "\x1b[0x)";
						int confirm_value = pk::choose(pk::encode(confirm_desc), { pk::encode(" 确定 "), pk::encode(" 取消 ") });
						if (confirm_value == 0)  //是否迁移人口
						{
							//添加剧情：安抚移民剧情
							scene_安抚移民(selected_base_name);
							//以后在说
							ch::add_population(menu_base.get_id(), -support_amount);
							// menu_base_t.population-=support_amount;
							//获取距离
							int city_distance = get_city_distance(menu_base, selected_base);  //移动回合
							//计算损耗
							float de_support_amount = support_amount * ((100-(random(5,10) * city_distance))/100.f) - pk::rand(1000);
							//增加减少
							// selected_base_t.population+=de_support_amount;
							ch::add_population(selected_building_arr[0].get_id(), int(de_support_amount));
							pk::history_log(selected_base.get_pos(), force_.color, pk::encode(pk::format("{}从{}迁移\x1b[1x{}\x1b[0x人到{},因路程跋涉艰难损失了\x1b[29x{}\x1b[0x人",force_name,base_name,support_amount,selected_base_name
							,int(support_amount-de_support_amount))));
							pk::say(pk::encode(pk::format("与民同在,此处已不可久留,急需迁民到\x1b[2x{}\x1b[0x",selected_base_name)), taishu_ , menu_base);
							auto district = pk::get_district(menu_base.get_district_id());
							pk::add_ap(district, -ACTION_COST);
							ch::add_tp(pk::get_force(menu_base.get_force_id()), -TP_COST, menu_base.get_pos());
							//pk::add_gold(menu_base,-GOLD_COST,true);
							kunshu_.action_done = true;
							gunshi_.action_done = true;
							taishu_.action_done = true;
							kunshu_.update();
							gunshi_.update();
							taishu_.update();
							menu_base.update();
						}
					}
				}
		}
		
		void scene_Event2()//暴政移民
		{
				//实现功能--迁移人口
				string selector_title = pk::encode("选择据点");
				string selector_desc = pk::encode("请选择人口迁移的其他据点.");
				array<pk::building@> selected_building_arr = pk::list_to_array(pk::building_selector(selector_title, selector_desc, selectable_base_list, 1, 1));
				if (int(selected_building_arr.length) > 0)
				{
					BaseInfo@ menu_base_t = @base_ex[menu_base.get_id()]; //迁移据点
					BaseInfo@ selected_base_t = @base_ex[selected_building_arr[0].get_id()]; //目标据点
					@selected_base = selected_building_arr[0];
					
					string selected_base_name = "\x1b[1x" + pk::decode(pk::get_name(selected_base)) + "\x1b[0x";  //被选择的建筑
					@selected_city_ = pk::building_to_city(selected_base);  //设置选择该城市的物品
					string force_name = "\x1b[2x" + pk::decode(pk::get_name(force_)) + "军\x1b[0x";
					string base_name =  "\x1b[1x" + pk::decode(pk::get_name(menu_base)) + "\x1b[0x"; 
					//添加剧情：安抚移民剧情
					scene_暴政移民(selected_base_name,base_name);
					//-----------------------
					int support_amount = int(menu_base_t.population * 0.9);  //所能移民最高的人口数量9成
					if (menu_base_t.population - support_amount < 5000)  //剩余人数少于5000
					{
						ch::add_population(menu_base.get_id(), (5000 + pk::rand(500) - menu_base_t.population));
						// menu_base_t.population = 5000+pk::rand(500);
					}
					else  //多余5000人暂时不管
					{
						ch::add_population(menu_base.get_id(), (5000 + pk::rand(500) - menu_base_t.population));
						// menu_base_t.population = menu_base_t.population - support_amount;
					}
					//获取距离
					int city_distance = get_city_distance(menu_base, selected_base);  //移动回合
					//计算损耗
					float de_support_amount=support_amount*((100-(random(5,10)*city_distance))/100.f);//去掉一部分距离损失
					de_support_amount = de_support_amount/3 + de_support_amount/4 + de_support_amount/5 + de_support_amount/6;
					//增加减少
					// selected_base_t.population+=de_support_amount;
					ch::add_population(selected_building_arr[0].get_id(), int(de_support_amount));
					pk::history_log(selected_base.get_pos(), force_.color, pk::encode(pk::format("{}从{}迁移\x1b[1x{}\x1b[0x人到{},因路程跋涉艰难损失了\x1b[29x{}\x1b[0x人",force_name,base_name,support_amount,selected_base_name
					,int(support_amount-de_support_amount))));
					//pk::say(pk::encode('哈哈哈!全部给我烧掉!'), pk::get_person(city_.get_taishu_id()));
					pk::say(pk::encode("哈哈哈!全部给我烧掉!"), kunshu_ , menu_base);
					auto district = pk::get_district(menu_base.get_district_id());
					pk::add_ap(district, -ACTION_COST);
					ch::add_tp(pk::get_force(menu_base.get_force_id()), -TP_COST, menu_base.get_pos());
					pk::add_gold(menu_base,-GOLD_COST,true);
					kunshu_.action_done = true;
					gunshi_.action_done = true;
					taishu_.action_done = true;
					kunshu_.update();
					gunshi_.update();
					taishu_.update();
				}
		}
		
		
		//---get_selectable_base_list---
		pk::list<pk::building@> get_selectable_base_list()   //选择相邻3个回合的据点-pk：building
		{
			pk::list<pk::building@> base_list;
			for (int base_id = 0; base_id < 据点_城市末; base_id++)
			{
				pk::building@ base = pk::get_building(base_id);
				if (menu_base.get_force_id() != base.get_force_id()) continue; //同势力跳出
				if (base.get_id() == menu_base.get_id()) continue;   //同一势力据点才行
				if (int(pk::get_idle_person_list(base).count) == 0) continue;  //没有可用的武将去掉
				if (pk::enemies_around(base)) continue;  //判断周边是否有敌人
				int city_distance = get_city_distance(menu_base, base);
				if (city_distance > 2) continue; //超过两回合回合的城市去掉
				base_list.add(base);
			}
			return base_list;
		}
		
		//---get_city_distance---
		int get_city_distance(pk::building@ base_1, pk::building@ base_2)  //获取据点之间的距离
		{
			int city_1_id = (base_1.get_id() < 据点_城市末) ? base_1.get_id() : pk::get_city_id(base_1.pos);
			int city_2_id = (base_2.get_id() < 据点_城市末) ? base_2.get_id() : pk::get_city_id(base_2.pos);
			return pk::get_city_distance(city_1_id, city_2_id);
		}
		
		string pad_callback(int line, int original_value, int current_value) { return ""; }
		
		//安抚移民剧情
		void scene_安抚移民(string sel_base_name)
		{
			pk::play_bgm(19);//平缓缓和
			pk::fade(0);
			pk::sleep();
			pk::background(场景_宫廷1);
			pk::play_bgm(19);//平缓缓和
			pk::fade(255);
			pk::message_box(pk::encode(pk::format("主公！\x1b[1x{}\x1b[0x土地有待开垦，商贸往来有待加强,现急需迁入一些人口." ,sel_base_name)),gunshi_);
			pk::message_box(pk::encode("经济发展乃我军重要方针，现实施经济性迁移居民，迁移非小事也，尔等应尽心为民"), kunshu_);
			pk::message_box(pk::encode("吾等必定尽心竭力，不会让百姓受欺负。" ), pk::get_person(武将_文官));
			pk::fade(0);
			pk::background(场景_市场);
			pk::fade(255);
			pk::cutin(CG_脚步);
			pk::message_box(pk::encode("张榜在此，大家快来看......" ), pk::get_person(武将_卫兵));
			pk::message_box(pk::encode("什么事情啊大人，我等文化不高，可否解释一下" ), pk::get_person(武将_农民));
			pk::message_box(pk::encode(pk::format("报名者迁入\x1b[1x{}\x1b[0x新地可免除10年赋税且有钱粮补贴......" ,sel_base_name)), pk::get_person(武将_卫兵));
			pk::message_box(pk::encode(pk::format("大家可以报名去\x1b[1x{}\x1b[0x新地发挥自己的劳动创收吧！",sel_base_name)), pk::get_person(武将_卫兵));
			pk::play_bgm(10);//高昂
			pk::message_box(pk::encode("看起来能让家人过得更好呢~" ), pk::get_person(武将_商人));
			pk::message_box(pk::encode("我还有一股子力气，也能出一份力~" ), pk::get_person(武将_农民));
			pk::message_box(pk::encode(pk::format("妈妈，妈妈，\x1b[1x{}\x1b[0x还能吃到糖葫芦吗~" ,sel_base_name)), pk::get_person(武将_童女));
			int building_gold=pk::get_gold(menu_base);  //建筑金钱
			int building_food=pk::get_food(menu_base); //建筑粮草
			pk::add_gold(menu_base,-int(building_gold*0.25),true);
			pk::add_food(menu_base,-int(building_food*0.25),true);
			menu_base.update();
			pk::fade(0);
			pk::background(-1);
			pk::fade(255);
		}
		
		//暴政迁民
		void scene_暴政移民(string sel_base_name,string self_base_name)
		{
			pk::play_bgm(19);//平缓缓和
			pk::fade(0);
			pk::sleep();
			pk::background(场景_自室);
			pk::play_bgm(23);//平缓缓和
			pk::fade(255);
			pk::cutin(CG_脚步);
			pk::message_box(pk::encode(pk::format("主公！吾观\x1b[1x{}\x1b[0x地狭人贫，治则无良田牲畜，守则无高墙石垒." ,self_base_name)),gunshi_);
			pk::cutin(CG_眼神);
			pk::message_box(pk::encode(pk::format("吾以为当下四面具敌，应战略放弃此城，迁百姓于\x1b[1x{}\x1b[0x，据守坚城矣!",sel_base_name)), gunshi_);
			pk::message_box(pk::encode("此计令我茅塞顿开，不愧是我的军师啊！" ), kunshu_);
			pk::message_box(pk::encode("现下令执行此策，速度开展！"), kunshu_);
			pk::fade(0);
			pk::background(场景_宫廷1);
			pk::fade(255);
			pk::play_bgm(12);
			pk::message_box(pk::encode("该地城防羸弱，难以御敌......" ), kunshu_);
			pk::message_box(pk::encode(pk::format("今日颁布诏令，全城居民携百官移居至\x1b[1x{}\x1b[0x，违令者以军法从事！",sel_base_name)),kunshu_);
			pk::message_box(pk::encode("主公，望三思啊！此项举措定会步骑驱蹙，更相蹈藉，饥饿寇掠，积尸盈路！"), pk::get_person(武将_文官));
			pk::message_box(pk::encode("此事已有定论，诸位无需议论，立即执行！"), kunshu_);
			pk::fade(0);
			pk::background(场景_市场);
			pk::fade(255);
			pk::play_bgm(10);//高昂
			pk::message_box(pk::encode("......路途遥远，颠沛流离，吾等遭受苦难啊~" ), pk::get_person(武将_农民));
			pk::message_box(pk::encode("我年事已高，何故遭此罪矣！" ), pk::get_person(武将_老人));
			pk::message_box(pk::encode("哎，寄人篱下，吾等也无其他方法！"), pk::get_person(武将_町娘));
			pk::message_box(pk::encode("妈妈~呜哇，呜哇，呜哇~"), pk::get_person(武将_童女));
			pk::fade(0);
			pk::sleep();
			pk::background(2);
			pk::fade(255);
			pk::message_box(pk::encode("这个城镇已是毫无价值的地方了。"), kunshu_);
			pk::message_box(pk::encode("与其给敌人，不如把它全部烧掉。"), kunshu_);
			pk::message_box(pk::encode("主公，那样会招怨百姓的.."), pk::get_person(武将_文官));
			pk::message_box(pk::encode("哼!那些鼠目寸光的家伙怎么能懂国家大计呢?!"), kunshu_);
			pk::message_box(pk::encode("但是..."), pk::get_person(武将_文官));
			pk::message_box(pk::encode("闭嘴!再阻挠一下，我必先斩断你的脖子!"), kunshu_);
			pk::message_box(pk::encode("..."), pk::get_person(武将_文官));
			//pk::background(21);
			//pk::background("mediaEX/background/huoshaoluoyang2.png", 1);
			pk::message_box(pk::encode("哈哈哈!全部给我烧掉!"), kunshu_);
			//int city_id = city_.get_id();
			//int pre_pop = base_ex[city_id].population;
			//ch::add_population(city_id, int(-pre_pop*0.3f));
			pk::fade(0);
			pk::background(-1);
			pk::fade(255);
			array<pk::point> range = pk::range(city_.get_pos(), 0, 8);  //八个范围全部起火
			//获取城市的资源 
			int building_gold=pk::get_gold(menu_base);  //建筑金钱
			int building_food=pk::get_food(menu_base); //建筑粮草
			int building_charge1 = pk::get_weapon_amount(menu_base,1); //枪
			int building_charge2 = pk::get_weapon_amount(menu_base,2); //戟
			int building_charge3 = pk::get_weapon_amount(menu_base,3); //驽
			int building_charge4 = pk::get_weapon_amount(menu_base,4); //马
			int sel_building_charge1 = pk::get_weapon_amount(selected_base,1); //枪
			int sel_building_charge2 = pk::get_weapon_amount(selected_base,2); //戟
			int sel_building_charge3 = pk::get_weapon_amount(selected_base,3); //驽
			int sel_building_charge4 = pk::get_weapon_amount(selected_base,4); //马
			int city_distance = get_city_distance(menu_base, selected_base);  //移动回合
			float n_ = 1.0f-0.25f*city_distance < 0 ? 0.25f : 1.0f-0.25f*city_distance; //最远不得超过3个回合
			int add_charge_amount1 = int(building_charge1*n_);
			int add_charge_amount2 = int(building_charge2*n_);
			int add_charge_amount3 = int(building_charge3*n_);
			int add_charge_amount4 = int(building_charge4*n_);
			int max_charge1 = pk::get_max_weapon_amount(menu_base,1); //枪
			int max_charge2 = pk::get_max_weapon_amount(menu_base,2); //戟
			int max_charge3 = pk::get_max_weapon_amount(menu_base,3); //驽
			int max_charge4 = pk::get_max_weapon_amount(menu_base,4); //马
			//修正数据
			if ((add_charge_amount1+ sel_building_charge1) > max_charge1)
			{
				add_charge_amount1 = max_charge1 - sel_building_charge1;
			}
			if ((add_charge_amount2 + sel_building_charge2) > max_charge2)
			{
				add_charge_amount2 = max_charge2 - sel_building_charge2;
			}
			if ((add_charge_amount3 + sel_building_charge3) > max_charge3)
			{
				add_charge_amount3 = max_charge3 - sel_building_charge3;
			}
			if ((add_charge_amount4 + sel_building_charge4) > max_charge4)
			{
				add_charge_amount4 = max_charge4 - sel_building_charge4;
			}
			pk::add_gold(selected_base,int(building_gold*0.25),true);
			pk::add_food(selected_base,int(building_food*0.25),true);
			pk::add_weapon_amount(selected_base, 1,add_charge_amount1, true);//枪
			pk::add_weapon_amount(selected_base, 2,add_charge_amount2, true);//戟
			pk::add_weapon_amount(selected_base, 3,add_charge_amount3, true);//弩
			pk::add_weapon_amount(selected_base, 4,add_charge_amount4, true);//马
			pk::set_hp(city_, 3000);
			pk::set_public_order(city_, 10);
			pk::neutralize(menu_base);  //所属变空白
			
			//城市更新
			selected_base.update();
			for (int l = 0; l < int(range.length); l++)
            {
				pk::create_fire(range[l], 1 + pk::rand(2), null, true); //火计
            }
			pk::history_log(selected_base.get_pos(), force_.color, pk::encode(pk::format("\x1b[1x{}\x1b[0x暴政掠夺资源转移到\x1b[1x{}\x1b[0x,损失了大量的\x1b[29x金、粮、兵装\x1b[0x",
			pk::decode(pk::get_name(menu_base)),pk::decode(pk::get_name(selected_base)))));
		}
		 
		 //负面影响
		 void 暴政负面影响()
		 {
			 
			auto city_list = pk::list_to_array(pk::get_city_list());

			for (int i = 0; i < int(city_list.length); i++)
			{
				pk::city@ city_ = city_list[i];
				pk::building@ building_ = pk::city_to_building(city_);
				if (!building_.is_player())
					continue;
				pk::person@ taishu = pk::get_person(pk::get_taishu_id(building_));
				if (pk::is_alive(taishu))
				{
					auto mibun_list = pk::list_to_array(pk::get_person_list(building_, pk::mibun_flags(身份_都督, 身份_太守, 身份_一般)));
					if (0 < mibun_list.length)
					{
						for (int j = 0; j < int(mibun_list.length); j++)
							{
							pk::person@ person_0 = mibun_list[j];
							// 야망 매우높음/높음 충성도 변화 
							if (person_0.ambition == 야망_매우높음 or person_0.ambition == 야망_높음)
							{
								pk::add_loyalty(person_0, -10);
							}
							// 야망 보통 충성도 변화 
							else if (person_0.ambition == 야망_보통)
							{
								pk::add_loyalty(person_0, 0);
							}
							// 야망 매우낮음/낮음 충성도 변화 
							else if (person_0.ambition == 야망_낮음 or person_0.ambition == 야망_매우낮음)
							{
								pk::add_loyalty(person_0, 10);
							}
						} // for
					} // if 

				} // if	
			} // for 
			pk::history_log(selected_base.get_pos(), force_.color,pk::encode(pk::format("由于\x1b[1x{}\x1b[0x暴政迁民的暴行，\x1b[1x{}\x1b[0x势力内部武将产生了分歧和忠诚的浮动.",pk::decode(pk::get_name(kunshu_)),pk::decode(pk::get_name(kunshu_)))));
		 }
	}//Main
	Main main;
}//namespace
