﻿// ## 2023/05/13 # 江东新风 # 适配血色，暂时将参数调回0328设置 ##
// ## 2023/04/12 # 铃 # 为了适配淡然做的山地和山路地形,把高度禁入和悬崖路禁入取消 ##
// ## 2023/04/12 # 铃 # 完全翻译 ##
// ## 2020/10/30 #江东新风#同步马术书大神的更新##
// ## 2020/08/03 #messi#改进表述##
// ## 2020/08/02 #江东新风#增加回合末及Save&Load时的重疊物檢測以防閃退##
// ## 2020/07/26 ##
/*
※ 주의사항 : 본 스크립트 적용 시 지형.xml 에서 적용된 일부 산악/물가 지형 설정이 무시됩니다.

@만든이: 기마책사
@Update: '18.12.22   / 최초적용: 거점 주변의 [地形_崖]과 [地形_岸]를 [地形_未定]으로 일괄 변경함
@Update: '18.12.23   / 변경내용: [地形_未定] 에 있는 부대 강제해산 추가
@Update: '19.1.2     / 변경내용: 지방별 고도제한 조건 추가, 벼랑길주변 낭떠러지 [地形_未定]으로 일괄 변경
@Update: '19.2.7     / 변경내용: [地形_未定] 에 있는 건물 강제철거 추가
@Update: '19.2.15    / 변경내용: 고도제한 판정 기준좌표 수정 (칸 모서리에서 가운데로)
@Update: '20.8.29    / 변경내용: 캠페인에서는 커스텀설정 비활성화
*/

namespace 地形移动限制
{
    //---------------------------------------------------------------------------------------

const bool 基地周围地形_更改设置 = true;     // 基地周围的 [地形_崖]和 [地形_岸]更改为 [地形_未定]

// 基地周围地形_更改设置=true 时 :
const bool 城市周围地形_更改设置 = true;     // 基地周围的 [地形_崖]和 [地形_岸]更改为 [地形_未定]
const bool 关隘周围地形_更改设置 = true;     // 基地周围的 [地形_崖]和 [地形_岸]更改为 [地形_未定]
const bool 港口周围地形_更改设置 = true;     // 基地周围的 [地形_崖]和 [地形_岸]更改为 [地形_未定]

const int  城市周围地形_更改范围 = 3;
const int  关隘周围地形_更改范围 = 2;
const int  港口周围地形_更改范围 = 2;

//---------------------------------------------------------------------------------------
const bool 限制地形_部队解散设置 = true;    // 注意: 建议设置为 true (移动时变化的地形的部队会弹出)
//---------------------------------------------------------------------------------------

const bool 高地地形_更改设置 = true;     // 地形高度高于标准的 [地形_崖]更改为 [地形_未定]
const int  高地地形_标准高度_西蜀南蛮 = 200;      // 注意事项 : 不可低于 180 设置
//下方两个参数设置成数组，是为了方便在地图间切换0：0328版本,1:大浪版本，2：备用参数
const array<int>  高地地形_标准高度_其他区域 = { 130, 180, 130 };      // 注意事项 : 不可低于 100 设置,大浪地圖此處為180,0328為130

//---------------------------------------------------------------------------------------

const array<bool> 悬崖路周围_更改设置 = { true, false, true };     // 悬崖路周围比悬崖路低的悬崖 [地形_崖]更改为 [地形_未定]，大浪此處為false

    //---------------------------------------------------------------------------------------

    class Main
    {

        Main()
        {
            pk::bind(102, pk::trigger102_t(onGameInit));
            //pk::bind(105, pk::trigger105_t(remove_invalid_object1));
            //pk::bind(106, pk::trigger106_t(remove_invalid_object1));
            pk::bind(112, pk::trigger112_t(remove_invalid_object3));
            pk::bind(112, pk::trigger112_t(onTurnEnd));
            //pk::bind(111, pk::trigger111_t(remove_invalid_object3));

        }

        void onGameInit()
        {
            if (!pk::is_campaign())
            {
                if (基地周围地形_更改设置)
                {
                    if (城市周围地形_更改设置)
                    {
                        for (int num = 建筑_城市开始; num < 城市_末; num++)
                        {
                            disable_terrain_around_building(num, 城市周围地形_更改范围);
                        }
                    }

                    if (关隘周围地形_更改设置)
                    {
                        for (int num = 建筑_关卡开始; num < 据点_关卡末; num++)
                        {
                            disable_terrain_around_building(num, 关隘周围地形_更改范围);
                        }
                    }

                    if (港口周围地形_更改设置)
                    {
                        for (int num = 据点_关卡末; num < 建筑_港口末; num++)
                        {
                            disable_terrain_around_building(num, 港口周围地形_更改范围);
                        }
                    }
                }

                if (高地地形_更改设置)
                {
                    disable_terrain_high_pos();
                }

                if (悬崖路周围_更改设置[(pk::is_new_map()?1:0)])
                {
                    disable_terrain_nearby_cliff();
                }
            }
            remove_invalid_object();

        }


        void onTurnEnd(pk::force@ force)
        {
            if (pk::is_campaign()) return;
            remove_invalid_object();
        }

        //---------------------------------------------------------------------------


        void disable_terrain_around_building(int building_id, int around_range)
        {
            pk::building@ building = pk::get_building(building_id);
            pk::array<pk::point> range = pk::range(building.pos, 1, around_range);
            for (int j = 0; j < int(range.length); j++)
            {
                pk::hex@ hex = pk::get_hex(range[j]);
                if (hex.terrain == 地形_崖 or hex.terrain == 地形_岸)
                {
                    hex.terrain = 地形_未定;
                }
            }

        }

        void disable_terrain_high_pos()
        {
            for (int pos_x = 0; pos_x < 200; pos_x++)
            {
                for (int pos_y = 0; pos_y < 200; pos_y++)
                {
                    pk::point pos;
                    pos.x = pos_x;
                    pos.y = pos_y;

                    pk::hex@ hex = pk::get_hex(pos);

                    // 지방별 기준높이
                    int region_id = pk::get_region_id(pk::get_city(pk::get_city_id(pos)));
                    int ref_height = 0;
                    if (region_id >= 地方_巴蜀)
                        ref_height = pk::max(180, 高地地形_标准高度_西蜀南蛮);
                    else
                        ref_height = pk::max(100, 高地地形_标准高度_其他区域[(pk::is_new_map() ? 1 : 0)]);

                    // 해당 좌표 높이 제한
                    int pos_height = pk::get_height_map(pk::hex_pos_to_height_map_pos(pos) + 2).height;
                    if (hex.terrain == 地形_崖 and pos_height >= ref_height)
                    {
                        hex.terrain = 地形_未定;
                    }
                }
            }
        }

        void disable_terrain_nearby_cliff()
        {
            for (int pos_x = 0; pos_x < 200; pos_x++)
            {
                for (int pos_y = 0; pos_y < 200; pos_y++)
                {
                    pk::point pos;
                    pos.x = pos_x;
                    pos.y = pos_y;

                    pk::hex@ hex = pk::get_hex(pos);
                    if (hex.terrain == 地形_栈道)
                    {
                        int pos_height = pk::get_height_map(pk::hex_pos_to_height_map_pos(pos) + 2).height;
                        for (int dir = 0; dir < 方向_末; dir++)
                        {
                            pk::point neighbor_pos = pk::get_neighbor_pos(pos, dir);
                            if (pk::is_valid_pos(neighbor_pos))
                            {
                                pk::hex@ neighbor_hex = pk::get_hex(neighbor_pos);
                                int neighbor_height = pk::get_height_map(pk::hex_pos_to_height_map_pos(neighbor_pos) + 2).height;
                                if (neighbor_hex.terrain == 地形_崖 and (pos_height >= neighbor_height))
                                {
                                    neighbor_hex.terrain = 地形_未定;
                                }
                            }
                        }
                    }
                }
            }
        }


        void remove_invalid_object()
        {
            for (int pos_x = 0; pos_x < 200; pos_x++)
            {
                for (int pos_y = 0; pos_y < 200; pos_y++)
                {
                    pk::point pos;
                    pos.x = pos_x;
                    pos.y = pos_y;

                    pk::hex@ hex = pk::get_hex(pos);
                    if (hex.terrain == 地形_未定 and hex.has_unit)
                        pk::kill(pk::get_unit(pos));

                    if (hex.terrain == 地形_未定 and hex.has_building)
                        pk::kill(pk::get_building(pos));
                    //增加当建筑物和单位重疊的处理by江东新风

                    //if (hex.has_unit and hex.has_building)
                        // pk::kill(pk::get_unit(pos));
                }
            }

        }

        
        void remove_invalid_object3(pk::force@ force)
        {
            array<pk::unit@> arr = pk::list_to_array(pk::get_unit_list());
            for (int i = 0; i < int(arr.length); i++)
            {
                pk::unit@ dst = arr[i];
                pk::point pos = dst.pos;
                pk::hex@ hex = pk::get_hex(pos);
                if (hex.has_building)
                {
                    pk::history_log(dst.get_pos(), force.color, pk::encode(pk::format("\x1b[1x{}\x1b[0x内部士兵与主将出现了分歧,士兵们解甲归田了.", pk::decode(pk::get_name(dst)))));
                    pk::kill(dst);
                }

            }

        }



    }

    Main main;
}

