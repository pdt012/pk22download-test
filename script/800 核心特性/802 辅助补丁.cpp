﻿// ## 2023/05/12 # 江东新风 # 精简功能，并改为非custom ##
// ## 2021/10/21 # 江东新风 # 改进贼将杀死的设定 ##
// ## 2021/07/09 # 白马叔叔 # AI探索未发现人才、小城的未发现移动到大城、杀死不该活着的贼、杀死没武将的势力 ##
// @@ 2021/04/18 @ 白马叔叔 @@

namespace 辅助补丁
{
	//---------------------------------------------------------------------------------------
	const int begin_month = 4; // 从哪月之后才开始探索，权衡玩家偷鸡，也不能浪费太多回合
	//---------------------------------------------------------------------------------------

	class Main
	{

		Main()
		{
			pk::bind(107, pk::trigger107_t(callback));
			pk::bind(108, pk::trigger108_t(callback_month));
			pk::bind(102, pk::trigger102_t(game_init));
		}

		void game_init()
		{
			//用于替代处理自制武将双特技飞将的问题
			if (pk::get_elapsed_days() <= 1)
			{
				for (int i = 850; i < 武将_末; ++i)
				{
					pk::person@ person = pk::get_person(i);
					if (person.skill2 == 特技_飞将 and person.skill2 == person.skill3)
					{
						person.skill2 = -1;
						person.skill3 = -1;
					}
				}
			}
		}
		// 事件-AI把未发现人才探索出来
		void callback()
		{
			// 经过月份超过4个月保护期、当前月份超过第4个月
			if (pk::get_elapsed_months() >= begin_month && pk::get_month() >= begin_month)
				pk::scene(pk::scene_t(scene_explore_person));

			// 在小城登场的武将移动到所属城市，设定在1月11日手动发生
			if (pk::get_month() == 1 && pk::get_day() == 11)
				pk::scene(pk::scene_t(scene_move_person));
		}

		void callback_month()
		{
			// 杀死变成普通武将的临时武将
			pk::scene(pk::scene_t(scene_kill_person));

			// 杀死已经没有城市、没有港关的势力
			pk::scene(pk::scene_t(scene_kill_force));
		}

		// 粗糙点就是有未发现直接变成在野。再细一些就是有无眼力、有无政治高、而且没有任务中、外出的
		void scene_explore_person()
		{
			for (int i = 0; i < 建筑_城市末; i++)
			{
				auto building_t = pk::get_building(i);

				if (building_t.is_player())
					continue; // 跳过玩家地盘
				if (building_t.get_force_id() == -1)
					continue; // 跳过空城无势力
				if (pk::get_taishu_id(building_t) == -1)
					continue; // 跳过无太守

				auto list_0 = pk::get_person_list(building_t, pk::mibun_flags(身份_未发现)); // 只取未发现身份的

				if (list_0.count == 0)
					continue; // 跳过没有未发现武将

				bool 有眼力或政治高 = false;																				// 有眼力或者有政治高的人才
				auto list_1 = pk::get_person_list(building_t, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般)); // 只取未发现身份的

				if (building_t.has_skill(特技_眼力))
					有眼力或政治高 = true; // 城市有眼力特技，直接判定true省计算
				else
				{
					for (int j = 0; j < list_1.count; j++)
					{
						auto person_1 = list_1[j];
						if (!pk::is_absent(person_1) && !pk::is_unitize(person_1) && person_1.stat[2] > 69)
						{
							有眼力或政治高 = true; // 人物非离开，非出征，政治超过69
							break;
						}
					}
				}

				if (有眼力或政治高)
				{
					for (int k = 0; k < list_0.count; k++)
					{
						auto person_2 = list_0[k];
						if (pk::is_absent(person_2))
							break; // 人物不在场直接结束（三顾触发后，使刘表不能找出诸葛均、黄月英）
						else
						{
							person_2.mibun = 身份_在野;
							// pk::trace(pk::format("被AI探索的人：{}", pk::decode(pk::get_name(person_2))));
							person_2.update();
							break;
						}
					}
				}
			}
		}

		// 移动武将到城市，以免永远找不到
		void scene_move_person()
		{
			for (int i = 42; i < 建筑_据点末; i++)
			{
				auto building_t = pk::get_building(i);
				auto list_0 = pk::get_person_list(building_t, pk::mibun_flags(身份_未发现)); // 只取未发现身份的
				auto list_count = list_0.count;
				if (list_count == 0)
					continue; // 跳过没有未发现武将
				for (int j = 0; j < list_count; j++)
				{
					auto person = list_0[j];
					if (person.appearance < pk::get_year()) // 登场年得小于当前年，比方登场是207，要晚一年，在208才会自动出现
					{
						// pk::trace(pk::format("被移动到城市的人：{}", pk::decode(pk::get_name(person))));
						pk::move(person, pk::get_building(pk::get_city_id(building_t.pos)), false);
					}
				}
			}
		}

		// 杀死变成普通武将的临时武将
		void scene_kill_person()
		{
			for (int person_id = 敌将_开始; person_id < 敌将_末; person_id++)
			{
				pk::person @person = pk::get_person(person_id);
				if (pk::is_alive(person))
				{
					if (person.mibun != 身份_死亡 && person.district < 非贼势力_末)
						pk::kill(person, null, null, null, 1); // 杀死 还活着但归属非异族军团的
					if (person.mibun == 身份_在野)
						pk::kill(person, null, null, null, 1);
				}
				if (person.mibun == 身份_死亡)
				{
					// pk::trace(pk::format("重置贼将 id:{}", person_id));
					pk::reset(person);
				}
			}
		}

		// 杀死已经没有城市、没有港关的势力
		void scene_kill_force()
		{
			pk::list<pk::force @> force_list = pk::get_force_list(false);
			if (force_list.count == 1)
				return; // 只有一个返回
			for (int i = 0; i < force_list.count; i++)
			{
				if (ch::get_force_person_count(force_list[i]) <= 0)
					pk::kill(force_list[i], false, null); // 杀死已经没有武将仍不破灭的势力
			}
		}
	}
Main main;
}