﻿// ## 2023/07/26 # 黑店小小二 # 修复无数据时界面默认选中数组溢出 ##
// ## 2021/03/19 # 江东新风 # 记忆组队及组队调整基本完善 ##
// ## 2021/03/19 # 江东新风 # 组队调整的设定 ##
/*
   玩家主动调整的设想
   按钮 调整组队
   1.调整现有组队优先级：选择兵种，选择主将，展示队伍组成，选择后询问调整后的优先级
   2.新增部队组成 选择兵种，选择主将，选择副将，组成后选择希望放置的优先级。
*/

namespace 自动_组队
{
    const bool 采用记忆组队 = true;

    /*
    // 如果据点存在未出征的高能力武将，则主将更换将被应用 거점에 미출진한 높은 능력의 무장 있는 경우 주장 변경 적용함
    const bool 优化出征武将_武将变更 = true;    // true=적용,  false=미적용

    // 能力值低于基准值的武将将被变更 기준값 이하 무장이 주장인 경우 변경
    const int 武将变更_能力基准值 = 70;

    // 出征武将能力比较权重(无预设范围限制)출진무장 능력 비교용 가중치 (설정값 범위제약 없음)
    const float 武将变更_统率权重 = 0.8f;
    const float 武将变更_武力权重 = 1.0f;
    const float 武将变更_智力权重 = 0.5f;

    // 무장변경 기준이하 적성 고려 ('19.2.9)
    const bool 武将变更_启用适性标准 = true;      // 기준이하의 적성 시 변경 적용
    const int  武将变更_适性基准值 = 1;           // 0:适性_C,   1:적성_B,   2:적성_A,   3:적성_S 이상
    const float 武将变更_适性权重 = 0.3f;
    // 무장변경 특기 보유자 우선 고려 ('20.9.16)
    const bool 武将变更_启用特技加权 = true;    //true=启用적용,  false=不启用미적용
*/
    array<array<int>> 记忆组队数组;

    const array<array<int>> unit_info_data = {
        //{7829,2,-1,70,9,12,54,976,634,0},
        {7843,0,-1,316,1,0,0,8,8,0},
        {7844,0,-1,314,1,531,0,8,8,0},
        {7847,9,-1,428,1,8,0,523,8,0},
        {7849,9,-1,428,1,6,263,528,8,0},
        {7850,9,-1,435,1,0,8,8,263,0},
        {7851,9,-1,435,1,531,8,8,263,0},
        {7906,2,-1,384,9,11,12,80,24,0},
        {7907,2,-1,393,9,19,204,42,22,0},
        {7909,2,-1,393,9,19,234,42,22,0},
        {7910,2,-1,411,9,63,204,42,22,0},
        {7911,2,-1,411,9,63,234,42,22,0},
        {7957,6,-1,-1,-1,19,16,64,16,80},
        {7958,7,-1,-1,0,99,42,298,154,0},
        {7959,6,-1,-1,-1,24,207,32,16,80},
        {7960,6,-1,-1,-1,68,207,32,16,80},
        {7961,6,-1,-1,-1,24,237,32,16,80},
        {7962,6,-1,-1,-1,72,237,24,16,336},
        {7984,2,-1,393,9,121,234,42,22,0},
        {7985,6,7984,-1,-1,5,3,32,16,80},
        {7986,2,-1,411,9,165,234,74,22,0},
        {7987,6,7986,-1,-1,5,3,64,16,80},
        {7988,2,-1,411,9,247,234,74,22,0},
        {7989,6,7988,-1,-1,5,3,64,16,80},
        {7990,2,-1,411,9,329,234,74,22,0},
        {7991,6,7990,-1,-1,5,3,64,16,80},
        {8023,2,-1,393,9,121,204,74,22,0},
        {8024,6,8023,-1,-1,5,3,64,16,80},
        {8025,2,-1,411,9,197,204,26,22,0},
        {8026,6,-1,-1,-1,202,207,16,16,80},
        {8036,8,-1,238,24,99,10,78,28,0},
        {8037,8,-1,238,24,185,10,78,28,0}

    };

    const array<array<int>> person_info_data = {
        //头像框
{7838,9,-1,524,1,76,12,396,172,0},
{7839,9,-1,525,1,12,12,64,172,0},
{7840,9,-1,526,1,472,12,64,172,0},
{7841,2,-1,375,9,84,16,112,164,0},
{7842,2,-1,375,9,240,16,224,164,0},
{7895,2,-1,327,9,103,130,74,22,0},
{7896,2,-1,327,9,259,130,74,22,0},
{7897,2,-1,327,9,371,130,74,22,0},
{7898,2,-1,357,9,103,154,74,22,0},
{7899,2,-1,357,9,259,154,74,22,0},
{7900,2,-1,357,9,371,154,74,22,0},
{7901,2,-1,366,9,373,40,70,86,0},
{7902,2,-1,366,9,261,40,70,86,0},
{7903,2,-1,366,9,105,40,70,86,0},
{7936,6,-1,-1,-1,384,157,48,16,80},
{7937,6,-1,-1,-1,272,157,48,16,80},
{7938,6,-1,-1,-1,116,157,48,16,80},
{7940,8,-1,190,24,17,16,46,26,0},
{7941,7,-1,-1,0,376,43,64,80,0},
{7942,7,-1,-1,0,264,43,64,80,0},
{7943,7,-1,-1,0,108,43,64,80,0},
{7944,6,-1,-1,-1,376,133,64,16,80},
{7945,6,-1,-1,-1,264,133,64,16,80},
{7946,6,-1,-1,-1,108,133,64,16,80},
{7947,6,-1,-1,-1,336,20,32,16,80},
{7948,6,-1,-1,-1,124,20,32,16,80},
//框线
{8200,0,-1,316,1,0,0,8,8,0},
{8201,0,-1,314,1,531,0,8,8,0},
{8202,9,-1,428,1,8,0,523,8,0},
{8203,9,-1,428,1,6,303,528,8,0},
{8204,9,-1,435,1,0,8,8,303,0},
{8205,9,-1,435,1,531,8,8,303,0},
//兵器选择和频次
{7881,2,-1,327,9,70,268,276,18,0},
{7882,7,7881,-1,0,2,2,272,14,0},
{7887,2,-1,327,9,353,264,122,26,0},
{7888,6,7887,-1,-1,2,2,104,16,400},
{7956,8,-1,190,24,17,268,46,26,0},
{7992,8,-1,190,9,67,193,62,22,0},
{7993,6,7992,-1,-1,15,3,32,16,80},
{7995,8,-1,190,9,133,193,62,22,0},
{7996,6,7995,-1,-1,15,3,32,16,80},
{7998,8,-1,190,9,199,193,62,22,0},
{7999,6,7998,-1,-1,15,3,32,16,80},
{8001,8,-1,190,9,265,193,62,22,0},
{8002,6,8001,-1,-1,15,3,32,16,80},
{8004,8,-1,190,9,331,193,62,22,0},
{8005,6,8004,-1,-1,15,3,32,16,80},
{8007,8,-1,190,9,397,193,62,22,0},
{8008,6,8007,-1,-1,15,3,32,16,80},
{8010,8,-1,190,9,463,193,62,22,0},
{8011,6,8010,-1,-1,15,3,32,16,80},
{8013,8,-1,190,9,67,219,62,22,0},
{8014,6,8013,-1,-1,15,3,32,16,80},
{8016,8,-1,190,9,133,219,62,22,0},
{8017,6,8016,-1,-1,15,3,32,16,80},
{8019,2,-1,384,9,17,192,42,24,0},
{8020,6,8019,-1,-1,5,4,32,16,80},
{8021,2,-1,384,9,17,219,42,24,0},
{8022,6,8021,-1,-1,5,4,32,16,80}

    };

    const array<array<int>> middle_info_data = {
{9131,5,-1,286,8,660,626,78,30,0},
{9133,0,-1,1225,1,575,235,58,28,0},
{9134,5,-1,286,8,760,626,78,30,0},
{9136,0,-1,1226,1,575,435,58,28,0}


    };

    const array<string> textstr = { "部队能力","兵科","剑兵","气力","80","战法","","","","兵种适性","" };

    const array<string> text183_person_str = { "副将2特技","副将1特技","主将特技","副将2","副将1","主将","副将","主将","0","剑","枪","戟","弩","军马","木兽","井阑","走舸","楼船","武器","舰船" };

    const array<string> btn183_person_str = { "武将","频次","剑","枪","戟","弩","军马","冲车","井阑","走舸","楼船" };

    bool 调试模式 = false;
    class Main
    {
        file f;

        pk::dialog@ autoarmydlg_ = @null;
        array<pk::text@> text183_unit2_(11 + 12, null);//部队默认信息
        array<pk::button@> btn183_unit2_(2 + 9, null);

        array<pk::text@> text183_unit_(15, null);//部队能力信息
        pk::chart@ chart183_ = @null;
        pk::march_cmd_info march_info_;
        autoarmyinfo@ autoinfo_;

        array<pk::text@> text183_person_(8 + 12, null);
        array<pk::button@> btn183_person_(1 + 10, null);
        array<pk::face@> face183_person_(3, null);//三个头像
        pk::slider@ slider183_ = @null;

        array<pk::button@> btn183_edit_(2, null);

        //listview
        pk::listview@ lv_ = @null;

        array<autoarmyinfo> autoarmy_ex_(组队数_末);
        array<int16> autoitems_(组队数_末, -1);
        bool is_navy_ = false;

        Main()
        {
            pk::set_func(56, pk::func56_t(callback));
            pk::bind(108, pk::trigger108_t(每月记录部队));
            //pk::bind(107, pk::trigger107_t(每月记录部队));
            add_menu();
        }

        void add_menu()
        {
            // 组队调整 메뉴 (유저용)
            pk::menu_item 组队调整;
            组队调整.menu = 101;
            // 组队调整.pos = 8;
            //组队调整.shortcut = global_menu::shortcut_组队调整;
            组队调整.init = pk::building_menu_item_init_t(init);
            组队调整.is_visible = pk::menu_item_is_visible_t(isVisible_组队调整);
            组队调整.is_enabled = pk::menu_item_is_enabled_t(isEnabled_组队调整);
            组队调整.get_text = pk::menu_item_get_text_t(getText_组队调整);
            组队调整.get_desc = pk::menu_item_get_desc_t(getDesc_组队调整);
            组队调整.handler = pk::menu_item_handler_t(handler_组队调整);
            pk::add_menu_item(组队调整);
        }

        //---------------------------------------------------------------------------------------
        pk::building@ building_;
        pk::force@ force_;
        void init(pk::building@ building)
        {
            @building_ = @building;
            @force_ = pk::get_force(building.get_force_id());
        }

        string getText_组队调整()
        {
            return pk::encode("组队调整"); //chengqiangweixiu
        }

        string getDesc_组队调整()
        {
            return pk::encode("调整组队的优先级或新增默认组队");
        }

        bool isEnabled_组队调整()
        {
            return true;
        }

        bool isVisible_组队调整()
        {
            if (pk::is_campaign()) return false;
            if (!pk::is_alive(building_)) return false;
            if (!building_.is_player()) return false;
            return true;
        }

        bool handler_组队调整()
        {
            pk::dialog@ dialog = create_dlg();

            open_dlg(dialog);
            //update_dlg183(march_info_);
            /*
            int choose = pk::choose(pk::encode("请选择要执行的操作"), { pk::encode("调整组队优先级"), pk::encode(" 新增组队设定 "), pk::encode("取消") });

            if (choose == 0) handler_调整组队优先级();
            else if (choose == 1) handler_新增组队设定();
            else return false;*/
            return true;

        }

        void handler_调整组队优先级()
        {
            array<array<int>> temp_arr;
            for (int i = 0; i < 300; ++i)
            {
                autoarmyinfo@ army_info = @autoarmy_ex[i];
                if (army_info.member[0] == -1) break;//-1表示已经到末尾
                if (army_info.member[0] == 0 and army_info.member[1] == 0) break;
                array<int> arr_t = { army_info.member[0], army_info.member[1], army_info.member[2], army_info.ground_weapon, army_info.sea_weapon, army_info.freq };
                temp_arr.insertLast(arr_t);
            }
            记忆组队数组 = temp_arr;
            //1.调整现有组队优先级：选择兵种，选择主将，展示队伍组成，选择后询问调整后的优先级
            int weapon_choose = pk::choose(pk::encode("请选择要对象兵种"), { pk::encode("枪兵"), pk::encode("戟兵"), pk::encode("弩兵"), pk::encode("骑兵") });
            pk::list<pk::person@ >person_list = get_target_person_list(weapon_choose + 1, 记忆组队数组);
            if (person_list.count > 0)
            {
                pk::list<pk::person@> person_sel = get_person_sel(person_list);
                array<array<int>> target_weapon_unit_arr = get_target_weapon_unit_arr(weapon_choose + 1, 记忆组队数组);//此数组用于排序优先级
                if (person_sel.count == 0)
                {
                    int final_choose = pk::choose(pk::encode("当前并未选择主将，是否继续选择？"), { pk::encode(" 是 "), pk::encode(" 否 ") });
                    if (final_choose == 0) person_sel = get_person_sel(person_list);
                    else return;
                }
                else
                {
                    int leader_id = person_sel[0].get_id();
                    //选择好主将的情况，罗列该主将对应的部队组成，展示目前优先级，选择要更改后的优先级
                    array<array<int>> target_unit_arr = get_target_unit_arr(leader_id, target_weapon_unit_arr);
                    array<string> unit_info_list;
                    if (调试模式)
                    {
                        for (int i = 0; i < int(target_unit_arr.length); ++i)
                        {
                            for (int j = 0; j < int(target_unit_arr[i].length); ++j)
                            {
                                pk::trace(pk::format("target_unit_arr.{}:j:{},{}", i, j, target_unit_arr[i][j]));
                            }

                        }
                        for (int i = 0; i < int(target_weapon_unit_arr.length); ++i)
                        {
                            for (int j = 0; j < int(target_weapon_unit_arr[i].length); ++j)
                            {
                                pk::trace(pk::format("target_weapon_unit_arr.{}:j:{},{}", i, j, target_weapon_unit_arr[i][j]));
                            }

                        }
                        for (int i = 0; i < int(记忆组队数组.length); ++i)
                        {
                            for (int j = 0; j < int(记忆组队数组[i].length); ++j)
                            {
                                pk::trace(pk::format("记忆组队数组.{}:j:{},{}", i, j, 记忆组队数组[i][j]));
                            }

                        }
                    }

                    //选择数量不定的制作比较麻烦，就做6个或10个的情况
                    if (int(target_unit_arr.length) < 6)
                    {
                        for (int i = 0; i < int(target_unit_arr.length); ++i)
                        {
                            unit_info_list.insertLast(pk::encode(pk::format("{}，{}，{}，{}，优先级：{}", get_weapon_name(target_unit_arr[i][3]), pk::decode(pk::get_name(pk::get_person(target_unit_arr[i][0]))), pk::decode(pk::get_name(pk::get_person(target_unit_arr[i][1]))), pk::decode(pk::get_name(pk::get_person(target_unit_arr[i][2]))), target_unit_arr[i][7])));
                        }
                        if (调试模式)
                        {
                            for (int i = 0; i < int(unit_info_list.length); ++i)
                            {
                                pk::trace(pk::format("listpos:{},{}", i, pk::decode(unit_info_list[i])));
                            }
                        }


                        unit_choose_0(unit_info_list, target_unit_arr, target_weapon_unit_arr);

                    }
                    else if (int(target_unit_arr.length) > 6)
                    {
                        for (int i = 0; i < int(target_unit_arr.length); ++i)
                        {
                            if (i != 5 and i != int(target_unit_arr.length - 1)) unit_info_list.insertLast(pk::encode(pk::format("{}，{}，{}，{}，优先级：{}", get_weapon_name(target_unit_arr[i][3]), pk::decode(pk::get_name(pk::get_person(target_unit_arr[i][0]))), pk::decode(pk::get_name(pk::get_person(target_unit_arr[i][1]))), pk::decode(pk::get_name(pk::get_person(target_unit_arr[i][2]))), target_unit_arr[i][7])));
                            if (i == 5) unit_info_list.insertLast(pk::encode("NEXT"));
                            if (i == int(target_unit_arr.length - 1)) unit_info_list.insertLast(pk::encode("BACK"));
                        }
                        if (调试模式)
                        {
                            for (int i = 0; i < int(unit_info_list.length); ++i)
                            {
                                pk::trace(pk::format("listpos:{},{}", i, pk::decode(unit_info_list[i])));
                            }
                        }

                        unit_choose_0(unit_info_list, target_unit_arr, target_weapon_unit_arr);

                    }



                }
            }
            else
            {
                int final_choose = pk::choose(pk::encode("所选兵种并无储存的部队，是否继续选择？"), { pk::encode(" 是 "), pk::encode(" 否 ") });
                if (final_choose == 0) handler_调整组队优先级();
                else return;
            }
        }

        void handler_新增组队设定()
        {
            //选择武将
            int weapon_choose = pk::choose(pk::encode("请选择要对象兵种"), { pk::encode("枪兵"), pk::encode("戟兵"), pk::encode("弩兵"), pk::encode("骑兵") });
            string weapon_name = get_weapon_name(weapon_choose + 1);
            pk::list<pk::person@ >person_list = pk::get_person_list(force_, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般));
            pk::list<pk::person@ >person_sel = pk::person_selector(pk::encode(pk::format("选择{}主将", weapon_name)), pk::encode(pk::format("选择{}的主将", weapon_name)), person_list, 1, 1, person_list, 37);
            if (person_sel.count == 0) return;
            int leader_id = person_sel[0].get_id();

            pk::list<pk::person@ >person_list2 = get_deputy_list(person_sel[0]);
            pk::list<pk::person@ >person_sel2 = pk::person_selector(pk::encode(pk::format("选择{}副将", weapon_name)), pk::encode(pk::format("选择{}的两位副将", weapon_name)), person_list2, 0, 2, person_list2, 37);
            int deputy1 = -1;
            int deputy2 = -1;
            if (person_sel2.count > 0) deputy1 = person_sel2[0].get_id();
            if (person_sel2.count == 2) deputy2 = person_sel2[1].get_id();

            //选择优先级
            array<array<int>> temp_arr;
            for (int i = 0; i < 300; ++i)
            {
                autoarmyinfo@ army_info = @autoarmy_ex[i];
                if (army_info.member[0] == -1) break;//-1表示已经到末尾
                if (army_info.member[0] == 0 and army_info.member[1] == 0) break;
                array<int> arr_t = { army_info.member[0], army_info.member[1], army_info.member[2], army_info.ground_weapon, army_info.sea_weapon, army_info.freq };
                temp_arr.insertLast(arr_t);
            }
            记忆组队数组 = temp_arr;

            array<array<int>> target_weapon_unit_arr = get_target_weapon_unit_arr(weapon_choose + 1, 记忆组队数组);//

            int same_pos = -1;
            for (int i = 0; i < int(target_weapon_unit_arr.length); ++i)
            {
                if (leader_id == target_weapon_unit_arr[i][0] and (weapon_choose + 1) == target_weapon_unit_arr[i][3])
                {
                    if ((deputy1 == target_weapon_unit_arr[i][1] and deputy2 == target_weapon_unit_arr[i][2]) or (deputy2 == target_weapon_unit_arr[i][1] and deputy1 == target_weapon_unit_arr[i][2]))
                    {
                        same_pos = i;
                    }
                }
            }
            if (same_pos != -1)
            {
                pk::message_box(pk::encode(pk::format("记录里存在相同的组队。\n{}，{}，{}，{}，优先级：{}。请问是否调整此队伍的优先级。", get_weapon_name(target_weapon_unit_arr[same_pos][3]), pk::decode(pk::get_name(pk::get_person(target_weapon_unit_arr[same_pos][0]))), pk::decode(pk::get_name(pk::get_person(target_weapon_unit_arr[same_pos][1]))), pk::decode(pk::get_name(pk::get_person(target_weapon_unit_arr[same_pos][2]))), same_pos)));
                reset_priority(same_pos, target_weapon_unit_arr);
            }
            else
            {
                //设定优先级
                array<int> temp1 = { leader_id, deputy1, deputy2, weapon_choose + 1 , 兵器_走舸, 1 };
                set_priority(target_weapon_unit_arr, temp1);
            }

        }

        void preset_dlg183(pk::dialog@dialog)
        {
            for (int i = 0; i < 11; ++i)
            {
                text183_unit2_[i].set_text(pk::encode(textstr[i]));
            }
            btn183_unit2_[0].set_text(pk::encode("陆上"));
            btn183_unit2_[1].set_text(pk::encode("水上"));

            pk::detail::funcref func0 = cast<pk::button_on_pressed_t@>(function(button) {
                main.is_navy_ = false;
                main.update_unitinfo(main.autoinfo_, main.is_navy_);
            });
            btn183_unit2_[0].on_button_pressed(func0);
            pk::detail::funcref func00 = cast<pk::button_on_pressed_t@>(function(button) {
                main.is_navy_ = true;
                main.update_unitinfo(main.autoinfo_, main.is_navy_);
            });
            btn183_unit2_[1].on_button_pressed(func00);


            for (int i = 0; i < 10; ++i)
            {
                if (i % 2 == 0)
                {
                    text183_unit_[i].set_text(pk::encode(ch::get_unit_stat_name(i / 2)));
                    text183_unit_[i].set_text_color(0, ch::get_stat_color(i / 2));
                }
            }
            for (int i = 10; i < 15; ++i)
            {
                text183_unit_[i].set_text(pk::encode(ch::get_unit_stat_name(i - 10)));
                text183_unit_[i].set_text_color(0, ch::get_stat_color(i - 10));
            }

            for (int i = 0; i < 20; ++i)
            {
                if (i > 8 and i < 18) continue;
                text183_person_[i].set_text(pk::encode(text183_person_str[i]));
            }

            for (int i = 0; i < 11; ++i)
            {
                btn183_person_[i].set_text(pk::encode(btn183_person_str[i]));
            }
            pk::force@ force = pk::get_force(pk::get_current_turn_force_id());
            bool has_tech_mushou = pk::has_tech(force, 技巧_开发木兽);
            bool has_tech_toushi = pk::has_tech(force, 技巧_开发投石);

            if (has_tech_mushou) btn183_person_[7].set_text(pk::encode("木兽"));

            if (has_tech_toushi)
            {
                btn183_person_[8].set_text(pk::encode("投石"));
                btn183_person_[10].set_text(pk::encode("斗舰"));
            }

            func0 = cast<pk::button_on_released_t@>(function(button) {
                main.select_person();
            });
            btn183_person_[0].on_button_released(func0);

            func0 = cast<pk::button_on_released_t@>(function(button) {
                main.set_freq_num();
            });
            btn183_person_[1].on_button_released(func0);

            //设置武器选择按钮功能

            pk::detail::funcref func1 = cast<pk::button_on_pressed_t@>(function(button) { main.autoinfo_.ground_weapon = 0; main.update_unitinfo(main.autoinfo_, main.is_navy_); });
            btn183_person_[2].on_button_pressed(func1);
            func1 = cast<pk::button_on_pressed_t@>(function(button) { main.autoinfo_.ground_weapon = 1; main.update_unitinfo(main.autoinfo_, main.is_navy_); });
            btn183_person_[3].on_button_pressed(func1);
            func1 = cast<pk::button_on_pressed_t@>(function(button) { main.autoinfo_.ground_weapon = 2; main.update_unitinfo(main.autoinfo_, main.is_navy_); });
            btn183_person_[4].on_button_pressed(func1);
            func1 = cast<pk::button_on_pressed_t@>(function(button) { main.autoinfo_.ground_weapon = 3; main.update_unitinfo(main.autoinfo_, main.is_navy_); });
            btn183_person_[5].on_button_pressed(func1);
            func1 = cast<pk::button_on_pressed_t@>(function(button) { main.autoinfo_.ground_weapon = 4; main.update_unitinfo(main.autoinfo_, main.is_navy_); });
            btn183_person_[6].on_button_pressed(func1);
            //冲车木兽
            func1 = cast<pk::button_on_pressed_t@>(function(button) {
                pk::force@ force = pk::get_force(pk::get_current_turn_force_id());
                bool has_tech_mushou = pk::has_tech(force, 技巧_开发木兽);
                main.autoinfo_.ground_weapon = has_tech_mushou ? 8 : 5;
                main.update_unitinfo(main.autoinfo_, main.is_navy_);
            });
            btn183_person_[7].on_button_pressed(func1);

            //井阑投石
            func1 = cast<pk::button_on_pressed_t@>(function(button) {
                pk::force@ force = pk::get_force(pk::get_current_turn_force_id());
                bool has_tech_toushi = pk::has_tech(force, 技巧_开发投石);
                main.autoinfo_.ground_weapon = has_tech_toushi ? 7 : 6;
                main.update_unitinfo(main.autoinfo_, main.is_navy_);
            });
            btn183_person_[8].on_button_pressed(func1);
            func1 = cast<pk::button_on_pressed_t@>(function(button) { main.autoinfo_.sea_weapon = 9; main.update_unitinfo(main.autoinfo_, main.is_navy_); });
            btn183_person_[9].on_button_pressed(func1);

            //楼船斗舰
            func1 = cast<pk::button_on_pressed_t@>(function(button) {
                pk::force@ force = pk::get_force(pk::get_current_turn_force_id());
                bool has_tech_toushi = pk::has_tech(force, 技巧_开发投石);
                main.autoinfo_.sea_weapon = has_tech_toushi ? 11 : 10;
                main.update_unitinfo(main.autoinfo_, main.is_navy_);
            });
            btn183_person_[10].on_button_pressed(func1);

            //列表项目显示
            if (true)
            {
                func1 = cast<pk::get_item_data_t@>(function(listview, column, row) {
                    switch (column)
                    {
                    case 0:
                        return pk::listview_item(pk::encode(pk::format("{}", row)));
                    case 1:
                        return pk::listview_item(ch::get_person_name(pk::get_person(main.autoarmy_ex_[main.autoitems_[row]].member[0])));
                    case 2:
                        return pk::listview_item(ch::get_person_name(pk::get_person(main.autoarmy_ex_[main.autoitems_[row]].member[1])));
                    case 3:
                        return pk::listview_item(ch::get_person_name(pk::get_person(main.autoarmy_ex_[main.autoitems_[row]].member[2])));
                    case 4:
                        return pk::listview_item(ch::get_weapon_name(main.autoarmy_ex_[main.autoitems_[row]].ground_weapon));
                    case 5:
                        return pk::listview_item(ch::get_weapon_name(main.autoarmy_ex_[main.autoitems_[row]].sea_weapon));
                    case 6:
                        return pk::listview_item(pk::encode(pk::format("{}", main.autoarmy_ex_[main.autoitems_[row]].freq)));
                    default:
                        return pk::listview_item("---");
                    }
                    return pk::listview_item("---");
                });
                lv_.get_item_data(func1);


                func1 = cast<pk::get_comparison_value_t@>(function(listview, column, row, asc) {
                    if (column == 0)
                        return row;
                    if (column == 1)
                        return main.autoarmy_ex_[main.autoitems_[row]].member[0];
                    if (column == 2)
                        return main.autoarmy_ex_[main.autoitems_[row]].member[1];
                    if (column == 3)
                        return main.autoarmy_ex_[main.autoitems_[row]].member[2];
                    if (column == 4)
                        return main.autoarmy_ex_[main.autoitems_[row]].ground_weapon;
                    if (column == 5)
                        return main.autoarmy_ex_[main.autoitems_[row]].sea_weapon;
                    if (column == 6)
                        return main.autoarmy_ex_[main.autoitems_[row]].freq;
                    return 0;
                });
                lv_.get_comparison_value(func1);
            }

            //列表项目选中功能
            func1 = cast<pk::listview_on_item_clicked_t@>(function(a, index, selected) {
                //需要确认选中对象，选中后更新对应的dlg
                if (selected and UI::main.autoparam_.max == 1)//只有单选时触发
                {
                    int row = main.lv_.get_row(index);//UI::main.autoparam_.items[index];
                    //pk::trace("index:" + index + ",id:" + id + "row:" + main.lv_.get_row(index));
                    @main.autoinfo_ = @main.autoarmy_ex_[main.autoitems_[row]];
                    main.update_dlg183(main.autoinfo_, main.is_navy_);
                    //pk::trace("index:" + index + "select_item_id" + UI::main.specparam_.select_item_id[0]);

                }
                return;
            });
            lv_.on_item_clicked(func1);/**/

            //编辑功能
            btn183_edit_[0].set_text(pk::encode("新增组队"));
            btn183_edit_[1].set_text(pk::encode("删除选中"));

            //新增组队
            func1 = cast<pk::button_on_pressed_t@>(function(button) {

                main.write_to_file();
                main.read_from_file();
                int id = main.get_empty_autoarmy_id();
                if (id != -1)
                {
                    main.autoitems_.insertLast(id);
                    pk::trace("新增组队:" + id);

                    main.lv_.set_item_count(main.lv_.get_item_count() + 1);

                    @main.autoinfo_ = @main.autoarmy_ex_[id];

                    main.select_person();

                    main.update_dlg183(main.autoinfo_);
                    //main.lv_.set_item_selected(main.lv_.get_item_count()-1);
                    main.set_sigleitem_selected(main.lv_.get_item_count() - 1);
                }

            });
            btn183_edit_[0].on_button_pressed(func1);//

            //删除选中
            func1 = cast<pk::button_on_pressed_t@>(function(button) {
                if (main.lv_.get_selected_items().length() > 0)
                {
                    if (pk::yes_no(pk::encode("是否删除选中组队?")))
                    {
                        int items_id = main.lv_.get_selected_items()[0];
                        int16 id = main.autoitems_[items_id];
                        main.reset_autoarmy_ex(id);
                        main.lv_.set_item_count(main.lv_.get_item_count() - 1);
                        main.autoitems_.removeAt(items_id);

                        @main.autoinfo_ = @main.autoarmy_ex_[0];
                        main.update_dlg183(main.autoinfo_);
                    }

                }

            });
            btn183_edit_[1].on_button_pressed(func1);
        }

        void update_personinfo(autoarmyinfo@ march_info)
        {
            for (int i = 0; i < 3; i++)
            {
                int person_id = march_info.member[i];
                if (pk::is_valid_person_id(person_id))
                {
                    pk::person@person = pk::get_person(person_id);
                    pk::trace("person_id:" + person_id + "i:" + i + "skill:" + person.skill);
                    //顺便更新武将头像
                    face183_person_[2 - i].set_face(person.face);
                    int skill_id = person.skill;

                    if (pk::is_valid_skill_id2(skill_id))
                    {
                        text183_person_[2 - i].set_text(pk::get_name(pk::get_skill(skill_id)));//特技
                        pk::trace("person_id:" + person_id + "i:" + i + "skill:" + person.skill + pk::decode(pk::get_name(pk::get_skill(skill_id))));
                    }

                    text183_person_[2 - i + 3].set_text(pk::get_name(person));
                }
                else
                {
                    face183_person_[2 - i].set_face(-1);
                    text183_person_[2 - i].set_text("");
                    text183_person_[2 - i + 3].set_text("");
                }
            }

            slider183_.set_value(march_info.freq);

            //武器默认选中
            int btn_id0 = weapon_id_to_btn_id(march_info.ground_weapon);
            pk::left_click(btn183_person_[2 + btn_id0]);
            btn_id0 = weapon_id_to_btn_id(march_info.sea_weapon);
            pk::left_click(btn183_person_[2 + btn_id0]);

            //TODO
            //部队能力切换---√
            //列表只显示已设置数据---√
            //新增组队数据，删除组队数据---√
            //按频次排序---√
            //战法显示异常---√
            //主将设置---√
            //默认选中武将---√
            //输出时默认将按频次排序好的数组给ex---√
            //决定和取消的逻辑调整回来---√
        }

        void update_unitinfo(autoarmyinfo@ march_info, bool navy = false)
        {
            pk::unit_attr@ attr = pk::new_unit_attr();
            int weapon_id = navy ? march_info.sea_weapon : march_info.ground_weapon;
            if (pk::is_valid_person_id(march_info.member[0]))
            {

                pk::list<pk::person@> member_list;
                for (int i = 0; i < 3; i++)
                {
                    int person_id = march_info.member[i];
                    if (pk::is_valid_person_id(person_id))
                    {
                        pk::person@person = pk::get_person(person_id);
                        member_list.add(person);
                    }
                    else member_list.add(null);
                }

                //pk::trace("weapon_id:" + weapon_id + "," + march_info.sea_weapon + "," + march_info.ground_weapon);

                UNIT_ATTR::cal_unit_attr(attr, member_list, weapon_id, 8000, 部队类型_战斗, 部队状态_通常, navy, false);

                array<int> c_max = { 140,140,120,120,50 };
                for (int i = 0; i < 5; ++i)
                {
                    int id = i;
                    if (i < 2) id = i + 5;
                    if (i > 2) id = i + 4;

                    text183_unit_[i * 2 + 1].set_text(pk::encode(pk::format("{}", attr.stat[id])));

                    chart183_.set_value(i, float(float(attr.stat[id]) / c_max[i]));
                }
            }
            else
            {
                for (int i = 0; i < 5; ++i)
                {
                    int id = i;
                    if (i < 2) id = i + 5;
                    if (i > 2) id = i + 4;

                    text183_unit_[i * 2 + 1].set_text("");

                    chart183_.set_value(i, 0);
                }
            }

            //const array<string> textstr = {"部队能力","兵科","剑兵","气力","80","战法","","","","兵种适性",""};

            int best_heishu = (weapon_id == 兵器_剑 or weapon_id == -1) ? 0 : attr.tekisei[pk::equipment_id_to_heishu(weapon_id)];
            text183_unit2_[2].set_text(pk::get_equipment(weapon_id).name);
            //6,7,8 战法， 不能放的战法是灰色
            pk::equipment@ equip = pk::get_equipment(weapon_id);
            array<int> tactics(3, -1);
            array<bool> enabled(3, false);

            int index = 0;
            for (int i = 0; i < 战法_末; ++i)
            {
                if (index == 3) break;
                if (equip.tactics[i])
                {
                    tactics[index] = i;
                    if (pk::get_tactics(i).tekisei <= best_heishu)
                    {
                        pk::trace(pk::format("i:{},tekisei:{},best_heishu:{}", i, pk::get_tactics(i).tekisei, best_heishu));
                        enabled[index] = true;
                    }
                    index += 1;
                }
            }

            for (int i = 0; i < 3; ++i)
            {
                if (tactics[i] == -1)
                {
                    text183_unit2_[6 + i].set_text("");
                    continue;
                }
                else
                {
                    text183_unit2_[6 + i].set_text(pk::get_tactics(tactics[i]).name);
                    if (!enabled[i])text183_unit2_[6 + i].set_text_color(0, 0x80808080);
                    else text183_unit2_[6 + i].set_text_color(0, 0xffffffff);
                }
            }

            text183_unit2_[10].set_text(pk::format("{}", string(pk::core["tekisei"][best_heishu]["name"])));//适性名

        }

        void update_dlg183(autoarmyinfo@ march_info, bool navy = false)
        {
            //if (!pk::is_valid_person_id(march_info.member[0])) return;

            update_personinfo(march_info);

            update_unitinfo(march_info, navy);

        }

        int weapon_id_to_btn_id(int weapon_id)
        {
            switch (weapon_id)
            {
            case 兵器_剑:
            case 兵器_枪:
            case 兵器_戟:
            case 兵器_弩:
            case 兵器_战马:
                return weapon_id;
            case 兵器_冲车:
            case 兵器_木兽:
                return 5;
            case 兵器_井阑:
            case 兵器_投石:
                return 6;
            case 兵器_走舸:
                return 7;
            case 兵器_楼船:
            case 兵器_斗舰:
                return 8;
            default:
                break;
            }
            return -1;
        }

        autoarmyinfo get_update_info()
        {
            autoarmy_ex_[0] = autoinfo_;
            autoarmyinfo info;

            return info;
        }

        void open_dlg(pk::dialog@ dialog, pk::building@ building = @null)
        {

            //update_dlg92(dialog, building, actor92_, choose_weapon_id_);//这里需改成实际值
            //update_dlg181(dlg181_, actor92_);

            //dialog.set_visible(true);

            preset_dlg183(dialog);
            if (false)
            {
                march_info_.type = 0;
                march_info_.member[0] = 武将_关羽;
                march_info_.member[1] = 武将_赵云;
                march_info_.member[2] = 武将_诸葛亮;
                march_info_.weapon_id[0] = 兵器_戟;
                march_info_.weapon_amount[0] = 5000;
            }
            else
            {
                autoarmy_ex_ = autoarmy_ex;//备份数据
                @autoinfo_ = @autoarmy_ex_[0];
            }
            update_dlg183(autoinfo_, is_navy_);
            dialog.open();
            dialog.delete();
        }

        pk::dialog@ create_dlg()
        {
            ///////////////////背景窗体///////////////////
            pk::dialog@ dialog = pk::new_dialog();//pk::get_frame().create_dialog();//

            pk::size dialog_size = pk::size(1170, 650);
            auto buttons = UI::create_dlg_320(dialog, dialog_size, pk::encode("自动组队"));
            pk::button@ ok_button = buttons[0];
            pk::button@ esc_button = buttons[1];

            @autoarmydlg_ = @dialog;
            dialog.set_pos(pk::get_resolution().width / 2 - dialog_size.width / 2, pk::get_resolution().height / 2 - dialog_size.height / 2 - 60);
            //dialog.set_size(dialog_size);
            pk::detail::funcref func0 = cast<pk::button_on_released_t@>(function(button) {
                //应用修改不需要设定，返回需要设定回原值
                //main.autoarmydlg_.set_visible(false);
                main.sort_freq();//调整后默认将排序的数组传回，方便出征时的遍历
                autoarmy_ex = main.autoarmy_ex_;
                main.autoarmydlg_.close(1);
                //main.autoarmydlg_.delete();
            });
            ok_button.on_button_released(func0);

            func0 = cast<pk::button_on_released_t@>(function(button) {
                //应用修改不需要设定，返回需要设定回原值
                main.autoarmydlg_.close(1);

            });
            esc_button.on_button_released(func0);

            ///////////////////武将信息///////////////////
            pk::dialog@ dialog1 = dialog.create_dialog(false);
            pk::size dialog_size1 = pk::size(527 + 32, 316);//y331-68
            dialog1.set_pos(16, 263 + 16 + 60);
            dialog1.set_size(dialog_size1);
            init_dlg_from_arr(dialog1, person_info_data, 0);

            //init_dlg_by_id(dialog, 160);
            pk::dialog@ dialog2 = dialog.create_dialog(false);
            pk::size dialog_size2 = pk::size(298, 154);
            dialog2.set_pos(99 + 16, 42 + 60);
            dialog2.set_size(dialog_size2);

            init_dlg_by_id(dialog2, 183);//初始位置有问题

            ///////////////////兵器选择///////////////////
            //auto bg = dialog.create_sprite(384);
            //bg.set_pos();
            //bg.set_size();

            //auto tx0 = dialog.create_text();
            //tx0.set_pos();
            //tx0.set_size();

            ///////////////////部队信息///////////////////
            pk::dialog@ dialog3 = dialog.create_dialog(false);
            pk::size dialog_size3 = pk::size(973 - 564 + 16 + 124, 263 + 16);//973-564,263
            dialog3.set_pos(16, 0 + 60);
            dialog3.set_size(dialog_size3);

            init_dlg_from_arr(dialog3, unit_info_data, 1);


            ///////////////////组队选择///////////////////
            autoitems_.resize(组队数_末);
            int count = 0;
            for (int16 i = 0; i < 组队数_末; ++i)
            {
                if (pk::is_valid_person_id(autoarmy_ex[i].member[0]))//create时数据还没转换，故先用全局的
                {
                    autoitems_[count] = i;
                    count += 1;
                }
            }
            autoitems_.resize(count);
            array<int16> selected_items(1, -1);
            // selected_items[0] = autoitems_[0];
            // 未有数据时不设定，防止数据溢出
            if (autoitems_.length > 0) selected_items[0] = autoitems_[0];
            pk::listview@ lv = UI::autoarmy_listview(dialog, pk::point(659, 0 + 60), pk::size(513, 562), pk::encode("组队选择"), pk::encode("组队选择"), autoitems_, 1, 1, selected_items, true);
            @lv_ = @lv;

            //auto arr = main.lv_.get_selected_items();
            //int16 select_item_id = main.autoparam_.items[int16(arr[i])];

            ///////////////////中间按钮///////////////////
            init_dlg_from_arr(dialog, middle_info_data, 2);

            return dialog;
        }

        //此函数用于设置通用基础属性及储存组件信息
        pk::dialog@ init_dlg_by_id(pk::dialog@ new_dialog, int dlg_id)
        {
            int start_id = pk::get_dlg_childstart(dlg_id);
            int child_count = pk::get_dlg_childcount(dlg_id);

            int text_index = 0;
            int button_index = 0;
            //7823-8037
            for (int i = 0; i < child_count; ++i)
            {
                pk::dlgitem@ item0 = pk::get_dlgitem(dlg_id, i);
                pk::dlgitem@ item1 = @null;
                pk::dlgitem@ item2 = @null;
                pk::widget@ widget = new_dialog.create_widget(item0.type, item0.texture_start);
                int relid = item0.relative_id;
                int relid2 = -1;
                if (relid != -1)
                {
                    //pk::trace(i + "," + relid + "," + start_id);
                    @ item1 = pk::get_dlgitem(dlg_id, relid - start_id);
                    relid2 = item1.relative_id;
                }
                if (relid2 != -1)
                {
                    @ item2 = pk::get_dlgitem(dlg_id, relid2 - start_id);
                }
                int x = item0.pos.x + ((relid == -1) ? 0 : ((relid2 == -1) ? item1.pos.x : item1.pos.x + item2.pos.x));
                int y = item0.pos.y + ((relid == -1) ? 0 : ((relid2 == -1) ? item1.pos.y : item1.pos.y + item2.pos.y));
                if (widget !is null)
                {
                    //pk::trace(pk::format("{},{},{},{},0x{:x}", x, y, width, height, widget.get_p()));
                    widget.set_pos(x, y);
                    widget.set_size(item0.size);

                    int type_id = item0.type;
                    if (type_id == 5 or type_id == 8)
                    {
                        //@btn183_unit2_[button_index] = @widget;
                        button_index += 1;
                    }
                    else if (type_id == 6)
                    {

                        @text183_unit_[text_index] = @widget;
                        text_index += 1;
                    }

                    //183特殊设置
                    if (i == 28)
                    {
                        pk::chart@ chart = new_dialog.create_chart();
                        chart.set_pos(x + 56, y + 56);
                        chart.set_scale(0.56);
                        chart.set_value(0, 1);
                        chart.set_value(1, 0.7);
                        chart.set_value(2, 0.8);
                        chart.set_value(3, 0.9);
                        chart.set_value(4, 1);
                        @chart183_ = @chart;
                    }
                }
            }
            return new_dialog;
        }

        pk::dialog@ init_dlg_from_arr(pk::dialog@ new_dialog, array<array<int>>arr, int dlg_type = -1)
        {

            int id = 0;
            int text_index = 0;
            int button_index = 0;
            int custom_index = 0;
            for (int i = 0; i < int(arr.length()); ++i)
            {
                pk::widget@ widget = new_dialog.create_widget(arr[i][1], arr[i][3]);
                int parent = arr[i][2];

                int x = arr[i][5] + ((parent == -1) ? 0 : arr[i + arr[i][2] - arr[i][0]][5]);
                int y = arr[i][6] + ((parent == -1) ? 0 : arr[i + arr[i][2] - arr[i][0]][6]);
                int width = arr[i][7];
                int height = arr[i][8];


                if (widget !is null)
                {
                    widget.set_pos(x, y);
                    widget.set_size(width, height);

                    int type_id = arr[i][1];
                    if (dlg_type == 0)//person
                    {
                        if (type_id == 5 or type_id == 8)
                        {
                            @btn183_person_[button_index] = @widget;
                            button_index += 1;
                        }
                        else if (type_id == 6)
                        {

                            @text183_person_[text_index] = @widget;
                            text183_person_[text_index].set_text_align(TEXT_ALIGN_CENTER);
                            text_index += 1;
                        }

                    }
                    else if (dlg_type == 1)//unit
                    {

                        if (type_id == 5 or type_id == 8)
                        {
                            @btn183_unit2_[button_index] = @widget;
                            button_index += 1;
                        }
                        else if (type_id == 6)
                        {

                            @text183_unit2_[text_index] = @widget;
                            text_index += 1;
                        }
                    }
                    else if (dlg_type == 2)
                    {
                        if (type_id == 5 or type_id == 8)
                        {
                            @btn183_edit_[button_index] = @widget;
                            button_index += 1;
                        }

                    }

                }
                else
                {
                    int type_id = arr[i][1];
                    if (dlg_type == 0)//person
                    {
                        if (type_id == 7)
                        {
                            if (custom_index < 3)
                            {
                                pk::trace("set_face");
                                pk::face@ face = new_dialog.create_face();
                                face.set_pos(x, y);
                                face.set_face_size(pk::size(width, height));
                                face.set_face_type(1, 0);

                                @face183_person_[custom_index] = @ face;
                                custom_index += 1;
                            }
                            else
                            {
                                pk::slider@ slider = new_dialog.create_slider();
                                slider.set_pos(x, y);
                                slider.set_size(width, height);
                                slider.set_min_max(0, 9999);
                                slider.set_value(0);
                                @slider183_ = @slider;
                                pk::detail::funcref funcslider = cast<pk::slider_on_value_changed_t@>(function(self, value) {
                                    main.text183_person_[8].set_text(pk::format("{}", value));
                                    main.autoinfo_.freq = value;
                                });
                                slider.on_slider_value_changed(funcslider);
                            }

                        }
                    }

                }

            }

            if (dlg_type == 0)//person
            {
                //设置武将选择按钮功能      
                array<int> btnid(7, -1);
                for (int i = 2; i < 9; ++i)
                {
                    btnid[i - 2] = btn183_person_[i].get_id();
                    pk::trace("btnid" + btnid[i - 2]);
                }
                new_dialog.add_button_group(btnid);

                new_dialog.add_button_group({ btn183_person_[9].get_id(),btn183_person_[10].get_id() });
            }
            else if (dlg_type == 1)//unit
            {
                new_dialog.add_button_group({ btn183_unit2_[0].get_id(),btn183_unit2_[1].get_id() });
            }

            return new_dialog;
        }

        void select_person()
        {
            pk::list<pk::person@ >person_list = pk::get_person_list(pk::get_force(pk::get_current_turn_force_id()), pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般));
            autoarmydlg_.set_visible(false);
            //初始选中武将
            pk::list<pk::person@ >pre_sel;
            for (int i = 0; i < 3; ++i)
            {
                int person_id = autoinfo_.member[i];
                if (pk::is_valid_person_id(person_id))
                {
                    pk::person@ person = pk::get_person(person_id);
                    pre_sel.add(person);
                }
            }

            pk::list<pk::person@ >person_sel = pk::person_selector(pk::encode("选择武将"), pk::encode("选择自动组队的武将"), person_list, 1, 3, pre_sel, 37);
            person_sel.sort(function(a, b) //
            {
                return pk::leader_compare(a, b); // 主将选择
            });
            for (int i = 0; i < person_sel.count; ++i)
            {
                autoinfo_.member[i] = person_sel[i].get_id();
            }
            update_dlg183(main.autoinfo_, main.is_navy_);
            autoarmydlg_.set_visible(true);
        }

        bool set_freq_num()
        {
            pk::int_bool numpad = pk::numberpad(pk::encode("设定频次"), 0, 9999, slider183_.get_value(), pk::numberpad_t(pad_callback));

            if (!numpad.second) return false;
            slider183_.set_value(numpad.first);
            return true;
        }

        void set_sigleitem_selected(int index)
        {
            int count = lv_.get_item_count();
            for (int i = 0; i < count; ++i)
            {
                bool selected = false;
                if (i == index) selected = true;
                lv_.set_item_selected(i, selected);
            }
        }

        int get_empty_autoarmy_id()
        {
            for (int i = 0; i < 组队数_末; ++i)
            {
                if (!pk::is_valid_person_id(autoarmy_ex_[i].member[0])) return i;
            }
            return -1;
        }

        void sort_freq()
        {
            autoarmy_ex_.sort(function(a, b) { return a.freq > b.freq; });
        }

        void reset_autoarmy_ex(int id)
        {
            autoarmy_ex_[id].member[0] = -1;
            autoarmy_ex_[id].member[1] = -1;
            autoarmy_ex_[id].member[2] = -1;
            autoarmy_ex_[id].ground_weapon = 0;
            autoarmy_ex_[id].sea_weapon = 9;
            autoarmy_ex_[id].freq = 0;
        }

        void write_to_file()
        {
            string path = get_path('autoarmy' + '.data');
            f.open(path, "w");

            for (int i = 0; i < 组队数_末; i++)
            {
                autoarmy_ex[i].update(i);
                for (int j = 0; j < 组队结构体_int32数; j++)
                    f.writeUInt(autoarmy_ex_info_temp[j][i], 8);
            }


            //f.writeUInt(888,8);
            f.close();
        }

        void read_from_file()
        {
            string path = get_path('autoarmy' + '.data');
            f.open(path, "r");

            for (int i = 0; i < 组队数_末; i++)
            {
                for (int j = 0; j < 组队结构体_int32数; j++)
                    autoarmy_ex_info_temp[j][i] = uint32(f.readUInt(8));
                //autoarmy_ex_info_temp[j][i] = int32(pk::load(KEY_其他, (KEY_索引_追加_组队起始 + (i * 组队结构体_int32数 + j)), 0));
            //autoarmyinfo autoarmy_t(i);
            //autoarmy_ex[i] = autoarmy_t;
            }

            f.close();
        }

        string get_path(string fileName)
        {
            return '../pk2.2/Koei/San11/SaveData/' + fileName;
        }

        //----------------------------组队调整的支持函数------------------------------------------

        pk::list<pk::person@ > get_target_person_list(int weapon, array<array<int>> arr)
        {
            pk::list<pk::person@ > temp_list;
            for (int i = 0; i < int(arr.length); ++i)
            {
                if (arr[i][3] == weapon)
                {
                    temp_list.add(pk::get_person(arr[i][0]));
                }
            }
            return temp_list;
        }

        array<array<int>> get_target_weapon_unit_arr(int weapon, array<array<int>> arr)
        {
            array<array<int>> temp_arr;
            for (int i = 0; i < int(arr.length); ++i)
            {
                if (arr[i][3] == weapon)
                {
                    if (调试模式) pk::trace(pk::format("arraypos:{},{}", i, arr[i][3]));
                    array<int> temp = arr[i];
                    temp.insertLast(i + 1);//插入数组原来的序号，方便后面排序
                    temp_arr.insertLast(temp);
                }
            }
            if (调试模式) pk::trace(pk::format("end arr", 1));
            return temp_arr;
        }

        pk::list<pk::person@ > get_person_sel(pk::list<pk::person@ > person_list)
        {
            pk::list<pk::person@ >person_list_t = pk::person_selector(pk::encode("选择对象主将"), pk::encode("选择需要调整的主将."), person_list, 1, 1);
            return person_list_t;
        }
        //进行此函数时，传入的数组应该已经限定好武器了
        array<array<int>> get_target_unit_arr(int leader_id, array<array<int>> arr)
        {
            array<array<int>> temp_arr;
            for (int i = 0; i < int(arr.length); ++i)
            {
                if (arr[i][0] == leader_id)
                {
                    if (调试模式) pk::trace(pk::format("array2pos:{},{}", i, arr[i][3]));
                    array<int> temp = arr[i];
                    temp.insertLast(i);//插入在选择好武器数组的位置序号，方便后面寻找
                    temp_arr.insertLast(temp);
                }
            }
            if (调试模式) pk::trace(pk::format("end arr2", 1));
            return temp_arr;
        }

        string get_weapon_name(int weapon_id)
        {
            string weapon_name;
            switch (weapon_id)
            {
            case 兵器_剑: weapon_name = "剑兵"; break;
            case 兵器_枪: weapon_name = "枪兵"; break;
            case 兵器_戟: weapon_name = "戟兵"; break;
            case 兵器_弩: weapon_name = "弩兵"; break;
            case 兵器_战马: weapon_name = "骑兵"; break;
            case 兵器_冲车: weapon_name = "冲车"; break;
            case 兵器_井阑: weapon_name = "井阑"; break;
            case 兵器_投石: weapon_name = "投石"; break;
            case 兵器_木兽: weapon_name = "木兽"; break;
            case 兵器_走舸: weapon_name = "水军"; break;
            case 兵器_楼船: weapon_name = "水军"; break;
            case 兵器_斗舰: weapon_name = "水军"; break;
            default: weapon_name = "??"; break;
            }
            return weapon_name;
        }

        void unit_choose_0(array<string> unit_info_arr, array<array<int>> arr, array<array<int>> arr2)
        {
            if (调试模式) pk::trace(pk::format("choose0", 2));
            int n = pk::choose(pk::encode("请选择要更改优先级的队伍"), unit_info_arr);
            if (n != 5)
            {
                reset_priority(arr[n][7], arr2);
            }
            else
            {
                if (arr.length < 7)
                {
                    reset_priority(arr[n][7], arr2);
                }
                else
                {
                    unit_choose_1(unit_info_arr, arr, arr2);
                }
            }
            if (调试模式) pk::trace(pk::format("choose0-2", 2));
        }

        void unit_choose_1(array<string> unit_info_arr, array<array<int>> arr, array<array<int>> arr2)
        {
            if (调试模式) pk::trace(pk::format("choose1", 2));
            int n = pk::choose(pk::encode("请选择要更改优先级的队伍"), unit_info_arr);
            if ((n + 6) != int(unit_info_arr.length)) //不是最后一项的处理
            {
                reset_priority(arr[n + 6][7], arr2);
            }
            else
            {
                //最后一项的处理
                unit_choose_0(unit_info_arr, arr, arr2);
            }
            if (调试模式) pk::trace(pk::format("choose1-2", 2));
        }

        void reset_priority(int pos, array<array<int>> arr2)
        {
            pk::message_box(pk::encode(pk::format("当前部队的优先级为:{},频次为：{}。\n请选择输入调整后的优先级。", pos, arr2[pos][5])));
            int def_value = pk::numberpad(pk::encode("优先级"), 1, arr2.length, pos, pk::numberpad_t(pad_callback)).first;

            //优先级和当前优先级相同的情况的处理

            //调整优先级,实际上是获取n-1位的频次，然后选择的项目的频次变为目标优先级项目的频次+1，然后重新排序
            int id = arr2[pos][6] - 1;//获取所在序号
            int id2 = arr2[def_value - 1][6] - 1;//获取目标所在序号
            if (调试模式) pk::trace(pk::format("id:{},id2{},数组长度：{}", id, id2, 记忆组队数组.length));
            记忆组队数组[id][5] = 记忆组队数组[id2][5];//选择的项目的频次变为目标优先级项目的频次 然后插入到目标上方

            if (id > id2)
            {
                array<int> temp = 记忆组队数组[id];
                记忆组队数组.removeAt(id);
                记忆组队数组.insertAt(id2, temp);
            }
            else if (id < id2)
            {
                array<int> temp = 记忆组队数组[id];
                记忆组队数组.insertAt(id2, temp);
                记忆组队数组.removeAt(id);
            }

            for (int i = 0; i < int(记忆组队数组.length); ++i)
            {
                if (i >= 300) break;
                autoarmyinfo@ army_info = @autoarmy_ex[i];
                army_info.member[0] = 记忆组队数组[i][0];
                army_info.member[1] = 记忆组队数组[i][1];
                army_info.member[2] = 记忆组队数组[i][2];
                army_info.ground_weapon = 记忆组队数组[i][3];
                army_info.sea_weapon = 记忆组队数组[i][4];
                army_info.freq = 记忆组队数组[i][5];
            }
            bool confirm = pk::yes_no(pk::encode("设定完成，是否继续下一个设定？"));
            if (confirm) handler_组队调整();
        }

        void set_priority(array<array<int>> arr2, array<int> insert_arr)
        {
            pk::message_box(pk::encode("请输入优先级。"));
            int def_value = pk::numberpad(pk::encode("优先级"), 1, pk::max(arr2.length, 1), 1, pk::numberpad_t(pad_callback)).first;

            //优先级和当前优先级相同的情况的处理

            //调整优先级,实际上是获取n-1位的频次，然后选择的项目的频次变为目标优先级项目的频次+1，然后重新排序
            int id = 0;
            if (arr2.length() > 0) id = arr2[def_value - 1][6] - 1;//获取目标所在序号

            记忆组队数组.insertAt(id, insert_arr); //直接插入

            //信息同步
            for (int i = 0; i < int(记忆组队数组.length); ++i)
            {
                if (i >= 300) break;
                autoarmyinfo@ army_info = @autoarmy_ex[i];
                army_info.member[0] = 记忆组队数组[i][0];
                army_info.member[1] = 记忆组队数组[i][1];
                army_info.member[2] = 记忆组队数组[i][2];
                army_info.ground_weapon = 记忆组队数组[i][3];
                army_info.sea_weapon = 记忆组队数组[i][4];
                army_info.freq = 记忆组队数组[i][5];
            }
            bool confirm = pk::yes_no(pk::encode("设定完成，是否继续下一个设定？"));
            if (confirm) handler_新增组队设定();
        }

        pk::list<pk::person@ > get_deputy_list(pk::person@leader)
        {
            int force_id = leader.get_force_id();
            int leader_id = leader.get_id();
            pk::list<pk::person@ > temp_list;
            for (int i = 0; i < 非贼武将_末; ++i)
            {
                if (leader_id != i)
                {
                    pk::person@ deputy = pk::get_person(i);
                    if (deputy.get_force_id() == force_id)
                    {
                        temp_list.add(deputy);
                    }
                }
            }
            return temp_list;
        }

        string pad_callback(int line, int original_value, int current_value)
        {

            return pk::encode("输入数字");  // 
        }
        //---------------------------------------------------------------------------------------


        //int weapon_heishu = -1;

        //pk::int_int_int callback(int person_id)
        //pk::int_int_int_int_int_int callback(int leader, int member1, int member2, int troops, int ground_weapon, int sea_weapon)
        void callback(pk::march_cmd_info& in info)
        {
            if (info.member[0] == -1) return;
            if (info.base is null) return;
            bool trace0 = true;
            if (trace0) pk::trace(pk::format("pos1,info.member[0]:{},info.member[1]:{}", ch::get_person_name(info.member[0]), ch::get_person_name(info.member[1])));
            pk::building@base = @info.base;

            pk::list<pk::person@> idle_person_list = pk::get_idle_person_list(base);//pk::get_person_list(base, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般));
            if (idle_person_list.count == 0) return;
            int weapon_heishu = pk::equipment_id_to_heishu(info.weapon_id[0]);
            pk::int_int_int info1_t = pk::int_int_int(-1, -1, -1);//暂存搜索到的信息
            if (采用记忆组队)
            {
                int leader_id = -1;
                int member_id1 = -1;
                int member_id2 = -1;
                bool finished = false;
                array<array<int>> a(组队数_末, { -1,-1,-1,兵器_剑,兵器_走舸,0 });
                记忆组队数组 = a;
                for (int i = 0; i < 组队数_末; ++i)
                {
                    pk::trace("i:" + i);
                    ex_to_array(i);
                    if (trace0) pk::trace(pk::format("pos0,i:{},数组武器：{}，编制武器：{} ", i, pk::decode(ch::get_weapon_name(记忆组队数组[i][3])), pk::decode(ch::get_weapon_name(info.weapon_id[0]))));
                    if (记忆组队数组[i][0] == -1) break; //无主将时说明到末尾，退出循环
                    if (记忆组队数组[i][3] != info.weapon_id[0]) continue;//如果部队兵装不符，不进入判断
                    //pk::trace(pk::format("pos1,i:{}", i));
                    leader_id = 记忆组队数组[i][0];
                    for (int j = 0; j < idle_person_list.count; ++j)
                    {
                        if (trace0) pk::trace(pk::format("pos2,i:{},j:{}", i, j));
                        if (idle_person_list[j].get_id() == leader_id)
                        {
                            //开始搜索副将
                            int person_count = 记忆组队数组[i][1] == -1 ? 1 : (记忆组队数组[i][2] == -1 ? 2 : 3);
                            if (trace0) pk::trace(pk::format("pos2,i:{},j:{},找到主将id：{},部队人数：{}", i, j, leader_id, person_count));
                            if (person_count == 1)
                            {
                                finished = true;
                                info1_t.first = leader_id;
                                break;//退出到记忆组队数组那个循环
                            }

                            member_id1 = 记忆组队数组[i][1];
                            member_id2 = 记忆组队数组[i][2];
                            int 副将计数 = 0;
                            int member1_t = -1;
                            for (int k = 0; k < idle_person_list.count; ++k)
                            {
                                if (trace0) pk::trace(pk::format("pos2,i:{},j:{},k:{},目标副将id：{}，{},目前的id：{}", i, j, k, member_id1, member_id2, idle_person_list[k].get_id()));
                                if (idle_person_list[k].get_id() == leader_id) continue;//跳过重复的

                                if (idle_person_list[k].get_id() == member_id1 or idle_person_list[k].get_id() == member_id2)
                                {
                                    if (person_count == 2)
                                    {
                                        finished = true;
                                        info1_t.first = idle_person_list[j].get_id();
                                        info1_t.second = idle_person_list[k].get_id();
                                        if (trace0) pk::trace(pk::format("找到一个副将退出循环", 1));
                                        break;//退出寻找副将的循环
                                    }
                                    else
                                    {
                                        //两个副将的情况
                                        副将计数 += 1;
                                        if (副将计数 == 1)
                                        {
                                            member1_t = idle_person_list[k].get_id();
                                            continue;
                                        }
                                        else
                                        {
                                            info1_t.first = idle_person_list[j].get_id();
                                            info1_t.second = member1_t;
                                            info1_t.third = idle_person_list[k].get_id();
                                            finished = true;
                                            break;//退出寻找副将的循环
                                        }

                                    }
                                }
                            }

                            if (finished) break;//退出寻找主将的循环
                        }
                    }
                    if (finished) break;
                }

                if (info1_t.first != -1)
                {
                    //temp.leader = info1_t.first;
                    //temp.deputy1 = info1_t.second;
                    //temp.deputy2 = info1_t.third;
                    info.member[0] = info1_t.first;
                    info.member[1] = info1_t.second;
                    info.member[2] = info1_t.third;
                    //info.member = member;
                    //pk::trace(pk::format("上传武将id，武将1,{}。武将2：{}，武将3：{}", info1_t.first, info1_t.second, info1_t.third));
                    //pk::trace(pk::format("接受武将id，武将1,{}。武将2：{}，武将3：{}", info1.first, info1.second, info1.third));
                    return;
                }
                else return;

                //还需根据获取的武将组合配给兵力
            }
        }

        void array_to_ex(int index)
        {
            autoarmy_ex_[index].member[0] = 记忆组队数组[index][0];
            autoarmy_ex_[index].member[1] = 记忆组队数组[index][1];
            autoarmy_ex_[index].member[2] = 记忆组队数组[index][2];
            autoarmy_ex_[index].ground_weapon = 记忆组队数组[index][3];
            autoarmy_ex_[index].sea_weapon = 记忆组队数组[index][4];
            autoarmy_ex_[index].freq = 记忆组队数组[index][5];
        }

        void ex_to_array(int index)
        {
            记忆组队数组[index][0] = autoarmy_ex_[index].member[0];
            记忆组队数组[index][1] = autoarmy_ex_[index].member[1];
            记忆组队数组[index][2] = autoarmy_ex_[index].member[2];
            记忆组队数组[index][3] = autoarmy_ex_[index].ground_weapon;
            记忆组队数组[index][4] = autoarmy_ex_[index].sea_weapon;
            记忆组队数组[index][5] = autoarmy_ex_[index].freq;
        }

        void callback22(pk::int_int_int& info1, pk::int_int_int& info2)
        {
            pk::person@ leader = pk::get_person(info1.first);
            pk::building@ base = pk::get_building(leader.service);
            pk::list<pk::person@> idle_person_list = pk::get_idle_person_list(base);//pk::get_person_list(base, pk::mibun_flags(身份_君主, 身份_都督, 身份_太守, 身份_一般));
            if (idle_person_list.count == 0) return;
            int weapon_heishu = pk::equipment_id_to_heishu(info2.second);

            //武将数据的暂存数组
            pk::int_int_int info1_t = pk::int_int_int(-1, -1, -1);
            pk::int_int_int info2_t = pk::int_int_int(-1, -1, -1);
            //根据特技组合选择组队？
            //根据武将id组合选择组队？
            //官职高的必定是主将
            if (采用记忆组队)
            {
                int leader_id = -1;
                int member_id1 = -1;
                int member_id2 = -1;
                bool finished = false;
                for (int i = 0; i < int(记忆组队数组.length); ++i)
                {
                    //pk::trace(pk::format("pos0,i:{},数组武器：{}，编制武器：{} ", i, 记忆组队数组[i][3], info2.second));
                    if (记忆组队数组[i][0] == -1) break; //无主将时说明到末尾，退出循环
                    if (记忆组队数组[i][3] != info2.second) continue;//如果部队兵装不符，不进入判断
                    //pk::trace(pk::format("pos1,i:{}", i));
                    leader_id = 记忆组队数组[i][0];
                    for (int j = 0; j < idle_person_list.count; ++j)
                    {
                        //pk::trace(pk::format("pos2,i:{},j:{}", i,j));
                        if (idle_person_list[j].get_id() == leader_id)
                        {

                            //开始搜索副将
                            int person_count = 记忆组队数组[i][1] == -1 ? 1 : (记忆组队数组[i][2] == -1 ? 2 : 3);
                            //pk::trace(pk::format("pos2,i:{},j:{},找到主将id：{},部队人数：{}", i, j, leader_id,person_count));
                            if (person_count == 1)
                            {
                                finished = true;
                                info1_t.first = leader_id;
                                break;//退出到记忆组队数组那个循环
                            }

                            member_id1 = 记忆组队数组[i][1];
                            member_id2 = 记忆组队数组[i][2];
                            int 副将计数 = 0;
                            int member1_t = -1;
                            for (int k = 0; k < idle_person_list.count; ++k)
                            {
                                //pk::trace(pk::format("pos2,i:{},j:{},k:{},目标副将id：{}，{},目前的id：{}", i, j,k, member_id1, member_id2, idle_person_list[k].get_id()));
                                if (idle_person_list[k].get_id() == leader_id) continue;//跳过重复的

                                if (idle_person_list[k].get_id() == member_id1 or idle_person_list[k].get_id() == member_id2)
                                {
                                    if (person_count == 2)
                                    {
                                        finished = true;
                                        info1_t.first = idle_person_list[j].get_id();
                                        info1_t.second = idle_person_list[k].get_id();
                                        //pk::trace(pk::format("找到一个副将退出循环", 1));
                                        break;//退出寻找副将的循环
                                    }
                                    else
                                    {
                                        //两个副将的情况
                                        副将计数 += 1;
                                        if (副将计数 == 1)
                                        {
                                            member1_t = idle_person_list[k].get_id();
                                            continue;
                                        }
                                        else
                                        {
                                            info1_t.first = idle_person_list[j].get_id();
                                            info1_t.second = member1_t;
                                            info1_t.third = idle_person_list[k].get_id();
                                            finished = true;
                                            break;//退出寻找副将的循环
                                        }

                                    }
                                }
                            }

                            if (finished) break;//退出寻找主将的循环
                        }
                    }
                    if (finished) break;
                }

                if (info1_t.first != -1)
                {
                    info1 = info1_t;
                    //pk::trace(pk::format("上传武将id，武将1,{}。武将2：{}，武将3：{}", info1_t.first, info1_t.second, info1_t.third));
                    //pk::trace(pk::format("接受武将id，武将1,{}。武将2：{}，武将3：{}", info1.first, info1.second, info1.third));
                    return;
                }
                else return;

                //还需根据获取的武将组合配给兵力
            }



            //pk::trace(pk::format("outside:leader:{}, member1:{}, member2:{}, troops:{}, ground_weapon:{}, sea_weapon:{}", info1.first, info1.second, info1.third, info2.first, info2.second, info2.third));
            //pk::trace(pk::format("outside:leader:{}, member1:{}, member2:{}, troops:{}, ground_weapon:{}, sea_weapon:{}", leader, member1, member2, troops, ground_weapon, sea_weapon));

            //info1.first = 武将_关羽;
            //info1.second = 武将_张飞;
            //info1.third = 武将_诸葛亮;
            //return;
            return;
        }


        void 每月记录部队()
        {
            if (pk::is_campaign()) return;
            if (pk::get_player_count() < 1) return;
            if (调试模式) pk::trace("start autoarmy");
            pk::force@ player_force;
            for (int i = 0; i < 非贼势力_末; ++i)
            {
                if (pk::get_force(i).player == 0)  @player_force = pk::get_force(i);
            }

            pk::list<pk::unit@> unit_list = pk::get_unit_list(player_force);
            array<array<int>> temp_arr;
            for (int i = 0; i < 300; ++i)
            {
                autoarmyinfo@ army_info = @autoarmy_ex[i];
                if (army_info.member[0] == -1) break;//-1表示已经到末尾
                if (army_info.member[0] == 0 and army_info.member[1] == 0) break;
                array<int> arr_t = { army_info.member[0], army_info.member[1], army_info.member[2], army_info.ground_weapon, army_info.sea_weapon, army_info.freq };
                temp_arr.insertLast(arr_t);
            }
            记忆组队数组 = temp_arr;


            for (int i = 0; i < unit_list.count; ++i)
            {
                bool same_unit = false;
                if (unit_list[i].type == 部队类型_战斗)
                {
                    for (int j = 0; j < int(记忆组队数组.length); ++j)
                    {
                        //pk::trace(pk::format("same juge :,unit weapon:{}.记忆组队数组weapon:{}", unit_list[i].weapon, 记忆组队数组[j][3]));
                        //暂时只考虑陆地兵种
                        if (unit_list[i].weapon == 记忆组队数组[j][3])
                        {
                            //pk::trace(pk::format("same juge :,unit member1:{}.member2{}.member3{}", unit_list[i].member[0], unit_list[i].member[1], unit_list[i].member[1]));
                            //pk::trace(pk::format("same juge :,记忆组队数组 member1:{}.member2{}.member3{}", 记忆组队数组[j][0], 记忆组队数组[j][1], 记忆组队数组[j][2]));
                            //相同的情况
                            if (unit_list[i].member[0] == 记忆组队数组[j][0] and ((unit_list[i].member[1] == 记忆组队数组[j][1] and unit_list[i].member[2] == 记忆组队数组[j][2]) or (unit_list[i].member[1] == 记忆组队数组[j][2] and unit_list[i].member[2] == 记忆组队数组[j][1])))
                            {
                                //pk::trace("pos same");
                                记忆组队数组[j][5] += 1;
                                //当场排序
                                if (j != 0)
                                {
                                    for (int k = (j - 1); k > -1; --k)
                                    {
                                        if (记忆组队数组[j][5] < 记忆组队数组[k][5] and (k + 1) != j)
                                        {
                                            array<int> temp = 记忆组队数组[j];
                                            记忆组队数组[j] = 记忆组队数组[k + 1];
                                            记忆组队数组[k + 1] = temp;
                                            break;
                                        }
                                    }
                                }

                                same_unit = true;
                                //pk::trace(pk::format("same :,member1:{}.member2{}", unit_list[i].member[0], unit_list[i].member[1]));
                                break;//退出记忆组队数组的循环
                            }
                        }

                    }
                }
                if (!same_unit and (unit_list[i].weapon > 0 and unit_list[i].weapon < 9))
                {
                    array<int> unit_infomation = { unit_list[i].member[0], unit_list[i].member[1], unit_list[i].member[2], unit_list[i].weapon, 兵器_走舸, 1 };

                    //pk::trace(pk::format("not same i:{},member1:{}.member2{}.weapon:{}", i, unit_list[i].member[0], unit_list[i].member[1], unit_list[i].weapon));
                    记忆组队数组.insertLast(unit_infomation);
                }
            }

            //for (int i = 0; i < 记忆组队数组.length; ++i)
            //{
            //    pk::trace(pk::format("id:{},member1:{}.member2{}, weapon:{}, freq:{}", i, 记忆组队数组[i][0], 记忆组队数组[i][1], 记忆组队数组[i][3], 记忆组队数组[i][5]));
            //}

            array<array<int>> temp_arr2;
            for (int i = 0; i < int(记忆组队数组.length); ++i)
            {
                if (i >= 300) break;
                autoarmyinfo@ army_info = @autoarmy_ex[i];
                army_info.member[0] = 记忆组队数组[i][0];
                army_info.member[1] = 记忆组队数组[i][1];
                army_info.member[2] = 记忆组队数组[i][2];
                army_info.ground_weapon = 记忆组队数组[i][3];
                army_info.sea_weapon = 记忆组队数组[i][4];
                army_info.freq = 记忆组队数组[i][5];
            }
            if (调试模式) pk::trace("end autoarmy");
        }


        /*
        // 무장 능력치 계산식
        int get_stat_sum(pk::person@ person_t, bool include_wisdom)
        {
            int stat = int(武将变更_统率权重 * person_t.stat[武将能力_统率]
                + 武将变更_武力权重 * person_t.stat[武将能力_武力]);

            if (include_wisdom)
                stat += int(武将变更_智力权重 * person_t.stat[武将能力_智力]);

            return stat;
        }

        // 무장 적성 능력가중치 환산
        int get_stat_tekisei(pk::person@ person_t)
        {
            int stat = 0;
            int tekisei = person_t.tekisei[weapon_heishu];
            if (武将变更_启用适性标准)
            {
                int stat_base = (tekisei > pk::max(0, pk::min(3, 武将变更_适性基准值))) ? 30 : 0;
                stat = int(武将变更_适性权重 * (stat_base + 15 * pk::min(3, tekisei)));
            }
            return stat;
        }

        // 부대 적성 능력가중치 환산
        int get_stat_unit_tekisei(pk::unit@ unit_t)
        {
            int stat = 0;
            int tekisei = pk::get_tekisei(unit_t, unit_t.weapon);
            if (武将变更_启用适性标准)
            {
                int stat_base = (tekisei > pk::max(0, pk::min(3, 武将变更_适性基准值))) ? 30 : 0;
                stat = int(武将变更_适性权重 * (stat_base + 15 * pk::min(3, tekisei)));
            }
            return stat;
        }

        // 武将特技权重换算무장 특기 가중치 환산
        int get_stat_skill(pk::person@ person_t)
        {
            int stat = 0;
            if (武将变更_启用特技加权)
            {
                if (ch::has_skill(person_t, 特技_飞将)) stat = pk::max(5, stat);
                if (ch::has_skill(person_t, 特技_霸王)) stat = pk::max(5, stat);
                if (ch::has_skill(person_t, 特技_勇将)) stat = pk::max(3, stat);

                if (weapon_heishu == 兵种_枪兵)
                {
                    if (ch::has_skill(person_t, 特技_神将)) stat = pk::max(4, stat);
                    if (ch::has_skill(person_t, 特技_斗神)) stat = pk::max(4, stat);
                    if (ch::has_skill(person_t, 特技_枪神)) stat = pk::max(4, stat);
                    if (ch::has_skill(person_t, 特技_枪将)) stat = pk::max(3, stat);
                    if (ch::has_skill(person_t, 特技_猛者)) stat = pk::max(3, stat);
                }
                else if (weapon_heishu == 兵种_戟兵)
                {
                    if (ch::has_skill(person_t, 特技_神将)) stat = pk::max(4, stat);
                    if (ch::has_skill(person_t, 特技_斗神)) stat = pk::max(4, stat);
                    if (ch::has_skill(person_t, 特技_戟神)) stat = pk::max(4, stat);
                    if (ch::has_skill(person_t, 特技_戟将)) stat = pk::max(3, stat);
                    if (ch::has_skill(person_t, 特技_猛者)) stat = pk::max(3, stat);
                    if (ch::has_skill(person_t, 特技_威风)) stat = pk::max(3, stat);
                }
                else if (weapon_heishu == 兵种_弩兵)
                {
                    if (ch::has_skill(person_t, 特技_神将)) stat = pk::max(3, stat);
                    if (ch::has_skill(person_t, 特技_弓神)) stat = pk::max(5, stat);
                    if (ch::has_skill(person_t, 特技_弓将)) stat = pk::max(4, stat);
                    if (ch::has_skill(person_t, 特技_射手)) stat = pk::max(3, stat);
                    if (ch::has_skill(person_t, 特技_威风)) stat = pk::max(3, stat);
                }
                else if (weapon_heishu == 兵种_骑兵)
                {
                    if (ch::has_skill(person_t, 特技_神将)) stat = pk::max(3, stat);
                    if (ch::has_skill(person_t, 特技_骑神)) stat = pk::max(5, stat);
                    if (ch::has_skill(person_t, 特技_骑将)) stat = pk::max(4, stat);
                    if (ch::has_skill(person_t, 特技_白马)) stat = pk::max(4, stat);
                    if (ch::has_skill(person_t, 特技_疾驰)) stat = pk::max(4, stat);
                    if (ch::has_skill(person_t, 特技_猛者)) stat = pk::max(3, stat);
                }
                else if (weapon_heishu == 兵种_兵器)
                {
                    if (ch::has_skill(person_t, 特技_工神)) stat = pk::max(5, stat);
                    if (ch::has_skill(person_t, 特技_攻城)) stat = pk::max(4, stat);
                    if (ch::has_skill(person_t, 特技_射程)) stat = pk::max(4, stat);
                    if (ch::has_skill(person_t, 特技_奏乐)) stat = pk::max(2, stat);
                }
            }
            return stat;
        }*/
    }

    Main main;
}