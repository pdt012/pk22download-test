﻿// ## 2023/03/26 # 铃 # 适配新特技(直接抄了小二在大浪的修改) ##
// ## 2022/08/18 # 黑店小小二 # 选中部队时更新部队，修复光环影响不实时更新问题 ##
// ## 2021/11/03 # 江东新风 # 伤兵显示开关 ##
// ## 2021/10/29 # 江东新风 # 结构体存储调用方式改进 ##
// ## 2021/10/26 # 江东新风 # 伤兵数字显示 ##
// ## 2021/10/11 # 江东新风 # 修正截粮不显示 ##
// ## 2021/01/09 # 江东新风 # 显示部队特技信息，添加半透明底色 ##
// ## 2020/08/14 # 氕氘氚 # 14:50 ##
// @@ 2020/08/13 @ 氕氘氚 @@

namespace 部队信息显示
{
	//---------------------------------------------------------------------------------------
	const bool 显示边匡 = false;

	//---------------------------------------------------------------------------------------

	class Main
	{
		int max_skill = 165;
		int max_skill_effect = 500;
		Main()
		{
			// 当鼠标放在部队上时会在上方显示相关信息
			pk::bind(120, pk::trigger120_t(func_信息显示_部队信息));			

		}


		void func_信息显示_部队信息()
		{
			// 光标指的坐标
			pk::point cursor_pos = pk::get_cursor_hex_pos();
			if (!pk::is_valid_pos(cursor_pos)) return;


			// 光标上指示的部队
			pk::unit@ unit = pk::get_unit(cursor_pos);
			if (unit is null) return;

			bool is_player = unit.is_player();

			// 2022/08/18 修复光环动态控制
			unit.update();

			// 显示信息
			string unit_name = pk::decode(pk::get_name(unit));

			string title = pk::format("部队信息(\x1b[1x{}\x1b[0x)", unit_name);
			unitinfo@ unit_t = @unit_ex[unit.get_id()];
			string info_wounded = pk::format("伤兵:\x1b[16x{}\x1b[0x", unit_t.wounded);

			string info_攻击力 = pk::format("攻击力: \x1b[2x{}\x1b[0x", unit.attr.stat[部队能力_攻击]);
			string info_防御力 = pk::format("防御力: \x1b[2x{}\x1b[0x", unit.attr.stat[部队能力_防御]);
			string info_智力 = pk::format("智力: \x1b[2x{}\x1b[0x", unit.attr.stat[部队能力_智力]);
			//特技显示,为了能画底色，所以将参数计算上移
			string info_部队特技信息 = get_unit_skill_name(unit);//改成显示特效，用于debug
			int 基础光环范围 = halo::func_光环范围(unit, 0);

			int middle = int(pk::get_resolution().width) / 2;
			int left = middle - 200;
			int right = middle + 200;
			int top = 5;
			int bottom = top + 80;

			pk::point leftdown;
			if (基础光环范围 > 0) leftdown = pk::point(left + 380, top + 125 + skill_line * 20);
			else leftdown = pk::point(left + 380, top + 60 + skill_line * 20);
			pk::draw_filled_rect(pk::rectangle(pk::point(left + 5, top + 5), leftdown), ((0xff / 2) << 24) | 0x777777);
			// pk::draw_rect(pk::rectangle(left, top, right, bottom), 0xff00ccff);
			pk::draw_text(pk::encode(title), pk::point(left + 5, top + 5), 0xffffffff, FONT_BIG, 0xff000000);
			if (开启伤兵系统) pk::draw_text(pk::encode(info_wounded), pk::point(left + 270, top + 13), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_攻击力), pk::point(left + 10, top + 40), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_防御力), pk::point(left + 140, top + 40), 0xffffffff, FONT_SMALL, 0xff000000);
			pk::draw_text(pk::encode(info_智力), pk::point(left + 270, top + 40), 0xffffffff, FONT_SMALL, 0xff000000);



			if (基础光环范围 > 0)
			{
				int 战法暴击光环范围 = halo::func_光环范围(unit, 1);
				int 暴击伤害光环范围 = halo::func_光环范围(unit, 2);
				int 计策成功率光环范围 = halo::func_光环范围(unit, 3);
				int 计策识破率光环范围 = halo::func_光环范围(unit, 4);
				int 辅助攻击光环范围 = halo::func_光环范围(unit, 5);
				int 气力回复光环范围 = halo::func_光环范围(unit, 6);

				string info_光环范围 = (基础光环范围 > 0) ? pk::format("光环范围: \x1b[2x{}\x1b[0x", 基础光环范围) : "光环范围: \x1b[29x无\x1b[0x";
				string info_基础光环 = (基础光环范围 > 0) ? pk::format("基础光环: \x1b[2x+{}\x1b[0x", halo::func_光环效果(unit, 0)) : "基础光环: \x1b[29x无\x1b[0x";
				string info_战法暴击光环 = (战法暴击光环范围 > 0) ? pk::format("战法暴击: \x1b[2x+{}%\x1b[0x", halo::func_光环效果(unit, 1)) : "战法暴击: \x1b[29x无\x1b[0x";
				string info_暴击伤害光环 = (暴击伤害光环范围 > 0) ? pk::format("暴击伤害: \x1b[2x+{}%\x1b[0x", halo::func_光环效果(unit, 2)) : "暴击伤害: \x1b[29x无\x1b[0x";
				string info_计策成功率光环 = (计策成功率光环范围 > 0) ? pk::format("计策成功: \x1b[2x+{}%\x1b[0x", halo::func_光环效果(unit, 3)) : "计策成功: \x1b[29x无\x1b[0x";
				string info_计策识破率光环 = (计策识破率光环范围 > 0) ? pk::format("计策识破: \x1b[2x+{}%\x1b[0x", halo::func_光环效果(unit, 4)) : "计策识破: \x1b[29x无\x1b[0x";
				string info_辅助攻击光环 = (辅助攻击光环范围 > 0) ? pk::format("辅助攻击: \x1b[2x+{}%\x1b[0x", halo::func_光环效果(unit, 5)) : "辅助攻击: \x1b[29x无\x1b[0x";
				string info_气力回复光环 = (气力回复光环范围 > 0) ? pk::format("气力回复: \x1b[2x+{}\x1b[0x", halo::func_光环效果(unit, 6)) : "气力回复: \x1b[29x无\x1b[0x";


				pk::draw_text(pk::encode(info_光环范围), pk::point(left + 10, top + 65), 0xffffffff, FONT_SMALL, 0xff000000);
				pk::draw_text(pk::encode(info_基础光环), pk::point(left + 140, top + 65), 0xffffffff, FONT_SMALL, 0xff000000);
				pk::draw_text(pk::encode(info_战法暴击光环), pk::point(left + 10, top + 85), 0xffffffff, FONT_SMALL, 0xff000000);
				pk::draw_text(pk::encode(info_暴击伤害光环), pk::point(left + 140, top + 85), 0xffffffff, FONT_SMALL, 0xff000000);
				pk::draw_text(pk::encode(info_计策成功率光环), pk::point(left + 10, top + 105), 0xffffffff, FONT_SMALL, 0xff000000);
				pk::draw_text(pk::encode(info_计策识破率光环), pk::point(left + 140, top + 105), 0xffffffff, FONT_SMALL, 0xff000000);
				pk::draw_text(pk::encode(info_辅助攻击光环), pk::point(left + 270, top + 85), 0xffffffff, FONT_SMALL, 0xff000000);
				pk::draw_text(pk::encode(info_气力回复光环), pk::point(left + 270, top + 105), 0xffffffff, FONT_SMALL, 0xff000000);

			}

			//特技显示
			//string info_部队特技信息 = get_unit_skill_name(unit);
			pk::draw_text(info_部队特技信息, pk::point(left + 10, top + ((基础光环范围 > 0) ? 125 : 60)), 0xffffffff, FONT_SMALL, 0xff000000);


		}

		int skill_line;

		string get_unit_skill_name(pk::unit@ unit)
		{
			string name_list = pk::encode("部队特技: ");
			int n = 0;
			skill_line = 1;
			pk::person@ person = pk::get_person(血色_昭阳);
			//pk::trace(pk::format("昭阳：1：{}，2{}，3{}", person.skill,person.skill2, person.skill3));
			//单特技只到159
			for (int i = 0; i <= max_skill; i++)
			{
				if (ch::has_skill(unit, i))
				{
					n += 1;
					if (n == 8)
					{
						name_list += "\n          ";
						skill_line += 1;
						n = 1;
					}
					//pk::trace(pk::format("has skill: {}",i));
					name_list += pk::get_skill(i).name + ' ';
				}
			}
			if (n == 0) name_list += pk::encode("无");
			return name_list;
		}

		string get_unit_skill_effect_name(pk::unit@ unit)
		{
			string name_list = pk::encode("部队特效: ");
			int n = 0;
			skill_line = 1;
	
			for (int i = 0; i <= max_skill; i++)
			{
				if (ch::has_skill(unit, i))
				{
					n += 1;
					if (n == 4)
					{
						name_list += "\n          ";
						skill_line += 1;
						n = 1;
					}
					//pk::trace(pk::format("has skill: {}",i));
					name_list += get_skill_effect_name(i) + ' ';
				}
			}
			if (n == 0) name_list += pk::encode("无");
			return name_list;
		}

		string get_skill_effect_name(int skill_effect)
		{
			if (skill_effect < int(特效名.length))
				return  pk::encode(特效名[skill_effect]);
			return "";
		}

		array<string>特效名 = {
			"步无ZOC",
"骑无ZOC",
"工无ZOC",
"水无ZOC",
"栈道无伤",
"毒泉无伤",
"运输无ZOC",
"辅攻减力小",
"辅攻减力大",
"主攻减力小",
"主攻减力大",
"灭队回力",
"被攻回气",
"陆无反伤",
"水无反伤",
"普攻连击",
"攻击吸兵",
"骑战法加状态",
"齐攻致异常",
"灭队抓将",
"灭队加技巧",
"灭队夺宝物",
"灭队夺物资",
"自防死伤",
"自防俘虏",
"队防死伤",
"队防俘虏",
"兵少格挡",
"低伤格挡",
"射击使敌受伤",
"位移使敌受伤",
"普攻必暴",
"普攻高武暴",
"骑射必暴",
"对设施必暴",
"枪战法高武暴",
"戟战法高武暴",
"弩战法高武暴",
"骑战法高武暴",
"工战法高武暴",
"水战法高武暴",
"枪战法必暴",
"戟战法必暴",
"弩战法必暴",
"骑战法必暴",
"工战法必暴",
"水战法必暴",
"乱射无误伤",
"弩无视森林",
"乐于援助",
"伏兵智高必中",
"镇定智高必中",
"灭火智高必中",
"火计智高必中",
"伪报智高必中",
"扰乱智高必中",
"内讧智高必中",
"智高计策必中",
"免疫火计",
"免疫伪报",
"免疫扰乱",
"免疫内讧",
"免疫伏兵",
"免疫妖术落雷",
"智高免疫计策",
"计策距离增加",
"计策波及邻队",
"计策反弹",
"计策命中加倍",
"减计策耗力大",
"减计策耗力小",
"陷阱减伤",
"免疫火伤害",
"受火伤害加倍",
"军乐效果增加",
"每回合加力",
"耐久回复加倍",
"建造速度加倍",
"港关不耗粮",
"征兵增加",
"枪产量增加",
"戟产量增加",
"弩产量增加",
"马产量增加",
"造攻具快",
"造舰船快",
"外交优势",
"收钱加成",
"收粮加成",
"每旬收钱",
"每月收粮",
"东北无异族",
"西北无异族",
"东南无异族",
"西南无异族",
"不易出贼",
"武将不降忠",
"免疫灾害",
"容易丰收",
"结婚加五维",
"可施展妖术",
"可施展落雷",
"智高计策必暴",
"智低计策必暴",
"火计必暴",
"伪报必暴",
"扰乱必暴",
"镇定必暴",
"伏兵必暴",
"内讧必暴",
"落石增益",
"火伤双倍",
"火伤增益",
"跨水陆行军",
"研究优势",
"友经验双倍",
"自经验双倍",
"必发现人才",
"必发现金钱",
"易发现宝物",
"骑兵加射程",
"弩兵加射程",
"攻具加射程",
"部队减伤大",
"部队减伤小",
"抵御齐攻",
"部队不被位移",
"火计固定耗力",
"灭火固定耗力",
"伪报固定耗力",
"扰乱固定耗力",
"镇定固定耗力",
"伏兵固定耗力",
"内讧固定耗力",
"妖术固定耗力",
"落雷固定耗力",
"地形暴击1",
"地形暴击2",
"地形暴击3",
"地形暴击4",
"地形暴击5",
"地形暴击6",
"运输队攻防",
"运输队移动",
"剑兵攻防",
"剑兵移动",
"枪兵攻",
"枪兵防",
"枪兵移动",
"戟兵攻",
"戟兵防",
"戟兵移动",
"弩兵攻",
"弩兵防",
"弩兵移动",
"骑兵攻",
"骑兵防",
"骑兵移动",
"攻具攻",
"攻具防",
"攻具移动",
"水军攻",
"水军防",
"水军移动",
"自由属性1",
"自由属性2",
"自由属性3",
"自由属性4",
"自由属性5",
"自由属性6",
"自由属性7",
"自由属性8",
"自由属性9",
"自由属性10",
"自由属性11",
"自由属性12",
"战法抵挡大",
"战法抵挡小",
"枪战统高必中",
"戟战统高必中",
"弩战统高必中",
"骑战统高必中",
"工战统高必中",
"水战统高必中",
"部队暴伤大",
"部队暴伤小",
"耐久暴伤大",
"耐久暴伤小",
"枪加暴伤",
"戟加暴伤",
"弩加暴伤",
"骑加暴伤",
"工加暴伤",
"水加暴伤",
"条件暴击1",
"条件暴击2",
"条件暴击3",
"条件暴击4",
"条件暴击5",
"减战法耗力大",
"减战法耗力小",
"枪战减耗力",
"戟战减耗力",
"弩战减耗力",
"骑战减耗力",
"工战减耗力",
"水战减耗力",
"驻军减伤大",
"驻军减伤小",
"耐久减伤大",
"耐久减伤小",
"战法不限地形",
"战斗异常免疫",
"计策抵御加倍",
"强势反击",
"战法强制一骑",
"骑兵冲锋增伤",
"周围敌多减伤",
"灭队减气力",
"邻敌自动异常",
"战法二动",
"计策二动",
"造币效果加强",
"谷仓效果加强",
"军屯农增产",
"行动恢复大",
"行动恢复小",
"破城保留设施",
"破城保留资源",
"防止俘虏逃跑",
"内政建设加速",
"月收钱",
"月收粮",
"月招兵",
"月产枪",
"月产戟",
"月产弩",
"月产马",
"增加技巧值大",
"增加技巧值小",
"减少治安下降",
"部队自动补充",
"亲善效果加倍",
"登人必成功",
"登人必舌战",
"领地外登人",
"自动推荐",
"光环范围广",
"光环加成高",
"枪光环加成高",
"戟光环加成高",
"弩光环加成高",
"骑光环加成高",
"工光环加成高",
"水光环加成高",
"战法暴击免疫",
"兵种相克有利",
"对异常敌加伤",
"太鼓效果提升",
"阵寨效果提升",
"吸引在野人才",
"地形加速1",
"地形加速2",
"地形加速3",
"地形加速4",
"地形加速5",
"地形加速6",
"内政永动",
"抢劫钱粮",
"防抢劫钱粮",
"抢劫人口",
"防抢劫人口",
"防人口损耗",
"防资源损耗",
"交易优势",
"巡查效果提升",
"训练效果提升",
"易被辅助",
"增加带兵数",
"防止被流言",
"赈济贫病",
"市民重税",
"农民重税",
"东北异族内附",
"西北异族内附",
"东南异族内附",
"西南异族内附",
"流民归附",
"市民市场增益",
"农民农场增益",
"市民增长大",
"市民增长小",
"农民增长大",
"农民增长小",
"征兵减人口少",
"市民科技增加",
"农民科技增加",
"屠戮人口",
"防止屠戮",
"精准射击",
"无须分摊战线",
"部队兵低粮耗",
"据点兵低粮耗",
"部队马低粮耗",
"据点马低粮耗",
"回合不减气",
"本土作战加气",
"战法敌减气",
"断粮加气",
"袭击兵粮",
"部队加智",
"恢复状态1",
"恢复状态2",
"恢复状态3",
"恢复状态4",
"恢复状态5",
"恢复状态6",
"恢复状态7",
"建设行动力",
"征兵行动力",
"生产行动力",
"巡查行动力",
"商人行动力",
"研究行动力",
"训练行动力",
"出征行动力",
"移动行动力",
"探索行动力",
"登用行动力",
"褒奖行动力",
"外交行动力",
"计策行动力",
"黑市不拆除",
"大市场无限制",
"鱼市不限唯一",
"自身功绩增加",
"设施间距缩小",
"加战法暴击率",
"加计策暴击率",
"征兵成本降低",
"巡查成本降低",
"生产成本降低",
"开发成本降低",
"工事成本降低",
"计略成本降低",

		};
	}

	Main main;
}
