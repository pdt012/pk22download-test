﻿// ## 2023/05/11 # 江东新风 # 将据点正上方的信息显示均移至此处 ##


namespace BASE_ADD_INFO
{

    //---------------------------------------------------------------------------------------
    int 信息行数 = 0;
    const int 据点信息版本 = 1; //0老版本，1新窗体版本
    const array<string> desc_148 = { "人口", "伤兵", "总兵役","可用兵役","军饷开支","兵装维护","士气","城防","筑城开支","筑城量","金收入","内政开支","治安"};
    //封地显示信息： 将军 人口 兵力 招兵量 资金 金收支 兵粮 粮收支
    const array<string> desc2_148 = { "将军", "人口", "兵力","招兵量","资金","金收支","兵粮","粮收支"};
    // -------------------------------------------------------------------------------------------------------

    class Main
    {
        pk::dialog@ new_dialog_;
        pk::dialog@ new_dialog2_;
        pk::text@ base_name_text_;
        array<pk::sprite9@> bbg0_(13, null);
        array<pk::sprite9@> bbg1_(13, null);
        array<pk::text@> btext0_(13, null);
        array<pk::text@> btext1_(13, null);
        Main()
        {
            // 当鼠标放在据点上时会在上方显示金錢消耗的相关信息
            pk::bind2(120, pk::trigger120_t(信息显示_据点信息), 999);
            pk::bind(102, pk::trigger102_t(onGameInit));
            pk::bind(99, pk::trigger99_t(SCENECHANGE));
        }

        void onGameInit()
        {
            dialog_init();
        }

        void SCENECHANGE(int scene_id)
        {
            if (scene_id == 场景_游戏)
            {
                dialog_init();
            }
 
        }

        void dialog_init()
        {
            //窗体初始化

            pk::size dialog_size = pk::size(319, 181);
            pk::frame@ frame = pk::get_frame();
            pk::dialog@ new_dialog = frame.create_dialog(false);
            @new_dialog_ = @new_dialog;
            new_dialog.set_pos(pk::get_resolution().width / 2 - dialog_size.width / 2, 5);
            new_dialog.set_size(dialog_size);
            new_dialog.set_visible(false);

            //pk::sprite9@ bg = new_dialog.create_sprite9(70);
            //bg.set_color(((0xff / 2) << 24) | 0x010101);
            //bg.set_size(dialog_size);


            pk::text@ text = new_dialog.create_text();
            text.set_size(120, 24);
            text.set_pos(5, 1);
            //text.set_text(pk::encode(desc_148[i]));
            text.set_visible(true);
            text.set_text_font(FONT_BIG);
            @base_name_text_ = @text;


            for (int i = 0; i < 13; ++i)
            {
                //文字描述
                pk::sprite9@ bg0 = new_dialog.create_sprite9(393);
                bg0.set_size(74, 22);
                bg0.set_pos(7 + 154 * (i % 2), 5 + 26 + 24 * (i / 2));
                if (i == 12) bg0.set_pos(7+154,5+2);
                bg0.set_visible(true);
                @bbg0_[i] = @bg0;

                pk::text@ text0 = new_dialog.create_text();
                text0.set_size(64, 16);
                text0.set_pos(7 + 154 * (i % 2) + 5, 5 + 26 + 24 * (i / 2) + 3);
                if (i == 12) text0.set_pos(7 + 154+5, 5 + 2+3);
                //text0.set_text(pk::encode(desc_148[i]));
                text0.set_visible(true);
                text0.set_text_align(TEXT_ALIGN_CENTER);
                @btext0_[i] = @text0;

                //具体数值
                pk::sprite9@ bg1 = new_dialog.create_sprite9(411);
                bg1.set_size(74, 22);
                bg1.set_pos(7 + 78 + 154 * (i % 2), 5 + 26 + 24 * (i / 2));
                if (i == 12) bg1.set_pos(7 + 78 + 154, 5 + 2);
                bg1.set_visible(true);
                @bbg1_[i] = @bg1;

                pk::text@ text1 = new_dialog.create_text();
                text1.set_size(64, 16);
                text1.set_pos(7 + 78 + 154 * (i % 2) + 5, 5 + 26 + 24 * (i / 2) + 3);
                if (i == 12) text1.set_pos(7 + 78 + 154+5, 5 + 2 + 3);
                text1.set_text_align(2);
                @btext1_[i] = @text1;
            }
        }

        //显示信息
        void 信息显示_据点信息()
        {
            
            if (pk::is_alive(new_dialog_,true)
                and new_dialog_.is_visible())
                new_dialog_.set_visible(false);/**/
            // 光标指的坐标
            pk::point cursor_pos = pk::get_cursor_hex_pos();
            if (!pk::is_valid_pos(cursor_pos)) return;

            // 光标上指示的建筑物
            pk::building@ building = pk::get_building(cursor_pos);
            if (building is null) return; 

            int building_type = get_building_type(building);
            if (building_type == 0)
            {
                据点信息行数 = 0;
                //pk::trace("军饷维护费");
                // 显示信息
                func_信息显示_据点信息(building);
            }
            else if (building_type == 1)
            {
                func_信息显示_地名信息(building);
            }

        }

        void func_信息显示_基本据点信息(pk::building@ building)
        {
            bool 测试 = true;
            if (测试)
            {
                string building_name1 = pk::get_old_base_name(building.get_id());
                string building_name2 = pk::get_new_base_name(building.get_id());
                string a = building.facility == 设施_城市 ? "城市信息(\x1b[1x{}{}\x1b[0x)" : "据点信息(\x1b[1x{}{}\x1b[0x)";
                string title = pk::format(a, building_name1, building_name2);
                int middle = int(pk::get_resolution().width) / 2;
                int left = middle - 200;
                int right = middle + 200;
                int top = 5;
                pk::draw_text(pk::encode(title), pk::point(left + 5, top + 5), 0xffffffff, FONT_BIG, 0xff000000);
            }
        }

        void func_信息显示_据点信息(pk::building@ building)
        {
            // pk::draw_rect(pk::rectangle(left, top, right, bottom), 0xff00ccff);
            int base_id = building.get_id();
            BaseInfo @base_t = @base_ex[base_id];
            BuildingInfo @building_p = @building_ex[base_id];

            string building_name = pk::decode(pk::get_name(building));

            string a = building.facility == 设施_城市 ? "城市信息(\x1b[1x{}\x1b[0x)" : "据点信息(\x1b[1x{}\x1b[0x)";
            string title = pk::format(a, building_name);          
          
            int 信息行数 = 0;

            int 总开支 = int((building_p.troops_effic + building_p.repair_effic + building_p.porder_effic + building_p.train_effic + building_p.weapon_effic + building_p.horse_effic + building_p.punch_effic + building_p.boat_effic));
  
            if (据点信息版本 == 0)
            {
                int level = ch::get_city_level(base_id);
                string level_name = ch::get_level_string(level);
                string info_人口 = pk::format("人口:\x1b[1x{}\x1b[0x(\x1b[1x{}\x1b[0x)", base_t.population, level_name);

                //string info_规模 = pk::format("城市规模:\x1b[1x{}\x1b[0x", level_name);
                string info_总兵役 = pk::format("总兵役:\x1b[1x{}\x1b[0x", base_t.mil_pop_all);
                string info_可用兵役 = pk::format("可用兵役:\x1b[1x{}\x1b[0x", base_t.mil_pop_av);
                string info_伤兵 = pk::format("伤兵:\x1b[16x{}\x1b[0x", base_t.wounded);
                string info_额外城防 = pk::format("额外城防:\x1b[16x{}\x1b[0x", building_p.city_defense);
                string info_城市士气 = pk::format("城市士气:\x1b[16x{}\x1b[0x", building.energy);
                string info_总城防 = pk::format("总城防:\x1b[16x{}\x1b[0x", building_p.city_defense + building.hp);
                string info_筑城开支 = pk::format("筑城开支:\x1b[1x{}\x1b[0x", building_p.repair_effic);
                string info_筑城量 = pk::format("筑城量:\x1b[1x{}\x1b[0x", building_p.repair_gain);
                string info_金收入 = pk::format("月度总收入:\x1b[1x{}\x1b[0x", int(building_p.building_revenue));
                string info_内政开支 = pk::format("旬内政开支:\x1b[1x{}\x1b[0x", 总开支);
                string info_军饷 = pk::format("旬军饷开支:\x1b[1x{}\x1b[0x", int(building_p.troops_expense));//需调整
                string info_维护费 = pk::format("兵装维护费:\x1b[1x{}\x1b[0x", int(building_p.weapon_expense));


                // 显示治安

                string info_治安;
                if (base_id >= 据点_城市末 and base_id < 据点_末)
                    info_治安 = pk::format("治安:\x1b[1x{}\x1b[0x", base_ex[base_id].public_order);
                if (base_id >= 0 and base_id < 据点_城市末)
                    info_治安 = pk::format("治安:\x1b[1x{}\x1b[0x", pk::building_to_city(building).public_order);

                // 绘制阴影和标题
                int middle = int(pk::get_resolution().width) / 2;
                int left = middle - 200;
                int right = middle + 200;
                int top = 5;
                int bottom = top + 80;
                int line_count = 1 + (开启人口系统 ? 2 : 0) + (ch::get_auto_affairs_status() ? 3 : 0);

                pk::point leftdown = pk::point(middle + 100, top + 40 + (/*据点信息总行数*/line_count) * 20 + 5);

                pk::draw_filled_rect(pk::rectangle(pk::point(left, top), leftdown), ((0xff / 2) << 24) | 0x010101); //((0xff / 2) << 24) | 0x777777
                pk::draw_text(pk::encode(title), pk::point(left + 5, top + 5), 0xffffffff, FONT_BIG, 0xff000000);

                // 绘制主要信息
                pk::draw_text(pk::encode(info_治安), pk::point(middle - 10, top + 8), 0xffffffff, FONT_SMALL, 0xff000000);
                if (开启人口系统)
                {
                    pk::draw_text(pk::encode(info_人口), pk::point(left + 20, top + 40 + (信息行数) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    //pk::draw_text(pk::encode(info_规模), pk::point(middle - 30, top + 40 + (信息行数) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    pk::draw_text(pk::encode(info_伤兵), pk::point(middle - 30, top + 40 + (信息行数) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    pk::draw_text(pk::encode(info_总兵役), pk::point(left + 20, top + 40 + (信息行数 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    pk::draw_text(pk::encode(info_可用兵役), pk::point(middle - 30, top + 40 + (信息行数 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    信息行数 += 2;
                }
                if (ch::get_auto_affairs_status())
                {
                    pk::draw_text(pk::encode(info_城市士气), pk::point(left + 20, top + 40 + (信息行数) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    // pk::draw_text(pk::encode(info_额外城防), pk::point(left + 20, top + 40 + (信息行数 + 2) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    pk::draw_text(pk::encode(info_总城防), pk::point(middle - 30, top + 40 + (信息行数) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    pk::draw_text(pk::encode(info_筑城开支), pk::point(left + 20, top + 40 + (信息行数 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    pk::draw_text(pk::encode(info_筑城量), pk::point(middle - 30, top + 40 + (信息行数 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    pk::draw_text(pk::encode(info_金收入), pk::point(left + 20, top + 40 + (信息行数 + 2) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    pk::draw_text(pk::encode(info_内政开支), pk::point(middle - 30, top + 40 + (信息行数 + 2) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    信息行数 += 3;
                }

                pk::draw_text(pk::encode(info_维护费), pk::point(left + 20, top + 40 + (信息行数) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                pk::draw_text(pk::encode(info_军饷), pk::point(middle - 30, top + 40 + (信息行数) * 20), 0xffffffff, FONT_SMALL, 0xff000000);

            }
            else
            {
                if (new_dialog_ !is null)
                {
                    pk::size dialog_size = pk::size(318, (181- (ch::get_auto_affairs_status()?0:48)));
                    new_dialog_.set_pos(pk::get_resolution().width / 2 - dialog_size.width / 2, 5);//重设一下，确保分辨率不产生影响
                   
                    pk::draw_filled_rect(pk::rectangle(pk::point(pk::get_resolution().width / 2 - dialog_size.width / 2, 5), dialog_size), ((0xff / 2) << 24) | 0x010101); //((0xff / 2) << 24) | 0x777777
                    new_dialog_.set_visible(true);
                    base_name_text_.set_text(pk::get_name(building));
                    bool need_hide = check_force(building);
                    if (开启人口系统)
                    {
                        for (int i = 0; i < 4; ++i)
                        {
                            bbg0_[i].set_visible(true);
                            btext0_[i].set_visible(true);
                            bbg1_[i].set_visible(true);
                            btext1_[i].set_visible(true);
                            btext0_[i].set_text(pk::encode(desc_148[i]));
                        }

                        btext1_[0].set_text(need_hide ?"***":pk::format("{}", base_t.population));
                        btext1_[1].set_text(need_hide ? "***" : pk::format("{}", base_t.wounded));
                        btext1_[2].set_text(need_hide ? "***" : pk::format("{}", base_t.mil_pop_all));
                        btext1_[3].set_text(need_hide ? "***" : pk::format("{}", base_t.mil_pop_av));
                    }
                    else
                    {
                        for (int i = 0; i < 4; ++i)
                        {
                            bbg0_[i].set_visible(false);
                            btext0_[i].set_visible(false);
                            bbg1_[i].set_visible(false);
                            btext1_[i].set_visible(false);
                        }
                    }

                    if (true)
                    {
                        for (int i = 4; i < 6; ++i)
                        {
                            bbg0_[i].set_visible(true);
                            btext0_[i].set_visible(true);
                            bbg1_[i].set_visible(true);
                            btext1_[i].set_visible(true);
                            btext0_[i].set_text(pk::encode(desc_148[i]));

                        }
                        btext1_[4].set_text(need_hide ? "***" : pk::format("{}", building_p.troops_expense));
                        btext1_[5].set_text(need_hide ? "***" : pk::format("{}", building_p.weapon_expense));
			
			bbg0_[12].set_pos(7 + 154 * (6 % 2), 5 + 26 + 24 * 6 / 2);
			btext0_[12].set_pos(7 + 154 * (6 % 2) + 5, 5 + 26 + 24 * (6 / 2) + 3);
			bbg1_[12].set_pos(7 + 78 + 154 * (6 % 2), 5 + 26 + 24 * (6 / 2));
			btext1_[12].set_pos(7 + 78 + 154 * (6 % 2) + 5, 5 + 26 + 24 * (6 / 2) + 3);
	
                        bbg0_[12].set_visible(true);
                        btext0_[12].set_visible(true);
                        bbg1_[12].set_visible(true);
                        btext1_[12].set_visible(true);
                        btext0_[12].set_text(pk::encode(desc_148[12]));
                        int p_order = base_id < 据点_城市末? pk::building_to_city(building).public_order:base_ex[base_id].public_order;
                        btext1_[12].set_text(need_hide ? "***" : pk::format("{}", p_order));
			
			bbg0_[7].set_visible(true);
                        btext0_[7].set_visible(true);
                        bbg1_[7].set_visible(true);
                        btext1_[7].set_visible(true);
                        btext0_[7].set_text(pk::encode(desc_148[7]));
                        btext1_[7].set_text(pk::format("{}", building_p.city_defense + building.hp));
                       
                    }
                    else
                    {
                        for (int i = 4; i < 6; ++i)
                        {
                            bbg0_[i].set_visible(false);
                            btext0_[i].set_visible(false);
                            bbg1_[i].set_visible(false);
                            btext1_[i].set_visible(false);
                        }
			
                    }


                    if (ch::get_auto_affairs_status())
                    {
                        for (int i = 6; i < 12; ++i)
                        {
                            bbg0_[i].set_visible(true);
                            btext0_[i].set_visible(true);
                            bbg1_[i].set_visible(true);
                            btext1_[i].set_visible(true);
                            btext0_[i].set_text(pk::encode(desc_148[i]));
                        }

                        btext1_[6].set_text(pk::format("{}", building.energy));
                        btext1_[7].set_text(pk::format("{}", building_p.city_defense + building.hp));
                        btext1_[8].set_text(need_hide ? "***" : pk::format("{}", building_p.repair_effic));
                        btext1_[9].set_text(need_hide ? "***" : pk::format("{}", building_p.repair_gain));
                        btext1_[10].set_text(need_hide ? "***" : pk::format("{}", building_p.building_revenue));
                        btext1_[11].set_text(need_hide ? "***" : pk::format("{}", 总开支));
                    }
                    else
                    {
                        for (int i = 6; i < 12; ++i)
                        {
			    if (i == 7) continue;
                            bbg0_[i].set_visible(false);
                            btext0_[i].set_visible(false);
                            bbg1_[i].set_visible(false);
                            btext1_[i].set_visible(false);
                        }
                    }

                }
                //const array<string> desc_148 = { "人口", "伤兵", "总兵役","可用兵役","士气","城防","军饷开支","兵装维护","筑城开支","筑城量","金收入","内政开支"};
                //array<pk::text@> btext1_(12, null);


            }
  

        }

        void func_信息显示_地名信息(pk::building@ building)
        {
            //已在悬浮窗显示的信息应该不显示 

            int id = ch::get_spec_id(building);
           // 地名_id = id;

            int gold_income = 0;
            int food_income = 0;
            int troops_income = 0;

            specialinfo @spec_t = @special_ex[id];
            int person_id = spec_t.person;
            bool has_person = pk::is_valid_person_id(person_id);
            string person_name = ch::get_spec_person_name(id);

            if (has_person)
            {
                //两者通用信息提前计算
                float 玩家修正 = ch::get_income_difficulty_inf(building);

                gold_income = int(sqrt(spec_t.population) * sqrt(pk::get_person(spec_t.person).stat[武将能力_政治]) * 0.2 * 玩家修正 * 3);
                food_income = int(sqrt(spec_t.population) * sqrt(pk::get_person(spec_t.person).stat[武将能力_政治]) * 1.0 * 玩家修正 * 3);
                troops_income = int(sqrt(spec_t.population) * sqrt(pk::get_person(spec_t.person).stat[武将能力_魅力]) * 0.2 * 玩家修正 * 3);

            }

            if (据点信息版本 == 1)
            {
                if (new_dialog_ !is null)
                {
                    int 行数 = has_person?4:1;
                    pk::size dialog_size = pk::size(318, 31+24*行数);
                    new_dialog_.set_pos(pk::get_resolution().width / 2 - dialog_size.width / 2, 5);//重设一下，确保分辨率不产生影响

                    pk::draw_filled_rect(pk::rectangle(pk::point(pk::get_resolution().width / 2 - dialog_size.width / 2, 5), dialog_size), ((0xff / 2) << 24) | 0x010101); //((0xff / 2) << 24) | 0x777777
                    new_dialog_.set_visible(true);

                    string title = pk::format("{}({})", ch::get_spec_name(id),(pk::is_valid_person_id(person_id)?"封地":"郡县"));

                    base_name_text_.set_text(pk::encode(title));
                    bool need_hide = check_force(building);

                    //显示信息： 将军 人口 兵力 招兵量 资金 金收支 兵粮 粮收支


                    for (int i = 0; i < 8; ++i)
                    {
                        if (!has_person and i > 1)
                        {
                            bbg0_[i].set_visible(false);
                            btext0_[i].set_visible(false);
                            bbg1_[i].set_visible(false);
                            btext1_[i].set_visible(false);                           
                        }
                        else
                        {
                            bbg0_[i].set_visible(true);
                            btext0_[i].set_visible(true);
                            bbg1_[i].set_visible(true);
                            btext1_[i].set_visible(true);
                            btext0_[i].set_text(pk::encode(desc2_148[i]));
                        }
                    }
                    for (int i = 8; i < 12; ++i)
                    {
                        bbg0_[i].set_visible(false);
                        btext0_[i].set_visible(false);
                        bbg1_[i].set_visible(false);
                        btext1_[i].set_visible(false);
                    }


                    btext1_[0].set_text(need_hide ? "***" : pk::format("{}", person_name));
                    btext1_[1].set_text(need_hide ? "***" : pk::format("{}", spec_t.population));
                    if (has_person)
                    {
                        btext1_[2].set_text(need_hide ? "***" : pk::format("{}", spec_t.troops));
                        btext1_[3].set_text(need_hide ? "***" : pk::format("{}", troops_income));
                        btext1_[4].set_text(need_hide ? "***" : pk::format("{}", spec_t.gold));
                        btext1_[5].set_text(need_hide ? "***" : pk::format("{}", gold_income));
                        btext1_[6].set_text(need_hide ? "***" : pk::format("{}", spec_t.food));
                        btext1_[7].set_text(need_hide ? "***" : pk::format("{}", food_income));
                    }

                }
             
            }
            else
            {

                int middle = int(pk::get_resolution().width) / 2;
                int left = middle - 200;
                int right = middle + 200;
                int top = 5;
                int bottom = top + 80;
                int 府信息行数 = 0;

                // 绘制阴影
                pk::point leftdown = pk::point(middle + 100, top + 30 + 5 * 20 + 5);

                pk::draw_filled_rect(pk::rectangle(pk::point(left, top), leftdown), ((0xff / 2) << 24) | 0x010101); //((0xff / 2) << 24) | 0x777777

                // pk::draw_rect(pk::rectangle(left, top, right, bottom), 0xff00ccff);
              

                string title = pk::format("县城信息(\x1b[1x{}\x1b[0x) (封地)", ch::get_spec_name(id)); // 后面改成地名
                if (person_id == -1)
                    title = pk::format("县城信息(\x1b[1x{}\x1b[0x) ", ch::get_spec_name(id)); // 后面改成地名

                pk::draw_text(pk::encode(title), pk::point(left + 5, top + 5), 0xffffffff, FONT_BIG, 0xff000000);

                // BaseInfo@ base_t = @base_ex[building_id];
               //显示信息： 将军 人口 兵力 招兵量 资金 金收支 兵粮 粮收支

                string info_将军 = pk::format("将军: \x1b[16x{}\x1b[0x", person_name);
                string info_人口 = pk::format("人口: \x1b[1x{}\x1b[0x", int(spec_t.population));

                pk::draw_text(pk::encode(info_将军), pk::point(left + 10, top + 40 + 据点信息行数 * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                pk::draw_text(pk::encode(info_人口), pk::point(middle - 50, top + 40 + (府信息行数 + 0) * 20), 0xffffffff, FONT_SMALL, 0xff000000);

                if (person_id != -1)
                {
                    string info_兵力 = pk::format("府兵: \x1b[16x{}\x1b[0x", spec_t.troops);
                    string info_资金 = pk::format("资金: \x1b[1x{}\x1b[0x", spec_t.gold);
                    string info_兵粮 = pk::format("兵粮: \x1b[1x{}\x1b[0x", spec_t.food);
                    // string info_统领 = pk::format("统领: \x1b[1x{}\x1b[0x", 3); // 以后再考虑变化
                    string info_人口显示 = pk::format("人口: \x1b[1x{}\x1b[0x", spec_t.population);
                    string info_招兵量 = pk::format("招兵量: \x1b[16x{}\x1b[0x", troops_income);
                    string info_金收支 = pk::format("金收支: \x1b[1x{}\x1b[0x", gold_income);
                    string info_粮收支 = pk::format("粮收支: \x1b[1x{}\x1b[0x", food_income);

                    pk::draw_text(pk::encode(info_人口显示), pk::point(middle - 50, top + 40 + (府信息行数 + 0) * 20), 0xffffffff, FONT_SMALL, 0xff000000);

                    pk::draw_text(pk::encode(info_兵力), pk::point(left + 10, top + 40 + (府信息行数 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    pk::draw_text(pk::encode(info_招兵量), pk::point(middle - 50, top + 40 + (府信息行数 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);

                    pk::draw_text(pk::encode(info_资金), pk::point(left + 10, top + 40 + (府信息行数 + 2) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    pk::draw_text(pk::encode(info_兵粮), pk::point(middle - 50, top + 40 + (府信息行数 + 2) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    // pk::draw_text(pk::encode(info_统领), pk::point(middle + 10, top + 20 + 府信息行数 * 20), 0xffffffff, FONT_SMALL, 0xff000000);

                    pk::draw_text(pk::encode(info_金收支), pk::point(left + 10, top + 40 + (府信息行数 + 3) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                    pk::draw_text(pk::encode(info_粮收支), pk::point(middle - 50, top + 40 + (府信息行数 + 3) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
                }

            }

            // if (person_id != -1)
            // {
            // 	string info_兵力 = pk::format("府兵: \x1b[1x{}\x1b[0x", spec_t.troops);
            // 	string info_资金 = pk::format("资金: \x1b[1x{}\x1b[0x", spec_t.gold);
            // 	string info_兵粮 = pk::format("兵粮: \x1b[1x{}\x1b[0x", spec_t.food);
            // 	string info_统领 = pk::format("统领: \x1b[1x{}\x1b[0x", 3);//以后再考虑变化
            // 	pk::draw_text(pk::encode(info_兵力), pk::point(middle + 10, top + 40 + 据点信息行数 * 20), 0xffffffff, FONT_SMALL, 0xff000000);
            // 	pk::draw_text(pk::encode(info_资金), pk::point(left + 10, top + 40 + (据点信息行数 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
            // 	pk::draw_text(pk::encode(info_兵粮), pk::point(middle + 10, top + 40 + (据点信息行数 + 1) * 20), 0xffffffff, FONT_SMALL, 0xff000000);
            // 	pk::draw_text(pk::encode(info_统领), pk::point(middle + 10, top + 20 + 据点信息行数 * 20), 0xffffffff, FONT_SMALL, 0xff000000);
            // 	据点信息行数 += 2;
            // }
            // else 据点信息行数 += 1;
        }

        //-1非建筑，0 城港关，1 郡县， 2 其他
        int get_building_type(pk::building@ building)
        {
            int building_id = building.get_id();
            if (pk::is_valid_base_id(building_id)) return 0;
            int spec_id = ch::to_spec_id(building_id);
            if (ch::is_valid_spec_id(spec_id)) return 1;          
            //地名_id = id;
            // pk::trace(pk::format("地名id:{}", id));
            //func_信息显示_基本据点信息(building);

            return 2;
        }

        bool check_force(pk::building@ building)
        {
            if (pk::get_scenario().difficulty < 难易度_超级) return false;
            if (building.is_player()) return false;
            if (pk::is_player_controlled(building)) return false;
            if (building.get_force_id() == pk::get_current_turn_force_id()) return false;
            return true;
        }


    }

    Main main;

}
