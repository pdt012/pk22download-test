﻿// ## 2023/06/06 # 江东新风 # 测试ui ##

namespace UI_MOD_RELATE_SET
{

    const array<array<int>>dlg124_data = {
{0,-1,423,1,0,0,16,16,0},
{2,-1,393,9,16,23,154,22,0},
{6,5596,-1,-1,5,3,144,16,80},
{2,-1,393,9,16,67,154,22,0},
{6,5598,-1,-1,5,3,144,16,80},
{2,-1,393,9,16,111,154,22,0},
{6,5600,-1,-1,5,3,144,16,80},
{2,-1,393,9,16,155,154,22,0},
{6,5602,-1,-1,5,3,144,16,80},
{2,-1,393,9,16,199,154,22,0},
{6,5604,-1,-1,5,3,144,16,80},
{2,-1,393,9,16,243,154,22,0},
{6,5606,-1,-1,5,3,144,16,80},
{2,-1,393,9,16,287,154,22,0},
{6,5608,-1,-1,5,3,144,16,80},
{2,-1,393,9,16,331,154,22,0},
{6,5610,-1,-1,5,3,144,16,80},
{8,-1,238,24,178,20,78,28,0},
{8,-1,238,24,178,64,78,28,0},
{8,-1,238,24,178,108,78,28,0},
{8,-1,238,24,178,152,78,28,0},
{8,-1,238,24,178,196,78,28,0},
{8,-1,238,24,178,240,78,28,0},
{8,-1,238,24,178,284,78,28,0},
{8,-1,238,24,264,20,78,28,0},
{8,-1,238,24,264,64,78,28,0},
{8,-1,238,24,264,108,78,28,0},
{8,-1,238,24,264,152,78,28,0},
{8,-1,238,24,264,196,78,28,0},
{8,-1,238,24,264,240,78,28,0},
{8,-1,238,24,264,284,78,28,0},
{8,-1,238,24,350,20,78,28,0},
{8,-1,238,24,350,64,78,28,0},
{8,-1,238,24,350,152,78,28,0},
{8,-1,286,24,178,328,28,28,0},
{6,5629,-1,-1,6,6,16,16,80},
{8,-1,286,24,465,328,28,28,0},
{6,5631,-1,-1,6,6,16,16,80},
{2,-1,327,9,501,331,42,22,0},
{6,5633,-1,-1,5,3,32,16,400},
{0,-1,422,1,805,489,16,16,0},
{0,-1,420,1,805,0,16,16,0},
{0,-1,425,1,0,489,16,16,0},
{9,-1,434,1,0,16,16,473,0},
{9,-1,434,1,805,16,16,473,0},
{9,-1,427,1,16,0,789,16,0},
{9,-1,427,1,16,489,789,16,0},
{2,-1,327,9,213,333,244,18,0},
{7,-1,-1,0,215,335,240,14,0},
{2,-1,411,9,460,15,346,220,0},
{6,-1,-1,-1,465,18,336,214,16}
    };
    const array<string> dlg124_desc0 = { "事件菜单扩展",
"培养菜单扩展",
"据点计略扩展",
"个人菜单扩展",
"买卖菜单扩展",
"军团菜单扩展",
"国事菜单扩展",
"自动内政扩展",
"单港关存活",
"疑兵伏兵奇袭",
"异族交涉扩展",
"部队计略扩展",
"部队指挥扩展",
"战术战略妙计",
"隐藏编辑功能",
"府兵强度调整"
    };
    const array<string> dlg124_desc1 = { "设定是否开启新增的三国志系列剧情，包括曹操南征，刘表之死，三顾茅庐，三让徐州，孙刘联盟。非三国mod或剧本建议关闭，防止异常。",
"设定是否开启新增的培养菜单，包括去除特技，培养特技，脱胎换骨。喜欢偏史实设定的玩家建议关闭。韩版血色衣冠默认关闭。",
"设定是否开启新增的据点计略菜单，包括据点的破坏，鼓舞，扰乱，攻心，援军，镇静，伪报，火攻，调查，待劳，奇袭攻具。如关闭，ai和玩家均不会使用此功能。",
"设定是否开启新增的君主个人菜单，包括拜访，纳妾，养子，寿命延长，太学，休养。韩版血色衣冠默认关闭。",
"设定是否开启新增的买卖菜单，包括兵器买卖和兵装买卖。",
"设定是否开启新增的军团菜单，包括一系列军团命令和禁止军团拆建功能。",
"设定是否开启新增的国事菜单，包括势力颜色更改，退位，祭祀，称帝，国号变更，拜见，迁都，拥立，废黜。",
"设定是否开启新增的自动内政设定菜单，极大方便游戏，建议开启。",
"设定是否开启单港关存活，三顾茅庐剧本默认开启，其他剧本默认关闭，可能存在未知bug，谨慎开启。",
"设定是否开启疑兵伏兵奇袭功能，开启疑兵伏兵奇袭功能后，ai可能在初次和己方部队交战时，触发疑兵，或者走向指定地格后，触发伏兵，或运输队路过ai境内时，触发ai奇袭部队，纯增加难度设定，新手可以考虑关闭，老玩家建议挑战。",
"设定是否开启异族交涉功能，开启后，玩家可以和异族进行外交，获取宝物或买通异族进攻ai，同时ai也会买通异族进攻玩家。丰富玩法的设定，如果自觉水平不足，打不过异族，可以关闭。",
"设定是否开启新增的部队计略菜单，包括部队的落石，奇袭，鼓舞，大鼓舞，攻城，挑衅，回归。由于ai暂不会使用此类功能，为纯玩家加强功能，如追求公平或追求难度，可关闭。",
"设定是否开启新增的部队指挥菜单，包括部队的解散，合并，招募乡勇，换帅，强行军。由于ai暂不会使用此类功能，为纯玩家加强功能，如追求公平或追求难度，可关闭。",
"设定是否开启新增的部队战术战略妙计菜单，包括战术：昂扬奋战，威风屈敌，枪兵冲锋，戟兵破甲，骑兵突袭，大杀四方，战略：士气高涨，敌军动摇，援军支援，敌军煽动，妙计：神火计，八阵图，虚虚实实，速战固守。如关闭，ai和玩家均不会使用此功能。",
"设定是否隐藏游戏内编辑，如开启，游戏内编辑按钮会隐藏",
"设定府兵的强度。封地府兵功能作为pk2.2特色，不支持关闭，但支持在一定范围内调整强度，如感觉府兵强度过高，可在此处适当调整。(强度决定一回合府兵出兵次数及府兵武将能力平均强度)。封地府兵虽然增加游戏难度，但是也将增加游戏乐趣，新版的府兵已经削弱了其输出，更多的体现府兵封地的战略价值，建议初次游玩新版时直接用目前的默认强度体验。",
	};

	//按剧本区分的初始设定
	const array <array<uint8>> dlg124_scene_set_ = {
{1,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1},
{1,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1},
{1,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1},
{1,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1},
{1,1,1,1,1,1,1,1,1,0,1,1,1,1,0,1},
{1,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1},
{1,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1},
{1,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1},
{1,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1},
{1,1,1,1,1,1,1,1,1,0,1,1,1,1,0,1},
{1,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1},
{1,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1},
{1,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1},
{1,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1},
{1,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1},
{0,0,1,0,1,1,1,1,0,0,1,1,1,0,0,1}
	};//初始化

    class Main
    {
        //dlg120相关参数
        pk::dialog@ new_dialog120_;
        pk::dialog@ new_dialog124_;

		pk::button@ new_button_ = @null;
		pk::button@ btnf_easy_ = @null;
		pk::button@ btnf_normal_ = @null;
		pk::button@ btnf_hard_ = @null;
		array<int> btnfid_(3,-1);
		array<pk::button@> btnf_(3, null);
        array<int> dlg124_btnid_(30, -1);
        array<pk::button@> dlg124_btn_(30, null);
		
        int dlg124_textid_ = -1;
        pk::text@ dlg124_text_ = @null;
        pk::text@ slider_text_ = @null;
        array<bool> dlg124_set(32, false);//默认设置


		uint8 spec_strength = 1;//府兵默认强度

        Main()
        {
			pk::bind(99, pk::trigger99_t(SCENECHANGE));
			pk::bind2(102, pk::trigger102_t(callback));
            pk::bind(210, pk::trigger210_t(dialog_init));
            pk::bind(238, pk::trigger238_t(DLG_OPEN));
			pk::bind(231, pk::trigger231_t(buttonreleased));
        }

		void SCENECHANGE(int scene_id)
		{
			if (scene_id == 场景_势力选择)
			{
				for (int i = 0; i < 15; ++i)
				{
					dlg124_set[i] = ((dlg124_scene_set_[pk::get_scenario().no][i] == 1) ? true : false);
					//pk::trace("game init setting_ex.mod_set[i]:" + setting_ex.mod_set[i] + ",dlg124_set:" + main.dlg124_set[i]);
				}
				spec_strength = dlg124_scene_set_[pk::get_scenario().no][15];
			}
			//场景_新武将管理
			/*
			if (scene_id == 场景_新武将管理)
			{
				pk::person@ person = pk::get_person(血色_管夷吾);
				pk::trace("场景_新武将管理：管夷吾：skill:" + person.skill + "wadai" + person.wadai);
			}
			if (scene_id == 场景_游戏读取)
			{
				pk::person@ person = pk::get_person(血色_管夷吾);
				pk::trace("场景_游戏读取：管夷吾：skill:" + person.skill + "wadai" + person.wadai);
			}
			if (scene_id == 场景_游戏)
			{
				pk::person@ person = pk::get_person(血色_管夷吾);
				pk::trace("场景_游戏：管夷吾：skill:" + person.skill + "wadai" + person.wadai);
			}*/
		}

		void DLG_OPEN(pk::dialog@ dialog)
		{
			pk::dialog@dlg12 = pk::get_dialog(12);
			if (dlg12 !is null and dlg12.get_p() == dialog.get_p())
			{
				pk::trace("dlg12 open");
				if (new_button_ !is null)pk::left_click(new_button_);

				//更改ok键的功能
				//决定按钮多个界面重复使用，功能会变化？
				pk::button@ btn453 = dialog.find_child(453);
				if (btn453 !is null)
				{
					pk::detail::funcref func0 = cast<pk::button_on_released_t@>(function(button) {
						for (int i = 0; i < 32; ++i)
						{
							setting_ex.mod_set[i] = main.dlg124_set[i];
							//pk::trace("setting_ex.mod_set[i]:" + setting_ex.mod_set[i] + ",dlg124_set:" + main.dlg124_set[i]);
						}
						main.new_dialog124_.delete(); @main.new_dialog124_ = @null;

					});
					btn453.on_button_released(func0);
				}

			}
		}

		void buttonreleased(pk::button@button)
		{
			//pk::trace(pk::format("on release button_id:{},p:0x{:x},parent:0x{:x}",button.get_id() ,button.get_p() , button.get_parent().get_p()));
		}

		void callback()
		{
			//读档时
			for (int i = 0; i < 32; ++i)
			{
				dlg124_set[i] = setting_ex.mod_set[i];
				//pk::trace("game init setting_ex.mod_set[i]:" + setting_ex.mod_set[i] + ",dlg124_set:" + main.dlg124_set[i]);
			}
			pk::set_can_edit(!setting_ex.mod_set[隐藏编辑功能_开关]);
		} // callback()

		void dialog_init(pk::dialog@ dialog, int dlg_id)
		{
			if (dialog is null) return;
			if (dlg_id == 12)
			{
				hook12(dialog);
			}
			else if (dlg_id == 126)
			{
				hook126(dialog);
			}
			else if (dlg_id == 129)
			{
				hook129(dialog);
			}
			else if (dlg_id == 130)
			{
				hook130(dialog);
			}
			else if (dlg_id == 131)
			{
				hook131(dialog);
			}
			else if (dlg_id == 136)
			{
				hook136(dialog);
			}
			//128 129 131 136 137

		}

		void hook12(pk::dialog@ dialog)
		{
			init_dlg124(dialog);
			add_new_button(dialog);

		}

		void add_new_button(pk::dialog@ dialog)
		{

			pk::button@ btn268 = dialog.find_child(268);
			pk::button@ btn269 = dialog.find_child(269);
			pk::button@ btn270 = dialog.find_child(270);
			pk::button@ btn271 = dialog.find_child(271);
			pk::button@ button = dialog.create_button(1747);
			button.set_size(175, 32);
			button.set_text(pk::encode("mod设定"));
			button.set_text_align(TEXT_ALIGN_CENTER | TEXT_ALIGN_MIDDLE);
			button.set_text_font(FONT_BIG);
			button.set_pos(btn268.get_pos().x, btn268.get_pos().y);//(785,83);
			btn268.set_pos(btn268.get_pos().x + 175, btn268.get_pos().y);
			btn269.set_pos(btn269.get_pos().x + 175, btn269.get_pos().y);
			btn270.set_pos(btn270.get_pos().x + 175, btn270.get_pos().y);
			btn271.set_pos(btn271.get_pos().x + 175, btn271.get_pos().y);
			/**/
			pk::detail::funcref func0 = cast<pk::button_on_pressed_t@>(function(button) {
				/*
				pk::dialog@ dlg13 = pk::get_dialog(13);//失败原因是获得的dlg指针是最大的dlg,未发现子dlg
				pk::dialog@ dlg14 = pk::get_dialog(14);
				pk::dialog@ dlg15 = pk::get_dialog(15);
				pk::dialog@ dlg44 = pk::get_dialog(44);
				dlg44.set_visible(false);
				dlg13.set_visible(false);
				dlg14.set_visible(false);
				dlg15.set_visible(false);*/
				pk::dialog@ dlg12 = pk::get_dialog(12);
				main.set_dlg13_visible(dlg12, false);
				main.set_dlg14_visible(dlg12, false);
				main.set_dlg15_visible(dlg12, false);
				main.set_dlg44_visible(dlg12, false);
				if (main.new_dialog124_ !is null) main.new_dialog124_.set_visible(true);
			});
			button.on_button_pressed(func0);
			@new_button_ = @button;
			pk::detail::funcref func1 = cast<pk::widget_on_destroy_t@>(function(button) { @main.new_button_ = @null;});
			button.on_widget_destroy(func1);

			pk::detail::funcref func2 = cast<pk::button_on_pressed_t@>(function(button) { if (main.new_dialog124_ !is null) main.new_dialog124_.set_visible(false); });
			btn268.on_button_pressed(func2);
			btn269.on_button_pressed(func2);
			btn270.on_button_pressed(func2);
			btn271.on_button_pressed(func2);

			//按钮组
			dialog.add_button_group(0, { new_button_.get_id() });/**/
			//pk::left_click(new_button_);//确保mod界面先展开
		}

        void set_dlg13_visible(pk::dialog@dialog, bool visible)
        {
            set_child_visible(dialog, visible, 273, 307);
        }

        void set_dlg14_visible(pk::dialog@dialog, bool visible)
        {
            set_child_visible(dialog, visible, 308, 332);
        }

        void set_dlg15_visible(pk::dialog@dialog, bool visible)
        {
            set_child_visible(dialog, visible, 333, 371);
        }

        void set_dlg44_visible(pk::dialog@dialog, bool visible)
        {
            set_child_visible(dialog, visible, 1743, 1800);
        }

        void set_child_visible(pk::dialog@ dlg, bool visible, int start_id, int end_id)
        {
            for (int i = start_id; i <= end_id; ++i)
            {
                auto widget = dlg.find_child(i);
                if (widget !is null)
                {
                    widget.set_visible(visible);
                }
            }
        }

		void init_dlg120()
		{
			pk::size dialog_size = pk::size(900, 800);
			pk::frame@ frame = pk::get_frame();
			pk::dialog@ new_dialog = pk::new_dialog();//frame.create_dialog(false);//
			@new_dialog120_ = @new_dialog;
			new_dialog.set_pos(pk::get_resolution().width / 2 - dialog_size.width / 2, pk::get_resolution().height / 2 - dialog_size.height / 2);
			new_dialog.set_size(dialog_size);
			new_dialog.set_visible(false);
			//pk::trace("init_dlg120 dlg id1:" + new_dialog.get_dlg_id());

			//设置数据初始化
			//dlg124_set = setting_ex.mod_set;
			//5392
			pk::sprite9@ bg0 = new_dialog.create_sprite9(70);
			bg0.set_pos(12, 54);
			bg0.set_size(840, 574);
			//5393
			pk::image@ im0 = new_dialog.create_image(527);
			im0.set_pos(720, 389);
			im0.set_size(120, 184);
			//5394
			@ im0 = new_dialog.create_image(319);
			im0.set_pos(608, 477);
			im0.set_size(112, 96);

			//5395
			@ im0 = new_dialog.create_image(320);
			im0.set_pos(0, 0);
			im0.set_size(284, 64);
			//5396
			@ bg0 = new_dialog.create_sprite9(70);
			bg0.set_pos(21, 70);
			bg0.set_size(822, 542);
			//5397
			pk::button@ bt0 = new_dialog.create_button(238);
			bt0.set_pos(29, 78);
			bt0.set_size(78, 28);
			bt0.set_text(pk::encode("基本设定"));
			//5398
			@ bt0 = new_dialog.create_button(238);
			bt0.set_pos(115, 78);
			bt0.set_size(78, 28);
			bt0.set_text(pk::encode("特殊设定"));

			//5399
			@ im0 = new_dialog.create_image(323);
			im0.set_pos(5, 622);
			im0.set_size(252, 60);
			//5400
			@ im0 = new_dialog.create_image(322);
			im0.set_pos(799, 622);
			im0.set_size(64, 48);
			//5401
			@ im0 = new_dialog.create_image(321);
			im0.set_pos(823, 36);
			im0.set_size(40, 28);
			//5402
			pk::sprite0@ sp0 = new_dialog.create_sprite0(430);
			sp0.set_pos(284, 36);
			sp0.set_size(539, 32);
			//5403
			@ sp0 = new_dialog.create_sprite0(431);
			sp0.set_pos(851, 64);
			sp0.set_size(16, 558);
			//5404
			@ sp0 = new_dialog.create_sprite0(432);
			sp0.set_pos(5, 64);
			sp0.set_size(16, 558);
			//5405
			@ sp0 = new_dialog.create_sprite0(433);
			sp0.set_pos(257, 622);
			sp0.set_size(542, 64);
			//5406
			@ im0 = new_dialog.create_image(316);
			im0.set_pos(18, 68);
			im0.set_size(8, 8);
			//5407
			@ im0 = new_dialog.create_image(315);
			im0.set_pos(18, 607);
			im0.set_size(8, 8);
			//5408
			@ im0 = new_dialog.create_image(313);
			im0.set_pos(837, 607);
			im0.set_size(8, 8);
			//5409
			@ im0 = new_dialog.create_image(314);
			im0.set_pos(837, 68);
			im0.set_size(8, 8);
			//5410
			@ sp0 = new_dialog.create_sprite0(428);
			sp0.set_pos(26, 68);
			sp0.set_size(811, 8);
			//5411
			@ sp0 = new_dialog.create_sprite0(428);
			sp0.set_pos(26, 607);
			sp0.set_size(811, 8);
			//5412
			@ sp0 = new_dialog.create_sprite0(435);
			sp0.set_pos(18, 76);
			sp0.set_size(8, 531);
			//5413
			@ sp0 = new_dialog.create_sprite0(435);
			sp0.set_pos(837, 76);
			sp0.set_size(8, 531);

			@ bt0 = new_dialog.create_button(180);
			bt0.set_pos(115, 632);
			bt0.set_size(96, 44);
			bt0.set_text(pk::encode("返回"));
			pk::detail::funcref func1 = cast<pk::button_on_released_t@>(function(button) { main.new_dialog120_.delete(); @main.new_dialog120_ = @null; });
			bt0.on_button_released(func1);

			@ bt0 = new_dialog.create_button(172);
			bt0.set_pos(27, 632);
			bt0.set_size(88, 44);
			bt0.set_text(pk::encode("决定"));
			pk::detail::funcref func2 = cast<pk::button_on_released_t@>(function(button) {
				for (int i = 0; i < 32; ++i)
				{
					setting_ex.mod_set[i] = main.dlg124_set[i];
					//pk::trace("setting_ex.mod_set[i]:" + setting_ex.mod_set[i] + ",dlg124_set:" + main.dlg124_set[i]);
				}
				main.new_dialog120_.delete(); @main.new_dialog120_ = @null;

			});
			bt0.on_button_released(func2);

			pk::text@ tx0 = new_dialog.create_text();
			tx0.set_pos(40, 22);
			tx0.set_size(96, 24);
			tx0.set_text(pk::encode("mod设置"));
			tx0.set_text_font(FONT_BIG);

			init_dlg124(new_dialog);
		}

		void init_dlg124(pk::dialog@parent_dlg, bool is_preference = true)
		{
			//开关
			/*
			* 单港关存活？
			* 府兵
			* 伏兵，奇袭运输队
			* 事件扩展
			* 培养扩展
			* 据点计略扩展
			* 个人菜单扩展
			* 买卖菜单扩展
			* 军团菜单扩展
			* 国事菜单扩展
			* 自动内容扩展
			*/

			pk::size dialog_size = pk::size(822, 506);
			//pk::frame@ frame = pk::get_frame();
			pk::dialog@ new_dialog = parent_dlg.create_dialog(false);
			@new_dialog124_ = @new_dialog;
			//pk::trace("init_dlg124 dlg id1:" + new_dialog124_.get_dlg_id());//所以新生成的dlg没有这个设定，默认都是0

			if (is_preference)
			{
				//pk::widget@ child_w = parent_dlg.find_child(290);
				//new_dialog.set_pos(child_w.get_pos().x, child_w.get_pos().y);
				new_dialog.set_pos(113-16, 177-23-20);//113, 177
			}
			else new_dialog.set_pos(21, 107);//相对dialog120的位置

			new_dialog.set_size(dialog_size);
			if (is_preference) new_dialog.set_visible(false);
			if (true)
			{
				//府兵难度按钮
				if (true)
				{
					pk::sprite9@ sp9 = new_dialog.create_sprite9(is_preference ? 1804 : 393);
					sp9.set_pos(16, 23);
					sp9.set_size(154, 22);

					pk::text@ text = new_dialog.create_text();
					text.set_pos(16 + 5, 23);
					text.set_size(144, 16);
					text.set_text(pk::encode(dlg124_desc0[15]));
					text.set_text_align(0x24);

					//第二列
					pk::button@ bt0 = new_dialog.create_button(is_preference ? 2306 : 238);
					bt0.set_pos(178 + (is_preference ? 10 : 0), 20);
					bt0.set_size(78, 28);
					bt0.set_text(pk::encode((is_preference ? "      " : "") + "初级"));
					//dlg124_btnid_[i * 2] = bt0.get_id();
					//@dlg124_btn_[i * 2] = @bt0;
					//if(i==0) pk::trace(pk::format("init:{},p: 0x{:x}", bt0.get_id(), bt0.get_p()));//证明add_button_group后，button的地址没有变，id没有变

					//第三列
					pk::button@ bt1 = new_dialog.create_button(is_preference ? 2306 : 238);
					bt1.set_pos(264 + (is_preference ? 10 : 0), 20);
					bt1.set_size(78, 28);
					bt1.set_text(pk::encode((is_preference ? "      " : "") + "上级"));
					//dlg124_btnid_[i * 2 + 1] = bt1.get_id();
					//@dlg124_btn_[i * 2 + 1] = @bt1;

					pk::button@ bt2 = new_dialog.create_button(is_preference ? 2306 : 238);
					bt2.set_pos(350 + (is_preference ? 10 : 0), 20);
					bt2.set_size(78, 28);
					bt2.set_text(pk::encode((is_preference ? "      " : "") + "超级"));

					//府兵对应按钮储存
					@btnf_[0] = @bt0;
					@btnf_[1] = @bt1;
					@btnf_[2] = @bt2;
					btnfid_[0] = bt0.get_id();
					btnfid_[1] = bt1.get_id();
					btnfid_[2] = bt2.get_id();

					pk::detail::funcref func0 = cast<pk::button_on_pressed_t@>(function(button) { main.spec_strength = 0; });
					bt0.on_button_pressed(func0);
					pk::detail::funcref func1 = cast<pk::button_on_pressed_t@>(function(button) { main.spec_strength = 1; });
					bt1.on_button_pressed(func1);
					pk::detail::funcref func2 = cast<pk::button_on_pressed_t@>(function(button) { main.spec_strength = 2; });
					bt2.on_button_pressed(func2);

					//根据spec_strength值设定按下的按钮
					if (spec_strength == 0)
					{
						new_dialog.add_button_group({ bt0.get_id(), bt1.get_id(), bt2.get_id() });
					}
					else if (spec_strength == 1)
					{
						new_dialog.add_button_group({ bt1.get_id(), bt0.get_id(),bt2.get_id() });
					}
					else if (spec_strength == 2)
					{
						new_dialog.add_button_group({ bt2.get_id(), bt1.get_id(), bt0.get_id() });
					}
				}

				//左侧列
				for (int i = 0; i < 10; ++i)
				{
					//第一列
					pk::sprite9@ sp9 = new_dialog.create_sprite9(is_preference?1804:393);
					sp9.set_pos(16, 23 + 44 + 44 * i);
					sp9.set_size(154, 22);

					pk::text@ text = new_dialog.create_text();
					text.set_pos(16 + 5, 23 + 44 + 44 * i + 3);
					text.set_size(144, 16);
					text.set_text(pk::encode(dlg124_desc0[i]));
					text.set_text_align(0x24);

					//第二列
					pk::button@ bt0 = new_dialog.create_button(is_preference ? 2306:238);
					bt0.set_pos(178 + (is_preference ?10:0) , 20 + 44 + 44 * i);
					bt0.set_size(78, 28);
					bt0.set_text(pk::encode((is_preference ? "      " : "") + "开启"));
					dlg124_btnid_[i * 2] = bt0.get_id();					
					@dlg124_btn_[i * 2] = @bt0;
					//if(i==0) pk::trace(pk::format("init:{},p: 0x{:x}", bt0.get_id(), bt0.get_p()));//证明add_button_group后，button的地址没有变，id没有变

					//第三列
					pk::button@ bt1 = new_dialog.create_button(is_preference ? 2306 : 238);
					bt1.set_pos(264 + (is_preference ? 10 : 0), 20 + 44 + 44 * i);
					bt1.set_size(78, 28);
					bt1.set_text(pk::encode((is_preference ? "      " : "") + "关闭"));
					dlg124_btnid_[i * 2 + 1] = bt1.get_id();
					@dlg124_btn_[i * 2 + 1] = @bt1;

					//pk::trace(bt0.get_id() + "," + bt1.get_id());
					//根据设定值决定哪个开启
					if (dlg124_set[i])
					{
						//if (i == 0) pk::trace("init124 setting_ex.mod_set[i]" + setting_ex.mod_set[i]);
						new_dialog.add_button_group({ dlg124_btn_[i * 2].get_id(), dlg124_btn_[i * 2 + 1].get_id() });//所以add之后，形成了一个新的组件？
						//new_dialog.add_button_group({bt0.get_id(), bt1.get_id() });//所以add之后，形成了一个新的组件？
					}
					else
					{
						//if (i == 0) pk::trace("init124 setting_ex.mod_set[i]" + setting_ex.mod_set[i]);
						new_dialog.add_button_group({ dlg124_btn_[i * 2 + 1].get_id(),dlg124_btn_[i * 2].get_id() });//所以add之后，形成了一个新的组件？
						//new_dialog.add_button_group({ bt1.get_id(),bt0.get_id() });//所以add之后，形成了一个新的组件？
					}
				}
				//右侧列
				for (int i = 10; i < 15; ++i)
				{
					//第一列
					pk::sprite9@ sp9 = new_dialog.create_sprite9(is_preference ? 1804 : 393);
					sp9.set_pos(465, 23 + 44 * (i-4));
					sp9.set_size(154, 22);

					pk::text@ text = new_dialog.create_text();
					text.set_pos(465 + 5, 23 + 44 * (i - 4) + 3);
					text.set_size(144, 16);
					text.set_text(pk::encode(dlg124_desc0[i]));
					text.set_text_align(0x24);

					//第二列
					pk::button@ bt0 = new_dialog.create_button(is_preference ? 2306 : 238);
					bt0.set_pos(617 + (is_preference ? 10 : 0), 20 + 44 * (i - 4));
					bt0.set_size(78, 28);
					bt0.set_text(pk::encode((is_preference ? "      " : "") + "开启"));
					dlg124_btnid_[i * 2] = bt0.get_id();
					@dlg124_btn_[i * 2] = @bt0;
					//if(i==0) pk::trace(pk::format("init:{},p: 0x{:x}", bt0.get_id(), bt0.get_p()));//证明add_button_group后，button的地址没有变，id没有变

					//第三列
					pk::button@ bt1 = new_dialog.create_button(is_preference ? 2306 : 238);
					bt1.set_pos(713 + (is_preference ? 10 : 0), 20 + 44 * (i - 4));
					bt1.set_size(78, 28);
					bt1.set_text(pk::encode((is_preference ? "      " : "") + "关闭"));
					dlg124_btnid_[i * 2 + 1] = bt1.get_id();
					@dlg124_btn_[i * 2 + 1] = @bt1;

					//pk::trace(bt0.get_id() + "," + bt1.get_id());
					//根据设定值决定哪个开启
					if (dlg124_set[i])
					{
						//if (i == 0) pk::trace("init124 setting_ex.mod_set[i]" + setting_ex.mod_set[i]);
						new_dialog.add_button_group({ dlg124_btn_[i * 2].get_id(), dlg124_btn_[i * 2 + 1].get_id() });//所以add之后，形成了一个新的组件？
						//new_dialog.add_button_group({bt0.get_id(), bt1.get_id() });//所以add之后，形成了一个新的组件？
					}
					else
					{
						//if (i == 0) pk::trace("init124 setting_ex.mod_set[i]" + setting_ex.mod_set[i]);
						new_dialog.add_button_group({ dlg124_btn_[i * 2 + 1].get_id(),dlg124_btn_[i * 2].get_id() });//所以add之后，形成了一个新的组件？
						//new_dialog.add_button_group({ bt1.get_id(),bt0.get_id() });//所以add之后，形成了一个新的组件？
					}
				}


				if (is_preference)
				{
					pk::widget@ w_text = parent_dlg.find_child(272);
					@dlg124_text_ = @w_text;
					dlg124_textid_ = w_text.get_id();
				}
				else
				{

					pk::sprite9@ sp9 = new_dialog.create_sprite9(411);
					sp9.set_pos(440, 15);
					sp9.set_size(366, 220);

					pk::text@ text = new_dialog.create_text();
					text.set_pos(445, 18);
					text.set_size(356, 214);
					text.set_text(pk::encode(""));
					text.set_text_align(TEXT_ALIGN_LEFT | TEXT_ALIGN_TOP);

					@dlg124_text_ = @text;
					dlg124_textid_ = text.get_id();
				}
				

				if (!is_preference)
				{
					//设置sliderbar
					int default_value = 50;

					auto bg1 = new_dialog.create_sprite(327);//2334
					bg1.set_pos(625, 240);
					bg1.set_size(42, 22);
					pk::text@ stext = new_dialog.create_text();
					stext.set_pos(bg1.get_pos().x + 5, bg1.get_pos().y + 3);
					stext.set_size(bg1.get_size().width - 10, bg1.get_size().height - 6);
					stext.set_text(pk::format("{}", default_value));
					@slider_text_ = @stext;

					auto bg0 = new_dialog.create_sprite(327);//2334
					bg0.set_pos(440, 240);
					bg0.set_size(180, 18);
					pk::slider@ slider = new_dialog.create_slider();
					slider.set_pos(bg0.get_pos() + 2);
					slider.set_size(bg0.get_size() - 4);
					slider.set_min_max(0, 100);
					slider.set_value(default_value);
					pk::detail::funcref funcslider = cast<pk::slider_on_value_changed_t@>(function(self, value) { main.slider_text_.set_text(pk::format("{}", value)); });
					slider.on_slider_value_changed(funcslider);



				}
	
				if (true)
				{
					pk::detail::funcref func00 = cast<pk::widget_on_hover_enter_t@>(function(self, button) {
						for (int i = 0; i < 15; ++i)
						{
							if (main.dlg124_btnid_[i * 2] == button.get_id() or main.dlg124_btnid_[i * 2 + 1] == button.get_id())
							{
								main.dlg124_text_.set_text(pk::text_wrap(pk::encode(dlg124_desc1[i]), 42, 9));

							}
						}
						for (int i = 0; i < 3; ++i)
						{
							if (main.btnfid_[i] == button.get_id())
							{
								main.dlg124_text_.set_text(pk::text_wrap(pk::encode(dlg124_desc1[15]), 42, 9));
							}
						}
						

					});
					pk::detail::funcref func01 = cast<pk::widget_on_hover_leave_t@>(function(self, button) { main.dlg124_text_.set_text(""); });
					pk::detail::funcref func02 = cast<pk::widget_on_destroy_t@>(function(self) {
						//组件摧毁时清空对应指针，防止出错
						for (int i = 0; i < 30; ++i)
						{
							@main.dlg124_btn_[i] = @null;
							main.dlg124_btnid_[i] = -1;
						};
						for (int i = 0; i < 3; ++i)
						{
							main.btnfid_[i] = -1;
							@main.btnf_[i] = @null;
						}
					});
					//dlg124_btn_[0].on_widget_hover_enter(func00);
					pk::widget@ parent_w = dlg124_btn_[0].get_parent();
					//pk::trace(pk::format(" parent 0x{:x}", parent_w.get_p()));
					parent_w.on_widget_hover_enter(func00);
					parent_w.on_widget_hover_leave(func01);
					//pk::detail::funcref func01 = cast<pk::widget_on_hover_enter_t@>(function(self, button) {main.dlg124_text_.set_text("");});


					pk::detail::funcref func0 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[事件菜单扩展_开关] = true; });
					dlg124_btn_[0].on_button_pressed(func0);
					pk::detail::funcref func2 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[培养菜单扩展_开关] = true; });
					dlg124_btn_[2].on_button_pressed(func2);
					pk::detail::funcref func4 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[据点计略扩展_开关] = true; });
					dlg124_btn_[4].on_button_pressed(func4);
					pk::detail::funcref func6 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[个人菜单扩展_开关] = true; });
					dlg124_btn_[6].on_button_pressed(func6);
					pk::detail::funcref func8 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[买卖菜单扩展_开关] = true; });
					dlg124_btn_[8].on_button_pressed(func8);
					pk::detail::funcref func10 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[军团菜单扩展_开关] = true; });
					dlg124_btn_[10].on_button_pressed(func10);
					pk::detail::funcref func12 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[国事菜单扩展_开关] = true; });
					dlg124_btn_[12].on_button_pressed(func12);
					pk::detail::funcref func14 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[自动内政扩展_开关] = true; });
					dlg124_btn_[14].on_button_pressed(func14);
					pk::detail::funcref func16 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[单港关存活_开关] = true; });
					dlg124_btn_[16].on_button_pressed(func16);
					pk::detail::funcref func18 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[疑兵伏兵奇袭_开关] = true; });
					dlg124_btn_[18].on_button_pressed(func18);
					pk::detail::funcref func20 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[异族交涉扩展_开关] = true; });
					dlg124_btn_[20].on_button_pressed(func20);
					pk::detail::funcref func22 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[部队计略扩展_开关] = true; });
					dlg124_btn_[22].on_button_pressed(func22);
					pk::detail::funcref func24 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[部队指挥扩展_开关] = true; });
					dlg124_btn_[24].on_button_pressed(func24);
					pk::detail::funcref func26 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[战术战略妙计_开关] = true; });
					dlg124_btn_[26].on_button_pressed(func26);
					pk::detail::funcref func28 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[隐藏编辑功能_开关] = true; });
					dlg124_btn_[28].on_button_pressed(func28);

					pk::detail::funcref func1 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[事件菜单扩展_开关] = false; });
					dlg124_btn_[1].on_button_pressed(func1);
					pk::detail::funcref func3 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[培养菜单扩展_开关] = false; });
					dlg124_btn_[3].on_button_pressed(func3);
					pk::detail::funcref func5 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[据点计略扩展_开关] = false; });
					dlg124_btn_[5].on_button_pressed(func5);
					pk::detail::funcref func7 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[个人菜单扩展_开关] = false; });
					dlg124_btn_[7].on_button_pressed(func7);
					pk::detail::funcref func9 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[买卖菜单扩展_开关] = false; });
					dlg124_btn_[9].on_button_pressed(func9);
					pk::detail::funcref func11 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[军团菜单扩展_开关] = false; });
					dlg124_btn_[11].on_button_pressed(func11);
					pk::detail::funcref func13 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[国事菜单扩展_开关] = false; });
					dlg124_btn_[13].on_button_pressed(func13);
					pk::detail::funcref func15 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[自动内政扩展_开关] = false; });
					dlg124_btn_[15].on_button_pressed(func15);
					pk::detail::funcref func17 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[单港关存活_开关] = false; });
					dlg124_btn_[17].on_button_pressed(func17);
					pk::detail::funcref func19 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[疑兵伏兵奇袭_开关] = false; });
					dlg124_btn_[19].on_button_pressed(func19);
					pk::detail::funcref func21 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[异族交涉扩展_开关] = false; });
					dlg124_btn_[21].on_button_pressed(func21);
					pk::detail::funcref func23 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[部队计略扩展_开关] = false; });
					dlg124_btn_[23].on_button_pressed(func23);
					pk::detail::funcref func25 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[部队指挥扩展_开关] = false; });
					dlg124_btn_[25].on_button_pressed(func25);
					pk::detail::funcref func27 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[战术战略妙计_开关] = false; });
					dlg124_btn_[27].on_button_pressed(func27);
					pk::detail::funcref func29 = cast<pk::button_on_pressed_t@>(function(button) { main.dlg124_set[隐藏编辑功能_开关] = false; });
					dlg124_btn_[29].on_button_pressed(func29);
				}

			}
			else
			{
				int start_id = 5595;
				for (int i = 0; i < int(dlg124_data.length); ++i)
				{
					if (i == (5526 - start_id) or i == (5527 - start_id) or i == (5528 - start_id)) continue;
					pk::widget@ widget = new_dialog.create_widget(dlg124_data[i][0], dlg124_data[i][2]);
					int parent = dlg124_data[i][1];
					int x = dlg124_data[i][4] + ((parent == -1) ? 0 : dlg124_data[parent - start_id][4]);
					int y = dlg124_data[i][5] + ((parent == -1) ? 0 : dlg124_data[parent - start_id][5]);
					int width = dlg124_data[i][6];
					int height = dlg124_data[i][7];
					if (i == 1)
					{
						//pk::trace(pk::format("x{},self:{}", x, dlg124_data[i][4]));
					}
					if (i == 2)
					{
						//pk::trace(pk::format("x{},self:{},parent:{}", x, dlg124_data[i][4], dlg124_data[parent - start_id][4]));
					}
					if (widget !is null)
					{
						widget.set_pos(x, y);
						widget.set_size(width, height);
					}
					//5597到5609的文本
					if (i > 1 and i < 15 and i % 2 == 0)
					{
						pk::text@ text0 = @widget;
						text0.set_text(pk::encode(dlg124_desc0[i / 2]));
						text0.set_text_align(TEXT_ALIGN_MIDDLE | TEXT_ALIGN_CENTER);
					}
				}
			}

		}

		//128 129 131 136 137
		void hook126(pk::dialog@ dialog)
		{
			if (setting_ex.mod_set[隐藏编辑功能_开关])
			{
				auto widget = dialog.find_child(5803);
				if (widget !is null)
				{
					widget.set_visible(false);
				}
			}
		}

		void hook129(pk::dialog@ dialog)
		{
			if (setting_ex.mod_set[隐藏编辑功能_开关])
			{
				auto widget = dialog.find_child(6029);
				if (widget !is null)
				{
					widget.set_visible(false);
				}
			}
		}

		void hook130(pk::dialog@ dialog)
		{
			if (setting_ex.mod_set[隐藏编辑功能_开关])
			{
				auto widget = dialog.find_child(6098);
				if (widget !is null)
				{
					widget.set_visible(false);
				}
			}
		}

		void hook131(pk::dialog@ dialog)
		{
			if (setting_ex.mod_set[隐藏编辑功能_开关])
			{
				auto widget = dialog.find_child(6145);
				if (widget !is null)
				{
					widget.set_visible(false);
				}
			}
		}

		void hook136(pk::dialog@ dialog)
		{
			if (setting_ex.mod_set[隐藏编辑功能_开关])
			{
				auto widget = dialog.find_child(6433);
				if (widget !is null)
				{
					widget.set_visible(false);
				}
			}
		}
    }

    Main main;

}