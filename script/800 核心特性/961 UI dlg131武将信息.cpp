﻿// ## 2023/08/31 # 黑店小小二 # 特技显示颜色调整 ##
// ## 2023/06/06 # 江东新风 # 测试ui ##

namespace UI_DLG131_PERSON
{
    const array<string> item_desc = { "马匹", "武器", "宝物" };
    class Main
    {
        //功阶特技名及描述
        array<pk::text@> ktext_(4, null);

        array<pk::face@> iface_(3, null);//宝物图像
        array<pk::text@> itext_(3, null);//宝物文字

        //dlg131更改
        pk::dialog@dlg200_;

        Main()
        {
			//pk::bind2(102, pk::trigger102_t(callback));
            //pk::bind(210, pk::trigger210_t(dialog_init));
            //pk::bind(238, pk::trigger238_t(DLG_OPEN));
			//pk::bind(231, pk::trigger231_t(buttonreleased));
            pk::bind(210, pk::trigger210_t(dialog_init));
            pk::bind(215, pk::trigger215_t(DLG131_SETPERSON));
            //pk::bind(238, pk::trigger238_t(DLG_OPEN));//新选项卡
        }


        void dialog_init(pk::dialog@ dlg, int dlg_id)
        {
            //dlg是没有对应的widget_id的
            if (dlg_id == 131)
            {
                hook131(dlg);
            }
            if (dlg_id == 132)
            {
                hook132(dlg);//宝物显示
            }
            if (dlg_id == 133)
            {
                if (pk::get_scenario().no != 血色剧本) hook133(dlg);//功阶特技
            }
        }

        void hook131(pk::dialog@ dlg131)
        {
            //array<int> btn_id(3, -1);
            init_dlg200(dlg131);
        }

        void DLG131_SETPERSON(pk::person@ person)
        {
            /*
        TEXT_ALIGN_LEFT = 1,
        TEXT_ALIGN_RIGHT = 2,
        TEXT_ALIGN_CENTER = 4,
        TEXT_ALIGN_BOTTOM = 0x10,
        TEXT_ALIGN_MIDDLE = 0x20,
        TEXT_ALIGN_TOP = 0x40,
            */
            int person_id = person.get_id();
            if (pk::is_valid_person_id(person_id))
            {
                //设置功阶特技
                if (pk::get_scenario().no != 血色剧本)
                {
                    for (int i = 0; i < 2; ++i)
                    {
                        if (ktext_[i * 2] !is null and ktext_[i * 2 + 1] !is null)
                        {
                            int skill_id0 = pk::get_kouseki_skill(person_id, i);
                            if (pk::is_valid_skill_id2(skill_id0))
                            {
                                pk::skill@ skill0 = pk::get_skill(skill_id0);

                                string skill_name = (int(person.kouseki) < pk::get_kouseki_limit(i)) ? calcStringColor(pk::get_name(skill0)) : pk::get_name(skill0);
                                ktext_[i * 2].set_text(skill_name);
                                //ktext_[i * 2].set_text_align(0x4 | 0x20);
                                string color = (int(person.kouseki) < pk::get_kouseki_limit(i)) ? "\x1b[20x" : "";

                                string skill_desc = (int(person.kouseki) < pk::get_kouseki_limit(i)) ? calcStringColor(skill0.desc) : skill0.desc;

                                ktext_[i * 2 + 1].set_text(color + pk::text_wrap(skill_desc, 30, 2));
                                // ktext_[i * 2 + 1].set_text_align(0x1 | 0x40);
                                //if (person.kouseki < pk::get_kouseki_limit(i))
                                //{
                                //    pk::trace(pk::format("{},{}", person.kouseki , pk::get_kouseki_limit(i)));
                                //    ktext_[i * 2].set_text_color(0x808080,0);
                                //    ktext_[i * 2+1].set_text_color(0x808080, 0);
                                //}
                            }
                            else
                            {
                                ktext_[i * 2].set_text("");
                                ktext_[i * 2 + 1].set_text("");
                            }
                        }

                    }
                }

                //设置武将宝物
                array<pk::item@> item_arr = get_item_array(person_id);
                for (int i = 0; i < 3; ++i)
                {
                    if (iface_[i] !is null and itext_[i] !is null)
                    {
                        if (item_arr[i] !is null)
                        {
                            int item_id = item_arr[i].get_id();
                            iface_[i].set_face(-2 - item_id);
                            itext_[i].set_text(item_arr[i].name);
                        }
                        else
                        {
                            iface_[i].set_face(-1);
                            itext_[i].set_text(pk::encode(item_desc[i]));
                        }

                        @item_arr[i] = @null;//需在设置完后清空
                    }
                }

            }
        }

        void DLG_OPEN(pk::dialog@ dialog)
        {
            if (dialog is null) return;
            //pk::trace("dlg_open:" + dialog.get_dlg_id());
            if (dialog.find_child(6139) !is null)//确保是dialog131
            {

                array<int> btn_id = {};// {6139, 6140, 6441, 6442};
                for (int i = 0; i < 1; ++i)
                {
                    pk::button@ button1 = dialog.create_button(238);
                    button1.set_text(pk::encode("测试" + i));
                    button1.set_pos(135 + 8 + 258 + 86 + 86 * i, 387 + 8 + 0);
                    button1.set_size(78, 28);//
                    //btn_id[i] = button1.get_id();
                    btn_id.insertLast(button1.get_id());
                    pk::detail::funcref func0 = cast<pk::button_on_pressed_t@>(function(button) {
                        pk::dialog@ dlg132 = pk::get_dialog(132);
                        pk::dialog@ dlg133 = pk::get_dialog(133);
                        pk::dialog@ dlg134 = pk::get_dialog(134);
                        pk::dialog@ dlg135 = pk::get_dialog(135);
                        dlg132.set_visible(false);
                        dlg133.set_visible(false);
                        dlg134.set_visible(false);
                        dlg135.set_visible(false);
                        if (main.dlg200_ !is null) main.dlg200_.set_visible(true);
                    });
                    button1.on_button_pressed(func0);
                }
                //注意，对应组件的func应该只能设置一个，重复设置可能会被冲掉
                pk::button@btn6139 = @dialog.find_child(6139);
                pk::button@btn6140 = @dialog.find_child(6140);
                pk::button@btn6141 = @dialog.find_child(6141);
                pk::button@btn6142 = @dialog.find_child(6142);
                pk::detail::funcref func0 = cast<pk::button_on_pressed_t@>(function(button) { if (main.dlg200_ !is null) main.dlg200_.set_visible(false); });
                btn6139.on_button_pressed(func0);
                btn6140.on_button_pressed(func0);
                btn6141.on_button_pressed(func0);
                btn6142.on_button_pressed(func0);
                //dlg133.add_button_group(btn_id);
                dialog.add_button_group(0, btn_id);/**/
            }

        }

        void hook133(pk::dialog@ dlg133)
        {
            //既然还无法做按钮事件。那就先重写成类似国内版的ui模型？

            //6296是特技描述文本组件，size(240,34)
            //根据列传，人物属性框大概184高，特技描述高40，所以4个没问题
            pk::size size = pk::size(48, 22);
            pk::point pos = pk::point(300, 78);
            //前面错误原因只是因为cpp代码页为中文代码页
            string name_text;
            dlg133.set_size(pk::size(693, 218));
            pk::widget @ pre_w = dlg133.find_child(6265);//适性
            pre_w.set_size(pk::size(240, 0));
            @pre_w = dlg133.find_child(6266);//适性
            pre_w.set_size(pk::size(240, 0));
            @pre_w = dlg133.find_child(6229);
            pre_w.set_size(pk::size(398, 16));//原70
            @pre_w = dlg133.find_child(6239);
            pre_w.set_size(pk::size(398, 16));
            int init_id = 6267;
            for (int i = 0; i < 6; ++i)
            {
                init_id = 6267 + i * 4;
                pk::point hpos = pk::point(166, 15 + 27 * i);
                pk::point hpos1 = hpos;
                @pre_w = dlg133.find_child(init_id);//对应兵种适性名
                pre_w.set_pos(hpos1);

                pk::point hpos2 = hpos1;
                hpos1.x += pre_w.get_size().width + 2;//提前获取第一个框的宽度
                hpos2.x += 5;
                hpos2.y += 3;
                @pre_w = dlg133.find_child(init_id + 1);//对应兵种适性名文本
                pre_w.set_pos(hpos2);

                @pre_w = dlg133.find_child(init_id + 2);//对应兵种适性
                pre_w.set_pos(hpos1);

                hpos2 = hpos1;
                hpos2.x += 5;
                hpos2.y += 3;
                @pre_w = dlg133.find_child(init_id + 3);//对应兵种适性文本
                pre_w.set_pos(hpos2);
            }
            //左边 6228 6231 6238
            //713下三角，714上三角

            //处理不必要组件
            for (int i = 0; i < 5; ++i)
            {
                if (i == 4) continue;
                int id = 6233 + i;
                @pre_w = dlg133.find_child(id);
                pre_w.set_size(pk::size(0, 0));
            }

            //184-32
            @pre_w = dlg133.find_child(6230);//右上角
            pre_w.set_pos(pk::point(564, 0));//(pk::point(236, 0));
            @pre_w = dlg133.find_child(6231);
            pre_w.set_size(pk::size(pre_w.get_size().width, 152));
            @pre_w = dlg133.find_child(6232);
            pre_w.set_size(pk::size(pre_w.get_size().width, 152));
            pre_w.set_pos(pk::point(564, pre_w.get_pos().y));
            @pre_w = dlg133.find_child(6237);//中间间隔
            pre_w.set_size(pk::size(pre_w.get_size().width, 152 + 16));
            pre_w.set_pos(pk::point(236, 8));
            @pre_w = dlg133.find_child(6238);
            pre_w.set_pos(pk::point(pre_w.get_pos().x, 168));
            @pre_w = dlg133.find_child(6239);
            pre_w.set_pos(pk::point(pre_w.get_pos().x, 168));
            @pre_w = dlg133.find_child(6240);//右下角
            pre_w.set_pos(pk::point(564, 168));


            @pre_w = dlg133.find_child(6291);//隐藏特技文字
            pre_w.set_size(pk::size(0, 0));
            @pre_w = dlg133.find_child(6292);
            pre_w.set_size(pk::size(0, 0));

            @pre_w = dlg133.find_child(6293);//特技name
            pre_w.set_pos(pk::point(251, 24));
            @pre_w = dlg133.find_child(6294);
            pre_w.set_pos(pk::point(256, 27));
            @pre_w = dlg133.find_child(6295);//特技desc
            pre_w.set_pos(pk::point(303, 15));
            @pre_w = dlg133.find_child(6296);
            pre_w.set_pos(pk::point(308, 18));

            //pk::trace("p:" + pre_w.get_p());
            //功阶1 2 特技名

            if (true)
            {
                for (int i = 0; i < 2; ++i)
                {
                    //特技名
                    pk::sprite9@ sp1 = dlg133.create_sprite9(357);
                    sp1.set_pos(251, 24 + 52 + 52 * i);
                    sp1.set_size(58, 22);
                    pk::text@ tx0 = dlg133.create_text();
                    tx0.set_pos(251 + 5, 24 + 3 + 52 + 52 * i);
                    tx0.set_size(48, 16);
                    tx0.set_text_align(0x4 | 0x20);
                    @ktext_[i * 2] = @tx0;


                    //特技描述
                    pk::sprite9@ sp3 = dlg133.create_sprite9(411);
                    sp3.set_pos(303, 67 + 52 * i);
                    sp3.set_size(250, 40);
                    pk::text@ tx1 = dlg133.create_text();
                    tx1.set_pos(303 + 5, 67 + 3 + 52 * i);
                    tx1.set_size(240, 34);
                    tx1.set_text_align(0x1 | 0x40);
                    @ktext_[i * 2 + 1] = @tx1;
                }
            }

        }

        void hook132(pk::dialog@ dlg132)
        {
            dlg132.set_size(pk::size(693, 218));
            for (int i = 0; i < 2; ++i)
            {
                int child_id = 6204 + i * 4;
                pk::widget @ pre_w = dlg132.find_child(child_id);//
                pk::point pos0 = pk::point(16, 119);
                pre_w.set_pos(pk::point(pos0.x, pos0.y + i * 25));
                @pre_w = dlg132.find_child(child_id + 1);//
                pre_w.set_pos(pk::point(pos0.x + 5, pos0.y + i * 25 + 3));
                @pre_w = dlg132.find_child(child_id + 2);//任务背景
                pre_w.set_pos(pk::point(pos0.x + 44, pos0.y + i * 25));
                @pre_w = dlg132.find_child(child_id + 3);//任务文本
                pre_w.set_pos(pk::point(pos0.x + 44 + 5, pos0.y + i * 25 + 3));
            }
            for (int i = 0; i < 2; ++i)
            {
                int child_id = 6212 + i * 4;
                pk::widget @ pre_w = dlg132.find_child(child_id);//
                pk::point pos0 = pk::point(165, 119);
                pre_w.set_pos(pk::point(pos0.x, pos0.y + i * 25));
                @pre_w = dlg132.find_child(child_id + 1);//
                pre_w.set_pos(pk::point(pos0.x + 5, pos0.y + i * 25 + 3));
                @pre_w = dlg132.find_child(child_id + 2);//任务背景
                pre_w.set_pos(pk::point(pos0.x + 44 + 1, pos0.y + i * 25));
                pre_w.set_size(pk::size(74, 22));
                @pre_w = dlg132.find_child(child_id + 3);//任务文本
                pre_w.set_pos(pk::point(pos0.x + 44 + 5, pos0.y + i * 25 + 3));
                pre_w.set_size(pk::size(64, 16));
            }
            //身份（6180-6183）移到原功绩位置（）165,41,44,22 32 16  209,41,74,22 64 16
            //俸禄(6200-6203)移动到原指挥位置 165,93
            for (int i = 0; i < 2; ++i)
            {
                int child_id = 6200 - i * 20;
                pk::widget @ pre_w = dlg132.find_child(child_id);//
                pk::point pos0 = pk::point(165, 93 - i * 52);
                pre_w.set_pos(pk::point(pos0.x, pos0.y));
                @pre_w = dlg132.find_child(child_id + 1);//
                pre_w.set_pos(pk::point(pos0.x + 5, pos0.y + 3));
                @pre_w = dlg132.find_child(child_id + 2);//任务背景
                pre_w.set_pos(pk::point(pos0.x + 44 + 1, pos0.y));
                pre_w.set_size(pk::size(74, 22));
                @pre_w = dlg132.find_child(child_id + 3);//任务文本
                pre_w.set_pos(pk::point(pos0.x + 44 + 5, pos0.y + 3));
                pre_w.set_size(pk::size(64, 16));
            }
            //功绩6188-6191 指挥6196-6199移动到宝物下，调整大小
            for (int i = 0; i < 2; ++i)
            {
                int child_id = 6188 + i * 8;
                pk::widget @ pre_w = dlg132.find_child(child_id);//
                pk::point pos0 = pk::point(308 + i * 154, 142);
                pre_w.set_pos(pk::point(pos0.x, pos0.y));
                pre_w.set_size(pk::size(42, 28));
                @pre_w = dlg132.find_child(child_id + 1);//
                pre_w.set_pos(pk::point(pos0.x + 5, pos0.y + 1));
                pre_w.set_size(pk::size(32, 26));
                pk::text@ text0 = @pre_w;
                text0.set_text_font(FONT_SMALL);
                @pre_w = dlg132.find_child(child_id + 2);//任务背景
                pre_w.set_pos(pk::point(pos0.x + 54 + 1, pos0.y));
                pre_w.set_size(pk::size(90, 28));
                @pre_w = dlg132.find_child(child_id + 3);//任务文本
                pre_w.set_pos(pk::point(pos0.x + 54 + 5, pos0.y + 3));
                pre_w.set_size(pk::size(80, 22));
                @ text0 = @pre_w;
                text0.set_text_font(FONT_BIG);
            }


            //宝物组件及对应背景
            pk::size img_size = pk::size(64, 64);
            pk::size imgb_size = pk::size(60, 60);

            //宝物图像
            for (int i = 0; i < 3; ++i)
            {
                pk::sprite9 @ imgb0 = dlg132.create_sprite9(411);
                imgb0.set_size(imgb_size);
                imgb0.set_pos(pk::point(324 + 102 * i, 31));

                pk::face@ fac0 = dlg132.create_face();
                //fac0.set_face(-2-115);
                fac0.set_face_size(img_size);
                fac0.set_pos(pk::point(324 + 102 * i, 31));
                fac0.set_face_type(3);
                @iface_[i] = @fac0;
            }

            //宝物文本

            for (int i = 0; i < 3; ++i)
            {
                pk::point pos = pk::point(308 + 102 * i, 103);
                pk::sprite9 @ imgb = dlg132.create_sprite9(393);
                imgb.set_size(pk::size(90, 22));
                imgb.set_pos(pos);

                //pk::point pos = pk::point(308 + 102 * i, 103);
                pk::text@ tex0 = dlg132.create_text();//
                tex0.set_pos(pk::point(pos.x + 5, pos.y + 3));
                tex0.set_size(pk::size(80, 16));
                tex0.set_text(pk::encode(item_desc[i]));
                tex0.set_text_align(4);
                @itext_[i] = @tex0;
            }

            //边框
            //纵向分隔 6153 -6156
            //底边 6157 6158 6159 6160 6161 6162 6163
            //左侧底边特殊处理 6151 6152 6162 6163
            //底部173(+48)，左侧431+180
            for (int i = 0; i < 4; ++i)
            {
                int id = 6153 + i;
                pk::widget@ pre_w = dlg132.find_child(id);
                pre_w.set_size(pk::size(pre_w.get_size().width, pre_w.get_size().height + 32));
            }
            for (int i = 0; i < 7; ++i)
            {
                int id = 6157 + i;
                pk::widget@ pre_w = dlg132.find_child(id);
                pre_w.set_pos(pk::point(pre_w.get_pos().x, pre_w.get_pos().y + 32));
            }

            pk::widget@ pre_w = dlg132.find_child(6151);
            pre_w.set_size(pk::size(pre_w.get_size().width + 188, pre_w.get_size().height));
            @pre_w = dlg132.find_child(6162);
            pre_w.set_size(pk::size(pre_w.get_size().width + 188, pre_w.get_size().height));

            @pre_w = dlg132.find_child(6152);
            pre_w.set_pos(pk::point(pre_w.get_pos().x + 188, pre_w.get_pos().y));
            @pre_w = dlg132.find_child(6163);
            pre_w.set_pos(pk::point(pre_w.get_pos().x + 188, pre_w.get_pos().y));
            @pre_w = dlg132.find_child(6156);
            pre_w.set_pos(pk::point(pre_w.get_pos().x + 188, pre_w.get_pos().y));
        }

        void init_dlg200(pk::dialog@ dialog)
        {
            pk::size dialog_size = pk::size(700, 400);
            pk::dialog@dlg200 = dialog.create_dialog(false);
            @dlg200_ = @dlg200;
            dlg200.set_pos(21, 427);//相对dialog131的位置
            dlg200.set_size(dialog_size);
            dlg200.set_visible(false);

            //UI_TEST::main.create_spec_listview(dialog,pk::point(0,0));

            pk::size size0 = pk::size(58, 22);
            pk::size size1 = pk::size(48, 16);
            pk::point pos0 = pk::point(251, 24 + 52);
            pk::point pos1 = pk::point(251 + 5, 24 + 3 + 52);
            pk::sprite9@ sp1 = dlg200.create_sprite9(357);
            sp1.set_pos(pos0);
            sp1.set_size(size0);
            pk::text@ tx0 = dlg200.create_text();
            tx0.set_pos(pos1);
            tx0.set_size(size1);


            pos0.y = pos0.y + 52;
            pos1.y = pos1.y + 52;
            pk::sprite9@ sp2 = dlg200.create_sprite9(357);
            sp2.set_pos(pos0);
            sp2.set_size(size0);
            pk::text@ tx1 = dlg200.create_text();
            tx1.set_pos(pos1);
            tx1.set_size(size1);

            //功阶2 3 描述
            pk::size size2 = pk::size(250, 40);
            pk::size size3 = pk::size(240, 34);
            pk::point pos2 = pk::point(303, 67);
            pk::point pos3 = pk::point(303 + 5, 67 + 3);
            pk::sprite9@ sp3 = dlg200.create_sprite9(411);
            sp3.set_pos(pos2);
            sp3.set_size(size2);
            pk::text@ tx2 = dlg200.create_text();
            tx2.set_pos(pos3);
            tx2.set_size(size3);

            pos2.y = pos2.y + 52;
            pos3.y = pos3.y + 52;
            pk::sprite9@ sp4 = dlg200.create_sprite9(411);
            sp4.set_pos(pos2);
            sp4.set_size(size2);
            pk::text@ tx3 = dlg200.create_text();
            tx3.set_pos(pos3);

            //create_spec_listview(dialog, pk::point(300, 50));
        }

        array<pk::item@> get_item_array(int person_id)
        {
            array<pk::item@> arr(3, null);
            if (pk::is_valid_person_id(person_id))
            {
                for (int i = 0; i < 扩展宝物_末; ++i)
                {
                    pk::item@ item = pk::get_item(i);
                    if (pk::is_alive(item))
                    {
                        int group_id = pk::item_type_to_group(item.type);
                        if (group_id > 宝物组_道具) continue;
                        if (arr[group_id] !is null) continue;
                        if (item.owner == person_id)
                        {
                            @arr[group_id] = @item;
                            if (arr[0] !is null and arr[1] !is null and arr[2] !is null) break;
                        }
                    }
                }
            }
            return arr;

        }

        string calcStringColor(string skill)
        {
            array<string> skill_desc = skill.split('[2x');
            string _skill = join(skill_desc, '[20x');
            skill_desc = _skill.split('[1x');
            _skill = join(skill_desc, '[20x');
            skill_desc = _skill.split('[0x');
            _skill = join(skill_desc, '[20x');

            skill_desc = _skill.split('[00x');
            _skill = join(skill_desc, '[20x');
            skill_desc = _skill.split('[19x');
            _skill = join(skill_desc, '[20x');
            skill_desc = _skill.split('[22x');
            _skill = join(skill_desc, '[20x');
            skill_desc = _skill.split('[23x');
            _skill = join(skill_desc, '[20x');
            return _skill;
        }

    }

    Main main;

}