﻿// ## 2023/08/30 # 黑店小小二 # 添加道具菜单 ##
// ## 2023/06/06 # 江东新风 # 测试ui ##

namespace UI_BUILDING
{

    const array<string> desc_148 = { "将军", "资金", "兵粮","人口","耐久","府兵" };

    class Main
    {
        //dlg126相关
        pk::button@ nbtn_126_ = @null;
        pk::force@force126_ = @null;

        //dlg128相关
        pk::button@ nbtn_128_ = @null;
        pk::district@district128_ = @null;

        //dlg129相关
        pk::button@ nbtn_129_ = @null;
        pk::city@city129_ = @null;

        //dlg148相关
        array<pk::sprite9@> bbg0_(6, null);
        array<pk::sprite9@> bbg1_(6, null);
        array<pk::text@> btext0_(6, null);
        array<pk::text@> btext1_(6, null);

        Main()
        {
            pk::bind(210, pk::trigger210_t(dialog_init));
            pk::bind(211, pk::trigger211_t(DLG126_SETFORCE));
            pk::bind(212, pk::trigger212_t(DLG128_SETDISTRICT));
            pk::bind(213, pk::trigger213_t(DLG129_SETCITY));
            pk::bind(220, pk::trigger220_t(BUILDING_HUD_INIT));
            pk::bind(222, pk::trigger222_t(DLG148_SET_BUILDING));
          
        }

        void dialog_init(pk::dialog@ dlg, int dlg_id)
        {
            if (dlg_id == 148)
            {
                hook148(dlg);
            }
            else if (dlg_id == 129)
            {
                hook129(dlg);
            }
            else if (dlg_id == 128)
            {
                hook128(dlg);
            }
            else if (dlg_id == 126)
            {
                hook126(dlg);
            }
        }

        void DLG126_SETFORCE(pk::force@ force)
        {
            @force126_ = @force;
        }

        void DLG128_SETDISTRICT(pk::district@ district)
        {
            @district128_ = @district;
        }

        void DLG129_SETCITY(pk::city@ city)
        {
            @city129_ = @city;
        }

        void DLG148_SET_BUILDING(pk::building@ building)
        {
            //pk::trace("DLG148_SET_BUILDING:" + dlg148_.get_p());
            pk::dialog@dlg148_ = pk::get_dialog(148);
            if (dlg148_ !is null)
            {
                //pk::trace("DLG148_SET_BUILDING");
                int spec_id = ch::get_spec_id(building);
                if (ch::is_valid_spec_id(spec_id))
                {
                    //设置组件延长--应该放到set里而不是init
                    dlg148_.set_size(pk::size(148, 203));
                    pk::widget@ bg = dlg148_.find_child(7283);
                    bg.set_size(136, 199);

                    pk::widget@ widget = dlg148_.find_child(7292);
                    widget.set_size(4, 199 - 12);
                    @ widget = dlg148_.find_child(7293);
                    widget.set_size(4, 199 - 12);
                    @ widget = dlg148_.find_child(7294);
                    widget.set_pos(12, 199);
                    @ widget = dlg148_.find_child(7297);
                    widget.set_pos(0, 191);
                    @ widget = dlg148_.find_child(7298);
                    widget.set_pos(128, 191);

                    @ widget = dlg148_.find_child(7286);
                    widget.set_visible(false);
                    @ widget = dlg148_.find_child(7287);
                    widget.set_visible(false);
                    @ widget = dlg148_.find_child(7290);
                    widget.set_visible(false);
                    @ widget = dlg148_.find_child(7291);
                    pk::text@ hp_w = @widget;
                    string hp_text = hp_w.get_text();
                    widget.set_visible(false);

                    for (int i = 0; i < 6; ++i)
                    {
                        bbg0_[i].set_visible(true);//府的情况下再显示
                        btext0_[i].set_visible(true);//府的情况下再显示
                        bbg1_[i].set_visible(true);//府的情况下再显示
                        btext1_[i].set_visible(true);//府的情况下再显示
                    }

                    specialinfo@ spec_t = @special_ex[spec_id];
                    int person_id = spec_t.person;
                    // const array<string> desc_148 = { "将军", "资金", "兵粮","人口","耐久","府兵"};
                    @ widget = dlg148_.find_child(7285);
                    pk::text@ name_w = @widget;
                    widget.set_size(80, 16);
                    string spec_desc = pk::is_valid_person_id(person_id) ? "封地" : "县城";
                    name_w.set_text(pk::format("{}({})", pk::encode(ch::get_spec_name(spec_id)), pk::encode(spec_desc)));

                    string name = pk::is_valid_person_id(person_id) ? pk::get_name(pk::get_person(person_id)) : pk::encode("无");
                    btext1_[0].set_text(name);

                    btext1_[1].set_text(pk::format("{}", pk::is_valid_person_id(person_id) ? spec_t.gold : 0));
                    btext1_[2].set_text(pk::format("{}", pk::is_valid_person_id(person_id) ? spec_t.food : 0));
                    btext1_[3].set_text(pk::format("{}", spec_t.population));
                    btext1_[4].set_text(hp_text);
                    btext1_[5].set_text(pk::format("{}", spec_t.troops));

                }
                else
                {
                    //设置组件延长--应该放到set里而不是init
                    dlg148_.set_size(pk::size(140, 79));
                    pk::widget@ bg = dlg148_.find_child(7283);
                    bg.set_size(132, 75);

                    pk::widget@ widget = dlg148_.find_child(7292);
                    widget.set_size(4, 59);
                    @ widget = dlg148_.find_child(7293);
                    widget.set_size(4, 59);
                    @ widget = dlg148_.find_child(7294);
                    widget.set_pos(12, 79);
                    @ widget = dlg148_.find_child(7297);
                    widget.set_pos(0, 71);
                    @ widget = dlg148_.find_child(7298);
                    widget.set_pos(128, 71);

                    if (!pk::is_trap_type(building) and building.facility != 设施_堤防)
                    {
                        @ widget = dlg148_.find_child(7286);
                        widget.set_visible(true);
                        @ widget = dlg148_.find_child(7287);
                        widget.set_visible(true);
                        @ widget = dlg148_.find_child(7290);
                        widget.set_visible(true);
                        @ widget = dlg148_.find_child(7291);
                        widget.set_visible(true);
                    }


                    for (int i = 0; i < 6; ++i)
                    {
                        bbg0_[i].set_visible(false);//府的情况下再显示
                        btext0_[i].set_visible(false);//府的情况下再显示
                        bbg1_[i].set_visible(false);//府的情况下再显示
                        btext1_[i].set_visible(false);//府的情况下再显示
                    }

                }
            }

        }

        void BUILDING_HUD_INIT(pk::building@ building, pk::hud_container@ hud_container)
        {
            int spec_id = ch::get_spec_id(building);
            if (ch::is_valid_spec_id(spec_id))
            {
                string name = ch::get_spec_name(spec_id);
                pk::text_hud@ text = hud_container.create_text();
                text.set_text(pk::encode(name));
                text.set_pos(5, 3);
                pk::number_hud@ num = hud_container.create_number();
                num.set_value(special_ex[spec_id].troops);
                num.set_max_width(6);
                num.set_pos(23, 20);
            }

        }

        void hook148(pk::dialog@ dialog)
        {
            for (int i = 0; i < 6; ++i)
            {
                pk::sprite9@ bg0 = dialog.create_sprite9(393);
                bg0.set_size(42, 22);
                bg0.set_pos(7, 54 + 24 * i);
                bg0.set_visible(false);//府的情况下再显示
                @bbg0_[i] = @bg0;

                pk::text@ text0 = dialog.create_text();
                text0.set_size(32, 16);
                text0.set_pos(7 + 5, 54 + 24 * i + 3);
                text0.set_text(pk::encode(desc_148[i]));
                text0.set_visible(false);
                @btext0_[i] = @text0;


                pk::sprite9@ bg1 = dialog.create_sprite9(411);
                bg1.set_size(82, 22);
                bg1.set_pos(51, 54 + 24 * i);
                bg1.set_visible(false);
                @bbg1_[i] = @bg1;

                pk::text@ text1 = dialog.create_text();
                text1.set_size(72, 16);
                text1.set_pos(51 + 5, 54 + 24 * i + 3);
                text1.set_text_align(2);
                @btext1_[i] = @text1;
            }


            //搜索对应setDLG最快的方法是搜索对应组件text的id
            // 
            //7285是阵的名字

            //pk::sprite9@ bg = dialog.create_sprite9(411);
            //bg.set_size(pk::size(148,201));//原132，75
            //
            //bg.set_pos(pk::point(0, 0));
        }

        void hook126(pk::dialog@ dialog)
        {
            pk::button@ btn5795 = dialog.find_child(5795);
            pk::button@ btn5800 = dialog.find_child(5800);
            pk::button@ btn5801 = dialog.find_child(5801);
            pk::button@ btn5802 = dialog.find_child(5802);
            pk::button@ btn5805 = dialog.find_child(5805);

            btn5800.set_pos(253 + 76, 652);// 部队
            btn5801.set_pos(253+76 * 2,652);// 武将
            btn5802.set_pos(253 + 76 * 4, 652);// 外交
            btn5795.set_pos(253 + 76 * 5, 652);// 技巧表
            btn5805.set_pos(253 + 76 * 6, 652);// 能力表 

            pk::button@ nbtn_126 = dialog.create_button(214);
            @ nbtn_126_ = @nbtn_126;
            nbtn_126.set_pos(253, 652);
            nbtn_126.set_size(68,36);
            nbtn_126.set_text(pk::encode("县城"));
            //没有县城时，灰色？
            pk::detail::funcref func0 = cast<pk::button_on_released_t@>(function(button) {
                main.show_force_spec_listview(main.force126_);
                //隐藏主菜单
            });
            nbtn_126.on_button_released(func0);

            pk::button@ nbtn_item = dialog.create_button(214); // 国库
            nbtn_item.set_pos(253 + 76 * 3, 652);
            nbtn_item.set_size(68, 36);
            nbtn_item.set_text(pk::encode("道具"));
            func0 = cast<pk::button_on_released_t@>(function(button) {
                pk::list<pk::item@> force_item_list;
                pk::list<pk::item@> select_item_list;
                for (int i = 0; i < 901; i += 1)
                {
                    pk::item@ item = pk::get_item(i);
                    if (item !is null and item.status == 宝物状态_登场)
                    {
                        pk::person@ person = pk::get_person(item.owner);
                        if (person !is null && person.get_force_id() == main.force126_.get_id())
                        {
                            force_item_list.add(item);
                        } else if (item.owner >= 武将_末 and (item.owner - 武将_末) == main.force126_.get_id())
                        {
                            force_item_list.add(item);
                        }
                    }
                }
                
                pk::list<pk::item@> item_list = pk::item_selector(
                    pk::encode('势力道具'),
                    pk::encode('势力武将所有道具以及国库道具'),
                    force_item_list,
                    0,
                    0,
                    select_item_list,
                    1
                );
            });
            nbtn_item.on_button_released(func0);
        }

        void hook128(pk::dialog@ dialog)
        {
            pk::button@ btn5953 = dialog.find_child(5953);
            pk::button@ btn5954 = dialog.find_child(5954);

            btn5953.set_pos(177, 652);//(btn5953.get_pos().x +76, btn5953.get_pos().y);
            btn5954.set_pos(177 + 76, 652);// (btn5954.get_pos().x + 76, btn5954.get_pos().y);

            pk::button@ nbtn_128 = dialog.create_button(214);
            @ nbtn_128_ = @nbtn_128;
            nbtn_128.set_pos(101, 652);
            nbtn_128.set_size(68, 36);
            nbtn_128.set_text(pk::encode("县城"));
            //没有县城时，灰色？
            pk::detail::funcref func0 = cast<pk::button_on_released_t@>(function(button) {

                main.show_district_spec_listview(main.district128_);
                //隐藏主菜单
            });
            nbtn_128.on_button_released(func0);
        }

        void hook129(pk::dialog@ dialog)
        {
            pk::button@ btn6027 = dialog.find_child(6027);
            pk::button@ btn6025 = dialog.find_child(6025);

            btn6025.set_pos(177+76, 652);//(btn6025.get_pos().x +76, btn6025.get_pos().y);
            btn6027.set_pos(177,652);// (btn6027.get_pos().x + 76, btn6027.get_pos().y);

            pk::button@ nbtn_129 = dialog.create_button(214);
            @ nbtn_129_ = @nbtn_129;
            nbtn_129.set_pos(101, 652);
            nbtn_129.set_size(68,36);
            nbtn_129.set_text(pk::encode("县城"));
            //没有县城时，灰色？
            pk::detail::funcref func0 = cast<pk::button_on_released_t@>(function(button) {

                main.show_city_spec_listview(main.city129_);
                //隐藏主菜单
            });
            nbtn_129.on_button_released(func0);
        }

        void show_city_spec_listview(pk::city@ city)
        {
            auto arr = ch::get_city_able_spec_int16(city);
            UI::spec_selector(pk::encode("县城一览"), pk::encode("县城一览"), arr, 0, 0);
        }

        void show_force_spec_listview(pk::force@ force)
        {
            auto arr = ch::get_force_able_spec_int16(force);
            UI::spec_selector(pk::encode("县城一览"), pk::encode("县城一览"), arr, 0, 0);
        }

        void show_district_spec_listview(pk::district@ district)
        {
            auto arr = ch::get_district_able_spec_int16(district);
            UI::spec_selector(pk::encode("县城一览"), pk::encode("县城一览"), arr, 0, 0);
        }
    }

    Main main;

}