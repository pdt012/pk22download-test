﻿// ## 2023/05/12 # 江东新风 # mod地图如果已经把五废港用起来，那需要在此设置调整ai ##


namespace 五废港调整
{

	class Main
	{

		Main()
		{
			pk::bind(103, pk::trigger103_t(callback)); // 每次剧本初始化时，调整五废港
		}

		void callback()
		{
			if (!pk::is_new_map()) pk::set_wufeigang(false);//地图为0328时，关闭五废港ai
			else if (pk::is_new_map())pk::set_wufeigang(true);//地图为大浪时，开启五废港ai
		}


	}

	Main main;
}